#!/usr/bin/env python2.7

# Tnode := [cat, action, LorRHead, headIndex]
# Lnode := [cat, action, POS, word]

# pipe note:
# ( move down one level, ) move up one level

import argparse
import re


def ProcessPipe(pipe, args):
    nodes = []
    sortedNodes = []
    parseCount = 0
    unaryCount = 0
    lineCount = 0
    printedFirstLine = False
    
        
    if (args.unaryRoot): 
        outFileUnaryRoot = open(args.unaryRoot, 'w+')
    if (args.outputfile):
        out = open(args.outputfile, 'w+')

    for line in pipe:

        if line == '###\n':
            if (args.outputfile):
                out.write('###' + '\n')
            elif args.lineID:
                print '###' + str(parseCount+1)
            else:
                print '###'
            parseCount = parseCount + 1
            printedFirstLine = False
            continue
        splitLine = line.rstrip('\n').split()
        if len(splitLine) > 1 and splitLine[0][2] == 'T':
            assert('\\\\' not in splitLine[2]);
            assert('//' not in splitLine[2]);
            assert(len(splitLine) == 6)
            assert(int(splitLine[5][0]) in [1, 2])
            rawCat = splitLine[2]
            if args.rmNUM2 and rawCat == 'N[num]' and int(splitLine[5][0]) == 2:
                rawCat = 'N'
            if rawCat.find('[conj]') != -1:
                newCat = Find(rawCat)
            else: newCat = rawCat
            if '\\' in newCat or '/' in newCat:
                bracketedCat = '(' + newCat + ')' 
                nodes.append([bracketedCat, int(splitLine[5][0]), int(splitLine[4])])
            else: 
                nodes.append([newCat, int(splitLine[5][0]), int(splitLine[4])])
            
            if (args.unaryRoot):
                if lineCount == 1:
                    #debug
                    if int(splitLine[5][0]) == 1:
                        outFileUnaryRoot.write('###\n')
                        unaryCount = unaryCount + 1
                        printedFirstLine = True
        if (args.unaryRoot):            
            if printedFirstLine:
                outFileUnaryRoot.write(line)


          
        if len(splitLine) > 1 and splitLine[0][2] == 'L':
            assert(len(splitLine) == 5)
            assert('\\\\' not in splitLine[2]);
            assert('//' not in splitLine[2]);
            rawCat = splitLine[2]
            if rawCat.find('[conj]') != -1:
                newCat = Find(rawCat)
            else: newCat = rawCat
            if '\\' in newCat or '/' in newCat:
                bracketedCat = '(' + newCat + ')' 
                nodes.append([bracketedCat, 0, splitLine[3], splitLine[4][:-1]])
            else:
                nodes.append([newCat, 0, splitLine[3], splitLine[4][:-1]])
            
        if splitLine != [] and splitLine[0] == ')':
            sortedNodes.append(nodes[-1])
            del nodes[-1]  
            
        if line == '\n':
            if sortedNodes != []:
                #debug
                #print nodes
                       
                del nodes[:]
                SetHeads(sortedNodes)
                #debug
                #print sortedNodes[-1]
                if len(sortedNodes) >= 2:
                    # last action cannot be shift
                    assert(sortedNodes[-1][1] != 0)
                #sortedNodes[-1][1] = 3, finish action no longer needed
                if (args.outputfile):
                    for item in sortedNodes:
                        out.write(' '.join(str(elt) for elt in item))
                        out.write('\n')              
                else:
                    for item in sortedNodes:
                        print ' '.join(str(elt) for elt in item)
                del sortedNodes[:]
        
        lineCount = lineCount + 1
                  
    #if (args.unaryRoot):    
        #print 'total at root: ' + str(unaryCount)
     

    
def SetHeads(sortedNodes):
    for node in sortedNodes:
        i = sortedNodes.index(node)
        if node[1] != 0:
            if node[1] == 1:
                node.append(i - 1)
                # first node must not from unary
                # so the number just appended must not be less than 0
                assert(node[-1] >= 0) 
            if node[1] == 2:
                if node[2] == 1:
                   node.append(i - 1)
                   # first right child starts from position 1             
                   assert(node[-1] >= 1)
                if node[2] == 0:
                    j = i
                    while 1:
                        j = j - 1;
                        prevNode = sortedNodes[j]
                        if prevNode[1] != 1:
                            node.append(j - 1)
                            break
               
              
def Find(cat):
    endInd = cat.find('[conj]')
    subString = cat[:endInd]
    if '\\' in subString or '/' in subString:
        bracketedCat = '(' + subString + ')'
        return Convert(bracketedCat)
    return Convert(subString)
    
                 
def Convert(cat):
    newCat = cat + '\\' + cat
    return newCat                            


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Process wsj02-21.pipe')
    parser.add_argument('-u', '--unaryroot', dest = 'unaryRoot') 
    parser.add_argument('-f', '--file', dest = 'outputfile', default = None)
    parser.add_argument('-n', '--rmnum', action='store_true', dest = 'rmNUM2', default = False)
    parser.add_argument('-i', '--id', dest = 'lineID', default = False)
    args = parser.parse_args()
  
    
    #pipe = open('/local/scratch/wx217/experiment/gold/wsj02-21.pipe', 'r')
    pipe = open('/home/xwd/Desktop/cacrelated/experiment/gold/wsj02-21.pipe', 'r')
    #pipe = open('example', 'r')
    ProcessPipe(pipe, args)

