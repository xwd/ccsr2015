################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CC_SRCS += \
../src/tests/test_categories.cc \
../src/tests/test_format.cc \
../src/tests/test_hashtables.cc \
../src/tests/test_offset_vector.cc \
../src/tests/test_options.cc \
../src/tests/test_readers.cc 

CC_DEPS += \
./src/tests/test_categories.d \
./src/tests/test_format.d \
./src/tests/test_hashtables.d \
./src/tests/test_offset_vector.d \
./src/tests/test_options.d \
./src/tests/test_readers.d 

OBJS += \
./src/tests/test_categories.o \
./src/tests/test_format.o \
./src/tests/test_hashtables.o \
./src/tests/test_offset_vector.o \
./src/tests/test_options.o \
./src/tests/test_readers.o 


# Each subdirectory must supply rules for building sources it contributes
src/tests/%.o: ../src/tests/%.cc
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


