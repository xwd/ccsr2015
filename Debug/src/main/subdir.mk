################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CC_SRCS += \
../src/main/bootstrap.cc \
../src/main/candc.cc \
../src/main/chunk.cc \
../src/main/collect_grs.cc \
../src/main/count_rules.cc \
../src/main/forests.cc \
../src/main/generate.cc \
../src/main/lexicon.cc \
../src/main/mchunk.cc \
../src/main/mner.cc \
../src/main/mpos.cc \
../src/main/msuper.cc \
../src/main/ner.cc \
../src/main/parser.cc \
../src/main/pos.cc \
../src/main/reestimate.cc \
../src/main/relations.cc \
../src/main/rnn_parser.cc \
../src/main/rnn_pos.cc \
../src/main/rnn_super.cc \
../src/main/super.cc \
../src/main/thesaurus.cc \
../src/main/train_chunk.cc \
../src/main/train_class.cc \
../src/main/train_ner.cc \
../src/main/train_perceptron.cc \
../src/main/train_pos.cc \
../src/main/train_rnn_parser.cc \
../src/main/train_rnn_parser_xf1.cc \
../src/main/train_rnn_super_mt.cc \
../src/main/train_super.cc \
../src/main/train_xf1.cc \
../src/main/train_xf1_original.cc \
../src/main/tree_gis.cc 

O_SRCS += \
../src/main/msuper.o \
../src/main/pos.o \
../src/main/rnn_parser.o \
../src/main/rnn_pos.o \
../src/main/rnn_super.o \
../src/main/super.o \
../src/main/train_rnn_parser.o \
../src/main/train_rnn_parser_xf1.o \
../src/main/train_rnn_super_mt.o 

CC_DEPS += \
./src/main/bootstrap.d \
./src/main/candc.d \
./src/main/chunk.d \
./src/main/collect_grs.d \
./src/main/count_rules.d \
./src/main/forests.d \
./src/main/generate.d \
./src/main/lexicon.d \
./src/main/mchunk.d \
./src/main/mner.d \
./src/main/mpos.d \
./src/main/msuper.d \
./src/main/ner.d \
./src/main/parser.d \
./src/main/pos.d \
./src/main/reestimate.d \
./src/main/relations.d \
./src/main/rnn_parser.d \
./src/main/rnn_pos.d \
./src/main/rnn_super.d \
./src/main/super.d \
./src/main/thesaurus.d \
./src/main/train_chunk.d \
./src/main/train_class.d \
./src/main/train_ner.d \
./src/main/train_perceptron.d \
./src/main/train_pos.d \
./src/main/train_rnn_parser.d \
./src/main/train_rnn_parser_xf1.d \
./src/main/train_rnn_super_mt.d \
./src/main/train_super.d \
./src/main/train_xf1.d \
./src/main/train_xf1_original.d \
./src/main/tree_gis.d 

OBJS += \
./src/main/bootstrap.o \
./src/main/candc.o \
./src/main/chunk.o \
./src/main/collect_grs.o \
./src/main/count_rules.o \
./src/main/forests.o \
./src/main/generate.o \
./src/main/lexicon.o \
./src/main/mchunk.o \
./src/main/mner.o \
./src/main/mpos.o \
./src/main/msuper.o \
./src/main/ner.o \
./src/main/parser.o \
./src/main/pos.o \
./src/main/reestimate.o \
./src/main/relations.o \
./src/main/rnn_parser.o \
./src/main/rnn_pos.o \
./src/main/rnn_super.o \
./src/main/super.o \
./src/main/thesaurus.o \
./src/main/train_chunk.o \
./src/main/train_class.o \
./src/main/train_ner.o \
./src/main/train_perceptron.o \
./src/main/train_pos.o \
./src/main/train_rnn_parser.o \
./src/main/train_rnn_parser_xf1.o \
./src/main/train_rnn_super_mt.o \
./src/main/train_super.o \
./src/main/train_xf1.o \
./src/main/train_xf1_original.o \
./src/main/tree_gis.o 


# Each subdirectory must supply rules for building sources it contributes
src/main/%.o: ../src/main/%.cc
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


