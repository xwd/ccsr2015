################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CC_SRCS += \
../src/lib/parser/ShiftReduceFeature.cc \
../src/lib/parser/ShiftReduceHypothesis.cc \
../src/lib/parser/atom.cc \
../src/lib/parser/beam.cc \
../src/lib/parser/canonical.cc \
../src/lib/parser/categories.cc \
../src/lib/parser/category.cc \
../src/lib/parser/chart.cc \
../src/lib/parser/count_rules.cc \
../src/lib/parser/decoder.cc \
../src/lib/parser/decoder_deps_recall.cc \
../src/lib/parser/decoder_deps_sum.cc \
../src/lib/parser/decoder_derivs.cc \
../src/lib/parser/decoder_derivs_random.cc \
../src/lib/parser/decoder_factory.cc \
../src/lib/parser/depscore.cc \
../src/lib/parser/equiv.cc \
../src/lib/parser/feature.cc \
../src/lib/parser/feature_dep.cc \
../src/lib/parser/feature_dep_dist.cc \
../src/lib/parser/feature_genrule.cc \
../src/lib/parser/feature_root.cc \
../src/lib/parser/feature_rule.cc \
../src/lib/parser/feature_rule_dep.cc \
../src/lib/parser/feature_rule_dep_dist.cc \
../src/lib/parser/feature_rule_head.cc \
../src/lib/parser/generator.cc \
../src/lib/parser/gr.cc \
../src/lib/parser/gr_constraints.cc \
../src/lib/parser/inside_outside.cc \
../src/lib/parser/integration.cc \
../src/lib/parser/markedup.cc \
../src/lib/parser/parser.cc \
../src/lib/parser/print_ccgbank.cc \
../src/lib/parser/print_deps.cc \
../src/lib/parser/print_factory.cc \
../src/lib/parser/print_forest.cc \
../src/lib/parser/print_grs.cc \
../src/lib/parser/print_prolog.cc \
../src/lib/parser/print_xml.cc \
../src/lib/parser/relations.cc \
../src/lib/parser/rule.cc \
../src/lib/parser/rule_instances.cc \
../src/lib/parser/supercat.cc \
../src/lib/parser/treebank.cc \
../src/lib/parser/unify.cc \
../src/lib/parser/variable.cc \
../src/lib/parser/varid.cc 

O_SRCS += \
../src/lib/parser/ShiftReduceFeature.o \
../src/lib/parser/ShiftReduceHypothesis.o \
../src/lib/parser/atom.o \
../src/lib/parser/beam.o \
../src/lib/parser/canonical.o \
../src/lib/parser/categories.o \
../src/lib/parser/category.o \
../src/lib/parser/chart.o \
../src/lib/parser/count_rules.o \
../src/lib/parser/decoder.o \
../src/lib/parser/decoder_deps_recall.o \
../src/lib/parser/decoder_derivs.o \
../src/lib/parser/decoder_derivs_random.o \
../src/lib/parser/decoder_factory.o \
../src/lib/parser/depscore.o \
../src/lib/parser/equiv.o \
../src/lib/parser/feature.o \
../src/lib/parser/feature_dep.o \
../src/lib/parser/feature_dep_dist.o \
../src/lib/parser/feature_genrule.o \
../src/lib/parser/feature_root.o \
../src/lib/parser/feature_rule.o \
../src/lib/parser/feature_rule_dep.o \
../src/lib/parser/feature_rule_dep_dist.o \
../src/lib/parser/feature_rule_head.o \
../src/lib/parser/generator.o \
../src/lib/parser/gr.o \
../src/lib/parser/gr_constraints.o \
../src/lib/parser/inside_outside.o \
../src/lib/parser/integration.o \
../src/lib/parser/markedup.o \
../src/lib/parser/parser.o \
../src/lib/parser/print_ccgbank.o \
../src/lib/parser/print_deps.o \
../src/lib/parser/print_factory.o \
../src/lib/parser/print_forest.o \
../src/lib/parser/print_grs.o \
../src/lib/parser/print_prolog.o \
../src/lib/parser/print_xml.o \
../src/lib/parser/relations.o \
../src/lib/parser/rule.o \
../src/lib/parser/rule_instances.o \
../src/lib/parser/supercat.o \
../src/lib/parser/treebank.o \
../src/lib/parser/unify.o \
../src/lib/parser/variable.o \
../src/lib/parser/varid.o 

CC_DEPS += \
./src/lib/parser/ShiftReduceFeature.d \
./src/lib/parser/ShiftReduceHypothesis.d \
./src/lib/parser/atom.d \
./src/lib/parser/beam.d \
./src/lib/parser/canonical.d \
./src/lib/parser/categories.d \
./src/lib/parser/category.d \
./src/lib/parser/chart.d \
./src/lib/parser/count_rules.d \
./src/lib/parser/decoder.d \
./src/lib/parser/decoder_deps_recall.d \
./src/lib/parser/decoder_deps_sum.d \
./src/lib/parser/decoder_derivs.d \
./src/lib/parser/decoder_derivs_random.d \
./src/lib/parser/decoder_factory.d \
./src/lib/parser/depscore.d \
./src/lib/parser/equiv.d \
./src/lib/parser/feature.d \
./src/lib/parser/feature_dep.d \
./src/lib/parser/feature_dep_dist.d \
./src/lib/parser/feature_genrule.d \
./src/lib/parser/feature_root.d \
./src/lib/parser/feature_rule.d \
./src/lib/parser/feature_rule_dep.d \
./src/lib/parser/feature_rule_dep_dist.d \
./src/lib/parser/feature_rule_head.d \
./src/lib/parser/generator.d \
./src/lib/parser/gr.d \
./src/lib/parser/gr_constraints.d \
./src/lib/parser/inside_outside.d \
./src/lib/parser/integration.d \
./src/lib/parser/markedup.d \
./src/lib/parser/parser.d \
./src/lib/parser/print_ccgbank.d \
./src/lib/parser/print_deps.d \
./src/lib/parser/print_factory.d \
./src/lib/parser/print_forest.d \
./src/lib/parser/print_grs.d \
./src/lib/parser/print_prolog.d \
./src/lib/parser/print_xml.d \
./src/lib/parser/relations.d \
./src/lib/parser/rule.d \
./src/lib/parser/rule_instances.d \
./src/lib/parser/supercat.d \
./src/lib/parser/treebank.d \
./src/lib/parser/unify.d \
./src/lib/parser/variable.d \
./src/lib/parser/varid.d 

OBJS += \
./src/lib/parser/ShiftReduceFeature.o \
./src/lib/parser/ShiftReduceHypothesis.o \
./src/lib/parser/atom.o \
./src/lib/parser/beam.o \
./src/lib/parser/canonical.o \
./src/lib/parser/categories.o \
./src/lib/parser/category.o \
./src/lib/parser/chart.o \
./src/lib/parser/count_rules.o \
./src/lib/parser/decoder.o \
./src/lib/parser/decoder_deps_recall.o \
./src/lib/parser/decoder_deps_sum.o \
./src/lib/parser/decoder_derivs.o \
./src/lib/parser/decoder_derivs_random.o \
./src/lib/parser/decoder_factory.o \
./src/lib/parser/depscore.o \
./src/lib/parser/equiv.o \
./src/lib/parser/feature.o \
./src/lib/parser/feature_dep.o \
./src/lib/parser/feature_dep_dist.o \
./src/lib/parser/feature_genrule.o \
./src/lib/parser/feature_root.o \
./src/lib/parser/feature_rule.o \
./src/lib/parser/feature_rule_dep.o \
./src/lib/parser/feature_rule_dep_dist.o \
./src/lib/parser/feature_rule_head.o \
./src/lib/parser/generator.o \
./src/lib/parser/gr.o \
./src/lib/parser/gr_constraints.o \
./src/lib/parser/inside_outside.o \
./src/lib/parser/integration.o \
./src/lib/parser/markedup.o \
./src/lib/parser/parser.o \
./src/lib/parser/print_ccgbank.o \
./src/lib/parser/print_deps.o \
./src/lib/parser/print_factory.o \
./src/lib/parser/print_forest.o \
./src/lib/parser/print_grs.o \
./src/lib/parser/print_prolog.o \
./src/lib/parser/print_xml.o \
./src/lib/parser/relations.o \
./src/lib/parser/rule.o \
./src/lib/parser/rule_instances.o \
./src/lib/parser/supercat.o \
./src/lib/parser/treebank.o \
./src/lib/parser/unify.o \
./src/lib/parser/variable.o \
./src/lib/parser/varid.o 


# Each subdirectory must supply rules for building sources it contributes
src/lib/parser/%.o: ../src/lib/parser/%.cc
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


