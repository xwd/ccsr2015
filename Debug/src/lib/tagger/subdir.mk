################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CC_SRCS += \
../src/lib/tagger/_baseimpl.cc \
../src/lib/tagger/chunk.cc \
../src/lib/tagger/ner.cc \
../src/lib/tagger/pos.cc \
../src/lib/tagger/super.cc \
../src/lib/tagger/tagdict.cc \
../src/lib/tagger/tagger.cc \
../src/lib/tagger/taghist.cc \
../src/lib/tagger/tagsetdict.cc \
../src/lib/tagger/unigram.cc 

O_SRCS += \
../src/lib/tagger/_baseimpl.o \
../src/lib/tagger/pos.o \
../src/lib/tagger/super.o \
../src/lib/tagger/tagdict.o \
../src/lib/tagger/tagger.o \
../src/lib/tagger/taghist.o \
../src/lib/tagger/tagsetdict.o \
../src/lib/tagger/unigram.o 

CC_DEPS += \
./src/lib/tagger/_baseimpl.d \
./src/lib/tagger/chunk.d \
./src/lib/tagger/ner.d \
./src/lib/tagger/pos.d \
./src/lib/tagger/super.d \
./src/lib/tagger/tagdict.d \
./src/lib/tagger/tagger.d \
./src/lib/tagger/taghist.d \
./src/lib/tagger/tagsetdict.d \
./src/lib/tagger/unigram.d 

OBJS += \
./src/lib/tagger/_baseimpl.o \
./src/lib/tagger/chunk.o \
./src/lib/tagger/ner.o \
./src/lib/tagger/pos.o \
./src/lib/tagger/super.o \
./src/lib/tagger/tagdict.o \
./src/lib/tagger/tagger.o \
./src/lib/tagger/taghist.o \
./src/lib/tagger/tagsetdict.o \
./src/lib/tagger/unigram.o 


# Each subdirectory must supply rules for building sources it contributes
src/lib/tagger/%.o: ../src/lib/tagger/%.cc
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


