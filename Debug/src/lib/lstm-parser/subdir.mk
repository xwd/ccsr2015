################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CC_SRCS += \
../src/lib/lstm-parser/cnn_lstm_parser.cc \
../src/lib/lstm-parser/lstm_parser.cc \
../src/lib/lstm-parser/rnn_canonical.cc \
../src/lib/lstm-parser/rnn_parser_hinge_temp.cc \
../src/lib/lstm-parser/rnn_rule.cc 

CC_DEPS += \
./src/lib/lstm-parser/cnn_lstm_parser.d \
./src/lib/lstm-parser/lstm_parser.d \
./src/lib/lstm-parser/rnn_canonical.d \
./src/lib/lstm-parser/rnn_parser_hinge_temp.d \
./src/lib/lstm-parser/rnn_rule.d 

OBJS += \
./src/lib/lstm-parser/cnn_lstm_parser.o \
./src/lib/lstm-parser/lstm_parser.o \
./src/lib/lstm-parser/rnn_canonical.o \
./src/lib/lstm-parser/rnn_parser_hinge_temp.o \
./src/lib/lstm-parser/rnn_rule.o 


# Each subdirectory must supply rules for building sources it contributes
src/lib/lstm-parser/%.o: ../src/lib/lstm-parser/%.cc
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


