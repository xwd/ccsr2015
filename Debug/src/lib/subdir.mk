################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CC_SRCS += \
../src/lib/base.cc \
../src/lib/candc.cc \
../src/lib/classifier.cc \
../src/lib/cluster.cc \
../src/lib/gazetteers.cc \
../src/lib/huge.cc \
../src/lib/input.cc \
../src/lib/lexicon.cc \
../src/lib/licence.cc \
../src/lib/tagset.cc \
../src/lib/timer.cc \
../src/lib/version.cc \
../src/lib/word.cc 

O_SRCS += \
../src/lib/base.o \
../src/lib/gazetteers.o \
../src/lib/input.o \
../src/lib/lexicon.o \
../src/lib/licence.o \
../src/lib/tagset.o \
../src/lib/timer.o \
../src/lib/version.o \
../src/lib/word.o 

CC_DEPS += \
./src/lib/base.d \
./src/lib/candc.d \
./src/lib/classifier.d \
./src/lib/cluster.d \
./src/lib/gazetteers.d \
./src/lib/huge.d \
./src/lib/input.d \
./src/lib/lexicon.d \
./src/lib/licence.d \
./src/lib/tagset.d \
./src/lib/timer.d \
./src/lib/version.d \
./src/lib/word.d 

OBJS += \
./src/lib/base.o \
./src/lib/candc.o \
./src/lib/classifier.o \
./src/lib/cluster.o \
./src/lib/gazetteers.o \
./src/lib/huge.o \
./src/lib/input.o \
./src/lib/lexicon.o \
./src/lib/licence.o \
./src/lib/tagset.o \
./src/lib/timer.o \
./src/lib/version.o \
./src/lib/word.o 


# Each subdirectory must supply rules for building sources it contributes
src/lib/%.o: ../src/lib/%.cc
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


