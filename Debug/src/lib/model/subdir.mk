################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CC_SRCS += \
../src/lib/model/affix.cc \
../src/lib/model/cache.cc \
../src/lib/model/model.cc \
../src/lib/model/registry.cc \
../src/lib/model/types.cc \
../src/lib/model/unigram.cc 

O_SRCS += \
../src/lib/model/affix.o \
../src/lib/model/cache.o \
../src/lib/model/model.o \
../src/lib/model/registry.o \
../src/lib/model/types.o \
../src/lib/model/unigram.o 

CC_DEPS += \
./src/lib/model/affix.d \
./src/lib/model/cache.d \
./src/lib/model/model.d \
./src/lib/model/registry.d \
./src/lib/model/types.d \
./src/lib/model/unigram.d 

OBJS += \
./src/lib/model/affix.o \
./src/lib/model/cache.o \
./src/lib/model/model.o \
./src/lib/model/registry.o \
./src/lib/model/types.o \
./src/lib/model/unigram.o 


# Each subdirectory must supply rules for building sources it contributes
src/lib/model/%.o: ../src/lib/model/%.cc
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


