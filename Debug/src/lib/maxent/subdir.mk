################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CC_SRCS += \
../src/lib/maxent/bfgs.cc \
../src/lib/maxent/gis.cc 

CC_DEPS += \
./src/lib/maxent/bfgs.d \
./src/lib/maxent/gis.d 

OBJS += \
./src/lib/maxent/bfgs.o \
./src/lib/maxent/gis.o 


# Each subdirectory must supply rules for building sources it contributes
src/lib/maxent/%.o: ../src/lib/maxent/%.cc
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


