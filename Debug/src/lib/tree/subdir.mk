################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CC_SRCS += \
../src/lib/tree/attributes.cc \
../src/lib/tree/bfgs.cc \
../src/lib/tree/catvalue.cc \
../src/lib/tree/depdist.cc \
../src/lib/tree/dependency.cc \
../src/lib/tree/distance.cc \
../src/lib/tree/forest.cc \
../src/lib/tree/genrule.cc \
../src/lib/tree/gis.cc \
../src/lib/tree/node.cc \
../src/lib/tree/options.cc \
../src/lib/tree/perceptron.cc \
../src/lib/tree/rule.cc 

O_SRCS += \
../src/lib/tree/catvalue.o \
../src/lib/tree/depdist.o \
../src/lib/tree/dependency.o \
../src/lib/tree/distance.o \
../src/lib/tree/genrule.o \
../src/lib/tree/rule.o 

CC_DEPS += \
./src/lib/tree/attributes.d \
./src/lib/tree/bfgs.d \
./src/lib/tree/catvalue.d \
./src/lib/tree/depdist.d \
./src/lib/tree/dependency.d \
./src/lib/tree/distance.d \
./src/lib/tree/forest.d \
./src/lib/tree/genrule.d \
./src/lib/tree/gis.d \
./src/lib/tree/node.d \
./src/lib/tree/options.d \
./src/lib/tree/perceptron.d \
./src/lib/tree/rule.d 

OBJS += \
./src/lib/tree/attributes.o \
./src/lib/tree/bfgs.o \
./src/lib/tree/catvalue.o \
./src/lib/tree/depdist.o \
./src/lib/tree/dependency.o \
./src/lib/tree/distance.o \
./src/lib/tree/forest.o \
./src/lib/tree/genrule.o \
./src/lib/tree/gis.o \
./src/lib/tree/node.o \
./src/lib/tree/options.o \
./src/lib/tree/perceptron.o \
./src/lib/tree/rule.o 


# Each subdirectory must supply rules for building sources it contributes
src/lib/tree/%.o: ../src/lib/tree/%.cc
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


