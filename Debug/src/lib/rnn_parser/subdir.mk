################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CC_SRCS += \
../src/lib/rnn_parser/rnn_canonical.cc \
../src/lib/rnn_parser/rnn_parser.cc \
../src/lib/rnn_parser/rnn_parser_hinge_temp.cc \
../src/lib/rnn_parser/rnn_rule.cc 

O_SRCS += \
../src/lib/rnn_parser/rnn_canonical.o \
../src/lib/rnn_parser/rnn_parser.o \
../src/lib/rnn_parser/rnn_rule.o 

CC_DEPS += \
./src/lib/rnn_parser/rnn_canonical.d \
./src/lib/rnn_parser/rnn_parser.d \
./src/lib/rnn_parser/rnn_parser_hinge_temp.d \
./src/lib/rnn_parser/rnn_rule.d 

OBJS += \
./src/lib/rnn_parser/rnn_canonical.o \
./src/lib/rnn_parser/rnn_parser.o \
./src/lib/rnn_parser/rnn_parser_hinge_temp.o \
./src/lib/rnn_parser/rnn_rule.o 


# Each subdirectory must supply rules for building sources it contributes
src/lib/rnn_parser/%.o: ../src/lib/rnn_parser/%.cc
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


