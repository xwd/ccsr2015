################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CC_SRCS += \
../src/lib/io/format.cc \
../src/lib/io/reader.cc \
../src/lib/io/reader_factory.cc \
../src/lib/io/reader_group.cc \
../src/lib/io/reader_horiz.cc \
../src/lib/io/reader_multi_horiz.cc \
../src/lib/io/stream.cc \
../src/lib/io/writer.cc \
../src/lib/io/writer_factory.cc \
../src/lib/io/writer_format.cc \
../src/lib/io/writer_group.cc \
../src/lib/io/writer_horiz.cc \
../src/lib/io/writer_multi_horiz.cc \
../src/lib/io/writer_multi_vert.cc \
../src/lib/io/writer_stream.cc \
../src/lib/io/writer_vert.cc 

O_SRCS += \
../src/lib/io/format.o \
../src/lib/io/reader.o \
../src/lib/io/reader_factory.o \
../src/lib/io/reader_group.o \
../src/lib/io/reader_horiz.o \
../src/lib/io/reader_multi_horiz.o \
../src/lib/io/stream.o \
../src/lib/io/writer.o \
../src/lib/io/writer_factory.o \
../src/lib/io/writer_format.o \
../src/lib/io/writer_group.o \
../src/lib/io/writer_horiz.o \
../src/lib/io/writer_multi_horiz.o \
../src/lib/io/writer_multi_vert.o \
../src/lib/io/writer_stream.o \
../src/lib/io/writer_vert.o 

CC_DEPS += \
./src/lib/io/format.d \
./src/lib/io/reader.d \
./src/lib/io/reader_factory.d \
./src/lib/io/reader_group.d \
./src/lib/io/reader_horiz.d \
./src/lib/io/reader_multi_horiz.d \
./src/lib/io/stream.d \
./src/lib/io/writer.d \
./src/lib/io/writer_factory.d \
./src/lib/io/writer_format.d \
./src/lib/io/writer_group.d \
./src/lib/io/writer_horiz.d \
./src/lib/io/writer_multi_horiz.d \
./src/lib/io/writer_multi_vert.d \
./src/lib/io/writer_stream.d \
./src/lib/io/writer_vert.d 

OBJS += \
./src/lib/io/format.o \
./src/lib/io/reader.o \
./src/lib/io/reader_factory.o \
./src/lib/io/reader_group.o \
./src/lib/io/reader_horiz.o \
./src/lib/io/reader_multi_horiz.o \
./src/lib/io/stream.o \
./src/lib/io/writer.o \
./src/lib/io/writer_factory.o \
./src/lib/io/writer_format.o \
./src/lib/io/writer_group.o \
./src/lib/io/writer_horiz.o \
./src/lib/io/writer_multi_horiz.o \
./src/lib/io/writer_multi_vert.o \
./src/lib/io/writer_stream.o \
./src/lib/io/writer_vert.o 


# Each subdirectory must supply rules for building sources it contributes
src/lib/io/%.o: ../src/lib/io/%.cc
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


