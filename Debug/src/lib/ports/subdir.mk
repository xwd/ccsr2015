################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CC_SRCS += \
../src/lib/ports/bsd.cc \
../src/lib/ports/colour.cc \
../src/lib/ports/mingw.cc \
../src/lib/ports/mono.cc \
../src/lib/ports/sunos.cc \
../src/lib/ports/unix.cc \
../src/lib/ports/unix_common.cc 

O_SRCS += \
../src/lib/ports/colour.o \
../src/lib/ports/unix.o \
../src/lib/ports/unix_common.o 

CC_DEPS += \
./src/lib/ports/bsd.d \
./src/lib/ports/colour.d \
./src/lib/ports/mingw.d \
./src/lib/ports/mono.d \
./src/lib/ports/sunos.d \
./src/lib/ports/unix.d \
./src/lib/ports/unix_common.d 

OBJS += \
./src/lib/ports/bsd.o \
./src/lib/ports/colour.o \
./src/lib/ports/mingw.o \
./src/lib/ports/mono.o \
./src/lib/ports/sunos.o \
./src/lib/ports/unix.o \
./src/lib/ports/unix_common.o 


# Each subdirectory must supply rules for building sources it contributes
src/lib/ports/%.o: ../src/lib/ports/%.cc
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


