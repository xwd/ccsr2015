################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CC_SRCS += \
../src/lib/rnn_tagger/rnn_pos.cc \
../src/lib/rnn_tagger/rnn_tagger.cc \
../src/lib/rnn_tagger/xf1_trainner.cc 

O_SRCS += \
../src/lib/rnn_tagger/rnn_pos.o \
../src/lib/rnn_tagger/rnn_tagger.o 

CC_DEPS += \
./src/lib/rnn_tagger/rnn_pos.d \
./src/lib/rnn_tagger/rnn_tagger.d \
./src/lib/rnn_tagger/xf1_trainner.d 

OBJS += \
./src/lib/rnn_tagger/rnn_pos.o \
./src/lib/rnn_tagger/rnn_tagger.o \
./src/lib/rnn_tagger/xf1_trainner.o 


# Each subdirectory must supply rules for building sources it contributes
src/lib/rnn_tagger/%.o: ../src/lib/rnn_tagger/%.cc
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


