################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CC_SRCS += \
../src/lib/extract/_baseimpl.cc \
../src/lib/extract/attributes.cc \
../src/lib/extract/chunk.cc \
../src/lib/extract/classifier.cc \
../src/lib/extract/contexts.cc \
../src/lib/extract/ner.cc \
../src/lib/extract/pos.cc \
../src/lib/extract/super.cc \
../src/lib/extract/tagger.cc 

O_SRCS += \
../src/lib/extract/_baseimpl.o \
../src/lib/extract/attributes.o \
../src/lib/extract/contexts.o 

CC_DEPS += \
./src/lib/extract/_baseimpl.d \
./src/lib/extract/attributes.d \
./src/lib/extract/chunk.d \
./src/lib/extract/classifier.d \
./src/lib/extract/contexts.d \
./src/lib/extract/ner.d \
./src/lib/extract/pos.d \
./src/lib/extract/super.d \
./src/lib/extract/tagger.d 

OBJS += \
./src/lib/extract/_baseimpl.o \
./src/lib/extract/attributes.o \
./src/lib/extract/chunk.o \
./src/lib/extract/classifier.o \
./src/lib/extract/contexts.o \
./src/lib/extract/ner.o \
./src/lib/extract/pos.o \
./src/lib/extract/super.o \
./src/lib/extract/tagger.o 


# Each subdirectory must supply rules for building sources it contributes
src/lib/extract/%.o: ../src/lib/extract/%.cc
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


