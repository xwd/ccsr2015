################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CC_SRCS += \
../cnn/cnn/aligned-mem-pool.cc \
../cnn/cnn/cfsm-builder.cc \
../cnn/cnn/cnn.cc \
../cnn/cnn/conv.cc \
../cnn/cnn/cuda.cc \
../cnn/cnn/deep-lstm.cc \
../cnn/cnn/devices.cc \
../cnn/cnn/dict.cc \
../cnn/cnn/dim.cc \
../cnn/cnn/exec.cc \
../cnn/cnn/expr.cc \
../cnn/cnn/fast-lstm.cc \
../cnn/cnn/grad-check.cc \
../cnn/cnn/graph.cc \
../cnn/cnn/gru.cc \
../cnn/cnn/hsm-builder.cc \
../cnn/cnn/init.cc \
../cnn/cnn/lstm.cc \
../cnn/cnn/mem.cc \
../cnn/cnn/model.cc \
../cnn/cnn/mp.cc \
../cnn/cnn/nodes-common.cc \
../cnn/cnn/nodes.cc \
../cnn/cnn/param-nodes.cc \
../cnn/cnn/rnn-state-machine.cc \
../cnn/cnn/rnn.cc \
../cnn/cnn/saxe-init.cc \
../cnn/cnn/shadow-params.cc \
../cnn/cnn/tensor.cc \
../cnn/cnn/training.cc 

CC_DEPS += \
./cnn/cnn/aligned-mem-pool.d \
./cnn/cnn/cfsm-builder.d \
./cnn/cnn/cnn.d \
./cnn/cnn/conv.d \
./cnn/cnn/cuda.d \
./cnn/cnn/deep-lstm.d \
./cnn/cnn/devices.d \
./cnn/cnn/dict.d \
./cnn/cnn/dim.d \
./cnn/cnn/exec.d \
./cnn/cnn/expr.d \
./cnn/cnn/fast-lstm.d \
./cnn/cnn/grad-check.d \
./cnn/cnn/graph.d \
./cnn/cnn/gru.d \
./cnn/cnn/hsm-builder.d \
./cnn/cnn/init.d \
./cnn/cnn/lstm.d \
./cnn/cnn/mem.d \
./cnn/cnn/model.d \
./cnn/cnn/mp.d \
./cnn/cnn/nodes-common.d \
./cnn/cnn/nodes.d \
./cnn/cnn/param-nodes.d \
./cnn/cnn/rnn-state-machine.d \
./cnn/cnn/rnn.d \
./cnn/cnn/saxe-init.d \
./cnn/cnn/shadow-params.d \
./cnn/cnn/tensor.d \
./cnn/cnn/training.d 

OBJS += \
./cnn/cnn/aligned-mem-pool.o \
./cnn/cnn/cfsm-builder.o \
./cnn/cnn/cnn.o \
./cnn/cnn/conv.o \
./cnn/cnn/cuda.o \
./cnn/cnn/deep-lstm.o \
./cnn/cnn/devices.o \
./cnn/cnn/dict.o \
./cnn/cnn/dim.o \
./cnn/cnn/exec.o \
./cnn/cnn/expr.o \
./cnn/cnn/fast-lstm.o \
./cnn/cnn/grad-check.o \
./cnn/cnn/graph.o \
./cnn/cnn/gru.o \
./cnn/cnn/hsm-builder.o \
./cnn/cnn/init.o \
./cnn/cnn/lstm.o \
./cnn/cnn/mem.o \
./cnn/cnn/model.o \
./cnn/cnn/mp.o \
./cnn/cnn/nodes-common.o \
./cnn/cnn/nodes.o \
./cnn/cnn/param-nodes.o \
./cnn/cnn/rnn-state-machine.o \
./cnn/cnn/rnn.o \
./cnn/cnn/saxe-init.o \
./cnn/cnn/shadow-params.o \
./cnn/cnn/tensor.o \
./cnn/cnn/training.o 


# Each subdirectory must supply rules for building sources it contributes
cnn/cnn/%.o: ../cnn/cnn/%.cc
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


