################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../cnn/eigen/unsupported/doc/examples/BVH_Example.cpp \
../cnn/eigen/unsupported/doc/examples/FFT.cpp \
../cnn/eigen/unsupported/doc/examples/MatrixExponential.cpp \
../cnn/eigen/unsupported/doc/examples/MatrixFunction.cpp \
../cnn/eigen/unsupported/doc/examples/MatrixLogarithm.cpp \
../cnn/eigen/unsupported/doc/examples/MatrixPower.cpp \
../cnn/eigen/unsupported/doc/examples/MatrixPower_optimal.cpp \
../cnn/eigen/unsupported/doc/examples/MatrixSine.cpp \
../cnn/eigen/unsupported/doc/examples/MatrixSinh.cpp \
../cnn/eigen/unsupported/doc/examples/MatrixSquareRoot.cpp \
../cnn/eigen/unsupported/doc/examples/PolynomialSolver1.cpp \
../cnn/eigen/unsupported/doc/examples/PolynomialUtils1.cpp 

OBJS += \
./cnn/eigen/unsupported/doc/examples/BVH_Example.o \
./cnn/eigen/unsupported/doc/examples/FFT.o \
./cnn/eigen/unsupported/doc/examples/MatrixExponential.o \
./cnn/eigen/unsupported/doc/examples/MatrixFunction.o \
./cnn/eigen/unsupported/doc/examples/MatrixLogarithm.o \
./cnn/eigen/unsupported/doc/examples/MatrixPower.o \
./cnn/eigen/unsupported/doc/examples/MatrixPower_optimal.o \
./cnn/eigen/unsupported/doc/examples/MatrixSine.o \
./cnn/eigen/unsupported/doc/examples/MatrixSinh.o \
./cnn/eigen/unsupported/doc/examples/MatrixSquareRoot.o \
./cnn/eigen/unsupported/doc/examples/PolynomialSolver1.o \
./cnn/eigen/unsupported/doc/examples/PolynomialUtils1.o 

CPP_DEPS += \
./cnn/eigen/unsupported/doc/examples/BVH_Example.d \
./cnn/eigen/unsupported/doc/examples/FFT.d \
./cnn/eigen/unsupported/doc/examples/MatrixExponential.d \
./cnn/eigen/unsupported/doc/examples/MatrixFunction.d \
./cnn/eigen/unsupported/doc/examples/MatrixLogarithm.d \
./cnn/eigen/unsupported/doc/examples/MatrixPower.d \
./cnn/eigen/unsupported/doc/examples/MatrixPower_optimal.d \
./cnn/eigen/unsupported/doc/examples/MatrixSine.d \
./cnn/eigen/unsupported/doc/examples/MatrixSinh.d \
./cnn/eigen/unsupported/doc/examples/MatrixSquareRoot.d \
./cnn/eigen/unsupported/doc/examples/PolynomialSolver1.d \
./cnn/eigen/unsupported/doc/examples/PolynomialUtils1.d 


# Each subdirectory must supply rules for building sources it contributes
cnn/eigen/unsupported/doc/examples/%.o: ../cnn/eigen/unsupported/doc/examples/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


