################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../cnn/eigen/bench/spbench/sp_solver.cpp \
../cnn/eigen/bench/spbench/spbenchsolver.cpp \
../cnn/eigen/bench/spbench/test_sparseLU.cpp 

OBJS += \
./cnn/eigen/bench/spbench/sp_solver.o \
./cnn/eigen/bench/spbench/spbenchsolver.o \
./cnn/eigen/bench/spbench/test_sparseLU.o 

CPP_DEPS += \
./cnn/eigen/bench/spbench/sp_solver.d \
./cnn/eigen/bench/spbench/spbenchsolver.d \
./cnn/eigen/bench/spbench/test_sparseLU.d 


# Each subdirectory must supply rules for building sources it contributes
cnn/eigen/bench/spbench/%.o: ../cnn/eigen/bench/spbench/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


