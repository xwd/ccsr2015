################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../cnn/eigen/bench/analyze-blocking-sizes.cpp \
../cnn/eigen/bench/basicbenchmark.cpp \
../cnn/eigen/bench/benchBlasGemm.cpp \
../cnn/eigen/bench/benchCholesky.cpp \
../cnn/eigen/bench/benchEigenSolver.cpp \
../cnn/eigen/bench/benchFFT.cpp \
../cnn/eigen/bench/benchGeometry.cpp \
../cnn/eigen/bench/benchVecAdd.cpp \
../cnn/eigen/bench/bench_gemm.cpp \
../cnn/eigen/bench/bench_norm.cpp \
../cnn/eigen/bench/bench_reverse.cpp \
../cnn/eigen/bench/bench_sum.cpp \
../cnn/eigen/bench/benchmark-blocking-sizes.cpp \
../cnn/eigen/bench/benchmark.cpp \
../cnn/eigen/bench/benchmarkSlice.cpp \
../cnn/eigen/bench/benchmarkX.cpp \
../cnn/eigen/bench/benchmarkXcwise.cpp \
../cnn/eigen/bench/check_cache_queries.cpp \
../cnn/eigen/bench/dense_solvers.cpp \
../cnn/eigen/bench/eig33.cpp \
../cnn/eigen/bench/geometry.cpp \
../cnn/eigen/bench/product_threshold.cpp \
../cnn/eigen/bench/quat_slerp.cpp \
../cnn/eigen/bench/quatmul.cpp \
../cnn/eigen/bench/sparse_cholesky.cpp \
../cnn/eigen/bench/sparse_dense_product.cpp \
../cnn/eigen/bench/sparse_lu.cpp \
../cnn/eigen/bench/sparse_product.cpp \
../cnn/eigen/bench/sparse_randomsetter.cpp \
../cnn/eigen/bench/sparse_setter.cpp \
../cnn/eigen/bench/sparse_transpose.cpp \
../cnn/eigen/bench/sparse_trisolver.cpp \
../cnn/eigen/bench/spmv.cpp \
../cnn/eigen/bench/vdw_new.cpp 

OBJS += \
./cnn/eigen/bench/analyze-blocking-sizes.o \
./cnn/eigen/bench/basicbenchmark.o \
./cnn/eigen/bench/benchBlasGemm.o \
./cnn/eigen/bench/benchCholesky.o \
./cnn/eigen/bench/benchEigenSolver.o \
./cnn/eigen/bench/benchFFT.o \
./cnn/eigen/bench/benchGeometry.o \
./cnn/eigen/bench/benchVecAdd.o \
./cnn/eigen/bench/bench_gemm.o \
./cnn/eigen/bench/bench_norm.o \
./cnn/eigen/bench/bench_reverse.o \
./cnn/eigen/bench/bench_sum.o \
./cnn/eigen/bench/benchmark-blocking-sizes.o \
./cnn/eigen/bench/benchmark.o \
./cnn/eigen/bench/benchmarkSlice.o \
./cnn/eigen/bench/benchmarkX.o \
./cnn/eigen/bench/benchmarkXcwise.o \
./cnn/eigen/bench/check_cache_queries.o \
./cnn/eigen/bench/dense_solvers.o \
./cnn/eigen/bench/eig33.o \
./cnn/eigen/bench/geometry.o \
./cnn/eigen/bench/product_threshold.o \
./cnn/eigen/bench/quat_slerp.o \
./cnn/eigen/bench/quatmul.o \
./cnn/eigen/bench/sparse_cholesky.o \
./cnn/eigen/bench/sparse_dense_product.o \
./cnn/eigen/bench/sparse_lu.o \
./cnn/eigen/bench/sparse_product.o \
./cnn/eigen/bench/sparse_randomsetter.o \
./cnn/eigen/bench/sparse_setter.o \
./cnn/eigen/bench/sparse_transpose.o \
./cnn/eigen/bench/sparse_trisolver.o \
./cnn/eigen/bench/spmv.o \
./cnn/eigen/bench/vdw_new.o 

CPP_DEPS += \
./cnn/eigen/bench/analyze-blocking-sizes.d \
./cnn/eigen/bench/basicbenchmark.d \
./cnn/eigen/bench/benchBlasGemm.d \
./cnn/eigen/bench/benchCholesky.d \
./cnn/eigen/bench/benchEigenSolver.d \
./cnn/eigen/bench/benchFFT.d \
./cnn/eigen/bench/benchGeometry.d \
./cnn/eigen/bench/benchVecAdd.d \
./cnn/eigen/bench/bench_gemm.d \
./cnn/eigen/bench/bench_norm.d \
./cnn/eigen/bench/bench_reverse.d \
./cnn/eigen/bench/bench_sum.d \
./cnn/eigen/bench/benchmark-blocking-sizes.d \
./cnn/eigen/bench/benchmark.d \
./cnn/eigen/bench/benchmarkSlice.d \
./cnn/eigen/bench/benchmarkX.d \
./cnn/eigen/bench/benchmarkXcwise.d \
./cnn/eigen/bench/check_cache_queries.d \
./cnn/eigen/bench/dense_solvers.d \
./cnn/eigen/bench/eig33.d \
./cnn/eigen/bench/geometry.d \
./cnn/eigen/bench/product_threshold.d \
./cnn/eigen/bench/quat_slerp.d \
./cnn/eigen/bench/quatmul.d \
./cnn/eigen/bench/sparse_cholesky.d \
./cnn/eigen/bench/sparse_dense_product.d \
./cnn/eigen/bench/sparse_lu.d \
./cnn/eigen/bench/sparse_product.d \
./cnn/eigen/bench/sparse_randomsetter.d \
./cnn/eigen/bench/sparse_setter.d \
./cnn/eigen/bench/sparse_transpose.d \
./cnn/eigen/bench/sparse_trisolver.d \
./cnn/eigen/bench/spmv.d \
./cnn/eigen/bench/vdw_new.d 


# Each subdirectory must supply rules for building sources it contributes
cnn/eigen/bench/%.o: ../cnn/eigen/bench/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


