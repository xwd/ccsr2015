################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../cnn/eigen/bench/btl/libs/eigen2/btl_tiny_eigen2.cpp \
../cnn/eigen/bench/btl/libs/eigen2/main_adv.cpp \
../cnn/eigen/bench/btl/libs/eigen2/main_linear.cpp \
../cnn/eigen/bench/btl/libs/eigen2/main_matmat.cpp \
../cnn/eigen/bench/btl/libs/eigen2/main_vecmat.cpp 

OBJS += \
./cnn/eigen/bench/btl/libs/eigen2/btl_tiny_eigen2.o \
./cnn/eigen/bench/btl/libs/eigen2/main_adv.o \
./cnn/eigen/bench/btl/libs/eigen2/main_linear.o \
./cnn/eigen/bench/btl/libs/eigen2/main_matmat.o \
./cnn/eigen/bench/btl/libs/eigen2/main_vecmat.o 

CPP_DEPS += \
./cnn/eigen/bench/btl/libs/eigen2/btl_tiny_eigen2.d \
./cnn/eigen/bench/btl/libs/eigen2/main_adv.d \
./cnn/eigen/bench/btl/libs/eigen2/main_linear.d \
./cnn/eigen/bench/btl/libs/eigen2/main_matmat.d \
./cnn/eigen/bench/btl/libs/eigen2/main_vecmat.d 


# Each subdirectory must supply rules for building sources it contributes
cnn/eigen/bench/btl/libs/eigen2/%.o: ../cnn/eigen/bench/btl/libs/eigen2/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


