################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CXX_SRCS += \
../cnn/eigen/bench/btl/data/mean.cxx \
../cnn/eigen/bench/btl/data/regularize.cxx \
../cnn/eigen/bench/btl/data/smooth.cxx 

CXX_DEPS += \
./cnn/eigen/bench/btl/data/mean.d \
./cnn/eigen/bench/btl/data/regularize.d \
./cnn/eigen/bench/btl/data/smooth.d 

OBJS += \
./cnn/eigen/bench/btl/data/mean.o \
./cnn/eigen/bench/btl/data/regularize.o \
./cnn/eigen/bench/btl/data/smooth.o 


# Each subdirectory must supply rules for building sources it contributes
cnn/eigen/bench/btl/data/%.o: ../cnn/eigen/bench/btl/data/%.cxx
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


