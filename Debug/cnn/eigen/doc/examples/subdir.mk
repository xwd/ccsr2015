################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../cnn/eigen/doc/examples/CustomizingEigen_Inheritance.cpp \
../cnn/eigen/doc/examples/DenseBase_middleCols_int.cpp \
../cnn/eigen/doc/examples/DenseBase_middleRows_int.cpp \
../cnn/eigen/doc/examples/DenseBase_template_int_middleCols.cpp \
../cnn/eigen/doc/examples/DenseBase_template_int_middleRows.cpp \
../cnn/eigen/doc/examples/QuickStart_example.cpp \
../cnn/eigen/doc/examples/QuickStart_example2_dynamic.cpp \
../cnn/eigen/doc/examples/QuickStart_example2_fixed.cpp \
../cnn/eigen/doc/examples/TemplateKeyword_flexible.cpp \
../cnn/eigen/doc/examples/TemplateKeyword_simple.cpp \
../cnn/eigen/doc/examples/TutorialLinAlgComputeTwice.cpp \
../cnn/eigen/doc/examples/TutorialLinAlgExComputeSolveError.cpp \
../cnn/eigen/doc/examples/TutorialLinAlgExSolveColPivHouseholderQR.cpp \
../cnn/eigen/doc/examples/TutorialLinAlgExSolveLDLT.cpp \
../cnn/eigen/doc/examples/TutorialLinAlgInverseDeterminant.cpp \
../cnn/eigen/doc/examples/TutorialLinAlgRankRevealing.cpp \
../cnn/eigen/doc/examples/TutorialLinAlgSVDSolve.cpp \
../cnn/eigen/doc/examples/TutorialLinAlgSelfAdjointEigenSolver.cpp \
../cnn/eigen/doc/examples/TutorialLinAlgSetThreshold.cpp \
../cnn/eigen/doc/examples/Tutorial_ArrayClass_accessors.cpp \
../cnn/eigen/doc/examples/Tutorial_ArrayClass_addition.cpp \
../cnn/eigen/doc/examples/Tutorial_ArrayClass_cwise_other.cpp \
../cnn/eigen/doc/examples/Tutorial_ArrayClass_interop.cpp \
../cnn/eigen/doc/examples/Tutorial_ArrayClass_interop_matrix.cpp \
../cnn/eigen/doc/examples/Tutorial_ArrayClass_mult.cpp \
../cnn/eigen/doc/examples/Tutorial_BlockOperations_block_assignment.cpp \
../cnn/eigen/doc/examples/Tutorial_BlockOperations_colrow.cpp \
../cnn/eigen/doc/examples/Tutorial_BlockOperations_corner.cpp \
../cnn/eigen/doc/examples/Tutorial_BlockOperations_print_block.cpp \
../cnn/eigen/doc/examples/Tutorial_BlockOperations_vector.cpp \
../cnn/eigen/doc/examples/Tutorial_PartialLU_solve.cpp \
../cnn/eigen/doc/examples/Tutorial_ReductionsVisitorsBroadcasting_broadcast_1nn.cpp \
../cnn/eigen/doc/examples/Tutorial_ReductionsVisitorsBroadcasting_broadcast_simple.cpp \
../cnn/eigen/doc/examples/Tutorial_ReductionsVisitorsBroadcasting_broadcast_simple_rowwise.cpp \
../cnn/eigen/doc/examples/Tutorial_ReductionsVisitorsBroadcasting_colwise.cpp \
../cnn/eigen/doc/examples/Tutorial_ReductionsVisitorsBroadcasting_maxnorm.cpp \
../cnn/eigen/doc/examples/Tutorial_ReductionsVisitorsBroadcasting_reductions_bool.cpp \
../cnn/eigen/doc/examples/Tutorial_ReductionsVisitorsBroadcasting_reductions_norm.cpp \
../cnn/eigen/doc/examples/Tutorial_ReductionsVisitorsBroadcasting_reductions_operatornorm.cpp \
../cnn/eigen/doc/examples/Tutorial_ReductionsVisitorsBroadcasting_rowwise.cpp \
../cnn/eigen/doc/examples/Tutorial_ReductionsVisitorsBroadcasting_visitors.cpp \
../cnn/eigen/doc/examples/Tutorial_simple_example_dynamic_size.cpp \
../cnn/eigen/doc/examples/Tutorial_simple_example_fixed_size.cpp \
../cnn/eigen/doc/examples/class_Block.cpp \
../cnn/eigen/doc/examples/class_CwiseBinaryOp.cpp \
../cnn/eigen/doc/examples/class_CwiseUnaryOp.cpp \
../cnn/eigen/doc/examples/class_CwiseUnaryOp_ptrfun.cpp \
../cnn/eigen/doc/examples/class_FixedBlock.cpp \
../cnn/eigen/doc/examples/class_FixedVectorBlock.cpp \
../cnn/eigen/doc/examples/class_VectorBlock.cpp \
../cnn/eigen/doc/examples/function_taking_eigenbase.cpp \
../cnn/eigen/doc/examples/function_taking_ref.cpp \
../cnn/eigen/doc/examples/make_circulant.cpp \
../cnn/eigen/doc/examples/matrixfree_cg.cpp \
../cnn/eigen/doc/examples/tut_arithmetic_add_sub.cpp \
../cnn/eigen/doc/examples/tut_arithmetic_dot_cross.cpp \
../cnn/eigen/doc/examples/tut_arithmetic_matrix_mul.cpp \
../cnn/eigen/doc/examples/tut_arithmetic_redux_basic.cpp \
../cnn/eigen/doc/examples/tut_arithmetic_scalar_mul_div.cpp \
../cnn/eigen/doc/examples/tut_matrix_coefficient_accessors.cpp \
../cnn/eigen/doc/examples/tut_matrix_resize.cpp \
../cnn/eigen/doc/examples/tut_matrix_resize_fixed_size.cpp 

OBJS += \
./cnn/eigen/doc/examples/CustomizingEigen_Inheritance.o \
./cnn/eigen/doc/examples/DenseBase_middleCols_int.o \
./cnn/eigen/doc/examples/DenseBase_middleRows_int.o \
./cnn/eigen/doc/examples/DenseBase_template_int_middleCols.o \
./cnn/eigen/doc/examples/DenseBase_template_int_middleRows.o \
./cnn/eigen/doc/examples/QuickStart_example.o \
./cnn/eigen/doc/examples/QuickStart_example2_dynamic.o \
./cnn/eigen/doc/examples/QuickStart_example2_fixed.o \
./cnn/eigen/doc/examples/TemplateKeyword_flexible.o \
./cnn/eigen/doc/examples/TemplateKeyword_simple.o \
./cnn/eigen/doc/examples/TutorialLinAlgComputeTwice.o \
./cnn/eigen/doc/examples/TutorialLinAlgExComputeSolveError.o \
./cnn/eigen/doc/examples/TutorialLinAlgExSolveColPivHouseholderQR.o \
./cnn/eigen/doc/examples/TutorialLinAlgExSolveLDLT.o \
./cnn/eigen/doc/examples/TutorialLinAlgInverseDeterminant.o \
./cnn/eigen/doc/examples/TutorialLinAlgRankRevealing.o \
./cnn/eigen/doc/examples/TutorialLinAlgSVDSolve.o \
./cnn/eigen/doc/examples/TutorialLinAlgSelfAdjointEigenSolver.o \
./cnn/eigen/doc/examples/TutorialLinAlgSetThreshold.o \
./cnn/eigen/doc/examples/Tutorial_ArrayClass_accessors.o \
./cnn/eigen/doc/examples/Tutorial_ArrayClass_addition.o \
./cnn/eigen/doc/examples/Tutorial_ArrayClass_cwise_other.o \
./cnn/eigen/doc/examples/Tutorial_ArrayClass_interop.o \
./cnn/eigen/doc/examples/Tutorial_ArrayClass_interop_matrix.o \
./cnn/eigen/doc/examples/Tutorial_ArrayClass_mult.o \
./cnn/eigen/doc/examples/Tutorial_BlockOperations_block_assignment.o \
./cnn/eigen/doc/examples/Tutorial_BlockOperations_colrow.o \
./cnn/eigen/doc/examples/Tutorial_BlockOperations_corner.o \
./cnn/eigen/doc/examples/Tutorial_BlockOperations_print_block.o \
./cnn/eigen/doc/examples/Tutorial_BlockOperations_vector.o \
./cnn/eigen/doc/examples/Tutorial_PartialLU_solve.o \
./cnn/eigen/doc/examples/Tutorial_ReductionsVisitorsBroadcasting_broadcast_1nn.o \
./cnn/eigen/doc/examples/Tutorial_ReductionsVisitorsBroadcasting_broadcast_simple.o \
./cnn/eigen/doc/examples/Tutorial_ReductionsVisitorsBroadcasting_broadcast_simple_rowwise.o \
./cnn/eigen/doc/examples/Tutorial_ReductionsVisitorsBroadcasting_colwise.o \
./cnn/eigen/doc/examples/Tutorial_ReductionsVisitorsBroadcasting_maxnorm.o \
./cnn/eigen/doc/examples/Tutorial_ReductionsVisitorsBroadcasting_reductions_bool.o \
./cnn/eigen/doc/examples/Tutorial_ReductionsVisitorsBroadcasting_reductions_norm.o \
./cnn/eigen/doc/examples/Tutorial_ReductionsVisitorsBroadcasting_reductions_operatornorm.o \
./cnn/eigen/doc/examples/Tutorial_ReductionsVisitorsBroadcasting_rowwise.o \
./cnn/eigen/doc/examples/Tutorial_ReductionsVisitorsBroadcasting_visitors.o \
./cnn/eigen/doc/examples/Tutorial_simple_example_dynamic_size.o \
./cnn/eigen/doc/examples/Tutorial_simple_example_fixed_size.o \
./cnn/eigen/doc/examples/class_Block.o \
./cnn/eigen/doc/examples/class_CwiseBinaryOp.o \
./cnn/eigen/doc/examples/class_CwiseUnaryOp.o \
./cnn/eigen/doc/examples/class_CwiseUnaryOp_ptrfun.o \
./cnn/eigen/doc/examples/class_FixedBlock.o \
./cnn/eigen/doc/examples/class_FixedVectorBlock.o \
./cnn/eigen/doc/examples/class_VectorBlock.o \
./cnn/eigen/doc/examples/function_taking_eigenbase.o \
./cnn/eigen/doc/examples/function_taking_ref.o \
./cnn/eigen/doc/examples/make_circulant.o \
./cnn/eigen/doc/examples/matrixfree_cg.o \
./cnn/eigen/doc/examples/tut_arithmetic_add_sub.o \
./cnn/eigen/doc/examples/tut_arithmetic_dot_cross.o \
./cnn/eigen/doc/examples/tut_arithmetic_matrix_mul.o \
./cnn/eigen/doc/examples/tut_arithmetic_redux_basic.o \
./cnn/eigen/doc/examples/tut_arithmetic_scalar_mul_div.o \
./cnn/eigen/doc/examples/tut_matrix_coefficient_accessors.o \
./cnn/eigen/doc/examples/tut_matrix_resize.o \
./cnn/eigen/doc/examples/tut_matrix_resize_fixed_size.o 

CPP_DEPS += \
./cnn/eigen/doc/examples/CustomizingEigen_Inheritance.d \
./cnn/eigen/doc/examples/DenseBase_middleCols_int.d \
./cnn/eigen/doc/examples/DenseBase_middleRows_int.d \
./cnn/eigen/doc/examples/DenseBase_template_int_middleCols.d \
./cnn/eigen/doc/examples/DenseBase_template_int_middleRows.d \
./cnn/eigen/doc/examples/QuickStart_example.d \
./cnn/eigen/doc/examples/QuickStart_example2_dynamic.d \
./cnn/eigen/doc/examples/QuickStart_example2_fixed.d \
./cnn/eigen/doc/examples/TemplateKeyword_flexible.d \
./cnn/eigen/doc/examples/TemplateKeyword_simple.d \
./cnn/eigen/doc/examples/TutorialLinAlgComputeTwice.d \
./cnn/eigen/doc/examples/TutorialLinAlgExComputeSolveError.d \
./cnn/eigen/doc/examples/TutorialLinAlgExSolveColPivHouseholderQR.d \
./cnn/eigen/doc/examples/TutorialLinAlgExSolveLDLT.d \
./cnn/eigen/doc/examples/TutorialLinAlgInverseDeterminant.d \
./cnn/eigen/doc/examples/TutorialLinAlgRankRevealing.d \
./cnn/eigen/doc/examples/TutorialLinAlgSVDSolve.d \
./cnn/eigen/doc/examples/TutorialLinAlgSelfAdjointEigenSolver.d \
./cnn/eigen/doc/examples/TutorialLinAlgSetThreshold.d \
./cnn/eigen/doc/examples/Tutorial_ArrayClass_accessors.d \
./cnn/eigen/doc/examples/Tutorial_ArrayClass_addition.d \
./cnn/eigen/doc/examples/Tutorial_ArrayClass_cwise_other.d \
./cnn/eigen/doc/examples/Tutorial_ArrayClass_interop.d \
./cnn/eigen/doc/examples/Tutorial_ArrayClass_interop_matrix.d \
./cnn/eigen/doc/examples/Tutorial_ArrayClass_mult.d \
./cnn/eigen/doc/examples/Tutorial_BlockOperations_block_assignment.d \
./cnn/eigen/doc/examples/Tutorial_BlockOperations_colrow.d \
./cnn/eigen/doc/examples/Tutorial_BlockOperations_corner.d \
./cnn/eigen/doc/examples/Tutorial_BlockOperations_print_block.d \
./cnn/eigen/doc/examples/Tutorial_BlockOperations_vector.d \
./cnn/eigen/doc/examples/Tutorial_PartialLU_solve.d \
./cnn/eigen/doc/examples/Tutorial_ReductionsVisitorsBroadcasting_broadcast_1nn.d \
./cnn/eigen/doc/examples/Tutorial_ReductionsVisitorsBroadcasting_broadcast_simple.d \
./cnn/eigen/doc/examples/Tutorial_ReductionsVisitorsBroadcasting_broadcast_simple_rowwise.d \
./cnn/eigen/doc/examples/Tutorial_ReductionsVisitorsBroadcasting_colwise.d \
./cnn/eigen/doc/examples/Tutorial_ReductionsVisitorsBroadcasting_maxnorm.d \
./cnn/eigen/doc/examples/Tutorial_ReductionsVisitorsBroadcasting_reductions_bool.d \
./cnn/eigen/doc/examples/Tutorial_ReductionsVisitorsBroadcasting_reductions_norm.d \
./cnn/eigen/doc/examples/Tutorial_ReductionsVisitorsBroadcasting_reductions_operatornorm.d \
./cnn/eigen/doc/examples/Tutorial_ReductionsVisitorsBroadcasting_rowwise.d \
./cnn/eigen/doc/examples/Tutorial_ReductionsVisitorsBroadcasting_visitors.d \
./cnn/eigen/doc/examples/Tutorial_simple_example_dynamic_size.d \
./cnn/eigen/doc/examples/Tutorial_simple_example_fixed_size.d \
./cnn/eigen/doc/examples/class_Block.d \
./cnn/eigen/doc/examples/class_CwiseBinaryOp.d \
./cnn/eigen/doc/examples/class_CwiseUnaryOp.d \
./cnn/eigen/doc/examples/class_CwiseUnaryOp_ptrfun.d \
./cnn/eigen/doc/examples/class_FixedBlock.d \
./cnn/eigen/doc/examples/class_FixedVectorBlock.d \
./cnn/eigen/doc/examples/class_VectorBlock.d \
./cnn/eigen/doc/examples/function_taking_eigenbase.d \
./cnn/eigen/doc/examples/function_taking_ref.d \
./cnn/eigen/doc/examples/make_circulant.d \
./cnn/eigen/doc/examples/matrixfree_cg.d \
./cnn/eigen/doc/examples/tut_arithmetic_add_sub.d \
./cnn/eigen/doc/examples/tut_arithmetic_dot_cross.d \
./cnn/eigen/doc/examples/tut_arithmetic_matrix_mul.d \
./cnn/eigen/doc/examples/tut_arithmetic_redux_basic.d \
./cnn/eigen/doc/examples/tut_arithmetic_scalar_mul_div.d \
./cnn/eigen/doc/examples/tut_matrix_coefficient_accessors.d \
./cnn/eigen/doc/examples/tut_matrix_resize.d \
./cnn/eigen/doc/examples/tut_matrix_resize_fixed_size.d 


# Each subdirectory must supply rules for building sources it contributes
cnn/eigen/doc/examples/%.o: ../cnn/eigen/doc/examples/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


