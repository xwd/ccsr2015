################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../cnn/eigen/blas/f2c/chbmv.c \
../cnn/eigen/blas/f2c/chpmv.c \
../cnn/eigen/blas/f2c/complexdots.c \
../cnn/eigen/blas/f2c/ctbmv.c \
../cnn/eigen/blas/f2c/d_cnjg.c \
../cnn/eigen/blas/f2c/drotm.c \
../cnn/eigen/blas/f2c/drotmg.c \
../cnn/eigen/blas/f2c/dsbmv.c \
../cnn/eigen/blas/f2c/dspmv.c \
../cnn/eigen/blas/f2c/dtbmv.c \
../cnn/eigen/blas/f2c/lsame.c \
../cnn/eigen/blas/f2c/r_cnjg.c \
../cnn/eigen/blas/f2c/srotm.c \
../cnn/eigen/blas/f2c/srotmg.c \
../cnn/eigen/blas/f2c/ssbmv.c \
../cnn/eigen/blas/f2c/sspmv.c \
../cnn/eigen/blas/f2c/stbmv.c \
../cnn/eigen/blas/f2c/zhbmv.c \
../cnn/eigen/blas/f2c/zhpmv.c \
../cnn/eigen/blas/f2c/ztbmv.c 

OBJS += \
./cnn/eigen/blas/f2c/chbmv.o \
./cnn/eigen/blas/f2c/chpmv.o \
./cnn/eigen/blas/f2c/complexdots.o \
./cnn/eigen/blas/f2c/ctbmv.o \
./cnn/eigen/blas/f2c/d_cnjg.o \
./cnn/eigen/blas/f2c/drotm.o \
./cnn/eigen/blas/f2c/drotmg.o \
./cnn/eigen/blas/f2c/dsbmv.o \
./cnn/eigen/blas/f2c/dspmv.o \
./cnn/eigen/blas/f2c/dtbmv.o \
./cnn/eigen/blas/f2c/lsame.o \
./cnn/eigen/blas/f2c/r_cnjg.o \
./cnn/eigen/blas/f2c/srotm.o \
./cnn/eigen/blas/f2c/srotmg.o \
./cnn/eigen/blas/f2c/ssbmv.o \
./cnn/eigen/blas/f2c/sspmv.o \
./cnn/eigen/blas/f2c/stbmv.o \
./cnn/eigen/blas/f2c/zhbmv.o \
./cnn/eigen/blas/f2c/zhpmv.o \
./cnn/eigen/blas/f2c/ztbmv.o 

C_DEPS += \
./cnn/eigen/blas/f2c/chbmv.d \
./cnn/eigen/blas/f2c/chpmv.d \
./cnn/eigen/blas/f2c/complexdots.d \
./cnn/eigen/blas/f2c/ctbmv.d \
./cnn/eigen/blas/f2c/d_cnjg.d \
./cnn/eigen/blas/f2c/drotm.d \
./cnn/eigen/blas/f2c/drotmg.d \
./cnn/eigen/blas/f2c/dsbmv.d \
./cnn/eigen/blas/f2c/dspmv.d \
./cnn/eigen/blas/f2c/dtbmv.d \
./cnn/eigen/blas/f2c/lsame.d \
./cnn/eigen/blas/f2c/r_cnjg.d \
./cnn/eigen/blas/f2c/srotm.d \
./cnn/eigen/blas/f2c/srotmg.d \
./cnn/eigen/blas/f2c/ssbmv.d \
./cnn/eigen/blas/f2c/sspmv.d \
./cnn/eigen/blas/f2c/stbmv.d \
./cnn/eigen/blas/f2c/zhbmv.d \
./cnn/eigen/blas/f2c/zhpmv.d \
./cnn/eigen/blas/f2c/ztbmv.d 


# Each subdirectory must supply rules for building sources it contributes
cnn/eigen/blas/f2c/%.o: ../cnn/eigen/blas/f2c/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


