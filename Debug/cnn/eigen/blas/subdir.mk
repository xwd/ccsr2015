################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../cnn/eigen/blas/complex_double.cpp \
../cnn/eigen/blas/complex_single.cpp \
../cnn/eigen/blas/double.cpp \
../cnn/eigen/blas/single.cpp \
../cnn/eigen/blas/xerbla.cpp 

OBJS += \
./cnn/eigen/blas/complex_double.o \
./cnn/eigen/blas/complex_single.o \
./cnn/eigen/blas/double.o \
./cnn/eigen/blas/single.o \
./cnn/eigen/blas/xerbla.o 

CPP_DEPS += \
./cnn/eigen/blas/complex_double.d \
./cnn/eigen/blas/complex_single.d \
./cnn/eigen/blas/double.d \
./cnn/eigen/blas/single.d \
./cnn/eigen/blas/xerbla.d 


# Each subdirectory must supply rules for building sources it contributes
cnn/eigen/blas/%.o: ../cnn/eigen/blas/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


