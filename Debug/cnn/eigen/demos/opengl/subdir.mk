################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../cnn/eigen/demos/opengl/camera.cpp \
../cnn/eigen/demos/opengl/gpuhelper.cpp \
../cnn/eigen/demos/opengl/icosphere.cpp \
../cnn/eigen/demos/opengl/quaternion_demo.cpp \
../cnn/eigen/demos/opengl/trackball.cpp 

OBJS += \
./cnn/eigen/demos/opengl/camera.o \
./cnn/eigen/demos/opengl/gpuhelper.o \
./cnn/eigen/demos/opengl/icosphere.o \
./cnn/eigen/demos/opengl/quaternion_demo.o \
./cnn/eigen/demos/opengl/trackball.o 

CPP_DEPS += \
./cnn/eigen/demos/opengl/camera.d \
./cnn/eigen/demos/opengl/gpuhelper.d \
./cnn/eigen/demos/opengl/icosphere.d \
./cnn/eigen/demos/opengl/quaternion_demo.d \
./cnn/eigen/demos/opengl/trackball.d 


# Each subdirectory must supply rules for building sources it contributes
cnn/eigen/demos/opengl/%.o: ../cnn/eigen/demos/opengl/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


