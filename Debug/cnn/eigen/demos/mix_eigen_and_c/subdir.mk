################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../cnn/eigen/demos/mix_eigen_and_c/binary_library.cpp 

C_SRCS += \
../cnn/eigen/demos/mix_eigen_and_c/example.c 

OBJS += \
./cnn/eigen/demos/mix_eigen_and_c/binary_library.o \
./cnn/eigen/demos/mix_eigen_and_c/example.o 

CPP_DEPS += \
./cnn/eigen/demos/mix_eigen_and_c/binary_library.d 

C_DEPS += \
./cnn/eigen/demos/mix_eigen_and_c/example.d 


# Each subdirectory must supply rules for building sources it contributes
cnn/eigen/demos/mix_eigen_and_c/%.o: ../cnn/eigen/demos/mix_eigen_and_c/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

cnn/eigen/demos/mix_eigen_and_c/%.o: ../cnn/eigen/demos/mix_eigen_and_c/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


