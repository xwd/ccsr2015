################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CC_SRCS += \
../cnn/examples/embed-cl.cc \
../cnn/examples/encdec.cc \
../cnn/examples/mlc.cc \
../cnn/examples/nlm.cc \
../cnn/examples/poisson-regression.cc \
../cnn/examples/read-write.cc \
../cnn/examples/rnnlm-aevb.cc \
../cnn/examples/rnnlm-batch.cc \
../cnn/examples/rnnlm-cfsm.cc \
../cnn/examples/rnnlm-givenbag.cc \
../cnn/examples/rnnlm-mp.cc \
../cnn/examples/rnnlm.cc \
../cnn/examples/rnnlm2.cc \
../cnn/examples/segrnn-sup.cc \
../cnn/examples/skiprnnlm.cc \
../cnn/examples/tag-bilstm.cc \
../cnn/examples/textcat.cc \
../cnn/examples/tok-embed.cc \
../cnn/examples/xor-batch-lookup.cc \
../cnn/examples/xor-batch.cc \
../cnn/examples/xor-xent.cc \
../cnn/examples/xor.cc 

CC_DEPS += \
./cnn/examples/embed-cl.d \
./cnn/examples/encdec.d \
./cnn/examples/mlc.d \
./cnn/examples/nlm.d \
./cnn/examples/poisson-regression.d \
./cnn/examples/read-write.d \
./cnn/examples/rnnlm-aevb.d \
./cnn/examples/rnnlm-batch.d \
./cnn/examples/rnnlm-cfsm.d \
./cnn/examples/rnnlm-givenbag.d \
./cnn/examples/rnnlm-mp.d \
./cnn/examples/rnnlm.d \
./cnn/examples/rnnlm2.d \
./cnn/examples/segrnn-sup.d \
./cnn/examples/skiprnnlm.d \
./cnn/examples/tag-bilstm.d \
./cnn/examples/textcat.d \
./cnn/examples/tok-embed.d \
./cnn/examples/xor-batch-lookup.d \
./cnn/examples/xor-batch.d \
./cnn/examples/xor-xent.d \
./cnn/examples/xor.d 

OBJS += \
./cnn/examples/embed-cl.o \
./cnn/examples/encdec.o \
./cnn/examples/mlc.o \
./cnn/examples/nlm.o \
./cnn/examples/poisson-regression.o \
./cnn/examples/read-write.o \
./cnn/examples/rnnlm-aevb.o \
./cnn/examples/rnnlm-batch.o \
./cnn/examples/rnnlm-cfsm.o \
./cnn/examples/rnnlm-givenbag.o \
./cnn/examples/rnnlm-mp.o \
./cnn/examples/rnnlm.o \
./cnn/examples/rnnlm2.o \
./cnn/examples/segrnn-sup.o \
./cnn/examples/skiprnnlm.o \
./cnn/examples/tag-bilstm.o \
./cnn/examples/textcat.o \
./cnn/examples/tok-embed.o \
./cnn/examples/xor-batch-lookup.o \
./cnn/examples/xor-batch.o \
./cnn/examples/xor-xent.o \
./cnn/examples/xor.o 


# Each subdirectory must supply rules for building sources it contributes
cnn/examples/%.o: ../cnn/examples/%.cc
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


