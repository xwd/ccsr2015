#!/bin/bash

for i in {1..30}
do
    ../bin/trainTagger --srtrain  --iter $i  --model ./sr-parser-model/parser/  --super ./sr-parser-model/super --input ./sr-parser-model/wsj02-21.stagged  -ifmt "%w|%p|%s \n" 2> train.err.$i > train.$i
done

