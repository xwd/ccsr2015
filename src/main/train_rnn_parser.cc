// C&C NLP tools
// Copyright (c) Universities of Edinburgh, Oxford and Sydney
// Copyright (c) James R. Curran
//
// This software is covered by a non-commercial use licence.
// See LICENCE.txt for the full text of the licence.
//
// If LICENCE.txt is not included in this distribution
// please email candc@it.usyd.edu.au to obtain a copy.

#include "base.h"

#include "config/config.h"

#include "pool.h"

#include "model/model.h"

#include "io/format.h"
#include "config/format.h"

#include "io/reader.h"
#include "io/writer.h"

#include "io/reader_factory.h"

#include "tagger/tagdict.h"
#include "tagger/tagsetdict.h"
#include "tagger/tagger.h"
#include "tagger/super.h"

#include "rnn_parser/rnn_parser.h"
#include "parser/decoder_factory.h"

#include "parser/printer.h"
#include "parser/print_stream.h"
#include "parser/print_factory.h"

#include "parser/integration.h"
#include "rnn_tagger.h"
#include "parser/ShiftReduceHypothesis.h"

#include "timer.h"

#include "rnn_parser.h"

const char *PROGRAM_NAME = "rnn_parser";

using namespace std;
using namespace NLP;
using namespace NLP::Tagger;
using namespace NLP::CCG;
using namespace NLP::Taggers;


std::vector<double> Integration::s_perceptronId;
bool Integration::s_train;

void load_cv0001_supercats(const std::string &filename,
    std::vector<std::vector<std::vector<std::string>* >* > &supercatVec) {
  std::cerr << "Integration::LoadSupercats start" << std::endl;
  cerr << "loading " << filename << endl;
  ifstream in(filename.c_str());
  if(!in)
    throw NLP::IOException("could not open supercats file", filename);
  std::vector<std::vector<std::string>* >* sent =  new std::vector<std::vector<std::string>* >;
  size_t sentcount = 0;
  size_t catcount = 0;
  while (in)
  {
    string s;
    if (!getline(in, s)) break;
    if (s.empty())
    {
      if (sent->size() > 0)
        supercatVec.push_back(sent);
      ++sentcount;
      sent = new std::vector<std::vector<std::string>* >;
      continue;
    }

    istringstream ss(s);
    size_t count = 0;
    std::vector<std::string>* word = new vector<string>;
    while (ss)
    {
      string s;
      if (!getline(ss, s, ' ')) break;
      ++count;
      if (count <= 2) continue;
      //const Cat *cat = cats.markedup[s];
      //if (!cat)
      //{
      //  std::cerr << "sent ID: " << sentcount + 1 << " " << s << " not in markedup" << std::endl;
      //  continue;
      // }
      ++catcount;
      word->push_back(s);
    }
    assert(word->size() > 0);
    sent->push_back(word);
  }
  std::cerr << "total super: " << catcount << " total sent: " << sentcount << std::endl;
  std::cerr << "done" << std::endl;
}

int
run(int argc, char** argv){
  std::ostringstream PREFACE;
  PREFACE << start_preface(argc, argv);

  Config::Main cfg(PROGRAM_NAME);
  Super::Config super_cfg;
  RNNParser::RNNConfig parser_cfg;
  Integration::Config int_cfg("int");

  Config::Alias parser_alias(cfg, SPACE, parser_cfg, "model", "parser");
  Config::Alias super_alias(cfg, super_cfg, "super", "super");
  Config::Op<std::string> config_dir(cfg, SPACE, "config_dir", "the dir storing all config files for rnn_parser", "./rnn_parser_config_files");
  Config::Op<std::string> input_file(cfg, SPACE, "input", "the input file to read from", STDIN);
  Config::Op<IO::Format> ifmt(cfg, "ifmt", "the input file format", Format("%w|%p|%s \n"));
  Config::Op<std::string> output_file(cfg, SPACE, "output", "the output file to write to", STDOUT);
  Config::Op<std::string> log_file(cfg, "log", "the log file to write to", STDERR);
  Config::Op<std::string> prefix(cfg, "prefix", "the prefix of the output and log files to write to", "");
  Config::Restricted<std::string> printer_name(cfg, "printer", "the parser output printer [prolog, deps, grs]",
      &PrinterFactory::check, "grs");

  Config::Op<std::string> cv_supercat_file(cfg, "cv_supercat_file", "cross validated multisuper results", "wsj02-21.cv.0.00025.pos+rnn_super.sfef");
  Config::Op<bool> use_rnn_supertagger(cfg, "use_rnn_tagger", "use rnn tagger to do supertagging", false);
  Config::Op<bool> use_candc_tagdict(cfg, "use_candc_tagdict", "use candc tagdict", false);
  Config::Op<bool> use_pos_backoff(cfg, "use_pos_backoff", "pos backoff tagdict when using c&c super tagdict", false);
  Config::Op<string> model_base_dir(cfg, "rnn_model_base_dir", "the dir for which to load everything related to the model", "./");

  cfg.reg(int_cfg, SPACE);
  cfg.reg(parser_cfg, SPACE);
  cfg.reg(super_cfg, SPACE);

  cfg.parse(argc, argv);
  cfg.check();

  if(prefix() != ""){
    output_file.set_value(prefix() + ".out");
    log_file.set_value(prefix() + ".log");
  }

  if(printer_name() == "grs" && !parser_cfg.alt_markedup.has_changed())
    parser_cfg.alt_markedup.set_value(true);

  Sentence sent;
  ReaderFactory reader(input_file(), ifmt());
  Integration::s_train = true;
  const bool use_rnn_tagger = use_rnn_supertagger();
  Categories cats(parser_cfg.cats(), parser_cfg.markedup(), parser_cfg.alt_markedup());
  Integration integration(int_cfg, super_cfg, parser_cfg, sent, true, 0);
  StreamPrinter::Format FMT = StreamPrinter::FMT_DEV |
      StreamPrinter::FMT_PRINT_UNARY;

  //  if(force_words())
  //    FMT |= StreamPrinter::FMT_FORCE_WORDS;

  IO::Output out(output_file());
  IO::Log log(log_file());
  PrinterFactory printer(printer_name(), out, log, cats, FMT);
  printer.header(PREFACE.str());

  // RNN tagger related
  //  std::string wx_mat = "../files/rnn_tagger/wx.13.mat";
  //  std::string wh_mat = "../files/rnn_tagger/wh.13.mat";
  //  std::string wy_mat = "../files/rnn_tagger/wy.13.mat";
  //  std::string emb_mat = "../files/rnn_tagger/emb.13.mat";
  //  std::string suf_mat = "../files/rnn_tagger/suf.13.mat";
  //  std::string cap_mat = "../files/rnn_tagger/cap.13.mat";

  //  RnnTagger rnn_tagger(200, 23085, 426, 7, 3, 619,
  //      wx_mat, wh_mat, wy_mat, emb_mat, suf_mat, cap_mat,
  //      "../files/rnn_tagger/label_ind.txt");

  // a hack for now
  // rnn_tagger.load_emb_mat("../files/all_turian50_word_emb_dict.txt");

  // RNN parser weights
  //string parser_model_epoch = rnn_model();
  string model_dir = model_base_dir();

  //training related
  //################

  // RNN parser training initialization
  RNNParser parser(parser_cfg, sent, cats, 64, config_dir(), true);

  // load rnn parser training related files
  parser.load_lex_cat_dict(config_dir() + "/markedup_raw_ind");
  unordered_map<size_t, size_t> cover_sent_ids_map;
  parser.load_gold_trees(config_dir() + "/std_train/new_emb_dict_and_pos_feat/sfef/wsj02-21.pipe.processed.sfef");
  parser.load_gold_trees_rnn_fmt(config_dir() + "/std_train/new_emb_dict_and_pos_feat/sfef/wsj02-21.pipe.processed.sfef.rnn_fmt");
  //parser.load_pos_str_ind_map("../files/new_emb_dict_and_pos_feat/sfef/pos_ind_map.txt");

  if (use_rnn_tagger) {

    const size_t dict_cutoff = 20;

    cerr << "using rnn tagger cv supercats\n";

    double total_words = 0.0;
    double total_cats = 0.0;
    Super super(super_cfg);
    std::vector<std::vector<std::vector<std::string>* >* > all_cv_supercats;
    const string cv_supercat_file_path = config_dir() + "/std_train/" + cv_supercat_file();
    load_cv0001_supercats(cv_supercat_file_path, all_cv_supercats);
    size_t sent_id = 0;

   // for (size_t e = 0; e < 60; ++e) {
      sent_id = 0;
      //total_words = 0.0;
      //total_cats = 0.0;
      reader.reset();

      while(reader.next(sent)) {
        if(sent.words.size() == 0) {
          cerr << "end of input" << endl;
          break;
        }

        const size_t NWORDS = sent.words.size();
        total_words += NWORDS;
        sent.msuper.resize(NWORDS);

        // supply mtags for each word
        for (size_t i = 0; i < NWORDS; ++i) {
          std::vector<std::vector<std::string>* > &sentcats =
              *all_cv_supercats[static_cast<size_t>(sent_id)];
          assert(sentcats.size() == NWORDS);
          sent.msuper.resize(NWORDS);
          MultiRaw &mraw = sent.msuper[i];
          mraw.clear();
          std::vector<std::string> &wordcats = *sentcats[i];

          if (use_candc_tagdict()) {
            // ######## use tagdict
            bool has_gold_super = false;
            for (size_t t = 0; t < wordcats.size(); ++t) {

              if (!super.exists(sent.words[i], wordcats[t], sent.pos[i], dict_cutoff, use_pos_backoff()))
                continue;

              mraw.push_back(ScoredRaw(wordcats[t], 0.0));
              ++total_cats;
              if (wordcats[t] == sent.super[i])
                has_gold_super = true;
            }

            if (mraw.size() == 0) {
              // all tags are pruned, try again without a tagdict
              cerr << "word tags get all pruned\n";
              for (size_t j = 0; j < wordcats.size(); ++j) {
                mraw.push_back(ScoredRaw(wordcats[j], 0.0));
                ++total_cats;
                if (wordcats[j] == sent.super[i])
                  has_gold_super = true;
              }

            }

            // ensure gold supertag is supplied
            if (!has_gold_super) {
              ++total_cats;
              mraw.push_back(ScoredRaw(sent.super[i], 1.0));
            }

          } else {
            bool has_gold_super = false;
            for (size_t j = 0; j < wordcats.size(); ++j) {
              mraw.push_back(ScoredRaw(wordcats[j], 0.0));
              ++total_cats;
              if (wordcats[j] == sent.super[i])
                has_gold_super = true;
            }
            // ensure gold supertag is supplied
            if (!has_gold_super) {
              mraw.push_back(ScoredRaw(sent.super[i], 1.0));
              ++total_cats;
            }
          }
        }

        // generate training data (context vecs, target vecs etc.)
        parser.sr_parse_train(0.0, sent_id);
        //parser.sr_parse_train_max_margin(0.0, sent_id);
        ++sent_id;
      } // end epoch

      //cerr << "avg number of supercats per word: " << total_cats/total_words << endl;
      //parser.save_weights(e, model_dir);

    //}

    cerr << "bptt training started...\n";

    // start training
    parser.train(60, model_dir);

  } else {

    // use cv0001 supercats
    cerr << "using C&C tagger cv0001 supercats..\n";

    std::vector<std::vector<std::vector<std::string>* >* > all_cv_supercats;
    load_cv0001_supercats(config_dir() + "/std_train/new_emb_dict_and_pos_feat/sfef/train.in.cv0001.sfef", all_cv_supercats);
    assert(all_cv_supercats.size() == cover_sent_ids_map.size());

    size_t sent_id = 0;

    while (reader.next(sent)) {

      //reader_rnn_fmt.next(sent_rnn_fmt);
      const size_t NWORDS = sent.words.size();
      //assert(NWORDS == sent_rnn_fmt.words.size());

      sent.msuper.resize(NWORDS);

      std::vector<std::vector<std::string>* > &sentcats =
          *all_cv_supercats[static_cast<size_t>(sent_id)];
      sent.msuper.resize(sentcats.size());
      assert(sentcats.size() == NWORDS);

      for (size_t i = 0; i < NWORDS; ++i) {

        //assert(sent.pos[i] == sent_rnn_fmt.pos[i]);

        std::vector<std::string> &wordcats = *sentcats[i];
        MultiRaw &cats = sent.msuper[i];
        cats.resize(0);
        bool has_gold_super = false;
        for (size_t j = 0; j < wordcats.size(); ++j) {

          if (wordcats[j] == sent.super[i]) {
            has_gold_super = true;
          }
          cats.push_back(ScoredRaw(wordcats[j], 0.0));

        }
        assert(has_gold_super);
      }

      if(sent.words.size() == 0) {
        log.stream << "end of input" << endl;
        break;
      }

      parser.sr_parse_train(0.0, sent_id);
      ++sent_id;

    }

    cerr << "bptt training started...\n";

    // start training
    parser.train(60, model_dir);

  }

  return 0;

}

#include "main.h"
