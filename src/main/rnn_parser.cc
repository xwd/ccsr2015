// C&C NLP tools
// Copyright (c) Universities of Edinburgh, Oxford and Sydney
// Copyright (c) James R. Curran
//
// This software is covered by a non-commercial use licence.
// See LICENCE.txt for the full text of the licence.
//
// If LICENCE.txt is not included in this distribution
// please email candc@it.usyd.edu.au to obtain a copy.

#include "base.h"

#include "config/config.h"

#include "pool.h"

#include "model/model.h"

#include "io/format.h"
#include "config/format.h"

#include "io/reader.h"
#include "io/writer.h"

#include "io/reader_factory.h"

#include "tagger/tagdict.h"
#include "tagger/tagsetdict.h"
#include "tagger/tagger.h"
#include "tagger/super.h"

#include "rnn_parser/rnn_parser.h"
#include "parser/decoder_factory.h"

#include "parser/printer.h"
#include "parser/print_stream.h"
#include "parser/print_factory.h"
#include "parser/integration.h"

#include "rnn_tagger.h"
#include "parser/ShiftReduceHypothesis.h"

#include "timer.h"

#include "rnn_parser.h"

const char *PROGRAM_NAME = "rnn_parser";

using namespace std;
using namespace NLP;
using namespace NLP::Tagger;
using namespace NLP::CCG;
using namespace NLP::Taggers;


const static int FMT_WORDS = 1 << 0;
const static int FMT_LEMMA = 1 << 1;
const static int FMT_POS = 1 << 2;
const static int FMT_CHUNK = 1 << 3;
const static int FMT_NER = 1 << 4;
const static int FMT_SUPER = 1 << 5;
const static int FMT_CAT = 1 << 6;

// these two extra lines are needed here
// for legacy reasons, it's a hack, remove later
std::vector<double> Integration::s_perceptronId;
bool Integration::s_train;

void lexical(Sentence &sent, IO::Output &out, const int FORMAT){
  const ulong NWORDS = sent.words.size();

  if((FORMAT & FMT_LEMMA) && sent.lemmas.size() != NWORDS)
    throw NLP::Exception("not enough lemmas for the sentence");

  // don't need to check POS tags since they must already exist

  if((FORMAT & FMT_CHUNK) && sent.chunks.size() != NWORDS)
    throw NLP::Exception("not enough chunks for the sentence");

  if((FORMAT & FMT_NER) && sent.entities.size() != NWORDS)
    throw NLP::Exception("not enough named entities for the sentence");

  const bool HAS_CORRECT_STAG = sent.cats.size();

  if(FORMAT & FMT_WORDS){
    out.stream << "<c>";
    for(ulong i = 0; i < NWORDS; ++i){
      out.stream << ' ' << sent.words[i];
      if(FORMAT & FMT_LEMMA)
        out.stream << '|' << sent.lemmas[i];
      if(FORMAT & FMT_POS)
        out.stream << '|' << sent.pos[i];
      if(FORMAT & FMT_CHUNK)
        out.stream << '|' << sent.chunks[i];
      if(FORMAT & FMT_NER)
        out.stream << '|' << sent.entities[i];
      if(FORMAT & FMT_SUPER){
        out.stream << '|';
        if(HAS_CORRECT_STAG)
          sent.cats[i]->out_novar_noX(out.stream, false);
        else
          out.stream << sent.msuper[i][0].raw;
      }
      if(FORMAT & FMT_CAT){
        out.stream << '|';
        if(HAS_CORRECT_STAG)
          sent.cats[i]->out(out.stream);
        else
          out.stream << "none";
      }
    }
    out.stream << '\n';
  }
}


void load_cv0001_supercats(const std::string &filename,
    std::vector<std::vector<std::vector<std::string>* >* > &supercatVec) {
  std::cerr << "LoadSupercats start" << std::endl;
  ifstream in(filename.c_str());
  if(!in)
    throw NLP::IOException("could not open supercats file", filename);
  std::vector<std::vector<std::string>* >* sent =  new std::vector<std::vector<std::string>* >;
  size_t sentcount = 0;
  size_t catcount = 0;
  while (in)
  {
    string s;
    if (!getline(in, s)) break;
    if (s.empty())
    {
      if (sent->size() > 0)
        supercatVec.push_back(sent);
      ++sentcount;
      sent = new std::vector<std::vector<std::string>* >;
      continue;
    }

    istringstream ss(s);
    size_t count = 0;
    std::vector<std::string>* word = new vector<string>;
    while (ss)
    {
      string s;
      if (!getline(ss, s, ' ')) break;
      ++count;
      if (count <= 2) continue;
      //const Cat *cat = cats.markedup[s];
      //if (!cat)
      //{
      //  std::cerr << "sent ID: " << sentcount + 1 << " " << s << " not in markedup" << std::endl;
      //  continue;
      // }
      ++catcount;
      word->push_back(s);
    }
    assert(word->size() > 0);
    sent->push_back(word);
  }
  std::cerr << "total super: " << catcount << " total sent: " << sentcount << std::endl;
  std::cerr << "done" << std::endl;
}


int
run(int argc, char** argv){
  std::ostringstream PREFACE;
  PREFACE << start_preface(argc, argv);

  Config::Main cfg(PROGRAM_NAME);
  Super::Config super_cfg;
  RNNParser::RNNConfig parser_cfg;
  //Integration::Config int_cfg("int");

  Config::Alias parser_alias(cfg, SPACE, parser_cfg, "model", "parser");
  Config::Alias super_alias(cfg, super_cfg, "super", "super");
  Config::Op<std::string> config_dir(cfg, SPACE, "config_dir", "the dir storing all config files for rnn_parser", "./rnn_parser_config_files");

  Config::Op<std::string> input_file(cfg, SPACE, "input", "the input file to read from", STDIN);
  Config::Op<IO::Format> ifmt(cfg, "ifmt", "the input file format", Format("%w|%p \n"));
  //Config::Op<IO::Format> rnn_ifmt(cfg, "rnn_ifmt", "the input file format", Format("%w|%p \n"));

  Config::Op<std::string> output_file(cfg, SPACE, "output", "the output file to write to", STDOUT);
  Config::Op<std::string> log_file(cfg, "log", "the log file to write to", STDERR);
  //Config::Op<std::string> trace_file(cfg, "trace", "the trace file to write to", STDERR);
  Config::Op<std::string> prefix(cfg, "prefix", "the prefix of the output and log files to write to", "");
  Config::Op<bool> test_calc_f1(cfg, "test_calc_f1", "test f1 calculation for XF1 training", false);
  Config::Op<bool> test_calc_f1_train(cfg, "test_calc_f1_train", "test f1 calculation for XF1 on 0221 (true)/ 00 (false)", false);
  Config::Op<bool> oracle(cfg, "oracle", "oracle decoding", false);

  Config::Restricted<std::string> printer_name(cfg, "printer", "the parser output printer [prolog, deps, grs]",
      &PrinterFactory::check, "grs");
  Config::Op<bool> force_words(cfg, "force_words", "force the parser to print words on fails", true);

  //Config::Op<bool> sr_train(cfg, "srtrain", "run rnn shiftreduce training", false);
  //Config::Op<std::string> sr_model(cfg, SPACE, "srmodel", "perceptron model for the shiftreduce parser", STDIN);
  //Config::Op<bool> frag(cfg, "frag", "allow fragmented output", false);
  Config::Op<ushort> sr_beam(cfg, "srbeam", "shift reduce beam size", 16);

  //Config::Op<bool> sr_trace(cfg, "sr_trace", "to trace output", false);
  Config::Op<bool> use_rnn_supertagger(cfg, "use_rnn_tagger", "use rnn super", false);
  Config::Op<bool> bi_super(cfg, "bi_super", "use bidirectional rnn super", true);
  Config::Op<bool> use_rnn_tagdict(cfg, "use_rnn_tagdict", "use_rnn_tagdict", false);
  Config::Op<bool> use_candc_tagdict(cfg, "use_candc_tagdict", "use_candc_tagdict", false);
  Config::Op<size_t> candc_dict_cutoff(cfg, "candc_dict_cutoff", "candc tagdict cutoff", 20);
  Config::Op<bool> candc_pos_backoff(cfg, "candc_pos_backoff", "use pos tagdict backoff when using c&c super tagdict", false);
  Config::Op<std::string> rnn_super_model(cfg, "rnn_super_model", "rnn super model", "./");
  Config::Op<std::string> emb_file(cfg, "emb", "file name of the embeddings file (*UNKONWN* changed to ***PADDING***, otherwise untouched)", "");
  Config::Op<std::string> suf_file(cfg, "suf", "file name of the suffix file", "");
  Config::Op<size_t> precompute(cfg, "precompute", "precompute lh vals (0=disable)", 0);
  Config::Op<bool> use_lh_cache(cfg, "use_lh_cache", "use cached lh vals", false);
  //Config::Op<bool> use_backoff_tagdict(cfg, "use_backoff_tagdict", "backoff to easyccg dict when word freq is < cutoff", false);

  //Config::Op<std::string> input_file_rnn_fmt(cfg, SPACE, "input_rnn_fmt", "the rnn fmt input file to read from", STDIN);
  Config::Op<double> beta(cfg, "tagger_beam_beta", "beam threshold for the tagger", 0.0001);
  Config::Op<string> rnn_model(cfg, "rnn_parser_model", "specify the weights to use", "0");
  Config::Op<string> model_base_dir(cfg, "rnn_model_base_dir", "the dir which stores all the model matrices etc.", "./");
  //Config::Op<string> cover(cfg, "training_cover", "choose training data coverage (sfef/stet)", "sfef");
  //Config::Op<string> dev_test_wiki_bio(cfg, "dev_test_wiki_bio", "dev_or_test", "dev");

  //cfg.reg(int_cfg, SPACE);
  cfg.reg(parser_cfg, SPACE);
  cfg.reg(super_cfg, SPACE);

  cfg.parse(argc, argv);
  cfg.check();

  if(prefix() != ""){
    output_file.set_value(prefix() + ".out");
    log_file.set_value(prefix() + ".log");
  }

  if(printer_name() == "grs" && !parser_cfg.alt_markedup.has_changed())
    parser_cfg.alt_markedup.set_value(true);

  Sentence sent;
  //Sentence sent_rnn_fmt;

  ReaderFactory reader(input_file(), ifmt());
  //ReaderFactory reader_rnn_fmt(input_file_rnn_fmt(), rnn_ifmt());

  //Integration::s_train = false;
  const size_t srbeam = sr_beam();
  const bool use_rnn_tagger = use_rnn_supertagger();
  //string coverage = cover();

//  if (train)
//    cerr << "training data coverage: " << coverage << endl;

  Categories cats(parser_cfg.cats(), parser_cfg.markedup(), parser_cfg.alt_markedup());

  //Integration integration(int_cfg, super_cfg, parser_cfg, sent, false, srbeam);

  StreamPrinter::Format FMT = StreamPrinter::FMT_DEV |
      StreamPrinter::FMT_PRINT_UNARY;

  if(force_words())
    FMT |= StreamPrinter::FMT_FORCE_WORDS;

  IO::Output out(output_file());
  IO::Log log(log_file());
  //IO::Output trace(trace_file());
  PrinterFactory printer(printer_name(), out, log, cats, FMT);
  printer.header(PREFACE.str());


//  RnnTagger(const string &model_dir,
//            const string &all_untouched_embeddings,
//            const string &suf_str_ind_file,
//            size_t output_fmt,
//            double dropout_success_prob,
//            bool dropout,
//            bool use_candc_tag_dicts,
//            size_t candc_tagdict_cutoff,
//            bool candc_tagdict_pos_backoff,
//            Super::Config &super_cfg);

  //RNN tagger related
  NLP::Taggers::RnnTagger rnn_super(rnn_super_model(), emb_file(), suf_file(), 0, 0.75, true, "sig", bi_super(), true);

  // RNN parser weights
  string parser_model_epoch = rnn_model();
  string model_dir = model_base_dir();

  std::string wx = model_dir + "/wx." + parser_model_epoch + ".mat";
  std::string wh = model_dir + "/wh." + parser_model_epoch + ".mat";
  std::string wy = model_dir + "/wy." + parser_model_epoch + ".mat";
  std::string emb = model_dir + "/emb." + parser_model_epoch + ".mat";
  std::string suf = model_dir + "/suf." + parser_model_epoch + ".mat";
  std::string cap = model_dir + "/cap." + parser_model_epoch + ".mat";

  std::string super_emb;
  std::string comp_mat;
  if (parser_cfg.use_supercat_feats())
    super_emb = model_dir + "/super_emb." + parser_model_epoch + ".mat";
  else
    super_emb = "";
  if (parser_cfg.use_phrase_emb_feats())
    comp_mat = model_dir + "/comp_mat." + parser_model_epoch + ".mat";
  else
    comp_mat = "";


  //  if (use_rnn_tagger) {
  //    rnn_tagger.load_dev_test_data();
  //    rnn_tagger.eval_super(true);
  //    if (dev_test_wiki_bio() == "dev")
  //      rnn_tagger.mtag(true);
  //    else if (dev_test_wiki_bio() == "wiki")
  //      rnn_tagger.mtag_wiki();
  //    else
  //      rnn_tagger.mtag(false);
  //  }

  if (precompute() > 0)
    parser_cfg.precompute.set_value(true);
  if (use_lh_cache())
    parser_cfg.use_lh_cache.set_value(true);

  RNNParser parser(parser_cfg, sent, cats, srbeam, config_dir(), false);
  parser.re_init_mats(wx, wh, wy, emb, suf, cap, super_emb, comp_mat);
  parser.load_lex_cat_dict(config_dir() + "/markedup_raw_ind");
  //parser.load_pos_str_ind_map("../files/new_emb_dict_and_pos_feat/sfef/pos_ind_map.txt");

  parser.load_supercat_map(model_dir + "/supercat_map." + parser_model_epoch);

  //parser.load_emb_mat_full("../files/all_turian50_word_emb_dict.txt");

  const double BETA = beta();
  size_t sent_id = 0;

  Super super(super_cfg);
  //ulong dict_cutoff = 20;

//  std::vector<std::vector<std::vector<std::pair<std::string, double> > > > m_all_rnn_tagging_res;
//  if (dev_test_wiki_bio() == "dev")
//    m_all_rnn_tagging_res = rnn_tagger.m_all_res_dev;
//  else if (dev_test_wiki_bio() == "wiki")
//    m_all_rnn_tagging_res = rnn_tagger.m_all_res_wiki;
//  else
//    m_all_rnn_tagging_res = rnn_tagger.m_all_res_test;

  // xf1 training related
  double total_correct_deps = 0.0;
  double total_returned_deps = 0.0;
  double total_gold_deps_train = 0.0;
  if (test_calc_f1())
    parser.init_xf1_training(test_calc_f1_train());

  std::chrono::time_point<std::chrono::system_clock> start_t, end_t;
  start_t = std::chrono::system_clock::now();

  if (use_rnn_tagger) {
    while(reader.next(sent)) {
      const size_t NWORDS = sent.words.size();
      sent.msuper.resize(NWORDS);
      rnn_super.mtag_sent(sent, BETA);
      // supply mtags for each word
//      for (size_t i = 0; i < NWORDS; ++i) {
//
//        sent.msuper.resize(NWORDS);
//        MultiRaw &mraw = sent.msuper[i];
//        mraw.resize(0);
//        vector<pair<string, double> > &word_mtags = sent_mtags[i];
//
//        double word_1best = sent_word_1best[i];
//        double prob_cutoff = word_1best*BETA;
//
//        for (size_t j = 0; j < word_mtags.size(); ++j) {
//          if (word_mtags[j].second < prob_cutoff)
//            continue;
//
//          if (use_rnn_tagdict() && !super.exists(sent.words[i], word_mtags[j].first, dict_cutoff, use_easy_tagdict()))
//            continue;
//
//          mraw.push_back(ScoredRaw(word_mtags[j].first, word_mtags[j].second));
//        }
//
//        if (mraw.size() == 0) {
//          // all tags are pruned, try again without a tagdict
//          for (size_t j = 0; j < word_mtags.size(); ++j) {
//            if (word_mtags[j].second < prob_cutoff)
//              continue;
//
//            mraw.push_back(ScoredRaw(word_mtags[j].first, word_mtags[j].second));
//          }
//
//        }
//      }

      if(NWORDS == 0) {

        log.stream << "end of input" << endl;
        break;

      } else if (NWORDS == 1) {
        sent.cats.clear();
        printer.parsed(0, sent, BETA, 0);
        lexical(sent, out, FMT);
        out.stream << "\n";

      } else {

        const ShiftReduceHypothesis *output = parser.sr_parse_beam_search(srbeam, sent_id, oracle(), test_calc_f1_train());

        if (!output) {
          parser.Clear();
          cerr << "sent: " << sent_id << " has no output" << endl;
        } else {

          size_t stackSize = output->GetStackSize();
          std::vector<const SuperCat *> roots;
          vector<const SuperCat *>::const_reverse_iterator rit;
          roots.push_back(output->GetStackTopSuperCat());

          for (size_t i = 0; i < stackSize - 1; ++i) {
            output = output->GetPrvStack();
            roots.push_back(output->GetStackTopSuperCat());
          }

          for (rit = roots.rbegin(); rit < roots.rend(); ++rit) {
            printer.parsed(*rit, sent, BETA, 0);
            if (test_calc_f1()) {
              parser.get_deps(*rit, sent_id, total_correct_deps, total_returned_deps, FMT, test_calc_f1_train());
            }

          }

          lexical(sent, out, FMT);
          out.stream << "\n";

          parser.Clear();
        }
      }
      if(test_calc_f1())
        total_gold_deps_train += parser.get_total_sent_gold_deps(sent_id, test_calc_f1_train());
      ++sent_id;
    }

    if (precompute()) {
      cerr << "caching lh vals" << endl;
      parser.precompute(precompute());
    }

    if (test_calc_f1()) {
      cerr << "total_correct_deps: " << total_correct_deps << endl;
      cerr << "total_deps: " << total_returned_deps << endl;
      cerr << "total_gold_deps: " << total_gold_deps_train << endl;
      cerr << "p: " << total_correct_deps / total_returned_deps << endl;
      cerr << "r: " << total_correct_deps / total_gold_deps_train << endl;
    }

  } else {

    Super super(super_cfg);
    double beta = 0.0001;

    while(reader.next(sent)) {

      //reader_rnn_fmt.next(sent_rnn_fmt);
      //const size_t NWORDS = sent.words.size();
      //assert(NWORDS == sent_rnn_fmt.words.size());

      if(sent.words.size() == 0){
        log.stream << "end of input" << endl;
        break;
      }

      if(sent.words.size() == 1) {
        ulong dict_cutoff = 20;
        //std::cerr << "size = 1" << std::endl;
        super.mtag(sent, NLP::Tagger::FWDBWD, dict_cutoff, beta);
        sent.cats.clear();
        printer.parsed(0, sent, beta, dict_cutoff);
        lexical(sent, out, FMT);
        out.stream << "\n";

      } else {
        ulong dict_cutoff = 20;
        super.mtag(sent, NLP::Tagger::FWDBWD, dict_cutoff, beta);
        const ShiftReduceHypothesis *output = parser.sr_parse_beam_search(beta, sent_id);

        if (!output) {
          parser.Clear();
          cerr << "sent: " << sent_id << " has no output" << endl;
        } else {

          size_t stackSize = output->GetStackSize();
          std::vector<const SuperCat *> roots;
          vector<const SuperCat *>::const_reverse_iterator rit;
          roots.push_back(output->GetStackTopSuperCat());

          for (size_t i = 0; i < stackSize - 1; ++i) {
            output = output->GetPrvStack();
            roots.push_back(output->GetStackTopSuperCat());
          }

          for (rit = roots.rbegin(); rit < roots.rend(); ++rit) {
            printer.parsed(*rit, sent, BETA, 0);

          }

          lexical(sent, out, FMT);
          out.stream << "\n";

          parser.Clear();
        }
      }

      ++sent_id;
    }
  }

  end_t = std::chrono::system_clock::now();
  std::chrono::duration<double> elapsed_seconds = end_t - start_t;
  cerr << " time: " << elapsed_seconds.count() << endl;

  return 0;
}

#include "main.h"
