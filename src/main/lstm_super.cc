const char *PROGRAM_NAME = "lstm_super";

#include "base.h"
#include "main.h"

#include "config/config.h"
#include "io/format.h"
#include "config/format.h"

#include "io/reader.h"
#include "io/reader_factory.h"
#include "io/writer.h"
#include "io/writer_factory.h"

#include "model/model.h"
#include "parser/integration.h"

#include "timer.h"
#include "lstm_tagger.h"

using namespace std;
using namespace NLP;
using namespace NLP::IO;
using namespace NLP::Taggers;
using namespace NLP::CCG;

// these two extra lines are needed here
// for legacy reasons, it's a hack, remove later
std::vector<double> Integration::s_perceptronId;
bool Integration::s_train;

int
run(int argc, char** argv){
  Config::Main cfg(PROGRAM_NAME);
  Config::Op<bool> train(cfg, "train", "training (ture/false)", false);
  Config::Op<bool> att(cfg, "att", "use attention", true);
  Config::Op<size_t> att_t(cfg, "att_t", "local (0) or global attention (1)", 0);
  Config::Op<bool> att_c(cfg, "att_c", "context target hidden concat order (true = cache, false = simple)", false);
  Config::Op<bool> att_h(cfg, "att_h", "attentional hidden layer before softmax", true);
  Config::Op<std::string> input_file(cfg, NLP::Config::SPACE, "input", "the input file to read from", "./");
  Config::Op<std::string> input_file_dev(cfg, NLP::Config::SPACE, "input_dev", "the input file to read from", "./");
  Config::Op<IO::Format> ifmt(cfg, "ifmt", "the input file format", Format("%w|%p|%s \n"));
  Config::Op<std::string> model(cfg, "model", "the dir to load/save the model", "./");
  Config::Op<std::string> model_fname(cfg, "model_fname", "fname of model to load from", "");
  Config::Op<std::string> emb_file(cfg, "emb", "file name of the embeddings file (*UNKONWN* changed to ***PADDING***, otherwise untouched)", "emb_turian_100_scaled");
  Config::Op<unsigned> de(cfg, "de", "embedding dimension", 100);
  Config::Op<unsigned> dwin(cfg, "dwin", "context win size", 1);
  Config::Op<double> pdrop(cfg, "pdrop", "drop out prob", 0.3);
  Config::Op<unsigned> dh(cfg, "dh", "hidden layer size", 256);
  Config::Op<unsigned> layers(cfg, "layers", "number of layers in one direction", 1);
  Config::Op<size_t> output_fmt(cfg, "output_fmt", "0: no output (run evaluation) 1: singe 2: multi", 0);
  Config::Op<double> beta(cfg, "beta", "beta cutoff", 0.0);

  cfg.parse(argc, argv);
  cfg.check();

  if (!train()) {
    //########## testing
    ReaderFactory reader_dev(input_file_dev(), ifmt());
    string emb_file_name = model() + "/" + emb_file();
    LSTMTagger lstm_super(model(), emb_file_name, de(), dh(), dwin(), layers(), pdrop(), att(), att_t(), att_c(), att_h());
    lstm_super.decode(reader_dev, model(), model_fname(), beta());

    //    if (output_fmt() == 0)
    //      rnn_super.eval_mtag(reader_dev);
    //    else if (output_fmt() == 1)
    //      rnn_super.tag_file(reader, false);
    //    else if (output_fmt() == 2)
    //      rnn_super.mtag_file(reader, beta());
  } else {
    //########## training
    cerr << "training input file: " << input_file() << endl;
    cerr << "att: " << att() << endl;
    cerr << "att_t: " << att_t() << endl;
    cerr << "att_c: " << att_c() << endl;
    cerr << "att_h: " << att_h() << endl;
    ReaderFactory reader(input_file(), ifmt());
    ReaderFactory reader_dev(input_file_dev(), ifmt());
    string emb_file_name = model() + "/" + emb_file();

    LSTMTagger lstm_super(model(), emb_file_name, de(), dh(), dwin(), layers(), pdrop(), att(), att_t(), att_c(), att_h());
    cerr << "training started\n";
    lstm_super.run(reader, reader_dev, model(), model_fname());
  }
  return 0;
}
