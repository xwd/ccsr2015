// C&C NLP tools
// Copyright (c) Universities of Edinburgh, Oxford and Sydney
// Copyright (c) James R. Curran
//
// This software is covered by a non-commercial use licence.
// See LICENCE.txt for the full text of the licence.
//
// If LICENCE.txt is not included in this distribution
// please email candc@it.usyd.edu.au to obtain a copy.

#include "base.h"

#include "config/config.h"

#include "pool.h"

#include "model/model.h"

#include "io/format.h"
#include "config/format.h"

#include "io/reader.h"
#include "io/writer.h"

#include "io/reader_factory.h"

#include "tagger/tagdict.h"
#include "tagger/tagsetdict.h"
#include "tagger/tagger.h"
#include "tagger/super.h"

#include "rnn_parser/rnn_parser.h"
#include "parser/decoder_factory.h"

#include "parser/printer.h"
#include "parser/print_stream.h"
#include "parser/print_factory.h"
#include "parser/integration.h"

#include "rnn_tagger.h"
#include "parser/ShiftReduceHypothesis.h"

#include "timer.h"

#include "rnn_parser.h"

const char *PROGRAM_NAME = "rnn_parser";

using namespace std;
using namespace NLP;
using namespace NLP::Tagger;
using namespace NLP::CCG;
using namespace NLP::Taggers;


const static int FMT_WORDS = 1 << 0;
const static int FMT_LEMMA = 1 << 1;
const static int FMT_POS = 1 << 2;
const static int FMT_CHUNK = 1 << 3;
const static int FMT_NER = 1 << 4;
const static int FMT_SUPER = 1 << 5;
const static int FMT_CAT = 1 << 6;

// these two extra lines are needed here
// for legacy reasons, it's a hack, remove later
std::vector<double> Integration::s_perceptronId;
bool Integration::s_train;

void lexical(Sentence &sent, IO::Output &out, const int FORMAT){
  const ulong NWORDS = sent.words.size();

  if((FORMAT & FMT_LEMMA) && sent.lemmas.size() != NWORDS)
    throw NLP::Exception("not enough lemmas for the sentence");

  // don't need to check POS tags since they must already exist

  if((FORMAT & FMT_CHUNK) && sent.chunks.size() != NWORDS)
    throw NLP::Exception("not enough chunks for the sentence");

  if((FORMAT & FMT_NER) && sent.entities.size() != NWORDS)
    throw NLP::Exception("not enough named entities for the sentence");

  const bool HAS_CORRECT_STAG = sent.cats.size();

  if(FORMAT & FMT_WORDS){
    out.stream << "<c>";
    for(ulong i = 0; i < NWORDS; ++i){
      out.stream << ' ' << sent.words[i];
      if(FORMAT & FMT_LEMMA)
        out.stream << '|' << sent.lemmas[i];
      if(FORMAT & FMT_POS)
        out.stream << '|' << sent.pos[i];
      if(FORMAT & FMT_CHUNK)
        out.stream << '|' << sent.chunks[i];
      if(FORMAT & FMT_NER)
        out.stream << '|' << sent.entities[i];
      if(FORMAT & FMT_SUPER){
        out.stream << '|';
        if(HAS_CORRECT_STAG)
          sent.cats[i]->out_novar_noX(out.stream, false);
        else
          out.stream << sent.msuper[i][0].raw;
      }
      if(FORMAT & FMT_CAT){
        out.stream << '|';
        if(HAS_CORRECT_STAG)
          sent.cats[i]->out(out.stream);
        else
          out.stream << "none";
      }
    }
    out.stream << '\n';
  }
}

void load_cv0001_supercats(const std::string &filename,
                           std::vector<std::vector<std::vector<std::string>* >* > &supercatVec) {
  std::cerr << "LoadSupercats start" << std::endl;
  ifstream in(filename.c_str());
  if(!in)
    throw NLP::IOException("could not open supercats file", filename);
  std::vector<std::vector<std::string>* >* sent =  new std::vector<std::vector<std::string>* >;
  size_t sentcount = 0;
  size_t catcount = 0;
  while (in)
  {
    string s;
    if (!getline(in, s)) break;
    if (s.empty())
    {
      if (sent->size() > 0)
        supercatVec.push_back(sent);
      ++sentcount;
      sent = new std::vector<std::vector<std::string>* >;
      continue;
    }

    istringstream ss(s);
    size_t count = 0;
    std::vector<std::string>* word = new vector<string>;
    while (ss)
    {
      string s;
      if (!getline(ss, s, ' ')) break;
      ++count;
      if (count <= 2) continue;
      //const Cat *cat = cats.markedup[s];
      //if (!cat)
      //{
      //  std::cerr << "sent ID: " << sentcount + 1 << " " << s << " not in markedup" << std::endl;
      //  continue;
      // }
      ++catcount;
      word->push_back(s);
    }
    assert(word->size() > 0);
    sent->push_back(word);
  }
  std::cerr << "total super: " << catcount << " total sent: " << sentcount << std::endl;
  std::cerr << "done" << std::endl;
}

int
run(int argc, char** argv){
  std::ostringstream PREFACE;
  PREFACE << start_preface(argc, argv);

  Config::Main cfg(PROGRAM_NAME);
  Super::Config super_cfg;
  RNNParser::RNNConfig parser_cfg;
  //Integration::Config int_cfg("int");

  Config::Alias parser_alias(cfg, SPACE, parser_cfg, "model", "parser");
  Config::Alias super_alias(cfg, super_cfg, "super", "super");

  Config::Op<ushort> sr_beam(cfg, "srbeam", "shift reduce beam size", 5);
  Config::Op<std::string> input_file(cfg, SPACE, "input", "the input file to read from", STDIN);
  Config::Op<IO::Format> ifmt(cfg, "ifmt", "the input file format", Format("%w|%p \n"));
  Config::Op<std::string> output_file(cfg, SPACE, "output", "the output file to write to", STDOUT);
  Config::Op<bool> force_words(cfg, "force_words", "force the parser to print words on fails", true);
  Config::Op<std::string> log_file(cfg, "log", "the log file to write to", STDERR);
  Config::Op<std::string> prefix(cfg, "prefix", "the prefix of the output and log files to write to", "");
  Config::Restricted<std::string> printer_name(cfg, "printer", "the parser output printer [prolog, deps, grs]", &PrinterFactory::check, "grs");

  Config::Op<std::string> config_dir(cfg, SPACE, "config_dir", "the dir storing all config files for rnn_parser", "./rnn_parser_config_files");
  Config::Op<size_t> mode(cfg, "mode", "0: test f1 calc; 1: xf1 grad check; 2: run training", 2);

  Config::Op<bool> use_lh_cache(cfg, "use_lh_cache", "use cached lh vals", false);
  Config::Op<bool> test_calc_f1_train(cfg, "use_train_or_dev_data", "run on 0221 (true)/ 00 (false)", true);
  Config::Op<bool> oracle(cfg, "oracle", "oracle decoding", false);
  Config::Op<bool> xf1_super(cfg, "xf1_super", "train xf1 super", false);
  Config::Op<bool> bptt_graph(cfg, "bptt_graph", "bptt over dag", true);

  Config::Op<bool> use_rnn_supertagger(cfg, "use_rnn_tagger", "use rnn tagger to do supertagging", true);
  Config::Op<bool> bi_super(cfg, "bi_super", "use bidirectional rnn super", false);
  Config::Op<std::string> rnn_super_model(cfg, "rnn_super_model", "rnn super model", "./");
  Config::Op<std::string> emb_file(cfg, "emb", "file name of the embeddings file (*UNKONWN* changed to ***PADDING***, otherwise untouched)", "");
  Config::Op<std::string> suf_file(cfg, "suf", "file name of the suffix file", "");
  Config::Op<double> beta(cfg, "tagger_beam_beta", "beam threshold for the tagger", 0.08);
  Config::Op<double> xf1_super_lr(cfg, "xf1_super_lr", "learning rate for xf1_super training", 0.0025);

  Config::Op<string> model_base_dir(cfg, "rnn_model_base_dir", "the dir to save weights", "./");
  Config::Op<string> rnn_model(cfg, "rnn_parser_model", "specify the weights to use", "0");
  Config::Op<string> xf1_model(cfg, "xf1_model", "the dir to save weights", "./");
  Config::Op<size_t> num_threads(cfg, "num_threads", "number of threads", 1);
  Config::Op<size_t> epochs(cfg, "epochs", "number of epochs", 60);
  Config::Op<size_t> restart(cfg, "restart", "restart training (give an epoch number to restart from, 0 means from scratch)", 0);

  assert(bptt_graph());
  //cfg.reg(int_cfg, SPACE);
  cfg.reg(parser_cfg, SPACE);
  cfg.reg(super_cfg, SPACE);

  cfg.parse(argc, argv);
  cfg.check();

  if(prefix() != ""){
    output_file.set_value(prefix() + ".out");
    log_file.set_value(prefix() + ".log");
  }

  if(printer_name() == "grs" && !parser_cfg.alt_markedup.has_changed())
    parser_cfg.alt_markedup.set_value(true);

//  if (precompute()) {
//    parser_cfg.precompute.set_value(true);
//    assert(mode() == 0);
//  }

  Sentence sent;
  //Sentence sent_rnn_fmt;

  ReaderFactory reader(input_file(), ifmt());
  //ReaderFactory reader_rnn_fmt(input_file_rnn_fmt(), rnn_ifmt());

  //Integration::s_train = false;
  const size_t srbeam = sr_beam();
  const bool use_rnn_tagger = use_rnn_supertagger();
  bool run_training = false;
  if (mode() == 2) {
    run_training = true;
  }
  //string coverage = cover();

  //  if (train)
  //    cerr << "training data coverage: " << coverage << endl;

  Categories cats(parser_cfg.cats(), parser_cfg.markedup(), parser_cfg.alt_markedup());

  //Integration integration(int_cfg, super_cfg, parser_cfg, sent, false, srbeam);

  StreamPrinter::Format FMT = StreamPrinter::FMT_DEV |
      StreamPrinter::FMT_PRINT_UNARY;

  if(force_words())
    FMT |= StreamPrinter::FMT_FORCE_WORDS;

  IO::Output out(output_file());
  IO::Log log(log_file());
  //IO::Output trace(trace_file());
  PrinterFactory printer(printer_name(), out, log, cats, FMT);
  printer.header(PREFACE.str());

  //  RnnTagger(const string &model_dir,
  //            const string &all_untouched_embeddings,
  //            const string &suf_str_ind_file,
  //            size_t output_fmt,
  //            double dropout_success_prob,
  //            bool dropout,
  //            bool use_candc_tag_dicts,
  //            size_t candc_tagdict_cutoff,
  //            bool candc_tagdict_pos_backoff,
  //            Super::Config &super_cfg);

  //RNN tagger related
  NLP::Taggers::RnnTagger rnn_super(rnn_super_model(), emb_file(), suf_file(), 0, 0.75, true, "sig", bi_super(), true, xf1_super_lr());

  string cv_super_models;
  if (bi_super())
    cv_super_models = config_dir() + "/cv_super_models_bi/";
  else
    cv_super_models = config_dir() + "/cv_super_models/";

  NLP::Taggers::RnnTagger rnn_super1(cv_super_models + "/model.1", emb_file(), suf_file(), 0, 0.75, true, "sig", bi_super(), true);
  NLP::Taggers::RnnTagger rnn_super2(cv_super_models + "/model.2", emb_file(), suf_file(), 0, 0.75, true, "sig", bi_super(), true);
  NLP::Taggers::RnnTagger rnn_super3(cv_super_models + "/model.3", emb_file(), suf_file(), 0, 0.75, true, "sig", bi_super(), true);
  NLP::Taggers::RnnTagger rnn_super4(cv_super_models + "/model.4", emb_file(), suf_file(), 0, 0.75, true, "sig", bi_super(), true);
  NLP::Taggers::RnnTagger rnn_super5(cv_super_models + "/model.5", emb_file(), suf_file(), 0, 0.75, true, "sig", bi_super(), true);
  NLP::Taggers::RnnTagger rnn_super6(cv_super_models + "/model.6", emb_file(), suf_file(), 0, 0.75, true, "sig", bi_super(), true);
  NLP::Taggers::RnnTagger rnn_super7(cv_super_models + "/model.7", emb_file(), suf_file(), 0, 0.75, true, "sig", bi_super(), true);
  NLP::Taggers::RnnTagger rnn_super8(cv_super_models + "/model.8", emb_file(), suf_file(), 0, 0.75, true, "sig", bi_super(), true);
  NLP::Taggers::RnnTagger rnn_super9(cv_super_models + "/model.9", emb_file(), suf_file(), 0, 0.75, true, "sig", bi_super(), true);
  NLP::Taggers::RnnTagger rnn_super10(cv_super_models + "/model.10", emb_file(), suf_file(), 0, 0.75, true, "sig", bi_super(), true);

  NLP::Taggers::RnnTagger *_rnn_super = &rnn_super;;

  vector<NLP::Taggers::RnnTagger *> cv_super_vec;
  cv_super_vec.reserve(10);
  cv_super_vec.emplace_back(&rnn_super1);
  cv_super_vec.emplace_back(&rnn_super2);
  cv_super_vec.emplace_back(&rnn_super3);
  cv_super_vec.emplace_back(&rnn_super4);
  cv_super_vec.emplace_back(&rnn_super5);
  cv_super_vec.emplace_back(&rnn_super6);
  cv_super_vec.emplace_back(&rnn_super7);
  cv_super_vec.emplace_back(&rnn_super8);
  cv_super_vec.emplace_back(&rnn_super9);
  cv_super_vec.emplace_back(&rnn_super10);

  // RNN parser weights
  string parser_model_epoch = rnn_model();
  string model_dir = model_base_dir();

  std::string wx = model_dir + "/wx." + parser_model_epoch + ".mat";
  std::string wh = model_dir + "/wh." + parser_model_epoch + ".mat";
  std::string wy = model_dir + "/wy." + parser_model_epoch + ".mat";
  std::string emb = model_dir + "/emb." + parser_model_epoch + ".mat";
  std::string suf = model_dir + "/suf." + parser_model_epoch + ".mat";
  std::string cap = model_dir + "/cap." + parser_model_epoch + ".mat";

  std::string super_emb;
  std::string comp_mat;
  if (parser_cfg.use_supercat_feats())
    super_emb = model_dir + "/super_emb." + parser_model_epoch + ".mat";
  else
    super_emb = "";
  if (parser_cfg.use_phrase_emb_feats())
    comp_mat = model_dir + "/comp_mat." + parser_model_epoch + ".mat";
  else
    comp_mat = "";


  //  if (use_rnn_tagger) {
  //    rnn_tagger.load_dev_test_data();
  //    rnn_tagger.eval_super(true);
  //    if (dev_test_wiki_bio() == "dev")
  //      rnn_tagger.mtag(true);
  //    else if (dev_test_wiki_bio() == "wiki")
  //      rnn_tagger.mtag_wiki();
  //    else
  //      rnn_tagger.mtag(false);
  //  }

  if (use_lh_cache())
    parser_cfg.use_lh_cache.set_value(true);
  if (xf1_super())
    parser_cfg.xf1_super.set_value(true);


  RNNParser parser(parser_cfg, sent, cats, srbeam, config_dir(), false);
  parser.re_init_mats(wx, wh, wy, emb, suf, cap, super_emb, comp_mat);
  parser.load_lex_cat_dict(config_dir() + "/markedup_raw_ind");
  parser.load_supercat_map(model_dir + "/supercat_map." + parser_model_epoch);
  const double BETA = beta();
  size_t sent_id = 0;

  Super super(super_cfg);
  //ulong dict_cutoff = 20;

  //  std::vector<std::vector<std::vector<std::pair<std::string, double> > > > m_all_rnn_tagging_res;
  //  if (dev_test_wiki_bio() == "dev")
  //    m_all_rnn_tagging_res = rnn_tagger.m_all_res_dev;
  //  else if (dev_test_wiki_bio() == "wiki")
  //    m_all_rnn_tagging_res = rnn_tagger.m_all_res_wiki;
  //  else
  //    m_all_rnn_tagging_res = rnn_tagger.m_all_res_test;

  // xf1 training related
  double total_correct_deps = 0.0;
  double total_returned_deps = 0.0;
  double total_gold_deps = 0.0;
  parser.init_xf1_training(test_calc_f1_train());
  double total_xf1 = 0.0;
  double total_parses = 0.0;
  vector<Sentence> all_sents;
  vector<size_t> train_data_inds;

  if (restart() > 0) {
    while (reader.next(sent)) {
      all_sents.push_back(sent);
      train_data_inds.push_back(sent_id);
      ++sent_id;
    }
  }

  reader.reset();
  sent_id = 0;

  std::chrono::time_point<std::chrono::system_clock> start_t, end_t;
  if (use_rnn_tagger) {
    for (size_t e = restart(); e < epochs(); ++e) {
      start_t = std::chrono::system_clock::now();
      reader.reset();
      sent_id = 0;
      total_xf1 = 0.0;
      total_parses = 0.0;
      auto seed = chrono::high_resolution_clock::now().time_since_epoch().count();
      std::mt19937_64 engine(seed);

      if (e > 0) {
        assert(train_data_inds.size() == 39604);
        shuffle(begin(train_data_inds), end(train_data_inds), engine);
        cerr << "epoch " << e << ", total training sents: " << train_data_inds.size() << endl;
      } else {
        cerr << "epoch " << e << " " << endl;
      }
      auto sent_id_iter = train_data_inds.begin();
      while (reader.next(sent)) {
        if (e == 0) {
          all_sents.push_back(sent);
          train_data_inds.push_back(sent_id);
        } else {
          sent.reset();
          sent_id = *sent_id_iter;
          sent = all_sents[sent_id];
        }
        const size_t NWORDS = sent.words.size();
        sent.msuper.resize(NWORDS);
        sent.all_ly_vals.reserve(NWORDS);

        if (mode() == 2 && run_training && !xf1_super()) {
          // use cv super model
          size_t cv_model_number = sent_id / 3960;
          if (cv_model_number >= cv_super_vec.size()) {
            cv_model_number = cv_super_vec.size() - 1;
          }
          cerr << "using cv super model: " << cv_model_number << endl;
          _rnn_super = cv_super_vec[cv_model_number];
        }
        _rnn_super->mtag_sent(sent, BETA);

        if(NWORDS == 0) {
          log.stream << "end of input" << endl;
          break;
        } else if (NWORDS == 1 && mode() != 2) {
          printer.parsed(0, sent, BETA, 0);
          lexical(sent, out, FMT);
          out.stream << "\n";
        } else {
          const ShiftReduceHypothesis *output = 0;
          if (mode() == 0 || mode() == 2) {
            output = parser.sr_parse_beam_search_train_xf1(*_rnn_super, srbeam, sent_id, oracle(),
                                                           test_calc_f1_train(), total_xf1, total_parses,
                                                           num_threads(), bptt_graph(), run_training);
          } else if (mode() == 1) {
            if (xf1_super())
              parser.xf1_grad_check_super(*_rnn_super, srbeam, sent_id, total_parses);
            else
              parser.xf1_grad_check(*_rnn_super, srbeam, sent_id, total_parses, num_threads());
          }

          if (!output) {
            cerr << "sent: " << sent_id << " has no output" << endl;

          } else if (output && mode() == 0) {
            size_t stackSize = output->GetStackSize();
            std::vector<const SuperCat *> roots;
            vector<const SuperCat *>::const_reverse_iterator rit;
            roots.push_back(output->GetStackTopSuperCat());

            for (size_t i = 0; i < stackSize - 1; ++i) {
              output = output->GetPrvStack();
              roots.push_back(output->GetStackTopSuperCat());
            }

            for (rit = roots.rbegin(); rit < roots.rend(); ++rit) {
              printer.parsed(*rit, sent, BETA, 0);
              if (mode() == 0) {
                parser.get_deps(*rit, sent_id, total_correct_deps,
                                total_returned_deps, FMT, test_calc_f1_train());
              }
            }
            lexical(sent, out, FMT);
            out.stream << "\n";
          }
        }

        if(mode() == 0)
          total_gold_deps += parser.get_total_sent_gold_deps(sent_id, test_calc_f1_train());
        if (e == 0) ++sent_id;
        else ++sent_id_iter;
        if (NWORDS != 1)
          parser.clear_xf1();
      } // end while

      cerr << "avg parse count: " << total_parses / (double) train_data_inds.size() << endl;

      end_t = std::chrono::system_clock::now();
      std::chrono::duration<double> elapsed_seconds = end_t - start_t;
      cerr << "epoch " << e << " time: " << elapsed_seconds.count() << endl;
      cerr << "avg xf1: " << total_xf1 / (double) train_data_inds.size() << endl;

      if (mode() == 0) {
//        if (precompute()) {
//          cerr << "caching lh vals" << endl;
//          parser.precompute(10000);
//        }
        cerr << "total_correct_deps: " << total_correct_deps << endl;
        cerr << "total_deps: " << total_returned_deps << endl;
        cerr << "total_gold_deps: " << total_gold_deps << endl;
        cerr << "p: " << total_correct_deps / total_returned_deps << endl;
        cerr << "r: " << total_correct_deps / total_gold_deps << endl;
        break;
      }

      if (mode() == 2 && !xf1_super()) {
        parser.save_weights(e, xf1_model());
      }

      if (mode() == 2 && xf1_super()) {
        _rnn_super->save_xf1_model(e, xf1_model());
      }
    }

  } else {

    //    Super super(super_cfg);
    //    double beta = 0.0001;
    //    ulong dict_cutoff = 20;
    //
    //    while(reader.next(sent)) {
    //
    //      //reader_rnn_fmt.next(sent_rnn_fmt);
    //      const size_t NWORDS = sent.words.size();
    //      //assert(NWORDS == sent_rnn_fmt.words.size());
    //
    //      if(sent.words.size() == 0){
    //        log.stream << "end of input" << endl;
    //        break;
    //      }
    //
    //      if(sent.words.size() == 1) {
    //
    //        //std::cerr << "size = 1" << std::endl;
    //        super.mtag(sent, NLP::Tagger::FWDBWD, dict_cutoff, beta);
    //        sent.cats.clear();
    //        printer.parsed(0, sent, beta, dict_cutoff);
    //        lexical(sent, out, FMT);
    //        out.stream << "\n";
    //
    //      } else {
    //        super.mtag(sent, NLP::Tagger::FWDBWD, dict_cutoff, beta);
    //        const ShiftReduceHypothesis *output = parser.sr_parse_beam_search(beta, sent_id);
    //
    //        if (!output) {
    //          parser.Clear();
    //          cerr << "sent: " << sent_id << " has no output" << endl;
    //        } else {
    //
    //          size_t stackSize = output->GetStackSize();
    //          std::vector<const SuperCat *> roots;
    //          vector<const SuperCat *>::const_reverse_iterator rit;
    //          roots.push_back(output->GetStackTopSuperCat());
    //
    //          for (size_t i = 0; i < stackSize - 1; ++i) {
    //            output = output->GetPrvStack();
    //            roots.push_back(output->GetStackTopSuperCat());
    //          }
    //
    //          for (rit = roots.rbegin(); rit < roots.rend(); ++rit) {
    //            printer.parsed(*rit, sent, BETA, 0);
    //
    //          }
    //
    //          lexical(sent, out, FMT);
    //          out.stream << "\n";
    //
    //          parser.Clear();
    //        }
    //      }
    //
    //      ++sent_id;
    //    }
  }
  return 0;
}

#include "main.h"
