// C&C NLP tools
// Copyright (c) Universities of Edinburgh, Oxford and Sydney
// Copyright (c) James R. Curran
//
// This software is covered by a non-commercial use licence.
// See LICENCE.txt for the full text of the licence.
//
// If LICENCE.txt is not included in this distribution
// please email candc@it.usyd.edu.au to obtain a copy.

#include "base.h"

#include "config/config.h"

#include "pool.h"

#include "model/model.h"

#include "io/format.h"
#include "config/format.h"

#include "io/reader.h"
#include "io/writer.h"

#include "io/reader_factory.h"

#include "tagger/tagdict.h"
#include "tagger/tagsetdict.h"
#include "tagger/tagger.h"
#include "tagger/super.h"

#include "parser/parser.h"
#include "parser/decoder_factory.h"

#include "parser/printer.h"
#include "parser/print_stream.h"
#include "parser/print_factory.h"

#include "parser/integration.h"

#include "timer.h"

#include "rnn_tagger.h"
#include "xf1_trainner.h"

const char *PROGRAM_NAME = "xf1_trainner";

using namespace std;
using namespace NLP;
using namespace NLP::IO;
using namespace NLP::Taggers;
using namespace NLP::CCG;

std::vector<double> Integration::s_perceptronId;
bool Integration::s_train;

int
run(int argc, char** argv){
  std::ostringstream PREFACE;
  PREFACE << start_preface(argc, argv);

  Config::Main cfg(PROGRAM_NAME);
  Super::Config super_cfg;
  Parser::Config parser_cfg;
  Integration::Config int_cfg("int");

  Config::Alias parser_alias(cfg, SPACE, parser_cfg, "model", "parser");
  Config::Alias super_alias(cfg, super_cfg, "super", "super");

  Config::Op<std::string> input_file(cfg, SPACE, "input", "the input file to read from", STDIN);
  Config::Op<std::string> input_file_dev(cfg, SPACE, "input_dev", "the input sec00 file to read from", STDIN);

  Config::Op<IO::Format> ifmt(cfg, "ifmt", "the input file format", Format("%w|%p \n"));

  Config::Op<std::string> output_file(cfg, SPACE, "output", "the output file to write to", STDOUT);
  Config::Op<std::string> output_file_xf1(cfg, SPACE, "output_xf1", "the output file to write to", STDOUT);
	Config::Op<std::string> trace_file(cfg, "trace", "the trace file to write to", STDERR);
  Config::Op<std::string> log_file(cfg, "log", "the log file to write to", STDERR);
  Config::Op<std::string> prefix(cfg, "prefix", "the prefix of the output and log files to write to", "");

  Config::Restricted<std::string> decoder_name(cfg, SPACE, "decoder", "the parser decoder [deps, derivs, random]",
                 &DecoderFactory::check, "derivs");
  Config::Restricted<std::string> printer_name(cfg, "printer", "the parser output printer [deps, prolog, boxer, ccgbank, grs, xml, debug, js]", &PrinterFactory::check, "grs");
  Config::Op<bool> force_words(cfg, "force_words", "force the parser to print words on fails", true);

  Config::Op<bool> oracle(cfg, SPACE, "oracle", "use gold standard supertags from input", false);
  Config::Op<bool> ext_super(cfg, "ext_super", "use an external supertags from input", false);

  Config::Alias start_alias(cfg, SPACE, int_cfg.start, "start_level", "int-start_level");
  Config::Alias betas_alias(cfg, int_cfg.betas, "betas", "int-betas");
  Config::Alias dict_cutoff_alias(cfg, int_cfg.dict_cutoffs, "dict_cutoffs", "int-dict_cutoffs");

  Config::Op<std::string> model_dir(cfg, "xf1_rnn_model_output_dir", "the output dir to put the trained tagger model", "./");
  Config::Op<bool> ignore_0_f1_seq(cfg, "ignore_0_f1_seq", "ignore 0 f1 tag sequences for xf1 training", false);

  Config::Op<std::string> sr_model(cfg, SPACE, "srmodel", "perceptron model for the shiftreduce parser", STDIN);
  Config::Op<ushort> sr_beam(cfg, "srbeam", "shift reduce beam size", 16);
  Config::Op<bool> sr_train(cfg, "srtrain", "run shiftreduce training", false);
  Config::Op<ushort> xf1_epochs(cfg, "xf1_epochs", "number of epochs to train", 30);
  Config::Op<bool> xf1_test(cfg, "xf1_test", "run xf1 training test", false);

  Config::Op<ushort> bptt_steps(cfg, "bptt_steps", "number of bptt steps", 50);
  Config::Op<double> grad_check_eps(cfg, "grad_check_eps", "number of epochs to train", 0.0001);



  cfg.reg(int_cfg, SPACE);
  cfg.reg(parser_cfg, SPACE);
  cfg.reg(super_cfg, SPACE);

  cfg.parse(argc, argv);
  cfg.check();

  if(prefix() != ""){
    output_file.set_value(prefix() + ".out");
    log_file.set_value(prefix() + ".log");
  }

  if(printer_name() == "grs" && !parser_cfg.alt_markedup.has_changed())
    parser_cfg.alt_markedup.set_value(true);

  std::string model = sr_model();
  ifstream model_file(model.c_str());
  const bool train = sr_train();
  Integration::s_train = train; //!!!
  const size_t srbeam = sr_beam();
  const size_t epochs = xf1_epochs();

  Sentence sent;
  Sentence sent1;

  ReaderFactory reader(input_file(), ifmt());

  Integration integration(int_cfg, super_cfg, parser_cfg);
  Integration integration1(int_cfg, super_cfg, parser_cfg, sent1, train, srbeam);
  integration1.LoadModel(model);
  //Integration integration1(int_cfg, super_cfg, parser_cfg);

  StreamPrinter::Format FMT = StreamPrinter::FMT_DEV |
    StreamPrinter::FMT_PRINT_UNARY;

  if(force_words())
    FMT |= StreamPrinter::FMT_FORCE_WORDS;

  IO::Output out(output_file());
  IO::Log log(log_file());
	IO::Output trace(trace_file());
  DepsPrinter printer(integration.cats, FMT, out, log);
  DepsPrinter printer1(integration1.cats, FMT, out, log);
  //PrinterFactory printer(printer_name(), out, log, integration.cats, FMT);
  //PrinterFactory printer1(printer_name(), out, log, integration1.cats, FMT);

  //printer.header(PREFACE.str());

  DecoderFactory decoder(decoder_name());

  Stopwatch watch;

//
//  RnnTagger rnn_tagger_test(200, 23085, 426, 7, 3, 619, xf1_test_wx_mat, xf1_test_wh_mat,
//  		                      xf1_test_wy_mat, xf1_test_emb_mat, xf1_test_suf_mat, xf1_test_cap_mat);
//  rnn_tagger_test.load_dev_test_data();
//  rnn_tagger_test.load_ind2rawtag_map();
//  rnn_tagger_test.eval_super(true, false);

//  ReaderFactory reader_dev(input_file_dev(), ifmt());
//
//  XF1Trainer xf1_trainer(200, 23085, 426, 7, 3, 619, 9,
//  		50, 5, 5, 0.01, ignore_0_f1_seq(),
//			wx_mat, wh_mat, wy_mat,
//			emb_mat, suf_mat, cap_mat, 100,
//			parser_cfg, 2, printer, srbeam, model,
//			Integration::s_perceptronId);
//
//  xf1_trainer.init_xf1_eval();
//  reader_dev.reset();
//  xf1_trainer.eval_dev_xf1(reader_dev, FMT);

//  int i = -1;
//  while(reader_dev.next(sent1)){
//  	++i;
//  	if(oracle())
//  		sent1.copy_multi('s', 'S');
//
//  	if(sent1.words.size() == 0){
//  		log.stream << "end of input" << endl;
//  		break;
//  	}


  //}

//  rnn_tagger.mtag(true);
//  //rnn_tagger.Test_kbest();
//
//  ReaderFactory reader_dev(input_file_dev(), ifmt());
//
//  bool USE_SUPER = not (oracle() or ext_super());
//  int i = -1;
//  while(reader_dev.next(sent)){
//    ++i;
//    if(oracle())
//      sent.copy_multi('s', 'S');
//
//    if(sent.words.size() == 0){
//      log.stream << "end of input" << endl;
//      break;
//    }
//
//    integration.parse_rnn(sent, decoder, printer, rnn_tagger.m_all_res_dev[i]);
//  }
//
//  return 0;

  std::string wx_mat = "./wx.13.mat";
  std::string wh_mat = "./wh.13.mat";
  std::string wy_mat = "./wy.13.mat";
  std::string emb_mat = "./emb.13.mat";
  std::string suf_mat = "./suf.13.mat";
  std::string cap_mat = "./cap.13.mat";

//  std::string xf1_test_wx_mat;
//  std::string xf1_test_wh_mat;
//  std::string xf1_test_wy_mat;
//  std::string xf1_test_emb_mat;
//  std::string xf1_test_suf_mat;
//  std::string xf1_test_cap_mat;

  RnnTagger rnn_tagger(200, 23085, 426, 7, 3, 619,
                       wx_mat, wh_mat, wy_mat, emb_mat, suf_mat, cap_mat,
                       "./label_ind.txt");
  rnn_tagger.load_dev_test_data();
  //rnn_tagger.load_ind2rawtag_map("./label_ind.txt");
//  rnn_tagger.eval_super(true);
//  rnn_tagger.mtag(true);

  ReaderFactory reader_dev(input_file_dev(), ifmt());
  const size_t bptt_step = bptt_steps();
  double eps = grad_check_eps();

  XF1Trainer xf1_trainer(200, 23085, 426, 7, 3, 619, 9,
                         50, 5, 5, bptt_step, 0.01, ignore_0_f1_seq(),
                         wx_mat, wh_mat, wy_mat, emb_mat,
                         suf_mat, cap_mat, 100,
                         parser_cfg, 2, printer, srbeam, model,
												 Integration::s_perceptronId, eps, "./label_ind.txt");
  xf1_trainer.load_skip_ids("./skipped_ids_sr");
  //xf1_trainer.init_training("./train_words_intersection_sr.txt", "./train_labels_intersection_sr.txt");
  xf1_trainer.init_xf1_eval();
  for (size_t e = 0; e < 20; ++e) {

    cerr << "xf1 training" << " epoch: " << e << endl;
    xf1_trainer.eval_dev_1best_rescore(reader_dev, FMT);

//    xf1_test_wx_mat = "./xf1_results_sr/xf1_wx." + to_string(e) + ".mat";
//    xf1_test_wh_mat = "./xf1_results_sr/xf1_wh." + to_string(e) + ".mat";
//    xf1_test_wy_mat = "./xf1_results_sr/xf1_wy." + to_string(e) + ".mat";
//    xf1_test_emb_mat = "./xf1_results_sr/xf1_emb." + to_string(e) + ".mat";
//    xf1_test_suf_mat = "./xf1_results_sr/xf1_suf." + to_string(e) + ".mat";
//    xf1_test_cap_mat = "./xf1_results_sr/xf1_cap." + to_string(e) + ".mat";

//    xf1_trainer.train(reader, FMT, e);
////    xf1_trainer.save_model(model_dir.value, e);
//
//    string wx_mat_new = model_dir.value + "/xf1_wx." + to_string(e) + ".mat";
//    string wh_mat_new = model_dir.value + "/xf1_wh." + to_string(e) + ".mat";
//    string wy_mat_new = model_dir.value + "/xf1_wy." + to_string(e) + ".mat";
//    string emb_mat_new = model_dir.value + "/xf1_emb." + to_string(e) + ".mat";
//    string suf_mat_new = model_dir.value + "/xf1_suf." + to_string(e) + ".mat";
//    string cap_mat_new = model_dir.value + "/xf1_cap." + to_string(e) + ".mat";
//
//    rnn_tagger.re_init_mats(wx_mat_new, wh_mat_new, wy_mat_new,
//    		                    emb_mat_new, suf_mat_new, cap_mat_new);
//
////    rnn_tagger.re_init_mats(wx_mat, wh_mat, wy_mat,
////                            emb_mat, suf_mat, cap_mat);
//    const bool use_softmax = false;
//    rnn_tagger.eval_super(true, use_softmax);
//    rnn_tagger.mtag(true);

//    reader_dev.reset();
//    out.fstream.close();
//    out.fstream.open(output_file().c_str(), std::fstream::out | std::fstream::trunc);
//
//    int i = -1;
//    while(reader_dev.next(sent1)){
//      ++i;
//      if(oracle())
//        sent1.copy_multi('s', 'S');
//
//      if(sent1.words.size() == 0){
//        log.stream << "end of input" << endl;
//        break;
//      }
//      integration1.ShiftReduceParseRnn(false, sent1, decoder, printer1, true, out, trace, FMT,
//      		                             rnn_tagger.m_all_res_dev[i]);
//
//      //if (xf1_test() && i > 5) break;
//    }
//
//    out.fstream.close();
//    string newname = output_file_xf1.value + to_string(e);
//    std::ifstream  src(output_file().c_str(), std::ios::binary);
//    std::ofstream  dst(newname.c_str(),   std::ios::binary);
//    dst << src.rdbuf();
//    dst.close();
//    src.close();
//  }
//
//  std::fstream self_stat("/proc/self/status", std::ios::in);
//  std::string line;
//  while (getline(self_stat, line)) {
//    std::cerr << line << '\n';
 }


  return 0;

//  bool USE_SUPER = not (oracle() or ext_super());
//  int i = -1;
//  while(reader.next(sent)){
//    ++i;
//    if(oracle())
//      sent.copy_multi('s', 'S');
//
//    if(sent.words.size() == 0){
//      log.stream << "end of input" << endl;
//      break;
//    }
//
//    //integration.parse_rnn(sent, decoder, printer, rnn_tagger.m_all_res_dev[i]);
//  }
//  printer.footer();

//  double seconds = watch.stop();
//  double sentence_speed = integration.nsentences / seconds;
//  double word_speed = integration.nwords / seconds;
//
//  log.stream << endl;
//
//  log.stream << "use super = " << USE_SUPER << endl;
//  log.stream << "beta levels = " << integration.BETAS << endl;
//  log.stream << "dict cutoffs = " << integration.DICT_CUTOFFS << endl;
//  log.stream << "start level = " << integration.START << endl;
//
//  log.stream << "nwords = " << integration.nwords << endl;
//  log.stream << "nsentences = " << integration.nsentences << endl;
//
//  log.stream << "nexceptions = " << integration.nexceptions << endl;
//
//  log.stream << "ncombines = " << integration.global_stats.ncombines << endl;
//  log.stream << "ncombines_zeros = " << integration.global_stats.ncombines_zeros << endl;
//  log.stream << "ncombines_reduced = " << integration.global_stats.ncombines_reduced << endl;
//  log.stream << "ncombines_rejected = " << integration.global_stats.ncombines_rejected << endl;
//
//  ulong nfail_bound = integration.nfail_nospan + integration.nfail_explode;
//  ulong nfail_back = integration.nfail_nospan_explode + integration.nfail_explode_nospan;
//  ulong nfail = nfail_bound + nfail_back;
//
//  log.stream << "nfailures = " << nfail << endl;
//  log.stream << "  run out of levels = " << nfail_bound << endl;
//  log.stream << "    nospan = " << integration.nfail_nospan << endl;
//  log.stream << "    explode = " << integration.nfail_explode << endl;
//  log.stream << "  backtrack on levels = " << nfail_back << endl;
//  log.stream << "    nospan/explode = " << integration.nfail_nospan_explode << endl;
//  log.stream << "    explode/nospan = " << integration.nfail_explode_nospan << endl;
//
//  for(int i = 0; i < static_cast<int>(integration.BETAS.size()); ++i){
//    log.stream << "nsuccess " << i << ' ' << setw(6) << integration.BETAS[i];
//    log.stream << ' ' << setw(6) << integration.nsuccesses[i];
//    if(i == integration.START)
//      log.stream << " <--";
//    log.stream << endl;
//  }
//
//  log.stream << "total parsing time = " << seconds << " seconds\n";
//  log.stream << "sentence speed = " << sentence_speed << " sentences/second\n";
//  log.stream << "word speed = " << word_speed << " words/second\n";
//
//  return 0;
}

#include "main.h"
