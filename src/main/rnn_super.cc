const char *PROGRAM_NAME = "rnn_super";

#include "base.h"
#include "main.h"

#include "config/config.h"
#include "io/format.h"
#include "config/format.h"

#include "io/reader.h"
#include "io/reader_factory.h"
#include "io/writer.h"
#include "io/writer_factory.h"

#include "model/model.h"
#include "parser/integration.h"

#include "timer.h"
#include "rnn_tagger.h"

using namespace std;
using namespace NLP;
using namespace NLP::IO;
using namespace NLP::Taggers;
using namespace NLP::CCG;

// these two extra lines are needed here
// for legacy reasons, it's a hack, remove later
std::vector<double> Integration::s_perceptronId;
bool Integration::s_train;

int
run(int argc, char** argv){

  Config::Main cfg(PROGRAM_NAME);
//  Super::Config super_cfg;
//  Config::Alias super_alias(cfg, super_cfg, "super", "super");
  Config::Op<bool> train(cfg, "train", "training (ture/false)", false);
  Config::Op<bool> bi(cfg, "bi", "bidirectional", true);
  Config::Op<std::string> input_file(cfg, NLP::Config::SPACE, "input", "the input file to read from", STDIN);
  Config::Op<std::string> input_file_dev(cfg, NLP::Config::SPACE, "input_dev", "the input file to read from", STDIN);
  Config::Op<IO::Format> ifmt(cfg, "ifmt", "the input file format", Format("%w|%p|%s \n"));
  Config::Op<std::string> model(cfg, "model", "the dir to load/save the model", "./");
  Config::Op<std::string> emb_file(cfg, "emb", "file name of the embeddings file (*UNKONWN* changed to ***PADDING***, otherwise untouched)", "emb_turian_50_scaled");
  Config::Op<std::string> suf_file(cfg, "suf", "file name of the suffix file", "suf_str_ind_map.txt");
  Config::Op<std::size_t> de(cfg, "de", "embedding dimension", 50);
  Config::Op<std::size_t> bs(cfg, "bptt_bs", "bptt steps", 9);
  Config::Op<std::size_t> epochs(cfg, "epochs", "epochs", 50);
  Config::Op<std::string> act(cfg, "act", "sig or relu", "sig");
  Config::Op<double> clip(cfg, "clip", "gradient max abs (0.0 == disable)", 0.0);
  Config::Op<bool> dropout(cfg, "dropout", "use dropout", true);
  Config::Op<bool> verbose(cfg, "verbose", "logging output", true);
  Config::Op<double> dropout_prob(cfg, "dropout_prob", "drop out success prob", 0.75);
  Config::Op<double> lr(cfg, "lr", "learning rate", 0.0025);
  Config::Op<double> nh(cfg, "nh", "hidden layer size", 200);
  Config::Op<size_t> output_fmt(cfg, "output_fmt", "0: no output (run evaluation) 1: singe 2: multi", 0);
  Config::Op<double> beta(cfg, "beta", "beta cutoff", 0.0);
  Config::Op<bool> use_rnn_tagdict(cfg, "use_rnn_tagdict", "use_rnn_tagdict", false);
  Config::Op<bool> use_candc_tagdict(cfg, "use_candc_tagdict", "use_candc_tagdict", false);
  Config::Op<size_t> candc_dict_cutoff(cfg, "candc_dict_cutoff", "candc tagdict cutoff", 20);
  Config::Op<bool> candc_pos_backoff(cfg, "candc_pos_backoff", "use pos tagdict backoff when using c&c super tagdict", false);

  cfg.parse(argc, argv);
  cfg.check();

  if (!train()) {

    std::string wx_mat;
    std::string wh_mat;
    std::string wy_mat;
    std::string emb_mat;
    std::string suf_mat;
    std::string cap_mat;


    //########## evaluating all models
//    for (size_t i = 0; i < 30; ++i) {
//      cerr << i << endl;
//      wx_mat = model() + "/wx." + to_string(i) + ".mat";
//      wh_mat = model() + "/wh." + to_string(i) + ".mat";
//      wy_mat = model() + "/wy." + to_string(i) + ".mat";
//      emb_mat = model() + "/emb." + to_string(i) + ".mat";
//      suf_mat = model() + "/suffix." + to_string(i) + ".mat";
//      cap_mat = model() + "/cap." + to_string(i) + ".mat";
//
//      rnn_super.re_init_mats(wx_mat, wh_mat, wy_mat, emb_mat, suf_mat, cap_mat);
//      ReaderFactory reader(input_file(), ifmt());
//      rnn_super.tag_file(reader, true);
//    }

// for reference
//    RnnTagger::RnnTagger(const string &model_dir,
//        const string &all_untouched_embeddings,
//        const string &suf_str_ind_file,
//        size_t output_fmt,
//        double dropout_success_prob,
//        bool dropout,
//        bool use_candc_tag_dicts,
//        bool candc_tagdict_cutoff,
//        bool candc_tagdict_pos_backoff,
//        Super::Config &super_cfg)

    //########## testing

    RnnTagger rnn_super(model(), emb_file(), suf_file(), output_fmt(), dropout_prob(), dropout(), act(), bi(), true);
    ReaderFactory reader(input_file(), ifmt());
    ReaderFactory reader_dev(input_file_dev(), ifmt());

    if (output_fmt() == 0)
      rnn_super.eval_mtag(reader_dev);
    else if (output_fmt() == 1)
      rnn_super.tag_file(reader, false);
    else if (output_fmt() == 2)
      rnn_super.mtag_file(reader, beta());

  } else {

    //########## training
    ReaderFactory reader(input_file(), ifmt());
    ReaderFactory reader_dev(input_file_dev(), ifmt());
    string emb_file_name = model() + "/" + emb_file();
    string suf_file_name = model() + "/" + suf_file();

    RnnTagger rnn_super(de(), 5, 5, 7, nh(), bs(), emb_file_name, suf_file_name, model(),
                        true, epochs(), dropout(), dropout_prob(), verbose(), lr(), clip(), act(), bi());
    cerr << "training started\n";
    //rnn_super.grad_check_bi(reader, reader_dev);
    rnn_super.train(reader, reader_dev);
  }

  return 0;

}

