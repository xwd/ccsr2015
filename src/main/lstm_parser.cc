// C&C NLP tools
// Copyright (c) Universities of Edinburgh, Oxford and Sydney
// Copyright (c) James R. Curran
//
// This software is covered by a non-commercial use licence.
// See LICENCE.txt for the full text of the licence.
//
// If LICENCE.txt is not included in this distribution
// please email candc@it.usyd.edu.au to obtain a copy.

#include "base.h"

#include "config/config.h"

#include "pool.h"

#include "model/model.h"

#include "io/format.h"
#include "config/format.h"

#include "io/reader.h"
#include "io/writer.h"

#include "io/reader_factory.h"

#include "tagger/tagdict.h"
#include "tagger/tagsetdict.h"
#include "tagger/tagger.h"
#include "tagger/super.h"

#include "lstm_parser/lstm_parser.h"
#include "parser/decoder_factory.h"

#include "parser/printer.h"
#include "parser/print_stream.h"
#include "parser/print_factory.h"
#include "parser/integration.h"

#include "rnn_tagger.h"
#include "lstm_tagger.h"
#include "parser/ShiftReduceHypothesis.h"

#include "timer.h"

#include "lstm_parser/lstm_parser.h"

#include "cnn/cnn.h"
#include "cnn/training.h"
#include "cnn/timing.h"

#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>


const char *PROGRAM_NAME = "lstm_parser";

using namespace std;
using namespace NLP;
using namespace NLP::Tagger;
using namespace NLP::CCG;
using namespace NLP::Taggers;


const static int FMT_WORDS = 1 << 0;
const static int FMT_LEMMA = 1 << 1;
const static int FMT_POS = 1 << 2;
const static int FMT_CHUNK = 1 << 3;
const static int FMT_NER = 1 << 4;
const static int FMT_SUPER = 1 << 5;
const static int FMT_CAT = 1 << 6;

// these two extra lines are needed here
// for legacy reasons, it's a hack, remove later
std::vector<double> Integration::s_perceptronId;
bool Integration::s_train;

void lexical(Sentence &sent, IO::Output &out, const int FORMAT){
  const ulong NWORDS = sent.words.size();

  if((FORMAT & FMT_LEMMA) && sent.lemmas.size() != NWORDS)
    throw NLP::Exception("not enough lemmas for the sentence");

  // don't need to check POS tags since they must already exist

  if((FORMAT & FMT_CHUNK) && sent.chunks.size() != NWORDS)
    throw NLP::Exception("not enough chunks for the sentence");

  if((FORMAT & FMT_NER) && sent.entities.size() != NWORDS)
    throw NLP::Exception("not enough named entities for the sentence");

  const bool HAS_CORRECT_STAG = sent.cats.size();

  if(FORMAT & FMT_WORDS){
    out.stream << "<c>";
    for(ulong i = 0; i < NWORDS; ++i){
      out.stream << ' ' << sent.words[i];
      if(FORMAT & FMT_LEMMA)
        out.stream << '|' << sent.lemmas[i];
      if(FORMAT & FMT_POS)
        out.stream << '|' << sent.pos[i];
      if(FORMAT & FMT_CHUNK)
        out.stream << '|' << sent.chunks[i];
      if(FORMAT & FMT_NER)
        out.stream << '|' << sent.entities[i];
      if(FORMAT & FMT_SUPER){
        out.stream << '|';
        if(HAS_CORRECT_STAG)
          sent.cats[i]->out_novar_noX(out.stream, false);
        else
          out.stream << sent.msuper[i][0].raw;
      }
      if(FORMAT & FMT_CAT){
        out.stream << '|';
        if(HAS_CORRECT_STAG)
          sent.cats[i]->out(out.stream);
        else
          out.stream << "none";
      }
    }
    out.stream << '\n';
  }
}

void load_cv0001_supercats(const std::string &filename,
                           std::vector<std::vector<std::vector<std::string>* >* > &supercatVec) {
  std::cerr << "load_cv0001_supercats start "<< filename << std::endl;
  ifstream in(filename.c_str());
  if(!in)
    throw NLP::IOException("could not open supercats file", filename);
  std::vector<std::vector<std::string>* >* sent =  new std::vector<std::vector<std::string>* >;
  size_t sentcount = 0;
  size_t catcount = 0;
  while (in) {
    string s;
    if (!getline(in, s)) break;
    if (s.empty())
    {
      if (sent->size() > 0)
        supercatVec.push_back(sent);
      ++sentcount;
      sent = new std::vector<std::vector<std::string>* >;
      continue;
    }

    istringstream ss(s);
    size_t count = 0;
    std::vector<std::string>* word = new vector<string>;
    while (ss)
    {
      string s;
      if (!getline(ss, s, ' ')) break;
      ++count;
      if (count <= 2) continue;
      //const Cat *cat = cats.markedup[s];
      //if (!cat)
      //{
      //  std::cerr << "sent ID: " << sentcount + 1 << " " << s << " not in markedup" << std::endl;
      //  continue;
      // }
      ++catcount;
      word->push_back(s);
    }
    assert(word->size() > 0);
    sent->push_back(word);
  }
  std::cerr << "total super: " << catcount << " total sent: " << sentcount << std::endl;
  std::cerr << "done" << std::endl;
}

void decode(const unsigned mode,
            IO::ReaderFactory &reader,
            Sentence &sent,
            LSTMParser &parser,
            const size_t &srbeam,
            const bool oracle, const bool dev_or_train,
            NLP::Taggers::RnnTagger rnn_super,
            const float BETA,
            IO::Output &out, const int FMT,
            PrinterFactory &printer,
            cnn::LSTMBuilder &t_l2rbuilder,
            cnn::LSTMBuilder &t_r2lbuilder,
            float &total_xf1, float &total_parses,
            float &total_correct_deps, float &total_returned_deps, float &total_gold_deps,
            float &acts,
            float &correct,
            bool use_rnn_super,
            bool test_lstm_tagger) {
  reader.reset();
  parser.change_mode(mode);
  size_t sent_id = 0;
  std::chrono::time_point<std::chrono::system_clock> start_t, end_t;
  start_t = std::chrono::system_clock::now();
  while(reader.next(sent)) {
    ComputationGraph cg;
    const size_t NWORDS = sent.words.size();
    sent.msuper.resize(NWORDS);

    if (use_rnn_super)
     rnn_super.mtag_sent(sent, BETA);
    else
     parser.lstm_tag(t_l2rbuilder, t_r2lbuilder, cg, BETA);

    if(NWORDS == 0) {
      //log.stream << "end of input" << endl;
      break;
    } else if (NWORDS == 1 && !test_lstm_tagger) {
      sent.cats.clear();
      printer.parsed(0, sent, BETA, 0);
      lexical(sent, out, FMT);
      out.stream << "\n";
    } else {
      const ShiftReduceHypothesis *output = parser.sr_parse_beam_search_xf1(t_l2rbuilder, t_r2lbuilder, cg, sent_id, BETA, total_xf1, total_parses, acts, correct, oracle);
      if (!output) {
        parser.Clear();
        cerr << "sent: " << sent_id << " has no output" << endl;
      } else {
        size_t stackSize = output->GetStackSize();
        std::vector<const SuperCat *> roots;
        vector<const SuperCat *>::const_reverse_iterator rit;
        roots.push_back(output->GetStackTopSuperCat());

        for (size_t i = 0; i < stackSize - 1; ++i) {
          output = output->GetPrvStack();
          roots.push_back(output->GetStackTopSuperCat());
        }

        for (rit = roots.rbegin(); rit < roots.rend(); ++rit) {
          printer.parsed(*rit, sent, BETA, 0);
          if (mode == 1) parser.get_deps(*rit, sent_id, total_correct_deps, total_returned_deps, FMT, dev_or_train);
        }
        if (mode == 1) total_gold_deps += parser.get_total_sent_gold_deps(sent_id, dev_or_train);
        lexical(sent, out, FMT);
        out.stream << "\n";
        out.stream.flush();
        parser.Clear();
      }
    }
    ++sent_id;
    sent.reset();
  } // end while
  end_t = std::chrono::system_clock::now();
  std::chrono::duration<double> elapsed_seconds = end_t - start_t;
  cerr << " time: " << elapsed_seconds.count() << endl;
  if (test_lstm_tagger)
    parser.calc_tagging_acc();
}

int
run(int argc, char** argv){
  std::ostringstream PREFACE;
  PREFACE << start_preface(argc, argv);

  Config::Main cfg(PROGRAM_NAME);
  Super::Config super_cfg;
  Integration::Config int_cfg("int");
  LSTMParser::LSTMConfig parser_cfg;
  Config::Alias parser_alias(cfg, SPACE, parser_cfg, "model", "parser");
  //Config::Alias super_alias(cfg, super_cfg, "super", "super");

  Config::Op<unsigned> de(cfg, "de", "word embedding size", 100);
  Config::Op<unsigned> dp(cfg, "dp", "pos embedding size", 50);
  Config::Op<unsigned> dsuper(cfg, "dsuper", "supercat embedding size", 50);
  Config::Op<unsigned> daction(cfg, "daction", "action embedding size", 50);
  Config::Op<unsigned> nh(cfg, "nh", "lstm hidden dimension", 128);
  Config::Op<unsigned> th(cfg, "th", "target hidden dimension", 80);
  Config::Op<unsigned> layers(cfg, "layers", "number layers for each lstm", 1);
  Config::Op<bool> use_super(cfg, "use_super", "use super embeddings", false);
  Config::Op<bool> use_act(cfg, "use_act", "use action embeddings", false);
  Config::Op<bool> use_pos(cfg, "use_pos", "use pos embeddings", false);
  Config::Op<bool> use_biqueue(cfg, "use_biqueue", "use bidirectional queue encoding", false);
  Config::Op<bool> use_comp(cfg, "use_comp", "compose word, super, action and pos", false);
  Config::Op<bool> feeding(cfg, "feeding", "feed hidden states from pos to action to super to word stacks", false);
  Config::Op<bool> att(cfg, "att", "use attention", false);
  Config::Op<bool> att_all(cfg, "att_all", "attention on all features", false);
  Config::Op<bool> att_prev_context(cfg, "att_prev_context", "attention on all features and previous context", false);
  Config::Op<double> pdrop(cfg, "pdrop", "lstm dropout p (0.0 == disable)", 0.3);
  Config::Op<bool> load_pretrained(cfg, "load_pretrained", "greedy training from scratch", true);
  Config::Op<double> eta(cfg, "eta", "initial learning rate", 0.1);
  Config::Op<unsigned> delay_decay(cfg, "delay_decay", "delay eta until after the specified epoch", 10);
  Config::Op<bool> xf1_dropout(cfg, "xf1_dropout", "dropout when doing xF1 training", false);

  Config::Op<unsigned> mode(cfg, "mode", "0: plain decode; 1: decode + f1 and xf1 calc; 2: greedy training; 3: xf1 training", 0);
  Config::Op<bool> use_rnn_super(cfg, "use_rnn_super", "use rnn or lstm tagger (true = rnn, false = lstm)", false);
  Config::Op<bool> integrate(cfg, "integrate", "integrated supertagging and parsing training (!not working!)", false);
  Config::Op<bool> test_lstm_tagger(cfg, "test_lstm_tagger", "for debugging only: test lstm tagger decoding", false);

  Config::Op<std::string> config_dir(cfg, SPACE, "config_dir", "the dir storing all config files for rnn_parser", "./rnn_parser_config_files");
  Config::Op<ushort> srbeam(cfg, "srbeam", "shift reduce beam size", 1);
  Config::Op<string> model_dir(cfg, "model_dir", "the dir to load/save model from", "./");
  Config::Op<unsigned> model_epoch(cfg, "model_epoch", "the model number x in model.x", 1);
  Config::Op<string> lstm_tagger_model(cfg, "lstm_super_model", "lstm supertagger model file", "");
  Config::Op<string> model_dir_xf1(cfg, "model_dir_xf1", "the dir to save xf1_model to", "./");

  Config::Op<std::string> input_file(cfg, SPACE, "input", "the input file to read from", "");
  Config::Op<std::string> input_dev(cfg, SPACE, "input_dev", "the dev input file", "");
  Config::Op<IO::Format> ifmt(cfg, "ifmt", "the input file format", Format("%w|%p|%s \n"));
  Config::Op<IO::Format> ifmt_dev(cfg, "ifmt_dev", "the dev input file format", Format("%w|%p \n"));
  Config::Op<std::string> output_file(cfg, SPACE, "output", "the output file to write to", STDOUT);
  Config::Op<bool> force_words(cfg, "force_words", "force the parser to print words on fails", true);
  Config::Op<std::string> prefix(cfg, "prefix", "the prefix of the output and log files to write to", "");
  Config::Restricted<std::string> printer_name(cfg, "printer", "the parser output printer [prolog, deps, grs]", &PrinterFactory::check, "deps");

  Config::Op<bool> test_calc_f1_train(cfg, "use_train_or_dev_data", "run oracle on 0221 (true)/ 00 (false)", false);
  Config::Op<bool> oracle(cfg, "oracle", "oracle decoding", false);
  //Config::Op<bool> xf1_super(cfg, "xf1_super", "train xf1 super (not implemented)", false);

  Config::Op<float> beta(cfg, "beta", "beam threshold for the supertagger", 0.06);
  Config::Op<std::string> cv_supercat_file_rnn(cfg, "cv_supercat_file_rnn", "cross validated multisuper results with rnn", "wsj02-21.cv.0.00025.pos+rnn_super.sfef");
  Config::Op<std::string> cv_supercat_file_lstm(cfg, "cv_supercat_file_lstm", "cross validated multisuper results with lstm", "wsj02-21.cv.0.00001.pos+lstm_super.sfef");
  Config::Op<std::string> rnn_super_model(cfg, "rnn_super_model", "rnn super model", "rnn_parser_config_files/rnn_super_model_bi.2");
//  /Config::Op<bool> bi_super(cfg, "bi_super", "use bidirectional rnn super", true); // for rnn super only
  Config::Op<std::string> rnn_super_emb_file(cfg, "rnn_super_emb", "file name of the embeddings file (*UNKONWN* changed to ***PADDING***, otherwise untouched)", "emb_turian_50_scaled");
  Config::Op<std::string> rnn_super_suf_file(cfg, "rnn_super_suf", "file name of the suffix file", "suf_str_ind_map.txt");

  Config::Op<std::string> log_file(cfg, "log", "the log file to write to", STDERR);

  //Config::Op<bool> use_lh_cache(cfg, "use_lh_cache", "use cached lh vals", false);
  //Config::Op<bool> bptt_graph(cfg, "bptt_graph", "bptt over dag", true);
  //Config::Op<bool> use_rnn_supertagger(cfg, "use_rnn_tagger", "use rnn tagger to do supertagging", true);
  //Config::Op<bool> bi_super(cfg, "bi_super", "use bidirectional rnn super", false);
  //Config::Op<std::string> suf_file(cfg, "suf", "file name of the suffix file", "");
  //Config::Op<double> xf1_super_lr(cfg, "xf1_super_lr", "learning rate for xf1_super training", 0.0025);
  //Config::Op<string> model(cfg, "model", "the dir to save weights", "./");
  //Config::Op<string> rnn_model(cfg, "rnn_parser_model", "specify the weights to use", "0");
  //Config::Op<size_t> num_threads(cfg, "num_threads", "number of threads", 1);
  //Config::Op<size_t> epochs(cfg, "epochs", "number of epochs", 60);
  //Config::Op<size_t> restart(cfg, "restart", "restart training (give an epoch number to restart from, 0 means from scratch)", 0);

  cfg.reg(int_cfg, SPACE);
  cfg.reg(parser_cfg, SPACE);
  cfg.reg(super_cfg, SPACE);
  cfg.parse(argc, argv);
  cfg.check();

  if(prefix() != ""){
    output_file.set_value(prefix() + ".out");
   // log_file.set_value(prefix() + ".log");
  }

  if(printer_name() == "grs" && !parser_cfg.alt_markedup.has_changed())
    parser_cfg.alt_markedup.set_value(true);

  bool load_p = load_pretrained();
  if (mode() != 2) load_p = false;

  parser_cfg.de.set_value(de());
  parser_cfg.dp.set_value(dp());
  parser_cfg.dsuper.set_value(dsuper());
  parser_cfg.daction.set_value(daction());
  parser_cfg.nh.set_value(nh());
  parser_cfg.th.set_value(th());
  parser_cfg.layers.set_value(layers());
  parser_cfg.use_super.set_value(use_super());
  parser_cfg.use_act.set_value(use_act());
  parser_cfg.use_pos.set_value(use_pos());
  parser_cfg.use_biqueue.set_value(use_biqueue());
  parser_cfg.use_comp.set_value(use_comp());
  parser_cfg.feeding.set_value(feeding());
  parser_cfg.att.set_value(att());
  parser_cfg.att_all.set_value(att_all());
  parser_cfg.att_prev_context.set_value(att_prev_context());
  parser_cfg.use_rnn_super.set_value(use_rnn_super());
  parser_cfg.pdrop.set_value(pdrop());
  parser_cfg.load_pretrained.set_value(load_p);
  parser_cfg.xf1_dropout.set_value(xf1_dropout());

  if (att()) {
    cerr << "attention turned on, turning on super, action, pos and comp too." << endl;
    parser_cfg.use_comp.set_value(true);
    parser_cfg.use_super.set_value(true);
    parser_cfg.use_act.set_value(true);
    parser_cfg.use_pos.set_value(true);
  }

  if (att_all()) {
    cerr << "attention on all features, turning off comp and att, turning on super, action and pos." << endl;
    parser_cfg.use_comp.set_value(false);
    parser_cfg.att.set_value(false);
    parser_cfg.att_all.set_value(true);
    parser_cfg.use_super.set_value(true);
    parser_cfg.use_act.set_value(true);
    parser_cfg.use_pos.set_value(true);
  }

  if (att_prev_context()) {
    cerr << "attention on all features and previous context, turning off comp and att, turning on super, action, pos and att_all." << endl;
    parser_cfg.use_comp.set_value(false);
    parser_cfg.att.set_value(false);
    parser_cfg.att_all.set_value(true);
    parser_cfg.att_prev_context.set_value(true);
    parser_cfg.use_super.set_value(true);
    parser_cfg.use_act.set_value(true);
    parser_cfg.use_pos.set_value(true);
  }

  if (!use_rnn_super()) assert(lstm_tagger_model() != "");

  if (oracle()) assert(mode() == 1);  // model() must == 1 for oracel decoding
  bool _oracle = oracle();
  if (mode() != 1) _oracle = false;

  Sentence sent;
  ReaderFactory reader(input_file(), ifmt());
  ReaderFactory reader_dev(input_dev(), ifmt_dev());
  Categories cats(parser_cfg.cats(), parser_cfg.markedup(), parser_cfg.alt_markedup());
  //Integration integration(int_cfg, super_cfg, parser_cfg, sent, true, 0);
  StreamPrinter::Format FMT = StreamPrinter::FMT_DEV | StreamPrinter::FMT_PRINT_UNARY;
  if(force_words()) FMT |= StreamPrinter::FMT_FORCE_WORDS;

  IO::Output out(output_file());
  IO::Log log(log_file());
  //IO::Output trace(trace_file());
  PrinterFactory printer(printer_name(), out, log, cats, FMT);
  printer.header(PREFACE.str());

  NLP::Taggers::RnnTagger rnn_super(rnn_super_model(), rnn_super_emb_file(), rnn_super_suf_file(), 0, 0.75, true, "sig", true, true);

  cnn::Initialize(1024);
  cnn::Model model;
  cnn::Model t_model;

  const unsigned pretrained_dim = 100;
  const unsigned suf_dim = 20;
  const unsigned dwin = 1;
  const unsigned hidden_dim = 256;
  const unsigned tlayers = 1;

  cnn::Model &tagger_model = integrate() ? model : t_model;
  cnn::LSTMBuilder t_l2rbuilder(tlayers, (pretrained_dim + suf_dim)*dwin, hidden_dim, &tagger_model);
  cnn::LSTMBuilder t_r2lbuilder(tlayers, (pretrained_dim + suf_dim)*dwin, hidden_dim, &tagger_model);

  // todo refactor here, get rid of these null pts
  LSTMParser parser(model, t_model, parser_cfg, sent, cats, srbeam(), config_dir(), mode(), integrate());
  parser.load_lex_cat_dict(config_dir() + "/markedup_raw_ind"); // needed for both training and testing
  bool use_momentum = false;
  Trainer* sgd = nullptr;
  if (use_momentum)
    sgd = new MomentumSGDTrainer(&model);
  else {
    sgd = new SimpleSGDTrainer(&model);
    sgd->eta = eta();
    sgd->eta_decay = 0.08;
  }

  auto seed = chrono::high_resolution_clock::now().time_since_epoch().count();
  std::mt19937_64 engine(seed);
  float total_xf1 = 0.0;
  float total_parses = 0.0;

  unsigned queue_builder_in_dim = de();
  if (use_pos())
    queue_builder_in_dim += dp();
  //unsigned c_dim = 4*nh();
  unsigned w_dim = de();
  unsigned s_dim = dsuper();
  unsigned a_dim = daction();
  if (feeding()) {
    w_dim = w_dim + nh();
    s_dim = s_dim + nh();
    a_dim = a_dim + nh();
  }
  cnn::LSTMBuilder queue_l2rbuilder(1, queue_builder_in_dim, nh(), &model);
  cnn::LSTMBuilder queue_r2lbuilder(1, queue_builder_in_dim, nh(), &model);
  cnn::LSTMBuilder w_stackbuilder(layers(), w_dim, nh(), &model);
  cnn::LSTMBuilder s_stackbuilder(layers(), s_dim, nh(), &model);
  cnn::LSTMBuilder a_stackbuilder(layers(), a_dim, nh(), &model);
  cnn::LSTMBuilder p_stackbuilder(layers(), dp(), nh(), &model);
  //cnn::LSTMBuilder c_stackbuilder(layers(), c_dim, nh(), &model);

  // todo refactor this out; only needed becuase of the above constructors needed to be called after the parser constructor
  // otherwise, the params won't init correctly if loading from dumped params (which was trained with the above constructors
  // called after the parser constructor -- dumpping and loading params order needs to be consistent)
  parser.set_stackbuiler_pts(queue_l2rbuilder, queue_r2lbuilder, w_stackbuilder, s_stackbuilder, a_stackbuilder, p_stackbuilder);

  if (!use_rnn_super()) {
    assert(lstm_tagger_model() != "");
    string fname3 = model_dir() + "/" + lstm_tagger_model();
    cerr << "lstm super model params: " << fname3 << endl;
    if (!integrate()) {
      cerr << "loading lstm tagger model params\n";
      ifstream in3(fname3);
      boost::archive::text_iarchive ia3(in3);
      ia3 >> tagger_model;
    }
  }

  if (mode() == 0 || mode() == 1 || mode () == 3) { // decode or xf1 training
    string fname = model_dir() + "/model." + std::to_string(model_epoch());
    string fname2 = model_dir() + "/supercat_map." + std::to_string(model_epoch());
    cerr << "model params: " << fname << endl;
    cerr << "supercat map: " << fname2 << endl;

    ifstream in(fname);
    boost::archive::text_iarchive ia(in);
    ia >> model;
    parser.load_supercat_map(fname2);

//    if (!use_rnn_super()) {
//      assert(lstm_tagger_model() != "");
//      string fname3 = model_dir() + "/" + lstm_tagger_model();
//      cerr << "lstm super model params: " << fname3 << endl;
//      if (!integrate()) {
//        cerr << "loading lstm tagger model params\n";
//        ifstream in3(fname3);
//        boost::archive::text_iarchive ia3(in3);
//        ia3 >> tagger_model;
//      }
//    }

    if (mode() == 1)
      parser.init_xf1_training(false);
    else if (mode() == 3)
      parser.init_xf1_training(true);

    float acts = 0.0;
    float correct = 0.0;
    float total_xf1 = 0.0;
    float total_parses = 0.0;
    float total_correct_deps = 0.0;
    float total_returned_deps = 0.0;
    float total_gold_deps = 0.0;
    if (mode() == 0 || mode() == 1)
      decode(mode(), reader, sent, parser, srbeam(), _oracle, test_calc_f1_train(),
           rnn_super, beta(), out, FMT, printer,
           t_l2rbuilder, t_r2lbuilder,
           total_xf1, total_parses, total_correct_deps, total_returned_deps, total_gold_deps,
           acts, correct, use_rnn_super(), test_lstm_tagger());
    if (mode() == 1) {
      cerr << "total_correct_deps: " << total_correct_deps << endl;
      cerr << "total_deps: " << total_returned_deps << endl;
      cerr << "total_gold_deps: " << total_gold_deps << endl;
      cerr << "p: " << total_correct_deps / total_returned_deps << endl;
      cerr << "r: " << total_correct_deps / total_gold_deps << endl;
    }
  }

  if (mode() == 2) { // greedy training
    parser.load_gold_trees(config_dir() + "/std_train/new_emb_dict_and_pos_feat/sfef/wsj02-21.pipe.processed.sfef");
    parser.load_gold_trees_rnn_fmt(config_dir() + "/std_train/new_emb_dict_and_pos_feat/sfef/wsj02-21.pipe.processed.sfef.rnn_fmt");

    float total_words = 0.0;
    float total_cats = 0.0;
    //Super super(super_cfg);
    std::vector<std::vector<std::vector<std::string>*>*> all_cv_supercats;
    string cv_supercat_file_path;
    if (use_rnn_super())
      cv_supercat_file_path = config_dir() + "/std_train/" + cv_supercat_file_rnn();
    else
      cv_supercat_file_path = config_dir() + "/std_train/" + cv_supercat_file_lstm();
    load_cv0001_supercats(cv_supercat_file_path, all_cv_supercats);

    unsigned sent_id = 0;
    size_t e = 0;
    vector<Sentence> all_sents;
    vector<size_t> train_data_inds;
    while (1) {
      assert(mode() >= 2);
      parser.change_mode(mode());
      reader.reset();
      sent_id = 0;
      if (e > 0) {
        assert(train_data_inds.size() == 36829);
        shuffle(begin(train_data_inds), end(train_data_inds), engine);
      }
      auto sent_id_iter = train_data_inds.begin();
      float loss = 0.0;
      float acts = 0.0;
      float correct = 0.0;

      while(reader.next(sent)) {
        //if (sent_id == 1000) break;
        ComputationGraph cg;
        if(sent.words.size() == 0) {
          cerr << "end of input" << endl;
          break;
        }
        if (e == 0) {
          all_sents.push_back(sent);
          train_data_inds.push_back(sent_id);
        } else {
          sent.reset();
          assert(sent_id_iter != train_data_inds.end());
          sent_id = *sent_id_iter;
          sent = all_sents[sent_id];
        }

        const size_t NWORDS = sent.words.size();
        total_words += NWORDS;
        sent.msuper.resize(NWORDS);
        std::vector<std::vector<std::string>*> &sentcats = *all_cv_supercats[static_cast<size_t>(sent_id)];
        assert(sentcats.size() == NWORDS);
        // supply mtags for each word
        for (size_t i = 0; i < NWORDS; ++i) {
          sent.msuper.resize(NWORDS);
          MultiRaw &mraw = sent.msuper[i];
          mraw.clear();
          std::vector<std::string> &wordcats = *sentcats[i];
          bool has_gold_super = false;
          for (size_t j = 0; j < wordcats.size(); ++j) {
            mraw.push_back(ScoredRaw(wordcats[j], 0.0));
            ++total_cats;
            if (wordcats[j] == sent.super[i])
              has_gold_super = true;
          }
          // ensure gold supertag is supplied
          if (!has_gold_super) {
            mraw.push_back(ScoredRaw(sent.super[i], 1.0));
            ++total_cats;
          }
        }

        parser.sr_parse_beam_search_xf1(t_l2rbuilder, t_r2lbuilder, cg, sent_id, beta(), total_xf1, total_parses, acts, correct, false);
        double lp = as_scalar(cg.incremental_forward());
        assert(lp >= 0.0);
        cg.backward();
        if (sgd->epoch < 10)
          sgd->eta = 0.1;
        sgd->update(1.0);
        loss += lp;
        if (e == 0) ++sent_id;
        else ++sent_id_iter;
      } // end epoch

      ++e;
      sgd->status();
      cerr << " E = " << (loss / acts) << " ppl=" << exp(loss / acts)
                   << " (acc=" << (correct / acts) << ") " << endl;
      sgd->update_epoch();
      loss = acts = correct = 0.0;

      // save model
      string fname = model_dir() + "/model." + std::to_string(e);
      ofstream out_model(fname);
      boost::archive::text_oarchive oa(out_model);
      oa << model;

      if (use_super()) {
        string fname2 = model_dir() + "/supercat_map." + std::to_string(e);
        parser.dump_supercat_map(fname2);
      }

      IO::Output dev_out(output_file() + "." + std::to_string(e));
      PrinterFactory printer(printer_name(), dev_out, log, cats, FMT);
      printer.header(PREFACE.str());
      float total_correct_deps, total_returned_deps, total_gold_deps = 0.0;
      decode(0, reader_dev, sent, parser, srbeam(), _oracle, test_calc_f1_train(),
             rnn_super, beta(), dev_out, FMT, printer,
             t_l2rbuilder, t_r2lbuilder,
             total_xf1, total_parses, total_correct_deps, total_returned_deps, total_gold_deps, acts, correct, use_rnn_super(), test_lstm_tagger());
    }
  } else if (mode() == 3) { // xf1 training
    float total_words = 0.0;
    float total_cats = 0.0;
    //Super super(super_cfg);
    std::vector<std::vector<std::vector<std::string>*>*> all_cv_supercats;
    string cv_supercat_file_path;
    if (use_rnn_super())
      cv_supercat_file_path = config_dir() + "/std_train/" + cv_supercat_file_rnn();
    else
      cv_supercat_file_path = config_dir() + "/std_train/" + cv_supercat_file_lstm();
    load_cv0001_supercats(cv_supercat_file_path, all_cv_supercats);
    assert(all_cv_supercats.size() == 39604);

    unsigned sent_id = 0;
    size_t e = 0;
    vector<Sentence> all_sents;
    vector<size_t> train_data_inds;
    while (1) {
      parser.change_mode(mode());
      reader.reset();
      sent_id = 0;
      if (e > 0) {
        assert(train_data_inds.size() == 39604);
        shuffle(begin(train_data_inds), end(train_data_inds), engine);
      }
      auto sent_id_iter = train_data_inds.begin();
      float loss = 0.0;
      float acts = 0.0;
      float correct = 0.0;
      total_parses = 0.0;
      total_xf1 = 0.0;

      while(reader.next(sent)) {
        //if (sent_id == 1000) break;
        ComputationGraph cg;
        if(sent.words.size() == 0) {
          cerr << "end of input" << endl;
          break;
        }
        if (e == 0) {
          all_sents.push_back(sent);
          train_data_inds.push_back(sent_id);
        } else {
          sent.reset();
          assert(sent_id_iter != train_data_inds.end());
          sent_id = *sent_id_iter;
          sent = all_sents[sent_id];
        }

        const size_t NWORDS = sent.words.size();
        total_words += NWORDS;
        sent.msuper.resize(NWORDS);

        // supply mtags for each word
        std::vector<std::vector<std::string>*> &sentcats = *all_cv_supercats[static_cast<size_t>(sent_id)];
        assert(sentcats.size() == NWORDS);
        sent.msuper.resize(NWORDS);
        for (size_t i = 0; i < NWORDS; ++i) {
          MultiRaw &mraw = sent.msuper[i];
          mraw.clear();
          std::vector<std::string> &wordcats = *sentcats[i];
          for (size_t j = 0; j < wordcats.size(); ++j) {
            mraw.push_back(ScoredRaw(wordcats[j], 0.0));
            ++total_cats;
          }
        }

        parser.sr_parse_beam_search_xf1(t_l2rbuilder, t_r2lbuilder, cg, sent_id, beta(), total_xf1, total_parses, acts, correct, false);
        double xf1 = as_scalar(cg.incremental_forward());
        cerr << "cnn xf1: " << xf1 << endl;
        assert(xf1 < 0.0 || xf1 == -0.0);
        cg.backward();
        if (sgd->epoch < delay_decay())
          sgd->eta = 0.1;
        sgd->update(1.0);
        loss += (-xf1);
        if (e == 0) ++sent_id;
        else ++sent_id_iter;
        parser.clear_xf1();
      } // end epoch

      ++e;
      sgd->status();
      cerr << " E = " << (loss / (double)train_data_inds.size()) << " ppl=" << exp(loss / acts) << endl;
      sgd->update_epoch();
      loss = acts = correct = 0.0;

      cerr << "avg parse count: " << total_parses / (double)train_data_inds.size() << endl;

      // save model
      string fname = model_dir_xf1() + "/model." + std::to_string(e);
      ofstream out_model(fname);
      boost::archive::text_oarchive oa(out_model);
      oa << model;

      if (use_super()) {
        string fname2 = model_dir_xf1() + "/supercat_map." + std::to_string(e);
        parser.dump_supercat_map(fname2);
      }

      IO::Output dev_out(output_file() + "." + std::to_string(e));
      PrinterFactory printer(printer_name(), dev_out, log, cats, FMT);
      printer.header(PREFACE.str());
      float total_correct_deps, total_returned_deps, total_gold_deps = 0.0;
      decode(0, reader_dev, sent, parser, srbeam(), _oracle, test_calc_f1_train(),
             rnn_super, beta(), dev_out, FMT, printer,
             t_l2rbuilder, t_r2lbuilder,
             total_xf1, total_parses, total_correct_deps, total_returned_deps, total_gold_deps, acts, correct, use_rnn_super(), test_lstm_tagger());
    }
    //    const double BETA = beta();
    //    size_t sent_id = 0;
    //    Super super(super_cfg);
    //
    //    // xf1 training related
    //    double total_correct_deps = 0.0;
    //    double total_returned_deps = 0.0;
    //    double total_gold_deps = 0.0;
    //    parser.init_xf1_training(test_calc_f1_train());
    //    double total_xf1 = 0.0;
    //    double total_parses = 0.0;
    //    vector<Sentence> all_sents;
    //    vector<size_t> train_data_inds;
    //
    //    if (restart() > 0) {
    //      while (reader.next(sent)) {
    //        all_sents.push_back(sent);
    //        train_data_inds.push_back(sent_id);
    //        ++sent_id;
    //      }
    //    }
    //    reader.reset();
    //    sent_id = 0;
    //
    //    std::chrono::time_point<std::chrono::system_clock> start_t, end_t;
    //    for (size_t e = restart(); e < epochs(); ++e) {
    //      start_t = std::chrono::system_clock::now();
    //      reader.reset();
    //      sent_id = 0;
    //      total_xf1 = 0.0;
    //      total_parses = 0.0;
    //
    //      if (e > 0) {
    //        assert(train_data_inds.size() == 39604);
    //        shuffle(begin(train_data_inds), end(train_data_inds), engine);
    //        cerr << "epoch " << e << ", total training sents: " << train_data_inds.size() << endl;
    //      } else {
    //        cerr << "epoch " << e << " " << endl;
    //      }
    //      auto sent_id_iter = train_data_inds.begin();
    //      while (reader.next(sent)) {
    //        if (e == 0) {
    //          all_sents.push_back(sent);
    //          train_data_inds.push_back(sent_id);
    //        } else {
    //          sent.reset();
    //          sent_id = *sent_id_iter;
    //          sent = all_sents[sent_id];
    //        }
    //        const size_t NWORDS = sent.words.size();
    //        sent.msuper.resize(NWORDS);
    //        sent.all_ly_vals.reserve(NWORDS);
    //
    //        if (mode() == 2 && run_training && !xf1_super()) {
    //          // use cv super model
    //          size_t cv_model_number = sent_id / 3960;
    //          if (cv_model_number >= cv_super_vec.size()) {
    //            cv_model_number = cv_super_vec.size() - 1;
    //          }
    //          cerr << "using cv super model: " << cv_model_number << endl;
    //          _rnn_super = cv_super_vec[cv_model_number];
    //        }
    //        _rnn_super->mtag_sent(sent, BETA);
    //
    //        if(NWORDS == 0) {
    //          log.stream << "end of input" << endl;
    //          break;
    //        } else if (NWORDS == 1 && mode() != 2) {
    //          printer.parsed(0, sent, BETA, 0);
    //          lexical(sent, out, FMT);
    //          out.stream << "\n";
    //        } else {
    //          const ShiftReduceHypothesis *output = 0;
    //          if (mode() == 0 || mode() == 2) {
    //            output = parser.sr_parse_beam_search_train_xf1(*_rnn_super, srbeam, sent_id, oracle(),
    //                                                           test_calc_f1_train(), total_xf1, total_parses,
    //                                                           num_threads(), bptt_graph(), run_training);
    //          } else if (mode() == 1) {
    //            if (xf1_super())
    //              parser.xf1_grad_check_super(*_rnn_super, srbeam, sent_id, total_parses);
    //            else
    //              parser.xf1_grad_check(*_rnn_super, srbeam, sent_id, total_parses, num_threads());
    //          }
    //
    //          if (!output) {
    //            cerr << "sent: " << sent_id << " has no output" << endl;
    //
    //          } else if (output && mode() == 0) {
    //            size_t stackSize = output->GetStackSize();
    //            std::vector<const SuperCat *> roots;
    //            vector<const SuperCat *>::const_reverse_iterator rit;
    //            roots.push_back(output->GetStackTopSuperCat());
    //
    //            for (size_t i = 0; i < stackSize - 1; ++i) {
    //              output = output->GetPrvStack();
    //              roots.push_back(output->GetStackTopSuperCat());
    //            }
    //
    //            for (rit = roots.rbegin(); rit < roots.rend(); ++rit) {
    //              printer.parsed(*rit, sent, BETA, 0);
    //              if (mode() == 0) {
    //                parser.get_deps(*rit, sent_id, total_correct_deps,
    //                                total_returned_deps, FMT, test_calc_f1_train());
    //              }
    //            }
    //            lexical(sent, out, FMT);
    //            out.stream << "\n";
    //          }
    //        }
    //
    //        if(mode() == 0)
    //          total_gold_deps += parser.get_total_sent_gold_deps(sent_id, test_calc_f1_train());
    //        if (e == 0) ++sent_id;
    //        else ++sent_id_iter;
    //        if (NWORDS != 1)
    //          parser.clear_xf1();
    //      } // end while
    //
    //      cerr << "avg parse count: " << total_parses / (double) train_data_inds.size() << endl;
    //
    //      end_t = std::chrono::system_clock::now();
    //      std::chrono::duration<double> elapsed_seconds = end_t - start_t;
    //      cerr << "epoch " << e << " time: " << elapsed_seconds.count() << endl;
    //      cerr << "avg xf1: " << total_xf1 / (double) train_data_inds.size() << endl;
    //
    //      if (mode() == 0) {
    //        cerr << "total_correct_deps: " << total_correct_deps << endl;
    //        cerr << "total_deps: " << total_returned_deps << endl;
    //        cerr << "total_gold_deps: " << total_gold_deps << endl;
    //        cerr << "p: " << total_correct_deps / total_returned_deps << endl;
    //        cerr << "r: " << total_correct_deps / total_gold_deps << endl;
    //        break;
    //      }
    //
    //      if (mode() == 2 && !xf1_super()) {
    //        parser.save_weights(e, xf1_model());
    //      }
    //
    //      if (mode() == 2 && xf1_super()) {
    //        _rnn_super->save_xf1_model(e, xf1_model());
    //      }
    //    }
  }
  return 0;
}


//  RnnTagger(const string &model_dir,
//            const string &all_untouched_embeddings,
//            const string &suf_str_ind_file,
//            size_t output_fmt,
//            double dropout_success_prob,
//            bool dropout,
//            bool use_candc_tag_dicts,
//            size_t candc_tagdict_cutoff,
//            bool candc_tagdict_pos_backoff,
//            Super::Config &super_cfg);

//RNN tagger related
//NLP::Taggers::RnnTagger rnn_super(rnn_super_model(), emb_file(), suf_file(), 0, 0.75, true, "sig", bi_super(), true, xf1_super_lr());

//  string cv_super_models;
//  if (bi_super())
//    cv_super_models = config_dir() + "/cv_super_models_bi/";
//  else
//    cv_super_models = config_dir() + "/cv_super_models/";
//
//  NLP::Taggers::RnnTagger rnn_super1(cv_super_models + "/model.1", emb_file(), suf_file(), 0, 0.75, true, "sig", bi_super(), true);
//  NLP::Taggers::RnnTagger rnn_super2(cv_super_models + "/model.2", emb_file(), suf_file(), 0, 0.75, true, "sig", bi_super(), true);
//  NLP::Taggers::RnnTagger rnn_super3(cv_super_models + "/model.3", emb_file(), suf_file(), 0, 0.75, true, "sig", bi_super(), true);
//  NLP::Taggers::RnnTagger rnn_super4(cv_super_models + "/model.4", emb_file(), suf_file(), 0, 0.75, true, "sig", bi_super(), true);
//  NLP::Taggers::RnnTagger rnn_super5(cv_super_models + "/model.5", emb_file(), suf_file(), 0, 0.75, true, "sig", bi_super(), true);
//  NLP::Taggers::RnnTagger rnn_super6(cv_super_models + "/model.6", emb_file(), suf_file(), 0, 0.75, true, "sig", bi_super(), true);
//  NLP::Taggers::RnnTagger rnn_super7(cv_super_models + "/model.7", emb_file(), suf_file(), 0, 0.75, true, "sig", bi_super(), true);
//  NLP::Taggers::RnnTagger rnn_super8(cv_super_models + "/model.8", emb_file(), suf_file(), 0, 0.75, true, "sig", bi_super(), true);
//  NLP::Taggers::RnnTagger rnn_super9(cv_super_models + "/model.9", emb_file(), suf_file(), 0, 0.75, true, "sig", bi_super(), true);
//  NLP::Taggers::RnnTagger rnn_super10(cv_super_models + "/model.10", emb_file(), suf_file(), 0, 0.75, true, "sig", bi_super(), true);

//  NLP::Taggers::RnnTagger *_rnn_super = &rnn_super;;

//  vector<NLP::Taggers::RnnTagger *> cv_super_vec;
//  cv_super_vec.reserve(10);
//  cv_super_vec.emplace_back(&rnn_super1);
//  cv_super_vec.emplace_back(&rnn_super2);
//  cv_super_vec.emplace_back(&rnn_super3);
//  cv_super_vec.emplace_back(&rnn_super4);
//  cv_super_vec.emplace_back(&rnn_super5);
//  cv_super_vec.emplace_back(&rnn_super6);
//  cv_super_vec.emplace_back(&rnn_super7);
//  cv_super_vec.emplace_back(&rnn_super8);
//  cv_super_vec.emplace_back(&rnn_super9);
//  cv_super_vec.emplace_back(&rnn_super10);

// RNN parser weights
//string model_dir = model_base_dir();

//  if (use_lh_cache())
//    parser_cfg.use_lh_cache.set_value(true);
//  if (xf1_super())
//    parser_cfg.xf1_super.set_value(true);

#include "main.h"
