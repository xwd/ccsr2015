const char *PROGRAM_NAME = "rnn_super";

#include "base.h"
#include "main.h"

#include "config/config.h"
#include "io/format.h"
#include "config/format.h"
#include "tagger/super.h"

#include "io/reader.h"
#include "io/reader_factory.h"
#include "io/writer.h"
#include "io/writer_factory.h"

#include "model/model.h"
#include "parser/integration.h"

#include "timer.h"
#include "rnn_tagger.h"
#include <thread>
#include <armadillo>
#include <functional>

using namespace std;
using namespace NLP;
using namespace NLP::IO;
using namespace NLP::Taggers;
using namespace NLP::CCG;
using namespace arma;

// these two extra lines are needed here
// for legacy reasons, it's a hack, remove later
std::vector<double> Integration::s_perceptronId;
bool Integration::s_train;

void
train_mt(const size_t de, const size_t nh, const size_t bs,
         const string emb_file_name, const string suf_file_name, const string model,
         const bool dropout, const double dropout_prob, const bool verbose, const double lr,
         const size_t thread_id, const string input_base_name, const string input_file_dev,
         const Format &fmt, mat &wx, mat &wh, mat &wy,
         mat &emb, mat &suf, mat &cap, const size_t epoch,
         const bool iterative_avg) {
  //string input_file = "./train_rnn_super_mt_temp/" + input_base_name + to_string(thread_id);
  string input_file = "./train_rnn_super_mt_temp/wsj02-21.stagged";
  cerr << input_file << endl;
  ReaderFactory reader_train(input_file, fmt);
  ReaderFactory reader_dev(input_file_dev, fmt);

  RnnTagger rnn_super(de, 5, 5, 7, nh, bs, emb_file_name, suf_file_name, model,
                      true, 1, dropout, dropout_prob, verbose, lr);
  if (epoch == 0 || iterative_avg) {
    rnn_super.m_wx = wx;
    rnn_super.m_wy = wy;
    rnn_super.m_wh = wh;
    rnn_super.m_emb = emb;
    rnn_super.m_suf = suf;
    rnn_super.m_cap = cap;
  } else {
    const string _wx = "./train_rnn_super_mt_temp/" + to_string(thread_id) + ".wx.mat";
    const string _wh = "./train_rnn_super_mt_temp/" + to_string(thread_id) + ".wh.mat";
    const string _wy = "./train_rnn_super_mt_temp/" + to_string(thread_id) + ".wy.mat";
    const string _emb = "./train_rnn_super_mt_temp/" + to_string(thread_id) + ".emb.mat";
    const string _suf = "./train_rnn_super_mt_temp/" + to_string(thread_id) + ".suf.mat";
    const string _cap = "./train_rnn_super_mt_temp/" + to_string(thread_id) + ".cap.mat";
    rnn_super.re_init_mats(_wx, _wh, _wy, _emb, _suf, _cap);
  }

  rnn_super.train_mt(reader_train, reader_dev,
                     thread_id, "./train_rnn_super_mt_temp/");
}

int
run(int argc, char** argv){

  Config::Main cfg(PROGRAM_NAME);
  //Super::Config super_cfg;
  //Config::Alias super_alias(cfg, super_cfg, "super", "super");
  Config::Op<bool> train(cfg, "train", "training (ture/false)", false);
  //Config::Op<std::string> input_file(cfg, NLP::Config::SPACE, "input", "the input file to read from", STDIN);
  Config::Op<std::string> input_file_dev(cfg, NLP::Config::SPACE, "input_dev", "the input file to read from", STDIN);
  Config::Op<IO::Format> ifmt(cfg, "ifmt", "the input file format", Format("%w|%p|%s \n"));
  Config::Op<std::string> model(cfg, "model", "the dir to load/save the model", "./");
  Config::Op<std::string> emb_file(cfg, "emb", "file name of the embeddings file (*UNKONWN* changed to ***PADDING***, otherwise untouched)", "");
  Config::Op<std::string> suf_file(cfg, "suf", "file name of the suffix file", "");
  Config::Op<std::size_t> de(cfg, "de", "embedding dimension", 50);
  Config::Op<std::size_t> bs(cfg, "bptt_bs", "bptt steps", 9);
  Config::Op<bool> dropout(cfg, "dropout", "use dropout", true);
  Config::Op<bool> verbose(cfg, "verbose", "logging output", true);
  Config::Op<double> dropout_prob(cfg, "dropout_prob", "drop out success prob", 0.75);
  Config::Op<double> lr(cfg, "lr", "learning rate", 0.0025);
  Config::Op<double> nh(cfg, "nh", "hidden layer size", 200);
  Config::Op<size_t> output_fmt(cfg, "output_fmt", "0: no output (run evaluation) 1: singe 2: multi", 0);
  Config::Op<double> beta(cfg, "beta", "beta cutoff", 0.0);
  Config::Op<bool> use_rnn_tagdict(cfg, "use_rnn_tagdict", "use_rnn_tagdict", false);
  Config::Op<bool> use_candc_tagdict(cfg, "use_candc_tagdict", "use_candc_tagdict", false);
  Config::Op<size_t> candc_dict_cutoff(cfg, "candc_dict_cutoff", "candc tagdict cutoff", 20);
  Config::Op<bool> candc_pos_backoff(cfg, "candc_pos_backoff", "use pos tagdict backoff when using c&c super tagdict", false);
  Config::Op<bool> iter_avg(cfg, "iter_avg", "use iterative averagin", false);
  Config::Op<size_t> num_threads(cfg, "num_threads", "number of threads to use", 10);

  cfg.parse(argc, argv);
  cfg.check();

  string emb_file_name = model() + "/" + emb_file();
  string suf_file_name = model() + "/" + suf_file();

  const string input_base_name = "wsj02-21.stagged.part.";
  const Format fmt = ifmt();
  RnnTagger rnn_super(de(), 5, 5, 7, nh(), bs(), emb_file_name, suf_file_name, model(),
                      true, 50, dropout(), dropout_prob(), verbose(), lr());
  RnnTagger rnn_super_test(de(), 5, 5, 7, nh(), bs(), emb_file_name, suf_file_name, model(),
                           true, 50, dropout(), dropout_prob(), verbose(), lr());
  ReaderFactory reader_dev(input_file_dev(), ifmt());

  double z = (double)num_threads();

  for (size_t e = 0; e < 60; ++e) {
    std::vector<thread> threads;
    for (size_t i = 0; i < num_threads(); ++i) {
      threads.push_back(thread(&train_mt, de(), nh(), bs(), emb_file_name,
                               suf_file_name, model(), true, dropout_prob(), verbose(), lr(),
                               i, input_base_name, input_file_dev(), ref(fmt),
                               ref(rnn_super_test.m_wx), ref(rnn_super_test.m_wh), ref(rnn_super_test.m_wy),
                               ref(rnn_super_test.m_emb), ref(rnn_super_test.m_suf), ref(rnn_super_test.m_cap),
                               e, iter_avg()));
    }

    for (thread &th : threads) {
      th.join();
    }

    rnn_super_test.m_wx.zeros();
    rnn_super_test.m_wh.zeros();
    rnn_super_test.m_wy.zeros();
    rnn_super_test.m_emb.zeros();
    rnn_super_test.m_suf.zeros();
    rnn_super_test.m_cap.zeros();

    for (size_t j = 0; j < num_threads(); ++j) {
      mat _wx;
      mat _wh;
      mat _wy;
      mat _emb;
      mat _suf;
      mat _cap;
      const string __wx = "./train_rnn_super_mt_temp/" + to_string(j) + ".wx.mat";
      const string __wh = "./train_rnn_super_mt_temp/" + to_string(j) + ".wh.mat";
      const string __wy = "./train_rnn_super_mt_temp/" + to_string(j) + ".wy.mat";
      const string __emb = "./train_rnn_super_mt_temp/" + to_string(j) + ".emb.mat";
      const string __suf = "./train_rnn_super_mt_temp/" + to_string(j) + ".suf.mat";
      const string __cap = "./train_rnn_super_mt_temp/" + to_string(j) + ".cap.mat";
      _wx.load(__wx);
      _wh.load(__wh);
      _wy.load(__wy);
      _emb.load(__emb);
      _suf.load(__suf);
      _cap.load(__cap);

      rnn_super_test.m_wx += _wx;
      rnn_super_test.m_wh += _wh;
      rnn_super_test.m_wy += _wy;
      rnn_super_test.m_emb += _emb;
      rnn_super_test.m_suf += _suf;
      rnn_super_test.m_cap += _cap;
    }
    rnn_super_test.m_wx /= z;
    rnn_super_test.m_wh /= z;
    rnn_super_test.m_wy /= z;
    rnn_super_test.m_emb /= z;
    rnn_super_test.m_suf /= z;
    rnn_super_test.m_cap /= z;

    rnn_super_test.tag_file(reader_dev, true);

    rnn_super_test.m_wx.save(model() + "/wx." + to_string(e) + ".mat");
    rnn_super_test.m_wh.save(model() + "/wh." + to_string(e) + ".mat");
    rnn_super_test.m_wy.save(model() + "/wy." + to_string(e) + ".mat");
    rnn_super_test.m_suf.save(model() + "/suffix." + to_string(e) + ".mat");
    rnn_super_test.m_cap.save(model() + "/cap." + to_string(e) + ".mat");
    rnn_super_test.m_emb.save(model() + "/emb." + to_string(e) + ".mat");
  }

  return 0;
}

