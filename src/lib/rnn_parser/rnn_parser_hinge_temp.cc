// C&C NLP tools
// Copyright (c) Universities of Edinburgh, Oxford and Sydney
// Copyright (c) James R. Curran
//
// This software is covered by a non-commercial use licence.
// See LICENCE.txt for the full text of the licence.
//
// If LICENCE.txt is not included in this distribution
// please email candc@it.usyd.edu.au to obtain a copy.

#include "parser/ShiftReduceHypothesis.h"
#include "parser/ShiftReduceHypothesisQueue.h"
#include <stdlib.h>
#include <armadillo>
#include <fstream>
#include "rnn_parser/_rnn_parser.h"
#include <time.h>
#include <fstream>
#include "../../include/rnn_parser/_context_load_pos.h"

using namespace std;
using namespace arma;

namespace NLP { namespace CCG {

using namespace NLP::Tree;

//Pool *Parser::s_shiftReduceFeaturePool(new Pool(1 << 20));

RNNParser::RNNConfig::RNNConfig(const OpPath *base, const std::string &name, const std::string &desc)
: Directory(name, desc, base),
  cats(*this, "cats", "parser category directory", "//cats", &path),
  markedup(*this, "markedup", "parser markedup file", "//markedup", &cats),
  weights(*this, "weights", "parser model weights file", "//weights", &path),
  rules(*this, "rules", "parser rules file", "//rules", &path),
  maxwords(*this, SPACE, "maxwords", "maximum sentence length the parser will accept", 250),
  maxsupercats(*this, "maxsupercats", "maximum number of supercats before the parse explodes", 300000),
  alt_markedup(*this, SPACE, "alt_markedup", "use the alternative markedup categories (marked with !)", false),
  seen_rules(*this, "seen_rules", "only accept category combinations seen in the training data", true),
  extra_rules(*this, "extra_rules", "use additional punctuation and unary type-changing rules", true),
  question_rules(*this, "question_rules", "activate the unary rules that only apply to questions", false),
  eisner_nf(*this, "eisner_nf", "only accept composition when the Eisner (1996) constraints are met", true),
  partial_gold(*this, SPACE, "partial_gold", "used for generating feature forests for training", false),
  beam(*this, "beam_ratio", "(not fully tested)", 0.0),

  allowFrag(*this, "allowFrag", "allow fragmented output", true),

  bs(*this, SPACE, "bptt_steps", "number of bptt steps", 9),
  ds(*this, SPACE, "dim_suf", "suffix embedding dimension", 50),
  dc(*this, SPACE, "dim_cap", "cap embedding dimension", 5),
  de(*this, SPACE, "dim_cap", "word embedding dimension", 50),
  dsuper(*this, SPACE, "dim_super", "supercat embedding dimension", 50),
  daction(*this, SPACE, "dim_action", "action embedding dimension", 50),
  nh(*this, SPACE, "n_hidden", "hidden layer size", 200),
  lr(*this, "lr", "learning rate", 0.0025),
  use_supercat_feats(*this, "use_super_feats", "use super cat features", true),
  use_action_feats(*this, "use_action_feats", "use action features", false),
  use_structure_loss(*this, "use_structure_loss", "use structure loss", false),
  loss_pen(*this, "loss_pen", "structure loss penalty factor", 0.05) {}
  //rnn_model_base_dir(*this, "rnn model dir", "dir path to save trained rnn model", './')


void
RNNParser::_Impl::_load_rules(const std::string &filename){
  cerr << "loading rules: " << filename << endl;
  ulong nlines = 0;
  if(filename == "")
    return;

  ifstream in(filename.c_str());
  if(!in)
    throw NLP::IOException("could not open rules file", filename);

  comment += read_preface(filename, in, nlines);

  string cat1str, cat2str;
  while(in >> cat1str >> cat2str){
    try {
      const Cat *cat1 = cats.canonize(cat1str.c_str());
      const Cat *cat2 = cats.canonize(cat2str.c_str());

      rule_instances.insert(cat1, cat2);
    }catch(NLP::Exception e){
      throw NLP::IOException("error parsing category in rule instantiation", filename);
    }
  }
  cerr << "total binary rules: " << rule_instances.size() << endl;
}


void
RNNParser::_Impl::_load_features(const std::string &filename){

  //std::cerr << "_load_features in parser.cc" << std::endl;
  ulong nlines = 0;
  ifstream in(filename.c_str());
  if(!in)
    NLP::IOException("could not open features file", filename);

  comment += read_preface(filename, in, nlines);

  nfeatures = 1;
  string tmp;
  ulong freq;
  for(char c; in >> freq >> c; ++nfeatures){
    switch(c){
    case 'a': cat_feats.load(in, filename, nfeatures, LEX_WORD); break;
    case 'b': cat_feats.load(in, filename, nfeatures, LEX_POS); break;
    case 'c': cat_feats.load(in, filename, nfeatures, ROOT); break;
    case 'd': cat_feats.load(in, filename, nfeatures, ROOT_WORD); break;
    case 'e': cat_feats.load(in, filename, nfeatures, ROOT_POS); break;

    case 'f': dep_feats.load(in, filename, nfeatures, DEP_WORD); break;
    case 'g': dep_feats.load(in, filename, nfeatures, DEP_POS); break;
    case 'h': dep_feats.load(in, filename, nfeatures, DEP_WORD_POS); break;
    case 'i': dep_feats.load(in, filename, nfeatures, DEP_POS_WORD); break;

    case 'x': genrule_feats.load(in, filename, nfeatures, GEN_RULE); break;
    case 'm': rule_feats.load(in, filename, nfeatures, URULE); break;
    case 'n': rule_feats.load(in, filename, nfeatures, BRULE); break;

    case 'p': rule_head_feats.load(in, filename, nfeatures, URULE_HEAD); break;
    case 'q': rule_head_feats.load(in, filename, nfeatures, BRULE_HEAD); break;
    case 'r': rule_head_feats.load(in, filename, nfeatures, URULE_POS); break;
    case 's': rule_head_feats.load(in, filename, nfeatures, BRULE_POS); break;

    case 't': rule_dep_feats.load(in, filename, nfeatures, BRULE_HEAD_HEAD); break;
    case 'u': rule_dep_feats.load(in, filename, nfeatures, BRULE_POS_HEAD); break;
    case 'v': rule_dep_feats.load(in, filename, nfeatures, BRULE_HEAD_POS); break;
    case 'w': rule_dep_feats.load(in, filename, nfeatures, BRULE_POS_POS); break;

    case 'F': rule_dep_dist_feats.load(in, filename, nfeatures, DIST_ADJ_HEAD); break;
    case 'G': rule_dep_dist_feats.load(in, filename, nfeatures, DIST_VERBS_HEAD); break;
    case 'H': rule_dep_dist_feats.load(in, filename, nfeatures, DIST_PUNCT_HEAD); break;
    case 'I': rule_dep_dist_feats.load(in, filename, nfeatures, DIST_ADJ_POS); break;
    case 'J': rule_dep_dist_feats.load(in, filename, nfeatures, DIST_VERBS_POS); break;
    case 'K': rule_dep_dist_feats.load(in, filename, nfeatures, DIST_PUNCT_POS); break;

    case 'L': dep_dist_feats.load(in, filename, nfeatures, DIST_ADJ_HEAD); break;
    case 'M': dep_dist_feats.load(in, filename, nfeatures, DIST_VERBS_HEAD); break;
    case 'N': dep_dist_feats.load(in, filename, nfeatures, DIST_PUNCT_HEAD); break;
    case 'P': dep_dist_feats.load(in, filename, nfeatures, DIST_ADJ_POS); break;
    case 'Q': dep_dist_feats.load(in, filename, nfeatures, DIST_VERBS_POS); break;
    case 'R': dep_dist_feats.load(in, filename, nfeatures, DIST_PUNCT_POS); break;

    default:
      throw NLP::IOException("unexpected feature type in load features", filename, nfeatures);
    }
  }
  --nfeatures;
}


void
RNNParser::_Impl::load_lex_cat_dict(const string &raw_cats_from_markedup) {
  cerr << "load_lex_cat_dict..." << endl;
  ifstream in(raw_cats_from_markedup.c_str());

  if (!in) {
    cerr << "no such file: " << raw_cats_from_markedup << endl;
    exit (EXIT_FAILURE);
  }

  string s;
  while (getline(in, s)) {
    istringstream ss(s);
    string cat_str;
    size_t ind = 0;
    if (!(ss >> cat_str >> ind)) {
      cerr << "somthing is wrong in: " << raw_cats_from_markedup << endl;
      exit (EXIT_FAILURE);
      break;
    }
    int ind_in_markedup = cats.markedup.index(cat_str);
    assert(ind_in_markedup != -1);
    assert(m_lex_cat_ind_map.insert(make_pair(cat_str, ind)).second);
    assert(m_lex_ind_cat_map.insert(make_pair(ind, cat_str)).second);
  }
  cerr << "number of raw markedup cats in markedup_raw_ind_dict file: "
      << m_lex_cat_ind_map.size() << endl;
  cerr << "number of raw markedup cats in cats.markedup: "
      << cats.plain_markedup_count << endl;
  assert(m_lex_cat_ind_map.size() == 580);
  assert(m_lex_cat_ind_map.size() == cats.plain_markedup_count);
  assert(m_lex_ind_cat_map.size() == m_lex_cat_ind_map.size());
}


RNNParser::_Impl::_Impl(const RNNConfig &cfg, Sentence &sent,
                        Sentence &sent_rnn_fmt,
                        Categories &cats,
                        const size_t srbeam, const bool train)
: cfg(cfg), sent(sent), sent_rnn_fmt(sent_rnn_fmt),
  nsentences(0),
  cats(cats),
  lexicon("lexicon", cfg.lexicon()),
  cat_feats(cats, lexicon),
  rule_feats(cats),
  rule_head_feats(cats, lexicon),
  rule_dep_feats(cats, lexicon),
  rule_dep_dist_feats(cats, lexicon),
  dep_feats(cats, lexicon),
  dep_dist_feats(cats, lexicon),
  genrule_feats(cats),
  weights(0),
  chart(cats, cfg.extra_rules(), cfg.maxwords()),
  rules(chart.pool, cats.markedup, cfg.extra_rules(), rule_instances),

  NP(cats.markedup["NP"]), NbN(cats.markedup["N\\N"]), NPbNP(cats.markedup["NP\\NP"]),
  SbS(cats.markedup["S\\S"]), SfS(cats.markedup["S/S"]),
  SbNPbSbNP(cats.markedup["(S\\NP)\\(S\\NP)"]),
  SbNPfSbNP(cats.markedup["(S\\NP)/(S\\NP)"]), SfSbSfS(cats.markedup["(S/S)\\(S/S)"]),
  SbNPbSbNPbSbNPbSbNP(cats.markedup["((S\\NP)\\(S\\NP))\\((S\\NP)\\(S\\NP))"]),
  NPfNPbNP(cats.markedup["NP/(NP\\NP)"]),

  //m_shiftReduceFeatures(new ShiftReduceFeature(cats, lexicon)),
  m_allowFragTree(cfg.allowFrag()),
  //m_allowFragAndComplete(false),

  m_hasGoldAction(false),
  m_hasOutsideRule(false),
  m_lattice(0),
  m_beamSize(srbeam),
  m_train(train),
  // m_cat_ind is initialized to 1, it gets
  // incremented for the very first seen cat
  // whose ind will be 2, and the actual 0th
  // column in the cat embedding matrix is reserved
  // for the empty cat feature, and the actual 1st
  // reserved for unk: in context_load.h
  // const static size_t empty_supercat_feat_ind = 0;
  // const static size_t unk_supercat_ind = 1;
  m_cat_ind(1)
{
  cerr << "parser constructor parser.cc" << endl;
  cerr << "allowFragTree: " << m_allowFragTree << endl;
  cerr << "SR beam size: " << m_beamSize << endl;
  cerr << "is training: " << Integration::s_train << endl;
  cerr << "eisner: " << cfg.eisner_nf() << endl;

  if(cfg.seen_rules() && cfg.eisner_nf()) {
    string path = cfg.path() + string("//rules_nb_num_et");
    _load_rules(path);
  }

  if (cfg.seen_rules() && !cfg.eisner_nf()) {
    string path = cfg.path() + string("//rules_nb_num_ef");
    _load_rules(path);
  }

  sent.words.reserve(cfg.maxwords());
  sent.pos.reserve(cfg.maxwords());
  sent.msuper.reserve(cfg.maxwords());

  sent_rnn_fmt.words.reserve(cfg.maxwords());
  sent_rnn_fmt.pos.reserve(cfg.maxwords());

  std::cerr << "parser _Impl constructor in parser.cc done" << std::endl;
  std::cerr << "lexicon size: " << lexicon.size() << std::endl;

  // rnn related parameters

  m_use_supercat_feats = cfg.use_supercat_feats();
  m_use_structure_loss = cfg.use_structure_loss();
  m_loss_pen = cfg.loss_pen();

  m_bs = cfg.bs();
  m_ds = cfg.ds();
  m_dc = cfg.dc();
  m_de = cfg.de();
  m_nh = cfg.nh();
  m_dsuper = cfg.dsuper();
  m_lr = cfg.lr();

  m_nclasses = 608;
  //m_vocsize = 23554;
  // the first number is len(knwon_emb_dict) + len(unknown_emb_dict)
  // the second number is 3 types of unks
  m_vocsize = 40618 + 3;
  m_suf_count = 48;

  m_feature_count = 18; // 12 + 6
  m_supercat_feature_count = 10;

  cerr << "m_feature_count: " << m_feature_count << endl;

//  m_total_emb_dim_plus_unks = m_vocsize;
//  m_total_suf_dim_plus_unk = m_suf_count;

  // there are 580 (0 - 579) lexical categoreis in total
  // unary rule ids go from 1 to 18, so unary rule
  // id 1 will get the output class unary_base_count + 1
  // similarly for reduce
  m_unary_base_count = 579;
  m_reduce_base_count = 597;

  std::mt19937 engine;
  std::uniform_real_distribution<double> uni(-1.0, 1.0);

  // essentail to init the weights in m_emb first
  // this deals with 3 types of unks, except *UNKNOWN*
  // which is from the pre-trained embeddings

  if (train) {

    m_emb.set_size(m_de, m_vocsize);
    m_emb.imbue( [&]() { return uni(engine); } );
    m_emb = m_emb * 0.2;

    try {
      load_emb_mat(m_emb, "../files/new_emb_dict_and_pos_feat/known_word_emb_dict.txt");
      load_emb_mat(m_emb, "../files/new_emb_dict_and_pos_feat/unknown_backedoff_word_emb_dict.txt");
    } catch (runtime_error &err) {
      cerr << err.what() << ", exit now." << endl;
      exit(EXIT_FAILURE);
    }

  }

  // the 0th col is reserved for the empty
  // feature, and ind of pos starts from one
  m_suf.set_size(m_ds, m_suf_count + 1);
  m_suf.imbue( [&]() { return uni(engine); } );
  m_suf = m_suf * 0.2;

  m_cap.set_size(m_dc, 2);
  m_cap.imbue( [&]() { return uni(engine); } );
  m_cap *= 0.2;

  if (m_use_supercat_feats) {
    m_supercat_emb.set_size(m_dsuper, 1000);
    m_supercat_emb.imbue( [&]() { return uni(engine); } );
    m_supercat_emb *= 0.2;
  }

//  if (cfg.use_action_feats()) {
//    m_action_emb.set_size(m_daction, 4); // 3 action types + 1
//    m_action_emb.imbue( [&]() { return uni(engine); } );
//    m_action_emb *= 0.2;
//  }

  if (m_use_supercat_feats) {
    m_wx.set_size((m_de + m_ds)*m_feature_count + m_dsuper*m_supercat_feature_count, m_nh);
    m_wx.imbue( [&]() { return uni(engine); } );
    m_wx *= 0.2;
  } else {
    m_wx.set_size((m_de + m_ds)*m_feature_count, m_nh);
    m_wx.imbue( [&]() { return uni(engine); } );
    m_wx *= 0.2;
  }

  m_wy.set_size(m_nh, m_nclasses);
  m_wy.imbue( [&]() { return uni(engine); } );
  m_wy *= 0.2;

  m_wh.set_size(m_nh, m_nh);
  m_wh.imbue( [&]() { return uni(engine); } );
  m_wh *= 0.2;

  m_h_tm1.set_size(1, m_nh);
  m_h_tm1.zeros();

}

void
RNNParser::_Impl::re_init_mats(const string &wx, const string &wh, const string &wy,
                               const string &emb, const string &suf,
                               const string &super_emb) {
  cerr << "rnn_parser re_init_mats...\n";

  m_wx.reset();
  m_wy.reset();
  m_wh.reset();

  m_emb.reset();
  m_suf.reset();
  //m_cap.reset();

  m_wx.load(wx);
  m_wh.load(wh);
  m_wy.load(wy);
  m_emb.load(emb);
  m_suf.load(suf);
  //m_cap.load(cap);

  if (m_use_supercat_feats)
    m_supercat_emb.load(super_emb);
}


void
RNNParser::_Impl::load_emb_mat(mat &emb, const string &filename) {

  cerr << "loading pre-trained embeddings..., "
      << filename << endl;

  ifstream in(filename.c_str());

  size_t total = 0;

  if (!in)
    throw runtime_error("no such file: " + filename);
  string line;
  bool odd = true;
  int ind;

  while (in) {
    if (!getline(in, line)) break;
    istringstream iss(line);
    if (odd) {
      ++total;
      iss >> ind;
      odd = false;
    } else {

      assert(m_emb_ind_map.insert(make_pair(ind, 0)).second);
      assert(ind < (int) m_vocsize - 3);

      mat temp(line);
      emb.col(ind) = temp.t();
      odd = true;
    }
  }

  cerr << "total emb: " << total << endl;

  // test
  emb.col(0).print(cerr, "word emb 0th col");
}


void RNNParser::_Impl::Clear()
{
  chart.reset();
  chart.pool->clear();
  sent.reset();
  if (!m_train) delete m_lattice;

  if (m_train)
  {
    //delete m_shiftReduceFeatures;
    //m_shiftReduceFeatures = new ShiftReduceFeature(cats, lexicon);
  }
}


void RNNParser::_Impl::clear_xf1()
{
  chart.pool->clear();
  sent.reset();
  delete m_lattice;
}


void RNNParser::_Impl::load_gold_trees(const std::string &filename)
{
  std::cerr << "Parser::_Impl::LoadGoldTree start" << std::endl;
  size_t treeId = 0;

  ifstream in(filename.c_str());
  if(!in)
    throw runtime_error("no such file: " + filename);

  vector<ShiftReduceAction*> *tree = new vector<ShiftReduceAction*>;
  while (in)
  {
    string s;
    if (!getline(in, s)) break;
    if (s == "###")
    {
      if (tree->size() > 0)
        m_goldTreeVec.push_back(tree);
      ++treeId;
      tree = new vector<ShiftReduceAction*>;
      continue;
    }

    istringstream ss(s);
    vector<string> node;
    while (ss)
    {
      string s;
      if (!getline(ss, s, ' ')) break;
      node.push_back(s);
    }
    const Cat *cat = cats.markedup[node.at(0)];
    if(!cat)
    {
      try
      {
        cat = cats.canonize(node.at(0).c_str());
      }
      catch(NLP::Exception e)
      {
        cerr << "parse cat failed: " << node.at(0) << endl;
        throw NLP::IOException("attempting to parse category failed for non-SHIFT");
      }
    }
    assert(cat);
    ShiftReduceAction *action = new ShiftReduceAction(atoi(node.at(1).c_str()), cat);
    tree->push_back(action);
  }
  if (tree->size() > 0) m_goldTreeVec.push_back(tree);
  cerr << "total number of gold tress: " << m_goldTreeVec.size() << endl;
}


void
RNNParser::_Impl::load_gold_trees_rnn_fmt(const std::string &filename) {
  std::cerr << "Parser::_Impl::load_gold_trees_rnn_fmt..." << std::endl;
  size_t treeId = 0;

  ifstream in(filename.c_str());
  if(!in)
    throw runtime_error("no such file: " + filename);

  vector<pair<size_t, size_t> > tree;
  while (in)
  {
    string s;
    if (!getline(in, s)) break;
    if (s == "###")
    {
      if (treeId > 0)
        assert(tree.size() != 0);
      if (tree.size() > 0)
        m_gold_tree_vec_rnn.push_back(tree);
      ++treeId;
      tree.clear();
      continue;
    }

    istringstream ss(s);
    vector<string> node;
    while (ss)
    {
      string s;
      if (!getline(ss, s, ' ')) break;
      node.push_back(s);
    }

    int action_id = atoi(node.at(1).c_str());
    int res_cat_id = 0; // for all reduce actions, this will be zero
    if (action_id != 2) {
      assert(action_id == 0 || action_id == 1);
      res_cat_id = atoi(node.at(0).c_str());
    }

    tree.push_back(make_pair((size_t)action_id, (size_t)res_cat_id));
  }

  if (tree.size() > 0)
    m_gold_tree_vec_rnn.push_back(tree);
    cerr << "total number of gold trees rnn format: " << m_gold_tree_vec_rnn.size() << endl;

  //  // unit test
  //
  //  for (size_t i = 0; i < m_gold_tree_vec_rnn.size(); ++i) {
  //    for (size_t j = 0; j < m_gold_tree_vec_rnn[i].size(); ++j) {
  //      cerr << m_gold_tree_vec_rnn[i][j].first << " " << m_gold_tree_vec_rnn[i][j].second << endl;
  //    }
  //    cerr << endl;
  //  }


}


void RNNParser::_Impl::construct_hypo(HypothesisPQueue &hypoQueue, ShiftReduceLattice *lattice,
                                      const ShiftReduceHypothesis *&bestHypo, const ShiftReduceHypothesis *&goldHypo,
                                      ShiftReduceAction *goldAction, const bool goldFinished,
                                      const size_t gold_tree_size)
{

  if (m_train) {
    m_hasGoldAction = false;
  }

  size_t size = hypoQueue.size() > m_beamSize ? m_beamSize : hypoQueue.size();

  for (size_t beam = 0; beam < size; ++beam) {
    HypothesisTuple *tuple = hypoQueue.top();
    const ShiftReduceHypothesis *hypo = tuple->GetCurrentHypo();
    assert(hypo);
    double totalScore = tuple->GetHypoTotalScore();
    size_t actionId = tuple->GetAction()->GetAction();
    const SuperCat *superCat = tuple->GetAction()->GetSuperCat();
    ShiftReduceAction action(actionId, superCat);

    _action(hypo, actionId, m_lattice, superCat, totalScore,
            tuple->m_action_score, tuple->m_action_class, tuple->m_hypo_h_tm1);
    const ShiftReduceHypothesis *newHypo = m_lattice->GetLatticeArray(m_lattice->GetEndIndex());
    const_cast<ShiftReduceHypothesis *>(newHypo)->m_total_gold_action_count = newHypo->GetPrevHypo()->m_total_gold_action_count;
    assert(newHypo);

    if (m_train && !goldFinished) {

      if (m_use_structure_loss && (!bestHypo ||
          (newHypo->GetTotalScore() + m_loss_pen*(gold_tree_size - newHypo->m_total_gold_action_count))
            > (bestHypo->GetTotalScore() + m_loss_pen*(gold_tree_size - bestHypo->m_total_gold_action_count)) )) {
        bestHypo = newHypo;
      }

      if (!m_use_structure_loss && (!bestHypo || newHypo->GetTotalScore() > bestHypo->GetTotalScore())) {
        assert(m_lattice->GetEndIndex() != 0);
        bestHypo = newHypo;
        assert(bestHypo != 0);
      }

      if (hypo == goldHypo && action == *goldAction) {
        //cerr << "found gold\n";
        goldHypo = newHypo;
        const_cast<ShiftReduceHypothesis *>(newHypo)->m_total_gold_action_count += 1.0;
        m_hasGoldAction = true;
      }

    }
    hypoQueue.pop();
    delete tuple;
  } // end for

  // make this into a function of the PQ class?
  if (!hypoQueue.empty())
    assert(size == m_beamSize);
  else
    assert(size <= m_beamSize);

  while(!hypoQueue.empty()) {
    HypothesisTuple *tuple = hypoQueue.top();
    hypoQueue.pop();
    delete tuple;
  }
  assert(hypoQueue.size() == 0);
}


void RNNParser::_Impl::_action(const ShiftReduceHypothesis *hypo,
                               size_t action,
                               ShiftReduceLattice *lattice,
                               const SuperCat *supercat,
                               double total_score, double action_score,
                               size_t action_class, const mat &new_h_tm1) {
  assert(supercat);
  assert(hypo);
  switch (action)
  {
  case SHIFT:
    m_lattice->Add(m_lattice->GetEndIndex() + 1,
                   hypo->Shift_rnn(m_lattice->GetEndIndex() + 1, supercat, hypo->GetUnaryActionCount(),
                       total_score, action_score, action_class, new_h_tm1));
    m_lattice->IncrementEndIndex();
    break;
  case UNARY:
    m_lattice->Add(m_lattice->GetEndIndex() + 1,
                   hypo->Unary_rnn(m_lattice->GetEndIndex() + 1, supercat, hypo->GetUnaryActionCount() + 1,
                       total_score, action_score, action_class, new_h_tm1));
    m_lattice->IncrementEndIndex();
    break;
  case COMBINE:
    m_lattice->Add(m_lattice->GetEndIndex() + 1,
                   hypo->Combine_rnn(m_lattice->GetEndIndex() + 1, supercat, hypo->GetUnaryActionCount(),
                       total_score, action_score, action_class, new_h_tm1));
    m_lattice->IncrementEndIndex();
    break;
  }
}


void RNNParser::_Impl::_add_lex(const Cat *cat, const SuperCat *sc,
    bool replace, RuleID ruleid,
    vector<SuperCat *> &tmp,
    const size_t tc_id)
{
  SuperCat *new_sc = SuperCat::LexRule(chart.pool, cat, SuperCat::LEX, sc, replace, ruleid);
  new_sc->SetLex();
  assert(new_sc->left != 0);
  new_sc->m_unary_id = tc_id;
  tmp.push_back(new_sc);
}


void RNNParser::_Impl::unary_tc(const SuperCat *sc,
    bool qu_parsing,
    vector<SuperCat *> &tmp)
{
  // tmp will be the union of sc and lex'ed sc
  //tmp.push_back(sc);

  const Cat *cat = sc->cat;

  //cerr << "unary lex: " << cat->out_novar_noX_noNB_SR_str() << endl;

  if(cat->is_N())
    _add_lex(NP, sc, false, 1, tmp, 2);
  else if(cfg.extra_rules.get_value() && cat->is_NP())
    _add_lex(NPfNPbNP, sc, false, 11, tmp, 4);
  else if(cat->is_SbNP()){
    RuleID ruleid = 0;
    switch(cat->res->feature){
    case Features::DCL:
      if(!cfg.extra_rules.get_value())
        //continue;
        break;
      ruleid = 12;
      break;
    case Features::PSS:
      if(cfg.extra_rules.get_value()){
        _add_lex(SbNPbSbNP, sc, false, 17, tmp, 6);
        _add_lex(SfS, sc, false, 13, tmp, 7);
      }
      if(qu_parsing)
        _add_lex(NbN, sc, true, 95, tmp);
      ruleid = 2;
      break;
    case Features::NG:
      _add_lex(SbNPbSbNP, sc, false, 4, tmp, 6);
      _add_lex(SfS, sc, false, 5, tmp, 7);
      if(cfg.extra_rules.get_value()){
        _add_lex(SbNPfSbNP, sc, false, 18, tmp, 5);
        _add_lex(SbS, sc, false, 16, tmp, 8);
        _add_lex(NP, sc, false, 20, tmp, 2);
      }
      ruleid = 3;
      break;
    case Features::ADJ:
      // given this a 93 id to match the gen_lex rule
      _add_lex(SbNPbSbNP, sc, false, 93, tmp, 6);
      if(cfg.extra_rules.get_value())
        _add_lex(SfS, sc, false, 15, tmp, 7);
      if(qu_parsing)
        _add_lex(NbN, sc, true, 94, tmp);
      ruleid = 6;
      break;
    case Features::TO:
      _add_lex(SbNPbSbNP, sc, false, 8, tmp, 6);
      _add_lex(NbN, sc, true, 9, tmp, 1);
      if(cfg.extra_rules.get_value())
        _add_lex(SfS, sc, false, 14, tmp, 7);
      ruleid = 7;
      break;
    default:
      break;
      //continue;
    }
    _add_lex(NPbNP, sc, true, ruleid, tmp, 3);
  }else if(cat->is_SfNP() && cat->res->has_dcl())
    _add_lex(NPbNP, sc, true, 10, tmp, 3);
  else if(cfg.extra_rules.get_value() && cat->is_StobNPfNP())
    _add_lex(NPbNP, sc, true, 19, tmp, 3);
  else if(cfg.extra_rules.get_value() && cat->is_Sdcl()){
    _add_lex(NPbNP, sc, false, 21, tmp, 3);
    _add_lex(SbS, sc, false, 22, tmp, 8);
  }
}


void RNNParser::_Impl::unary_tr(const SuperCat *supercat,
    std::vector<SuperCat *> &tmp)
{
  //const size_t tmpSize = tmp.size();
  //for(size_t i = 0; i < tmpSize; ++i){
  //SuperCat *sc = tmp[i];
  const Cat *cat = supercat->cat;

  const TRCats *trcats = 0;
  if(cat->is_NP())
    trcats = &cats.trNP;
  else if(cat->is_AP())
    trcats = &cats.trAP;
  else if(cat->is_PP())
    trcats = &cats.trPP;
  else if(cat->is_StobNP())
    trcats = &cats.trVP_to;
  else { }

  if (trcats != 0)
  {
    for(TRCats::const_iterator j = trcats->begin(); j != trcats->end(); ++j)
    {
      //cerr << "TypeRaise" << endl;
      SuperCat *new_sc = SuperCat::TypeRaise(chart.pool, *j, SuperCat::TR, supercat, 11);
      assert(new_sc->left);
      new_sc->SetTr();
      new_sc->m_unary_id = j->ind;
      //cerr << "tr cat: " << *new_sc->cat << endl;
      tmp.push_back(new_sc);
    }
  }
  //}
}

void
RNNParser::_Impl::raws2words(const vector<std::string> &raw,
                             Words &words) const {
  words.resize(0);
  words.reserve(sent.words.size());
  for(vector<std::string>::const_iterator i = raw.begin(); i != raw.end(); ++i)
    words.push_back(lexicon[*i]);
}


void
RNNParser::_Impl::pos2pos_inds(const vector<std::string> &raw,
                               std::vector<size_t> &pos_inds_vec) const {
  pos_inds_vec.resize(0);
  pos_inds_vec.reserve(sent_rnn_fmt.words.size());
  unordered_map<string, size_t>::const_iterator iter;

  for(vector<std::string>::const_iterator i = raw.begin(); i != raw.end(); ++i) {
    iter = m_pos_str_ind_map.find(*i);
    assert(iter != m_pos_str_ind_map.end());
    assert(iter->second >= 1);
    pos_inds_vec.push_back(iter->second);
  }

    // test
//    cerr << "pos inds_vec: " << endl;
//    for (size_t i = 0; i < pos_inds_vec.size(); ++i) {
//      cerr << pos_inds_vec[i] << " " << endl;
//    }
//    cerr << endl;
}


void
RNNParser::_Impl::words2word_inds(const vector<std::string> &raw,
                                  std::vector<size_t> &word_inds_vec) const {
  word_inds_vec.resize(0);
  word_inds_vec.reserve(sent_rnn_fmt.words.size());
  for(vector<std::string>::const_iterator i = raw.begin(); i != raw.end(); ++i)
    word_inds_vec.push_back(std::stoi(*i));

    // test
//    cerr << "word inds_vec: " << endl;
//    for (size_t i = 0; i < word_inds_vec.size(); ++i) {
//      cerr << word_inds_vec[i] << " " << endl;
//    }
//    cerr << endl;
}


void
RNNParser::_Impl::suf_cap2suf_cap_inds(const vector<std::string> &raw,
                                       std::vector<size_t> &suf_inds_vec,
                                       std::vector<size_t> &cap_inds_vec) const {
  suf_inds_vec.resize(0);
  suf_inds_vec.reserve(sent_rnn_fmt.words.size());
  cap_inds_vec.resize(0);
  cap_inds_vec.reserve(sent_rnn_fmt.words.size());
  for(vector<std::string>::const_iterator i = raw.begin(); i != raw.end(); ++i) {
    string temp = *i;
    size_t pos = temp.find('_');
    size_t suf = stoi(temp.substr(0, pos));
    size_t cap = stoi(temp.substr(pos + 1));
    suf_inds_vec.push_back(suf);
    cap_inds_vec.push_back(cap);
  }

  assert(suf_inds_vec.size() == cap_inds_vec.size());

  //  cerr << "suf_inds_vec: " << endl;
  //
  //  // test
  //  for (size_t i = 0; i < suf_inds_vec.size(); ++i) {
  //    cerr << suf_inds_vec[i] << " " << endl;
  //  }
  //  cerr << endl;
  //
  //  cerr << "cap_inds_vec: " << endl;
  //
  //  for (size_t i = 0; i < cap_inds_vec.size(); ++i) {
  //    cerr << cap_inds_vec[i] << " " << endl;
  //  }
  //  cerr << endl;

}

void
RNNParser::_Impl::test_contextwin2minibatch() {
  vector<size_t> words;
  vector<size_t> sufs;
  vector<size_t> caps;

  words.push_back(1);
  sufs.push_back(2);
  caps.push_back(3);

  vector<vector<size_t> > context1;
  vector<vector<size_t> > context2;
  vector<vector<size_t> > context3;
  vector<vector<size_t> > context4;
  vector<vector<size_t> > context5;
  vector<vector<size_t> > context6;

  context1.push_back(words);
  context2.push_back(words);
  context3.push_back(words);
  context4.push_back(words);
  context5.push_back(words);
  context6.push_back(words);
  context1.push_back(sufs);
  context2.push_back(sufs);
  context3.push_back(sufs);
  context4.push_back(sufs);
  context5.push_back(sufs);
  context6.push_back(sufs);
  context1.push_back(caps);
  context2.push_back(caps);
  context3.push_back(caps);
  context4.push_back(caps);
  context5.push_back(caps);
  context6.push_back(caps);

  vector<vector<vector<size_t> > > all_context;
  all_context.push_back(context1);
  all_context.push_back(context2);
  all_context.push_back(context3);
  all_context.push_back(context4);
  all_context.push_back(context5);
  //all_context.push_back(context6);

  vector<vector<vector<vector<vector<size_t> > > > > minibatch_all_data;
  contextwin2minibatch(all_context, minibatch_all_data);

  for (size_t i = 0; i < minibatch_all_data[0].size(); ++i) {
    cerr << "minibatch: " << i << endl;
    for (size_t j = 0; j < minibatch_all_data[0][i].size(); ++j) {
      // for all context in this minibatch
      for (size_t k = 0; k < minibatch_all_data[0][i][j].size(); ++k) {
        // for a context
        for (size_t l = 0; l < minibatch_all_data[0][i][j][k].size(); ++l) {
          cerr << minibatch_all_data[0][i][j][k][0] << ",";
        }
        cerr << " ";
      }
      cerr << endl;
    }
  }
}


void
RNNParser::_Impl::test_labels2minibatch() {

  vector<size_t> all_labels;
  all_labels.push_back(0);
  all_labels.push_back(1);
  all_labels.push_back(2);
  all_labels.push_back(3);
  all_labels.push_back(4);
  all_labels.push_back(5);
  all_labels.push_back(6);

  vector<vector<vector<size_t> > > minibatch_all_data;
  labels2minibatch(all_labels, minibatch_all_data);

  for (size_t i = 0; i < minibatch_all_data[0].size(); ++i) {
    cerr << "minibatch: " << i << endl;
    for (size_t j = 0; j < minibatch_all_data[0][i].size(); ++j) {
        cerr << minibatch_all_data[0][i][j] << ",";
    }
    cerr << endl;
  }
}


void
RNNParser::_Impl::contextwin2minibatch(const vector<vector<vector<size_t> > > &sent_all_context,
                                       vector<vector<vector<vector<vector<size_t> > > > > &minibatch_all_data) {

  vector<vector<vector<size_t> > > minibatch;
  vector<vector<vector<vector<size_t> > > > minibatch_sent;

  for (size_t i = 0; i < sent_all_context.size(); ++i) {
    minibatch.push_back(sent_all_context[i]);
    if ((i + 1) % m_bs == 0 || i == sent_all_context.size() - 1) {
      minibatch_sent.push_back(minibatch);
      minibatch.clear();
    }
  }

  minibatch_all_data.push_back(minibatch_sent);
}


void
RNNParser::_Impl::labels2minibatch(const vector<size_t> &sent_all_labels,
                                   vector<vector<vector<size_t> > > &minibatch_all_data) {

  vector<size_t> minibatch;
  vector<vector<size_t> > minibatch_sent;

  for (size_t i = 0; i < sent_all_labels.size(); ++i) {
    minibatch.push_back(sent_all_labels[i]);
    if ((i + 1) % m_bs == 0 || i == sent_all_labels.size() - 1) {
      minibatch_sent.push_back(minibatch);
      minibatch.clear();
    }
  }

  minibatch_all_data.push_back(minibatch_sent);
}


void
RNNParser::_Impl::feasible_y_vals2minibatch(const vector<vector<size_t> > &sent_all_feasible_y_vals,
                                            vector<vector<vector<vector<size_t> > > > &minibatch_all_data) {

  vector<vector<size_t> > minibatch;
  vector<vector<vector<size_t> > > minibatch_sent;

  for (size_t i = 0; i < sent_all_feasible_y_vals.size(); ++i) {
    minibatch.push_back(sent_all_feasible_y_vals[i]);
    if ((i + 1) % m_bs == 0 || i == sent_all_feasible_y_vals.size() - 1) {
      minibatch_sent.push_back(minibatch);
      minibatch.clear();
    }
  }

  minibatch_all_data.push_back(minibatch_sent);
}


//void
//RNNParser::_Impl::train(const size_t total_epochs, const string &model_path) {
//
////  test_contextwin2minibatch();
////  test_labels2minibatch();
//
//  assert(m_all_sent_context_vec_train.size() ==
//         m_all_sent_target_vec_train.size());
//
//  vector<vector<vector<vector<vector<size_t> > > > > all_sent_minibatch_context_vec_train;
//  vector<vector<vector<size_t> > > all_sent_minibatch_label_train;
//  vector<vector<vector<vector<size_t> > > > all_sent_minibatch_feasible_y_vals_train;
//
//  for (size_t i = 0; i < m_all_sent_context_vec_train.size(); ++i) {
//
//    contextwin2minibatch(m_all_sent_context_vec_train[i],
//                         all_sent_minibatch_context_vec_train);
//
//    labels2minibatch(m_all_sent_target_vec_train[i],
//                     all_sent_minibatch_label_train);
//
//    feasible_y_vals2minibatch(m_all_sent_feasible_y_vals_train[i],
//                              all_sent_minibatch_feasible_y_vals_train);
//  }
//
//  assert(all_sent_minibatch_context_vec_train.size() ==
//         m_all_sent_context_vec_train.size());
//  assert(all_sent_minibatch_context_vec_train.size() ==
//      all_sent_minibatch_label_train.size());
//  assert(all_sent_minibatch_feasible_y_vals_train.size() ==
//      all_sent_minibatch_label_train.size());
//
//  vector<size_t> train_data_inds;
//  for (size_t i = 0; i < m_all_sent_context_vec_train.size(); ++i) {
//    train_data_inds.push_back(i);
//  }
//
//  time_t tstart;
//  time_t tend;
//  double total_err = 0.0;
//  time(&tstart);
//
//  size_t sent = 0;
//  for (size_t i = 0; i < total_epochs; ++i) {
//
//    cerr << "bptt training epoch: " << i << endl;
//
//    std::mt19937 engine;
//
//    if (i > 0)
//      shuffle(begin(train_data_inds), end(train_data_inds), engine);
//
//    time(&tstart);
//    total_err = 0.0;
//
//    for (size_t s = 0; s < train_data_inds.size(); ++s) {
//      sent = train_data_inds[s];
//      cerr << "sent: " << sent << endl;
//
//      // reset hidden states
//      m_h_tm1.zeros();
//
//      assert(m_all_sent_context_vec_train[sent].size() ==
//             m_all_sent_target_vec_train[sent].size());
//
//      assert(m_all_sent_feasible_y_vals_train[sent].size() ==
//             m_all_sent_target_vec_train[sent].size());
//
//      // number of minibatches should be equal
//      assert(all_sent_minibatch_context_vec_train[sent].size() ==
//             all_sent_minibatch_label_train[sent].size());
//
//      assert(all_sent_minibatch_label_train[sent].size() ==
//             all_sent_minibatch_feasible_y_vals_train[sent].size());
//
//      for (size_t m = 0; m < all_sent_minibatch_context_vec_train[sent].size(); ++m) {
//
//        // size of each minibatch should be equal
//        assert(all_sent_minibatch_context_vec_train[sent][m].size() ==
//               all_sent_minibatch_label_train[sent][m].size());
//
//        assert(all_sent_minibatch_context_vec_train[sent][m].size() ==
//               all_sent_minibatch_feasible_y_vals_train[sent][m].size());
//
////   // grad check
////        for (size_t i = 0; i < 6; ++i) {
////          train_bptt_multi_emb_grad_check(all_sent_minibatch_context_vec_train[sent][m],
////                                          all_sent_minibatch_label_train[sent][m],
////                                          all_sent_minibatch_feasible_y_vals_train[sent][m], i);
////        }
//
//        // bptt training
//        total_err += train_bptt_multi_emb(all_sent_minibatch_context_vec_train[sent][m],
//                                          all_sent_minibatch_label_train[sent][m],
//                                          all_sent_minibatch_feasible_y_vals_train[sent][m]);
//      }
//
//    }
//
//
//    time(&tend);
//    cerr << "epoch " << i << " total err: " << total_err << endl;
//    cerr << ">> epoch time: " << difftime(tend, tstart) << endl;
//
//
//    m_wx.save(model_path + "wx." + to_string(i) + ".mat");
//    m_wh.save(model_path + "wh." + to_string(i) + ".mat");
//    m_wy.save(model_path + "wy." + to_string(i) + ".mat");
//    m_suf.save(model_path + "suf." + to_string(i) + ".mat");
//    //m_cap.save(model_path + "cap." + to_string(i) + ".mat");
//    m_emb.save(model_path + "emb." + to_string(i) + ".mat");
//
//    if (m_use_supercat_feats)
//      m_supercat_emb.save(model_path + "super_emb." + to_string(i) + ".mat");
//
//    dump_supercat_map(model_path + "supercat_map." + to_string(i));
//    // each epoch gets a different supercat_map. This is for future-proof
//    // purposes when training with beam-search and seen_rules flag off.
//    // Because with seen_rules off, categories seen in later epochs may
//    // not have been seen in earlier epochs (some items will get pruned by a beam),
//    // thus seen and unseen categories should be strictly separated at test time by using
//    // different models for testing after different epochs.
//  }
//
//}

void
RNNParser::_Impl::load_supercat_map(const string &file) {

  cerr << "loading supercat_map\n";

  ifstream in(file.c_str());
  if(!in) {
     cerr << "could not open features file " << file << endl;
     exit(EXIT_FAILURE);
  }

  string cat;
  size_t ind;
  size_t total = 0;
  while (in >> cat >> ind) {
    ++total;
    m_cats_map.add(cats.parse(cat), ind);
  }

  cerr << "total supercat count (from RNN parser training features): " << total << endl;
}


void
RNNParser::_Impl::load_pos_str_ind_map(const string &file) {

  cerr << "loading pos str ind map\n";

  ifstream in(file.c_str());
  if(!in) {
     cerr << "could not open pos_str_ind file " << file << endl;
     exit(EXIT_FAILURE);
  }

  string pos;
  size_t ind;
  size_t total = 0;

  while (in >> pos >> ind) {
    ++total;
    if (total == 1)
      assert(ind == 1); // pos ind should start from 1, 0 is reserved for empty_pos feature
    m_pos_str_ind_map.insert(make_pair(pos, ind));
  }

  cerr << "total pos tag count (from sec0221): " << total << endl;

}


void
RNNParser::_Impl::dump_supercat_map(const string &file) {
  size_t total_cats = 0;
  ofstream catmap_output;
  catmap_output.open(file.c_str());
  total_cats = m_cats_map.dump(catmap_output);
  catmap_output.close();
  cerr << "total supercat count: " << total_cats << endl;
}


void
RNNParser::_Impl::calc_lh_proj_emb_train_beam(const vector<vector<size_t> > &context,
                                              colvec &current_hypo_converted_context_vec,
                                              mat &current_hypo_h_tm1,
                                              mat &new_h_tm1) {
  //colvec x;
  colvec x0;
  colvec x1;
  colvec x2;

  assert(context[0].size() == context[1].size());

  const vector<size_t> &word_inds = context[0];
  const vector<size_t> &suf_inds = context[1];

  for (size_t i = 0; i < word_inds.size(); ++i) {
    x0 = join_cols(x0, m_emb.col(word_inds[i]));
    x1 = join_cols(x1, m_suf.col(suf_inds[i]));
  }

  if (m_use_supercat_feats) {
    const vector<size_t> &super_inds = context[2];

    for (size_t i = 0; i < super_inds.size(); ++i) {
      x2 = join_cols(x2, m_supercat_emb.col(super_inds[i]));

    }

    current_hypo_converted_context_vec = join_cols(join_cols(x0, x1), x2);

  } else {

    current_hypo_converted_context_vec = join_cols(x0, x1);

  }

  //x_vals[step] = x;

  new_h_tm1 = sigmoid(current_hypo_converted_context_vec.t()*m_wx + current_hypo_h_tm1*m_wh);
  //mat one = ones<mat>(lh.n_rows, lh.n_cols);
  //mat lh_deriv = lh%(one - lh);

  //pair<mat, mat> res(lh, lh_deriv);
  //return lh;
}



pair<mat, mat>
RNNParser::_Impl::calc_lh_proj_emb_grad_check(int step,
                                              const vector<vector<size_t> > &context,
                                              mat *x_vals, mat &h_tm1, mat &wx, mat &wh,
                                              mat &emb, mat &suf, mat &cap, mat &super_emb) {
  colvec x;
  colvec x0;
  colvec x1;
  colvec x2;

  assert(context[0].size() == context[1].size());

  const vector<size_t> &word_inds = context[0];
  const vector<size_t> &suf_inds = context[1];

  assert(word_inds.size() == suf_inds.size());

  for (size_t i = 0; i < word_inds.size(); ++i) {
    x0 = join_cols(x0, emb.col(word_inds[i]));
    x1 = join_cols(x1, suf.col(suf_inds[i]));
  }

  if (m_use_supercat_feats) {

    const vector<size_t> &supercat_inds = context[2];
    for (size_t i = 0; i < supercat_inds.size(); ++i) {
      x2 = join_cols(x2, super_emb.col(supercat_inds[i]));
    }

    x = join_cols(join_cols(x0, x1), x2);


  } else {
    x = join_cols(x0, x1);
  }

  x_vals[step] = x;

  mat lh = sigmoid(x.t()*wx + h_tm1*wh);
  mat one = ones<mat>(lh.n_rows, lh.n_cols);
  mat lh_deriv = lh%(one - lh);

  pair<mat, mat> res(lh, lh_deriv);
  return res;
}


// grad check
void
RNNParser::_Impl::train_bptt_multi_emb_grad_check(const vector<vector<vector<size_t> > > &context_batch,
    const vector<size_t> &label_batch,
    const vector<vector<size_t> > &feasible_y_vals_batch,
    size_t check) {

  const double eps = 0.0001;

  size_t steps = context_batch.size();
  double err_plus = 0.0;
  double err_minus = 0.0;

  mat lh_vals_plus[steps + 1];
  lh_vals_plus[0] = m_h_tm1;
  mat ly_vals_plus[steps];
  mat x_vals_plus[steps];

  mat lh_vals_minus[steps + 1];
  lh_vals_minus[0] = m_h_tm1;
  mat ly_vals_minus[steps];
  mat x_vals_minus[steps];

  mat all_output_vals_feasible_plus[steps];
  mat all_output_vals_feasible_minus[steps];

  vector<size_t> gold_labels_ind_among_feasible;

  if (check == 0) {

    mat wx_plus = m_wx;
    wx_plus(0,0) += eps;
    mat wx_minus = m_wx;
    wx_minus(0,0) -= eps;

    for (size_t i = 0; i < steps; ++i) {
      pair<mat,  mat> vals_plus =
          calc_lh_proj_emb_grad_check(i, context_batch[i], x_vals_plus,
              lh_vals_plus[i], wx_plus, m_wh, m_emb,
              m_suf, m_cap, m_supercat_emb);
      mat lh_plus = vals_plus.first;
      ly_vals_plus[i] = lh_plus*m_wy; // no s oftmax here anymore
      lh_vals_plus[i + 1] = lh_plus;

      pair<mat,  mat> vals_minus =
          calc_lh_proj_emb_grad_check(i, context_batch[i], x_vals_minus,
              lh_vals_minus[i], wx_minus, m_wh, m_emb,
              m_suf, m_cap, m_supercat_emb);
      mat lh_minus = vals_minus.first;
      ly_vals_minus[i] = lh_minus*m_wy; // no softmax here anymore
      lh_vals_minus[i + 1] = lh_minus;
    }

  } else if (check == 1) {

    mat wh_plus = m_wh;
    wh_plus(0,0) += eps;
    mat wh_minus = m_wh;
    wh_minus(0,0) -= eps;

    for (size_t i = 0; i < steps; ++i) {
      pair<mat,  mat> vals_plus =
          calc_lh_proj_emb_grad_check(i, context_batch[i], x_vals_plus,
              lh_vals_plus[i], m_wx, wh_plus, m_emb, m_suf,
              m_cap, m_supercat_emb);
      mat lh_plus = vals_plus.first;
      ly_vals_plus[i] = lh_plus*m_wy; // no softmax here anymore
      lh_vals_plus[i + 1] = lh_plus;

      pair<mat,  mat> vals_minus =
          calc_lh_proj_emb_grad_check(i, context_batch[i], x_vals_minus,
              lh_vals_minus[i], m_wx, wh_minus, m_emb,
              m_suf, m_cap, m_supercat_emb);
      mat lh_minus = vals_minus.first;
      ly_vals_minus[i] = lh_minus*m_wy; // no softmax here anymore
      lh_vals_minus[i + 1] = lh_minus;
    }

  } else if (check == 2) {

    mat wy_plus = m_wy;
    wy_plus(0,0) += eps;
    mat wy_minus = m_wy;
    wy_minus(0,0) -= eps;

    for (size_t i = 0; i < steps; ++i) {
      pair<mat,  mat> vals_plus =
          calc_lh_proj_emb_grad_check(i, context_batch[i], x_vals_plus,
              lh_vals_plus[i], m_wx, m_wh,
              m_emb, m_suf, m_cap, m_supercat_emb);
      mat lh_plus = vals_plus.first;
      ly_vals_plus[i] = lh_plus*wy_plus; // no softmax here anymore
      lh_vals_plus[i + 1] = lh_plus;

      pair<mat,  mat> vals_minus =
          calc_lh_proj_emb_grad_check(i, context_batch[i], x_vals_minus,
              lh_vals_minus[i], m_wx, m_wh, m_emb,
              m_suf, m_cap, m_supercat_emb);
      mat lh_minus = vals_minus.first;
      ly_vals_minus[i] = lh_minus*wy_minus; // no softmax here anymore
      lh_vals_minus[i + 1] = lh_minus;
    }

  } else if (check == 3) {

    mat emb_plus = m_emb;
    emb_plus(0,10082) += eps;
    mat emb_minus = m_emb;
    emb_minus(0,10082) -= eps;

    for (size_t i = 0; i < steps; ++i) {
      pair<mat,  mat> vals_plus =
          calc_lh_proj_emb_grad_check(i, context_batch[i], x_vals_plus,
              lh_vals_plus[i], m_wx, m_wh, emb_plus,
              m_suf, m_cap, m_supercat_emb);
      mat lh_plus = vals_plus.first;
      ly_vals_plus[i] = lh_plus*m_wy; // no softmax here anymore
      lh_vals_plus[i + 1] = lh_plus;

      pair<mat,  mat> vals_minus =
          calc_lh_proj_emb_grad_check(i, context_batch[i], x_vals_minus,
              lh_vals_minus[i], m_wx, m_wh, emb_minus,
              m_suf, m_cap, m_supercat_emb);
      mat lh_minus = vals_minus.first;
      ly_vals_minus[i] = lh_minus*m_wy; // no softmax here anymore
      lh_vals_minus[i + 1] = lh_minus;
    }

  } else if (check == 4) {

    mat suf_plus = m_suf;
    suf_plus(0,1) += eps;
    mat suf_minus = m_suf;
    suf_minus(0,1) -= eps;

    for (size_t i = 0; i < steps; ++i) {
      pair<mat,  mat> vals_plus =
          calc_lh_proj_emb_grad_check(i, context_batch[i], x_vals_plus,
              lh_vals_plus[i], m_wx, m_wh, m_emb,
              suf_plus, m_cap, m_supercat_emb);
      mat lh_plus = vals_plus.first;
      ly_vals_plus[i] = lh_plus*m_wy; // no softmax here anymore
      lh_vals_plus[i + 1] = lh_plus;

      pair<mat,  mat> vals_minus =
          calc_lh_proj_emb_grad_check(i, context_batch[i], x_vals_minus,
              lh_vals_minus[i], m_wx, m_wh, m_emb,
              suf_minus, m_cap, m_supercat_emb);
      mat lh_minus = vals_minus.first;
      ly_vals_minus[i] = lh_minus*m_wy; // no softmax here anymore
      lh_vals_minus[i + 1] = lh_minus;
    }

//  } else if (check == 5) {
//
//    mat cap_plus = m_cap;
//    cap_plus(0,0) += eps;
//    mat cap_minus = m_cap;
//    cap_minus(0,0) -= eps;
//
//    for (size_t i = 0; i < steps; ++i) {
//      pair<mat,  mat> vals_plus =
//          calc_lh_proj_emb_grad_check(i, context_batch[i], x_vals_plus,
//              lh_vals_plus[i], m_wx, m_wh, m_emb,
//              m_suf, cap_plus, m_supercat_emb);
//      mat lh_plus = vals_plus.first;
//      ly_vals_plus[i] = lh_plus*m_wy; // no softmax here anymore
//      lh_vals_plus[i + 1] = lh_plus;
//
//      pair<mat,  mat> vals_minus =
//          calc_lh_proj_emb_grad_check(i, context_batch[i], x_vals_minus,
//              lh_vals_minus[i], m_wx, m_wh, m_emb,
//              m_suf, cap_minus, m_supercat_emb);
//      mat lh_minus = vals_minus.first;
//      ly_vals_minus[i] = lh_minus*m_wy; // no softmax here anymore
//      lh_vals_minus[i + 1] = lh_minus;
//    }
//
  } else if (check == 5) {

    mat super_emb_plus = m_supercat_emb;
    super_emb_plus(0, 3) += eps;
    mat super_emb_minus = m_supercat_emb;
    super_emb_minus(0, 3) -= eps;

    for (size_t i = 0; i < steps; ++i) {
      pair<mat,  mat> vals_plus =
          calc_lh_proj_emb_grad_check(i, context_batch[i], x_vals_plus,
              lh_vals_plus[i], m_wx, m_wh, m_emb,
              m_suf, m_cap, super_emb_plus);
      mat lh_plus = vals_plus.first;
      ly_vals_plus[i] = lh_plus*m_wy; // no softmax here anymore
      lh_vals_plus[i + 1] = lh_plus;

      pair<mat,  mat> vals_minus =
          calc_lh_proj_emb_grad_check(i, context_batch[i], x_vals_minus,
              lh_vals_minus[i], m_wx, m_wh, m_emb,
              m_suf, m_cap, super_emb_minus);
      mat lh_minus = vals_minus.first;
      ly_vals_minus[i] = lh_minus*m_wy; // no softmax here anymore
      lh_vals_minus[i + 1] = lh_minus;
    }

  }

  // dim of ly is 1xn, set the dim of delta_y
  // to nx1 without doing transpose later
  for (size_t i = 0; i < steps; ++i) {

    const vector<size_t> &feasible_y_vals = feasible_y_vals_batch[i];

    mat output_vals_feasible_plus(1, feasible_y_vals.size(), fill::zeros);
    mat output_vals_feasible_minus(1, feasible_y_vals.size(), fill::zeros);

    bool found_gold = false;
    for (size_t j = 0; j < feasible_y_vals.size(); ++j) {
      //cerr << "feasible_y_vals[j]" << feasible_y_vals[j] << endl;
      output_vals_feasible_plus(j) = ly_vals_plus[i](feasible_y_vals[j]);
      output_vals_feasible_minus(j) = ly_vals_minus[i](feasible_y_vals[j]);

      if (feasible_y_vals[j] == label_batch[i]) {
        found_gold = true;
        gold_labels_ind_among_feasible.push_back(j);
      }
    }
    assert(found_gold);
    // softmax over feasible actions
    output_vals_feasible_plus = exp(output_vals_feasible_plus)
                                    / accu(exp(output_vals_feasible_plus));

    output_vals_feasible_minus = exp(output_vals_feasible_minus)
                                    / accu(exp(output_vals_feasible_minus));

    all_output_vals_feasible_plus[i] = output_vals_feasible_plus;
    all_output_vals_feasible_minus[i] = output_vals_feasible_minus;

  }
  assert(gold_labels_ind_among_feasible.size() == steps);

  for (size_t i = 0; i < steps; ++i) {
    mat gold_dist(1, feasible_y_vals_batch[i].size(), fill::zeros);
    gold_dist(gold_labels_ind_among_feasible[i]) = 1.0;
    err_plus += x_ent_multi(all_output_vals_feasible_plus[i], gold_dist);
    err_minus += x_ent_multi(all_output_vals_feasible_minus[i], gold_dist);
  }

  double grad = (err_plus - err_minus)/(2.0*eps);
  cerr << "numerical grad: " << grad << endl;

}

// bptt multi bs number of steps, each step corresponds to a contextwin
//double
//RNNParser::_Impl::train_bptt_multi_emb(const vector<vector<vector<size_t> > > &context_batch,
//                                       const vector<size_t> &label_batch,
//                                       const vector<vector<size_t> > &feasible_y_vals_batch) {
//
//  // test
//  //  for (size_t i = 0; i < context_batch.size(); ++i) {
//  //    for (size_t j = 0; j < context_batch[i].size(); ++j) {
//  //      for (size_t k = 0; k < context_batch[i][j].size(); ++k) {
//  //        cerr << context_batch[i][j][k] << " ";
//  //      }
//  //      cerr << endl;
//  //    }
//  //    cerr << endl;
//  //  }
//
//  double err = 0.0;
//  size_t steps = context_batch.size();
//
//  mat lh_vals[steps + 1];
//  lh_vals[0] = m_h_tm1;
//  mat lh_deriv_vals[steps];
//  mat ly_vals[steps];
//  mat x_vals[steps];
//
//  mat delta_y_vals[steps];
//  mat delta_h_vals[steps];
//  mat delta_x_vals[steps];
//
//  mat all_output_vals_feasible[steps];
//  vector<size_t> gold_labels_ind_among_feasible;
//
//  for (size_t i = 0; i < steps; ++i) {
//
//    pair<mat,  mat> vals =
//        calc_lh_proj_emb(i, context_batch[i], x_vals, lh_vals[i]);
//
//    mat lh = vals.first;
//    mat lh_deriv = vals.second;
//
//    ly_vals[i] = lh*m_wy; // no softmax here anymore
//    lh_vals[i + 1] = lh;
//    lh_deriv_vals[i] = lh_deriv;
//
//  }
//
//  // dim of ly is 1xn, set the dim of delta_y
//  // to nx1 without doing transpose later
//  for (size_t i = 0; i < steps; ++i) {
//
//    delta_y_vals[i] = zeros<mat>(ly_vals[i].n_cols, 1);
//
//    const vector<size_t> &feasible_y_vals = feasible_y_vals_batch[i];
//    mat output_vals_feasible(1, feasible_y_vals.size(), fill::zeros);
//
//    bool found_gold = false;
//    for (size_t j = 0; j < feasible_y_vals.size(); ++j) {
//
//      output_vals_feasible(j) = ly_vals[i](feasible_y_vals[j]);
//
//      if (feasible_y_vals[j] == label_batch[i]) {
//        found_gold = true;
//        gold_labels_ind_among_feasible.push_back(j);
//      }
//    }
//    assert(found_gold);
//
//    // softmax over feasible actions
//    output_vals_feasible = exp(output_vals_feasible)
//                           / accu(exp(output_vals_feasible));
//    all_output_vals_feasible[i] = output_vals_feasible;
//
//    // each delta_y_vals have the same number of elements as ly
//    // the infeasible units will just get zero values
//    for (size_t j = 0; j < feasible_y_vals.size(); ++j) {
//      delta_y_vals[i](feasible_y_vals[j]) = output_vals_feasible(j);
//    }
//  }
//
//  assert(gold_labels_ind_among_feasible.size() == steps);
//
//  for (size_t i = 0; i < steps; ++i) {
//    mat gold_dist(1, feasible_y_vals_batch[i].size(), fill::zeros);
//    gold_dist(gold_labels_ind_among_feasible[i]) = 1.0;
//    err += x_ent_multi(all_output_vals_feasible[i], gold_dist);
//    delta_y_vals[i](label_batch[i]) -= 1.0;
//  }
//
//  // bptt multi
//  delta_h_vals[steps - 1] = lh_deriv_vals[steps - 1].t()
//                               % (m_wy*delta_y_vals[steps - 1]);
//
//  for (size_t i = steps; i > 1; --i) {
//    delta_h_vals[i - 2] = lh_deriv_vals[i - 2].t() %
//        ((m_wh*delta_h_vals[i - 1]) + m_wy*(delta_y_vals[i - 2]));
//  }
//
//  //double back_grad_wh = 0.0;
//  for (size_t i = 0; i < steps; ++i) {
//    mat grad = lh_vals[i].t()*delta_h_vals[i].t();
//    m_wh -= m_lr*grad;
//    //back_grad_wh += grad(0,0);
//  }
//
//  //double back_grad_wy = 0.0;
//  for (size_t i = 1; i <= steps; ++i) {
//    mat grad = lh_vals[i].t()*delta_y_vals[i - 1].t();
//    m_wy -= m_lr*grad;
//    //back_grad_wy += grad(0,0);
//  }
//
//  for (size_t i = 0; i < steps; ++i) {
//    delta_x_vals[i] = m_wx*delta_h_vals[i];
//  }
//
//  //double back_grad_wx = 0.0;
//  for (size_t i = 0; i < steps; ++i) {
//    mat grad = x_vals[i]*delta_h_vals[i].t();
//    m_wx -= m_lr*grad;
//    //back_grad_wx += grad(0,0);
//  }
//
//  // grad check
////  double bp_grad_emb = 0.0;
////  double bp_grad_suf = 0.0;
////  double bp_grad_cap = 0.0;
////  double bp_grad_super = 0.0;
//
//  for (size_t i = 0; i < steps; ++i) {
//
//    const vector<size_t> &word_inds = context_batch[i][0];
//    const vector<size_t> &suf_inds = context_batch[i][1];
//
//    assert(word_inds.size() == suf_inds.size());
//
//    // all start inds must be set here
//    int start = 0;
//    int start2 = m_de*word_inds.size(); // dim of word emb x num of embs
//    int start3 = start2 + m_ds*word_inds.size();
//
//    for (size_t k = 0; k < word_inds.size(); ++k) {
//      m_emb.col(word_inds[k]) -=
//          m_lr*delta_x_vals[i].rows(start, start + m_de - 1);
////      if (word_inds[k] == 10082)
////        bp_grad_emb += delta_x_vals[i].rows(start, start + m_de - 1)[0];
//      m_suf.col(suf_inds[k]) -=
//          m_lr*delta_x_vals[i].rows(start2, start2 + m_ds - 1);
////      if (suf_inds[k] == 1)
////        bp_grad_suf += delta_x_vals[i].rows(start2, start2 + m_ds - 1)[0];
////      m_cap.col(cap_inds[k]) -=
////          m_lr*delta_x_vals[i].rows(start3, start3 + m_dc - 1);
////      if (cap_inds[k]  == 0)
////        bp_grad_cap += delta_x_vals[i].rows(start3, start3 + m_dc - 1)[0];
//
//      start += m_de;
//      start2 += m_ds;
//    }
//
//    if (m_use_supercat_feats) {
//
//      assert(context_batch[i].size() >= 3);
//      assert(start2 == start3);
//
//      const vector<size_t> &supercat_inds = context_batch[i][2];
//
//      for (size_t k = 0; k < supercat_inds.size(); ++k) {
//        m_supercat_emb.col(supercat_inds[k]) -=
//            m_lr*delta_x_vals[i].rows(start3, start3 + m_dsuper - 1);
////        if (supercat_inds[k] == 3)
////          bp_grad_super += delta_x_vals[i].rows(start3, start3 + m_dsuper - 1)[0];
//
//        start3 += m_dsuper;
//      }
//
//    }
//
//
//  }
//
//  //grad check
////  cerr << "backprop grad wx: " << back_grad_wx << endl;
////  cerr << "backprop grad wh: " << back_grad_wh << endl;
////  cerr << "backprop grad wy: " << back_grad_wy << endl;
//////
////  cerr << "backprop grad emb: " << bp_grad_emb << endl;
////  cerr << "backprop grad suf: " << bp_grad_suf << endl;
//////  cerr << "backprop grad cap: " << bp_grad_cap << endl;
////  cerr << "backprop grad super: " << bp_grad_super << endl;
//
//  m_h_tm1 = lh_vals[steps];
//  return err;
//}


mat
RNNParser::_Impl::sigmoid(const mat &m) {
  mat one = ones<mat>(m.n_rows, m.n_cols);
  return one/(one + exp(-m));
}


mat
RNNParser::_Impl::soft_max(const mat &y) {
  mat res = exp(y)/accu(exp(y));
  return res;
}


double
RNNParser::_Impl::x_ent_multi(const mat&y, const mat&t) {
  return -accu(t % log(y));
}


void RNNParser::_Impl::save_mats(const string &model_path, const size_t i) {

  m_wx.save(model_path + "wx." + to_string(i) + ".mat");
  m_wh.save(model_path + "wh." + to_string(i) + ".mat");
  m_wy.save(model_path + "wy." + to_string(i) + ".mat");
  m_suf.save(model_path + "suf." + to_string(i) + ".mat");
  //m_cap.save(model_path + "cap." + to_string(i) + ".mat");
  m_emb.save(model_path + "emb." + to_string(i) + ".mat");

  if (m_use_supercat_feats)
    m_supercat_emb.save(model_path + "super_emb." + to_string(i) + ".mat");

  dump_supercat_map(model_path + "supercat_map." + to_string(i));
  // each epoch gets a different supercat_map. This is for future-proof
  // purposes when training with beam-search and seen_rules flag off.
  // Because with seen_rules off, categories seen in later epochs may
  // not have been seen in earlier epochs (some items will get pruned by a beam),
  // thus seen and unseen categories should be strictly separated at test time by using
  // different models for testing after different epochs.

}


const ShiftReduceHypothesis*
RNNParser::_Impl::sr_parse_train_and_test_beam(const double BETA, const size_t sent_id) {
  try {

    // reset hidden states
    m_h_tm1.zeros();

    vector<ShiftReduceAction*> null_vec;
    vector<pair<size_t, size_t> > null_vec2;
    const ShiftReduceHypothesis *best_hypo; // the highest-scored hypo on each level
    const ShiftReduceHypothesis *gold_hypo;
    vector<ShiftReduceAction*> &gold_tree_actions = m_train ? *m_goldTreeVec.at(sent_id) : null_vec;
    vector<pair<size_t, size_t> > &gold_tree_actions_rnn_fmt = null_vec2;
      if (m_train)
        gold_tree_actions_rnn_fmt = m_gold_tree_vec_rnn[sent_id];
    size_t gold_tree_size = gold_tree_actions.size();
    assert(gold_tree_actions.size() == gold_tree_actions_rnn_fmt.size());

    ShiftReduceAction *gold_action = 0;
    pair<size_t, size_t> gold_action_rnn_fmt;
    size_t gold_action_id = 0;

    bool gold_finished = false;

    //size_t total_shift_count = 0;

    if(sent.words.size() > cfg.maxwords())
      throw NLP::ParseError("sentence length exceeds maximum number of words for parser", nsentences);

    const size_t NWORDS = sent.words.size();
    assert(NWORDS == sent_rnn_fmt.words.size());

    const ShiftReduceHypothesis *candidate_output = 0;

    Words words;
    raws2words(sent.words, words);
    Words tags;
    raws2words(sent.pos, tags);

    // inds from the input sentence
    vector<size_t> word_inds_vec;
    vector<size_t> suf_inds_vec;

    words2word_inds(sent_rnn_fmt.words, word_inds_vec);
    pos2pos_inds(sent_rnn_fmt.pos, suf_inds_vec);
    assert(word_inds_vec.size() == suf_inds_vec.size());

    nsentences++;
    SuperCat::nsupercats = 0;

    //const MultiRaws &input = sent.msuper;

    m_lattice = new ShiftReduceLattice(NWORDS, m_beamSize, MAX_UNARY_ACTIONS);
    ShiftReduceHypothesis *start_hypo = new ShiftReduceHypothesis(0, 0, 0, 0, 0, 0, 0.0, false, 0, 0, 0, 0);
    start_hypo->SetStartHypoFlag();

    // the start hypo will get the 0-vec hidden states
    start_hypo->m_h_tm1 = m_h_tm1;
    m_lattice->Add(0, start_hypo);

    if (m_train)
      gold_hypo = m_lattice->GetLatticeArray(0); // gold == start

    size_t startIndex;

    // all context, target, and feasible target values
    // for this sentence
    vector<vector<vector<size_t> > > x_vals_sent;
    vector<size_t> y_vals_sent;
    vector<vector<size_t> > feasible_y_vals_sent;

    // inds from the current context
    vector<size_t> word_inds_ret;
    vector<size_t> suf_inds_ret;
    vector<size_t> supercat_inds_ret;

    //vector<size_t> action_inds_ret;

    // set start and end indexes of the lattice
    while (m_lattice->GetEndIndex() == 0 ||
           m_lattice->GetEndIndex() > m_lattice->GetPrevEndIndex()) {

      if (m_lattice->GetEndIndex() != 0)
        startIndex = m_lattice->GetPrevEndIndex() + 1;
      else
        startIndex = 0;
      m_lattice->SetPrevEndIndex(m_lattice->GetEndIndex());

      if (m_train) {
        if (gold_action_id < gold_tree_size) {
          gold_action = gold_tree_actions[gold_action_id];
          assert(gold_action);
          //cerr << "gold_action: " << gold_action->GetAction() << " " << gold_action->GetCat()->out_novar_noX_noNB_SR_str() << endl;
          gold_action_rnn_fmt = gold_tree_actions_rnn_fmt[gold_action_id];
          assert(gold_action_rnn_fmt.first == gold_action->GetAction());
        } else {
          //cerr << "gold_finished\n";
          gold_finished = true;
        }
      }

      HypothesisPQueue hypoQueue;

      // gold_hidden_states saves the
      // hidden layer directly connected
      // to the gold action, used later
      // to do early update
      // ***this var is per beam***
      mat gold_hidden_states;

      // other per beam vars
      // gold_action_class is used to identify
      // the gold action amoung all feasible actions
      // it's init'ed to -1 (the 1st feasible action is 0)
      int gold_action_class = -1;
      // action scores are needed if soft_max is used
      double gold_action_score = 0.0;

      // this will be set to true when expanding
      // the current gold hypo using the gold action
      bool gold_action_in_feasible = false;

      // expand hypos in current beam
      for (size_t i = startIndex; i <= m_lattice->GetEndIndex(); ++i) {

        ShiftReduceHypothesis *hypo = const_cast<ShiftReduceHypothesis *>(m_lattice->GetLatticeArray(i));

        // no more feasible actions, candidate output
        // ############## lengh normalization or not??

//        if (hypo->IsFinished(NWORDS, m_allowFragTree)) {
//          if (candidate_output == 0 ||
//              (hypo->GetTotalScore() / hypo->GetTotalActionCount()) >
//                (candidate_output->GetTotalScore() / candidate_output->GetTotalActionCount())) {
//            candidate_output = hypo;
//          }
//        }
//
//        if (hypo == gold_hypo)
//          cerr << "gold_hypo\n";

        if (hypo->IsFinished(NWORDS, m_allowFragTree)) {

          if ((candidate_output == 0 || hypo->GetTotalScore() > candidate_output->GetTotalScore()))
            candidate_output = hypo;

//          if (m_use_structure_loss &&
//              (candidate_output == 0 ||
//                ( (hypo->GetTotalScore() + m_loss_pen*(gold_tree_size - hypo->m_total_gold_action_count))
//                  > (candidate_output->GetTotalScore() + m_loss_pen*(gold_tree_size - candidate_output->m_total_gold_action_count)) )))
//            candidate_output = hypo;
        }

        vector<vector<size_t> > context_vec;
        word_inds_ret.clear();
        suf_inds_ret.clear();
        supercat_inds_ret.clear();

        //action_inds_ret.clear();

        ContextLoader context_loader(m_cat_ind, m_train);
        context_loader.load_context(hypo, word_inds_ret,
                                    suf_inds_ret, supercat_inds_ret,
                                    word_inds_vec, suf_inds_vec,
                                    m_cats_map);

        assert(word_inds_ret.size() == suf_inds_ret.size());
        assert(word_inds_ret.size() == m_feature_count);
        if (m_use_supercat_feats)
          assert(supercat_inds_ret.size() == m_supercat_feature_count);

        context_vec.push_back(word_inds_ret);
        context_vec.push_back(suf_inds_ret);
        if (m_use_supercat_feats)
          context_vec.push_back(supercat_inds_ret);

        hypo->m_context_vec = context_vec;

        // for the inital hypo, this is the 0-vec hidden states
//        hypo->m_context_vec.push_back(word_inds_ret);
//        hypo->m_context_vec.push_back(suf_inds_ret);
//        if (m_use_supercat_feats)
//          hypo->m_context_vec.push_back(supercat_inds_ret);

//        for (size_t ind = 0; ind < hypo->m_context_vec.size(); ++ind) {
//          for (size_t ind2 = 0; ind2 < hypo->m_context_vec[ind].size(); ++ind2) {
//            cerr << hypo->m_context_vec[ind][ind2] << " " << endl;
//          }
//        }
        //x_vals_sent.push_back(context_vec);


        // feasible target values for the current context
        // the map is used to assert all the feasible target
        // values for the current context are unique

        // feasible target values for the current context
        // the map is a check to make sure all the feasible
        // target values for the current context are unique
        // feasible_y_vals_map: key: output class (action id), value: (S/R/U, result cat)

        // the following are per hypo

        vector<size_t> feasible_y_vals;
        feasible_y_vals.reserve(700);
        unordered_map<size_t, pair<size_t, SuperCat*> > feasible_y_vals_map;

        // find all feasible actions,
        // but do not push them into the queue.
        // remember the gold action
        // if it gets pruned by the beam
        // we recover it and update the model
        // using it.

        // shift
        size_t j = hypo->GetNextInputIndex();

        if (j < NWORDS) {

          const MultiRaw &multi = sent.msuper[j];
          for (MultiRaw::const_iterator k = multi.begin(); k!= multi.end(); ++k) {
            const Cat *cat = cats.markedup[k->raw];

            if(!cat) {
              cerr << "SHIFT action error: attempted to load category without markedup "
                  << k->raw << endl;
              exit (EXIT_FAILURE);
            }

            assert(m_lex_cat_ind_map.find(k->raw) != m_lex_cat_ind_map.end());
            size_t cat_ind = m_lex_cat_ind_map.find(k->raw)->second;
            feasible_y_vals.push_back(cat_ind);

            //cerr << "cat id: " << m_lex_cat_ind_map[k->raw] << endl;
            SuperCat *stack_top_cat = SuperCat::Lexical(chart.pool, j+1, cat, 0);
            assert(feasible_y_vals_map.insert(make_pair(cat_ind, make_pair(0, stack_top_cat))).second);

            if (m_train && hypo == gold_hypo && gold_action_rnn_fmt.first == SHIFT
                && cat_ind == gold_action_rnn_fmt.second) {
              gold_action_class = cat_ind;
              gold_action_in_feasible = true;
            }

          } // end for
        } // end shift

        // unary
        if (hypo->GetStackTopSuperCat() != 0) {

          const SuperCat *sc = hypo->GetStackTopSuperCat();
          vector<SuperCat *> tmp;

          tmp.clear();
          if (!sc->IsLex() && !sc->IsTr()) {
            unary_tc(hypo->GetStackTopSuperCat(), false, tmp);
            unary_tr(sc, tmp);
          }
          else if (sc->IsLex() && !sc->IsTr()) {
            unary_tr(sc, tmp);
          }
          else if (!sc->IsLex() && sc->IsTr()) { /* do nothing */ }
          else if (sc->IsLex() && sc->IsTr()) { /* do nothing */ }

          for (vector<SuperCat *>::iterator j = tmp.begin(); j < tmp.end(); ++j) {
            SuperCat *stack_top_cat = *j;
            assert(stack_top_cat->left);
            feasible_y_vals.push_back(stack_top_cat->m_unary_id + m_unary_base_count);
            assert(feasible_y_vals_map.insert(make_pair(stack_top_cat->m_unary_id + m_unary_base_count,
                                              make_pair(1, stack_top_cat))).second);

            if (m_train && hypo == gold_hypo && gold_action_rnn_fmt.first == UNARY
                && stack_top_cat->m_unary_id + m_unary_base_count ==
                   gold_action_rnn_fmt.second + m_unary_base_count) {
              gold_action_in_feasible = true;
              gold_action_class = stack_top_cat->m_unary_id + m_unary_base_count;
            }

          }
        } // end unary

        // reduce
        if (hypo->GetPrvStack() != 0 && hypo->GetPrvStack()->GetStackTopSuperCat() != 0
            && hypo->GetStackTopSuperCat() != 0) {
          results.clear();
          if (!cfg.seen_rules() ||
              rule_instances(hypo->GetPrvStack()->GetStackTopSuperCat()->GetCat(), hypo->GetStackTopSuperCat()->GetCat()))
            rules(hypo->GetPrvStack()->GetStackTopSuperCat(), hypo->GetStackTopSuperCat(),
                cfg.eisner_nf(), cfg.seen_rules(), cfg.question_rules(), results);

          if (results.size() > 0) {
            for (vector<SuperCat *>::iterator k = results.begin(); k < results.end(); ++k) {
              SuperCat *stack_top_cat = *k;
              //cerr << "reduce_id: " << stack_top_cat->m_reduce_id << endl;

//              if (stack_top_cat->m_reduce_id == 0) {
//                feasible_y_vals.push_back(m_reduce_base_count + 1);
//                assert(feasible_y_vals_map.insert(make_pair(m_reduce_base_count + 1,
//                                                  make_pair(2, stack_top_cat))).second);
//              } else {
              feasible_y_vals.push_back(m_reduce_base_count + stack_top_cat->m_reduce_id + 1);
              assert(feasible_y_vals_map.insert(make_pair(m_reduce_base_count + stack_top_cat->m_reduce_id + 1,
                                                make_pair(2, stack_top_cat))).second);
              //}

              if (m_train && hypo == gold_hypo && gold_action_rnn_fmt.first == COMBINE
                  && eqSR(stack_top_cat->GetCat(), gold_action->GetCat())) {
                gold_action_in_feasible = true;
                gold_action_class = m_reduce_base_count + stack_top_cat->m_reduce_id + 1;
              }

            }
          }
        } // end reduce

        // debug
        // when expanding gold_hypo, the next gold
        // action must be one of the feasible actions
        // and we must have more than 1 feasible actions

        if (m_train && hypo == gold_hypo && !gold_finished) {
          assert(feasible_y_vals.size() > 0);
        }

        if (feasible_y_vals.size() > 0) {
          // has feasible actions, then
          // score current context and get the
          // scores for all feasible actions
          // push all hypo_tuples into queue

          mat output_vals_feasible(1, feasible_y_vals.size(), fill::zeros);
          mat hypo_new_h_tm1;
          hypo_new_h_tm1.copy_size(m_h_tm1);
          hypo_new_h_tm1.zeros();

          // every hypo expanded from the current hypo
          // will get hypo_new_h_tm1 as their hidden states
          // which will be set in score_context_beam_search(...)
          // hypo->score_context_beam_search will also be set

          score_context_beam_search(context_vec,
                                    feasible_y_vals,
                                    output_vals_feasible,
                                    hypo->m_converted_context_vec,
                                    hypo->m_h_tm1,
                                    hypo_new_h_tm1);

          // output_vals_feasible = log(output_vals_feasible);

//          if (m_train) {
//            //hypo->m_feasible_y_vals = feasible_y_vals;
//            // output_vals_feasible: softmax'ed feasible_y_vals
//            hypo->m_output_vals_feasible = output_vals_feasible;
//          }

          // output_vals_feasilbe have scores for feasible actions
          // feasible_y_vals have action classes for feasible actions

          // push all feasible actions into queue
          double total_score = 0.0;
          unordered_map<size_t, pair<size_t, SuperCat*> >::const_iterator action_iter;

          for (size_t action_ind = 0; action_ind < feasible_y_vals.size(); ++action_ind) {

            // this is currently a naive implementation
            // ideally, we don't have to expand every
            // feasible action into the beam, or do we ???

            action_iter = feasible_y_vals_map.find(feasible_y_vals[action_ind]);
            assert(action_iter != feasible_y_vals_map.end());

            const pair<size_t, SuperCat*> &feasible_action = action_iter->second;
            total_score = hypo->GetTotalScore() + output_vals_feasible(action_ind);

            // remember the hidden layer that directly derives the gold action
            if (m_train && hypo == gold_hypo
                && (size_t) gold_action_class == feasible_y_vals[action_ind]) {
              assert(gold_action_class >= 0);
              gold_hidden_states = hypo_new_h_tm1;
              gold_action_score = output_vals_feasible(action_ind);
            }

            // do the action
            // shift
            if (feasible_action.first == SHIFT) {
              assert(j < NWORDS);

              assert(feasible_action.second->cat);
              ShiftReduceAction *action_shift =
                  new ShiftReduceAction(SHIFT, feasible_action.second);
              HypothesisTuple *new_hypo_tuple =
                   new HypothesisTuple(hypo, action_shift, total_score, i, hypo_new_h_tm1,
                       feasible_y_vals[action_ind], output_vals_feasible(action_ind));
              hypoQueue.push(new_hypo_tuple);

              // unary
            } else if (feasible_action.first == UNARY) {

              assert(feasible_action.second->left);
              ShiftReduceAction *action_unary =
                  new ShiftReduceAction(UNARY, feasible_action.second);
              HypothesisTuple *new_hypo_tuple =
                  new HypothesisTuple(hypo, action_unary, total_score, i, hypo_new_h_tm1,
                      feasible_y_vals[action_ind], output_vals_feasible(action_ind));
              hypoQueue.push(new_hypo_tuple);

              // reduce
            } else if (feasible_action.first == COMBINE) {

              assert(hypo->GetPrvStack() != 0 && hypo->GetPrvStack()->GetStackTopSuperCat() != 0
                  && hypo->GetStackTopSuperCat() != 0);

              assert(feasible_action.second);
              ShiftReduceAction *action_combine =
                  new ShiftReduceAction(COMBINE, feasible_action.second);
              HypothesisTuple *new_hypo_tuple =
                  new HypothesisTuple(hypo, action_combine, total_score, i, hypo_new_h_tm1,
                      feasible_y_vals[action_ind], output_vals_feasible(action_ind));
              hypoQueue.push(new_hypo_tuple);

            } else {
              cerr << "action not recognized...\n";
              exit (EXIT_FAILURE);
            }
          }

        }
      } // end for

      if (m_train && !gold_finished) {
        assert(gold_action_in_feasible);
      }

      best_hypo = 0;
      construct_hypo(hypoQueue, m_lattice, best_hypo, gold_hypo, gold_action, gold_finished, gold_tree_size);

      //cerr << "m_hasGoldAction " << m_hasGoldAction << endl;
      //const bool xxx = candidate_output != gold_hypo;
      //const bool yyy = gold_action_id < gold_tree_size;
      //cerr << "candidate_output != gold_hypo" << xxx << endl;
     // cerr << "gold_action_id < gold_tree_size" << yyy << endl;

      // early update
      if (m_train && !m_hasGoldAction && gold_action_id < gold_tree_size) {

        assert(!gold_finished);

        cerr << "########## Early Update, sentenceID: " << sent_id
            << " -- found " << gold_action_id << " out of " << gold_tree_size  << " gold actions\n";
        assert(gold_action->GetCat());
        // check here

        assert(best_hypo);
        if (candidate_output != gold_hypo) {
          if (m_use_structure_loss) {

            if (candidate_output &&
                (((candidate_output->GetTotalScore() + m_loss_pen*(gold_tree_size - candidate_output->m_total_gold_action_count))
                    > (best_hypo->GetTotalScore() + m_loss_pen*(gold_tree_size - best_hypo->m_total_gold_action_count))) )) {
              best_hypo = candidate_output;
            }

          } else {
            if (best_hypo == 0 ||
                (candidate_output && candidate_output->GetTotalScore() > best_hypo->GetTotalScore())) {
              best_hypo = candidate_output;
            }
          }
        }

        assert(best_hypo != 0);
        assert(gold_action->GetCat());
        SuperCat *goldSuperCat = SuperCat::Wrap(chart.pool, gold_action->GetCat());
        assert(goldSuperCat);

        _action(gold_hypo, gold_action->GetAction(), m_lattice, goldSuperCat,
                0.0, gold_action_score, gold_action_class, gold_hidden_states);
        //cerr << "size: " << gold_hidden_states.n_elem;
        gold_hypo = m_lattice->GetLatticeArray(m_lattice->GetEndIndex());

        update_rnn(gold_hypo, false); // positive update
        update_rnn(best_hypo, true); // negative update

        // hack exit
        //chart.reset();
        sent.reset();
        sent_rnn_fmt.reset();
        delete m_lattice;

        return 0;
      }

      if (m_train && gold_action_id < gold_tree_size)
        ++gold_action_id;

    } // end while


    if (m_train) {

      if (candidate_output != gold_hypo) {

        cerr << "######### late update" << " --total gold actions: " << gold_tree_size << endl;

        update_rnn(gold_hypo, false);
        update_rnn(candidate_output, true);

        sent.reset();
        sent_rnn_fmt.reset();
        delete m_lattice;

        return 0;

      } else {

        cerr << "sentence: " << sent_id << " ###### output correct #####" <<
            " --total gold actions: " << gold_tree_size << endl;

        sent.reset();
        sent_rnn_fmt.reset();
        delete m_lattice;

        return 0;
      }

    }

    if (candidate_output != 0)
         return candidate_output;

    return 0;

  }
  catch(NLP::Exception e)
  {
    //throw NLP::ParseError(e.msg, nsentences);
    cerr << e.msg << endl;
    exit(EXIT_FAILURE);
  }
}


void
RNNParser::_Impl::update_rnn(const ShiftReduceHypothesis *hypo, const bool negative) {

  const ShiftReduceHypothesis *current_hypo = hypo;
  const ShiftReduceHypothesis *prev_hypo = current_hypo->GetPrevHypo();

  size_t total_steps = hypo->GetTotalActionCount();

  mat one;
  mat zero_h;
  one.copy_size(m_h_tm1);
  one.ones();
  zero_h.copy_size(m_h_tm1);
  zero_h.zeros();

  mat lh_vals[total_steps + 1];
  lh_vals[0] = zero_h;

  mat lh_deriv_vals[total_steps];
  size_t action_class_vals[total_steps];
  mat x_vals[total_steps];

  mat delta_y_vals[total_steps];

  vector<vector<vector<size_t> > > all_context_vec;
  all_context_vec.resize(total_steps);

  int i = total_steps - 1;

  while (prev_hypo != NULL) {
    assert(prev_hypo);

    //cerr << "here\n";
    //cerr << "nelem: " << current_hypo->m_h_tm1.n_elem << endl;
    lh_vals[i + 1] = current_hypo->m_h_tm1;
    lh_deriv_vals[i] = lh_vals[i + 1] % (one - lh_vals[i + 1]);

    action_class_vals[i] = current_hypo->m_action_class;

    x_vals[i] = prev_hypo->m_converted_context_vec;
    all_context_vec[i] = prev_hypo->m_context_vec;

    // dim of ly is 1xn, set the dim of delta_y
    // to nx1 without doing transpose later
    delta_y_vals[i] = zeros<mat>(m_wy.n_cols, 1);

    // each delta_y_vals have the same number of elements as ly
    // the infeasible units will just get zero values
    //for (size_t j = 0; j < prev_hypo->m_feasible_y_vals.size(); ++j) {
    //}

    //delta_y_vals[i](action_class_vals[i]) = current_h ypo->m_action_score * (1.0 - current_hypo->m_action_score);

    delta_y_vals[i](action_class_vals[i]) = 1.0;

    current_hypo = prev_hypo;
    prev_hypo = prev_hypo->GetPrevHypo();
    assert(current_hypo != prev_hypo);
    --i;
  }

  assert(i == -1);
  assert(current_hypo->GetTotalActionCount() == 0);
  assert(prev_hypo == NULL);


//  vector<vector<size_t> > all_ind_minibatches;
//  vector<size_t> inds_range;
//  inds_range.reserve(total_steps);
//  for (size_t i = 0; i < total_steps; ++i) {
//    inds_range.push_back(i);
//  }

  vector<mat> lh_vals_mini_batch;
  vector<mat> lh_deriv_vals_mini_batch;
  vector<mat> x_vals_mini_batch;
  vector<mat> delta_y_vals_mini_batch;
  vector<vector<vector<size_t> > > context_vec_minibatch;

  // add one, as need to keep lh_val from the previous step
  lh_vals_mini_batch.reserve(m_bs + 1);
  lh_deriv_vals_mini_batch.reserve(m_bs);;
  x_vals_mini_batch.reserve(m_bs);;
  delta_y_vals_mini_batch.reserve(m_bs);
  context_vec_minibatch.reserve(m_bs);

  for (size_t i = 0; i < total_steps; ++i) {

    lh_vals_mini_batch.push_back(lh_vals[i]);
    lh_deriv_vals_mini_batch.push_back(lh_deriv_vals[i]);
    x_vals_mini_batch.push_back(x_vals[i]);
    delta_y_vals_mini_batch.push_back(delta_y_vals[i]);
    context_vec_minibatch.push_back(all_context_vec[i]);

    if ((i + 1) % m_bs == 0 || i == total_steps - 1) {

      lh_vals_mini_batch.push_back(lh_vals[i + 1]);

      bptt(context_vec_minibatch,
           lh_vals_mini_batch, lh_deriv_vals_mini_batch,
           x_vals_mini_batch, delta_y_vals_mini_batch,
           negative);

      lh_vals_mini_batch.clear();
      lh_deriv_vals_mini_batch.clear();
      x_vals_mini_batch.clear();
      delta_y_vals_mini_batch.clear();
      context_vec_minibatch.clear();
    }
  }
}


void
RNNParser::_Impl::bptt(const vector<vector<vector<size_t> > > &context_batch,
                       const vector<mat> &lh_vals, const vector<mat> &lh_deriv_vals,
                       const vector<mat> &x_vals, const vector<mat> &delta_y_vals,
                       const bool negative) {

   size_t steps = lh_deriv_vals.size();
   mat delta_h_vals[steps];
   mat delta_x_vals[steps];

   // bptt multi
   delta_h_vals[steps - 1] = lh_deriv_vals[steps - 1].t()
                                % (m_wy*delta_y_vals[steps - 1]);

   for (size_t i = steps; i > 1; --i) {
     delta_h_vals[i - 2] = lh_deriv_vals[i - 2].t() %
         ((m_wh*delta_h_vals[i - 1]) + m_wy*(delta_y_vals[i - 2]));
   }

   //double back_grad_wh = 0.0;
   for (size_t i = 0; i < steps; ++i) {
     mat grad = lh_vals[i].t()*delta_h_vals[i].t();
     if (negative)
       m_wh -= m_lr*grad;
     else
       m_wh += m_lr*grad;
     //back_grad_wh += grad(0,0);
   }

   //double back_grad_wy = 0.0;
   for (size_t i = 1; i <= steps; ++i) {
     mat grad = lh_vals[i].t()*delta_y_vals[i - 1].t();
     if (negative)
       m_wy -= m_lr*grad;
     else
       m_wy += m_lr*grad;
     //back_grad_wy += grad(0,0);
   }

   for (size_t i = 0; i < steps; ++i) {
     delta_x_vals[i] = m_wx*delta_h_vals[i];
   }

   //double back_grad_wx = 0.0;
   for (size_t i = 0; i < steps; ++i) {
     mat grad = x_vals[i]*delta_h_vals[i].t();
     if (negative)
       m_wx -= m_lr*grad;
     else
       m_wx += m_lr*grad;
     //back_grad_wx += grad(0,0);
   }

   // grad check
 //  double bp_grad_emb = 0.0;
 //  double bp_grad_suf = 0.0;
 //  double bp_grad_cap = 0.0;
 //  double bp_grad_super = 0.0;

   for (size_t i = 0; i < steps; ++i) {

     const vector<size_t> &word_inds = context_batch[i][0];
     const vector<size_t> &suf_inds = context_batch[i][1];

     assert(word_inds.size() == suf_inds.size());

     // all start inds must be set here
     int start = 0;
     int start2 = m_de*word_inds.size(); // dim of word emb x num of embs
     int start3 = start2 + m_ds*word_inds.size();

     for (size_t k = 0; k < word_inds.size(); ++k) {
       if (negative)
         m_emb.col(word_inds[k]) -=
           m_lr*delta_x_vals[i].rows(start, start + m_de - 1);
       else
         m_emb.col(word_inds[k]) +=
                   m_lr*delta_x_vals[i].rows(start, start + m_de - 1);
 //      if (word_inds[k] == 10082)
 //        bp_grad_emb += delta_x_vals[i].rows(start, start + m_de - 1)[0];
       if (negative)
         m_suf.col(suf_inds[k]) -=
           m_lr*delta_x_vals[i].rows(start2, start2 + m_ds - 1);
       else
         m_suf.col(suf_inds[k]) +=
           m_lr*delta_x_vals[i].rows(start2, start2 + m_ds - 1);
 //      if (suf_inds[k] == 1)
 //        bp_grad_suf += delta_x_vals[i].rows(start2, start2 + m_ds - 1)[0];
 //      m_cap.col(cap_inds[k]) -=
 //          m_lr*delta_x_vals[i].rows(start3, start3 + m_dc - 1);
 //      if (cap_inds[k]  == 0)
 //        bp_grad_cap += delta_x_vals[i].rows(start3, start3 + m_dc - 1)[0];

       start += m_de;
       start2 += m_ds;
     }

     if (m_use_supercat_feats) {

       assert(context_batch[i].size() >= 3);
       assert(start2 == start3);

       const vector<size_t> &supercat_inds = context_batch[i][2];

       for (size_t k = 0; k < supercat_inds.size(); ++k) {
         if (negative)
           m_supercat_emb.col(supercat_inds[k]) -=
             m_lr*delta_x_vals[i].rows(start3, start3 + m_dsuper - 1);
         else
           m_supercat_emb.col(supercat_inds[k]) +=
             m_lr*delta_x_vals[i].rows(start3, start3 + m_dsuper - 1);
 //        if (supercat_inds[k] == 3)
 //          bp_grad_super += delta_x_vals[i].rows(start3, start3 + m_dsuper - 1)[0];

         start3 += m_dsuper;
       } // end for
     } // end if

   } // end for

}


// this can be improved
// (more efficient)

//void
//RNNParser::_Impl::ins2minibatch(const vector<size_t> &inds,
//                                vector<vector<size_t> >  &all_minibatches) {
//
//  vector<size_t> minibatch;
//
//  for (size_t i = 0; i < inds.size(); ++i) {
//    minibatch.push_back(inds[i]);
//    if ((i + 1) % m_bs == 0 || i == inds.size() - 1) {
//      all_minibatches.push_back(minibatch);
//      minibatch.clear();
//    }
//  }
//
//}


//double
//RNNParser::_Impl::train_bptt_multi_emb_scoher(const vector<vector<vector<size_t> > > &context_batch,
//                                              const vector<size_t> &label_batch,
//                                              const vector<vector<size_t> > &feasible_y_vals_batch) {
//
//  // test
//  //  for (size_t i = 0; i < context_batch.size(); ++i) {
//  //    for (size_t j = 0; j < context_batch[i].size(); ++j) {
//  //      for (size_t k = 0; k < context_batch[i][j].size(); ++k) {
//  //        cerr << context_batch[i][j][k] << " ";
//  //      }
//  //      cerr << endl;
//  //    }
//  //    cerr << endl;
//  //  }
//
//  double err = 0.0;
//  size_t steps = context_batch.size();
//
//  mat lh_vals[steps + 1];
//  lh_vals[0] = m_h_tm1;
//  mat lh_deriv_vals[steps];
//  mat ly_vals[steps];
//  mat x_vals[steps];
//
//  mat delta_y_vals[steps];
//  mat delta_h_vals[steps];
//  mat delta_x_vals[steps];
//
//  mat all_output_vals_feasible[steps];
//  vector<size_t> gold_labels_ind_among_feasible;
//
//  for (size_t i = 0; i < steps; ++i) {
//
//    pair<mat,  mat> vals =
//        calc_lh_proj_emb(i, context_batch[i], x_vals, lh_vals[i]);
//
//    mat lh = vals.first;
//    mat lh_deriv = vals.second;
//
//    ly_vals[i] = lh*m_wy; // no softmax here anymore
//    lh_vals[i + 1] = lh;
//    lh_deriv_vals[i] = lh_deriv;
//
//  }
//
//  // dim of ly is 1xn, set the dim of delta_y
//  // to nx1 without doing transpose later
//  for (size_t i = 0; i < steps; ++i) {
//
//    delta_y_vals[i] = zeros<mat>(ly_vals[i].n_cols, 1);
//
//    const vector<size_t> &feasible_y_vals = feasible_y_vals_batch[i];
//    mat output_vals_feasible(1, feasible_y_vals.size(), fill::zeros);
//
//    bool found_gold = false;
//    for (size_t j = 0; j < feasible_y_vals.size(); ++j) {
//
//      output_vals_feasible(j) = ly_vals[i](feasible_y_vals[j]);
//
//      if (feasible_y_vals[j] == label_batch[i]) {
//        found_gold = true;
//        gold_labels_ind_among_feasible.push_back(j);
//      }
//    }
//    assert(found_gold);
//
//    // softmax over feasible actions
//    output_vals_feasible = exp(output_vals_feasible)
//                           / accu(exp(output_vals_feasible));
//    all_output_vals_feasible[i] = output_vals_feasible;
//
//    // each delta_y_vals have the same number of elements as ly
//    // the infeasible units will just get zero values
//    for (size_t j = 0; j < feasible_y_vals.size(); ++j) {
//      delta_y_vals[i](feasible_y_vals[j]) = output_vals_feasible(j);
//    }
//  }
//
//  assert(gold_labels_ind_among_feasible.size() == steps);
//
//  for (size_t i = 0; i < steps; ++i) {
//    mat gold_dist(1, feasible_y_vals_batch[i].size(), fill::zeros);
//    gold_dist(gold_labels_ind_among_feasible[i]) = 1.0;
//    err += x_ent_multi(all_output_vals_feasible[i], gold_dist);
//    delta_y_vals[i](label_batch[i]) -= 1.0;
//  }
//
//  // bptt multi
//  delta_h_vals[steps - 1] = lh_deriv_vals[steps - 1].t()
//                               % (m_wy*delta_y_vals[steps - 1]);
//
//  for (size_t i = steps; i > 1; --i) {
//    delta_h_vals[i - 2] = lh_deriv_vals[i - 2].t() %
//        ((m_wh*delta_h_vals[i - 1]) + m_wy*(delta_y_vals[i - 2]));
//  }
//
//  //double back_grad_wh = 0.0;
//  for (size_t i = 0; i < steps; ++i) {
//    mat grad = lh_vals[i].t()*delta_h_vals[i].t();
//    m_wh -= m_lr*grad;
//    //back_grad_wh += grad(0,0);
//  }
//
//  //double back_grad_wy = 0.0;
//  for (size_t i = 1; i <= steps; ++i) {
//    mat grad = lh_vals[i].t()*delta_y_vals[i - 1].t();
//    m_wy -= m_lr*grad;
//    //back_grad_wy += grad(0,0);
//  }
//
//  for (size_t i = 0; i < steps; ++i) {
//    delta_x_vals[i] = m_wx*delta_h_vals[i];
//  }
//
//  //double back_grad_wx = 0.0;
//  for (size_t i = 0; i < steps; ++i) {
//    mat grad = x_vals[i]*delta_h_vals[i].t();
//    m_wx -= m_lr*grad;
//    //back_grad_wx += grad(0,0);
//  }
//
//  // grad check
////  double bp_grad_emb = 0.0;
////  double bp_grad_suf = 0.0;
////  double bp_grad_cap = 0.0;
////  double bp_grad_super = 0.0;
//
//  for (size_t i = 0; i < steps; ++i) {
//
//    const vector<size_t> &word_inds = context_batch[i][0];
//    const vector<size_t> &suf_inds = context_batch[i][1];
//
//    assert(word_inds.size() == suf_inds.size());
//
//    // all start inds must be set here
//    int start = 0;
//    int start2 = m_de*word_inds.size(); // dim of word emb x num of embs
//    int start3 = start2 + m_ds*word_inds.size();
//
//    for (size_t k = 0; k < word_inds.size(); ++k) {
//      m_emb.col(word_inds[k]) -=
//          m_lr*delta_x_vals[i].rows(start, start + m_de - 1);
////      if (word_inds[k] == 10082)
////        bp_grad_emb += delta_x_vals[i].rows(start, start + m_de - 1)[0];
//      m_suf.col(suf_inds[k]) -=
//          m_lr*delta_x_vals[i].rows(start2, start2 + m_ds - 1);
////      if (suf_inds[k] == 1)
////        bp_grad_suf += delta_x_vals[i].rows(start2, start2 + m_ds - 1)[0];
////      m_cap.col(cap_inds[k]) -=
////          m_lr*delta_x_vals[i].rows(start3, start3 + m_dc - 1);
////      if (cap_inds[k]  == 0)
////        bp_grad_cap += delta_x_vals[i].rows(start3, start3 + m_dc - 1)[0];
//
//      start += m_de;
//      start2 += m_ds;
//    }
//
//    if (m_use_supercat_feats) {
//
//      assert(context_batch[i].size() >= 3);
//      assert(start2 == start3);
//
//      const vector<size_t> &supercat_inds = context_batch[i][2];
//
//      for (size_t k = 0; k < supercat_inds.size(); ++k) {
//        m_supercat_emb.col(supercat_inds[k]) -=
//            m_lr*delta_x_vals[i].rows(start3, start3 + m_dsuper - 1);
////        if (supercat_inds[k] == 3)
////          bp_grad_super += delta_x_vals[i].rows(start3, start3 + m_dsuper - 1)[0];
//
//        start3 += m_dsuper;
//      }
//
//    }
//
//
//  }
//
//  //grad check
////  cerr << "backprop grad wx: " << back_grad_wx << endl;
////  cerr << "backprop grad wh: " << back_grad_wh << endl;
////  cerr << "backprop grad wy: " << back_grad_wy << endl;
//////
////  cerr << "backprop grad emb: " << bp_grad_emb << endl;
////  cerr << "backprop grad suf: " << bp_grad_suf << endl;
//////  cerr << "backprop grad cap: " << bp_grad_cap << endl;
////  cerr << "backprop grad super: " << bp_grad_super << endl;
//
//  m_h_tm1 = lh_vals[steps];
//  return err;
//}



void
RNNParser::_Impl::sr_parse_train(const double BETA, const size_t sent_id) {
  try {
    // training related
//    const size_t unary_base_count = 579;
//    const size_t reduce_base_count = 597;
    const ShiftReduceHypothesis *best_hypo; // the highest-scored hypo on each level
    const ShiftReduceHypothesis *gold_hypo;
    vector<ShiftReduceAction*> &gold_tree_actions = *m_goldTreeVec.at(sent_id);
    vector<pair<size_t, size_t> > &gold_tree_actions_rnn_fmt = m_gold_tree_vec_rnn[sent_id];
    size_t gold_tree_size = gold_tree_actions.size();
    assert(gold_tree_actions.size() == gold_tree_actions_rnn_fmt.size());
    ShiftReduceAction *gold_action = 0;
    pair<size_t, size_t> gold_action_rnn_fmt;
    size_t gold_action_id = 0;
    bool gold_finished = false;
    size_t total_shift_count = 0;

    if(sent.words.size() > cfg.maxwords())
      throw NLP::ParseError("sentence length exceeds maximum number of words for parser", nsentences);

    const size_t NWORDS = sent.words.size();
    assert(NWORDS == sent_rnn_fmt.words.size());

    //ShiftReduceHypothesis *candidate_output = 0;
    Words words;
    raws2words(sent.words, words);
    Words tags;
    raws2words(sent.pos, tags);

    // inds from the input sentence
    vector<size_t> word_inds_vec;
    vector<size_t> suf_inds_vec;

    words2word_inds(sent_rnn_fmt.words, word_inds_vec);
    pos2pos_inds(sent_rnn_fmt.pos, suf_inds_vec);
    assert(word_inds_vec.size() == suf_inds_vec.size());

    nsentences++;
    SuperCat::nsupercats = 0;

    //const MultiRaws &input = sent.msuper;

    m_lattice = new ShiftReduceLattice(NWORDS, m_beamSize, MAX_UNARY_ACTIONS);
    ShiftReduceHypothesis *start_hypo = new ShiftReduceHypothesis(0, 0, 0, 0, 0, 0, 0.0, false, 0, 0, 0, 0);
    start_hypo->SetStartHypoFlag();
    m_lattice->Add(0, start_hypo);
    gold_hypo = m_lattice->GetLatticeArray(0); // gold == start
    size_t startIndex;

    //cerr << "sent: " << sent_id << endl;

    // all context, target, and feasible target values
    // for this sentence
    vector<vector<vector<size_t> > > x_vals_sent;
    vector<size_t> y_vals_sent;
    vector<vector<size_t> > feasible_y_vals_sent;

    // inds from the current context
    vector<size_t> word_inds_ret;
    vector<size_t> suf_inds_ret;
    vector<size_t> supercat_inds_ret;

    //vector<size_t> action_inds_ret;

    // set start and end indexes of the lattice
    while (m_lattice->GetEndIndex() == 0 ||
           m_lattice->GetEndIndex() > m_lattice->GetPrevEndIndex()) {

      if (m_lattice->GetEndIndex() != 0)
        startIndex = m_lattice->GetPrevEndIndex() + 1;
      else
        startIndex = 0;
      m_lattice->SetPrevEndIndex(m_lattice->GetEndIndex());

      if (gold_action_id < gold_tree_size) {
        gold_action = gold_tree_actions[gold_action_id];
        assert(gold_action);
        //cerr << "gold: " << gold_action->GetAction() << " "
        //<< gold_action->GetCat()->out_novar_noX_noNB_SR_str() << endl;
        gold_action_rnn_fmt = gold_tree_actions_rnn_fmt[gold_action_id];
        assert(gold_action_rnn_fmt.first == gold_action->GetAction());
      }

      HypothesisPQueue hypoQueue;

      for (size_t i = startIndex; i <= m_lattice->GetEndIndex(); ++i) {

        assert(m_lattice->GetEndIndex() - startIndex == 0);
        const ShiftReduceHypothesis *hypo = m_lattice->GetLatticeArray(i);
        size_t j = hypo->GetNextInputIndex();

        //        if (hypo->IsFinished(NWORDS, m_allowFragTree))
        //        {
        //          if (candidateOutput == 0 || hypo->GetTotalScore() > candidateOutput->GetTotalScore())
        //            candidateOutput = hypo;
        //        }

        vector<vector<size_t> > context_vec;
        word_inds_ret.clear();
        suf_inds_ret.clear();
        supercat_inds_ret.clear();

        //action_inds_ret.clear();

        ContextLoader context_loader(m_cat_ind, true);
        context_loader.load_context(hypo, word_inds_ret,
                                    suf_inds_ret, supercat_inds_ret,
                                    word_inds_vec, suf_inds_vec,
                                    m_cats_map);

        assert(word_inds_ret.size() == suf_inds_ret.size());
        assert(word_inds_ret.size() == m_feature_count);
        assert(supercat_inds_ret.size() == m_supercat_feature_count);

        context_vec.push_back(word_inds_ret);
        context_vec.push_back(suf_inds_ret);
        context_vec.push_back(supercat_inds_ret);

        x_vals_sent.push_back(context_vec);

        size_t gold_action_class = 0;

        // feasible target values for the current context
        // the map is used to assert all the feasible target
        // values for the current context are unique
        vector<size_t> feasible_y_vals;
        unordered_map<size_t, size_t> feasible_y_vals_map;

        // find all feasible actions,
        // but do not push them into the queue
        // shift
        if (j < NWORDS) {

          const MultiRaw &multi = sent.msuper[j];
          for (MultiRaw::const_iterator k = multi.begin(); k!= multi.end(); ++k) {
            const Cat *cat = cats.markedup[k->raw];

            if(!cat) {
              cerr << "SHIFT action error: attempted to load category without markedup "
                  << k->raw << endl;
              exit (EXIT_FAILURE);
            }

            assert(m_lex_cat_ind_map.find(k->raw) != m_lex_cat_ind_map.end());
            feasible_y_vals.push_back(m_lex_cat_ind_map[k->raw]);
            //cerr << "cat id: " << m_lex_cat_ind_map[k->raw] << endl;
            assert(feasible_y_vals_map.insert(make_pair(m_lex_cat_ind_map[k->raw], 0)).second);
          } // end for
        } // end if

        // unary
        if (hypo->GetStackTopSuperCat() != 0) {
          const SuperCat *sc = hypo->GetStackTopSuperCat();
          vector<SuperCat *> tmp;
          tmp.clear();
          if (!sc->IsLex() && !sc->IsTr())
          {
            unary_tc(hypo->GetStackTopSuperCat(), false, tmp);
            unary_tr(sc, tmp);
          }
          else if (sc->IsLex() && !sc->IsTr())
          {
            unary_tr(sc, tmp);
          }
          else if (!sc->IsLex() && sc->IsTr()) { /* do nothing */ }
          else if (sc->IsLex() && sc->IsTr()) { /* do nothing */ }

          for (vector<SuperCat *>::iterator j = tmp.begin(); j < tmp.end(); ++j) {
            SuperCat *stack_top_cat = *j;
            assert(stack_top_cat->left);
            feasible_y_vals.push_back(stack_top_cat->m_unary_id + m_unary_base_count);
            assert(feasible_y_vals_map.insert(make_pair(stack_top_cat->m_unary_id + m_unary_base_count, 0)).second);
          }
        }

        // reduce
        if (hypo->GetPrvStack() != 0 && hypo->GetPrvStack()->GetStackTopSuperCat() != 0
            && hypo->GetStackTopSuperCat() != 0) {
          results.clear();
          if (!cfg.seen_rules() ||
              rule_instances(hypo->GetPrvStack()->GetStackTopSuperCat()->GetCat(), hypo->GetStackTopSuperCat()->GetCat()))
            rules(hypo->GetPrvStack()->GetStackTopSuperCat(), hypo->GetStackTopSuperCat(),
                cfg.eisner_nf(), cfg.seen_rules(), cfg.question_rules(), results);

          if (results.size() > 0) {
            for (vector<SuperCat *>::iterator k = results.begin(); k < results.end(); ++k) {
              SuperCat *stack_top_cat = *k;
              //cerr << "reduce_id: " << stack_top_cat->m_reduce_id << endl;
              if (stack_top_cat->m_reduce_id == 0) {
                feasible_y_vals.push_back(m_reduce_base_count + 1);
                assert(feasible_y_vals_map.insert(make_pair(m_reduce_base_count + 1, 0)).second);
              } else {
                feasible_y_vals.push_back(m_reduce_base_count + stack_top_cat->m_reduce_id + 1);
                assert(feasible_y_vals_map.insert(make_pair(m_reduce_base_count + stack_top_cat->m_reduce_id + 1, 0)).second);
              }
            }
          }
        }

        bool found_gold_action = false;

        // do the gold action
        // shift
        if (gold_action_rnn_fmt.first == SHIFT) {
          assert(j < NWORDS);

          const Cat *cat = cats.markedup[m_lex_ind_cat_map[gold_action_rnn_fmt.second]];
          assert(cat);

          SuperCat *stackTopCat = SuperCat::Lexical(chart.pool, j+1, cat, 0);
          assert(stackTopCat->cat);
          ShiftReduceAction *actionShift = new ShiftReduceAction(SHIFT, stackTopCat);
          HypothesisTuple *newHypoTuple = new HypothesisTuple(hypo, actionShift, 0.0, i);
          hypoQueue.push(newHypoTuple);

          found_gold_action = true;
          gold_action_class = gold_action_rnn_fmt.second;

          ++total_shift_count;

          // unary
        } else if (gold_action_rnn_fmt.first == UNARY) {
          const SuperCat *sc = hypo->GetStackTopSuperCat();
          vector<SuperCat *> tmp;
          tmp.clear();
          if (!sc->IsLex() && !sc->IsTr())
          {
            unary_tc(hypo->GetStackTopSuperCat(), false, tmp);
            unary_tr(sc, tmp);
          }
          else if (sc->IsLex() && !sc->IsTr())
          {
            unary_tr(sc, tmp);
          }
          else if (!sc->IsLex() && sc->IsTr()) { /* do nothing */ }
          else if (sc->IsLex() && sc->IsTr()) { /* do nothing */ }

          for (vector<SuperCat *>::iterator j = tmp.begin(); j < tmp.end(); ++j)
          {
            SuperCat *stack_top_cat = *j;
            assert(stack_top_cat->left);
            if (stack_top_cat->m_unary_id == gold_action_rnn_fmt.second) {
              found_gold_action = true;
              gold_action_class = gold_action_rnn_fmt.second + m_unary_base_count;
              ShiftReduceAction *actionUnary = new ShiftReduceAction(UNARY, stack_top_cat);
              HypothesisTuple *newHypoTuple = new HypothesisTuple(hypo, actionUnary, 0.0, i);
              hypoQueue.push(newHypoTuple);
            }
          }

          // reduce
        } else if (gold_action_rnn_fmt.first == COMBINE) {

          assert(hypo->GetPrvStack() != 0 && hypo->GetPrvStack()->GetStackTopSuperCat() != 0
              && hypo->GetStackTopSuperCat() != 0);
          results.clear();
          if(!cfg.seen_rules() ||
              rule_instances(hypo->GetPrvStack()->GetStackTopSuperCat()->cat, hypo->GetStackTopSuperCat()->cat))
          {
            rules(hypo->GetPrvStack()->GetStackTopSuperCat(), hypo->GetStackTopSuperCat(),
                cfg.eisner_nf(), cfg.seen_rules(), cfg.question_rules(), results);
          }

          assert (results.size() > 0);
          for (vector<SuperCat *>::iterator k = results.begin(); k < results.end(); ++k)
          {
            SuperCat *stack_top_cat = *k;
            if (eqSR(stack_top_cat->GetCat(), gold_action->GetCat())) {
              found_gold_action = true;
              ShiftReduceAction *actionCombine = new ShiftReduceAction(COMBINE, stack_top_cat);
              HypothesisTuple *newHypoTuple = new HypothesisTuple(hypo, actionCombine, 0.0, i);
              hypoQueue.push(newHypoTuple);

              if (stack_top_cat->m_reduce_id == 0) {
                gold_action_class = m_reduce_base_count + 1;
              } else {
                gold_action_class = m_reduce_base_count + stack_top_cat->m_reduce_id + 1;
              }
            }
          }
        } else {
          cerr << "gold action not recognized...\n";
          exit (EXIT_FAILURE);
        }

        assert(found_gold_action);
        assert(feasible_y_vals_map.find(gold_action_class)
               != feasible_y_vals_map.end());

        y_vals_sent.push_back(gold_action_class);
        feasible_y_vals_sent.push_back(feasible_y_vals);

      } // end for

      best_hypo = 0;
      construct_hypo(hypoQueue, m_lattice, best_hypo, gold_hypo, gold_action, gold_finished, gold_tree_size);
      assert(m_hasGoldAction);

      if (gold_action_id < gold_tree_size)
        ++gold_action_id;

      if (gold_action_id == gold_tree_size) {
        assert(total_shift_count == NWORDS);
        gold_finished = true;
        cerr << "sent: " << sent_id << " ### output correct ###" << endl;

        assert(x_vals_sent.size() == y_vals_sent.size());
        assert(feasible_y_vals_sent.size() == y_vals_sent.size());

        m_all_sent_context_vec_train.push_back(x_vals_sent);
        m_all_sent_target_vec_train.push_back(y_vals_sent);
        m_all_sent_feasible_y_vals_train.push_back(feasible_y_vals_sent);

        sent.reset();
        sent_rnn_fmt.reset();
        delete m_lattice;

        return;
      }

    } // end while

    sent.reset();
    sent_rnn_fmt.reset();
    delete m_lattice;

    return;
  }
  catch(NLP::Exception e)
  {
    //throw NLP::ParseError(e.msg, nsentences);
    cerr << e.msg << endl;
    return;
  }
}


//const ShiftReduceHypothesis*
//RNNParser::_Impl::sr_parse(const double BETA, const size_t sent_id) {
//  try {
//
//    // reset hidden states
//    m_h_tm1.zeros();
//    const ShiftReduceHypothesis *best_hypo; // the highest-scored hypo on each level
////    const ShiftReduceHypothesis *gold_hypo;
////    vector<ShiftReduceAction*> &gold_tree_actions = *m_goldTreeVec.at(sent_id);
////    vector<pair<size_t, size_t> > &gold_tree_actions_rnn_fmt = m_gold_tree_vec_rnn[sent_id];
////    size_t gold_tree_size = gold_tree_actions.size();
////    assert(gold_tree_actions.size() == gold_tree_actions_rnn_fmt.size());
////    ShiftReduceAction *gold_action = 0;
////    pair<size_t, size_t> gold_action_rnn_fmt;
//    size_t gold_action_id = 0;
//    bool gold_finished = false;
//    //size_t total_shift_count = 0;
//
//    if(sent.words.size() > cfg.maxwords())
//      throw NLP::ParseError("sentence length exceeds maximum number of words for parser", nsentences);
//
//    const size_t NWORDS = sent.words.size();
//    assert(NWORDS == sent_rnn_fmt.words.size());
//
//    const ShiftReduceHypothesis *candidate_output = 0;
//    Words words;
//    raws2words(sent.words, words);
////    Words tags;
////    raws2words(sent.pos, tags);
//
//    // inds from the input sentence
//    vector<size_t> word_inds_vec;
//    vector<size_t> suf_inds_vec;
//    vector<size_t> cap_inds_vec;
//
//    words2word_inds(sent_rnn_fmt.words, word_inds_vec);
//    pos2pos_inds(sent_rnn_fmt.pos, suf_inds_vec);
//    assert(word_inds_vec.size() == suf_inds_vec.size());
//
//    nsentences++;
//    SuperCat::nsupercats = 0;
//
//    //const MultiRaws &input = sent.msuper;
//
//    m_lattice = new ShiftReduceLattice(NWORDS, m_beamSize, MAX_UNARY_ACTIONS);
//    ShiftReduceHypothesis *start_hypo = new ShiftReduceHypothesis(0, 0, 0, 0, 0, 0, 0.0, false, 0, 0, 0, 0);
//    start_hypo->SetStartHypoFlag();
//    m_lattice->Add(0, start_hypo);
//    //gold_hypo = m_lattice->GetLatticeArray(0); // gold == start
//    size_t startIndex;
//
//    cerr << "sent: " << sent_id << endl;
//
//    // all context, target, and feasible target values
//    // for this sentence
//    //vector<vector<vector<size_t> > > x_vals_sent;
//    //vector<size_t> y_vals_sent;
//    vector<vector<size_t> > feasible_y_vals_sent;
//
//    // inds of the current context
//    vector<size_t> word_inds_ret;
//    vector<size_t> suf_inds_ret;
//    vector<size_t> supercat_inds_ret;
//
//    // set start and end indexes of the lattice
//    while (m_lattice->GetEndIndex() == 0 ||
//        m_lattice->GetEndIndex() > m_lattice->GetPrevEndIndex())
//    {
//      if (m_lattice->GetEndIndex() != 0)
//        startIndex = m_lattice->GetPrevEndIndex() + 1;
//      else
//        startIndex = 0;
//      m_lattice->SetPrevEndIndex(m_lattice->GetEndIndex());
//
////      if (gold_action_id < gold_tree_size)
////      {
////        gold_action = gold_tree_actions[gold_action_id];
////        assert(gold_action);
////        //cerr << "gold: " << gold_action->GetAction() << " "
////        //<< gold_action->GetCat()->out_novar_noX_noNB_SR_str() << endl;
////        gold_action_rnn_fmt = gold_tree_actions_rnn_fmt[gold_action_id];
////        assert(gold_action_rnn_fmt.first == gold_action->GetAction());
////      }
//
//      HypothesisPQueue hypoQueue;
//      for (size_t i = startIndex; i <= m_lattice->GetEndIndex(); ++i)
//      {
//        assert(m_lattice->GetEndIndex() - startIndex == 0);
//        const ShiftReduceHypothesis *hypo = m_lattice->GetLatticeArray(i);
//        size_t j = hypo->GetNextInputIndex();
//
//        if (hypo->IsFinished(NWORDS, m_allowFragTree))
//        {
//          if (candidate_output == 0 || hypo->GetTotalScore() > candidate_output->GetTotalScore())
//            candidate_output = hypo;
//        }
//
//        vector<vector<size_t> > context_vec;
//        word_inds_ret.clear();
//        suf_inds_ret.clear();
//        supercat_inds_ret.clear();
//
//        ContextLoader context_loader(m_cat_ind, false);
//        context_loader.load_context(hypo, word_inds_ret,
//                                    suf_inds_ret, supercat_inds_ret,
//                                    word_inds_vec, suf_inds_vec,
//                                    m_cats_map);
//
//        //cerr << "word_inds_ret.size(): " << word_inds_ret.size();
//
//        assert(word_inds_ret.size() == suf_inds_ret.size());
//        assert(word_inds_ret.size() == m_feature_count);
//        assert(supercat_inds_ret.size() == m_supercat_feature_count);
//
//        context_vec.push_back(word_inds_ret);
//        context_vec.push_back(suf_inds_ret);
//        context_vec.push_back(supercat_inds_ret);
//        //x_vals_sent.push_back(context_vec);
//
//        //bool found_gold_action = false;
//        //size_t gold_action_class = 0;
//
//        // feasible target values for the current context
//        // the map is a check to make sure all the feasible
//        // target values for the current context are unique
//        // feasible_y_vals_map: key: output class (action id), value: (S/R/U, result cat)
//        vector<size_t> feasible_y_vals;
//        unordered_map<size_t, pair<size_t, SuperCat*> > feasible_y_vals_map;
//
//        // find all feasible actions,
//        // but do not push them into the queue
//
//        // shift
//        if (j < NWORDS) {
//
//          const MultiRaw &multi = sent.msuper[j];
//          for (MultiRaw::const_iterator k = multi.begin(); k!= multi.end(); ++k) {
//            const Cat *cat = cats.markedup[k->raw];
//
//            if(!cat) {
//              cerr << "SHIFT action error: attempted to load category without markedup "
//                   << k->raw << endl;
//              exit (EXIT_FAILURE);
//            }
//
//            assert(m_lex_cat_ind_map.find(k->raw) != m_lex_cat_ind_map.end());
//            feasible_y_vals.push_back(m_lex_cat_ind_map[k->raw]);
//            SuperCat *stack_top_cat = SuperCat::Lexical(chart.pool, j+1, cat, 0);
//            assert(feasible_y_vals_map.insert(make_pair(m_lex_cat_ind_map[k->raw],
//                                              make_pair(0, stack_top_cat))).second);
//          } // end for
//        } // end if
//
//        // unary
//        if (hypo->GetStackTopSuperCat() != 0) {
//
//          const SuperCat *sc = hypo->GetStackTopSuperCat();
//          vector<SuperCat *> tmp;
//          tmp.clear();
//
//          if (!sc->IsLex() && !sc->IsTr()) {
//            unary_tc(hypo->GetStackTopSuperCat(), false, tmp);
//            unary_tr(sc, tmp);
//          }
//          else if (sc->IsLex() && !sc->IsTr()) {
//            unary_tr(sc, tmp);
//          }
//          else if (!sc->IsLex() && sc->IsTr()) { /* do nothing */ }
//          else if (sc->IsLex() && sc->IsTr()) { /* do nothing */ }
//
//          for (vector<SuperCat *>::iterator j = tmp.begin(); j < tmp.end(); ++j) {
//            SuperCat *stack_top_cat = *j;
//            assert(stack_top_cat->left);
//            feasible_y_vals.push_back(stack_top_cat->m_unary_id + m_unary_base_count);
//            assert(feasible_y_vals_map.insert(make_pair(stack_top_cat->m_unary_id + m_unary_base_count,
//                                              make_pair(1, stack_top_cat))).second);
//          }
//        }
//
//        // reduce
//        if (hypo->GetPrvStack() != 0 && hypo->GetPrvStack()->GetStackTopSuperCat() != 0
//            && hypo->GetStackTopSuperCat() != 0) {
//
//          results.clear();
//          if (!cfg.seen_rules() ||
//              rule_instances(hypo->GetPrvStack()->GetStackTopSuperCat()->GetCat(), hypo->GetStackTopSuperCat()->GetCat()))
//            rules(hypo->GetPrvStack()->GetStackTopSuperCat(), hypo->GetStackTopSuperCat(),
//                cfg.eisner_nf(), cfg.seen_rules(), cfg.question_rules(), results);
//
//          if (results.size() > 0) {
//            for (vector<SuperCat *>::iterator k = results.begin(); k < results.end(); ++k) {
//
//              SuperCat *stack_top_cat = *k;
//
//              if (stack_top_cat->m_reduce_id == 0) {
//                feasible_y_vals.push_back(m_reduce_base_count + 1);
//                assert(feasible_y_vals_map.insert(make_pair(m_reduce_base_count + 1,
//                                                  make_pair(2, stack_top_cat))).second);
//              } else {
//                feasible_y_vals.push_back(m_reduce_base_count + stack_top_cat->m_reduce_id + 1);
//                assert(feasible_y_vals_map.insert(make_pair(m_reduce_base_count + stack_top_cat->m_reduce_id + 1,
//                                                  make_pair(2, stack_top_cat))).second);
//              }
//            }
//          }
//        }
//
//        if (feasible_y_vals.size() > 0) {
//          // has feasible actions, then
//          // score current context
//
//          pair<size_t, double> best_class_score = score_context(context_vec, feasible_y_vals);
//          size_t max_ind = best_class_score.first;
//          double best_action_score = log(best_class_score.second);
//
//          double total_score = hypo->GetTotalScore() + best_action_score;
//
//          unordered_map<size_t, pair<size_t, SuperCat*> >::const_iterator action_iter
//                  = feasible_y_vals_map.find(max_ind);
//          assert(action_iter != feasible_y_vals_map.end());
//
//          pair<size_t, SuperCat*> best_action = action_iter->second;
//
//          // do the gold action
//          // shift
//          if (best_action.first == SHIFT) {
//            assert(j < NWORDS);
//
//            assert(best_action.second->cat);
//            ShiftReduceAction *action_shift = new ShiftReduceAction(SHIFT, best_action.second);
//            HypothesisTuple *new_hypo_tuple = new HypothesisTuple(hypo, action_shift, total_score, i);
//            hypoQueue.push(new_hypo_tuple);
//
//            //++total_shift_count;
//
//            // unary
//          } else if (best_action.first == UNARY) {
//
//            assert(best_action.second->left);
//            ShiftReduceAction *action_unary = new ShiftReduceAction(UNARY, best_action.second);
//            HypothesisTuple *new_hypo_tuple = new HypothesisTuple(hypo, action_unary, total_score, i);
//            hypoQueue.push(new_hypo_tuple);
//
//            // reduce
//          } else if (best_action.first == COMBINE) {
//
//            assert(hypo->GetPrvStack() != 0 && hypo->GetPrvStack()->GetStackTopSuperCat() != 0
//                && hypo->GetStackTopSuperCat() != 0);
//
//            assert(best_action.second);
//            ShiftReduceAction *action_combine = new ShiftReduceAction(COMBINE, best_action.second);
//            HypothesisTuple *new_hypo_tuple = new HypothesisTuple(hypo, action_combine, total_score, i);
//            hypoQueue.push(new_hypo_tuple);
//
//          } else {
//            cerr << "best action not recognized...\n";
//            exit (EXIT_FAILURE);
//          }
//        }
//
//      } // end for
//
//      if (!hypoQueue.empty()) {
//        best_hypo = 0;
//        const ShiftReduceHypothesis *gold_hypo = 0;
//        ShiftReduceAction *gold_action = 0;
//        construct_hypo(hypoQueue, m_lattice, best_hypo, gold_hypo, gold_action, gold_finished, gold_tree_size);
//      }
//
//    } // end while
//
//    if (candidate_output != 0)
//      return candidate_output;
//
//    return 0;
//  }
//  catch(NLP::Exception e)
//  {
//    //throw NLP::ParseError(e.msg, nsentences);
//    cerr << e.msg << endl;
//    return 0;
//  }
//}


void
RNNParser::_Impl::calc_lh_proj_emb(const vector<vector<size_t> > &context) {
  colvec x;
  colvec x0;
  colvec x1;
  colvec x2;

  assert(context[0].size() == context[1].size());

  const vector<size_t> &word_inds = context[0];
  const vector<size_t> &suf_inds = context[1];

  for (size_t i = 0; i < word_inds.size(); ++i) {
    x0 = join_cols(x0, m_emb.col(word_inds[i]));
    x1 = join_cols(x1, m_suf.col(suf_inds[i]));
  }

  if (m_use_supercat_feats) {
    const vector<size_t> &super_inds = context[2];

    for (size_t i = 0; i < super_inds.size(); ++i) {
      x2 = join_cols(x2, m_supercat_emb.col(super_inds[i]));
    }

    //mat temp = join_cols(join_cols(x0, x1), x2);
    //cerr << "size:" << temp.n_rows;
    //cerr << "x3 size: " << x3.n_rows;
    x = join_cols(join_cols(x0, x1), x2);

  } else {

    x = join_cols(x0, x1);

  }

  //x = join_cols(join_cols(x0, x1), x2);
  mat lh = sigmoid(x.t()*m_wx + m_h_tm1*m_wh);
  m_h_tm1 = lh;
}


// return scores for all feasilbe actions,
// not just the max
void
RNNParser::_Impl::score_context_beam_search(const vector<vector<size_t> > &context,
                                            const vector<size_t> &feasible_y_vals,
                                            mat &output_vals_feasible,
                                            colvec &current_hypo_converted_context_vec,
                                            mat &current_hypo_h_tm1,
                                            mat &new_h_tm1, const bool soft_max) {

  // feasible_y_vals contains inds of feasible output classes

  calc_lh_proj_emb_train_beam(context, current_hypo_converted_context_vec,
                              current_hypo_h_tm1, new_h_tm1);
  mat ly_vals = new_h_tm1*m_wy; // no softmax here anymore

  for (size_t j = 0; j < feasible_y_vals.size(); ++j) {
    output_vals_feasible(j) = ly_vals(feasible_y_vals[j]);
  }

  // soft_max defaults to false
  if (soft_max) {
    // softmax over feasible actions
    output_vals_feasible = exp(output_vals_feasible)
                             / accu(exp(output_vals_feasible));
  }

  //return output_vals_feasible;
}


pair<size_t, double>
RNNParser::_Impl::score_context(const vector<vector<size_t> > &context,
                                const vector<size_t> &feasible_y_vals) {

  //cerr << "context size: " << context.size() << endl;
  //cerr << "feasible_y_vals size: " << feasible_y_vals.size() << endl;

  // feasible_y_vals contains inds of feasible output classes

  calc_lh_proj_emb(context);
  mat ly_vals = m_h_tm1*m_wy; // no softmax here anymore

  //ly_vals.print("ly_vals");

  mat output_vals_feasible(1, feasible_y_vals.size(), fill::zeros);

  // get the raw scores of feasible actions
  for (size_t j = 0; j < feasible_y_vals.size(); ++j) {
    output_vals_feasible(j) = ly_vals(feasible_y_vals[j]);
  }

  // softmax over feasible actions
  output_vals_feasible = exp(output_vals_feasible)
                         / accu(exp(output_vals_feasible));

  //output_vals_feasible.print("output_vals");

  uword max_ind;
  output_vals_feasible.max(max_ind);

  return make_pair(feasible_y_vals[max_ind], output_vals_feasible.max());
}


RNNParser::RNNParser(void){};

RNNParser::RNNParser(const RNNConfig &cfg, NLP::Taggers::RnnTagger &rnn_super, Sentence &sent,
                     Sentence &sent_rnn_fmt,
                     Categories &cats,
                     const size_t srbeam,
                     const string &config_dir,
                     const bool train){
  _impl = new RNNParser::_Impl(cfg, sent, sent_rnn_fmt, cats, srbeam, config_dir, train);
}

RNNParser::~RNNParser(void){ delete _impl; }


void
RNNParser::load_gold_trees(const std::string &filename) {
  return _impl->load_gold_trees(filename); }

void
RNNParser::Clear() {
  _impl->Clear();
}

void
RNNParser::clear_xf1() {
  _impl->clear_xf1();
}


void
RNNParser::sr_parse_train(const double BETA,
    const size_t sent) {
  _impl->sr_parse_train(BETA, sent);
}

//
//const ShiftReduceHypothesis*
//RNNParser::sr_parse(const double BETA, const size_t sent) {
//  return _impl->sr_parse(BETA, sent);
//}


const ShiftReduceHypothesis*
RNNParser::sr_parse_beam_search(const double BETA, const size_t sent) {
  return _impl->sr_parse_train_and_test_beam(BETA, sent);
}


void
RNNParser::load_lex_cat_dict(const string &filename) {
  return _impl->load_lex_cat_dict(filename);
}


void
RNNParser::load_pos_str_ind_map(const string &filename) {
  try {
    return _impl->load_pos_str_ind_map(filename);
  } catch (runtime_error) {
    exit(EXIT_FAILURE);
  }
}

void
RNNParser::load_gold_trees_rnn_fmt(const string &filename) {
  try {
    return _impl->load_gold_trees_rnn_fmt(filename);
  } catch (runtime_error) {
    exit(EXIT_FAILURE);
  }
}

//void
//RNNParser::train(const size_t total_epochs, const string &model_path) {
//  _impl->train(total_epochs, model_path);
//}

void
RNNParser::re_init_mats(const string &wx, const string &wh, const string &wy,
                        const string &emb, const string &suf, const string &cap,
                        const string &super_emb) {
  _impl->re_init_mats(wx, wh, wy, emb, suf, super_emb);
}

void
RNNParser::load_supercat_map(const string &file) {
  _impl->load_supercat_map(file);
}

void
RNNParser::dump_supercat_map(string &file) {
  _impl->dump_supercat_map(file);
}

void
RNNParser::save_mats(const string &model_path, const size_t i) {
  _impl->save_mats(model_path, i);
}

//const ShiftReduceHypothesis*
//RNNParser::_Impl::sr_parse_beam_search(const double BETA, const size_t sent_id) {
//  try {
//
//    // reset hidden states
//    m_h_tm1.zeros();
//    const ShiftReduceHypothesis *best_hypo; // the highest-scored hypo on each level
////    const ShiftReduceHypothesis *gold_hypo;
////    vector<ShiftReduceAction*> &gold_tree_actions = *m_goldTreeVec.at(sent_id);
////    vector<pair<size_t, size_t> > &gold_tree_actions_rnn_fmt = m_gold_tree_vec_rnn[sent_id];
////    size_t gold_tree_size = gold_tree_actions.size();
////    assert(gold_tree_actions.size() == gold_tree_actions_rnn_fmt.size());
////    ShiftReduceAction *gold_action = 0;
////    pair<size_t, size_t> gold_action_rnn_fmt;
//    size_t gold_action_id = 0;
//    bool gold_finished = false;
//
//    if(sent.words.size() > cfg.maxwords())
//      throw NLP::ParseError("sentence length exceeds maximum number of words for parser", nsentences);
//
//    const size_t NWORDS = sent.words.size();
//    assert(NWORDS == sent_rnn_fmt.words.size());
//
//    const ShiftReduceHypothesis *candidate_output = 0;
//    Words words;
//    raws2words(sent.words, words);
////    Words tags;
////    raws2words(sent.pos, tags);
//
//    // inds from the input sentence
//    vector<size_t> word_inds_vec;
//    vector<size_t> suf_inds_vec;
//
//    words2word_inds(sent_rnn_fmt.words, word_inds_vec);
//    pos2pos_inds(sent.pos, suf_inds_vec);
//    assert(word_inds_vec.size() == suf_inds_vec.size());
//
//    nsentences++;
//    SuperCat::nsupercats = 0;
//
//    //const MultiRaws &input = sent.msuper;
//
//    m_lattice = new ShiftReduceLattice(NWORDS, m_beamSize, MAX_UNARY_ACTIONS);
//    ShiftReduceHypothesis *start_hypo = new ShiftReduceHypothesis(0, 0, 0, 0, 0, 0, 0.0, false, 0, 0, 0, 0);
//    start_hypo->SetStartHypoFlag();
//    m_lattice->Add(0, start_hypo);
//    //gold_hypo = m_lattice->GetLatticeArray(0); // gold == start
//    size_t startIndex;
//
//    cerr << "sent: " << sent_id << endl;
//
//    // all context, target, and feasible target values
//    // for this sentence
//    //vector<vector<vector<size_t> > > x_vals_sent;
//    //vector<size_t> y_vals_sent;
//    vector<vector<size_t> > feasible_y_vals_sent;
//
//    // inds of the current context
//    vector<size_t> word_inds_ret;
//    vector<size_t> suf_inds_ret;
//    vector<size_t> supercat_inds_ret;
//
//    // set start and end indexes of the lattice
//    while (m_lattice->GetEndIndex() == 0 ||
//        m_lattice->GetEndIndex() > m_lattice->GetPrevEndIndex())
//    {
//      if (m_lattice->GetEndIndex() != 0)
//        startIndex = m_lattice->GetPrevEndIndex() + 1;
//      else
//        startIndex = 0;
//      m_lattice->SetPrevEndIndex(m_lattice->GetEndIndex());
//
////      if (gold_action_id < gold_tree_size)
////      {
////        gold_action = gold_tree_actions[gold_action_id];
////        assert(gold_action);
////        //cerr << "gold: " << gold_action->GetAction() << " "
////        //<< gold_action->GetCat()->out_novar_noX_noNB_SR_str() << endl;
////        gold_action_rnn_fmt = gold_tree_actions_rnn_fmt[gold_action_id];
////        assert(gold_action_rnn_fmt.first == gold_action->GetAction());
////      }
//
//      HypothesisPQueue hypoQueue;
//      for (size_t i = startIndex; i <= m_lattice->GetEndIndex(); ++i)
//      {
//        //assert(m_lattice->GetEndIndex() - startIndex == 0);
//        const ShiftReduceHypothesis *hypo = m_lattice->GetLatticeArray(i);
//        size_t j = hypo->GetNextInputIndex();
//
////        if (hypo->IsFinished(NWORDS, m_allowFragTree)) {
////          if (candidate_output == 0 ||
////              (hypo->GetTotalScore() / hypo->GetTotalActionCount())
////               > (candidate_output->GetTotalScore() / candidate_output->GetTotalActionCount()));
////            candidate_output = hypo;
////        }
//
//        vector<vector<size_t> > context_vec;
//        word_inds_ret.clear();
//        suf_inds_ret.clear();
//        supercat_inds_ret.clear();
//
//        ContextLoader context_loader(m_cat_ind, false);
//        context_loader.load_context(hypo, word_inds_ret,
//                                    suf_inds_ret, supercat_inds_ret,
//                                    word_inds_vec, suf_inds_vec,
//                                    m_cats_map);
//
//        assert(word_inds_ret.size() == suf_inds_ret.size());
//
//        context_vec.push_back(word_inds_ret);
//        context_vec.push_back(suf_inds_ret);
//        context_vec.push_back(supercat_inds_ret);
//        //x_vals_sent.push_back(context_vec);
//
//        //bool found_gold_action = false;
//        //size_t gold_action_class = 0;
//
//        // feasible target values for the current context
//        // the map is a check to make sure all the feasible
//        // target values for the current context are unique
//        // feasible_y_vals_map: key: output class (action id), value: (S/R/U, result cat)
//        vector<size_t> feasible_y_vals;
//        unordered_map<size_t, pair<size_t, SuperCat*> > feasible_y_vals_map;
//
//        // find all feasible actions,
//        // but do not push them into the queue
//
//        // shift
//        if (j < NWORDS) {
//
//          const MultiRaw &multi = sent.msuper[j];
//          for (MultiRaw::const_iterator k = multi.begin(); k!= multi.end(); ++k) {
//            const Cat *cat = cats.markedup[k->raw];
//
//            if(!cat) {
//              cerr << "SHIFT action error: attempted to load category without markedup "
//                   << k->raw << endl;
//              exit (EXIT_FAILURE);
//            }
//
//            assert(m_lex_cat_ind_map.find(k->raw) != m_lex_cat_ind_map.end());
//            feasible_y_vals.push_back(m_lex_cat_ind_map[k->raw]);
//            SuperCat *stack_top_cat = SuperCat::Lexical(chart.pool, j+1, cat, 0);
//            assert(feasible_y_vals_map.insert(make_pair(m_lex_cat_ind_map[k->raw],
//                                              make_pair(0, stack_top_cat))).second);
//          } // end for
//        } // end if
//
//        // unary
//        if (hypo->GetStackTopSuperCat() != 0) {
//
//          const SuperCat *sc = hypo->GetStackTopSuperCat();
//          vector<SuperCat *> tmp;
//          tmp.clear();
//
//          if (!sc->IsLex() && !sc->IsTr()) {
//            unary_tc(hypo->GetStackTopSuperCat(), false, tmp);
//            unary_tr(sc, tmp);
//          }
//          else if (sc->IsLex() && !sc->IsTr()) {
//            unary_tr(sc, tmp);
//          }
//          else if (!sc->IsLex() && sc->IsTr()) { /* do nothing */ }
//          else if (sc->IsLex() && sc->IsTr()) { /* do nothing */ }
//
//          for (vector<SuperCat *>::iterator j = tmp.begin(); j < tmp.end(); ++j) {
//            SuperCat *stack_top_cat = *j;
//            assert(stack_top_cat->left);
//            feasible_y_vals.push_back(stack_top_cat->m_unary_id + m_unary_base_count);
//            assert(feasible_y_vals_map.insert(make_pair(stack_top_cat->m_unary_id + m_unary_base_count,
//                                              make_pair(1, stack_top_cat))).second);
//          }
//        }
//
//        // reduce
//        if (hypo->GetPrvStack() != 0 && hypo->GetPrvStack()->GetStackTopSuperCat() != 0
//            && hypo->GetStackTopSuperCat() != 0) {
//
//          results.clear();
//          if (!cfg.seen_rules() ||
//              rule_instances(hypo->GetPrvStack()->GetStackTopSuperCat()->GetCat(), hypo->GetStackTopSuperCat()->GetCat()))
//            rules(hypo->GetPrvStack()->GetStackTopSuperCat(), hypo->GetStackTopSuperCat(),
//                cfg.eisner_nf(), cfg.seen_rules(), cfg.question_rules(), results);
//
//          if (results.size() > 0) {
//            for (vector<SuperCat *>::iterator k = results.begin(); k < results.end(); ++k) {
//
//              SuperCat *stack_top_cat = *k;
//
//              if (stack_top_cat->m_reduce_id == 0) {
//                feasible_y_vals.push_back(m_reduce_base_count + 1);
//                assert(feasible_y_vals_map.insert(make_pair(m_reduce_base_count + 1,
//                                                  make_pair(2, stack_top_cat))).second);
//              } else {
//                feasible_y_vals.push_back(m_reduce_base_count + stack_top_cat->m_reduce_id + 1);
//                assert(feasible_y_vals_map.insert(make_pair(m_reduce_base_count + stack_top_cat->m_reduce_id + 1,
//                                                  make_pair(2, stack_top_cat))).second);
//              }
//            }
//          }
//        }
//
//        if (feasible_y_vals.size() > 0) {
//          // has feasible actions, then
//          // score current context
//
//          mat output_vals_feasible(1, feasible_y_vals.size(), fill::zeros);
//
//          score_context_beam_search(context_vec, feasible_y_vals, output_vals_feasible);
//          output_vals_feasible = log(output_vals_feasible);
//
//          double total_score = 0.0;
//          unordered_map<size_t, pair<size_t, SuperCat*> >::const_iterator action_iter;
//
//          for (size_t action_ind = 0; action_ind < feasible_y_vals.size(); ++action_ind) {
//
//            // this is currently a naive implementation
//            // ideally, we don't have to expand every
//            // feasible action into the beam
//
//            action_iter = feasible_y_vals_map.find(feasible_y_vals[action_ind]);
//            assert(action_iter != feasible_y_vals_map.end());
//
//            pair<size_t, SuperCat*> feasible_action = action_iter->second;
//            total_score = hypo->GetTotalScore() + output_vals_feasible(action_ind);
//
//            // do the action
//            // shift
//            if (feasible_action.first == SHIFT) {
//              assert(j < NWORDS);
//
//              assert(feasible_action.second->cat);
//              ShiftReduceAction *action_shift = new ShiftReduceAction(SHIFT, feasible_action.second);
//              HypothesisTuple *new_hypo_tuple = new HypothesisTuple(hypo, action_shift, total_score, i);
//              hypoQueue.push(new_hypo_tuple);
//
//              // unary
//            } else if (feasible_action.first == UNARY) {
//
//              assert(feasible_action.second->left);
//              ShiftReduceAction *action_unary = new ShiftReduceAction(UNARY, feasible_action.second);
//              HypothesisTuple *new_hypo_tuple = new HypothesisTuple(hypo, action_unary, total_score, i);
//              hypoQueue.push(new_hypo_tuple);
//
//              // reduce
//            } else if (feasible_action.first == COMBINE) {
//
//              assert(hypo->GetPrvStack() != 0 && hypo->GetPrvStack()->GetStackTopSuperCat() != 0
//                  && hypo->GetStackTopSuperCat() != 0);
//
//              assert(feasible_action.second);
//              ShiftReduceAction *action_combine = new ShiftReduceAction(COMBINE, feasible_action.second);
//              HypothesisTuple *new_hypo_tuple = new HypothesisTuple(hypo, action_combine, total_score, i);
//              hypoQueue.push(new_hypo_tuple);
//
//            } else {
//              cerr << "best action not recognized...\n";
//              exit (EXIT_FAILURE);
//            }
//          }
//
//        } else {
//
//          // no more feasible actions, candidate output
//          if (candidate_output == 0 ||
//              (hypo->GetTotalScore() / hypo->GetTotalActionCount()) >
//                (candidate_output->GetTotalScore() / candidate_output->GetTotalActionCount())) {
//            candidate_output = hypo;
//          }
//        }
//
//
//
//      } // end for
//
//      if (!hypoQueue.empty()) {
//        best_hypo = 0;
//        const ShiftReduceHypothesis *gold_hypo = 0;
//        ShiftReduceAction *gold_action = 0;
//        // apply beam
//        construct_hypo(hypoQueue, m_lattice, best_hypo, gold_hypo, gold_action, gold_finished);
//      }
//
//    } // end while
//
//    if (candidate_output != 0)
//      return candidate_output;
//
//    return 0;
//  }
//  catch(NLP::Exception e)
//  {
//    //throw NLP::ParseError(e.msg, nsentences);
//    cerr << e.msg << endl;
//    return 0;
//  }
//}



//        // do the gold action
//        // shift
//        if (gold_action_rnn_fmt.first == SHIFT) {
//          assert(j < NWORDS);
//
//          const Cat *cat = cats.markedup[m_lex_ind_cat_map[gold_action_rnn_fmt.second]];
//          assert(cat);
//
//          SuperCat *stackTopCat = SuperCat::Lexical(chart.pool, j+1, cat, 0);
//          assert(stackTopCat->cat);
//          ShiftReduceAction *actionShift = new ShiftReduceAction(SHIFT, stackTopCat);
//          HypothesisTuple *newHypoTuple = new HypothesisTuple(hypo, actionShift, 0.0, i);
//          hypoQueue.push(newHypoTuple);
//
//          found_gold_action = true;
//          gold_action_class = gold_action_rnn_fmt.second;
//
//          ++total_shift_count;
//
//          // unary
//        } else if (gold_action_rnn_fmt.first == UNARY) {
//          const SuperCat *sc = hypo->GetStackTopSuperCat();
//          vector<SuperCat *> tmp;
//          tmp.clear();
//          if (!sc->IsLex() && !sc->IsTr())
//          {
//            unary_tc(hypo->GetStackTopSuperCat(), false, tmp);
//            unary_tr(sc, tmp);
//          }
//          else if (sc->IsLex() && !sc->IsTr())
//          {
//            unary_tr(sc, tmp);
//          }
//          else if (!sc->IsLex() && sc->IsTr()) { /* do nothing */ }
//          else if (sc->IsLex() && sc->IsTr()) { /* do nothing */ }
//
//          for (vector<SuperCat *>::iterator j = tmp.begin(); j < tmp.end(); ++j)
//          {
//            SuperCat *stack_top_cat = *j;
//            assert(stack_top_cat->left);
//            if (stack_top_cat->m_unary_id == gold_action_rnn_fmt.second) {
//              found_gold_action = true;
//              gold_action_class = gold_action_rnn_fmt.second + m_unary_base_count;
//              ShiftReduceAction *actionUnary = new ShiftReduceAction(UNARY, stack_top_cat);
//              HypothesisTuple *newHypoTuple = new HypothesisTuple(hypo, actionUnary, 0.0, i);
//              hypoQueue.push(newHypoTuple);
//            }
//          }
//
//          // reduce
//        } else if (gold_action_rnn_fmt.first == COMBINE) {
//
//          assert(hypo->GetPrvStack() != 0 && hypo->GetPrvStack()->GetStackTopSuperCat() != 0
//              && hypo->GetStackTopSuperCat() != 0);
//          results.clear();
//          if(!cfg.seen_rules() ||
//              rule_instances(hypo->GetPrvStack()->GetStackTopSuperCat()->cat, hypo->GetStackTopSuperCat()->cat))
//          {
//            rules(hypo->GetPrvStack()->GetStackTopSuperCat(), hypo->GetStackTopSuperCat(),
//                cfg.eisner_nf(), cfg.seen_rules(), cfg.question_rules(), results);
//          }
//
//          assert (results.size() > 0);
//          for (vector<SuperCat *>::iterator k = results.begin(); k < results.end(); ++k)
//          {
//            SuperCat *stack_top_cat = *k;
//            if (eqSR(stack_top_cat->GetCat(), gold_action->GetCat())) {
//              found_gold_action = true;
//              ShiftReduceAction *actionCombine = new ShiftReduceAction(COMBINE, stack_top_cat);
//              HypothesisTuple *newHypoTuple = new HypothesisTuple(hypo, actionCombine, 0.0, i);
//              hypoQueue.push(newHypoTuple);
//
//              if (stack_top_cat->m_reduce_id == 0) {
//                gold_action_class = m_reduce_base_count + 1;
//              } else {
//                gold_action_class = m_reduce_base_count + stack_top_cat->m_reduce_id + 1;
//              }
//            }
//          }
//        } else {
//          cerr << "gold action not recognized...\n";
//          exit (EXIT_FAILURE);
//        }
//


//        assert(found_gold_action);
//
//
//        y_vals_sent.push_back(gold_action_class);
//        feasible_y_vals_sent.push_back(feasible_y_vals);


//const ShiftReduceHypothesis* RNNParser::_Impl::sr_parse(const double BETA, bool qu_parsing)
//{
//  try
//  {
//    // training related
//    vector<ShiftReduceAction*> nullVec;
//    const ShiftReduceHypothesis *bestHypo; // the highest-scored hypo on each level
//    const ShiftReduceHypothesis *goldHypo;
//    //vector<ShiftReduceAction*> &goldTreeActions = m_train ? *m_goldTreeVec.at(202) : nullVec;
//    vector<ShiftReduceAction*> &goldTreeActions = m_train ? *m_goldTreeVec.at(Integration::s_perceptronId[0] - 1) : nullVec;
//    size_t goldTreeSize = goldTreeActions.size();
//    ShiftReduceAction *goldAction = 0;
//    size_t gold_action_id = 0;
//    bool goldFinished = false;
//
//    assert(sent.words.size() != 0);
//    if(sent.words.size() > cfg.maxwords())
//      return 0;
//    //throw NLP::ParseError("sentence length exceeds maximum number of words for parser", nsentences);
//
//    const ShiftReduceHypothesis *candidateOutput = 0;
//    //vector<size_t> word_inds_vec;
//    //words2word_inds(sent.words, word_inds_vec);
//    //vector<size_t> suf_inds_vec;
//    //vector<size_t> cap_inds_vec;
//    //suf_cap2suf_cap_inds(sent.pos, suf_inds_vec, cap_inds_vec);
//
//    nsentences++;
//    SuperCat::nsupercats = 0;
//    const long NWORDS = sent.words.size();
//    const MultiRaws &input = sent.msuper;
//
//    m_lattice = new ShiftReduceLattice(NWORDS, m_beamSize, MAX_UNARY_ACTIONS);
//    ShiftReduceHypothesis *startHypo = new ShiftReduceHypothesis(0, 0, 0, 0, 0, 0, 0.0, false, 0, 0, 0);
//    startHypo->SetStartHypoFlag();
//    m_lattice->Add(0, startHypo);
//    if (m_train) goldHypo = m_lattice->GetLatticeArray(0); // gold == start
//    size_t startIndex;
//
//    //cerr << "sentence ID: " << Integration::s_perceptronId[0] << endl;
//
//    // set start and end indexes of the lattice
//    while (m_lattice->GetEndIndex() == 0 || m_lattice->GetEndIndex() > m_lattice->GetPrevEndIndex())
//    {
//      if (m_lattice->GetEndIndex() != 0)
//        startIndex = m_lattice->GetPrevEndIndex() + 1;
//      else
//        startIndex = 0;
//      m_lattice->SetPrevEndIndex(m_lattice->GetEndIndex());
//
//      HypothesisPQueue hypoQueue;
//      for (size_t i = startIndex; i <= m_lattice->GetEndIndex(); ++i)
//      {
//        const ShiftReduceHypothesis *hypo = m_lattice->GetLatticeArray(i);
//        size_t j = hypo->GetNextInputIndex();
//        if (hypo->IsFinished(NWORDS, m_allowFragTree))
//        {
//          if (candidateOutput == 0 || hypo->GetTotalScore() > candidateOutput->GetTotalScore())
//            candidateOutput = hypo;
//        }
//
//        ShiftReduceContext context;
//        context.LoadContext(hypo, words, tags);
//
//        // shift
//        if (j < static_cast<size_t>(NWORDS))
//        {
//          const MultiRaw &multi = input[j];
//          //double prob_cutoff = multi[0].score*BETA;
//          for (MultiRaw::const_iterator k = multi.begin(); k!= multi.end(); ++k)
//          {
//            //if(k->score < prob_cutoff)
//            //continue;
//            const Cat *cat = cats.markedup[k->raw];
//            if(!cat)
//            {
//              //cerr << "sent ID: " << Integration::s_perceptronId[0];
//              throw NLP::ParseError(": SHIFT action error: attempted to load category without markedup " + k->raw);
//            }
//            SuperCat *stackTopCat = SuperCat::Lexical(chart.pool, j+1, cat, 0);
//            assert(stackTopCat->cat);
//            //maybe change this to non-pointer type
//            ShiftReduceAction *actionShift = new ShiftReduceAction(SHIFT, stackTopCat);
//
//            //double actionScore = GetOrUpdateWeight(false, false, context, *actionShift, words, tags);
//            //double hypoTotalScore = hypo->GetTotalScore() + actionScore;
//            //HypothesisTuple *newHypoTuple = new HypothesisTuple(hypo, actionShift, hypoTotalScore, i);
//            //hypoQueue.push(newHypoTuple);
//
//            //delete actionShift;
//            //delete newHypoTuple;
//          } // end for
//        } // end if
//
//        // unary
//        if (hypo->GetStackTopSuperCat() != 0)
//        {
//          //          const SuperCat *sc = hypo->GetStackTopSuperCat();
//          //          vector<SuperCat *> tmp;
//          //          tmp.clear();
//          //          if (!sc->IsLex() && !sc->IsTr())
//          //          {
//          //            UnaryLex(hypo->GetStackTopSuperCat(), qu_parsing, tmp);
//          //            UnaryTr(sc, tmp);
//          //          }
//          //          else if (sc->IsLex() && !sc->IsTr())
//          //          {
//          //            UnaryTr(sc, tmp);
//          //          }
//          //          else if (!sc->IsLex() && sc->IsTr()) { /* do nothing */ }
//          //          else if (sc->IsLex() && sc->IsTr()) { /* do nothing */ }
//          //
//          //          for (vector<SuperCat *>::iterator j = tmp.begin(); j < tmp.end(); ++j)
//          //          {
//          //            SuperCat *stackTopCat = *j;
//          //            assert(stackTopCat->left);
//          //            ShiftReduceAction *actionUnary = new ShiftReduceAction(UNARY, stackTopCat);
//          //
//          //            double actionScore = GetOrUpdateWeight(false, false, context, *actionUnary, words, tags);
//          //            double hypoTotalScore = hypo->GetTotalScore() + actionScore;
//          //            HypothesisTuple *newHypoTuple = new HypothesisTuple(hypo, actionUnary, hypoTotalScore, i);
//          //            hypoQueue.push(newHypoTuple);
//          //          }
//        }
//
//        // combine
//        if (hypo->GetPrvStack() != 0 && hypo->GetPrvStack()->GetStackTopSuperCat() != 0
//            && hypo->GetStackTopSuperCat() != 0)
//        {
//          //          //if (hypo == goldHypo) cerr << "*gold*" << endl;
//          //          //cerr << "reduce: " << hypo->GetPrvStack()->GetStackTopSuperCat()->GetCat()->out_novar_noX_noNB_SR_str() << " and "
//          //          //<< hypo->GetStackTopSuperCat()->GetCat()->out_novar_noX_noNB_SR_str() << endl;
//          //          results.clear();
//          //          if (!cfg.seen_rules() ||
//          //              rule_instances(hypo->GetPrvStack()->GetStackTopSuperCat()->GetCat(), hypo->GetStackTopSuperCat()->GetCat()))
//          //            rules(hypo->GetPrvStack()->GetStackTopSuperCat(), hypo->GetStackTopSuperCat(),
//          //                cfg.eisner_nf(), cfg.seen_rules(), cfg.question_rules(), results);
//          //          if (results.size() > 0)
//          //          {
//          //            for (vector<SuperCat *>::iterator k = results.begin(); k < results.end(); ++k)
//          //            {
//          //              SuperCat *stackTopCat = *k;
//          //              //cerr << "result: " << stackTopCat->GetCat()->out_novar_noX_noNB_SR_str() << endl << endl;
//          //              ShiftReduceAction *actionCombine = new ShiftReduceAction(COMBINE, stackTopCat);
//          //
//          //              double actionScore = GetOrUpdateWeight(false, false, context, *actionCombine, words, tags);
//          //              double hypoTotalScore = hypo->GetTotalScore() + actionScore;
//          //              HypothesisTuple *newHypoTuple = new HypothesisTuple(hypo, actionCombine, hypoTotalScore, i);
//          //              hypoQueue.push(newHypoTuple);
//          //            }
//          //          }
//        }
//      } // end for
//
//      bestHypo = 0; // the highest-scored hypo on each level
//      if (m_train && gold_action_id < goldTreeSize)
//      {
//        goldAction = goldTreeActions[gold_action_id];
//        assert(goldAction);
//      }
//      if (gold_action_id >= goldTreeSize) goldFinished = true;
//      construct_hypo(hypoQueue, m_lattice, bestHypo, goldHypo, goldAction, goldFinished);
//
//      /*   //debug
//      if (m_train && m_hasOutsideRule && goldActionId < goldTreeSize)
//      {
//    cerr << "sent ID: " << m_perceptronId[0] << " ###has outside rule### " << *goldAction <<  endl;
//    cerr << "gold action is: ";
//    if (goldAction->GetAction() == 0)
//        cerr << *goldAction << endl;
//    if (goldAction->GetAction() == 1)
//    {
//        ShiftReduceAction *prevAction = goldTreeActions[goldActionId - 1];
//        cerr << "1: " << "from " << prevAction->GetCat()->out_novar_noX_noNB_SR_str() << "to "
//          << goldAction->GetCat()->out_novar_noX_noNB_SR_str() << endl;
//    }
//    if (goldAction->GetAction() == 2)
//    {
//        cerr << "2: " << goldHypo->GetPrvStack()->GetStackTopSuperCat()->GetCat()->out_novar_noX_noNB_SR_str() << " and "
//          << goldHypo->GetStackTopSuperCat()->GetCat()->out_novar_noX_noNB_SR_str() << endl;
//    }
//    cerr << endl << "at " << goldActionId + 1 << " out of " << goldTreeSize << " gold actions for sentence ID: " << Integration::s_perceptronId[0] << endl << endl;
//
//    return 0;
//      }*/
//
//      //assert(bestHypo != 0); best hypo *could* be 0
//
//      // early update
//      //std::cerr << "has gold action?: " << m_hasGoldAction << std::endl;
//      // it's possible to have missed the gold action, e.g., goes beyond the number of
//      // gold actions, but one previously updated candidate output already matches the gold
//      // thus the need for the third condition
//      if (m_train && !m_hasGoldAction && candidateOutput != goldHypo && goldActionId < goldTreeSize)
//      {
//        //        cerr << "########## Early Update, sentenceID: " << m_perceptronId[0] << endl;
//        //        cerr << "found: " << goldActionId << " out of " << goldTreeSize << " gold actions" << endl;
//        //        cerr << "faild to find gold action: " << goldAction->GetAction() << " " << goldAction->GetCat()->out_novar_noX_noNB_SR_str() << endl;
//        //        cerr << "gold action is: ";
//        //        if (goldAction->GetAction() == 0)
//        //          cerr << *goldAction << endl;
//        //        if (goldAction->GetAction() == 1)
//        //        {
//        //          ShiftReduceAction *prevAction = goldTreeActions[goldActionId - 1];
//        //          cerr << "1: " << "from " << prevAction->GetCat()->out_novar_noX_noNB_SR_str() << "to "
//        //              << goldAction->GetCat()->out_novar_noX_noNB_SR_str() << endl;
//        //        }
//        //        if (goldAction->GetAction() == 2)
//        //        {
//        //          cerr << "2: " << goldHypo->GetPrvStack()->GetStackTopSuperCat()->GetCat()->out_novar_noX_noNB_SR_str() << " and "
//        //              << goldHypo->GetStackTopSuperCat()->GetCat()->out_novar_noX_noNB_SR_str() << endl;
//        //        }
//        //
//        //        assert(goldAction->GetCat());
//        //        SuperCat *goldSuperCat = SuperCat::Wrap(chart.pool, goldAction->GetCat());
//        //        assert(goldSuperCat);
//        //
//        //        if (bestHypo == 0 || (candidateOutput && candidateOutput->GetTotalScore() > bestHypo->GetTotalScore()))
//        //          bestHypo = candidateOutput;
//        //        assert(bestHypo != 0);
//        //        // not all gold actions have been found
//        //        Action(goldHypo, goldAction->GetAction(), m_lattice, goldSuperCat, 0.0);
//        //        goldHypo = m_lattice->GetLatticeArray(m_lattice->GetEndIndex());
//        //        //PerceptronUpdates(bestHypo, goldHypo, words, tags);
//        //        std::cerr << "########## done early update" << std::endl;
//        //        // hack exit
//        //        //chart.reset();
//        //        sent.reset();
//        //        delete m_lattice;
//
//        return 0;
//      }
//      if (m_train) ++goldActionId;
//    } // end while
//
//    // final update, if no early update
//    if (m_train)
//    {
//      //      if (candidateOutput != goldHypo)
//      //      {
//      //        cerr << "######### late update" << endl;
//      //        //PerceptronUpdates(candidateOutput, goldHypo, words, tags);
//      //      }
//      //      else
//      //      {
//      //        cerr << "sentence: " << m_perceptronId[0] << "###### output correct #####" << endl;
//      //      }
//      //      sent.reset();
//      //      delete m_lattice;
//    }
//
//    if (candidateOutput != 0)
//      return candidateOutput;
//
//    return 0;
//  }
//  catch(NLP::Exception e)
//  {
//    //throw NLP::ParseError(e.msg, nsentences);
//    cerr << e.msg << endl;
//    return 0;
//  }
//}


} }
