// C&C NLP tools
// Copyright (c) Universities of Edinburgh, Oxford and Sydney
// Copyright (c) James R. Curran
//
// This software is covered by a non-commercial use licence.
// See LICENCE.txt for the full text of the licence.
//
// If LICENCE.txt is not included in this distribution
// please email candc@it.usyd.edu.au to obtain a copy.

#include "parser/ShiftReduceHypothesis.h"
#include "parser/ShiftReduceHypothesisQueue.h"
#include <stdlib.h>
#include <armadillo>
#include <fstream>
#include "rnn_parser/_rnn_parser.h"
#include "rnn_parser/context_load_pos_fast.h"
#include <time.h>
#include <fstream>
#include <thread>
#include <functional>
#include <fstream>

using namespace std;
using namespace arma;

namespace NLP { namespace CCG {

using namespace NLP::Tree;

//Pool *Parser::s_shiftReduceFeaturePool(new Pool(1 << 20));

RNNParser::RNNConfig::RNNConfig(const OpPath *base, const std::string &name, const std::string &desc)
: Directory(name, desc, base),
  cats(*this, "cats", "parser category directory", "//cats", &path),
  markedup(*this, "markedup", "parser markedup file", "//markedup", &cats),
  weights(*this, "weights", "parser model weights file", "//weights", &path),
  rules(*this, "rules", "parser rules file", "//rules", &path),
  maxwords(*this, SPACE, "maxwords", "maximum sentence length the parser will accept", 250),
  maxsupercats(*this, "maxsupercats", "maximum number of supercats before the parse explodes", 300000),
  alt_markedup(*this, SPACE, "alt_markedup", "use the alternative markedup categories (marked with !)", false),
  seen_rules(*this, "seen_rules", "only accept category combinations seen in the training data", false),
  extra_rules(*this, "extra_rules", "use additional punctuation and unary type-changing rules", true),
  question_rules(*this, "question_rules", "activate the unary rules that only apply to questions", false),
  eisner_nf(*this, "eisner_nf", "only accept composition when the Eisner (1996) constraints are met", false),
  partial_gold(*this, SPACE, "partial_gold", "used for generating feature forests for training", false),
  beam(*this, "beam_ratio", "(not fully tested)", 0.0),
  allowFrag(*this, "allowFrag", "allow fragmented output", true),

  bs(*this, SPACE, "bptt_steps", "number of bptt steps", 9),
  ds(*this, SPACE, "dim_suf", "suffix/pos embedding dimension", 50),
  dc(*this, SPACE, "dim_cap", "cap embedding dimension", 5),
  de(*this, SPACE, "dim_emb", "word embedding dimension", 50),
  dsuper(*this, SPACE, "dim_super", "supercat embedding dimension", 50),
  daction(*this, SPACE, "dim_action", "action embedding dimension", 50),
  nh(*this, SPACE, "n_hidden", "hidden layer size", 200),
  lr(*this, "lr", "learning rate", 0.0025),
  head_limit(*this, SPACE, "head_limit", "max number of heads per feature", 1),
  use_pos(*this, "use_pos", "true = pos, false = suffix (can't use both at the moment)", true),
  use_supercat_feats(*this, "use_super_feats", "use super cat features", true),
  use_phrase_emb_feats(*this, "use_phrase_emb_feats", "generate and use phrase embedding features", false),
  activation(*this, "activation", "sig or tanh", "sig"),
  word_emb_file(*this, "word_emb_file", "word embedding file to use", "emb_turian_50_scaled"),
  pos_emb_file(*this, "pos_emb_file", "word2vec pre-trained pos emb file", "wsj0221+nanc+wiki1st3.model.pos_emb"),
  super_emb_file(*this, "super_emb_file", "word2vec pre-trained super emb file", "wsj0221+nanc+wiki1.model.super_emb"),
  use_dropout(*this, "dropout", "to use dropout or not", true),
  xf1_mini_batch(*this, "xf1_mini_batch", "use mini_batch bptt for xf1 training", false),
  dropout_success_prob(*this, "drop_success_prob", "success prob of dropout", 0.75),
  use_structrue_loss(*this, "use_struct_loss", "use max-margin loss", true),
  loss_pen(*this, "loss_pen", "loss penalty of max-maring loss", 0.05),
  precompute(*this, "precompute", "precompute hidden states", false),
  use_lh_cache(*this, "use_lh_cache", "use_lh_cache", false),
  xf1_super(*this, "xf1_super", "xf1_super training", false) {}
//rnn_model_base_dir(*this, "rnn model dir", "dir path to save trained rnn model", './')

void
RNNParser::_Impl::_load_rules(const std::string &filename){
  cerr << "loading rules: " << filename << endl;
  ulong nlines = 0;
  if(filename == "")
    return;

  ifstream in(filename.c_str());
  if(!in)
    throw NLP::IOException("could not open rules file", filename);

  comment += read_preface(filename, in, nlines);

  string cat1str, cat2str;
  while(in >> cat1str >> cat2str){
    try {
      const Cat *cat1 = cats.canonize(cat1str.c_str());
      const Cat *cat2 = cats.canonize(cat2str.c_str());

      rule_instances.insert(cat1, cat2);
    }catch(NLP::Exception e){
      throw NLP::IOException("error parsing category in rule instantiation", filename);
    }
  }
  cerr << "total binary rules: " << rule_instances.size() << endl;
}


void
RNNParser::_Impl::_load_features(const std::string &filename){

  //std::cerr << "_load_features in parser.cc" << std::endl;
  ulong nlines = 0;
  ifstream in(filename.c_str());
  if(!in)
    NLP::IOException("could not open features file", filename);

  comment += read_preface(filename, in, nlines);

  nfeatures = 1;
  string tmp;
  ulong freq;
  for(char c; in >> freq >> c; ++nfeatures){
    switch(c){
    case 'a': cat_feats.load(in, filename, nfeatures, LEX_WORD); break;
    case 'b': cat_feats.load(in, filename, nfeatures, LEX_POS); break;
    case 'c': cat_feats.load(in, filename, nfeatures, ROOT); break;
    case 'd': cat_feats.load(in, filename, nfeatures, ROOT_WORD); break;
    case 'e': cat_feats.load(in, filename, nfeatures, ROOT_POS); break;

    case 'f': dep_feats.load(in, filename, nfeatures, DEP_WORD); break;
    case 'g': dep_feats.load(in, filename, nfeatures, DEP_POS); break;
    case 'h': dep_feats.load(in, filename, nfeatures, DEP_WORD_POS); break;
    case 'i': dep_feats.load(in, filename, nfeatures, DEP_POS_WORD); break;

    case 'x': genrule_feats.load(in, filename, nfeatures, GEN_RULE); break;
    case 'm': rule_feats.load(in, filename, nfeatures, URULE); break;
    case 'n': rule_feats.load(in, filename, nfeatures, BRULE); break;

    case 'p': rule_head_feats.load(in, filename, nfeatures, URULE_HEAD); break;
    case 'q': rule_head_feats.load(in, filename, nfeatures, BRULE_HEAD); break;
    case 'r': rule_head_feats.load(in, filename, nfeatures, URULE_POS); break;
    case 's': rule_head_feats.load(in, filename, nfeatures, BRULE_POS); break;

    case 't': rule_dep_feats.load(in, filename, nfeatures, BRULE_HEAD_HEAD); break;
    case 'u': rule_dep_feats.load(in, filename, nfeatures, BRULE_POS_HEAD); break;
    case 'v': rule_dep_feats.load(in, filename, nfeatures, BRULE_HEAD_POS); break;
    case 'w': rule_dep_feats.load(in, filename, nfeatures, BRULE_POS_POS); break;

    case 'F': rule_dep_dist_feats.load(in, filename, nfeatures, DIST_ADJ_HEAD); break;
    case 'G': rule_dep_dist_feats.load(in, filename, nfeatures, DIST_VERBS_HEAD); break;
    case 'H': rule_dep_dist_feats.load(in, filename, nfeatures, DIST_PUNCT_HEAD); break;
    case 'I': rule_dep_dist_feats.load(in, filename, nfeatures, DIST_ADJ_POS); break;
    case 'J': rule_dep_dist_feats.load(in, filename, nfeatures, DIST_VERBS_POS); break;
    case 'K': rule_dep_dist_feats.load(in, filename, nfeatures, DIST_PUNCT_POS); break;

    case 'L': dep_dist_feats.load(in, filename, nfeatures, DIST_ADJ_HEAD); break;
    case 'M': dep_dist_feats.load(in, filename, nfeatures, DIST_VERBS_HEAD); break;
    case 'N': dep_dist_feats.load(in, filename, nfeatures, DIST_PUNCT_HEAD); break;
    case 'P': dep_dist_feats.load(in, filename, nfeatures, DIST_ADJ_POS); break;
    case 'Q': dep_dist_feats.load(in, filename, nfeatures, DIST_VERBS_POS); break;
    case 'R': dep_dist_feats.load(in, filename, nfeatures, DIST_PUNCT_POS); break;

    default:
      throw NLP::IOException("unexpected feature type in load features", filename, nfeatures);
    }
  }
  --nfeatures;
}


void
RNNParser::_Impl::load_lex_cat_dict(const string &raw_cats_from_markedup) {
  cerr << "load_lex_cat_dict..." << endl;
  ifstream in(raw_cats_from_markedup.c_str());

  if (!in) {
    cerr << "no such file: " << raw_cats_from_markedup << endl;
    exit (EXIT_FAILURE);
  }

  string s;
  while (getline(in, s)) {
    istringstream ss(s);
    string cat_str;
    size_t ind = 0;
    if (!(ss >> cat_str >> ind)) {
      cerr << "somthing is wrong in: " << raw_cats_from_markedup << endl;
      exit (EXIT_FAILURE);
      break;
    }
    int ind_in_markedup = cats.markedup.index(cat_str);
    assert(ind_in_markedup != -1);
    assert(m_lex_cat_ind_map.insert(make_pair(cat_str, ind)).second);
    assert(m_lex_ind_cat_map.insert(make_pair(ind, cat_str)).second);
  }
  cerr << "number of raw markedup cats in markedup_raw_ind_dict file: "
      << m_lex_cat_ind_map.size() << endl;
  cerr << "number of raw markedup cats in cats.markedup: "
      << cats.plain_markedup_count << endl;
  assert(m_lex_cat_ind_map.size() == 580);
  assert(m_lex_cat_ind_map.size() == cats.plain_markedup_count);
  assert(m_lex_ind_cat_map.size() == m_lex_cat_ind_map.size());
}


RNNParser::_Impl::_Impl(const RNNConfig &cfg,
                        Sentence &sent,
                        Categories &cats,
                        const size_t srbeam, const string& config_dir,
                        const bool train)
: cfg(cfg), sent(sent),
  nsentences(0),
  cats(cats),
  lexicon("lexicon", cfg.lexicon()),
  cat_feats(cats, lexicon),
  rule_feats(cats),
  rule_head_feats(cats, lexicon),
  rule_dep_feats(cats, lexicon),
  rule_dep_dist_feats(cats, lexicon),
  dep_feats(cats, lexicon),
  dep_dist_feats(cats, lexicon),
  genrule_feats(cats),
  weights(0),
  chart(cats, cfg.extra_rules(), cfg.maxwords()),
  rules(chart.pool, cats.markedup, cfg.extra_rules(), rule_instances),

  NP(cats.markedup["NP"]), NbN(cats.markedup["N\\N"]), NPbNP(cats.markedup["NP\\NP"]),
  SbS(cats.markedup["S\\S"]), SfS(cats.markedup["S/S"]),
  SbNPbSbNP(cats.markedup["(S\\NP)\\(S\\NP)"]),
  SbNPfSbNP(cats.markedup["(S\\NP)/(S\\NP)"]), SfSbSfS(cats.markedup["(S/S)\\(S/S)"]),
  SbNPbSbNPbSbNPbSbNP(cats.markedup["((S\\NP)\\(S\\NP))\\((S\\NP)\\(S\\NP))"]),
  NPfNPbNP(cats.markedup["NP/(NP\\NP)"]),

  //m_shiftReduceFeatures(new ShiftReduceFeature(cats, lexicon)),
  m_allowFragTree(cfg.allowFrag()),
  //m_allowFragAndComplete(false),

  m_hasGoldAction(false),
  m_hasOutsideRule(false),
  m_lattice(0),
  m_beamSize(srbeam),
  m_train(train),
  // m_cat_ind is initialized to 1, it gets
  // incremented for the very first seen cat
  // whose ind will be 2, and the actual 0th
  // column in the cat embedding matrix is reserved
  // for the empty cat feature, and the actual 1st
  // reserved for unk: in context_load.h
  // const static size_t empty_supercat_feat_ind = 0;
  // const static size_t unk_supercat_ind = 1;
  m_cat_ind(1),
  m_dropout(cfg.use_dropout()),
  m_dropout_success_prob(cfg.dropout_success_prob()),
  m_total_reduce_count(0) // this gets incremented for the first phrase emb, 0th is for empty feat
{
  cerr << "parser constructor parser.cc" << endl;
  cerr << "allowFragTree: " << m_allowFragTree << endl;
  cerr << "SR beam size: " << m_beamSize << endl;
  cerr << "is training: " << train << endl;
  cerr << "eisner: " << cfg.eisner_nf() << endl;

  if(cfg.seen_rules() && cfg.eisner_nf()) {
    string path = cfg.path() + string("//rules_nb_num_et");
    _load_rules(path);
  }

  if (cfg.seen_rules() && !cfg.eisner_nf()) {
    string path = cfg.path() + string("//rules_nb_num_ef");
    _load_rules(path);
  }

  sent.words.reserve(cfg.maxwords());
  sent.pos.reserve(cfg.maxwords());
  sent.msuper.reserve(cfg.maxwords());

  //sent_rnn_fmt.words.reserve(cfg.maxwords());
  //sent_rnn_fmt.pos.reserve(cfg.maxwords());

  std::cerr << "parser _Impl constructor in parser.cc done" << std::endl;
  std::cerr << "lexicon size: " << lexicon.size() << std::endl;

  //  if (!cfg.use_pos())
  //    load_suf_str_ind_map("../files/new_emb_dict_and_pos_feat/sfef/suf_str_ind_map.txt");
  // rnn related parameters

  m_use_supercat_feats = cfg.use_supercat_feats();
  m_activation = cfg.activation();

  m_bs = cfg.bs();
  m_ds = cfg.ds();
  m_dc = cfg.dc();
  m_de = cfg.de();
  m_nh = cfg.nh();
  m_dsuper = cfg.dsuper();
  m_lr = cfg.lr();

  m_nclasses = 608;
  //m_vocsize = 23554;
  // the first number is len(knwon_emb_dict) + len(unknown_emb_dict)
  // the second number is 3 types of unks
  //m_vocsize = 40618 + 3;

  m_vocsize = 0;
  string line;
  ifstream emb_file(config_dir + "/emb_turian_50_scaled");
  if (!emb_file) {
    cerr << "no such file " << config_dir << "/emb_turian_50_scaled" << endl;
    exit (EXIT_FAILURE);
  }
  while (getline(emb_file, line))
    ++m_vocsize;

  if (cfg.use_pos()) {
    load_pos_str_ind_map(config_dir + "/pos_str_ind_map.txt");
    m_suf_count = m_pos_str_ind_map.size();
  }
  else {
    load_suf_str_ind_map(config_dir + "/suf_str_ind_map.txt");
    m_suf_count = m_suf_str_ind_map.size();
  }

  m_supercat_feature_count = 10;

  // q0w, q1w, q2w, q3w and s_s0w, s_s1w, s_s2w, s_s3w
  // generalize to pos too
  // (can have at most one word and one pos for q0 etc.)
  m_const_feature_count = 8;

  // s0w s1w, s2w, s3w (4) and s0lw, s0rw, s0uw (6)
  // generalize to pos too
  // (can have more than one word, pos when having multiple heads)
  m_variable_feature_count = 4 + 6;
  m_head_limit = cfg.head_limit();

  m_feature_count = (m_variable_feature_count * m_head_limit) + m_const_feature_count;

  //  // s0c, s1c deps (2 * 2)
  //  if (cfg.use_dep_feats()) {
  //    m_feature_count += 4; // 4 WORDS AND 4 POS
  //    m_supercat_feature_count += 2;
  //  }
  //
  //  // currently, the code assumes supercat_feats must be used if
  //  // dep features are used
  //
  //  if (cfg.use_dep_feats())
  //    assert(cfg.use_supercat_feats());

  cerr << "m_feature_count: " << m_feature_count << endl;

  //  m_total_emb_dim_plus_unks = m_vocsize;
  //  m_total_suf_dim_plus_unk = m_suf_count;

  // there are 580 (0 - 579) lexical categoreis in total
  // unary rule ids go from 1 to 18, so unary rule
  // id 1 will get the output class unary_base_count + 1
  // similarly for reduce
  m_unary_base_count = 579;
  m_reduce_base_count = 597;

  std::mt19937_64 engine;
  std::uniform_real_distribution<double> uni(-1.0, 1.0);

  // dist = 0, 1, 2, or empty feature (padding)
  //  m_dep_dist_mat.set_size(m_dc, 4);
  //  m_dep_dist_mat.imbue( [&]() { return uni(engine); } );
  //  m_dep_dist_mat *= 0.2;

  // essentail to init the weights in m_emb first
  // this deals with 3 types of unks, except *UNKNOWN*
  // which is from the pre-trained embeddings

  //  if (train) {

  m_emb.set_size(m_de, m_vocsize + 3);
  m_emb.imbue( [&]() { return uni(engine); } );
  m_emb = m_emb * 0.2;

  try {
    load_emb_mat(m_emb, config_dir + "/emb_turian_50_scaled");
  } catch (runtime_error e) {
    cerr << e.what() << endl;
    exit(EXIT_FAILURE);
  }

  if (cfg.word_emb_file() != "emb_turian_50_scaled") {
    m_emb.zeros();
    cerr << "loading pre-trained word embedding: " << cfg.word_emb_file() << endl;
    m_emb.load(config_dir + "/" + cfg.word_emb_file());
  }

  unordered_map<string, size_t>::const_iterator iter = m_full_wordstr_emb_map.find("***PADDING***");
  assert(iter != m_full_wordstr_emb_map.end());

  std::mt19937_64 engine1;

  // the 0th col is reserved for the empty
  // the 1st col is reserved for unk suf/pos
  // feature, and ind of suf/pos starts from two
  m_suf.set_size(m_ds, m_suf_count + 2);
  m_suf.imbue( [&]() { return uni(engine1); } );
  m_suf = m_suf * 0.2;

  if (cfg.use_pos()) {
    const string word2vec_pos_emb = config_dir + "/" + cfg.pos_emb_file();
    load_pos_emb_mat(m_suf, word2vec_pos_emb);
  }

  //  std::mt19937 engine2;
  //
  //  m_cap.set_size(m_dc, 2);
  //  m_cap.imbue( [&]() { return uni(engine2); } );
  //  m_cap *= 0.2;

  std::mt19937_64 engine3;

  if (m_use_supercat_feats) {
    m_supercat_emb.set_size(m_dsuper, 1000);
    m_supercat_emb.imbue( [&]() { return uni(engine3); } );
    m_supercat_emb *= 0.2;

    // for the very first training epoch, load label_ind.txt (the same file as used for rnn_super, except with
    // NNOONNEE cat removed)
    // and this fixes the order of the lex supercats in m_cats_map (rnn_parser.cc) for subsequent
    // training epochs (this is different from markedup_raw_ind, which is only used for finding feasilbe actions)
    load_supercat_map(config_dir + "/label_ind.txt", true);

    const string word2vec_super_emb = config_dir + "/" + cfg.super_emb_file();
    load_super_emb_mat(m_supercat_emb, word2vec_super_emb);
  }

  //  if (cfg.use_action_feats()) {
  //    m_action_emb.set_size(m_daction, 4); // 3 action types + 1
  //    m_action_emb.imbue( [&]() { return uni(engine); } );
  //    m_action_emb *= 0.2;
  //  }

  std::mt19937_64 engine4;

  if (m_use_supercat_feats) {
    double scaling = 0.0;
    if (cfg.use_phrase_emb_feats()) {
      scaling = (m_de + m_ds)*m_feature_count + m_dsuper*m_supercat_feature_count + m_phrase_emb_feat_count*m_de;
      m_wx.set_size((m_de + m_ds)*m_feature_count + m_dsuper*m_supercat_feature_count + m_phrase_emb_feat_count*m_de, m_nh);
    }
    else {
      scaling = (m_de + m_ds)*m_feature_count + m_dsuper*m_supercat_feature_count;
      m_wx.set_size((m_de + m_ds)*m_feature_count + m_dsuper*m_supercat_feature_count, m_nh);
    }

    std::uniform_real_distribution<double> uni1(-2.0 / scaling, 2.0 / scaling);
    m_wx.imbue( [&]() { return uni1(engine4); } );
    //m_wx *= 0.2;
  } else {
    m_wx.set_size((m_de + m_ds)*m_feature_count, m_nh);
    m_wx.imbue( [&]() { return uni(engine4); } );
    //m_wx *= 0.2;
  }

  //cerr << "m_wx size: " << m_wx.n_cols << endl;

  std::mt19937_64 engine5;
  std::uniform_real_distribution<double> uni2(-2.0 / double(m_nh), 2.0 / double(m_nh));
  m_wy.set_size(m_nh, m_nclasses);
  m_wy.imbue( [&]() { return uni2(engine5); } );
  //m_wy *= 0.2;

  std::mt19937_64 engine6;
  std::uniform_real_distribution<double> uni3(-2.0 / double(m_nh), 2.0 / double(m_nh));
  m_wh.set_size(m_nh, m_nh);
  m_wh.imbue( [&]() { return uni3(engine6); } );
  //m_wh *= 0.2;

  m_h_tm1.set_size(1, m_nh);
  m_h_tm1.zeros();

  // some constants
  m_UNK_ALPHNUM_LOWER_IND = m_vocsize;
  m_UNK_ALPHNUM_UPPER_IND = m_vocsize + 1;
  m_UNK_NON_ALPHNUM_IND = m_vocsize + 2;
  m_UNK_SUFFIX_IND = 1; // 1st col in m_suf, 0th col is for empty feat
  //m_UNKNWON_SUPERCAT_IND = iter->second;
  //m_WORD_PAD_IND = 0; // assumes the 1st col (wiht index 0) is ***PADDING***

  m_dropout_vec.set_size(m_wx.n_rows, 1);
  m_dropout_vec.fill(m_dropout_success_prob);

  std::mt19937_64 engine7;
  std::uniform_real_distribution<double> uni4(-2.0 / double(2*m_de), 2.0 / double(2*m_de));

  m_comp_mat.set_size(2*m_de, m_de);
  m_comp_mat.imbue( [&]() { return uni4(engine7); } );
  //m_comp_mat *= 0.2;

  m_phrase_mat.set_size(850000, m_de);
  m_phrase_mat.row(0).imbue( [&]() { return uni(engine6); } );
  m_phrase_mat.row(0) *= 0.2;

  m_config_dir = config_dir;
  //debug
  m_avg_unique_prev_hypo_count = 0.0;
  m_total_unique_prev_hypo_count = 0.0;

  // todo
  if (cfg.use_lh_cache()) {
    cerr << "loading lh caches...\n";
    load_lh_cache("word_feat_emb_ind_map", m_word_emb_lh_cache_val);
    load_lh_cache("suf_feat_emb_ind_map", m_suf_emb_lh_cache_val);
  }
}


void
RNNParser::_Impl::re_init_mats(const string &wx, const string &wh, const string &wy,
                               const string &emb, const string &suf,
                               const string &super_emb, const string &comp_mat) {
  cerr << "rnn_parser re_init_mats...\n";

  m_wx.reset();
  m_wy.reset();
  m_wh.reset();

  m_emb.reset();
  m_suf.reset();
  //m_cap.reset();

  m_wx.load(wx);
  m_wh.load(wh);
  m_wy.load(wy);
  m_emb.load(emb);
  m_suf.load(suf);
  //m_cap.load(cap);

  if (cfg.use_phrase_emb_feats()) {
    m_comp_mat.load(comp_mat);
  }

  if (m_use_supercat_feats)
    m_supercat_emb.load(super_emb);

  if (cfg.use_lh_cache()) {
    size_t off_set = (m_de + m_ds)*m_feature_count;
    // this needs changing if more features are added later
    m_wx_super_sub = m_wx.submat(off_set, 0, m_wx.n_rows - 1, m_wx.n_cols - 1);
  }
}


void
RNNParser::_Impl::load_emb_mat(mat &emb, const string &filename) {

  cerr << "loading emb mat..., "
      << filename << endl;

  ifstream in(filename.c_str());

  size_t total = 0;

  if (!in)
    throw runtime_error("no such file: " + filename);
  string line;

  while (in) {
    if (!getline(in, line)) break;
    istringstream iss(line);

    size_t first_space_pos = line.find(" ");
    string word = line.substr(0, first_space_pos);
    string emb_vec = line.substr(first_space_pos + 1); // get till the end

    colvec temp(emb_vec);
    emb.col(total) = temp;
    if (total == 0)
      assert(word == "***PADDING***");
    assert(m_full_wordstr_emb_map.insert(make_pair(word, total)).second);
    ++total;
  }

  assert(total == m_vocsize);
  cerr << "total emb: " << total << endl;

  // test
  //emb.col(0).print(cerr, "word emb 0th col");
}


void
RNNParser::_Impl::load_pos_emb_mat(mat &emb, const string &filename) {

  cerr << "loading pos emb mat..., " << filename << endl;

  ifstream in(filename.c_str());
  if (!in)
    throw runtime_error("no such file: " + filename);

  // test
  //emb.col(2).print("emb col 2 old");

  string line;
  size_t total = 1;
  while (in) {
    if (!getline(in, line)) break;
    istringstream iss(line);

    ++total;
    size_t first_space_pos = line.find(" ");
    string pos = line.substr(0, first_space_pos);
    string emb_vec = line.substr(first_space_pos + 1); // get till the end

    colvec temp(emb_vec);
    emb.col(total) = temp;
    assert(m_pos_str_ind_map.find(pos)->second == total);
  }

  //emb.col(2).print("emb col 2 new");
  cerr << "total pos emb: " << total - 1 << endl;

  // test
  //emb.col(0).print(cerr, "word emb 0th col");
}


void
RNNParser::_Impl::load_super_emb_mat(mat &emb, const string &filename) {

  cerr << "loading super emb mat..., " << filename << endl;

  ifstream in(filename.c_str());
  if (!in)
    throw runtime_error("no such file: " + filename);

  //  // test
  //  emb.col(2).print("emb col 2 old");

  string line;
  size_t total = 1;
  while (in) {
    if (!getline(in, line)) break;
    istringstream iss(line);

    ++total;
    size_t first_space_pos = line.find(" ");
    string super = line.substr(0, first_space_pos);
    string emb_vec = line.substr(first_space_pos + 1); // get till the end

    colvec temp(emb_vec);
    emb.col(total) = temp;
    const Cat *cat = cats.markedup[super];
    assert(m_cats_map.get_ind(cat) == total);
  }

  //emb.col(2).print("emb col 2 new");
  cerr << "total super emb: " << total - 1 << endl;

  // test
  //emb.col(0).print(cerr, "word emb 0th col");
}


//void
//RNNParser::_Impl::load_emb_mat(mat &emb, const string &filename, bool reduced) {
//
//  cerr << "loading pre-trained embeddings..., "
//      << filename << endl;
//
//  ifstream in(filename.c_str());
//
//  size_t total = 0;
//
//  if (!in)
//    throw runtime_error("no such file: " + filename);
//  string line;
//  bool odd = true;
//  int ind;
//
//  while (in) {
//    if (!getline(in, line)) break;
//    istringstream iss(line);
//    if (odd) {
//      ++total;
//      iss >> ind;
//      odd = false;
//    } else {
//
//      if (reduced) {
//        assert(m_emb_ind_map.insert(make_pair(ind, 0)).second);
//        assert(ind < (int) m_vocsize - 3);
//      }
//
//      mat temp(line);
//      emb.col(ind) = temp.t();
//      odd = true;
//    }
//  }
//
//  cerr << "total emb: " << total << endl;
//
//  // test
//  //emb.col(0).print(cerr, "word emb 0th col");
//}


void RNNParser::_Impl::Clear()
{
  chart.reset();
  chart.pool->clear();
  sent.reset();
  if (!m_train) delete m_lattice;

  if (m_train)
  {
    //delete m_shiftReduceFeatures;
    //m_shiftReduceFeatures = new ShiftReduceFeature(cats, lexicon);
  }
}


void RNNParser::_Impl::clear_xf1()
{
  chart.pool->clear();
  delete m_lattice;
}


void RNNParser::_Impl::load_gold_trees(const std::string &filename)
{
  std::cerr << "Parser::_Impl::LoadGoldTree start" << std::endl;
  size_t treeId = 0;

  ifstream in(filename.c_str());
  if(!in)
    throw runtime_error("no such file: " + filename);

  vector<ShiftReduceAction*> *tree = new vector<ShiftReduceAction*>;
  while (in)
  {
    string s;
    if (!getline(in, s)) break;
    if (s == "###")
    {
      if (tree->size() > 0)
        m_goldTreeVec.push_back(tree);
      ++treeId;
      tree = new vector<ShiftReduceAction*>;
      continue;
    }

    istringstream ss(s);
    vector<string> node;
    while (ss)
    {
      string s;
      if (!getline(ss, s, ' ')) break;
      node.push_back(s);
    }
    const Cat *cat = cats.markedup[node.at(0)];
    if(!cat)
    {
      try
      {
        cat = cats.canonize(node.at(0).c_str());
      }
      catch(NLP::Exception e)
      {
        cerr << "parse cat failed: " << node.at(0) << endl;
        throw NLP::IOException("attempting to parse category failed for non-SHIFT");
      }
    }
    assert(cat);
    ShiftReduceAction *action = new ShiftReduceAction(atoi(node.at(1).c_str()), cat);
    tree->push_back(action);
  }
  if (tree->size() > 0) m_goldTreeVec.push_back(tree);
  cerr << "total number of gold tress: " << m_goldTreeVec.size() << endl;
}


void
RNNParser::_Impl::load_gold_trees_rnn_fmt(const std::string &filename) {
  std::cerr << "Parser::_Impl::load_gold_trees_rnn_fmt..." << std::endl;
  size_t treeId = 0;

  ifstream in(filename.c_str());
  if (!in)
    throw runtime_error("no such file: " + filename);

  vector<pair<size_t, size_t> > tree;
  while (in)
  {
    string s;
    if (!getline(in, s)) break;
    if (s == "###")
    {
      if (treeId > 0)
        assert(tree.size() != 0);
      if (tree.size() > 0)
        m_gold_tree_vec_rnn.push_back(tree);
      ++treeId;
      tree.clear();
      continue;
    }

    istringstream ss(s);
    vector<string> node;
    while (ss)
    {
      string s;
      if (!getline(ss, s, ' ')) break;
      node.push_back(s);
    }

    int action_id = atoi(node.at(1).c_str());
    int res_cat_id = 0; // for all reduce actions, this will be zero
    if (action_id != 2) {
      assert(action_id == 0 || action_id == 1);
      res_cat_id = atoi(node.at(0).c_str());
    }

    tree.push_back(make_pair((size_t)action_id, (size_t)res_cat_id));
  }

  if (tree.size() > 0)
    m_gold_tree_vec_rnn.push_back(tree);
  cerr << "total number of gold trees rnn format: " << m_gold_tree_vec_rnn.size() << endl;

  //  // unit test
  //
  //  for (size_t i = 0; i < m_gold_tree_vec_rnn.size(); ++i) {
  //    for (size_t j = 0; j < m_gold_tree_vec_rnn[i].size(); ++j) {
  //      cerr << m_gold_tree_vec_rnn[i][j].first << " " << m_gold_tree_vec_rnn[i][j].second << endl;
  //    }
  //    cerr << endl;
  //  }


}


//const ShiftReduceHypothesis* RNNParser::_Impl::sr_parse(const double BETA, bool qu_parsing)
//{
//	try
//	{
//		// training related
//		vector<ShiftReduceAction*> nullVec;
//		const ShiftReduceHypothesis *bestHypo; // the highest-scored hypo on each level
//		const ShiftReduceHypothesis *goldHypo;
//		//vector<ShiftReduceAction*> &goldTreeActions = m_train ? *m_goldTreeVec.at(202) : nullVec;
//		vector<ShiftReduceAction*> &goldTreeActions = m_train ? *m_goldTreeVec.at(Integration::s_perceptronId[0] - 1) : nullVec;
//		size_t goldTreeSize = goldTreeActions.size();
//		ShiftReduceAction *goldAction = 0;
//		size_t gold_action_id = 0;
//		bool goldFinished = false;
//
//		assert(sent.words.size() != 0);
//		if(sent.words.size() > cfg.maxwords())
//			return 0;
//		//throw NLP::ParseError("sentence length exceeds maximum number of words for parser", nsentences);
//
//		const ShiftReduceHypothesis *candidateOutput = 0;
//		//vector<size_t> word_inds_vec;
//		//words2word_inds(sent.words, word_inds_vec);
//		//vector<size_t> suf_inds_vec;
//		//vector<size_t> cap_inds_vec;
//		//suf_cap2suf_cap_inds(sent.pos, suf_inds_vec, cap_inds_vec);
//
//		nsentences++;
//		SuperCat::nsupercats = 0;
//		const long NWORDS = sent.words.size();
//		const MultiRaws &input = sent.msuper;
//
//		m_lattice = new ShiftReduceLattice(NWORDS, m_beamSize, MAX_UNARY_ACTIONS);
//		ShiftReduceHypothesis *startHypo = new ShiftReduceHypothesis(0, 0, 0, 0, 0, 0, 0.0, false, 0, 0, 0);
//		startHypo->SetStartHypoFlag();
//		m_lattice->Add(0, startHypo);
//		if (m_train) goldHypo = m_lattice->GetLatticeArray(0); // gold == start
//		size_t startIndex;
//
//		//cerr << "sentence ID: " << Integration::s_perceptronId[0] << endl;
//
//		// set start and end indexes of the lattice
//		while (m_lattice->GetEndIndex() == 0 || m_lattice->GetEndIndex() > m_lattice->GetPrevEndIndex())
//		{
//			if (m_lattice->GetEndIndex() != 0)
//				startIndex = m_lattice->GetPrevEndIndex() + 1;
//			else
//				startIndex = 0;
//			m_lattice->SetPrevEndIndex(m_lattice->GetEndIndex());
//
//			HypothesisPQueue hypoQueue;
//			for (size_t i = startIndex; i <= m_lattice->GetEndIndex(); ++i)
//			{
//				const ShiftReduceHypothesis *hypo = m_lattice->GetLatticeArray(i);
//				size_t j = hypo->GetNextInputIndex();
//				if (hypo->IsFinished(NWORDS, m_allowFragTree))
//				{
//					if (candidateOutput == 0 || hypo->GetTotalScore() > candidateOutput->GetTotalScore())
//						candidateOutput = hypo;
//				}
//
//				ShiftReduceContext context;
//				context.LoadContext(hypo, words, tags);
//
//				// shift
//				if (j < static_cast<size_t>(NWORDS))
//				{
//					const MultiRaw &multi = input[j];
//					//double prob_cutoff = multi[0].score*BETA;
//					for (MultiRaw::const_iterator k = multi.begin(); k!= multi.end(); ++k)
//					{
//						//if(k->score < prob_cutoff)
//						//continue;
//						const Cat *cat = cats.markedup[k->raw];
//						if(!cat)
//						{
//							//cerr << "sent ID: " << Integration::s_perceptronId[0];
//							throw NLP::ParseError(": SHIFT action error: attempted to load category without markedup " + k->raw);
//						}
//						SuperCat *stackTopCat = SuperCat::Lexical(chart.pool, j+1, cat, 0);
//						assert(stackTopCat->cat);
//						//maybe change this to non-pointer type
//						ShiftReduceAction *actionShift = new ShiftReduceAction(SHIFT, stackTopCat);
//
//						//double actionScore = GetOrUpdateWeight(false, false, context, *actionShift, words, tags);
//						//double hypoTotalScore = hypo->GetTotalScore() + actionScore;
//						//HypothesisTuple *newHypoTuple = new HypothesisTuple(hypo, actionShift, hypoTotalScore, i);
//						//hypoQueue.push(newHypoTuple);
//
//						//delete actionShift;
//						//delete newHypoTuple;
//					} // end for
//				} // end if
//
//				// unary
//				if (hypo->GetStackTopSuperCat() != 0)
//				{
//					//          const SuperCat *sc = hypo->GetStackTopSuperCat();
//					//          vector<SuperCat *> tmp;
//					//          tmp.clear();
//					//          if (!sc->IsLex() && !sc->IsTr())
//					//          {
//					//            UnaryLex(hypo->GetStackTopSuperCat(), qu_parsing, tmp);
//					//            UnaryTr(sc, tmp);
//					//          }
//					//          else if (sc->IsLex() && !sc->IsTr())
//					//          {
//					//            UnaryTr(sc, tmp);
//					//          }
//					//          else if (!sc->IsLex() && sc->IsTr()) { /* do nothing */ }
//					//          else if (sc->IsLex() && sc->IsTr()) { /* do nothing */ }
//					//
//					//          for (vector<SuperCat *>::iterator j = tmp.begin(); j < tmp.end(); ++j)
//					//          {
//					//            SuperCat *stackTopCat = *j;
//					//            assert(stackTopCat->left);
//					//            ShiftReduceAction *actionUnary = new ShiftReduceAction(UNARY, stackTopCat);
//					//
//					//            double actionScore = GetOrUpdateWeight(false, false, context, *actionUnary, words, tags);
//					//            double hypoTotalScore = hypo->GetTotalScore() + actionScore;
//					//            HypothesisTuple *newHypoTuple = new HypothesisTuple(hypo, actionUnary, hypoTotalScore, i);
//					//            hypoQueue.push(newHypoTuple);
//					//          }
//				}
//
//				// combine
//				if (hypo->GetPrvStack() != 0 && hypo->GetPrvStack()->GetStackTopSuperCat() != 0
//						&& hypo->GetStackTopSuperCat() != 0)
//				{
//					//          //if (hypo == goldHypo) cerr << "*gold*" << endl;
//					//          //cerr << "reduce: " << hypo->GetPrvStack()->GetStackTopSuperCat()->GetCat()->out_novar_noX_noNB_SR_str() << " and "
//					//          //<< hypo->GetStackTopSuperCat()->GetCat()->out_novar_noX_noNB_SR_str() << endl;
//					//          results.clear();
//					//          if (!cfg.seen_rules() ||
//					//              rule_instances(hypo->GetPrvStack()->GetStackTopSuperCat()->GetCat(), hypo->GetStackTopSuperCat()->GetCat()))
//					//            rules(hypo->GetPrvStack()->GetStackTopSuperCat(), hypo->GetStackTopSuperCat(),
//					//                cfg.eisner_nf(), cfg.seen_rules(), cfg.question_rules(), results);
//					//          if (results.size() > 0)
//					//          {
//					//            for (vector<SuperCat *>::iterator k = results.begin(); k < results.end(); ++k)
//					//            {
//					//              SuperCat *stackTopCat = *k;
//					//              //cerr << "result: " << stackTopCat->GetCat()->out_novar_noX_noNB_SR_str() << endl << endl;
//					//              ShiftReduceAction *actionCombine = new ShiftReduceAction(COMBINE, stackTopCat);
//					//
//					//              double actionScore = GetOrUpdateWeight(false, false, context, *actionCombine, words, tags);
//					//              double hypoTotalScore = hypo->GetTotalScore() + actionScore;
//					//              HypothesisTuple *newHypoTuple = new HypothesisTuple(hypo, actionCombine, hypoTotalScore, i);
//					//              hypoQueue.push(newHypoTuple);
//					//            }
//					//          }
//				}
//			} // end for
//
//			bestHypo = 0; // the highest-scored hypo on each level
//			if (m_train && gold_action_id < goldTreeSize)
//			{
//				goldAction = goldTreeActions[gold_action_id];
//				assert(goldAction);
//			}
//			if (gold_action_id >= goldTreeSize) goldFinished = true;
//			construct_hypo(hypoQueue, m_lattice, bestHypo, goldHypo, goldAction, goldFinished);
//
//			/*   //debug
//	    if (m_train && m_hasOutsideRule && goldActionId < goldTreeSize)
//	    {
//		cerr << "sent ID: " << m_perceptronId[0] << " ###has outside rule### " << *goldAction <<  endl;
//		cerr << "gold action is: ";
//		if (goldAction->GetAction() == 0)
//		    cerr << *goldAction << endl;
//		if (goldAction->GetAction() == 1)
//		{
//		    ShiftReduceAction *prevAction = goldTreeActions[goldActionId - 1];
//		    cerr << "1: " << "from " << prevAction->GetCat()->out_novar_noX_noNB_SR_str() << "to "
//			    << goldAction->GetCat()->out_novar_noX_noNB_SR_str() << endl;
//		}
//		if (goldAction->GetAction() == 2)
//		{
//		    cerr << "2: " << goldHypo->GetPrvStack()->GetStackTopSuperCat()->GetCat()->out_novar_noX_noNB_SR_str() << " and "
//			    << goldHypo->GetStackTopSuperCat()->GetCat()->out_novar_noX_noNB_SR_str() << endl;
//		}
//		cerr << endl << "at " << goldActionId + 1 << " out of " << goldTreeSize << " gold actions for sentence ID: " << Integration::s_perceptronId[0] << endl << endl;
//
//		return 0;
//	    }*/
//
//			//assert(bestHypo != 0); best hypo *could* be 0
//
//			// early update
//			//std::cerr << "has gold action?: " << m_hasGoldAction << std::endl;
//			// it's possible to have missed the gold action, e.g., goes beyond the number of
//			// gold actions, but one previously updated candidate output already matches the gold
//			// thus the need for the third condition
//			if (m_train && !m_hasGoldAction && candidateOutput != goldHypo && goldActionId < goldTreeSize)
//			{
//				//        cerr << "########## Early Update, sentenceID: " << m_perceptronId[0] << endl;
//				//        cerr << "found: " << goldActionId << " out of " << goldTreeSize << " gold actions" << endl;
//				//        cerr << "faild to find gold action: " << goldAction->GetAction() << " " << goldAction->GetCat()->out_novar_noX_noNB_SR_str() << endl;
//				//        cerr << "gold action is: ";
//				//        if (goldAction->GetAction() == 0)
//				//          cerr << *goldAction << endl;
//				//        if (goldAction->GetAction() == 1)
//				//        {
//				//          ShiftReduceAction *prevAction = goldTreeActions[goldActionId - 1];
//				//          cerr << "1: " << "from " << prevAction->GetCat()->out_novar_noX_noNB_SR_str() << "to "
//				//              << goldAction->GetCat()->out_novar_noX_noNB_SR_str() << endl;
//				//        }
//				//        if (goldAction->GetAction() == 2)
//				//        {
//				//          cerr << "2: " << goldHypo->GetPrvStack()->GetStackTopSuperCat()->GetCat()->out_novar_noX_noNB_SR_str() << " and "
//				//              << goldHypo->GetStackTopSuperCat()->GetCat()->out_novar_noX_noNB_SR_str() << endl;
//				//        }
//				//
//				//        assert(goldAction->GetCat());
//				//        SuperCat *goldSuperCat = SuperCat::Wrap(chart.pool, goldAction->GetCat());
//				//        assert(goldSuperCat);
//				//
//				//        if (bestHypo == 0 || (candidateOutput && candidateOutput->GetTotalScore() > bestHypo->GetTotalScore()))
//				//          bestHypo = candidateOutput;
//				//        assert(bestHypo != 0);
//				//        // not all gold actions have been found
//				//        Action(goldHypo, goldAction->GetAction(), m_lattice, goldSuperCat, 0.0);
//				//        goldHypo = m_lattice->GetLatticeArray(m_lattice->GetEndIndex());
//				//        //PerceptronUpdates(bestHypo, goldHypo, words, tags);
//				//        std::cerr << "########## done early update" << std::endl;
//				//        // hack exit
//				//        //chart.reset();
//				//        sent.reset();
//				//        delete m_lattice;
//
//				return 0;
//			}
//			if (m_train) ++goldActionId;
//		} // end while
//
//		// final update, if no early update
//		if (m_train)
//		{
//			//      if (candidateOutput != goldHypo)
//			//      {
//			//        cerr << "######### late update" << endl;
//			//        //PerceptronUpdates(candidateOutput, goldHypo, words, tags);
//			//      }
//			//      else
//			//      {
//			//        cerr << "sentence: " << m_perceptronId[0] << "###### output correct #####" << endl;
//			//      }
//			//      sent.reset();
//			//      delete m_lattice;
//		}
//
//		if (candidateOutput != 0)
//			return candidateOutput;
//
//		return 0;
//	}
//	catch(NLP::Exception e)
//	{
//		//throw NLP::ParseError(e.msg, nsentences);
//		cerr << e.msg << endl;
//		return 0;
//	}
//}


void RNNParser::_Impl::construct_hypo(HypothesisPQueue &hypoQueue, ShiftReduceLattice *lattice,
                                      const ShiftReduceHypothesis *&bestHypo, const ShiftReduceHypothesis *&goldHypo,
                                      ShiftReduceAction *goldAction, const bool goldFinished) {

  unordered_map<size_t, short> hypo_index_map;

  if (m_train) {
    m_hasGoldAction = false;
  }

  size_t size = hypoQueue.size() > m_beamSize ? m_beamSize : hypoQueue.size();
  for (size_t beam = 0; beam < size; ++beam) {
    HypothesisTuple *tuple = hypoQueue.top();
    const ShiftReduceHypothesis *hypo = tuple->GetCurrentHypo();
    assert(hypo);
    double totalScore = tuple->GetHypoTotalScore();
    size_t actionId = tuple->GetAction()->GetAction();
    const SuperCat *superCat = tuple->GetAction()->GetSuperCat();
    ShiftReduceAction action(actionId, superCat);

    // debug, something needed for deciding bptt optimization
    //hypo_index_map.insert({hypo->m_indexInLattice, 0});

    _action(hypo, actionId, m_lattice, superCat, totalScore, tuple->m_action_id, tuple->m_action_score);

    // bptt optimization (implementing bptt over DAG)
    if (hypo->m_child_hypos.size() == 0)
      hypo->m_child_hypos.reserve(m_beamSize);
    hypo->m_child_hypos.emplace_back(m_lattice->GetLatticeArray(m_lattice->GetEndIndex()));

    if (m_train && !goldFinished) {
      const ShiftReduceHypothesis *newHypo = m_lattice->GetLatticeArray(m_lattice->GetEndIndex());
      assert(newHypo);
      if (bestHypo == 0 || newHypo->GetTotalScore() > bestHypo->GetTotalScore()) {
        assert(m_lattice->GetEndIndex() != 0);
        bestHypo = newHypo;
        assert(bestHypo != 0);
      }
      if (hypo == goldHypo && action == *goldAction) {
        goldHypo = newHypo;
        m_hasGoldAction = true;
      }
    }
    hypoQueue.pop();
    delete tuple;
  } // end for

  m_avg_unique_prev_hypo_count += hypo_index_map.size();

  // make this into a function of the PQ class?
  if (!hypoQueue.empty())
    assert(size == m_beamSize);
  else
    assert(size <= m_beamSize);

  while(!hypoQueue.empty())
  {
    HypothesisTuple *tuple = hypoQueue.top();
    hypoQueue.pop();
    delete tuple;
  }
  assert(hypoQueue.size() == 0);
}


void RNNParser::_Impl::construct_hypo_max_margin(HypothesisPQueue &hypoQueue, ShiftReduceLattice *lattice,
                                                 const ShiftReduceHypothesis *&bestHypo, const ShiftReduceHypothesis *&goldHypo,
                                                 ShiftReduceAction *goldAction, const bool goldFinished,
                                                 const size_t current_gold_action_count) {
  //  cerr << current_gold_action_count << endl;
  //  cerr << m_gold_equiv_map.size() << endl;
  assert(current_gold_action_count == m_gold_equiv_map.size());
  if (m_train) {  m_hasGoldAction = false; }
  size_t size = hypoQueue.size() > m_beamSize ? m_beamSize : hypoQueue.size();

  for (size_t beam = 0; beam < size; ++beam) {
    HypothesisTuple *tuple = hypoQueue.top();
    const ShiftReduceHypothesis *hypo = tuple->GetCurrentHypo();
    assert(hypo);
    double totalScore = tuple->GetHypoTotalScore();
    size_t actionId = tuple->GetAction()->GetAction();
    const SuperCat *superCat = tuple->GetAction()->GetSuperCat();
    ShiftReduceAction action(actionId, superCat);
    _action(hypo, actionId, m_lattice, superCat, totalScore, tuple->m_action_id, tuple->m_action_score);

    ShiftReduceHypothesis *newHypo = const_cast<ShiftReduceHypothesis *>(m_lattice->GetLatticeArray(m_lattice->GetEndIndex()));
    assert(newHypo);
    newHypo->m_total_gold_action_count = hypo->m_total_gold_action_count;
    count_gold_actions(newHypo, action);

    assert(newHypo->GetPrevHypo() == hypo);

    if (m_train && !goldFinished) {
      if (hypo == goldHypo && action == *goldAction) {
        goldHypo = newHypo;
        newHypo->m_total_gold_action_count += 1.0;
        m_hasGoldAction = true;
      }

      if (cfg.use_structrue_loss() && (!bestHypo ||
          (newHypo->GetTotalScore() + cfg.loss_pen()*(current_gold_action_count - newHypo->m_total_gold_action_count))
          > (bestHypo->GetTotalScore() + cfg.loss_pen()*(current_gold_action_count - bestHypo->m_total_gold_action_count)) )) {
        bestHypo = newHypo;
      }

      if (!cfg.use_structrue_loss() && (!bestHypo || newHypo->GetTotalScore() > bestHypo->GetTotalScore())) {
        assert(m_lattice->GetEndIndex() != 0);
        bestHypo = newHypo;
        assert(bestHypo);
      }
    }
    hypoQueue.pop();
    delete tuple;
  } // end for

  // make this into a function of the PQ class?
  if (!hypoQueue.empty())
    assert(size == m_beamSize);
  else
    assert(size <= m_beamSize);

  while(!hypoQueue.empty())
  {
    HypothesisTuple *tuple = hypoQueue.top();
    hypoQueue.pop();
    delete tuple;
  }
  assert(hypoQueue.size() == 0);
}


// for max-margin training
void RNNParser::_Impl::count_gold_actions(ShiftReduceHypothesis *&hypo,
                                          ShiftReduceAction &action) {
  // hypo is current hypo and action is a proposed action for this hypo
  //bool partial_match;
  unordered_map<ShiftReduceAction, size_t, KeyHash, KeyEqual>::iterator iter = m_gold_equiv_map.find(action);
  if (iter == m_gold_equiv_map.end())
    //    partial_match = true;
    //  else
    return;
  if (action.GetAction() == SHIFT) {
    ++hypo->m_total_gold_action_count;
  } else if (action.GetAction() == COMBINE) {
    if (hypo->m_prevHypo->m_stack_top_action_is_gold &&
        hypo->m_prevHypo->m_prevHypo->m_stack_top_action_is_gold)
      ++hypo->m_total_gold_action_count;
  } else if (action.GetAction() == UNARY) {
    if (hypo->m_prevHypo->m_stack_top_action_is_gold)
      ++hypo->m_total_gold_action_count;
  }
}


void RNNParser::_Impl::_action(const ShiftReduceHypothesis *hypo,
                               size_t action, ShiftReduceLattice *lattice,
                               const SuperCat *supercat, const double totalScore,
                               const size_t action_id,
                               const double action_score)
{
  assert(supercat);
  assert(hypo);
  switch (action) {
  case SHIFT:
    m_lattice->Add(m_lattice->GetEndIndex() + 1, hypo->Shift(m_lattice->GetEndIndex() + 1, supercat,
                                                             hypo->GetUnaryActionCount(), totalScore, action_id, action_score));
    m_lattice->IncrementEndIndex();
    break;
  case UNARY:
    m_lattice->Add(m_lattice->GetEndIndex() + 1, hypo->Unary(m_lattice->GetEndIndex() + 1, supercat,
                                                             hypo->GetUnaryActionCount() + 1, totalScore, action_id, action_score));
    m_lattice->IncrementEndIndex();
    break;
  case COMBINE:
    m_lattice->Add(m_lattice->GetEndIndex() + 1, hypo->Combine(m_lattice->GetEndIndex() + 1, supercat,
                                                               hypo->GetUnaryActionCount(), totalScore, action_id, action_score));
    m_lattice->IncrementEndIndex();
    break;
  }
}


void RNNParser::_Impl::_add_lex(const Cat *cat, const SuperCat *sc,
                                bool replace, RuleID ruleid,
                                vector<SuperCat *> &tmp,
                                const size_t tc_id)
{
  SuperCat *new_sc = SuperCat::LexRule(chart.pool, cat, SuperCat::LEX, sc, replace, ruleid);
  new_sc->SetLex();
  assert(new_sc->left != 0);
  new_sc->m_unary_id = tc_id;
  tmp.push_back(new_sc);
}


void RNNParser::_Impl::unary_tc(const SuperCat *sc,
                                bool qu_parsing,
                                vector<SuperCat *> &tmp)
{
  // tmp will be the union of sc and lex'ed sc
  //tmp.push_back(sc);

  const Cat *cat = sc->cat;

  //cerr << "unary lex: " << cat->out_novar_noX_noNB_SR_str() << endl;

  if(cat->is_N())
    _add_lex(NP, sc, false, 1, tmp, 2);
  else if(cfg.extra_rules.get_value() && cat->is_NP())
    _add_lex(NPfNPbNP, sc, false, 11, tmp, 4);
  else if(cat->is_SbNP()){
    RuleID ruleid = 0;
    switch(cat->res->feature){
    case Features::DCL:
      if(!cfg.extra_rules.get_value())
        //continue;
        break;
      ruleid = 12;
      break;
    case Features::PSS:
      if(cfg.extra_rules.get_value()){
        _add_lex(SbNPbSbNP, sc, false, 17, tmp, 6);
        _add_lex(SfS, sc, false, 13, tmp, 7);
      }
      if(qu_parsing)
        _add_lex(NbN, sc, true, 95, tmp);
      ruleid = 2;
      break;
    case Features::NG:
      _add_lex(SbNPbSbNP, sc, false, 4, tmp, 6);
      _add_lex(SfS, sc, false, 5, tmp, 7);
      if(cfg.extra_rules.get_value()){
        _add_lex(SbNPfSbNP, sc, false, 18, tmp, 5);
        _add_lex(SbS, sc, false, 16, tmp, 8);
        _add_lex(NP, sc, false, 20, tmp, 2);
      }
      ruleid = 3;
      break;
    case Features::ADJ:
      // given this a 93 id to match the gen_lex rule
      _add_lex(SbNPbSbNP, sc, false, 93, tmp, 6);
      if(cfg.extra_rules.get_value())
        _add_lex(SfS, sc, false, 15, tmp, 7);
      if(qu_parsing)
        _add_lex(NbN, sc, true, 94, tmp);
      ruleid = 6;
      break;
    case Features::TO:
      _add_lex(SbNPbSbNP, sc, false, 8, tmp, 6);
      _add_lex(NbN, sc, true, 9, tmp, 1);
      if(cfg.extra_rules.get_value())
        _add_lex(SfS, sc, false, 14, tmp, 7);
      ruleid = 7;
      break;
    default:
      break;
      //continue;
    }
    _add_lex(NPbNP, sc, true, ruleid, tmp, 3);
  }else if(cat->is_SfNP() && cat->res->has_dcl())
    _add_lex(NPbNP, sc, true, 10, tmp, 3);
  else if(cfg.extra_rules.get_value() && cat->is_StobNPfNP())
    _add_lex(NPbNP, sc, true, 19, tmp, 3);
  else if(cfg.extra_rules.get_value() && cat->is_Sdcl()){
    _add_lex(NPbNP, sc, false, 21, tmp, 3);
    _add_lex(SbS, sc, false, 22, tmp, 8);
  }
}


void RNNParser::_Impl::unary_tr(const SuperCat *supercat,
                                std::vector<SuperCat *> &tmp)
{
  //const size_t tmpSize = tmp.size();
  //for(size_t i = 0; i < tmpSize; ++i){
  //SuperCat *sc = tmp[i];
  const Cat *cat = supercat->cat;

  const TRCats *trcats = 0;
  if(cat->is_NP())
    trcats = &cats.trNP;
  else if(cat->is_AP())
    trcats = &cats.trAP;
  else if(cat->is_PP())
    trcats = &cats.trPP;
  else if(cat->is_StobNP())
    trcats = &cats.trVP_to;
  else { }

  if (trcats != 0)
  {
    for(TRCats::const_iterator j = trcats->begin(); j != trcats->end(); ++j)
    {
      //cerr << "TypeRaise" << endl;
      SuperCat *new_sc = SuperCat::TypeRaise(chart.pool, *j, SuperCat::TR, supercat, 11);
      assert(new_sc->left);
      new_sc->SetTr();
      new_sc->m_unary_id = j->ind;
      //cerr << "tr cat: " << *new_sc->cat << endl;
      tmp.push_back(new_sc);
    }
  }
  //}
}

void
RNNParser::_Impl::raws2words(const vector<std::string> &raw,
                             Words &words) const {
  words.resize(0);
  words.reserve(sent.words.size());
  for(vector<std::string>::const_iterator i = raw.begin(); i != raw.end(); ++i)
    words.push_back(lexicon[*i]);
}


//void
//RNNParser::_Impl::pos2pos_inds(const vector<std::string> &raw,
//                               std::vector<size_t> &pos_inds_vec) const {
//  pos_inds_vec.resize(0);
//  pos_inds_vec.reserve(sent_rnn_fmt.words.size());
//  unordered_map<string, size_t>::const_iterator iter;
//
//  for(vector<std::string>::const_iterator i = raw.begin(); i != raw.end(); ++i) {
//    iter = m_pos_str_ind_map.find(*i);
//    //cerr << "pos: "<< *i << endl;
//    //assert(iter != m_pos_str_ind_map.end());
//    //assert(iter->second >= 1);
//    if (iter == m_pos_str_ind_map.end()) {
//      // this is a hack for now, (for pos tag e.g. LQU that's not in 0221, 00, 23 but in wiki)
//      pos_inds_vec.push_back(0);
//    } else
//      pos_inds_vec.push_back(iter->second);
//  }
//
//    // test
////    cerr << "pos inds_vec: " << endl;
////    for (size_t i = 0; i < pos_inds_vec.size(); ++i) {
////      cerr << pos_inds_vec[i] << " " << endl;
////    }
////    cerr << endl;
//}

//
//void
//RNNParser::_Impl::suf2suf_inds(const vector<std::string> &raw,
//                               std::vector<size_t> &suf_inds_vec) const {
//  suf_inds_vec.resize(0);
//  suf_inds_vec.reserve(sent_rnn_fmt.words.size());
//  unordered_map<string, size_t>::const_iterator iter;
//
//  // test
//  //assert(m_suf_str_ind_map.size() != 0);
//
//  for(vector<std::string>::const_iterator i = raw.begin(); i != raw.end(); ++i) {
//    iter = m_suf_str_ind_map.find(*i);
//    if (iter == m_suf_str_ind_map.end()) {
//      // 0 for padding, 1 for unk
//      // at training time, should have no unk suffix
//      assert(!m_train);
//      suf_inds_vec.push_back(1);
//    } else
//      suf_inds_vec.push_back(iter->second);
//  }
//
////  // test
////  cerr << "suf inds_vec: " << endl;
////  for (size_t i = 0; i < suf_inds_vec.size(); ++i) {
////    cerr << suf_inds_vec[i] << " " << endl;
////  }
////  cerr << endl;
//}



void
RNNParser::_Impl::words2word_inds(const vector<std::string> &raw,
                                  const vector<std::string> &pos,
                                  std::vector<size_t> &word_inds_vec,
                                  std::vector<size_t> &suf_inds_vec) const {
  word_inds_vec.reserve(sent.words.size());
  suf_inds_vec.reserve(sent.words.size());
  assert(raw.size() == pos.size());
  assert(raw.size() == sent.words.size());
  for(size_t i = 0; i < raw.size(); ++i) {
    vector<size_t> word_inds = word2vec(raw[i]);
    word_inds_vec.emplace_back(word_inds[0]);
    if (cfg.use_pos()) {
      unordered_map<string, size_t>::const_iterator iter = m_pos_str_ind_map.find(pos[i]);
      assert(iter != m_pos_str_ind_map.end());
      suf_inds_vec.emplace_back(iter->second);
    }
    else
      suf_inds_vec.emplace_back(word_inds[1]);
  }
}


void
RNNParser::_Impl::clean_str(string &word, vector<string> &ret) const {

  string word_copy1 = word;
  if (!is_lower(word)) {
    std::transform(word_copy1.begin(), word_copy1.end(), word_copy1.begin(), ::tolower);
    ret.push_back(word_copy1);
  }

  string word_copy2 = word;
  replace_all_substrs(word_copy2, ",", "");
  replace_all_substrs(word_copy2, ".", "");
  replace_all_substrs(word_copy2, "-", "");
  replace_all_substrs(word_copy2, " ", "");
  replace_all_substrs(word_copy2, "\\/", "");

  if (all_of(word_copy2.begin(), word_copy2.end(), ::isdigit)) {
    ret.push_back("0");
  } else if (word.find('-') != string::npos) {
    size_t last_pos = word.find_last_of('-');
    assert(last_pos != string::npos);
    string sub_str = word.substr(last_pos + 1);
    ret.push_back(sub_str);

    if (!is_lower(sub_str)) {
      std::transform(sub_str.begin(), sub_str.end(), sub_str.begin(), ::tolower);
      ret.push_back(sub_str);
    }

  } else {
    size_t pos = word.find("\\/");
    if (pos != string::npos) {
      size_t last_pos = word.find_last_of("/");
      string sub_str = word.substr(last_pos + 1);
      ret.push_back(sub_str);

      if (!is_lower(sub_str)) {
        std::transform(sub_str.begin(), sub_str.end(), sub_str.begin(), ::tolower);
        ret.push_back(sub_str);
      }
    }
  }

}


string
RNNParser::_Impl::get_suf(const string &word) const {
  string word_copy = word;
  //cerr << "word: " << word << endl;
  replace_all_substrs(word_copy, ",", "");
  replace_all_substrs(word_copy, ".", "");
  replace_all_substrs(word_copy, "-", "");
  replace_all_substrs(word_copy, " ", "");
  replace_all_substrs(word_copy, "\\/", "");
  if (all_of(word_copy.begin(), word_copy.end(), ::isdigit))
    return "0";
  else {
    if (word.length() >= 2)
      return word.substr(word.length() - 2);
    else
      return word;
  }
}


void
RNNParser::_Impl::replace_all_substrs(string &str,
                                      const string &substr,
                                      const string &re) const {
  string::size_type n = 0;

  while ((n = str.find(substr, n)) != std::string::npos) {
    str.replace(n, substr.size(), re);
    n += substr.size();
  }
}


vector<size_t>
RNNParser::_Impl::word2vec(const string &word) const {
  vector<size_t> word_vec;
  string original_word = word;
  string word_copy = word;
  //
  //  if (m_lowercase_words)
  //    std::transform(word_copy.begin(), word_copy.end(), word_copy.begin(), ::tolower);

  unordered_map<string, size_t>::const_iterator iter = m_full_wordstr_emb_map.find(word_copy);

  if (iter != m_full_wordstr_emb_map.end()) {
    word_vec.push_back(iter->second);
  } else {
    // not in pre-trained embeddings, try to back off
    bool backed_off = false;
    vector<string> back_off_strs;
    clean_str(original_word, back_off_strs);
    for (size_t i = 0; i < back_off_strs.size(); ++i) {
      iter = m_full_wordstr_emb_map.find(back_off_strs[i]);
      if (iter != m_full_wordstr_emb_map.end()) {
        backed_off = true;
        word_vec.push_back(iter->second);
        break;
      }
    }

    if (!backed_off) {
      // unknown word
      if (is_alphnum(original_word)) {
        if (islower(original_word[0]))
          word_vec.push_back(m_UNK_ALPHNUM_LOWER_IND);
        else
          word_vec.push_back(m_UNK_ALPHNUM_UPPER_IND);
      } else {
        word_vec.push_back(m_UNK_NON_ALPHNUM_IND);
      }
    }

  }

  unordered_map<string, size_t>::const_iterator iter2 = m_suf_str_ind_map.find(get_suf(original_word));
  if (iter2 != m_suf_str_ind_map.end())
    word_vec.push_back(iter2->second);
  else
    word_vec.push_back(m_UNK_SUFFIX_IND);

  //  if (islower(original_word[0]))
  //    word_vec.push_back(0);
  //  else
  //    word_vec.push_back(1);

  return word_vec;
}

bool
RNNParser::_Impl::is_lower(const string &word) const {
  for (size_t i = 0; i < word.length(); ++i) {
    if (isupper(word[i]))
      return false;
  }

  return true;
}


bool
RNNParser::_Impl::is_alphnum(const string &word) const {
  for (size_t i = 0; i < word.length(); ++i) {
    if (!isalnum(word[i]))
      return false;
  }

  return true;

}


void
RNNParser::_Impl::load_suf_str_ind_map(const string &file) {

  cerr << "loading suf str ind map\n";

  ifstream in(file.c_str());
  if(!in) {
    cerr << "could not open suf_str_ind file " << file << endl;
    exit(EXIT_FAILURE);
  }

  string suf;
  size_t temp;
  size_t total = 1;

  // 0th col for empty feat, 1th col for unk_suf
  // so total starts from 2
  while (in >> suf >> temp) {
    ++total;
    m_suf_str_ind_map.insert(make_pair(suf, total));
  }

  cerr << "total suffix count (from sec0221): " << m_suf_str_ind_map.size() << endl;

}



//void
//RNNParser::_Impl::suf_cap2suf_cap_inds(const vector<std::string> &raw,
//                                       std::vector<size_t> &suf_inds_vec,
//                                       std::vector<size_t> &cap_inds_vec) const {
//  suf_inds_vec.resize(0);
//  suf_inds_vec.reserve(sent_rnn_fmt.words.size());
//  cap_inds_vec.resize(0);
//  cap_inds_vec.reserve(sent_rnn_fmt.words.size());
//  for(vector<std::string>::const_iterator i = raw.begin(); i != raw.end(); ++i) {
//    string temp = *i;
//    size_t pos = temp.find('_');
//    size_t suf = stoi(temp.substr(0, pos));
//    size_t cap = stoi(temp.substr(pos + 1));
//    suf_inds_vec.push_back(suf);
//    cap_inds_vec.push_back(cap);
//  }
//
//  assert(suf_inds_vec.size() == cap_inds_vec.size());
//
//  //  cerr << "suf_inds_vec: " << endl;
//  //
//  //  // test
//  //  for (size_t i = 0; i < suf_inds_vec.size(); ++i) {
//  //    cerr << suf_inds_vec[i] << " " << endl;
//  //  }
//  //  cerr << endl;
//  //
//  //  cerr << "cap_inds_vec: " << endl;
//  //
//  //  for (size_t i = 0; i < cap_inds_vec.size(); ++i) {
//  //    cerr << cap_inds_vec[i] << " " << endl;
//  //  }
//  //  cerr << endl;
//
//}

void
RNNParser::_Impl::test_contextwin2minibatch() {
  vector<size_t> words;
  vector<size_t> sufs;
  vector<size_t> caps;

  words.push_back(1);
  sufs.push_back(2);
  caps.push_back(3);

  vector<vector<size_t> > context1;
  vector<vector<size_t> > context2;
  vector<vector<size_t> > context3;
  vector<vector<size_t> > context4;
  vector<vector<size_t> > context5;
  vector<vector<size_t> > context6;

  context1.push_back(words);
  context2.push_back(words);
  context3.push_back(words);
  context4.push_back(words);
  context5.push_back(words);
  context6.push_back(words);
  context1.push_back(sufs);
  context2.push_back(sufs);
  context3.push_back(sufs);
  context4.push_back(sufs);
  context5.push_back(sufs);
  context6.push_back(sufs);
  context1.push_back(caps);
  context2.push_back(caps);
  context3.push_back(caps);
  context4.push_back(caps);
  context5.push_back(caps);
  context6.push_back(caps);

  vector<vector<vector<size_t> > > all_context;
  all_context.push_back(context1);
  all_context.push_back(context2);
  all_context.push_back(context3);
  all_context.push_back(context4);
  all_context.push_back(context5);
  //all_context.push_back(context6);

  vector<vector<vector<vector<vector<size_t> > > > > minibatch_all_data;
  contextwin2minibatch(all_context, minibatch_all_data);

  for (size_t i = 0; i < minibatch_all_data[0].size(); ++i) {
    cerr << "minibatch: " << i << endl;
    for (size_t j = 0; j < minibatch_all_data[0][i].size(); ++j) {
      // for all context in this minibatch
      for (size_t k = 0; k < minibatch_all_data[0][i][j].size(); ++k) {
        // for a context
        for (size_t l = 0; l < minibatch_all_data[0][i][j][k].size(); ++l) {
          cerr << minibatch_all_data[0][i][j][k][0] << ",";
        }
        cerr << " ";
      }
      cerr << endl;
    }
  }
}


void
RNNParser::_Impl::test_labels2minibatch() {

  vector<size_t> all_labels;
  all_labels.push_back(0);
  all_labels.push_back(1);
  all_labels.push_back(2);
  all_labels.push_back(3);
  all_labels.push_back(4);
  all_labels.push_back(5);
  all_labels.push_back(6);

  vector<vector<vector<size_t> > > minibatch_all_data;
  labels2minibatch(all_labels, minibatch_all_data);

  for (size_t i = 0; i < minibatch_all_data[0].size(); ++i) {
    cerr << "minibatch: " << i << endl;
    for (size_t j = 0; j < minibatch_all_data[0][i].size(); ++j) {
      cerr << minibatch_all_data[0][i][j] << ",";
    }
    cerr << endl;
  }
}


void
RNNParser::_Impl::contextwin2minibatch(const vector<vector<vector<size_t> > > &sent_all_context,
                                       vector<vector<vector<vector<vector<size_t> > > > > &minibatch_all_data) {

  vector<vector<vector<size_t> > > minibatch;
  vector<vector<vector<vector<size_t> > > > minibatch_sent;

  for (size_t i = 0; i < sent_all_context.size(); ++i) {
    minibatch.push_back(sent_all_context[i]);
    if ((i + 1) % m_bs == 0 || i == sent_all_context.size() - 1) {
      minibatch_sent.push_back(minibatch);
      minibatch.clear();
    }
  }

  minibatch_all_data.push_back(minibatch_sent);
}


void
RNNParser::_Impl::labels2minibatch(const vector<size_t> &sent_all_labels,
                                   vector<vector<vector<size_t> > > &minibatch_all_data) {

  vector<size_t> minibatch;
  vector<vector<size_t> > minibatch_sent;

  for (size_t i = 0; i < sent_all_labels.size(); ++i) {
    minibatch.push_back(sent_all_labels[i]);
    if ((i + 1) % m_bs == 0 || i == sent_all_labels.size() - 1) {
      minibatch_sent.push_back(minibatch);
      minibatch.clear();
    }
  }

  minibatch_all_data.push_back(minibatch_sent);
}


void
RNNParser::_Impl::feasible_y_vals2minibatch(const vector<vector<size_t> > &sent_all_feasible_y_vals,
                                            vector<vector<vector<vector<size_t> > > > &minibatch_all_data) {

  vector<vector<size_t> > minibatch;
  vector<vector<vector<size_t> > > minibatch_sent;

  for (size_t i = 0; i < sent_all_feasible_y_vals.size(); ++i) {
    minibatch.push_back(sent_all_feasible_y_vals[i]);
    if ((i + 1) % m_bs == 0 || i == sent_all_feasible_y_vals.size() - 1) {
      minibatch_sent.push_back(minibatch);
      minibatch.clear();
    }
  }

  minibatch_all_data.push_back(minibatch_sent);
}


void
RNNParser::_Impl::train(const size_t total_epochs, const string &model_path) {

  //  test_contextwin2minibatch();
  //  test_labels2minibatch();
  cerr << "total gold reduce count in training data: " << m_total_reduce_count << endl;

  assert(m_all_sent_context_vec_train.size() ==
      m_all_sent_target_vec_train.size());

  vector<vector<vector<vector<vector<size_t> > > > > all_sent_minibatch_context_vec_train;
  vector<vector<vector<size_t> > > all_sent_minibatch_label_train;
  vector<vector<vector<vector<size_t> > > > all_sent_minibatch_feasible_y_vals_train;

  for (size_t i = 0; i < m_all_sent_context_vec_train.size(); ++i) {

    contextwin2minibatch(m_all_sent_context_vec_train[i],
                         all_sent_minibatch_context_vec_train);

    labels2minibatch(m_all_sent_target_vec_train[i],
                     all_sent_minibatch_label_train);

    feasible_y_vals2minibatch(m_all_sent_feasible_y_vals_train[i],
                              all_sent_minibatch_feasible_y_vals_train);
  }

  assert(all_sent_minibatch_context_vec_train.size() ==
      m_all_sent_context_vec_train.size());
  assert(all_sent_minibatch_context_vec_train.size() ==
      all_sent_minibatch_label_train.size());
  assert(all_sent_minibatch_feasible_y_vals_train.size() ==
      all_sent_minibatch_label_train.size());

  vector<size_t> train_data_inds;
  for (size_t i = 0; i < m_all_sent_context_vec_train.size(); ++i) {
    train_data_inds.push_back(i);
  }

  time_t tstart;
  time_t tend;
  double total_err = 0.0;
  time(&tstart);

  size_t sent = 0;
  for (size_t i = 0; i < total_epochs; ++i) {

    cerr << "bptt training epoch: " << i << endl;

    auto seed = chrono::high_resolution_clock::now().time_since_epoch().count();
    std::mt19937_64 engine(seed);

    if (i > 0)
      shuffle(begin(train_data_inds), end(train_data_inds), engine);

    time(&tstart);
    total_err = 0.0;

    for (size_t s = 0; s < train_data_inds.size(); ++s) {
      sent = train_data_inds[s];
      cerr << "sent: " << sent << endl;

      // reset hidden states
      m_h_tm1.zeros();

      assert(m_all_sent_context_vec_train[sent].size() ==
          m_all_sent_target_vec_train[sent].size());

      assert(m_all_sent_feasible_y_vals_train[sent].size() ==
          m_all_sent_target_vec_train[sent].size());

      // number of minibatches should be equal
      assert(all_sent_minibatch_context_vec_train[sent].size() ==
          all_sent_minibatch_label_train[sent].size());

      assert(all_sent_minibatch_label_train[sent].size() ==
          all_sent_minibatch_feasible_y_vals_train[sent].size());

      for (size_t m = 0; m < all_sent_minibatch_context_vec_train[sent].size(); ++m) {

        // size of each minibatch should be equal
        assert(all_sent_minibatch_context_vec_train[sent][m].size() ==
            all_sent_minibatch_label_train[sent][m].size());

        assert(all_sent_minibatch_context_vec_train[sent][m].size() ==
            all_sent_minibatch_feasible_y_vals_train[sent][m].size());

        //   // grad check
        //        for (size_t i = 0; i < 6; ++i) {
        //          train_bptt_multi_emb_grad_check(all_sent_minibatch_context_vec_train[sent][m],
        //                                          all_sent_minibatch_label_train[sent][m],
        //                                          all_sent_minibatch_feasible_y_vals_train[sent][m], i);
        //        }

        // bptt training
        total_err += train_bptt_multi_emb_dropout(all_sent_minibatch_context_vec_train[sent][m],
                                                  all_sent_minibatch_label_train[sent][m],
                                                  all_sent_minibatch_feasible_y_vals_train[sent][m]);
      }

    }


    time(&tend);
    cerr << "epoch " << i << " total err: " << total_err << endl;
    cerr << ">> epoch time: " << difftime(tend, tstart) << endl;


    m_wx.save(model_path + "/wx." + to_string(i) + ".mat");
    m_wh.save(model_path + "/wh." + to_string(i) + ".mat");
    m_wy.save(model_path + "/wy." + to_string(i) + ".mat");
    m_suf.save(model_path + "/suf." + to_string(i) + ".mat");
    //m_cap.save(model_path + "cap." + to_string(i) + ".mat");
    m_emb.save(model_path + "/emb." + to_string(i) + ".mat");

    if (m_use_supercat_feats)
      m_supercat_emb.save(model_path + "/super_emb." + to_string(i) + ".mat");

    if (cfg.use_phrase_emb_feats()) {
      m_comp_mat.save(model_path + "/comp_mat." + to_string(i) + ".mat");
      m_phrase_mat.save(model_path + "/phrase_mat." + to_string(i) + ".mat");
    }

    dump_supercat_map(model_path + "/supercat_map." + to_string(i));
    // each epoch gets a different supercat_map. This is for future-proof
    // purposes when training with beam-search and seen_rules flag off.
    // Because with seen_rules off, categories seen in later epochs may
    // not have been seen in earlier epochs (some items will get pruned by a beam),
    // thus seen and unseen categories should be strictly separated at test time by using
    // different mo dels for testing after different epochs.
  }

}

void
RNNParser::_Impl::load_supercat_map(const string &file, bool init) {

  cerr << "loading supercat_map\n";

  ifstream in(file.c_str());
  if(!in) {
    cerr << "could not open features file " << file << endl;
    exit(EXIT_FAILURE);
  }

  string cat;
  size_t ind;
  size_t total = 1;
  while (in >> cat >> ind) {
    ++total;
    if (init && (cat.find_last_of("\\") != string::npos || cat.find_last_of("/") != string::npos)) {
      //cerr << cat << endl;
      cat.insert(0, 1, '(');
      cat.append(")");
      //cerr << cat << endl;
    }
    m_cats_map.add(cats.parse(cat), total);
  }

  cerr << "total supercat count (from RNN parser training features): " << m_cats_map.size() << endl;
}


void
RNNParser::_Impl::load_pos_str_ind_map(const string &file) {

  cerr << "loading pos str ind map\n";

  ifstream in(file.c_str());
  if(!in) {
    cerr << "could not open pos_str_ind file " << file << endl;
    exit(EXIT_FAILURE);
  }

  string pos;
  size_t ind;
  size_t total = 1;

  while (in >> pos >> ind) {
    ++total;
    m_pos_str_ind_map.insert(make_pair(pos, total));
  }

  cerr << "total pos tag count (from sec0221): " << m_pos_str_ind_map.size() << endl;
  // pos ind should start from 2, 0 is reserved for empty_pos feature, and 1 for unk_pos
  // (there shouldn't be unk_pos, but this is for keeping pos feats consistent with suffix feats)
}


//void
//RNNParser::_Impl::load_suf_str_ind_map(const string &file) {
//
//  cerr << "loading suffix str ind map\n";
//
//  ifstream in(file.c_str());
//  if(!in) {
//     cerr << "could not open suffix_str_ind file " << file << endl;
//     exit(EXIT_FAILURE);
//  }
//
//  string suf;
//  size_t ind;
//  size_t total = 0;
//
//  while (in >> suf >> ind) {
//    ++total;
//    if (total == 1)
//      assert(ind == 2); // suf ind should start from 2, 0 is reserved for empty_pos feature (pad), 1 for unk
//    m_suf_str_ind_map.insert(make_pair(suf, ind));
//  }
//
//  cerr << "total suffix count (from sec0221): " << total << endl;
//
//}

void
RNNParser::_Impl::load_emb_word_ind_map(const string &filename) {

  cerr << "loading emb word ind...(the same untounced emb file used for training), "
      << filename << endl;

  ifstream in(filename.c_str());

  size_t total = 0;

  if (!in)
    throw runtime_error("no such file: " + filename);
  string line;

  while (in) {
    if (!getline(in, line)) break;
    istringstream iss(line);

    size_t first_space_pos = line.find(" ");
    string word = line.substr(0, first_space_pos);
    string emb_vec = line.substr(first_space_pos + 1); // get till the end

    if (total == 0)
      assert(word == "***PADDING***");
    assert(m_full_wordstr_emb_map.insert(make_pair(word, total)).second);
    ++total;
  }

  cerr << "total emb: " << total << endl;

  // test
  //emb.col(0).print(cerr, "word emb 0th col");
}




void
RNNParser::_Impl::dump_supercat_map(const string &file) {
  size_t total_cats = 0;
  ofstream catmap_output;
  catmap_output.open(file.c_str());
  total_cats = m_cats_map.dump(catmap_output);
  catmap_output.close();
  cerr << "total supercat count: " << total_cats << endl;
}


pair<mat, mat>
RNNParser::_Impl::calc_lh_proj_emb_grad_check(int step,
                                              const vector<vector<size_t> > &context,
                                              mat *x_vals, mat &h_tm1, mat &wx, mat &wh,
                                              mat &emb, mat &suf, mat &cap, mat &super_emb) {
  colvec x;
  colvec x0;
  colvec x1;
  colvec x2;

  assert(context[0].size() == context[1].size());

  const vector<size_t> &word_inds = context[0];
  const vector<size_t> &suf_inds = context[1];

  assert(word_inds.size() == suf_inds.size());

  for (size_t i = 0; i < word_inds.size(); ++i) {
    x0 = join_cols(x0, emb.col(word_inds[i]));
    x1 = join_cols(x1, suf.col(suf_inds[i]));
  }

  if (m_use_supercat_feats) {

    const vector<size_t> &supercat_inds = context[2];
    for (size_t i = 0; i < supercat_inds.size(); ++i) {
      x2 = join_cols(x2, super_emb.col(supercat_inds[i]));
    }

    x = join_cols(join_cols(x0, x1), x2);


  } else {
    x = join_cols(x0, x1);
  }

  x_vals[step] = x;

  mat lh;
  mat one = ones<mat>(m_h_tm1.n_rows, m_h_tm1.n_cols);
  mat lh_deriv;

  if (m_activation == "tanh") {

    try {
      lh = arma::tanh(x.t()*wx + h_tm1*wh);
    } catch (...) {
      cerr << "exception tanh\n";
      exit (EXIT_FAILURE);
    }
    lh_deriv = one - arma::pow(lh, 2);

  } else {

    lh = sigmoid(x.t()*wx + h_tm1*wh);
    //mat one = ones<mat>(lh.n_rows, lh.n_cols);
    lh_deriv = lh % (one - lh);

  }

  pair<mat, mat> res(lh, lh_deriv);
  return res;
}


// grad check
void
RNNParser::_Impl::train_bptt_multi_emb_grad_check(const vector<vector<vector<size_t> > > &context_batch,
                                                  const vector<size_t> &label_batch,
                                                  const vector<vector<size_t> > &feasible_y_vals_batch,
                                                  size_t check) {

  const double eps = 0.0001;

  size_t steps = context_batch.size();
  double err_plus = 0.0;
  double err_minus = 0.0;

  mat lh_vals_plus[steps + 1];
  lh_vals_plus[0] = m_h_tm1;
  mat ly_vals_plus[steps];
  mat x_vals_plus[steps];

  mat lh_vals_minus[steps + 1];
  lh_vals_minus[0] = m_h_tm1;
  mat ly_vals_minus[steps];
  mat x_vals_minus[steps];

  mat all_output_vals_feasible_plus[steps];
  mat all_output_vals_feasible_minus[steps];

  vector<size_t> gold_labels_ind_among_feasible;

  if (check == 0) {

    mat wx_plus = m_wx;
    wx_plus(0,0) += eps;
    mat wx_minus = m_wx;
    wx_minus(0,0) -= eps;

    for (size_t i = 0; i < steps; ++i) {
      pair<mat,  mat> vals_plus =
          calc_lh_proj_emb_grad_check(i, context_batch[i], x_vals_plus,
                                      lh_vals_plus[i], wx_plus, m_wh, m_emb,
                                      m_suf, m_cap, m_supercat_emb);
      mat lh_plus = vals_plus.first;
      ly_vals_plus[i] = lh_plus*m_wy; // no softmax here anymore
      lh_vals_plus[i + 1] = lh_plus;

      pair<mat,  mat> vals_minus =
          calc_lh_proj_emb_grad_check(i, context_batch[i], x_vals_minus,
                                      lh_vals_minus[i], wx_minus, m_wh, m_emb,
                                      m_suf, m_cap, m_supercat_emb);
      mat lh_minus = vals_minus.first;
      ly_vals_minus[i] = lh_minus*m_wy; // no softmax here anymore
      lh_vals_minus[i + 1] = lh_minus;
    }

  } else if (check == 1) {

    mat wh_plus = m_wh;
    wh_plus(0,0) += eps;
    mat wh_minus = m_wh;
    wh_minus(0,0) -= eps;

    for (size_t i = 0; i < steps; ++i) {
      pair<mat,  mat> vals_plus =
          calc_lh_proj_emb_grad_check(i, context_batch[i], x_vals_plus,
                                      lh_vals_plus[i], m_wx, wh_plus, m_emb, m_suf,
                                      m_cap, m_supercat_emb);
      mat lh_plus = vals_plus.first;
      ly_vals_plus[i] = lh_plus*m_wy; // no softmax here anymore
      lh_vals_plus[i + 1] = lh_plus;

      pair<mat,  mat> vals_minus =
          calc_lh_proj_emb_grad_check(i, context_batch[i], x_vals_minus,
                                      lh_vals_minus[i], m_wx, wh_minus, m_emb,
                                      m_suf, m_cap, m_supercat_emb);
      mat lh_minus = vals_minus.first;
      ly_vals_minus[i] = lh_minus*m_wy; // no softmax here anymore
      lh_vals_minus[i + 1] = lh_minus;
    }

  } else if (check == 2) {

    mat wy_plus = m_wy;
    wy_plus(0,0) += eps;
    mat wy_minus = m_wy;
    wy_minus(0,0) -= eps;

    for (size_t i = 0; i < steps; ++i) {
      pair<mat,  mat> vals_plus =
          calc_lh_proj_emb_grad_check(i, context_batch[i], x_vals_plus,
                                      lh_vals_plus[i], m_wx, m_wh,
                                      m_emb, m_suf, m_cap, m_supercat_emb);
      mat lh_plus = vals_plus.first;
      ly_vals_plus[i] = lh_plus*wy_plus; // no softmax here anymore
      lh_vals_plus[i + 1] = lh_plus;

      pair<mat,  mat> vals_minus =
          calc_lh_proj_emb_grad_check(i, context_batch[i], x_vals_minus,
                                      lh_vals_minus[i], m_wx, m_wh, m_emb,
                                      m_suf, m_cap, m_supercat_emb);
      mat lh_minus = vals_minus.first;
      ly_vals_minus[i] = lh_minus*wy_minus; // no softmax here anymore
      lh_vals_minus[i + 1] = lh_minus;
    }

  } else if (check == 3) {

    mat emb_plus = m_emb;
    emb_plus(0,40235) += eps;
    mat emb_minus = m_emb;
    emb_minus(0,40235) -= eps;

    for (size_t i = 0; i < steps; ++i) {
      pair<mat,  mat> vals_plus =
          calc_lh_proj_emb_grad_check(i, context_batch[i], x_vals_plus,
                                      lh_vals_plus[i], m_wx, m_wh, emb_plus,
                                      m_suf, m_cap, m_supercat_emb);
      mat lh_plus = vals_plus.first;
      ly_vals_plus[i] = lh_plus*m_wy; // no softmax here anymore
      lh_vals_plus[i + 1] = lh_plus;

      pair<mat,  mat> vals_minus =
          calc_lh_proj_emb_grad_check(i, context_batch[i], x_vals_minus,
                                      lh_vals_minus[i], m_wx, m_wh, emb_minus,
                                      m_suf, m_cap, m_supercat_emb);
      mat lh_minus = vals_minus.first;
      ly_vals_minus[i] = lh_minus*m_wy; // no softmax here anymore
      lh_vals_minus[i + 1] = lh_minus;
    }

  } else if (check == 4) {

    mat suf_plus = m_suf;
    suf_plus(0,1) += eps;
    mat suf_minus = m_suf;
    suf_minus(0,1) -= eps;

    for (size_t i = 0; i < steps; ++i) {
      pair<mat,  mat> vals_plus =
          calc_lh_proj_emb_grad_check(i, context_batch[i], x_vals_plus,
                                      lh_vals_plus[i], m_wx, m_wh, m_emb,
                                      suf_plus, m_cap, m_supercat_emb);
      mat lh_plus = vals_plus.first;
      ly_vals_plus[i] = lh_plus*m_wy; // no softmax here anymore
      lh_vals_plus[i + 1] = lh_plus;

      pair<mat,  mat> vals_minus =
          calc_lh_proj_emb_grad_check(i, context_batch[i], x_vals_minus,
                                      lh_vals_minus[i], m_wx, m_wh, m_emb,
                                      suf_minus, m_cap, m_supercat_emb);
      mat lh_minus = vals_minus.first;
      ly_vals_minus[i] = lh_minus*m_wy; // no softmax here anymore
      lh_vals_minus[i + 1] = lh_minus;
    }

    //  } else if (check == 5) {
    //
    //    mat cap_plus = m_cap;
    //    cap_plus(0,0) += eps;
    //    mat cap_minus = m_cap;
    //    cap_minus(0,0) -= eps;
    //
    //    for (size_t i = 0; i < steps; ++i) {
    //      pair<mat,  mat> vals_plus =
    //          calc_lh_proj_emb_grad_check(i, context_batch[i], x_vals_plus,
    //              lh_vals_plus[i], m_wx, m_wh, m_emb,
    //              m_suf, cap_plus, m_supercat_emb);
    //      mat lh_plus = vals_plus.first;
    //      ly_vals_plus[i] = lh_plus*m_wy; // no softmax here anymore
    //      lh_vals_plus[i + 1] = lh_plus;
    //
    //      pair<mat,  mat> vals_minus =
    //          calc_lh_proj_emb_grad_check(i, context_batch[i], x_vals_minus,
    //              lh_vals_minus[i], m_wx, m_wh, m_emb,
    //              m_suf, cap_minus, m_supercat_emb);
    //      mat lh_minus = vals_minus.first;
    //      ly_vals_minus[i] = lh_minus*m_wy; // no softmax here anymore
    //      lh_vals_minus[i + 1] = lh_minus;
    //    }
    //
  } else if (check == 5) {

    mat super_emb_plus = m_supercat_emb;
    super_emb_plus(0, 3) += eps;
    mat super_emb_minus = m_supercat_emb;
    super_emb_minus(0, 3) -= eps;

    for (size_t i = 0; i < steps; ++i) {
      pair<mat,  mat> vals_plus =
          calc_lh_proj_emb_grad_check(i, context_batch[i], x_vals_plus,
                                      lh_vals_plus[i], m_wx, m_wh, m_emb,
                                      m_suf, m_cap, super_emb_plus);
      mat lh_plus = vals_plus.first;
      ly_vals_plus[i] = lh_plus*m_wy; // no softmax here anymore
      lh_vals_plus[i + 1] = lh_plus;

      pair<mat,  mat> vals_minus =
          calc_lh_proj_emb_grad_check(i, context_batch[i], x_vals_minus,
                                      lh_vals_minus[i], m_wx, m_wh, m_emb,
                                      m_suf, m_cap, super_emb_minus);
      mat lh_minus = vals_minus.first;
      ly_vals_minus[i] = lh_minus*m_wy; // no softmax here anymore
      lh_vals_minus[i + 1] = lh_minus;
    }

  } else if (check == 6) {



  }

  // dim of ly is 1xn, set the dim of delta_y
  // to nx1 without doing transpose later
  for (size_t i = 0; i < steps; ++i) {

    const vector<size_t> &feasible_y_vals = feasible_y_vals_batch[i];

    mat output_vals_feasible_plus(1, feasible_y_vals.size(), fill::zeros);
    mat output_vals_feasible_minus(1, feasible_y_vals.size(), fill::zeros);

    bool found_gold = false;
    for (size_t j = 0; j < feasible_y_vals.size(); ++j) {
      //cerr << "feasible_y_vals[j]" << feasible_y_vals[j] << endl;
      output_vals_feasible_plus(j) = ly_vals_plus[i](feasible_y_vals[j]);
      output_vals_feasible_minus(j) = ly_vals_minus[i](feasible_y_vals[j]);

      if (feasible_y_vals[j] == label_batch[i]) {
        found_gold = true;
        gold_labels_ind_among_feasible.push_back(j);
      }
    }
    assert(found_gold);
    // softmax over feasible actions
    output_vals_feasible_plus = exp(output_vals_feasible_plus)
        / accu(exp(output_vals_feasible_plus));

    output_vals_feasible_minus = exp(output_vals_feasible_minus)
        / accu(exp(output_vals_feasible_minus));

    all_output_vals_feasible_plus[i] = output_vals_feasible_plus;
    all_output_vals_feasible_minus[i] = output_vals_feasible_minus;

  }
  assert(gold_labels_ind_among_feasible.size() == steps);

  for (size_t i = 0; i < steps; ++i) {
    mat gold_dist(1, feasible_y_vals_batch[i].size(), fill::zeros);
    gold_dist(gold_labels_ind_among_feasible[i]) = 1.0;
    err_plus += x_ent_multi(all_output_vals_feasible_plus[i], gold_dist);
    err_minus += x_ent_multi(all_output_vals_feasible_minus[i], gold_dist);
  }

  double grad = (err_plus - err_minus)/(2.0*eps);
  cerr << "numerical grad: " << grad << endl;

}


// bptt multi bs number of steps, each step corresponds to a contextwin
double
RNNParser::_Impl::train_bptt_multi_emb_dropout(const vector<vector<vector<size_t> > > &context_batch,
                                               const vector<size_t> &label_batch,
                                               const vector<vector<size_t> > &feasible_y_vals_batch) {

  // test
  //  for (size_t i = 0; i < context_batch.size(); ++i) {
  //    for (size_t j = 0; j < context_batch[i].size(); ++j) {
  //      for (size_t k = 0; k < context_batch[i][j].size(); ++k) {
  //        cerr << context_batch[i][j][k] << " ";
  //      }
  //      cerr << endl;
  //    }
  //    cerr << endl;
  //  }

  double err = 0.0;
  size_t steps = context_batch.size();

  mat lh_vals[steps + 1];
  lh_vals[0] = m_h_tm1;
  mat lh_deriv_vals[steps];
  mat ly_vals[steps];
  mat x_vals[steps];
  mat x_mask_vals[steps];

  mat delta_y_vals[steps];
  mat delta_h_vals[steps];
  mat delta_x_vals[steps];

  mat all_output_vals_feasible[steps];
  vector<size_t> gold_labels_ind_among_feasible;

  for (size_t i = 0; i < steps; ++i) {
    pair<mat,  mat> vals =
        calc_lh_proj_emb(i, context_batch[i], x_vals, lh_vals[i], x_mask_vals);

    mat lh = vals.first;
    mat lh_deriv = vals.second;

    ly_vals[i] = lh*m_wy; // no softmax here anymore
    lh_vals[i + 1] = lh;
    lh_deriv_vals[i] = lh_deriv;
  }

  // dim of ly is 1xn, set the dim of delta_y
  // to nx1 without doing transpose later
  for (size_t i = 0; i < steps; ++i) {

    delta_y_vals[i] = zeros<mat>(ly_vals[i].n_cols, 1);

    const vector<size_t> &feasible_y_vals = feasible_y_vals_batch[i];
    mat output_vals_feasible(1, feasible_y_vals.size(), fill::zeros);

    bool found_gold = false;
    for (size_t j = 0; j < feasible_y_vals.size(); ++j) {

      output_vals_feasible(j) = ly_vals[i](feasible_y_vals[j]);

      if (feasible_y_vals[j] == label_batch[i]) {
        found_gold = true;
        gold_labels_ind_among_feasible.push_back(j);
      }
    }
    assert(found_gold);

    // softmax over feasible actions
    //output_vals_feasible = exp(output_vals_feasible) / accu(exp(output_vals_feasible));
    output_vals_feasible = soft_max(output_vals_feasible);
    all_output_vals_feasible[i] = output_vals_feasible;

    // each delta_y_vals have the same number of elements as ly
    // the infeasible units will just get zero values
    for (size_t j = 0; j < feasible_y_vals.size(); ++j) {
      delta_y_vals[i](feasible_y_vals[j]) = output_vals_feasible(j);
    }
  }

  assert(gold_labels_ind_among_feasible.size() == steps);

  for (size_t i = 0; i < steps; ++i) {
    mat gold_dist(1, feasible_y_vals_batch[i].size(), fill::zeros);
    gold_dist(gold_labels_ind_among_feasible[i]) = 1.0;
    err += x_ent_multi(all_output_vals_feasible[i], gold_dist);
    delta_y_vals[i](label_batch[i]) -= 1.0;
  }

  // bptt multi
  delta_h_vals[steps - 1] = lh_deriv_vals[steps - 1].t() % (m_wy*delta_y_vals[steps - 1]);

  for (size_t i = steps; i > 1; --i) {
    delta_h_vals[i - 2] = lh_deriv_vals[i - 2].t() %
        ((m_wh*delta_h_vals[i - 1]) + m_wy*(delta_y_vals[i - 2]));
  }

  //double back_grad_wh = 0.0;
  for (size_t i = 0; i < steps; ++i) {
    mat grad = lh_vals[i].t()*delta_h_vals[i].t();
    m_wh -= m_lr*grad;
    //back_grad_wh += grad(0,0);
  }

  //double back_grad_wy = 0.0;
  for (size_t i = 1; i <= steps; ++i) {
    mat grad = lh_vals[i].t()*delta_y_vals[i - 1].t();
    m_wy -= m_lr*grad;
    //back_grad_wy += grad(0,0);
  }

  for (size_t i = 0; i < steps; ++i) {
    delta_x_vals[i] = (m_wx*delta_h_vals[i]) % x_mask_vals[i];
  }

  //double back_grad_wx = 0.0;
  for (size_t i = 0; i < steps; ++i) {
    mat grad = x_vals[i]*delta_h_vals[i].t();
    m_wx -= m_lr*grad;
    //back_grad_wx += grad(0,0);
  }

  // grad check
  //double bp_grad_emb = 0.0;
  //double bp_grad_suf = 0.0;
  //  double bp_grad_cap = 0.0;
  //double bp_grad_super = 0.0;

  mat grad_comp_mat;
  grad_comp_mat.copy_size(m_comp_mat);
  grad_comp_mat.zeros();

  for (size_t i = 0; i < steps; ++i) {

    const vector<size_t> &word_inds = context_batch[i][0];
    const vector<size_t> &suf_inds = context_batch[i][1];

    assert(word_inds.size() == suf_inds.size());

    // all start inds must be set here
    int start = 0;
    int start2 = m_de*word_inds.size(); // dim of word emb x num of embs
    int start3 = start2 + m_ds*word_inds.size();
    //int start4 = 0;

    for (size_t k = 0; k < word_inds.size(); ++k) {
      m_emb.col(word_inds[k]) -=
          m_lr*delta_x_vals[i].rows(start, start + m_de - 1);
      //if (word_inds[k] == 40235)
      //bp_grad_emb += delta_x_vals[i].rows(start, start + m_de - 1)[0];
      m_suf.col(suf_inds[k]) -=
          m_lr*delta_x_vals[i].rows(start2, start2 + m_ds - 1);
      //if (suf_inds[k] == 1)
      //bp_grad_suf += delta_x_vals[i].rows(start2, start2 + m_ds - 1)[0];
      //      m_cap.col(cap_inds[k]) -=
      //          m_lr*delta_x_vals[i].rows(start3, start3 + m_dc - 1);
      //      if (cap_inds[k]  == 0)
      //        bp_grad_cap += delta_x_vals[i].rows(start3, start3 + m_dc - 1)[0];

      start += m_de;
      start2 += m_ds;
    }

    if (m_use_supercat_feats) {
      assert(context_batch[i].size() >= 3);
      assert(start2 == start3);

      const vector<size_t> &supercat_inds = context_batch[i][2];
      //start4 = start3 + m_dsuper*supercat_inds.size();

      for (size_t k = 0; k < supercat_inds.size(); ++k) {
        m_supercat_emb.col(supercat_inds[k]) -=
            m_lr*delta_x_vals[i].rows(start3, start3 + m_dsuper - 1);
        // if (supercat_inds[k] == 3)
        //bp_grad_super += delta_x_vals[i].rows(start3, start3 + m_dsuper - 1)[0];

        start3 += m_dsuper;
      }
    }

    //    if (cfg.use_phrase_emb_feats()) {
    //      assert(context_batch[i].size() >= 5);
    //      assert(context_batch[i][3].size() == m_phrase_emb_feat_count); // phrase emb from s0, s1, s2, s3
    //      assert(context_batch[i][4].size() == 2*m_phrase_emb_feat_count);
    //      //assert(start4 + m_phrase_emb_feat_count*m_de == delta_x_vals[i].n_rows);
    //      const vector<size_t> &phrase_inds = context_batch[i][3];
    //      const vector<size_t> &child_phrase_inds = context_batch[i][4];
    //
    //      // for each step, there are m_phrase_emb_feat_count number of phrases
    //      // and each should have two child phrases
    //      size_t j = 0;
    //      for (size_t k = 0; k < m_phrase_emb_feat_count; ++k) {
    //
    //        // lex/unary
    //        if (child_phrase_inds[j] == null_child_word_vec_ind) {
    //          assert(child_phrase_inds[j+1] == null_child_word_vec_ind);
    //          j += 2;
    //          //start4 += m_de;
    //          continue;
    //        }
    //
    //        // binary
    //        assert(child_phrase_inds[j+1] != null_child_word_vec_ind);
    //        assert(child_phrase_inds[j] != m_phrase_emb_ind_base); // either < or >
    //        assert(child_phrase_inds[j+1] != m_phrase_emb_ind_base); // either < or >
    //
    //        mat one = ones<mat>(1, m_de);
    //        const size_t real_phrase_ind = (phrase_inds[k] > m_phrase_emb_ind_base) ? (phrase_inds[k] - m_phrase_emb_ind_base) : phrase_inds[k];
    //        mat delta_phrase_output = (m_phrase_mat.row(real_phrase_ind) % (one - m_phrase_mat.row(real_phrase_ind))) // 1x50
    //                                                    % (delta_x_vals[i].rows(start4, start4 + m_de - 1).t()); // 1x50
    //        mat delta_combined_phrase_input = m_comp_mat*(delta_phrase_output.t()); // 100x1
    //
    //        if ((child_phrase_inds[j] > m_phrase_emb_ind_base) && (child_phrase_inds[j+1] > m_phrase_emb_ind_base)) {
    //          grad_comp_mat += join_rows(m_phrase_mat.row(child_phrase_inds[j] - m_phrase_emb_ind_base),
    //                                     m_phrase_mat.row(child_phrase_inds[j+1] - m_phrase_emb_ind_base)).t()
    //                                            *(delta_phrase_output);
    //
    //        } else if ((child_phrase_inds[j] < m_phrase_emb_ind_base) && (child_phrase_inds[j+1] > m_phrase_emb_ind_base)) {
    //          grad_comp_mat += join_cols(m_emb.col(child_phrase_inds[j]), m_phrase_mat.row(child_phrase_inds[j+1] - m_phrase_emb_ind_base).t())
    //                                             *(delta_phrase_output);
    //          m_emb.col(child_phrase_inds[j]) -= m_lr*(delta_combined_phrase_input.rows(0, m_de - 1));
    //
    //        } else if ((child_phrase_inds[j] > m_phrase_emb_ind_base) && (child_phrase_inds[j+1] < m_phrase_emb_ind_base)) {
    //          grad_comp_mat += join_cols(m_phrase_mat.row(child_phrase_inds[j] - m_phrase_emb_ind_base).t(), m_emb.col(child_phrase_inds[j+1]))
    //                                             *(delta_phrase_output);
    //          m_emb.col(child_phrase_inds[j+1]) -= m_lr*(delta_combined_phrase_input.rows(m_de, 2*m_de - 1));
    //
    //        } else {
    //          grad_comp_mat += join_cols(m_emb.col(child_phrase_inds[j]), m_emb.col(child_phrase_inds[j+1]))*(delta_phrase_output);
    //          m_emb.col(child_phrase_inds[j]) -= m_lr*(delta_combined_phrase_input.rows(0, m_de - 1));
    //          m_emb.col(child_phrase_inds[j+1]) -= m_lr*(delta_combined_phrase_input.rows(m_de, 2*m_de - 1));
    //        }
    //
    //        j += 2;
    //        start4 += m_de;
    //      } // end for
    //    } // end if

    //    if (cfg.use_dep_feats()) {
    //
    //      assert(context_batch[i].size() >= 4);
    //      assert(context_batch[i][3].size() == 2);
    //      assert(start4 + 2*m_dc - 1 == delta_x_vals[i].n_rows - 1);
    //
    //      for (size_t k = 0; k < 2; ++k) {
    //        const size_t dist_col = context_batch[i][3][k];
    //
    //        m_dep_dist_mat.col(dist_col) -=
    //            m_lr*delta_x_vals[i].rows(start4, start4 + m_dc - 1);
    //
    //        start4 += m_dc;
    //      }
    //
    //    }

  } // end for all steps

  if (cfg.use_phrase_emb_feats())
    m_comp_mat -= m_lr*grad_comp_mat;

  //grad check
  //  cerr << "backprop grad wx: " << back_grad_wx << endl;
  //  cerr << "backprop grad wh: " << back_grad_wh << endl;
  //  cerr << "backprop grad wy: " << back_grad_wy << endl;
  //
  //  cerr << "backprop grad emb: " << bp_grad_emb << endl;
  //  cerr << "backprop grad suf: " << bp_grad_suf << endl;
  ////  cerr << "backprop grad cap: " << bp_grad_cap << endl;
  //  cerr << "backprop grad super: " << bp_grad_super << endl;

  m_h_tm1 = lh_vals[steps];
  return err;
}


// bptt multi bs number of steps, each step corresponds to a contextwin
double
RNNParser::_Impl::train_bptt_multi_emb(const vector<vector<vector<size_t> > > &context_batch,
                                       const vector<size_t> &label_batch,
                                       const vector<vector<size_t> > &feasible_y_vals_batch) {

  // test
  //  for (size_t i = 0; i < context_batch.size(); ++i) {
  //    for (size_t j = 0; j < context_batch[i].size(); ++j) {
  //      for (size_t k = 0; k < context_batch[i][j].size(); ++k) {
  //        cerr << context_batch[i][j][k] << " ";
  //      }
  //      cerr << endl;
  //    }
  //    cerr << endl;
  //  }

  double err = 0.0;
  size_t steps = context_batch.size();

  mat lh_vals[steps + 1];
  lh_vals[0] = m_h_tm1;
  mat lh_deriv_vals[steps];
  mat ly_vals[steps];
  mat x_vals[steps];

  mat delta_y_vals[steps];
  mat delta_h_vals[steps];
  mat delta_x_vals[steps];

  mat all_output_vals_feasible[steps];
  vector<size_t> gold_labels_ind_among_feasible;

  for (size_t i = 0; i < steps; ++i) {

    pair<mat,  mat> vals =
        calc_lh_proj_emb(i, context_batch[i], x_vals, lh_vals[i], 0);

    mat lh = vals.first;
    mat lh_deriv = vals.second;

    ly_vals[i] = lh*m_wy; // no softmax here anymore
    lh_vals[i + 1] = lh;
    lh_deriv_vals[i] = lh_deriv;

  }

  // dim of ly is 1xn, set the dim of delta_y
  // to nx1 without doing transpose later
  for (size_t i = 0; i < steps; ++i) {

    delta_y_vals[i] = zeros<mat>(ly_vals[i].n_cols, 1);

    const vector<size_t> &feasible_y_vals = feasible_y_vals_batch[i];
    mat output_vals_feasible(1, feasible_y_vals.size(), fill::zeros);

    bool found_gold = false;
    for (size_t j = 0; j < feasible_y_vals.size(); ++j) {

      output_vals_feasible(j) = ly_vals[i](feasible_y_vals[j]);

      if (feasible_y_vals[j] == label_batch[i]) {
        found_gold = true;
        gold_labels_ind_among_feasible.push_back(j);
      }
    }
    assert(found_gold);

    // softmax over feasible actions
    //output_vals_feasible = exp(output_vals_feasible)
     //   / accu(exp(output_vals_feasible));
    output_vals_feasible = soft_max(output_vals_feasible);
    all_output_vals_feasible[i] = output_vals_feasible;

    // each delta_y_vals have the same number of elements as ly
    // the infeasible units will just get zero values
    for (size_t j = 0; j < feasible_y_vals.size(); ++j) {
      delta_y_vals[i](feasible_y_vals[j]) = output_vals_feasible(j);
    }
  }

  assert(gold_labels_ind_among_feasible.size() == steps);

  for (size_t i = 0; i < steps; ++i) {
    mat gold_dist(1, feasible_y_vals_batch[i].size(), fill::zeros);
    gold_dist(gold_labels_ind_among_feasible[i]) = 1.0;
    err += x_ent_multi(all_output_vals_feasible[i], gold_dist);
    delta_y_vals[i](label_batch[i]) -= 1.0;
  }

  // bptt multi
  delta_h_vals[steps - 1] = lh_deriv_vals[steps - 1].t()
      % (m_wy * delta_y_vals[steps - 1]);

  for (size_t i = steps; i > 1; --i) {
    delta_h_vals[i - 2] = lh_deriv_vals[i - 2].t() %
        ((m_wh*delta_h_vals[i - 1]) + m_wy*(delta_y_vals[i - 2]));
  }

  //double back_grad_wh = 0.0;
  for (size_t i = 0; i < steps; ++i) {
    mat grad = lh_vals[i].t()*delta_h_vals[i].t();
    m_wh -= m_lr*grad;
    //back_grad_wh += grad(0,0);
  }

  //double back_grad_wy = 0.0;
  for (size_t i = 1; i <= steps; ++i) {
    mat grad = lh_vals[i].t()*delta_y_vals[i - 1].t();
    m_wy -= m_lr*grad;
    //back_grad_wy += grad(0,0);
  }

  for (size_t i = 0; i < steps; ++i) {
    delta_x_vals[i] = m_wx*delta_h_vals[i];
  }

  //double back_grad_wx = 0.0;
  for (size_t i = 0; i < steps; ++i) {
    mat grad = x_vals[i]*delta_h_vals[i].t();
    m_wx -= m_lr*grad;
    //back_grad_wx += grad(0,0);
  }

  // grad check
  //double bp_grad_emb = 0.0;
  //double bp_grad_suf = 0.0;
  //  double bp_grad_cap = 0.0;
  //double bp_grad_super = 0.0;

  for (size_t i = 0; i < steps; ++i) {

    const vector<size_t> &word_inds = context_batch[i][0];
    const vector<size_t> &suf_inds = context_batch[i][1];

    assert(word_inds.size() == suf_inds.size());

    // all start inds must be set here
    int start = 0;
    int start2 = m_de*word_inds.size(); // dim of word emb x num of embs
    int start3 = start2 + m_ds*word_inds.size();
    //int start4 = 0;


    for (size_t k = 0; k < word_inds.size(); ++k) {
      m_emb.col(word_inds[k]) -=
          m_lr*delta_x_vals[i].rows(start, start + m_de - 1);
      //if (word_inds[k] == 40235)
      //bp_grad_emb += delta_x_vals[i].rows(start, start + m_de - 1)[0];
      m_suf.col(suf_inds[k]) -=
          m_lr*delta_x_vals[i].rows(start2, start2 + m_ds - 1);
      //if (suf_inds[k] == 1)
      //bp_grad_suf += delta_x_vals[i].rows(start2, start2 + m_ds - 1)[0];
      //      m_cap.col(cap_inds[k]) -=
      //          m_lr*delta_x_vals[i].rows(start3, start3 + m_dc - 1);
      //      if (cap_inds[k]  == 0)
      //        bp_grad_cap += delta_x_vals[i].rows(start3, start3 + m_dc - 1)[0];

      start += m_de;
      start2 += m_ds;
    }

    if (m_use_supercat_feats) {

      assert(context_batch[i].size() >= 3);
      assert(start2 == start3);

      const vector<size_t> &supercat_inds = context_batch[i][2];
      //start4 = start3 + m_dsuper*supercat_inds.size();

      for (size_t k = 0; k < supercat_inds.size(); ++k) {
        m_supercat_emb.col(supercat_inds[k]) -=
            m_lr*delta_x_vals[i].rows(start3, start3 + m_dsuper - 1);
        // if (supercat_inds[k] == 3)
        //bp_grad_super += delta_x_vals[i].rows(start3, start3 + m_dsuper - 1)[0];

        start3 += m_dsuper;
      }

    }

    //    if (cfg.use_dep_feats()) {
    //
    //      assert(context_batch[i].size() >= 4);
    //      assert(context_batch[i][3].size() == 2);
    //      assert(start4 + 2*m_dc - 1 == delta_x_vals[i].n_rows - 1);
    //
    //      for (size_t k = 0; k < 2; ++k) {
    //        const size_t dist_col = context_batch[i][3][k];
    //
    //        m_dep_dist_mat.col(dist_col) -=
    //            m_lr*delta_x_vals[i].rows(start4, start4 + m_dc - 1);
    //
    //        start4 += m_dc;
    //      }
    //
    //    }

  }

  //grad check
  //  cerr << "backprop grad wx: " << back_grad_wx << endl;
  //  cerr << "backprop grad wh: " << back_grad_wh << endl;
  //  cerr << "backprop grad wy: " << back_grad_wy << endl;
  //
  //  cerr << "backprop grad emb: " << bp_grad_emb << endl;
  //  cerr << "backprop grad suf: " << bp_grad_suf << endl;
  ////  cerr << "backprop grad cap: " << bp_grad_cap << endl;
  //  cerr << "backprop grad super: " << bp_grad_super << endl;

  m_h_tm1 = lh_vals[steps];
  return err;
}


// max-margin version
mat
RNNParser::_Impl::calc_lh_proj_emb_max_margin(const vector<vector<size_t> > &context,
                                              mat &h_tm1,
                                              colvec &current_hypo_converted_context_vec,
                                              colvec &new_x_mask_val) {
  colvec x;
  colvec x0;
  colvec x1;
  colvec x2;

  assert(context[0].size() == context[1].size());

  const vector<size_t> &word_inds = context[0];
  const vector<size_t> &suf_inds = context[1];

  for (size_t i = 0; i < word_inds.size(); ++i) {
    x0 = join_cols(x0, m_emb.col(word_inds[i]));
    x1 = join_cols(x1, m_suf.col(suf_inds[i]));
  }

  // supercat_feats must be enalbed, it's not optional at the moment #######
  if (m_use_supercat_feats) {
    const vector<size_t> &super_inds = context[2];

    for (size_t i = 0; i < super_inds.size(); ++i) {
      x2 = join_cols(x2, m_supercat_emb.col(super_inds[i]));
    }

    x = join_cols(join_cols(x0, x1), x2);

  } else {

    x = join_cols(x0, x1);

  }

  if (cfg.use_phrase_emb_feats()) {
    assert(m_phrase_emb_feat_count == context[3].size());
    for (size_t i = 0; i < m_phrase_emb_feat_count; ++i) {
      assert(context[3][i] != m_phrase_emb_ind_base);
      if (context[3][i] > m_phrase_emb_ind_base) {
        x = join_cols(x, m_phrase_mat.row(context[3][i] - m_phrase_emb_ind_base).t());
      } else {
        x = join_cols(x, m_emb.col(context[3][i]));
      }
    }
  }

  //  if (cfg.use_dep_feats()) {
  //    size_t dep_dist_ind = context[3][0];
  //    x = join_cols(x, m_dep_dist_mat.col(dep_dist_ind));
  //    dep_dist_ind = context[3][1];
  //    x = join_cols(x, m_dep_dist_mat.col(dep_dist_ind));
  //
  //  }

  if (m_dropout) {
    // for dropout
    auto seed = chrono::high_resolution_clock::now().time_since_epoch().count();
    std::default_random_engine engine(seed);
    std::bernoulli_distribution dist(m_dropout_success_prob);
    colvec mask(x.n_rows, x.n_cols);
    mask.imbue( [&]() { return dist(engine); } );
    x = x % mask;
    new_x_mask_val = mask;
  }

  current_hypo_converted_context_vec = x;

  mat lh;
  mat one = ones<mat>(m_h_tm1.n_rows, m_h_tm1.n_cols);
  mat lh_deriv;

  if (m_activation == "tanh") {

    try {
      lh = arma::tanh(x.t()*m_wx + h_tm1*m_wh);
    } catch (...) {
      cerr << "tanh exception\n";
    }
    //lh_deriv = one - arma::pow(lh, 2);

  } else {

    lh = sigmoid(x.t()*m_wx + h_tm1*m_wh);
    //mat one = ones<mat>(lh.n_rows, lh.n_cols);
    //lh_deriv = lh % (one - lh);

  }

  //pair<mat, mat> res(lh, lh_deriv);
  return lh;
}



// non max-margin version
pair<mat, mat>
RNNParser::_Impl::calc_lh_proj_emb(int step,
                                   const vector<vector<size_t> > &context,
                                   mat *x_vals, mat &h_tm1, mat *x_mask_vals) {
  mat x;
  colvec x0;
  colvec x1;
  colvec x2;

  assert(context[0].size() == context[1].size());

  const vector<size_t> &word_inds = context[0];
  const vector<size_t> &suf_inds = context[1];

  for (size_t i = 0; i < word_inds.size(); ++i) {
    x0 = join_cols(x0, m_emb.col(word_inds[i]));
    x1 = join_cols(x1, m_suf.col(suf_inds[i]));
  }

  // supercat_feats must be enalbed, it's not optional at the moment #######
  if (m_use_supercat_feats) {
    const vector<size_t> &super_inds = context[2];

    for (size_t i = 0; i < super_inds.size(); ++i) {
      x2 = join_cols(x2, m_supercat_emb.col(super_inds[i]));
    }

    x = join_cols(join_cols(x0, x1), x2);

  } else {

    x = join_cols(x0, x1);

  }

  if (cfg.use_phrase_emb_feats()) {
    assert(m_phrase_emb_feat_count == context[3].size());
    for (size_t i = 0; i < m_phrase_emb_feat_count; ++i) {
      assert(context[3][i] != m_phrase_emb_ind_base);
      if (context[3][i] > m_phrase_emb_ind_base) {
        x = join_cols(x, m_phrase_mat.row(context[3][i] - m_phrase_emb_ind_base).t());
      } else {
        x = join_cols(x, m_emb.col(context[3][i]));
      }
    }
  }

  //  if (cfg.use_dep_feats()) {
  //    size_t dep_dist_ind = context[3][0];
  //    x = join_cols(x, m_dep_dist_mat.col(dep_dist_ind));
  //    dep_dist_ind = context[3][1];
  //    x = join_cols(x, m_dep_dist_mat.col(dep_dist_ind));
  //
  //  }

  if (m_dropout) {
    // for dropout
    auto seed = chrono::high_resolution_clock::now().time_since_epoch().count();
    std::default_random_engine engine(seed);
    std::bernoulli_distribution dist(m_dropout_success_prob);
    colvec mask(x.n_rows, x.n_cols);
    mask.imbue( [&]() { return dist(engine); } );
    x_mask_vals[step] = mask;
    x = x % mask;
  }

  x_vals[step] = x;

  mat lh;
  mat one = ones<mat>(m_h_tm1.n_rows, m_h_tm1.n_cols);
  mat lh_deriv;

  if (m_activation == "tanh") {

    try {
      lh = arma::tanh(x.t()*m_wx + h_tm1*m_wh);
    } catch (...) {
      cerr << "tanh exception\n";
    }
    lh_deriv = one - arma::pow(lh, 2);

  } else {

    lh = sigmoid(x.t()*m_wx + h_tm1*m_wh);
    //mat one = ones<mat>(lh.n_rows, lh.n_cols);
    lh_deriv = lh % (one - lh);

  }

  pair<mat, mat> res(lh, lh_deriv);
  return res;
}


// non max-margin version
void
RNNParser::_Impl::calc_lh_proj_emb_test(const vector<vector<size_t> > &context,
                                        const mat &h_tm1,
                                        mat &new_hidden_states) {
  colvec x;
  colvec x0;
  colvec x1;
  colvec x2;

  assert(context[0].size() == context[1].size());

  const vector<size_t> &word_inds = context[0];
  const vector<size_t> &suf_inds = context[1];

  for (size_t i = 0; i < word_inds.size(); ++i) {
    // NOT NEEDED ANYMORE   if (word_inds[i] >= 1000000) {
    //      // this is for parsing stuff other than CCGBank,
    //      // a hack that needs to done more properly later
    //      x0 = join_cols(x0, m_emb_all.col(word_inds[i] - 1000000));
    //    } else {
    x0 = join_cols(x0, m_emb.col(word_inds[i]));
    x1 = join_cols(x1, m_suf.col(suf_inds[i]));
  }

  if (m_use_supercat_feats) {
    const vector<size_t> &super_inds = context[2];

    for (size_t i = 0; i < super_inds.size(); ++i) {
      x2 = join_cols(x2, m_supercat_emb.col(super_inds[i]));
    }

    //mat temp = join_cols(join_cols(x0, x1), x2);
    //cerr << "size:" << temp.n_rows;
    //cerr << "x3 size: " << x3.n_rows;
    x = join_cols(join_cols(x0, x1), x2);

  } else {

    x = join_cols(x0, x1);

  }

  if (cfg.use_phrase_emb_feats()) {
    assert(m_phrase_emb_feat_count == context[3].size());
    for (size_t i = 0; i < m_phrase_emb_feat_count; ++i) {
      assert(context[3][i] != m_phrase_emb_ind_base); // either > or <
      if (context[3][i] > m_phrase_emb_ind_base) {
        x = join_cols(x, m_phrase_mat.row(context[3][i] - m_phrase_emb_ind_base).t());
      } else {
        x = join_cols(x, m_emb.col(context[3][i]));
      }
    }
  }

  //  if (cfg.use_dep_feats()) {
  //    size_t dep_dist_ind = context[3][0];
  //    x = join_cols(x, m_dep_dist_mat.col(dep_dist_ind));
  //    dep_dist_ind = context[3][1];
  //    x = join_cols(x, m_dep_dist_mat.col(dep_dist_ind));
  //  }

  if (m_dropout)
    x = x % m_dropout_vec;

  if (m_activation == "tanh") {
    try {
      new_hidden_states = arma::tanh(x.t()*m_wx + h_tm1*m_wh);
    } catch (...) {
      cerr << "tanh exception\n";
      exit (EXIT_FAILURE);
    }
  } else {
    new_hidden_states = sigmoid(x.t()*m_wx + h_tm1*m_wh);
  }

}


// non max-margin version
void
RNNParser::_Impl::calc_lh_proj_emb_xf1(const vector<vector<size_t> > &context,
                                       const mat &h_tm1,
                                       mat &new_hidden_states,
                                       colvec &converted_context_vec) {
  mat x;
  colvec x0;
  colvec x1;
  colvec x2;

  assert(context[0].size() == context[1].size());

  const vector<size_t> &word_inds = context[0];
  const vector<size_t> &suf_inds = context[1];

  for (size_t i = 0; i < word_inds.size(); ++i) {
    x0 = join_cols(x0, m_emb.col(word_inds[i]));
    x1 = join_cols(x1, m_suf.col(suf_inds[i]));
  }

  if (m_use_supercat_feats) {
    const vector<size_t> &super_inds = context[2];

    for (size_t i = 0; i < super_inds.size(); ++i) {
      x2 = join_cols(x2, m_supercat_emb.col(super_inds[i]));
    }

    x = join_cols(join_cols(x0, x1), x2);

  } else {

    x = join_cols(x0, x1);

  }

  if (cfg.use_phrase_emb_feats()) {
    assert(m_phrase_emb_feat_count == context[3].size());
    for (size_t i = 0; i < m_phrase_emb_feat_count; ++i) {
      assert(context[3][i] != m_phrase_emb_ind_base); // either > or <
      if (context[3][i] > m_phrase_emb_ind_base) {
        x = join_cols(x, m_phrase_mat.row(context[3][i] - m_phrase_emb_ind_base).t());
      } else {
        x = join_cols(x, m_emb.col(context[3][i]));
      }
    }
  }

  if (m_dropout)
    x = x % m_dropout_vec;

  if (m_activation == "tanh") {
    try {
      new_hidden_states = arma::tanh(x.t()*m_wx + h_tm1*m_wh);
    } catch (...) {
      cerr << "tanh exception\n";
      exit (EXIT_FAILURE);
    }
  } else {
    new_hidden_states = sigmoid(x.t()*m_wx + h_tm1*m_wh);
  }

  converted_context_vec = x;
}


void
RNNParser::_Impl::calc_lh_proj_emb_xf1_cached(const vector<vector<size_t> > &context,
                                              const mat &h_tm1,
                                              mat &new_hidden_states,
                                              colvec &converted_context_vec,
                                              const bool train_xf1) {
  mat x;
  colvec x0;
  colvec x1;
  colvec x2;

  assert(context[0].size() == context[1].size());

  const vector<size_t> &word_inds = context[0];
  const vector<size_t> &suf_inds = context[1];

  //  for (size_t i = 0; i < word_inds.size(); ++i) {
  //    x0 = join_cols(x0, m_emb.col(word_inds[i]));
  //    x1 = join_cols(x1, m_suf.col(suf_inds[i]));
  //  }

  mat lh_val(m_h_tm1.n_rows, m_h_tm1.n_cols, fill::zeros);
  for (size_t i = 0; i < word_inds.size(); ++i) {

    if (train_xf1) {
      x0 = join_cols(x0, m_emb.col(word_inds[i]));
      x1 = join_cols(x1, m_suf.col(suf_inds[i]));
    }

    auto it = m_word_emb_lh_cache_val.find(i);
    assert(it != m_word_emb_lh_cache_val.end());
    auto emb_ind_it = it->second.find(word_inds[i]);
    if (emb_ind_it != it->second.end()) {
      //cerr << "using cache\n";
      lh_val = lh_val + emb_ind_it->second;
    } else {
      cerr << "fallback w\n";
      size_t off_set = i*m_de; // word_pos * m_de
      mat m_wx_sub = m_wx.submat(off_set, 0, off_set + (m_de - 1), m_wx.n_cols - 1);
      lh_val += (m_emb.col(word_inds[i]).t()*m_dropout_success_prob)*m_wx_sub;
    }

    auto it2 = m_suf_emb_lh_cache_val.find(i);
    assert(it2 != m_suf_emb_lh_cache_val.end());
    auto pos_ind_it = it2->second.find(suf_inds[i]);
    if (pos_ind_it != it2->second.end()) {
      lh_val = lh_val + pos_ind_it->second;
    } else {
      cerr << "fallback p\n";
      size_t off_set = m_de*m_feature_count;
      size_t off_set2 = i*m_ds;
      mat m_wx_sub = m_wx.submat(off_set+off_set2, 0, off_set+off_set2+(m_ds-1), m_wx.n_cols - 1);
      lh_val += (m_suf.col(suf_inds[i]).t()*m_dropout_success_prob)*m_wx_sub;
    }
  }

  if (m_use_supercat_feats) {
    const vector<size_t> &super_inds = context[2];

    for (size_t i = 0; i < super_inds.size(); ++i) {
      x2 = join_cols(x2, m_supercat_emb.col(super_inds[i]));
    }

    if (train_xf1) {
      //mat temp = join_cols(join_cols(x0, x1), x2);
      //cerr << "size:" << temp.n_rows;
      //cerr << "x3 size: " << x3.n_rows;
      x = join_cols(join_cols(x0, x1), x2);
    }

  } else {
    // always uses supercat feats
    //    x = join_cols(x0, x1);

  }

  if (cfg.use_phrase_emb_feats()) {
    assert(m_phrase_emb_feat_count == context[3].size());
    for (size_t i = 0; i < m_phrase_emb_feat_count; ++i) {
      assert(context[3][i] != m_phrase_emb_ind_base); // either > or <
      if (context[3][i] > m_phrase_emb_ind_base) {
        x = join_cols(x, m_phrase_mat.row(context[3][i] - m_phrase_emb_ind_base).t());
      } else {
        x = join_cols(x, m_emb.col(context[3][i]));
      }
    }
  }

  //size_t off_set = (m_de + m_ds)*m_feature_count;
  // this needs changing if more features are added later
  //mat wx_super_sub = m_wx.submat(off_set, 0, m_wx.n_rows - 1, m_wx.n_cols - 1);
  lh_val += (x2.t()*m_dropout_success_prob)*m_wx_super_sub;

  if (train_xf1) {
    if (m_dropout)
      x = x % m_dropout_vec;
  }
  //
  //  if (m_activation == "tanh") {
  //    try {
  //      new_hidden_states = arma::tanh(x.t()*m_wx + h_tm1*m_wh);
  //    } catch (...) {
  //      cerr << "tanh exception\n";
  //      exit (EXIT_FAILURE);
  //    }
  //  } else {
  //new_hidden_states = sigmoid(x.t()*m_wx + h_tm1*m_wh);
  new_hidden_states = sigmoid(lh_val + h_tm1*m_wh);
  // }
  if (train_xf1) {
    converted_context_vec = x;
  }
}


mat
RNNParser::_Impl::sigmoid(const mat &m) {
  mat one = ones<mat>(m.n_rows, m.n_cols);
  return one/(one + exp(-m));
}


mat
RNNParser::_Impl::soft_max(const mat &y) {
//  mat res = exp(y)/accu(exp(y));
//  return res;
  mat dev = y - y.max();
  return exp(dev)/accu(exp(dev));
}


double
RNNParser::_Impl::x_ent_multi(const mat&y, const mat&t) {
  return -accu(t % log(y));
}


void
RNNParser::_Impl::sr_parse_train(const double BETA, const size_t sent_id) {
  try {
    // training related
    //    const size_t unary_base_count = 579;
    //    const size_t reduce_base_count = 597;
    const ShiftReduceHypothesis *best_hypo; // the highest-scored hypo on each level
    const ShiftReduceHypothesis *gold_hypo;
    vector<ShiftReduceAction*> &gold_tree_actions = *m_goldTreeVec.at(sent_id);
    vector<pair<size_t, size_t> > &gold_tree_actions_rnn_fmt = m_gold_tree_vec_rnn[sent_id];
    size_t gold_tree_size = gold_tree_actions.size();
    assert(gold_tree_actions.size() == gold_tree_actions_rnn_fmt.size());

    ShiftReduceAction *gold_action = 0;
    pair<size_t, size_t> gold_action_rnn_fmt;
    size_t gold_action_id = 0;
    bool gold_finished = false;
    size_t total_shift_count = 0;

    if(sent.words.size() > cfg.maxwords()) {
      throw NLP::ParseError("sentence length exceeds maximum number of words for parser", nsentences);
      exit (EXIT_FAILURE);
    }

    const size_t NWORDS = sent.words.size();
    //assert(NWORDS == sent_rnn_fmt.words.size());

    //ShiftReduceHypothesis *candidate_output = 0;
    //    Words words;
    //    raws2words(sent.words, words);
    //    Words tags;
    //    raws2words(sent.pos, tags);

    // inds from the input sentence
    vector<size_t> word_inds_vec;
    vector<size_t> suf_inds_vec;

    words2word_inds(sent.words, sent.pos, word_inds_vec, suf_inds_vec);
    //    if (cfg.use_pos())
    //      pos2pos_inds(sent_rnn_fmt.pos, suf_inds_vec);
    //    else
    //      suf2suf_inds(sent_rnn_fmt.pos, suf_inds_vec);
    assert(word_inds_vec.size() == suf_inds_vec.size());

    nsentences++;
    SuperCat::nsupercats = 0;

    //const MultiRaws &input = sent.msuper;

    m_lattice = new ShiftReduceLattice(NWORDS, m_beamSize, MAX_UNARY_ACTIONS);
    ShiftReduceHypothesis *start_hypo = new ShiftReduceHypothesis(0, 0, 0, 0, 0, 0, 0.0, false, 0, 0, 0, 0, 0.0, 0);
    start_hypo->SetStartHypoFlag();
    m_lattice->Add(0, start_hypo);
    gold_hypo = m_lattice->GetLatticeArray(0); // gold == start
    size_t startIndex;

    //cerr << "sent: " << sent_id << endl;

    // all context, target, and feasible target values
    // for this sentence
    vector<vector<vector<size_t> > > x_vals_sent;
    vector<size_t> y_vals_sent;
    vector<vector<size_t> > feasible_y_vals_sent;

    // inds from the current context
    vector<size_t> word_inds_ret;
    vector<size_t> suf_inds_ret;
    vector<size_t> supercat_inds_ret;
    vector<size_t> phrase_emb_ind_ret;
    vector<size_t> child_phrase_emb_ind_ret;

    //vector<size_t> action_inds_ret;

    // set start and end indexes of the lattice
    while (m_lattice->GetEndIndex() == 0 ||
        m_lattice->GetEndIndex() > m_lattice->GetPrevEndIndex()) {

      if (m_lattice->GetEndIndex() != 0)
        startIndex = m_lattice->GetPrevEndIndex() + 1;
      else
        startIndex = 0;
      m_lattice->SetPrevEndIndex(m_lattice->GetEndIndex());

      if (gold_action_id < gold_tree_size) {
        gold_action = gold_tree_actions[gold_action_id];
        assert(gold_action);
        //cerr << "gold: " << gold_action->GetAction() << " "
        //<< gold_action->GetCat()->out_novar_noX_noNB_SR_str() << endl;
        gold_action_rnn_fmt = gold_tree_actions_rnn_fmt[gold_action_id];
        assert(gold_action_rnn_fmt.first == gold_action->GetAction());
      }

      HypothesisPQueue hypoQueue;

      for (size_t i = startIndex; i <= m_lattice->GetEndIndex(); ++i) {
        assert(m_lattice->GetEndIndex() - startIndex == 0);
        const ShiftReduceHypothesis *hypo = m_lattice->GetLatticeArray(i);
        size_t j = hypo->GetNextInputIndex();

        //        if (hypo->IsFinished(NWORDS, m_allowFragTree))
        //        {
        //          if (candidateOutput == 0 || hypo->GetTotalScore() > candidateOutput->GetTotalScore())
        //            candidateOutput = hypo;
        //        }

        vector<vector<size_t> > context_vec;
        word_inds_ret.clear();
        suf_inds_ret.clear();
        supercat_inds_ret.clear();
        phrase_emb_ind_ret.clear();
        child_phrase_emb_ind_ret.clear();

        ContextLoader context_loader(m_cat_ind, m_head_limit, true);
        context_loader.load_context(hypo, word_inds_ret,
                                    suf_inds_ret, supercat_inds_ret,
                                    phrase_emb_ind_ret, child_phrase_emb_ind_ret,
                                    word_inds_vec, suf_inds_vec,
                                    m_cats_map, cats, cfg.use_phrase_emb_feats(), true);

        assert(word_inds_ret.size() == suf_inds_ret.size());
        assert(word_inds_ret.size() == m_feature_count);
        if (m_use_supercat_feats)
          assert(supercat_inds_ret.size() == m_supercat_feature_count);

        context_vec.push_back(word_inds_ret);
        context_vec.push_back(suf_inds_ret);
        context_vec.push_back(supercat_inds_ret);
        context_vec.push_back(phrase_emb_ind_ret);
        context_vec.push_back(child_phrase_emb_ind_ret);

        x_vals_sent.push_back(context_vec);

        size_t gold_action_class = 0;

        // feasible target values for the current context
        // the map is used to assert all the feasible target
        // values for the current context are unique
        vector<size_t> feasible_y_vals;
        unordered_map<size_t, size_t> feasible_y_vals_map;

        // find all feasible actions,
        // but do not push them into the queue
        // shift
        if (j < NWORDS) {
          const MultiRaw &multi = sent.msuper[j];
          for (MultiRaw::const_iterator k = multi.begin(); k!= multi.end(); ++k) {
            const Cat *cat = cats.markedup[k->raw];

            if(!cat) {
              cerr << "SHIFT action error: attempted to load category without markedup "
                  << k->raw << endl;
              exit (EXIT_FAILURE);
            }

            assert(m_lex_cat_ind_map.find(k->raw) != m_lex_cat_ind_map.end());
            feasible_y_vals.push_back(m_lex_cat_ind_map[k->raw]);
            //cerr << "cat id: " << m_lex_cat_ind_map[k->raw] << endl;
            assert(feasible_y_vals_map.insert(make_pair(m_lex_cat_ind_map[k->raw], 0)).second);
          } // end for
        } // end if

        // unary
        if (hypo->GetStackTopSuperCat() != 0) {
          const SuperCat *sc = hypo->GetStackTopSuperCat();
          vector<SuperCat *> tmp;
          tmp.clear();

          if (!sc->IsLex() && !sc->IsTr()) {
            unary_tc(hypo->GetStackTopSuperCat(), false, tmp);
            unary_tr(sc, tmp);
          } else if (sc->IsLex() && !sc->IsTr()) {
            unary_tr(sc, tmp);
          } else if (!sc->IsLex() && sc->IsTr()) { /* do nothing */ }
          else if (sc->IsLex() && sc->IsTr()) { /* do nothing */ }

          for (vector<SuperCat *>::iterator j = tmp.begin(); j < tmp.end(); ++j) {
            SuperCat *stack_top_cat = *j;
            assert(stack_top_cat->left);
            feasible_y_vals.push_back(stack_top_cat->m_unary_id + m_unary_base_count);
            assert(feasible_y_vals_map.insert(make_pair(stack_top_cat->m_unary_id + m_unary_base_count, 0)).second);
          }
        }

        // reduce
        if (hypo->GetPrvStack() != 0 && hypo->GetPrvStack()->GetStackTopSuperCat() != 0
            && hypo->GetStackTopSuperCat() != 0) {
          results.clear();
          if (!cfg.seen_rules() ||
              rule_instances(hypo->GetPrvStack()->GetStackTopSuperCat()->GetCat(), hypo->GetStackTopSuperCat()->GetCat()))
            rules(hypo->GetPrvStack()->GetStackTopSuperCat(), hypo->GetStackTopSuperCat(),
                  cfg.eisner_nf(), cfg.seen_rules(), cfg.question_rules(), results);

          if (results.size() > 0) {
            for (vector<SuperCat *>::iterator k = results.begin(); k < results.end(); ++k) {
              SuperCat *stack_top_cat = *k;
              //cerr << "reduce_id: " << stack_top_cat->m_reduce_id << endl;
              if (stack_top_cat->m_reduce_id == 0) {
                feasible_y_vals.push_back(m_reduce_base_count + 1);
                assert(feasible_y_vals_map.insert(make_pair(m_reduce_base_count + 1, 0)).second);
              } else {
                feasible_y_vals.push_back(m_reduce_base_count + stack_top_cat->m_reduce_id + 1);
                assert(feasible_y_vals_map.insert(make_pair(m_reduce_base_count + stack_top_cat->m_reduce_id + 1, 0)).second);
              }
            }
          }
        }

        bool found_gold_action = false;

        // do the gold action
        // shift
        if (gold_action_rnn_fmt.first == SHIFT) {
          assert(j < NWORDS);

          const Cat *cat = cats.markedup[m_lex_ind_cat_map[gold_action_rnn_fmt.second]];
          assert(cat);
          SuperCat *stackTopCat = SuperCat::Lexical(chart.pool, j+1, cat, 0);
          assert(stackTopCat->cat);
          stackTopCat->m_word_vec_ind = word_inds_vec[j];
          ShiftReduceAction *actionShift = new ShiftReduceAction(SHIFT, stackTopCat);
          HypothesisTuple *newHypoTuple = new HypothesisTuple(hypo, actionShift, 0.0, i);
          hypoQueue.push(newHypoTuple);

          found_gold_action = true;
          gold_action_class = gold_action_rnn_fmt.second;
          ++total_shift_count;

          // unary
        } else if (gold_action_rnn_fmt.first == UNARY) {
          const SuperCat *sc = hypo->GetStackTopSuperCat();
          vector<SuperCat *> tmp;
          tmp.clear();
          if (!sc->IsLex() && !sc->IsTr())
          {
            unary_tc(hypo->GetStackTopSuperCat(), false, tmp);
            unary_tr(sc, tmp);
          }
          else if (sc->IsLex() && !sc->IsTr())
          {
            unary_tr(sc, tmp);
          }
          else if (!sc->IsLex() && sc->IsTr()) { /* do nothing */ }
          else if (sc->IsLex() && sc->IsTr()) { /* do nothing */ }

          for (vector<SuperCat *>::iterator j = tmp.begin(); j < tmp.end(); ++j)
          {
            SuperCat *stack_top_cat = *j;
            assert(stack_top_cat->left);
            if (stack_top_cat->m_unary_id == gold_action_rnn_fmt.second) {
              found_gold_action = true;
              gold_action_class = gold_action_rnn_fmt.second + m_unary_base_count;
              //stack_top_cat->m_word_vec = hypo->GetStackTopSuperCat()->m_word_vec;
              stack_top_cat->m_word_vec_ind = hypo->GetStackTopSuperCat()->m_word_vec_ind;
              ShiftReduceAction *actionUnary = new ShiftReduceAction(UNARY, stack_top_cat);
              HypothesisTuple *newHypoTuple = new HypothesisTuple(hypo, actionUnary, 0.0, i);
              hypoQueue.push(newHypoTuple);
            }
          }

          // reduce
        } else if (gold_action_rnn_fmt.first == COMBINE) {

          assert(hypo->GetPrvStack() != 0 && hypo->GetPrvStack()->GetStackTopSuperCat() != 0
                 && hypo->GetStackTopSuperCat() != 0);
          results.clear();
          if(!cfg.seen_rules() ||
              rule_instances(hypo->GetPrvStack()->GetStackTopSuperCat()->cat, hypo->GetStackTopSuperCat()->cat))
          {
            rules(hypo->GetPrvStack()->GetStackTopSuperCat(), hypo->GetStackTopSuperCat(),
                  cfg.eisner_nf(), cfg.seen_rules(), cfg.question_rules(), results);
          }

          assert (results.size() > 0);
          for (vector<SuperCat *>::iterator k = results.begin(); k < results.end(); ++k) {
            SuperCat *stack_top_cat = *k;
            if (eqSR(stack_top_cat->GetCat(), gold_action->GetCat())) {
              found_gold_action = true;
              ++m_total_reduce_count;
              compose_word_vec(hypo->GetPrvStack()->GetStackTopSuperCat()->m_word_vec_ind,
                               hypo->GetStackTopSuperCat()->m_word_vec_ind, stack_top_cat);
              ShiftReduceAction *actionCombine = new ShiftReduceAction(COMBINE, stack_top_cat);
              HypothesisTuple *newHypoTuple = new HypothesisTuple(hypo, actionCombine, 0.0, i);
              hypoQueue.push(newHypoTuple);

              if (stack_top_cat->m_reduce_id == 0) {
                gold_action_class = m_reduce_base_count + 1;
              } else {
                gold_action_class = m_reduce_base_count + stack_top_cat->m_reduce_id + 1;
              }
            }
          }
        } else {
          cerr << "gold action not recognized...\n";
          exit (EXIT_FAILURE);
        }

        assert(found_gold_action);
        assert(feasible_y_vals_map.find(gold_action_class)
               != feasible_y_vals_map.end());

        y_vals_sent.push_back(gold_action_class);
        feasible_y_vals_sent.push_back(feasible_y_vals);

      } // end for

      best_hypo = 0;
      construct_hypo(hypoQueue, m_lattice, best_hypo, gold_hypo, gold_action, gold_finished);
      assert(m_hasGoldAction);

      if (gold_action_id < gold_tree_size)
        ++gold_action_id;

      if (gold_action_id == gold_tree_size) {
        assert(total_shift_count == NWORDS);
        gold_finished = true;
        cerr << "sent: " << sent_id << " ### output correct ###" << endl;

        assert(x_vals_sent.size() == y_vals_sent.size());
        assert(feasible_y_vals_sent.size() == y_vals_sent.size());

        m_all_sent_context_vec_train.push_back(x_vals_sent);
        m_all_sent_target_vec_train.push_back(y_vals_sent);
        m_all_sent_feasible_y_vals_train.push_back(feasible_y_vals_sent);

        sent.reset();
        //sent_rnn_fmt.reset();
        delete m_lattice;

        return;
      }

    } // end while

    sent.reset();
    //sent_rnn_fmt.reset();
    delete m_lattice;

    return;
  }
  catch(NLP::Exception e)
  {
    //throw NLP::ParseError(e.msg, nsentences);
    cerr << e.msg << endl;
    return;
  }
}


void
RNNParser::_Impl::compose_word_vec(const int left_ind, const int right_ind,
                                   SuperCat *&supercat) {
  rowvec left;
  rowvec right;
  assert(left_ind != (int) m_phrase_emb_ind_base); // either > or <
  assert(right_ind != (int) m_phrase_emb_ind_base); // either > or <
  assert(supercat->left->m_word_vec_ind == left_ind);
  assert(supercat->right->m_word_vec_ind == right_ind);

  if (left_ind > (int) m_phrase_emb_ind_base) {
    assert(left_ind - m_phrase_emb_ind_base >= 1);
    left = m_phrase_mat.row(left_ind - m_phrase_emb_ind_base);
  } else {
    left = m_emb.col(left_ind).t();
  }

  if (right_ind > (int) m_phrase_emb_ind_base) {
    assert(right_ind - m_phrase_emb_ind_base >= 1);
    right = m_phrase_mat.row(right_ind - m_phrase_emb_ind_base);
  } else {
    right = m_emb.col(right_ind).t();
  }

  m_phrase_mat.row(m_total_reduce_count) = sigmoid(join_rows(left, right)*m_comp_mat);
  supercat->m_word_vec_ind = m_total_reduce_count + m_phrase_emb_ind_base;
}


bool RNNParser::_Impl::ignore_dep(const CCG::Filled *&filled) {

  if (m_deps_ignore_map_rule.find((ulong)filled->rule)
      != m_deps_ignore_map_rule.end())
    return true;

  int marked_up = cats.markedup.index((string) cats.relations[filled->rel].cat);
  assert(marked_up != -1);
  if (m_deps_ignore_map_1.find(make_tuple((ulong)marked_up,
                                          cats.relations[filled->rel].jslot,
                                          (ulong)filled->rule)) != m_deps_ignore_map_1.end())
    return true;

  if (m_deps_ignore_map_2.find(make_tuple(sent.words[filled->head - 1],
                                          (ulong)marked_up, cats.relations[filled->rel].jslot,
                                          (ulong)filled->rule)) != m_deps_ignore_map_2.end())
    return true;

  if (m_deps_ignore_map_3.find(make_tuple(sent.words[filled->head - 1],
                                          (ulong)marked_up,
                                          cats.relations[filled->rel].jslot,
                                          sent.words[filled->filler - 1],
                                          (ulong)filled->rule)) != m_deps_ignore_map_3.end())
    return true;

  return false;
}


void
RNNParser::_Impl::init_xf1_training(bool train) {
  if (train)
    load_gold_deps(m_config_dir + "/wsj02-21.ccgbank_deps", true);
  else
    load_gold_deps(m_config_dir + "/wsj00.ccgbank_deps", false);
  init_ignore_rules();
  load_dep_ignore_list(m_config_dir + "/deps_ignore_cat_slot_rule");
  load_dep_ignore_list2(m_config_dir + "/deps_ignore_pred_cat_slot_rule");
  load_dep_ignore_list3(m_config_dir + "/deps_ignore_pred_cat_slot_arg_rule");
}


void
RNNParser::_Impl::load_gold_deps(const string &filename, bool train) {
  // note: there are sentences in wsj0221_ccgbank_deps
  //       which have zero deps

  size_t total_sents = 0;
  if (train)
    total_sents = 39604;
  else
    total_sents = 1913;

  const ulong BASE = 10000;

  ifstream in(filename.c_str());
  if(!in) {
    cerr << "no such file: " << filename << endl;
    exit (EXIT_FAILURE);
  }
  string s;
  ulong sent_dep_count = 0;
  ulong empty = 0;

  while (getline(in, s)) {
    if (s.find("# this") != string::npos ||
        s.find("# src/") != string::npos)
      continue;

    if (s.empty()) {
      ++empty;
      if (empty >= 2 && empty <= total_sents + 1) {
        if (train)
          assert(m_ccg_gold_dep_count_map_train.insert(make_pair(empty - 2, sent_dep_count)).second);
        else
          assert(m_ccg_gold_dep_count_map_dev.insert(make_pair(empty - 2, sent_dep_count)).second);
      }
      sent_dep_count = 0;
      continue;
    }

    ++sent_dep_count;
    istringstream ss(s);
    string head_str;
    string plain_markedup_str;
    string arg_str;

    ulong head = 0;
    int markedup = 0;
    ulong slot = 0;
    ulong arg = 0;

    if(ss >> head_str >> plain_markedup_str >> slot >> arg_str) {
      head = stoi(head_str.substr(head_str.find("_") + 1));

      int ind = cats.markedup.index(plain_markedup_str);
      if (ind != -1) {
        // convert plain markedup index to markedup index
        // since they have different indexes in cats.markedup
        markedup = cats.markedup.index(cats.markedup.markedup(plain_markedup_str));
        assert(markedup != -1);
      } else {
        auto iter = m_unk_markedup_map.find(plain_markedup_str);
        if (iter != m_unk_markedup_map.end()) {
          markedup = iter->second;
        } else {
          markedup = m_unk_markedup_map.size() + BASE;
          m_unk_markedup_map.insert(make_pair(plain_markedup_str, m_unk_markedup_map.size() + BASE));
        }
      }

      arg = stoi(arg_str.substr(arg_str.find("_") + 1));

      CCGDep dep(empty - 1, head, (ulong)markedup, slot, arg);
      if (train)
        assert(m_ccg_dep_map_train.insert(make_pair(dep, 0)).second);
      else
        assert(m_ccg_dep_map_dev.insert(make_pair(dep, 0)).second);
    }
    else {
      cerr << "could not interpret dependency\n";
      exit(EXIT_FAILURE);
    }
  }

  if (train)
    assert(m_ccg_gold_dep_count_map_train.size() == 39604);
  else
    assert(m_ccg_gold_dep_count_map_dev.size() == 1913);
}


void
RNNParser::_Impl::init_ignore_rules() {
  m_deps_ignore_map_rule.insert({{7, 0}, {11, 0}, {12, 0},
    {14, 0}, {15, 0}, {16, 0},
    {17, 0}, {51, 0}, {52, 0},
    {56, 0}, {91, 0}, {92, 0},
    {95, 0}, {96, 0}, {98, 0}});
}


void
RNNParser::_Impl::load_dep_ignore_list(const std::string &filename) {
  ifstream in(filename.c_str());
  if (!in) {
    cerr << "no such file: " << filename << endl;
    exit(EXIT_FAILURE);
  }
  string s;
  while (getline(in, s)) {
    //cout << s << endl;
    if (s.find("#") == 0) {
      continue;
    }

    istringstream ss(s);
    string cat_str;
    int cat = 0;
    ulong slot = 0;
    ulong rule_id = 0;
    if(ss >> cat_str >> slot >> rule_id) {
      cat = cats.markedup.index(cat_str);
      if (cat == -1) {
        cerr << "unrecognized markedup cat in " <<
            filename << ", exit now...\n";
        exit(EXIT_FAILURE);
      }
      assert(m_deps_ignore_map_1.insert(make_pair(make_tuple((ulong)cat, slot, rule_id), 0)).second);
    } else {
      cerr << "something is wrong with deps_ignore file !!!\n";
      exit(EXIT_FAILURE);
    }
  }
}


void
RNNParser::_Impl::load_dep_ignore_list2(const std::string &filename) {
  ifstream in(filename.c_str());
  if (!in) {
    cerr << "no such file: " << filename << endl;
    exit(EXIT_FAILURE);
  }
  string s;
  while (getline(in, s)) {
    //cout << s << endl;
    if (s.find("#") == 0) {
      continue;
    }

    istringstream ss(s);
    string pred;
    string cat_str;
    int cat = 0;
    ulong slot = 0;
    ulong rule_id = 0;
    if(ss >> pred >> cat_str >> slot >> rule_id) {
      cat = cats.markedup.index(cat_str);
      if (cat == -1) {
        cerr << "unrecognized markedup cat in " <<
            filename << ", exit now...\n";
        exit(EXIT_FAILURE);
      }
      assert(m_deps_ignore_map_2.insert(make_pair(make_tuple(pred, (ulong)cat, slot, rule_id), 0)).second);
    } else {
      cerr << "something is wrong deps_ignore file !!!\n";
      exit(EXIT_FAILURE);
    }
  }
}


void
RNNParser::_Impl::load_dep_ignore_list3(const std::string &filename) {

  //  int id1 = m_cats.markedup.index("(S[pt]\\NP)/(S[ng]\\NP)");
  //  int id2 = m_cats.markedup.index("((S[pt]{_}\\NP{Y}<1>){_}/(S[ng]{Z}<2>\\NP{Y*}){Z}){_}");
  //
  //  cout << "id1: " << id1 << endl;
  //  cout << "id2: " << id2 << endl;

  ifstream in(filename.c_str());
  if (!in) {
    cerr << "no such file: " << filename << endl;
    exit(EXIT_FAILURE);
  }
  string s;
  while (getline(in, s)) {
    //cout << s << endl;
    if (s.find("#") == 0) {
      continue;
    }

    istringstream ss(s);
    string pred;
    string cat_str;
    int cat = 0;
    ulong slot = 0;
    string arg;
    ulong rule_id = 0;
    if(ss >> pred >> cat_str >> slot >> arg >> rule_id) {
      cat = cats.markedup.index(cat_str);
      if (cat == -1) {
        cerr << "unrecognized markedup cat in " <<
            filename << ", exit now...\n";
      }
      assert(m_deps_ignore_map_3.insert(make_pair(make_tuple(pred, (ulong)cat, slot, arg, rule_id), 0)).second);
    } else {
      cerr << "something is wrong deps_ignore file !!!\n";
      exit(EXIT_FAILURE);
    }
  }
}


double
RNNParser::_Impl::calc_f1(const ShiftReduceHypothesis *&output, const size_t id, bool train) {
  // sent id should start from 0
  double gold_total = 0.0;
  double total_correct = 0.0;
  double total_returned = 0.0;

  gold_total = get_total_sent_gold_deps(id, train);
  //assert(gold_total != 0); could be 0 in CCGBank
  size_t stackSize = output->GetStackSize();
  std::vector<const SuperCat *> roots;
  vector<const SuperCat *>::const_reverse_iterator rit;
  roots.push_back(output->GetStackTopSuperCat());
  const ShiftReduceHypothesis *prev_stack = output;

  for (size_t i = 0; i < stackSize - 1; ++i) {
    prev_stack = prev_stack->GetPrvStack();
    roots.push_back(prev_stack->GetStackTopSuperCat());
  }

  for (rit = roots.rbegin(); rit < roots.rend(); ++rit) {
    //printer.parsed(*rit, sent, BETA, 0);
    get_deps(*rit, id, total_correct, total_returned, 0, train);
  }
  cerr << "correct: " << total_correct;
  cerr << " total: " << total_returned;
  cerr << " gold total: " << gold_total << endl;
  assert(total_correct <= gold_total);
  double p = total_correct/total_returned;
  double r = total_correct/gold_total;
  double f1 = (p == 0.0) || (r == 0.0) ? 0.0 : (2.0*p*r)/(p + r);
  fprintf(stderr, "p: %f, r: %f, f1: %f\n", p, r, f1);
  assert(f1 <= 1.0);

  //  total_correct += gold;
  //  total_returned += total;
  //  total_gold += gold_total;

  return f1;
}


//void RNNParser::_Impl::test_internal_f1_calc(size_t sent_id) {
//  const ShiftReduceHypothesis *output = sr_parse_beam_search(0.0, sent_id);
//  if(output) {
//    assert(output != 0);
//
//    size_t stackSize = output->GetStackSize();
//    std::vector<const NLP::CCG::SuperCat *> roots;
//    vector<const NLP::CCG::SuperCat *>::const_reverse_iterator rit;
//    roots.push_back(output->GetStackTopSuperCat());
//
//    for (size_t i = 0; i < stackSize - 1; ++i) {
//      output = output->GetPrvStack();
//      roots.push_back(output->GetStackTopSuperCat());
//    }
//
//    for (rit = roots.rbegin(); rit < roots.rend(); ++rit) {
//      get_deps(*rit, sent_id + 1, m_gold_deps_sec0221, m_total, FMT, false);
//    }
//  }
//}


void
RNNParser::_Impl::get_deps(const CCG::SuperCat *sc, const ulong id,
                           double &gold, double &total, const int FMT, bool train) {
  if(sc->left){
    assert(sc->left);
    get_deps(sc->left, id, gold, total, FMT, train);
    if(sc->right){
      assert(sc->right);
      get_deps(sc->right, id, gold, total, FMT, train);
    }
  }

  pair<double, double> res;
  if (train)
    res = count_gold_deps(sc, id, true);
  else
    res = count_gold_deps(sc, id, false);
  gold += res.first;
  total += res.second;
}


pair<double, double>
RNNParser::_Impl::count_gold_deps(const SuperCat *sc, const ulong id, bool train) {
  double total = 0.0;
  double gold = 0.0;
  for(const Filled *filled = sc->filled; filled; filled = filled->next) {
    if (ignore_dep(filled)) {
      continue;
    }
    ++total;
    CCGDep dep(id, filled->head,
               cats.markedup.index((string) cats.relations[filled->rel].cat),
               cats.relations[filled->rel].jslot, filled->filler);
    if (train) {
      if (m_ccg_dep_map_train.find(dep) != m_ccg_dep_map_train.end())
        ++gold;
    } else {
      if (m_ccg_dep_map_dev.find(dep) != m_ccg_dep_map_dev.end())
        ++gold;
    }
  }

  return make_pair(gold, total);
}


double
RNNParser::_Impl::get_total_sent_gold_deps(const size_t id, const bool train) {
  if (train) {
    assert(m_ccg_gold_dep_count_map_train.find(id) != m_ccg_gold_dep_count_map_train.end());
    return m_ccg_gold_dep_count_map_train.find(id)->second;
  } else {
    assert(m_ccg_gold_dep_count_map_dev.find(id) != m_ccg_gold_dep_count_map_dev.end());
    return m_ccg_gold_dep_count_map_dev.find(id)->second;
  }
}


// this function is used for training using xf1 objective
//void RNNParser::_Impl::sr_parse_train_xf1 (const size_t k) {
//  vector<ShiftReduceHypothesis*> kbest_vec;
//  size_t popped = 0;
//  while (!kbest_output_queue.empty()) {
//    ++popped;
//    kbest_vec.push_back(kbest_output_queue.top());
//    kbest_output_queue.pop();
//    if (popped == k)
//      continue;
//  }
//
//  assert(kbest_vec.size() >= 1);
//  cerr << "kbest_vec.size(): " << kbest_vec.size() << endl;
//
//
//
//}



// this function is used for both training and
// testing with max-margin loss objective
const ShiftReduceHypothesis*
RNNParser::_Impl::sr_parse_max_margin(const double BETA, const size_t sent_id) {
  try {
    m_gold_equiv_map.clear();
    vector<ShiftReduceAction*> null_vec;
    vector<pair<size_t, size_t> > null_vec2;
    const ShiftReduceHypothesis *best_hypo; // the highest-scored hypo on each level
    const ShiftReduceHypothesis *gold_hypo;
    vector<ShiftReduceAction*> &gold_tree_actions = m_train ? *m_goldTreeVec.at(sent_id) : null_vec;
    vector<pair<size_t, size_t> > &gold_tree_actions_rnn_fmt = null_vec2;
    if (m_train)
      gold_tree_actions_rnn_fmt = m_gold_tree_vec_rnn[sent_id];
    size_t gold_tree_size = gold_tree_actions.size();
    assert(gold_tree_actions.size() == gold_tree_actions_rnn_fmt.size());

    ShiftReduceAction *gold_action = 0;
    pair<size_t, size_t> gold_action_rnn_fmt;
    size_t gold_action_id = 0;
    bool gold_finished = false;

    if(sent.words.size() > cfg.maxwords())
      throw NLP::ParseError("sentence length exceeds maximum number of words for parser", nsentences);

    const ShiftReduceHypothesis *candidate_output = 0;

    // inds from the input sentence
    const size_t NWORDS = sent.words.size();
    vector<size_t> word_inds_vec;
    vector<size_t> suf_inds_vec;
    words2word_inds(sent.words, sent.pos, word_inds_vec, suf_inds_vec);
    nsentences++;
    SuperCat::nsupercats = 0;

    m_lattice = new ShiftReduceLattice(NWORDS, m_beamSize, MAX_UNARY_ACTIONS);
    ShiftReduceHypothesis *start_hypo = new ShiftReduceHypothesis(0, 0, 0, 0, 0, 0, 0.0, false, 0, 0, 0, 0, 0.0, 0);
    start_hypo->SetStartHypoFlag();
    m_lattice->Add(0, start_hypo);
    size_t startIndex;
    if (m_train)
      gold_hypo = m_lattice->GetLatticeArray(0); // gold == start

    // inds from the current context
    vector<size_t> word_inds_ret;
    vector<size_t> suf_inds_ret;
    vector<size_t> supercat_inds_ret;
    vector<size_t> phrase_emb_ind_ret;
    vector<size_t> child_phrase_emb_ind_ret;

    // set start and end indexes of the lattice
    size_t current_gold_action_count = 0;
    while (m_lattice->GetEndIndex() == 0 ||
        m_lattice->GetEndIndex() > m_lattice->GetPrevEndIndex()) {

      if (m_lattice->GetEndIndex() != 0)
        startIndex = m_lattice->GetPrevEndIndex() + 1;
      else
        startIndex = 0;
      m_lattice->SetPrevEndIndex(m_lattice->GetEndIndex());

      if (m_train) {
        if (gold_action_id < gold_tree_size) {
          current_gold_action_count = gold_action_id + 1;
          gold_action = gold_tree_actions[gold_action_id];
          assert(gold_action);
          //cerr << "gold_action: " << gold_action->GetAction() << " " << gold_action->GetCat()->out_novar_noX_noNB_SR_str() << endl;
          gold_action_rnn_fmt = gold_tree_actions_rnn_fmt[gold_action_id];
          assert(gold_action_rnn_fmt.first == gold_action->GetAction());
        } else {
          //cerr << "gold_finished\n";
          gold_finished = true;
        }
      }

      HypothesisPQueue hypoQueue;

      // gold_hidden_states saves the
      // hidden layer directly connected
      // to the gold action, used later
      // to do early update
      // ***this var is per beam***
      //mat gold_hidden_states;

      // other per beam vars
      // gold_action_class is used to identify
      // the gold action amoung all feasible actions
      // it's init'ed to -1 (the 1st feasible action is 0 at the output layer)
      int gold_action_class = -1;
      double gold_action_score = 0.0; // not needed anymore, get rid of this asap

      // this will be set to true when expanding
      // the current gold hypo using the gold action
      bool gold_action_in_feasible = false;

      // expand hypos in current beam
      for (size_t i = startIndex; i <= m_lattice->GetEndIndex(); ++i) {
        //cerr << m_lattice->GetEndIndex() - startIndex << endl;
        //assert(m_lattice->GetEndIndex() - startIndex == 0);
        ShiftReduceHypothesis *hypo = const_cast<ShiftReduceHypothesis *>(m_lattice->GetLatticeArray(i));

        // no more feasible actions, candidate output
        // ############## lengh normalization or not??

        //        if (hypo->IsFinished(NWORDS, m_allowFragTree)) {
        //          if (candidate_output == 0 ||
        //              (hypo->GetTotalScore() / hypo->GetTotalActionCount()) >
        //                (candidate_output->GetTotalScore() / candidate_output->GetTotalActionCount())) {
        //            candidate_output = hypo;
        //          }
        //        }
        //
        //        if (hypo == gold_hypo)
        //          cerr << "gold_hypo\n";

        if (hypo->IsFinished(NWORDS, m_allowFragTree)) {
          if (candidate_output == 0 || hypo->GetTotalScore() > candidate_output->GetTotalScore())
            candidate_output = hypo;

          //          if (m_train && (candidate_output == 0 || ((hypo->GetTotalScore() + cfg.loss_pen()*(gold_action_id + 1 - hypo->m_total_gold_action_count))
          //                  > (candidate_output->GetTotalScore() + cfg.loss_pen()*(gold_action_id + 1 - candidate_output->m_total_gold_action_count))) ))
          //            candidate_output = hypo;
        }

        vector<vector<size_t> > context_vec;
        word_inds_ret.clear();
        suf_inds_ret.clear();
        supercat_inds_ret.clear();
        phrase_emb_ind_ret.clear();
        child_phrase_emb_ind_ret.clear();

        ContextLoader context_loader(m_cat_ind, m_head_limit, m_train);
        context_loader.load_context(hypo, word_inds_ret,
                                    suf_inds_ret, supercat_inds_ret,
                                    phrase_emb_ind_ret, child_phrase_emb_ind_ret,
                                    word_inds_vec, suf_inds_vec,
                                    m_cats_map, cats, cfg.use_phrase_emb_feats(), m_train);

        assert(word_inds_ret.size() == suf_inds_ret.size());
        assert(word_inds_ret.size() == m_feature_count);
        if (m_use_supercat_feats)
          assert(supercat_inds_ret.size() == m_supercat_feature_count);

        context_vec.push_back(word_inds_ret);
        context_vec.push_back(suf_inds_ret);
        if (m_use_supercat_feats)
          context_vec.push_back(supercat_inds_ret);
        context_vec.push_back(phrase_emb_ind_ret);
        context_vec.push_back(child_phrase_emb_ind_ret);
        hypo->m_context_vec = context_vec;

        // the map is a check to make sure all the feasible
        // target values for the current context are unique
        // feasible_y_vals_map: key: output class (action id), value: (S/R/U, result_cat)

        // the following are per hypo
        vector<size_t> feasible_y_vals;
        feasible_y_vals.reserve(700);
        unordered_map<size_t, pair<size_t, SuperCat*> > feasible_y_vals_map;

        // find all feasible actions,
        // but do not push them into the queue.
        // remember the gold action
        // if it gets pruned by the beam
        // we recover it and update the model
        // using it.

        // shift
        size_t j = hypo->GetNextInputIndex();

        if (j < NWORDS) {
          const MultiRaw &multi = sent.msuper[j];
          for (MultiRaw::const_iterator k = multi.begin(); k!= multi.end(); ++k) {
            const Cat *cat = cats.markedup[k->raw];

            if (!cat) {
              cerr << "SHIFT action error: attempted to load category without markedup "
                  << k->raw << endl;
              exit (EXIT_FAILURE);
            }

            assert(m_lex_cat_ind_map.find(k->raw) != m_lex_cat_ind_map.end());
            size_t cat_ind = m_lex_cat_ind_map[k->raw];
            feasible_y_vals.push_back(cat_ind);

            //cerr << "cat id: " << m_lex_cat_ind_map[k->raw] << endl;
            SuperCat *stack_top_cat = SuperCat::Lexical(chart.pool, j+1, cat, 0);
            stack_top_cat->m_left_ind = j;
            stack_top_cat->m_right_ind = j;
            assert(stack_top_cat->cat);
            stack_top_cat->m_word_vec_ind = word_inds_vec[j];
            assert(feasible_y_vals_map.insert(make_pair(cat_ind, make_pair(0, stack_top_cat))).second);

            if (m_train && hypo == gold_hypo && gold_action_rnn_fmt.first == SHIFT
                && cat_ind == gold_action_rnn_fmt.second) {
              gold_action_class = cat_ind;
              gold_action_in_feasible = true;
            }

          } // end for
        } // end shift

        // unary
        if (hypo->GetStackTopSuperCat() != 0) {
          const SuperCat *sc = hypo->GetStackTopSuperCat();
          vector<SuperCat *> tmp;
          tmp.clear();

          if (!sc->IsLex() && !sc->IsTr()) {
            unary_tc(hypo->GetStackTopSuperCat(), false, tmp);
            unary_tr(sc, tmp);
          } else if (sc->IsLex() && !sc->IsTr()) {
            unary_tr(sc, tmp);
          } else if (!sc->IsLex() && sc->IsTr()) { /* do nothing */ }
          else if (sc->IsLex() && sc->IsTr()) { /* do nothing */ }

          for (vector<SuperCat *>::iterator j = tmp.begin(); j < tmp.end(); ++j) {
            SuperCat *stack_top_cat = *j;
            assert(stack_top_cat->left);
            stack_top_cat->m_word_vec_ind = hypo->GetStackTopSuperCat()->m_word_vec_ind;
            feasible_y_vals.push_back(stack_top_cat->m_unary_id + m_unary_base_count);
            assert(feasible_y_vals_map.insert(make_pair(stack_top_cat->m_unary_id + m_unary_base_count,
                                                        make_pair(1, stack_top_cat))).second);

            if (m_train && hypo == gold_hypo && gold_action_rnn_fmt.first == UNARY
                && stack_top_cat->m_unary_id + m_unary_base_count ==
                    gold_action_rnn_fmt.second + m_unary_base_count) {
              gold_action_class = stack_top_cat->m_unary_id + m_unary_base_count;
              gold_action_in_feasible = true;
            }

            stack_top_cat->m_left_ind = sc->m_left_ind;
            stack_top_cat->m_right_ind = sc->m_right_ind;
          }
        } // end unary

        // reduce
        if (hypo->GetPrvStack() != 0 && hypo->GetPrvStack()->GetStackTopSuperCat() != 0
            && hypo->GetStackTopSuperCat() != 0) {
          results.clear();
          if (!cfg.seen_rules() ||
              rule_instances(hypo->GetPrvStack()->GetStackTopSuperCat()->GetCat(), hypo->GetStackTopSuperCat()->GetCat()))
            rules(hypo->GetPrvStack()->GetStackTopSuperCat(), hypo->GetStackTopSuperCat(),
                  cfg.eisner_nf(), cfg.seen_rules(), cfg.question_rules(), results);

          if (results.size() > 0) {
            for (vector<SuperCat *>::iterator k = results.begin(); k < results.end(); ++k) {
              SuperCat *stack_top_cat = *k;
              //cerr << "reduce_id: " << stack_top_cat->m_reduce_id << endl;

              if (stack_top_cat->m_reduce_id == 0) {
                feasible_y_vals.push_back(m_reduce_base_count + 1);
                assert(feasible_y_vals_map.insert(make_pair(m_reduce_base_count + 1,
                                                            make_pair(2, stack_top_cat))).second);
              } else {
                feasible_y_vals.push_back(m_reduce_base_count + stack_top_cat->m_reduce_id + 1);
                assert(feasible_y_vals_map.insert(make_pair(m_reduce_base_count + stack_top_cat->m_reduce_id + 1,
                                                            make_pair(2, stack_top_cat))).second);
              }

              if (m_train && hypo == gold_hypo && gold_action_rnn_fmt.first == COMBINE
                  && eqSR(stack_top_cat->GetCat(), gold_action->GetCat())) {
                gold_action_in_feasible = true;
                gold_action_class = m_reduce_base_count + stack_top_cat->m_reduce_id + 1;
              }

              stack_top_cat->m_left_ind = stack_top_cat->left->m_left_ind;
              stack_top_cat->m_right_ind = stack_top_cat->right->m_right_ind;
            }
          }
        } // end reduce

        // debug
        // when expanding gold_hypo, the next gold
        // action must be one of the feasible actions
        // and we must have more than 1 feasible actions

        if (m_train && hypo == gold_hypo && !gold_finished) {
          assert(feasible_y_vals.size() > 0);
        }

        if (feasible_y_vals.size() > 0) {
          // has feasible actions, then
          // score current context and get the
          // scores for all feasible actions
          // push all hypo_tuples into queue

          //mat output_vals_feasible(1, feasible_y_vals.size(), fill::zeros);
          //          mat hypo_new_hidden_state;
          //          hypo_new_hidden_state.copy_size(m_h_tm1);
          //          hypo_new_hidden_state.zeros();

          mat ly_val = zeros(1, m_wy.n_cols);
          // every hypo expanded from the current hypo
          // will get new_hidden_state as their hidden states
          // which will be set in score_context_beam_search(...)
          // hypo->score_context_beam_search will also be set
          mat prev_hypo_lh = hypo->IsStartHypo() ? zeros(m_h_tm1.n_rows, m_h_tm1.n_cols) :
              hypo->GetPrevHypo()->m_hidden_states;
          // this context vec will be converted to hypo->m_converted_context_vec below
          score_context_max_margin_no_softmax(context_vec, prev_hypo_lh,hypo->m_hidden_states,hypo->m_converted_context_vec,
                                              hypo->m_x_mask_vec, ly_val);

          //hypo->m_output_vals_feasible = output_vals_feasible;

          // output_vals_feasible = log(output_vals_feasible);

          //          if (m_train) {
          //            //hypo->m_feasible_y_vals = feasible_y_vals;
          //            // output_vals_feasible: softmax'ed feasible_y_vals
          //            hypo->m_output_vals_feasible = output_vals_feasible;
          //          }

          // output_vals_feasilbe have scores for feasible actions
          // feasible_y_vals have action classes for feasible actions

          // push all feasible actions into queue
          double total_score = 0.0;
          unordered_map<size_t, pair<size_t, SuperCat*> >::const_iterator action_iter;
          for (size_t action_ind = 0; action_ind < feasible_y_vals.size(); ++action_ind) {
            // this is currently a naive implementation
            // ideally, we don't have to expand every
            // feasible action into the beam, or do we ???
            const size_t action_id = feasible_y_vals[action_ind];
            action_iter = feasible_y_vals_map.find(action_id);
            assert(action_iter != feasible_y_vals_map.end());

            const pair<size_t, SuperCat*> &feasible_action = action_iter->second;
            total_score = hypo->GetTotalScore() + ly_val(action_id);

            bool is_gold_action = false;

            if (m_train && hypo == gold_hypo && (size_t) gold_action_class == action_id
                && gold_action_rnn_fmt.first == action_iter->second.first) {
              assert(gold_action_class >= 0);
              gold_action_score = ly_val(action_id);
              is_gold_action = true;
            }

            // do the action
            // shift
            if (feasible_action.first == SHIFT) {
              assert(j < NWORDS);
              assert(feasible_action.second->cat);
              ShiftReduceAction *action_shift = new ShiftReduceAction(SHIFT, feasible_action.second);
              HypothesisTuple *new_hypo_tuple =
                  new HypothesisTuple(hypo, action_shift, total_score, i,
                                      action_id, ly_val(action_id));
              hypoQueue.push(new_hypo_tuple);

              if (is_gold_action)
                m_gold_equiv_map.insert(make_pair(*action_shift, 0));

              // unary
            } else if (feasible_action.first == UNARY) {

              assert(feasible_action.second->left);
              ShiftReduceAction *action_unary = new ShiftReduceAction(UNARY, feasible_action.second);
              HypothesisTuple *new_hypo_tuple =
                  new HypothesisTuple(hypo, action_unary, total_score, i,
                                      action_id, ly_val(action_id));
              hypoQueue.push(new_hypo_tuple);

              if (is_gold_action)
                m_gold_equiv_map.insert(make_pair(*action_unary, 0));

              // reduce
            } else if (feasible_action.first == COMBINE) {

              assert(hypo->GetPrvStack() != 0 && hypo->GetPrvStack()->GetStackTopSuperCat() != 0
                     && hypo->GetStackTopSuperCat() != 0);
              assert(feasible_action.second);
              ShiftReduceAction *action_combine = new ShiftReduceAction(COMBINE, feasible_action.second);
              HypothesisTuple *new_hypo_tuple =
                  new HypothesisTuple(hypo, action_combine, total_score, i,
                                      action_id, ly_val(action_id));
              hypoQueue.push(new_hypo_tuple);

              if (is_gold_action)
                m_gold_equiv_map.insert(make_pair(*action_combine, 0));

            } else {
              cerr << "action not recognized...\n";
              exit (EXIT_FAILURE);
            }
          }

        }
      } // end for

      if (m_train && !gold_finished) {
        assert(gold_action_in_feasible);
      }

      best_hypo = 0;
      construct_hypo_max_margin(hypoQueue, m_lattice, best_hypo, gold_hypo, gold_action,
                                gold_finished, current_gold_action_count);

      //cerr << "m_hasGoldAction " << m_hasGoldAction << endl;
      //const bool xxx = candidate_output != gold_hypo;
      //const bool yyy = gold_action_id < gold_tree_size;
      //cerr << "candidate_output != gold_hypo" << xxx << endl;
      // cerr << "gold_action_id < gold_tree_size" << yyy << endl;

      // early update
      if (m_train && !m_hasGoldAction && gold_action_id < gold_tree_size) {
        assert(!gold_finished);
        assert(best_hypo);
        cerr << "########## Early Update, sentenceID: " << sent_id
            << " -- found " << gold_action_id + 1 << " out of " << gold_tree_size  << " gold actions\n";

        if (candidate_output != gold_hypo) {
          //          if (cfg.use_structrue_loss()) {
          //            if (candidate_output &&
          //                 (((candidate_output->GetTotalScore() + cfg.loss_pen()*(gold_action_id + 1 - candidate_output->m_total_gold_action_count))
          //                    > (best_hypo->GetTotalScore() + cfg.loss_pen()*(gold_action_id + 1 - best_hypo->m_total_gold_action_count))))) {
          //              best_hypo = candidate_output;
          //            }
          //          } else {
          if (best_hypo == 0 ||
              (candidate_output && candidate_output->GetTotalScore() > best_hypo->GetTotalScore())) {
            best_hypo = candidate_output;
          }
          //}
        }

        assert(best_hypo);
        assert(gold_action->GetCat());
        SuperCat *goldSuperCat = SuperCat::Wrap(chart.pool, gold_action->GetCat());
        assert(goldSuperCat);
        _action(gold_hypo, gold_action->GetAction(), m_lattice, goldSuperCat,
                0.0, gold_action_class, gold_action_score);
        gold_hypo = m_lattice->GetLatticeArray(m_lattice->GetEndIndex());

        mat grad_wh_plus;
        mat grad_wx_plus;
        mat grad_wy_plus;
        grad_wh_plus.copy_size(m_wh);
        grad_wx_plus.copy_size(m_wx);
        grad_wy_plus.copy_size(m_wy);
        grad_wh_plus.zeros();
        grad_wx_plus.zeros();
        grad_wy_plus.zeros();

        mat grad_wh_minus;
        mat grad_wx_minus;
        mat grad_wy_minus;
        grad_wh_minus.copy_size(m_wh);
        grad_wx_minus.copy_size(m_wx);
        grad_wy_minus.copy_size(m_wy);
        grad_wh_minus.zeros();
        grad_wx_minus.zeros();
        grad_wy_minus.zeros();

        update_rnn(gold_hypo, false, grad_wh_plus, grad_wx_plus, grad_wy_plus); // positive update
        update_rnn(best_hypo, true, grad_wh_minus, grad_wx_minus, grad_wy_minus); // negative update

        m_wh += m_lr*grad_wh_plus;
        m_wx += m_lr*grad_wx_plus;
        m_wy += m_lr*grad_wy_plus;

        m_wh -= m_lr*grad_wh_minus;
        m_wx -= m_lr*grad_wx_minus;
        m_wy -= m_lr*grad_wy_minus;

        // hack exit
        //chart.reset();
        sent.reset();
        delete m_lattice;
        return 0;
      }

      if (m_train && gold_action_id < gold_tree_size)
        ++gold_action_id;

    } // end while


    if (m_train) {
      if (candidate_output != gold_hypo) {
        cerr << "######### late update" << " -- total gold actions: " << gold_tree_size << endl;

        mat grad_wh_plus;
        mat grad_wx_plus;
        mat grad_wy_plus;
        grad_wh_plus.copy_size(m_wh);
        grad_wx_plus.copy_size(m_wx);
        grad_wy_plus.copy_size(m_wy);
        grad_wh_plus.zeros();
        grad_wx_plus.zeros();
        grad_wy_plus.zeros();

        mat grad_wh_minus;
        mat grad_wx_minus;
        mat grad_wy_minus;
        grad_wh_minus.copy_size(m_wh);
        grad_wx_minus.copy_size(m_wx);
        grad_wy_minus.copy_size(m_wy);
        grad_wh_minus.zeros();
        grad_wx_minus.zeros();
        grad_wy_minus.zeros();

        update_rnn(gold_hypo, false, grad_wh_plus, grad_wx_plus, grad_wy_plus);
        update_rnn(candidate_output, true, grad_wh_minus, grad_wx_minus, grad_wy_minus);

        m_wh += m_lr*grad_wh_plus;
        m_wx += m_lr*grad_wx_plus;
        m_wy += m_lr*grad_wy_plus;

        m_wh -= m_lr*grad_wh_minus;
        m_wx -= m_lr*grad_wx_minus;
        m_wy -= m_lr*grad_wy_minus;

        sent.reset();
        delete m_lattice;
        return 0;
      } else {
        cerr << "sentence: " << sent_id << " ###### output correct #####" <<
            " --total gold actions: " << gold_tree_size << endl;

        sent.reset();
        delete m_lattice;
        return 0;
      }
    }

    if (!m_train && candidate_output)
      return candidate_output;
    return 0;

  }
  catch(NLP::Exception e) {
    //throw NLP::ParseError(e.msg, nsentences);
    cerr << e.msg << endl;
    exit(EXIT_FAILURE);
  }
}


void
RNNParser::_Impl::update_rnn(const ShiftReduceHypothesis *hypo, const bool negative,
                             mat &grad_wh, mat &grad_wx, mat &grad_wy) {

  const ShiftReduceHypothesis *current_hypo = hypo;
  const ShiftReduceHypothesis *prev_hypo = current_hypo->GetPrevHypo();
  size_t total_steps = hypo->GetTotalActionCount();
  // total_step == number of predicted actions (including the gold one)
  // and is different from m_bs (might be shorter or longer)

  mat one;
  mat zero_h;
  one.copy_size(m_h_tm1);
  zero_h.copy_size(m_h_tm1);
  one.ones();
  zero_h.zeros();

  mat lh_vals[total_steps + 1];
  mat lh_deriv_vals[total_steps];
  mat x_vals[total_steps];
  mat x_mask_vals[total_steps];
  mat delta_y_vals[total_steps];
  lh_vals[0] = zero_h;

  vector<vector<vector<size_t> > > all_context_vec;
  all_context_vec.reserve(total_steps);

  int i = total_steps - 1;

  while (prev_hypo) {
    assert(prev_hypo);
    lh_vals[i + 1] = prev_hypo->m_hidden_states;
    lh_deriv_vals[i] = lh_vals[i + 1] % (one - lh_vals[i + 1]);

    x_vals[i] = prev_hypo->m_converted_context_vec;
    x_mask_vals[i] = prev_hypo->m_x_mask_vec;
    all_context_vec.push_back(prev_hypo->m_context_vec);

    // dim of ly is 1xm_wy.n_cols, set the dim of delta_y
    // to nx1 without doing transpose later
    delta_y_vals[i] = zeros<mat>(m_wy.n_cols, 1);
    delta_y_vals[i](current_hypo->m_action_id)
                                                                                                                                                                                                                                                                                                                                                                                                        = (current_hypo->m_action_score) * (1.0 - current_hypo->m_action_score);

    current_hypo = prev_hypo;
    prev_hypo = prev_hypo->GetPrevHypo();
    assert(current_hypo != prev_hypo);
    --i;
    if (i == -1)
      assert(!prev_hypo);
  }

  std::reverse(all_context_vec.begin(), all_context_vec.end());
  assert(i == -1);
  assert(current_hypo->GetTotalActionCount() == 0);
  assert(prev_hypo == NULL);

  vector<mat> lh_vals_mini_batch;
  vector<mat> lh_deriv_vals_mini_batch;
  vector<mat> x_vals_mini_batch;
  vector<mat> x_mask_vals_mini_batch;
  vector<mat> delta_y_vals_mini_batch;
  vector<vector<vector<size_t> > > context_vec_minibatch;

  // add one, as need to keep lh_val from the previous step
  lh_vals_mini_batch.reserve(m_bs + 1);
  lh_deriv_vals_mini_batch.reserve(m_bs);
  x_vals_mini_batch.reserve(m_bs);
  x_mask_vals_mini_batch.reserve(m_bs);
  delta_y_vals_mini_batch.reserve(m_bs);
  context_vec_minibatch.reserve(m_bs);

  for (size_t j = 0; j < total_steps; ++j) {
    lh_vals_mini_batch.push_back(lh_vals[j]);
    lh_deriv_vals_mini_batch.push_back(lh_deriv_vals[j]);
    x_vals_mini_batch.push_back(x_vals[j]);
    x_mask_vals_mini_batch.push_back(x_mask_vals[j]);
    delta_y_vals_mini_batch.push_back(delta_y_vals[j]);
    context_vec_minibatch.push_back(all_context_vec[j]);

    if ((j + 1) % m_bs == 0 || j == total_steps - 1) {
      lh_vals_mini_batch.push_back(lh_vals[j + 1]);
      bptt_dropout_max_margin(context_vec_minibatch,
                              lh_vals_mini_batch, lh_deriv_vals_mini_batch,
                              x_vals_mini_batch, x_mask_vals_mini_batch,
                              delta_y_vals_mini_batch,
                              grad_wh, grad_wx, grad_wy,
                              negative);

      lh_vals_mini_batch.clear();
      lh_deriv_vals_mini_batch.clear();
      x_vals_mini_batch.clear();
      x_mask_vals_mini_batch.clear();
      delta_y_vals_mini_batch.clear();
      context_vec_minibatch.clear();
    }
  }
}


// bptt multi bs number of steps, each step corresponds to a contextwin
void
RNNParser::_Impl::bptt_dropout_max_margin(const vector<vector<vector<size_t> > > &context_batch,
                                          const vector<mat> &lh_vals,
                                          const vector<mat> &lh_deriv_vals,
                                          const vector<mat> &x_vals,
                                          const vector<mat> &x_mask_vals,
                                          const vector<mat> &delta_y_vals,
                                          mat &grad_wh, mat &grad_wx, mat &grad_wy,
                                          bool negative) {
  //double err = 0.0;
  size_t steps = context_batch.size(); // how many actions predicted

  //mat lh_vals[steps + 1];
  //lh_vals[0] = m_h_tm1;
  //mat lh_deriv_vals[steps];
  //mat ly_vals[steps];
  //mat x_vals[steps];
  //mat x_mask_vals[steps];

  //mat delta_y_vals[steps];
  mat delta_h_vals[steps];
  mat delta_x_vals[steps];

  //mat all_output_vals_feasible[steps];
  //vector<size_t> gold_labels_ind_among_feasible;

  //  for (size_t i = 0; i < steps; ++i) {
  //    pair<mat,  mat> vals =
  //        calc_lh_proj_emb(i, context_batch[i], x_vals, lh_vals[i], x_mask_vals);
  //
  //    mat lh = vals.first;
  //    mat lh_deriv = vals.second;
  //
  //    ly_vals[i] = lh*m_wy; // no softmax here anymore
  //    lh_vals[i + 1] = lh;
  //    lh_deriv_vals[i] = lh_deriv;
  //  }

  // dim of ly is 1xn, set the dim of delta_y
  // to nx1 without doing transpose later
  //  for (size_t i = 0; i < steps; ++i) {
  //
  //    delta_y_vals[i] = zeros<mat>(ly_vals[i].n_cols, 1);
  //
  //    const vector<size_t> &feasible_y_vals = feasible_y_vals_batch[i];
  //    mat output_vals_feasible(1, feasible_y_vals.size(), fill::zeros);
  //
  //    bool found_gold = false;
  //    for (size_t j = 0; j < feasible_y_vals.size(); ++j) {
  //
  //      output_vals_feasible(j) = ly_vals[i](feasible_y_vals[j]);
  //
  //      if (feasible_y_vals[j] == label_batch[i]) {
  //        found_gold = true;
  //        gold_labels_ind_among_feasible.push_back(j);
  //      }
  //    }
  //    assert(found_gold);
  //
  //    // softmax over feasible actions
  //    output_vals_feasible = exp(output_vals_feasible)
  //                           / accu(exp(output_vals_feasible));
  //    all_output_vals_feasible[i] = output_vals_feasible;
  //
  //    // each delta_y_vals have the same number of elements as ly
  //    // the infeasible units will just get zero values
  //    for (size_t j = 0; j < feasible_y_vals.size(); ++j) {
  //      delta_y_vals[i](feasible_y_vals[j]) = output_vals_feasible(j);
  //    }
  //  }

  //assert(gold_labels_ind_among_feasible.size() == steps);

  //  for (size_t i = 0; i < steps; ++i) {
  //    mat gold_dist(1, feasible_y_vals_batch[i].size(), fill::zeros);
  //    gold_dist(gold_labels_ind_among_feasible[i]) = 1.0;
  //    err += x_ent_multi(all_output_vals_feasible[i], gold_dist);
  //    delta_y_vals[i](label_batch[i]) -= 1.0;
  //  }

  // bptt multi
  delta_h_vals[steps - 1] = lh_deriv_vals[steps - 1].t()
      % (m_wy * delta_y_vals[steps - 1]);

  for (size_t i = steps; i > 1; --i) {
    delta_h_vals[i - 2] = lh_deriv_vals[i - 2].t() %
        ((m_wh*delta_h_vals[i - 1]) + m_wy*(delta_y_vals[i - 2]));
  }

  //double back_grad_wh = 0.0;
  for (size_t i = 0; i < steps; ++i) {
    mat grad = lh_vals[i].t()*delta_h_vals[i].t();
    grad_wh += grad;
    //if (negative) m_wh -= m_lr*grad;
    //else m_wh += m_lr*grad;
    //back_grad_wh += grad(0,0);
  }

  //double back_grad_wy = 0.0;
  for (size_t i = 1; i <= steps; ++i) {
    mat grad = lh_vals[i].t()*delta_y_vals[i - 1].t();
    grad_wy += grad;
    //if (negative) m_wy -= m_lr*grad;
    //else m_wy += m_lr*grad;
    //back_grad_wy += grad(0,0);
  }

  for (size_t i = 0; i < steps; ++i) {
    if (cfg.use_dropout())
      delta_x_vals[i] = (m_wx*delta_h_vals[i]) % x_mask_vals[i];
    else
      delta_x_vals[i] = (m_wx*delta_h_vals[i]);
  }

  //double back_grad_wx = 0.0;
  for (size_t i = 0; i < steps; ++i) {
    mat grad = x_vals[i]*delta_h_vals[i].t();
    grad_wx += grad;
    //if (negative) m_wx -= m_lr*grad;
    //else m_wx += m_lr*grad;
    //back_grad_wx += grad(0,0);
  }

  // grad check
  //double bp_grad_emb = 0.0;
  //double bp_grad_suf = 0.0;
  //  double bp_grad_cap = 0.0;
  //double bp_grad_super = 0.0;

  //  mat grad_comp_mat;
  //  grad_comp_mat.copy_size(m_comp_mat);
  //  grad_comp_mat.zeros();

  for (size_t i = 0; i < steps; ++i) {
    const vector<size_t> &word_inds = context_batch[i][0];
    const vector<size_t> &suf_inds = context_batch[i][1];

    assert(word_inds.size() == suf_inds.size());

    // all start inds must be set here
    int start = 0;
    int start2 = m_de*word_inds.size(); // dim of word emb x num of embs
    int start3 = start2 + m_ds*word_inds.size();
    //    int start4 = 0;

    for (size_t k = 0; k < word_inds.size(); ++k) {
      if (negative) m_emb.col(word_inds[k]) -=
          m_lr*delta_x_vals[i].rows(start, start + m_de - 1);
      else m_emb.col(word_inds[k]) +=
          m_lr*delta_x_vals[i].rows(start, start + m_de - 1);
      //if (word_inds[k] == 40235)
      //bp_grad_emb += delta_x_vals[i].rows(start, start + m_de - 1)[0];
      if (negative) m_suf.col(suf_inds[k]) -=
          m_lr*delta_x_vals[i].rows(start2, start2 + m_ds - 1);
      else m_suf.col(suf_inds[k]) +=
          m_lr*delta_x_vals[i].rows(start2, start2 + m_ds - 1);
      //if (suf_inds[k] == 1)
      //bp_grad_suf += delta_x_vals[i].rows(start2, start2 + m_ds - 1)[0];
      //      m_cap.col(cap_inds[k]) -=
      //          m_lr*delta_x_vals[i].rows(start3, start3 + m_dc - 1);
      //      if (cap_inds[k]  == 0)
      //        bp_grad_cap += delta_x_vals[i].rows(start3, start3 + m_dc - 1)[0];

      start += m_de;
      start2 += m_ds;
    }

    if (m_use_supercat_feats) {
      assert(context_batch[i].size() >= 3);
      assert(start2 == start3);

      const vector<size_t> &supercat_inds = context_batch[i][2];
      //start4 = start3 + m_dsuper*supercat_inds.size();

      for (size_t k = 0; k < supercat_inds.size(); ++k) {
        if (negative) m_supercat_emb.col(supercat_inds[k]) -=
            m_lr*delta_x_vals[i].rows(start3, start3 + m_dsuper - 1);
        else m_supercat_emb.col(supercat_inds[k]) +=
            m_lr*delta_x_vals[i].rows(start3, start3 + m_dsuper - 1);
        // if (supercat_inds[k] == 3)
        //bp_grad_super += delta_x_vals[i].rows(start3, start3 + m_dsuper - 1)[0];

        start3 += m_dsuper;
      }
    }

    //    if (cfg.use_phrase_emb_feats()) {
    //      assert(context_batch[i].size() >= 5);
    //      assert(context_batch[i][3].size() == m_phrase_emb_feat_count); // phrase emb from s0, s1, s2, s3
    //      assert(context_batch[i][4].size() == 2*m_phrase_emb_feat_count);
    //      assert(start4 + m_phrase_emb_feat_count*m_de == delta_x_vals[i].n_rows);
    //      const vector<size_t> &phrase_inds = context_batch[i][3];
    //      const vector<size_t> &child_phrase_inds = context_batch[i][4];
    //
    //      // for each step, there are m_phrase_emb_feat_count number of phrases
    //      // and each should have two child phrases
    //      size_t j = 0;
    //      for (size_t k = 0; k < m_phrase_emb_feat_count; ++k) {
    //
    //        // lex/unary
    //        if (child_phrase_inds[j] == null_child_word_vec_ind) {
    //          assert(child_phrase_inds[j+1] == null_child_word_vec_ind);
    //          j += 2;
    //          start4 += m_de;
    //          continue;
    //        }
    //
    //        // binary
    //        assert(child_phrase_inds[j+1] != null_child_word_vec_ind);
    //        assert(child_phrase_inds[j] != m_phrase_emb_ind_base); // either < or >
    //        assert(child_phrase_inds[j+1] != m_phrase_emb_ind_base); // either < or >
    //
    //        mat one = ones<mat>(1, m_de);
    //        const size_t real_phrase_ind = (phrase_inds[k] > m_phrase_emb_ind_base) ? (phrase_inds[k] - m_phrase_emb_ind_base) : phrase_inds[k];
    //        mat delta_phrase_output = (m_phrase_mat.row(real_phrase_ind) % (one - m_phrase_mat.row(real_phrase_ind))) // 1x50
    //                                    % (delta_x_vals[i].rows(start4, start4 + m_de - 1).t()); // 1x50
    //        mat delta_combined_phrase_input = m_comp_mat*(delta_phrase_output.t()); // 100x1
    //
    //        if ((child_phrase_inds[j] > m_phrase_emb_ind_base) && (child_phrase_inds[j+1] > m_phrase_emb_ind_base)) {
    //          grad_comp_mat += join_rows(m_phrase_mat.row(child_phrase_inds[j] - m_phrase_emb_ind_base),
    //                                     m_phrase_mat.row(child_phrase_inds[j+1] - m_phrase_emb_ind_base)).t()
    //                            *(delta_phrase_output);
    //
    //        } else if ((child_phrase_inds[j] < m_phrase_emb_ind_base) && (child_phrase_inds[j+1] > m_phrase_emb_ind_base)) {
    //          grad_comp_mat += join_cols(m_emb.col(child_phrase_inds[j]), m_phrase_mat.row(child_phrase_inds[j+1] - m_phrase_emb_ind_base).t())
    //                             *(delta_phrase_output);
    //          m_emb.col(child_phrase_inds[j]) -= m_lr*(delta_combined_phrase_input.rows(0, m_de - 1));
    //
    //        } else if ((child_phrase_inds[j] > m_phrase_emb_ind_base) && (child_phrase_inds[j+1] < m_phrase_emb_ind_base)) {
    //          grad_comp_mat += join_cols(m_phrase_mat.row(child_phrase_inds[j] - m_phrase_emb_ind_base).t(), m_emb.col(child_phrase_inds[j+1]))
    //                             *(delta_phrase_output);
    //          m_emb.col(child_phrase_inds[j+1]) -= m_lr*(delta_combined_phrase_input.rows(m_de, 2*m_de - 1));
    //
    //        } else {
    //          grad_comp_mat += join_cols(m_emb.col(child_phrase_inds[j]), m_emb.col(child_phrase_inds[j+1]))*(delta_phrase_output);
    //          m_emb.col(child_phrase_inds[j]) -= m_lr*(delta_combined_phrase_input.rows(0, m_de - 1));
    //          m_emb.col(child_phrase_inds[j+1]) -= m_lr*(delta_combined_phrase_input.rows(m_de, 2*m_de - 1));
    //        }
    //
    //        j += 2;
    //        start4 += m_de;
    //      } // end for
    //    } // end if

    //    if (cfg.use_dep_feats()) {
    //
    //      assert(context_batch[i].size() >= 4);
    //      assert(context_batch[i][3].size() == 2);
    //      assert(start4 + 2*m_dc - 1 == delta_x_vals[i].n_rows - 1);
    //
    //      for (size_t k = 0; k < 2; ++k) {
    //        const size_t dist_col = context_batch[i][3][k];
    //
    //        m_dep_dist_mat.col(dist_col) -=
    //            m_lr*delta_x_vals[i].rows(start4, start4 + m_dc - 1);
    //
    //        start4 += m_dc;
    //      }
    //
    //    }

  } // end for all steps

  //  if (cfg.use_phrase_emb_feats())
  //    m_comp_mat -= m_lr*grad_comp_mat;

  //grad check
  //  cerr << "backprop grad wx: " << back_grad_wx << endl;
  //  cerr << "backprop grad wh: " << back_grad_wh << endl;
  //  cerr << "backprop grad wy: " << back_grad_wy << endl;
  //
  //  cerr << "backprop grad emb: " << bp_grad_emb << endl;
  //  cerr << "backprop grad suf: " << bp_grad_suf << endl;
  ////  cerr << "backprop grad cap: " << bp_grad_cap << endl;
  //  cerr << "backprop grad super: " << bp_grad_super << endl;

  //m_h_tm1 = lh_vals[steps];
  // return err;
}


const ShiftReduceHypothesis*
RNNParser::_Impl::sr_parse_beam_search(const size_t k, const size_t sent_id,
                                       const bool oracle, const bool train) {
  try {
    std::priority_queue<ShiftReduceHypothesis*, vector<ShiftReduceHypothesis*>, Hypo_comp> kbest_output_queue;
    m_total_reduce_count = 0; // reset total reduce count
    m_h_tm1.zeros();   // reset hidden states
    const ShiftReduceHypothesis *best_hypo; // the highest-scored hypo on each level
    //    const ShiftReduceHypothesis *gold_hypo;
    //    vector<ShiftReduceAction*> &gold_tree_actions = *m_goldTreeVec.at(sent_id);
    //    vector<pair<size_t, size_t> > &gold_tree_actions_rnn_fmt = m_gold_tree_vec_rnn[sent_id];
    //    size_t gold_tree_size = gold_tree_actions.size();
    //    assert(gold_tree_actions.size() == gold_tree_actions_rnn_fmt.size());
    //    ShiftReduceAction *gold_action = 0;
    //    pair<size_t, size_t> gold_action_rnn_fmt;
    //size_t gold_action_id = 0;
    bool gold_finished = false;

    if(sent.words.size() > cfg.maxwords())
      throw NLP::ParseError("sentence length exceeds maximum number of words for parser", nsentences);

    const size_t NWORDS = sent.words.size();
    //assert(NWORDS == sent_rnn_fmt.words.size());

    const ShiftReduceHypothesis *candidate_output = 0;
    //    Words words;
    //    raws2words(sent.words, words);
    //    Words tags;
    //    raws2words(sent.pos, tags);

    // inds from the input sentence
    vector<size_t> word_inds_vec;
    vector<size_t> suf_inds_vec;

    words2word_inds(sent.words, sent.pos, word_inds_vec, suf_inds_vec);
    //    if (cfg.use_pos())
    //      pos2pos_inds(sent.pos, suf_inds_vec);
    //    else
    //      suf2suf_inds(sent_rnn_fmt.pos, suf_inds_vec);
    assert(word_inds_vec.size() == suf_inds_vec.size());

    nsentences++;
    SuperCat::nsupercats = 0;

    //const MultiRaws &input = sent.msuper;

    m_lattice = new ShiftReduceLattice(NWORDS, m_beamSize, MAX_UNARY_ACTIONS);
    ShiftReduceHypothesis *start_hypo = new ShiftReduceHypothesis(0, 0, 0, 0, 0, 0, 0.0, false, 0, 0, 0, 0, 0.0, 0);
    start_hypo->SetStartHypoFlag();
    m_lattice->Add(0, start_hypo);
    start_hypo->m_hidden_states = m_h_tm1; // gets zero vec
    //gold_hypo = m_lattice->GetLatticeArray(0); // gold == start
    size_t startIndex;

    cerr << "sent: " << sent_id << endl;

    // all context, target, and feasible target values
    // for this sentence
    //vector<vector<vector<size_t> > > x_vals_sent;
    //vector<size_t> y_vals_sent;
    vector<vector<size_t> > feasible_y_vals_sent;

    // inds of the current context
    vector<size_t> word_inds_ret;
    vector<size_t> suf_inds_ret;
    vector<size_t> supercat_inds_ret;
    vector<size_t> phrase_emb_ind_ret;
    vector<size_t> child_phrase_emb_ind_ret; // dummy, won't be used at test time

    // set start and end indexes of the lattice
    while (m_lattice->GetEndIndex() == 0 ||
        m_lattice->GetEndIndex() > m_lattice->GetPrevEndIndex()) {
      if (m_lattice->GetEndIndex() != 0)
        startIndex = m_lattice->GetPrevEndIndex() + 1;
      else
        startIndex = 0;
      m_lattice->SetPrevEndIndex(m_lattice->GetEndIndex());

      //      if (gold_action_id < gold_tree_size)
      //      {
      //        gold_action = gold_tree_actions[gold_action_id];
      //        assert(gold_action);
      //        //cerr << "gold: " << gold_action->GetAction() << " "
      //        //<< gold_action->GetCat()->out_novar_noX_noNB_SR_str() << endl;
      //        gold_action_rnn_fmt = gold_tree_actions_rnn_fmt[gold_action_id];
      //        assert(gold_action_rnn_fmt.first == gold_action->GetAction());
      //      }

      HypothesisPQueue hypoQueue;
      for (size_t i = startIndex; i <= m_lattice->GetEndIndex(); ++i)
      {
        //assert(m_lattice->GetEndIndex() - startIndex == 0);
        ShiftReduceHypothesis *hypo = const_cast<ShiftReduceHypothesis *>(m_lattice->GetLatticeArray(i));
        size_t j = hypo->GetNextInputIndex();

        //        if (hypo->IsFinished(NWORDS, m_allowFragTree)) {
        //          if (candidate_output == 0 || (hypo->GetTotalScore() / hypo->GetTotalActionCount())
        //               > (candidate_output->GetTotalScore() / candidate_output->GetTotalActionCount())) {
        //            candidate_output = hypo;
        //          }
        //          kbest_output_queue.push(hypo);
        //        }

        vector<vector<size_t> > context_vec;
        word_inds_ret.clear();
        suf_inds_ret.clear();
        supercat_inds_ret.clear();
        phrase_emb_ind_ret.clear();
        context_vec.reserve(4); //todo remove hardcoded 4
        word_inds_ret.reserve(m_feature_count);
        suf_inds_ret.reserve(m_feature_count);
        supercat_inds_ret.reserve(m_supercat_feature_count);
        //child_phrase_emb_ind_ret.clear();

        ContextLoader context_loader(m_cat_ind, m_head_limit, false);
        context_loader.load_context(hypo, word_inds_ret,
                                    suf_inds_ret, supercat_inds_ret,
                                    phrase_emb_ind_ret, child_phrase_emb_ind_ret,
                                    word_inds_vec, suf_inds_vec,
                                    m_cats_map, cats, cfg.use_phrase_emb_feats(), false);

        assert(word_inds_ret.size() == suf_inds_ret.size());
        assert(word_inds_ret.size() == m_feature_count);
        assert(supercat_inds_ret.size() == m_supercat_feature_count);

        context_vec.emplace_back(word_inds_ret);
        context_vec.emplace_back(suf_inds_ret);
        context_vec.emplace_back(supercat_inds_ret);
        context_vec.emplace_back(phrase_emb_ind_ret);
        //x_vals_sent.push_back(context_vec);

        // precomputation related
        if (cfg.precompute()) {
          for (size_t w_i = 0; w_i < word_inds_ret.size(); ++w_i) {
            // for each word position
            auto iter = m_word_feat_emb_ind_map.find(w_i);
            if (iter == m_word_feat_emb_ind_map.end()) {
              m_word_feat_emb_ind_map.insert({w_i, unordered_map<size_t, size_t>({{word_inds_ret[w_i], 1}})});
            } else {
              // find this emb ind
              auto iter2 = iter->second.find(word_inds_ret[w_i]);

              if (iter2 == iter->second.end()) {
                assert(iter->second.insert({word_inds_ret[w_i], 1}).second);
              } else {
                iter2->second += 1;
              }
            }

            // for each suf position
            auto iter3 = m_suf_feat_emb_ind_map.find(w_i);
            if (iter3 == m_suf_feat_emb_ind_map.end()) {
              m_suf_feat_emb_ind_map.insert({w_i, unordered_map<size_t, size_t>({{suf_inds_ret[w_i], 1}})});
            } else {
              // find this emb ind
              auto iter4 = iter3->second.find(suf_inds_ret[w_i]);

              if (iter4 == iter3->second.end()) {
                assert(iter3->second.insert({suf_inds_ret[w_i], 1}).second);
              } else {
                iter4->second += 1;
              }
            }
          }
        }

        //bool found_gold_action = false;
        //size_t gold_action_class = 0;

        // feasible target values for the current context
        // the map is a check to make sure all the feasible
        // target values for the current context are unique
        // feasible_y_vals_map: key: output class (action id), value: (S/R/U, result cat)
        vector<size_t> feasible_y_vals;
        unordered_map<size_t, pair<size_t, SuperCat*> > feasible_y_vals_map;

        // find all feasible actions,
        // but do not push them into the queue

        // shift
        if (j < NWORDS) {

          const MultiRaw &multi = sent.msuper[j];
          for (MultiRaw::const_iterator k = multi.begin(); k!= multi.end(); ++k) {
            const Cat *cat = cats.markedup[k->raw];

            if(!cat) {
              cerr << "SHIFT action error: attempted to load category without markedup "
                  << k->raw << endl;
              exit (EXIT_FAILURE);
            }

            assert(m_lex_cat_ind_map.find(k->raw) != m_lex_cat_ind_map.end());
            feasible_y_vals.push_back(m_lex_cat_ind_map[k->raw]);
            SuperCat *stack_top_cat = SuperCat::Lexical(chart.pool, j+1, cat, 0);
            assert(feasible_y_vals_map.insert(make_pair(m_lex_cat_ind_map[k->raw],
                                                        make_pair(0, stack_top_cat))).second);
          } // end for
        } // end if

        // unary
        if (hypo->GetStackTopSuperCat() != 0) {

          const SuperCat *sc = hypo->GetStackTopSuperCat();
          vector<SuperCat *> tmp;
          tmp.clear();

          if (!sc->IsLex() && !sc->IsTr()) {
            unary_tc(hypo->GetStackTopSuperCat(), false, tmp);
            unary_tr(sc, tmp);
          }
          else if (sc->IsLex() && !sc->IsTr()) {
            unary_tr(sc, tmp);
          }
          else if (!sc->IsLex() && sc->IsTr()) { /* do nothing */ }
          else if (sc->IsLex() && sc->IsTr()) { /* do nothing */ }

          for (vector<SuperCat *>::iterator j = tmp.begin(); j < tmp.end(); ++j) {
            SuperCat *stack_top_cat = *j;
            assert(stack_top_cat->left);
            feasible_y_vals.push_back(stack_top_cat->m_unary_id + m_unary_base_count);
            assert(feasible_y_vals_map.insert(make_pair(stack_top_cat->m_unary_id + m_unary_base_count,
                                                        make_pair(1, stack_top_cat))).second);
          }
        }

        // reduce
        if (hypo->GetPrvStack() != 0 && hypo->GetPrvStack()->GetStackTopSuperCat() != 0
            && hypo->GetStackTopSuperCat() != 0) {

          results.clear();
          if (!cfg.seen_rules() ||
              rule_instances(hypo->GetPrvStack()->GetStackTopSuperCat()->GetCat(), hypo->GetStackTopSuperCat()->GetCat()))
            rules(hypo->GetPrvStack()->GetStackTopSuperCat(), hypo->GetStackTopSuperCat(),
                  cfg.eisner_nf(), cfg.seen_rules(), cfg.question_rules(), results);

          if (results.size() > 0) {
            for (vector<SuperCat *>::iterator k = results.begin(); k < results.end(); ++k) {

              SuperCat *stack_top_cat = *k;

              if (stack_top_cat->m_reduce_id == 0) {
                feasible_y_vals.push_back(m_reduce_base_count + 1);
                assert(feasible_y_vals_map.insert(make_pair(m_reduce_base_count + 1,
                                                            make_pair(2, stack_top_cat))).second);
              } else {
                feasible_y_vals.push_back(m_reduce_base_count + stack_top_cat->m_reduce_id + 1);
                assert(feasible_y_vals_map.insert(make_pair(m_reduce_base_count + stack_top_cat->m_reduce_id + 1,
                                                            make_pair(2, stack_top_cat))).second);
              }
            }
          }
        }

        if (feasible_y_vals.size() > 0) {
          // has feasible actions, then
          // score current context

          mat new_hidden_states;
          mat output_vals_feasible(1, feasible_y_vals.size(), fill::zeros);
          const mat &prev_hidden_stats = hypo->IsStartHypo() ? m_h_tm1 : hypo->GetPrevHypo()->m_hidden_states;
          score_context_beam_search(context_vec, feasible_y_vals, output_vals_feasible,
                                    prev_hidden_stats, hypo->m_hidden_states);
          output_vals_feasible = log(output_vals_feasible);

          double total_score = 0.0;
          unordered_map<size_t, pair<size_t, SuperCat*> >::const_iterator action_iter;

          for (size_t action_ind = 0; action_ind < feasible_y_vals.size(); ++action_ind) {

            // this is currently a naive implementation
            // ideally, we don't have to expand every
            // feasible action into the beam

            action_iter = feasible_y_vals_map.find(feasible_y_vals[action_ind]);
            assert(action_iter != feasible_y_vals_map.end());

            pair<size_t, SuperCat*> feasible_action = action_iter->second;
            total_score = hypo->GetTotalScore() + output_vals_feasible(action_ind);
            //debug
            //cerr << "total score: " << total_score << endl;
            //total_score /= (double) (hypo->GetTotalActionCount() + 1);

            // do the action
            // shift
            if (feasible_action.first == SHIFT) {
              assert(j < NWORDS);

              assert(feasible_action.second->cat);
              //feasible_action.second->m_word_vec_ind = word_inds_vec[j];
              ShiftReduceAction *action_shift = new ShiftReduceAction(SHIFT, feasible_action.second);
              HypothesisTuple *new_hypo_tuple = new HypothesisTuple(hypo, action_shift, total_score, i);
              hypoQueue.push(new_hypo_tuple);

              // unary
            } else if (feasible_action.first == UNARY) {

              assert(feasible_action.second->left);
              //assert(hypo->GetStackTopSuperCat()->m_word_vec_ind != -1);
              feasible_action.second->m_word_vec_ind = hypo->GetStackTopSuperCat()->m_word_vec_ind;
              ShiftReduceAction *action_unary = new ShiftReduceAction(UNARY, feasible_action.second);
              HypothesisTuple *new_hypo_tuple = new HypothesisTuple(hypo, action_unary, total_score, i);
              hypoQueue.push(new_hypo_tuple);

              // reduce
            } else if (feasible_action.first == COMBINE) {

              assert(hypo->GetPrvStack() != 0 && hypo->GetPrvStack()->GetStackTopSuperCat() != 0
                     && hypo->GetStackTopSuperCat() != 0);

              assert(feasible_action.second);
              ++m_total_reduce_count;
              //compose_word_vec(hypo->GetPrvStack()->GetStackTopSuperCat()->m_word_vec_ind,
              //                 hypo->GetStackTopSuperCat()->m_word_vec_ind, feasible_action.second);
              ShiftReduceAction *action_combine = new ShiftReduceAction(COMBINE, feasible_action.second);
              HypothesisTuple *new_hypo_tuple = new HypothesisTuple(hypo, action_combine, total_score, i);
              hypoQueue.push(new_hypo_tuple);

            } else {
              cerr << "best action not recognized...\n";
              exit (EXIT_FAILURE);
            }
          }

        } else {

          // no more feasible actions, candidate output
          kbest_output_queue.push(hypo);

          if (candidate_output == 0 ||
              (hypo->GetTotalScore() / hypo->GetTotalActionCount()) >
          (candidate_output->GetTotalScore() / candidate_output->GetTotalActionCount())) {
            candidate_output = hypo;
          }
        }
      } // end for

      if (!hypoQueue.empty()) {
        best_hypo = 0;
        const ShiftReduceHypothesis *gold_hypo = 0;
        ShiftReduceAction *gold_action = 0;
        // apply beam
        construct_hypo(hypoQueue, m_lattice, best_hypo, gold_hypo, gold_action, gold_finished);
      }

    } // end while

    if (oracle) {
      double top_f1 = calc_f1(candidate_output, sent_id, train);
      cerr << "top output score: " << candidate_output->GetTotalScore() << " f1: " << top_f1 << endl;

      while (!kbest_output_queue.empty()) {
        const ShiftReduceHypothesis* hypo = kbest_output_queue.top();
        double f1 = calc_f1(hypo, sent_id, train);
        cerr << "score: " << hypo->GetTotalScore() << " f1: " << f1 << endl;
        if (f1 > top_f1) {
          candidate_output = hypo;
          top_f1 = f1;
        }
        kbest_output_queue.pop();
      }
    }

    if (candidate_output != 0)
      return candidate_output;

    return 0;
  } catch(NLP::Exception e) {
    //throw NLP::ParseError(e.msg, nsentences);
    cerr << e.msg << endl;
    return 0;
  }
}


void
RNNParser::_Impl::save_weights(const size_t i, const string &model_path) {
  m_wx.save(model_path + "/wx." + to_string(i) + ".mat");
  m_wh.save(model_path + "/wh." + to_string(i) + ".mat");
  m_wy.save(model_path + "/wy." + to_string(i) + ".mat");
  m_suf.save(model_path + "/suf." + to_string(i) + ".mat");
  //m_cap.save(model_path + "cap." + to_string(i) + ".mat");
  m_emb.save(model_path + "/emb." + to_string(i) + ".mat");

  if (m_use_supercat_feats)
    m_supercat_emb.save(model_path + "/super_emb." + to_string(i) + ".mat");

  if (cfg.use_phrase_emb_feats()) {
    m_comp_mat.save(model_path + "/comp_mat." + to_string(i) + ".mat");
    m_phrase_mat.save(model_path + "/phrase_mat." + to_string(i) + ".mat");
  }

  dump_supercat_map(model_path + "/supercat_map." + to_string(i));
}

//pair<size_t, double>
//RNNParser::_Impl::score_context(const vector<vector<size_t> > &context,
//                                const vector<size_t> &feasible_y_vals) {
//
//  //cerr << "context size: " << context.size() << endl;
//  //cerr << "feasible_y_vals size: " << feasible_y_vals.size() << endl;
//
//  // feasible_y_vals contains inds of feasible output classes
//
//  calc_lh_proj_emb_test(context);
//  mat ly_vals = m_h_tm1*m_wy; // no softmax here anymore
//
//  //ly_vals.print("ly_vals");
//
//  mat output_vals_feasible(1, feasible_y_vals.size(), fill::zeros);
//
//  // get the raw scores of feasible actions
//  for (size_t j = 0; j < feasible_y_vals.size(); ++j) {
//    output_vals_feasible(j) = ly_vals(feasible_y_vals[j]);
//  }
//
//  // softmax over feasible actions
//  output_vals_feasible = exp(output_vals_feasible)
//                         / accu(exp(output_vals_feasible));
//
//  //output_vals_feasible.print("output_vals");
//
//  uword max_ind;
//  output_vals_feasible.max(max_ind);
//
//  return make_pair(feasible_y_vals[max_ind], output_vals_feasible.max());
//}


// return scores for all feasilbe actions,
// not just the max
void
RNNParser::_Impl::score_context_max_margin_no_softmax(const vector<vector<size_t> > &context,
                                                      mat &h_tm1,
                                                      mat &new_hidden_state,
                                                      colvec &converted_context,
                                                      colvec &mask_val,
                                                      mat &ly_val) {

  // feasible_y_vals contains inds of feasible output classes

  new_hidden_state = calc_lh_proj_emb_max_margin(context, h_tm1, converted_context, mask_val);
  ly_val = sigmoid(new_hidden_state*m_wy); // no softmax here anymore

  //  for (size_t j = 0; j < feasible_y_vals.size(); ++j) {
  //    output_vals_feasible(j) = ly_vals(feasible_y_vals[j]);
  //  }
  //
  //  // softmax over feasible actions
  //  output_vals_feasible = exp(output_vals_feasible)
  //                         / accu(exp(output_vals_feasible));

  //return output_vals_feasible;
}


// return scores for all feasilbe actions,
// not just the max
mat
RNNParser::_Impl::score_context_max_margin_softmax(const vector<vector<size_t> > &context,
                                                   const vector<size_t> &feasible_y_vals,
                                                   mat &output_vals_feasible,
                                                   mat &h_tm1,
                                                   mat &new_hidden_state,
                                                   colvec &converted_context,
                                                   colvec &mask_val) {

  // feasible_y_vals contains inds of feasible output classes

  new_hidden_state = calc_lh_proj_emb_max_margin(context, h_tm1, converted_context, mask_val);

  mat ly_vals = new_hidden_state*m_wy; // no softmax here anymore

  for (size_t j = 0; j < feasible_y_vals.size(); ++j) {
    output_vals_feasible(j) = ly_vals(feasible_y_vals[j]);
  }

  // softmax over feasible actions
//  output_vals_feasible = exp(output_vals_feasible)
//      / accu(exp(output_vals_feasible));
  output_vals_feasible = soft_max(output_vals_feasible);
  return output_vals_feasible;
}


// non max-margin version
// return scores for all feasilbe actions,
// not just the max
mat
RNNParser::_Impl::score_context_beam_search(const vector<vector<size_t> > &context,
                                            const vector<size_t> &feasible_y_vals,
                                            mat &output_vals_feasible,
                                            const mat &h_tm1,
                                            mat &new_hidden_states) {

  // feasible_y_vals contains inds of feasible output classes
  if (cfg.use_lh_cache()) {
    colvec temp;
    calc_lh_proj_emb_xf1_cached(context, h_tm1, new_hidden_states, temp, false);
  } else {
    calc_lh_proj_emb_test(context, h_tm1, new_hidden_states);
  }
  mat ly_vals = new_hidden_states*m_wy; // no softmax here anymore

  for (size_t j = 0; j < feasible_y_vals.size(); ++j) {
    output_vals_feasible(j) = ly_vals(feasible_y_vals[j]);
  }

  // softmax over feasible actions
//  output_vals_feasible = exp(output_vals_feasible)
//      / accu(exp(output_vals_feasible));
  output_vals_feasible = soft_max(output_vals_feasible);

  return output_vals_feasible;
}


void
RNNParser::_Impl::score_context_beam_search_xf1(const vector<vector<size_t>> &context,
                                                const vector<size_t> &feasible_y_vals,
                                                mat &output_vals_feasible,
                                                const mat &h_tm1,
                                                mat &new_hidden_states,
                                                colvec &converted_context_vec) {

  // feasible_y_vals contains inds of feasible output classes
  if (cfg.use_lh_cache())
    calc_lh_proj_emb_xf1_cached(context, h_tm1, new_hidden_states, converted_context_vec, true);
  else
    calc_lh_proj_emb_xf1(context, h_tm1, new_hidden_states, converted_context_vec);

  mat ly_vals = new_hidden_states*m_wy; // no softmax here anymore

  for (size_t j = 0; j < feasible_y_vals.size(); ++j) {
    output_vals_feasible(j) = ly_vals(feasible_y_vals[j]);
  }

  // softmax over feasible actions
//  output_vals_feasible = exp(output_vals_feasible)
//      / accu(exp(output_vals_feasible));
  output_vals_feasible = soft_max(output_vals_feasible);

}


void
RNNParser::_Impl::xf1_grad_check(NLP::Taggers::RnnTagger &rnn_super,
                                 const size_t k, const size_t sent_id,
                                 double &total_parses,
                                 const size_t num_threads) {
  double lambda = 0.00001;

  mat m_wx_cp = m_wx;
  mat m_wh_cp = m_wh;
  mat m_wy_cp = m_wy;
  mat m_emb_cp = m_emb;
  mat m_suf_cp = m_suf;
  mat m_supercat_emb_cp = m_supercat_emb;

  m_wx(12,12) += lambda;
  double xf1_plus = sr_parse_beam_search_train_xf1_grad_check(k, sent_id);
  clear_xf1();
  m_wx = m_wx_cp;

  m_wx(12,12) -= lambda;
  double xf1_minus = sr_parse_beam_search_train_xf1_grad_check(k, sent_id);
  double grad = (xf1_plus - xf1_minus) / (2.0*lambda);
  clear_xf1();
  cerr << "num_grad_wx: " << grad << endl;
  m_wx = m_wx_cp;

  m_wh(12,12) += lambda;
  xf1_plus = sr_parse_beam_search_train_xf1_grad_check(k, sent_id);
  clear_xf1();
  m_wh = m_wh_cp;

  m_wh(12,12) -= lambda;
  xf1_minus = sr_parse_beam_search_train_xf1_grad_check(k, sent_id);
  grad = (xf1_plus - xf1_minus) / (2.0*lambda);
  clear_xf1();
  m_wh = m_wh_cp;
  cerr << "num_grad_wh: " << grad << endl;

  m_wy(0,0) += lambda;
  xf1_plus = sr_parse_beam_search_train_xf1_grad_check(k, sent_id);
  clear_xf1();
  m_wy = m_wy_cp;

  m_wy(0,0) -= lambda;
  xf1_minus = sr_parse_beam_search_train_xf1_grad_check(k, sent_id);
  grad = (xf1_plus - xf1_minus) / (2.0*lambda);
  clear_xf1();
  m_wy = m_wy_cp;
  cerr << "num_grad_wy: " << grad << endl;

  m_emb(0,0) += lambda;
  xf1_plus = sr_parse_beam_search_train_xf1_grad_check(k, sent_id);
  clear_xf1();
  m_emb = m_emb_cp;

  m_emb(0,0) -= lambda;
  xf1_minus = sr_parse_beam_search_train_xf1_grad_check(k, sent_id);
  grad = (xf1_plus - xf1_minus) / (2.0*lambda);
  clear_xf1();
  m_emb = m_emb_cp;
  cerr << "num_grad_emb(0,0): " << grad << endl;

  m_emb(0,3) += lambda;
  xf1_plus = sr_parse_beam_search_train_xf1_grad_check(k, sent_id);
  clear_xf1();
  m_emb = m_emb_cp;

  m_emb(0,3) -= lambda;
  xf1_minus = sr_parse_beam_search_train_xf1_grad_check(k, sent_id);
  grad = (xf1_plus - xf1_minus) / (2.0*lambda);
  clear_xf1();
  m_emb = m_emb_cp;
  cerr << "num_grad_emb(0,3): " << grad << endl;

  m_suf(0,0) += lambda;
  xf1_plus = sr_parse_beam_search_train_xf1_grad_check(k, sent_id);
  clear_xf1();
  m_suf = m_suf_cp;

  m_suf(0,0) -= lambda;
  xf1_minus = sr_parse_beam_search_train_xf1_grad_check(k, sent_id);
  grad = (xf1_plus - xf1_minus) / (2.0*lambda);
  clear_xf1();
  m_suf = m_suf_cp;
  cerr << "num_grad_suf(0,0): " << grad << endl;

  m_suf(0,5) += lambda;
  xf1_plus = sr_parse_beam_search_train_xf1_grad_check(k, sent_id);
  clear_xf1();
  m_suf = m_suf_cp;

  m_suf(0,5) -= lambda;
  xf1_minus = sr_parse_beam_search_train_xf1_grad_check(k, sent_id);
  grad = (xf1_plus - xf1_minus) / (2.0*lambda);
  clear_xf1();
  m_suf = m_suf_cp;
  cerr << "num_grad_suf(0,5): " << grad << endl;

  m_supercat_emb(0,26) += lambda;
  xf1_plus = sr_parse_beam_search_train_xf1_grad_check(k, sent_id);
  clear_xf1();
  m_supercat_emb = m_supercat_emb_cp;

  m_supercat_emb(0,26) -= lambda;
  xf1_minus = sr_parse_beam_search_train_xf1_grad_check(k, sent_id);
  grad = (xf1_plus - xf1_minus) / (2.0*lambda);
  clear_xf1();
  m_supercat_emb = m_supercat_emb_cp;
  cerr << "num_grad_super(0,26): " << grad << endl;

  m_supercat_emb(0,9) += lambda;
  xf1_plus = sr_parse_beam_search_train_xf1_grad_check(k, sent_id);
  clear_xf1();
  m_supercat_emb = m_supercat_emb_cp;

  m_supercat_emb(0,9) -= lambda;
  xf1_minus = sr_parse_beam_search_train_xf1_grad_check(k, sent_id);
  grad = (xf1_plus - xf1_minus) / (2.0*lambda);
  clear_xf1();
  m_supercat_emb = m_supercat_emb_cp;
  cerr << "num_grad_super(0,9): " << grad << endl;


  double total_xf1 = 0.0;
  sr_parse_beam_search_train_xf1(rnn_super, k, sent_id, false, true, total_xf1, total_parses, num_threads, true, true);
}


void
RNNParser::_Impl::xf1_grad_check_super(NLP::Taggers::RnnTagger &rnn_super,
                                       const size_t k, const size_t sent_id,
                                       double &total_parses) {
  double lambda = 0.00001;

  mat m_wx_cp = rnn_super.m_wx;
  mat m_wh_cp = rnn_super.m_wh;
  mat m_wy_cp = rnn_super.m_wy;
  mat m_wx_back_cp = rnn_super.m_wx_back;
  mat m_wh_back_cp = rnn_super.m_wh_back;
  mat m_emb_cp = rnn_super.m_emb;
  mat m_suf_cp = rnn_super.m_suf;
  mat m_cap_cp = rnn_super.m_cap;

  // assume using training data as input (ie loads gold deps from training data)

  sent.all_ly_vals.clear();
  rnn_super.m_wx(12,12) += lambda;
  rnn_super.mtag_sent(sent, 0.00025);
  double xf1_plus = sr_parse_beam_search_train_xf1_grad_check_super(rnn_super, k, sent_id, false, true);
  clear_xf1();
  rnn_super.m_wx = m_wx_cp;

  sent.all_ly_vals.clear();
  rnn_super.m_wx(12,12) -= lambda;
  rnn_super.mtag_sent(sent, 0.00025);
  double xf1_minus = sr_parse_beam_search_train_xf1_grad_check_super(rnn_super, k, sent_id, false, true);
  double grad = (xf1_plus - xf1_minus) / (2.0*lambda);
  clear_xf1();
  cerr << "num_grad_wx: " << grad << endl;
  rnn_super.m_wx = m_wx_cp;

  sent.all_ly_vals.clear();
  rnn_super.m_wh(12,12) += lambda;
  rnn_super.mtag_sent(sent, 0.00025);
  // assume using training data as input (ie loads gold deps from training data)
  xf1_plus = sr_parse_beam_search_train_xf1_grad_check_super(rnn_super, k, sent_id, false, true);
  clear_xf1();
  rnn_super.m_wh = m_wh_cp;

  sent.all_ly_vals.clear();
  rnn_super.m_wh(12,12) -= lambda;
  rnn_super.mtag_sent(sent, 0.00025);
  xf1_minus = sr_parse_beam_search_train_xf1_grad_check_super(rnn_super, k, sent_id, false, true);
  grad = (xf1_plus - xf1_minus) / (2.0*lambda);
  clear_xf1();
  cerr << "num_grad_wh: " << grad << endl;
  rnn_super.m_wh = m_wh_cp;

  sent.all_ly_vals.clear();
  rnn_super.m_wh_back(12,12) += lambda;
  rnn_super.mtag_sent(sent, 0.00025);
  xf1_plus = sr_parse_beam_search_train_xf1_grad_check_super(rnn_super, k, sent_id, false, true);
  clear_xf1();
  rnn_super.m_wh_back = m_wh_back_cp;

  sent.all_ly_vals.clear();
  rnn_super.m_wh_back(12,12) -= lambda;
  rnn_super.mtag_sent(sent, 0.00025);
  xf1_minus = sr_parse_beam_search_train_xf1_grad_check_super(rnn_super, k, sent_id, false, true);
  grad = (xf1_plus - xf1_minus) / (2.0*lambda);
  clear_xf1();
  cerr << "num_grad_wh_back: " << grad << endl;
  rnn_super.m_wh_back = m_wh_back_cp;

  sent.all_ly_vals.clear();
  rnn_super.m_wx_back(12,12) += lambda;
  rnn_super.mtag_sent(sent, 0.00025);
  // assume using training data as input (ie loads gold deps from training data)
  xf1_plus = sr_parse_beam_search_train_xf1_grad_check_super(rnn_super, k, sent_id, false, true);
  clear_xf1();
  rnn_super.m_wx_back = m_wx_back_cp;

  sent.all_ly_vals.clear();
  rnn_super.m_wx_back(12,12) -= lambda;
  rnn_super.mtag_sent(sent, 0.00025);
  xf1_minus = sr_parse_beam_search_train_xf1_grad_check_super(rnn_super, k, sent_id, false, true);
  grad = (xf1_plus - xf1_minus) / (2.0*lambda);
  clear_xf1();
  cerr << "num_grad_wx_back: " << grad << endl;
  rnn_super.m_wx_back = m_wx_back_cp;

  sent.all_ly_vals.clear();
  rnn_super.m_wy(0,63) += lambda;
  rnn_super.mtag_sent(sent, 0.00025);
  // assume using training data as input (ie loads gold deps from training data)
  xf1_plus = sr_parse_beam_search_train_xf1_grad_check_super(rnn_super, k, sent_id, false, true);
  clear_xf1();
  rnn_super.m_wy = m_wy_cp;

  sent.all_ly_vals.clear();
  rnn_super.m_wy(0,63) -= lambda;
  rnn_super.mtag_sent(sent, 0.00025);
  xf1_minus = sr_parse_beam_search_train_xf1_grad_check_super(rnn_super, k, sent_id, false, true);
  grad = (xf1_plus - xf1_minus) / (2.0*lambda);
  clear_xf1();
  cerr << "num_grad_wy: " << grad << endl;
  rnn_super.m_wy = m_wy_cp;

  sent.all_ly_vals.clear();
  rnn_super.m_emb(0,0) += lambda;
  rnn_super.mtag_sent(sent, 0.00025);
  // assume using training data as input (ie loads gold deps from training data)
  xf1_plus = sr_parse_beam_search_train_xf1_grad_check_super(rnn_super, k, sent_id, false, true);
  clear_xf1();
  rnn_super.m_emb = m_emb_cp;

  sent.all_ly_vals.clear();
  rnn_super.m_emb(0,0) -= lambda;
  rnn_super.mtag_sent(sent, 0.00025);
  xf1_minus = sr_parse_beam_search_train_xf1_grad_check_super(rnn_super, k, sent_id, false, true);
  grad = (xf1_plus - xf1_minus) / (2.0*lambda);
  clear_xf1();
  cerr << "num_grad_emb(0,0): " << grad << endl;
  rnn_super.m_emb = m_emb_cp;

  sent.all_ly_vals.clear();
  rnn_super.m_emb(0,5) += lambda;
  rnn_super.mtag_sent(sent, 0.00025);
  // assume using training data as input (ie loads gold deps from training data)
  xf1_plus = sr_parse_beam_search_train_xf1_grad_check_super(rnn_super, k, sent_id, false, true);
  clear_xf1();
  rnn_super.m_emb = m_emb_cp;

  sent.all_ly_vals.clear();
  rnn_super.m_emb(0,5) -= lambda;
  rnn_super.mtag_sent(sent, 0.00025);
  xf1_minus = sr_parse_beam_search_train_xf1_grad_check_super(rnn_super, k, sent_id, false, true);
  grad = (xf1_plus - xf1_minus) / (2.0*lambda);
  clear_xf1();
  cerr << "num_grad_emb(0,5): " << grad << endl;
  rnn_super.m_emb = m_emb_cp;

  sent.all_ly_vals.clear();
  rnn_super.m_suf(0,0) += lambda;
  rnn_super.mtag_sent(sent, 0.00025);
  // assume using training data as input (ie loads gold deps from training data)
  xf1_plus = sr_parse_beam_search_train_xf1_grad_check_super(rnn_super, k, sent_id, false, true);
  clear_xf1();
  rnn_super.m_suf = m_suf_cp;

  sent.all_ly_vals.clear();
  rnn_super.m_suf(0,0) -= lambda;
  rnn_super.mtag_sent(sent, 0.00025);
  xf1_minus = sr_parse_beam_search_train_xf1_grad_check_super(rnn_super, k, sent_id, false, true);
  grad = (xf1_plus - xf1_minus) / (2.0*lambda);
  clear_xf1();
  cerr << "num_grad_suf(0,0): " << grad << endl;
  rnn_super.m_suf = m_suf_cp;

  sent.all_ly_vals.clear();
  rnn_super.m_suf(3,209) += lambda;
  rnn_super.mtag_sent(sent, 0.00025);
  // assume using training data as input (ie loads gold deps from training data)
  xf1_plus = sr_parse_beam_search_train_xf1_grad_check_super(rnn_super, k, sent_id, false, true);
  clear_xf1();
  rnn_super.m_suf = m_suf_cp;

  sent.all_ly_vals.clear();
  rnn_super.m_suf(3,209) -= lambda;
  rnn_super.mtag_sent(sent, 0.00025);
  xf1_minus = sr_parse_beam_search_train_xf1_grad_check_super(rnn_super, k, sent_id, false, true);
  grad = (xf1_plus - xf1_minus) / (2.0*lambda);
  clear_xf1();
  cerr << "num_grad_suf(3,209): " << grad << endl;
  rnn_super.m_suf = m_suf_cp;

  sent.all_ly_vals.clear();
  rnn_super.m_cap(0,0) += lambda;
  rnn_super.mtag_sent(sent, 0.00025);
  // assume using training data as input (ie loads gold deps from training data)
  xf1_plus = sr_parse_beam_search_train_xf1_grad_check_super(rnn_super, k, sent_id, false, true);
  clear_xf1();
  rnn_super.m_cap = m_cap_cp;

  sent.all_ly_vals.clear();
  rnn_super.m_cap(0,0) -= lambda;
  rnn_super.mtag_sent(sent, 0.00025);
  xf1_minus = sr_parse_beam_search_train_xf1_grad_check_super(rnn_super, k, sent_id, false, true);
  grad = (xf1_plus - xf1_minus) / (2.0*lambda);
  clear_xf1();
  cerr << "num_grad_cap(0,0): " << grad << endl;
  rnn_super.m_cap = m_cap_cp;

  sent.all_ly_vals.clear();
  rnn_super.m_cap(0,1) += lambda;
  rnn_super.mtag_sent(sent, 0.00025);
  // assume using training data as input (ie loads gold deps from training data)
  xf1_plus = sr_parse_beam_search_train_xf1_grad_check_super(rnn_super, k, sent_id, false, true);
  clear_xf1();
  rnn_super.m_cap = m_cap_cp;

  sent.all_ly_vals.clear();
  cerr << "sent.all_ly_vals size: " << sent.all_ly_vals.size() << endl;
  rnn_super.m_cap(0,1) -= lambda;
  rnn_super.mtag_sent(sent, 0.00025);
  xf1_minus = sr_parse_beam_search_train_xf1_grad_check_super(rnn_super, k, sent_id, false, true);
  grad = (xf1_plus - xf1_minus) / (2.0*lambda);
  clear_xf1();
  cerr << "num_grad_cap(0,1): " << grad << endl;
  rnn_super.m_cap = m_cap_cp;

  cerr << "sent.all_ly_vals size: " << sent.all_ly_vals.size() << endl;


  sent.all_ly_vals.clear();
  rnn_super.mtag_sent(sent, 0.00025);

  cerr << "sent.all_ly_vals size: " << sent.all_ly_vals.size() << endl;

  double total_xf1 = 0.0;
  cerr << "sent size: " << sent.words.size() << endl;
  sr_parse_beam_search_train_xf1(rnn_super, k, sent_id, false, true, total_xf1, total_parses, 1, false, true);
}



// assusmes using training data as input (ie loads training gold deps)
double
RNNParser::_Impl::sr_parse_beam_search_train_xf1_grad_check(const size_t k, const size_t sent_id) {
  try {

    // xf1 training related
    vector<double> seq_score_vec;
    vector<double> f1_vec;
    std::priority_queue<ShiftReduceHypothesis*, vector<ShiftReduceHypothesis*>, Hypo_comp> kbest_output_queue;

    m_total_reduce_count = 0; // reset total reduce count
    m_h_tm1.zeros();   // reset hidden states
    const ShiftReduceHypothesis *best_hypo; // the highest-scored hypo on each level
    //    const ShiftReduceHypothesis *gold_hypo;
    //    vector<ShiftReduceAction*> &gold_tree_actions = *m_goldTreeVec.at(sent_id);
    //    vector<pair<size_t, size_t> > &gold_tree_actions_rnn_fmt = m_gold_tree_vec_rnn[sent_id];
    //    size_t gold_tree_size = gold_tree_actions.size();
    //    assert(gold_tree_actions.size() == gold_tree_actions_rnn_fmt.size());
    //    ShiftReduceAction *gold_action = 0;
    //    pair<size_t, size_t> gold_action_rnn_fmt;
    //size_t gold_action_id = 0;
    bool gold_finished = false;

    if (sent.words.size() > cfg.maxwords())
      throw NLP::ParseError("sentence length exceeds maximum number of words for parser", nsentences);

    const size_t NWORDS = sent.words.size();
    //assert(NWORDS == sent_rnn_fmt.words.size());

    const ShiftReduceHypothesis *candidate_output = 0;
    //    Words words;
    //    raws2words(sent.words, words);
    //    Words tags;
    //    raws2words(sent.pos, tags);

    // inds from the input sentence
    vector<size_t> word_inds_vec;
    vector<size_t> suf_inds_vec;

    words2word_inds(sent.words, sent.pos, word_inds_vec, suf_inds_vec);
    //    if (cfg.use_pos())
    //      pos2pos_inds(sent.pos, suf_inds_vec);
    //    else
    //      suf2suf_inds(sent_rnn_fmt.pos, suf_inds_vec);
    assert(word_inds_vec.size() == suf_inds_vec.size());

    nsentences++;
    SuperCat::nsupercats = 0;

    //const MultiRaws &input = sent.msuper;

    m_lattice = new ShiftReduceLattice(NWORDS, m_beamSize, MAX_UNARY_ACTIONS);
    ShiftReduceHypothesis *start_hypo = new ShiftReduceHypothesis(0, 0, 0, 0, 0, 0, 0.0, false, 0, 0, 0, 0, 0.0, 0);
    start_hypo->SetStartHypoFlag();
    m_lattice->Add(0, start_hypo);
    //start_hypo->m_hidden_states = m_h_tm1; // gets zero vec
    //gold_hypo = m_lattice->GetLatticeArray(0); // gold == start
    size_t startIndex;

    cerr << "sent: " << sent_id << endl;

    // all context, target, and feasible target values
    // for this sentence
    //vector<vector<vector<size_t> > > x_vals_sent;
    //vector<size_t> y_vals_sent;
    vector<vector<size_t> > feasible_y_vals_sent;

    // inds of the current context
    vector<size_t> word_inds_ret;
    vector<size_t> suf_inds_ret;
    vector<size_t> supercat_inds_ret;
    vector<size_t> phrase_emb_ind_ret;
    vector<size_t> child_phrase_emb_ind_ret; // dummy, won't be used at test time

    // set start and end indexes of the lattice
    while (m_lattice->GetEndIndex() == 0 ||
        m_lattice->GetEndIndex() > m_lattice->GetPrevEndIndex())
    {
      if (m_lattice->GetEndIndex() != 0)
        startIndex = m_lattice->GetPrevEndIndex() + 1;
      else
        startIndex = 0;
      m_lattice->SetPrevEndIndex(m_lattice->GetEndIndex());

      //      if (gold_action_id < gold_tree_size)
      //      {
      //        gold_action = gold_tree_actions[gold_action_id];
      //        assert(gold_action);
      //        //cerr << "gold: " << gold_action->GetAction() << " "
      //        //<< gold_action->GetCat()->out_novar_noX_noNB_SR_str() << endl;
      //        gold_action_rnn_fmt = gold_tree_actions_rnn_fmt[gold_action_id];
      //        assert(gold_action_rnn_fmt.first == gold_action->GetAction());
      //      }

      HypothesisPQueue hypoQueue;
      for (size_t i = startIndex; i <= m_lattice->GetEndIndex(); ++i) {
        //assert(m_lattice->GetEndIndex() - startIndex == 0);
        ShiftReduceHypothesis *hypo = const_cast<ShiftReduceHypothesis *>(m_lattice->GetLatticeArray(i));
        size_t j = hypo->GetNextInputIndex();

        //        if (hypo->IsFinished(NWORDS, m_allowFragTree)) {
        //          if (candidate_output == 0 || (hypo->GetTotalScore() / hypo->GetTotalActionCount())
        //               > (candidate_output->GetTotalScore() / candidate_output->GetTotalActionCount())) {
        //            candidate_output = hypo;
        //          }
        //          kbest_output_queue.push(hypo);
        //        }

        vector<vector<size_t>> context_vec;
        word_inds_ret.clear();
        suf_inds_ret.clear();
        supercat_inds_ret.clear();
        phrase_emb_ind_ret.clear();
        //child_phrase_emb_ind_ret.clear();

        ContextLoader context_loader(m_cat_ind, m_head_limit, false);
        context_loader.load_context(hypo, word_inds_ret,
                                    suf_inds_ret, supercat_inds_ret,
                                    phrase_emb_ind_ret, child_phrase_emb_ind_ret,
                                    word_inds_vec, suf_inds_vec,
                                    m_cats_map, cats, cfg.use_phrase_emb_feats(), false);

        assert(word_inds_ret.size() == suf_inds_ret.size());
        assert(word_inds_ret.size() == m_feature_count);
        assert(supercat_inds_ret.size() == m_supercat_feature_count);

        context_vec.push_back(word_inds_ret);
        context_vec.push_back(suf_inds_ret);
        context_vec.push_back(supercat_inds_ret);
        context_vec.push_back(phrase_emb_ind_ret);
        //x_vals_sent.push_back(context_vec);

        //bool found_gold_action = false;
        //size_t gold_action_class = 0;

        // feasible target values for the current context
        // the map is a check to make sure all the feasible
        // target values for the current context are unique
        // feasible_y_vals_map: key: output class (action id), value: (S/R/U, result cat)
        vector<size_t> feasible_y_vals;
        unordered_map<size_t, pair<size_t, SuperCat*> > feasible_y_vals_map;

        // find all feasible actions,
        // but do not push them into the queue

        // shift
        if (j < NWORDS) {

          const MultiRaw &multi = sent.msuper[j];
          for (MultiRaw::const_iterator k = multi.begin(); k!= multi.end(); ++k) {
            const Cat *cat = cats.markedup[k->raw];

            if(!cat) {
              cerr << "SHIFT action error: attempted to load category without markedup "
                  << k->raw << endl;
              exit (EXIT_FAILURE);
            }

            assert(m_lex_cat_ind_map.find(k->raw) != m_lex_cat_ind_map.end());
            feasible_y_vals.push_back(m_lex_cat_ind_map[k->raw]);
            SuperCat *stack_top_cat = SuperCat::Lexical(chart.pool, j+1, cat, 0);
            assert(feasible_y_vals_map.insert(make_pair(m_lex_cat_ind_map[k->raw],
                                                        make_pair(0, stack_top_cat))).second);
          } // end for
        } // end if

        // unary
        if (hypo->GetStackTopSuperCat() != 0) {

          const SuperCat *sc = hypo->GetStackTopSuperCat();
          vector<SuperCat *> tmp;
          tmp.clear();

          if (!sc->IsLex() && !sc->IsTr()) {
            unary_tc(hypo->GetStackTopSuperCat(), false, tmp);
            unary_tr(sc, tmp);
          }
          else if (sc->IsLex() && !sc->IsTr()) {
            unary_tr(sc, tmp);
          }
          else if (!sc->IsLex() && sc->IsTr()) { /* do nothing */ }
          else if (sc->IsLex() && sc->IsTr()) { /* do nothing */ }

          for (vector<SuperCat *>::iterator j = tmp.begin(); j < tmp.end(); ++j) {
            SuperCat *stack_top_cat = *j;
            assert(stack_top_cat->left);
            feasible_y_vals.push_back(stack_top_cat->m_unary_id + m_unary_base_count);
            assert(feasible_y_vals_map.insert(make_pair(stack_top_cat->m_unary_id + m_unary_base_count,
                                                        make_pair(1, stack_top_cat))).second);
          }
        }

        // reduce
        if (hypo->GetPrvStack() != 0 && hypo->GetPrvStack()->GetStackTopSuperCat() != 0
            && hypo->GetStackTopSuperCat() != 0) {

          results.clear();
          if (!cfg.seen_rules() ||
              rule_instances(hypo->GetPrvStack()->GetStackTopSuperCat()->GetCat(), hypo->GetStackTopSuperCat()->GetCat()))
            rules(hypo->GetPrvStack()->GetStackTopSuperCat(), hypo->GetStackTopSuperCat(),
                  cfg.eisner_nf(), cfg.seen_rules(), cfg.question_rules(), results);

          if (results.size() > 0) {
            for (vector<SuperCat *>::iterator k = results.begin(); k < results.end(); ++k) {

              SuperCat *stack_top_cat = *k;

              if (stack_top_cat->m_reduce_id == 0) {
                feasible_y_vals.push_back(m_reduce_base_count + 1);
                assert(feasible_y_vals_map.insert(make_pair(m_reduce_base_count + 1,
                                                            make_pair(2, stack_top_cat))).second);
              } else {
                feasible_y_vals.push_back(m_reduce_base_count + stack_top_cat->m_reduce_id + 1);
                assert(feasible_y_vals_map.insert(make_pair(m_reduce_base_count + stack_top_cat->m_reduce_id + 1,
                                                            make_pair(2, stack_top_cat))).second);
              }
            }
          }
        }

        if (feasible_y_vals.size() > 0) {
          // has feasible actions, then
          // score current context

          mat new_hidden_states;
          mat output_vals_feasible(1, feasible_y_vals.size(), fill::zeros);
          const mat &prev_hidden_stats = hypo->IsStartHypo() ? m_h_tm1 : hypo->GetPrevHypo()->m_hidden_states;
          score_context_beam_search_xf1(context_vec, feasible_y_vals, output_vals_feasible,
                                        prev_hidden_stats, hypo->m_hidden_states, hypo->m_converted_context_vec);
          hypo->m_context_vec = context_vec;
          hypo->m_action_score_vec = output_vals_feasible; // softmax probabilities
          output_vals_feasible = log(output_vals_feasible); // log softmax probabilities
          vector<double> temp(feasible_y_vals.begin(), feasible_y_vals.end());
          hypo->m_feasible_action_ids = mat(temp);
          assert(output_vals_feasible.n_elem == feasible_y_vals.size());

          double total_score = 0.0;
          unordered_map<size_t, pair<size_t, SuperCat*> >::const_iterator action_iter;
          //size_t action_id = 0; // id of the output unit

          for (size_t action_ind = 0; action_ind < feasible_y_vals.size(); ++action_ind) {

            // this is currently a naive implementation
            // ideally, we don't have to expand every
            // feasible action into the beam

            action_iter = feasible_y_vals_map.find(feasible_y_vals[action_ind]);
            assert(action_iter != feasible_y_vals_map.end());

            pair<size_t, SuperCat*> feasible_action = action_iter->second;
            total_score = hypo->GetTotalScore() + output_vals_feasible(action_ind);
            //total_score /= (double) (hypo->GetTotalActionCount() + 1);

            // do the action
            // shift
            if (feasible_action.first == SHIFT) {
              assert(j < NWORDS);

              assert(feasible_action.second->cat);
              //feasible_action.second->m_word_vec_ind = word_inds_vec[j];
              ShiftReduceAction *action_shift = new ShiftReduceAction(SHIFT, feasible_action.second);
              HypothesisTuple *new_hypo_tuple = new HypothesisTuple(hypo, action_shift, total_score, i, feasible_y_vals[action_ind]);
              hypoQueue.push(new_hypo_tuple);

              // unary
            } else if (feasible_action.first == UNARY) {

              assert(feasible_action.second->left);
              //assert(hypo->GetStackTopSuperCat()->m_word_vec_ind != -1);
              //feasible_action.second->m_word_vec_ind = hypo->GetStackTopSuperCat()->m_word_vec_ind;
              ShiftReduceAction *action_unary = new ShiftReduceAction(UNARY, feasible_action.second);
              HypothesisTuple *new_hypo_tuple = new HypothesisTuple(hypo, action_unary, total_score, i, feasible_y_vals[action_ind]);
              hypoQueue.push(new_hypo_tuple);

              // reduce
            } else if (feasible_action.first == COMBINE) {

              assert(hypo->GetPrvStack() != 0 && hypo->GetPrvStack()->GetStackTopSuperCat() != 0
                     && hypo->GetStackTopSuperCat() != 0);

              assert(feasible_action.second);
              ++m_total_reduce_count;
              //              compose_word_vec(hypo->GetPrvStack()->GetStackTopSuperCat()->m_word_vec_ind,
              //                             hypo->GetStackTopSuperCat()->m_word_vec_ind, feasible_action.second);
              ShiftReduceAction *action_combine = new ShiftReduceAction(COMBINE, feasible_action.second);
              HypothesisTuple *new_hypo_tuple = new HypothesisTuple(hypo, action_combine, total_score, i, feasible_y_vals[action_ind]);
              hypoQueue.push(new_hypo_tuple);

            } else {
              cerr << "best action not recognized...\n";
              exit (EXIT_FAILURE);
            }
          }

        } else {

          // no more feasible actions, candidate output
          kbest_output_queue.push(hypo);

          if (candidate_output == 0 ||
              (hypo->GetTotalScore() / hypo->GetTotalActionCount()) >
          (candidate_output->GetTotalScore() / candidate_output->GetTotalActionCount())) {
            candidate_output = hypo;
          }
        }
      } // end for

      if (!hypoQueue.empty()) {
        best_hypo = 0;
        const ShiftReduceHypothesis *gold_hypo = 0;
        ShiftReduceAction *gold_action = 0;
        // apply beam
        construct_hypo(hypoQueue, m_lattice, best_hypo, gold_hypo, gold_action, gold_finished);
      }

    } // end while

    double top_f1 = calc_f1(candidate_output, sent_id, true);
    cerr << "top output score: " << candidate_output->GetTotalScore() << " f1: " << top_f1 << endl;

    size_t ind = 0;
    vector<const ShiftReduceHypothesis*> kbest_output_vec;
    while (!kbest_output_queue.empty()) {
      const ShiftReduceHypothesis* hypo = kbest_output_queue.top();
      kbest_output_vec.push_back(hypo);

      double f1 = calc_f1(hypo, sent_id, true);
      f1_vec.push_back(f1);
      seq_score_vec.push_back(hypo->GetTotalScore());

      cerr << "score: " << hypo->GetTotalScore() << " f1: " << f1 << endl;
      kbest_output_queue.pop();
      ++ind;
    }
    assert(ind == f1_vec.size());
    assert(ind == seq_score_vec.size());
    colvec f1_colvec(f1_vec);
    colvec seq_score_colvec(seq_score_vec);
    colvec seq_prob_colvec(ind);
    colvec xf1_colvec(ind);

    seq_prob_colvec = soft_max(seq_score_colvec);
    // calculate xf1
    xf1_colvec.fill(accu(seq_prob_colvec % f1_colvec));
    //
    //    if (candidate_output != 0)
    //      return candidate_output;

    return -xf1_colvec(0);
  }

  catch (NLP::Exception e) {
    //throw NLP::ParseError(e.msg, nsentences);
    cerr << e.msg << endl;
    return 0.0;
  }
}


//const ShiftReduceHypothesis*
//RNNParser::_Impl::sr_parse_beam_search_train_xf1_batch(const size_t k, const size_t sent_id,
//                                                       const bool oracle, const bool train,
//                                                       double &total_xf1,
//                                                       double &total_parses,
//                                                       const size_t &num_threads,
//                                                       const bool run_training) {
//  try {
//    // xf1 training related
//    vector<double> seq_score_vec;
//    vector<double> f1_vec;
//    m_total_reduce_count = 0; // reset total reduce count
//    m_h_tm1.zeros();   // reset hidden states
//    const ShiftReduceHypothesis *best_hypo; // the highest-scored hypo on each level
//    //    const ShiftReduceHypothesis *gold_hypo;
//    //    vector<ShiftReduceAction*> &gold_tree_actions = *m_goldTreeVec.at(sent_id);
//    //    vector<pair<size_t, size_t> > &gold_tree_actions_rnn_fmt = m_gold_tree_vec_rnn[sent_id];
//    //    size_t gold_tree_size = gold_tree_actions.size();
//    //    assert(gold_tree_actions.size() == gold_tree_actions_rnn_fmt.size());
//    //    ShiftReduceAction *gold_action = 0;
//    //    pair<size_t, size_t> gold_action_rnn_fmt;
//    //size_t gold_action_id = 0;
//    bool gold_finished = false;
//    std::priority_queue<ShiftReduceHypothesis*, vector<ShiftReduceHypothesis*>, Hypo_comp> kbest_output_queue;
//
//    if (sent.words.size() > cfg.maxwords())
//      throw NLP::ParseError("sentence length exceeds maximum number of words for parser", nsentences);
//
//    const size_t NWORDS = sent.words.size();
//    const ShiftReduceHypothesis *candidate_output = 0;
//    //    Words words;
//    //    raws2words(sent.words, words);
//    //    Words tags;
//    //    raws2words(sent.pos, tags);
//
//    // inds from the input sentence
//    vector<size_t> word_inds_vec;
//    vector<size_t> suf_inds_vec;
//
//    words2word_inds(sent.words, sent.pos, word_inds_vec, suf_inds_vec);
//    //    if (cfg.use_pos())
//    //      pos2pos_inds(sent.pos, suf_inds_vec);
//    //    else
//    //      suf2suf_inds(sent_rnn_fmt.pos, suf_inds_vec);
//    assert(word_inds_vec.size() == suf_inds_vec.size());
//
//    nsentences++;
//    SuperCat::nsupercats = 0;
//
//    //const MultiRaws &input = sent.msuper;
//
//    m_lattice = new ShiftReduceLattice(NWORDS, m_beamSize, MAX_UNARY_ACTIONS);
//    ShiftReduceHypothesis *start_hypo = new ShiftReduceHypothesis(0, 0, 0, 0, 0, 0, 0.0, false, 0, 0, 0, 0, 0.0, 0);
//    start_hypo->SetStartHypoFlag();
//    m_lattice->Add(0, start_hypo);
//    //start_hypo->m_hidden_states = m_h_tm1; // gets zero vec
//    //gold_hypo = m_lattice->GetLatticeArray(0); // gold == start
//    size_t startIndex;
//
//    cerr << "sent: " << sent_id << endl;
//
//    // all context, target, and feasible target values
//    // for this sentence
//    //vector<vector<vector<size_t> > > x_vals_sent;
//    //vector<size_t> y_vals_sent;
//    vector<vector<size_t> > feasible_y_vals_sent;
//
//    // set start and end indexes of the lattice
//    while (m_lattice->GetEndIndex() == 0 ||
//        m_lattice->GetEndIndex() > m_lattice->GetPrevEndIndex()) {
//      if (m_lattice->GetEndIndex() != 0)
//        startIndex = m_lattice->GetPrevEndIndex() + 1;
//      else
//        startIndex = 0;
//      m_lattice->SetPrevEndIndex(m_lattice->GetEndIndex());
//
//      //      if (gold_action_id < gold_tree_size)
//      //      {
//      //        gold_action = gold_tree_actions[gold_action_id];
//      //        assert(gold_action);
//      //        //cerr << "gold: " << gold_action->GetAction() << " "
//      //        //<< gold_action->GetCat()->out_novar_noX_noNB_SR_str() << endl;
//      //        gold_action_rnn_fmt = gold_tree_actions_rnn_fmt[gold_action_id];
//      //        assert(gold_action_rnn_fmt.first == gold_action->GetAction());
//      //      }
//
//      HypothesisPQueue hypoQueue;
//      vector<ShiftReduceHypothesis*> hypo_cache;
//      hypo_cache.reserve(m_beamsize);
//      for (size_t i = startIndex; i <= m_lattice->GetEndIndex(); ++i) {
//        //assert(m_lattice->GetEndIndex() - startIndex == 0);
//        ShiftReduceHypothesis *hypo = const_cast<ShiftReduceHypothesis *>(m_lattice->GetLatticeArray(i));
//        size_t j = hypo->GetNextInputIndex();
//
//        //        if (hypo->IsFinished(NWORDS, m_allowFragTree)) {
//        //          if (candidate_output == 0 || (hypo->GetTotalScore() / hypo->GetTotalActionCount())
//        //               > (candidate_output->GetTotalScore() / candidate_output->GetTotalActionCount())) {
//        //            candidate_output = hypo;
//        //          }
//        //          kbest_output_queue.push(hypo);
//        //        }
//
//        //vector<vector<size_t>> context_vec;
//        // inds of the current context
//        vector<size_t> word_inds_ret;
//        vector<size_t> suf_inds_ret;
//        vector<size_t> supercat_inds_ret;
//        vector<size_t> phrase_emb_ind_ret;
//        vector<size_t> child_phrase_emb_ind_ret; // dummy, won't be used at test time
//        hypo->m_context_vec.reserve(4); // TODO: remove this hard-coded 4
//        word_inds_ret.reserve(m_feature_count);
//        suf_inds_ret.reserve(m_feature_count);
//        supercat_inds_ret.reserve(m_supercat_feature_count);
//
//        //word_inds_ret.clear();
//        //suf_inds_ret.clear();
//        //supercat_inds_ret.clear();
//        //phrase_emb_ind_ret.clear();
//        //child_phrase_emb_ind_ret.clear();
//
//        ContextLoader context_loader(m_cat_ind, m_head_limit, false);
//        context_loader.load_context(hypo, word_inds_ret,
//                                    suf_inds_ret, supercat_inds_ret,
//                                    phrase_emb_ind_ret, child_phrase_emb_ind_ret,
//                                    word_inds_vec, suf_inds_vec,
//                                    m_cats_map, cats, cfg.use_phrase_emb_feats(), false);
//        assert(word_inds_ret.size() == suf_inds_ret.size());
//        assert(word_inds_ret.size() == m_feature_count);
//        assert(supercat_inds_ret.size() == m_supercat_feature_count);
//
//        hypo->m_context_vec.emplace_back(word_inds_ret);
//        hypo->m_context_vec.emplace_back(suf_inds_ret);
//        hypo->m_context_vec.emplace_back(supercat_inds_ret);
//        hypo->m_context_vec.emplace_back(phrase_emb_ind_ret);
//
//        //cerr << hypo->m_context_vec[0][0] << endl;
//        //x_vals_sent.push_back(context_vec);
//
//        //bool found_gold_action = false;
//        //size_t gold_action_class = 0;
//
//        // feasible target values for the current context
//        // the map is a check to make sure all the feasible
//        // target values for the current context are unique
//        // feasible_y_vals_map: key: output class (action id), value: (S/R/U, result cat)
//        vector<size_t> feasible_y_vals;
//        unordered_map<size_t, pair<size_t, SuperCat*> > feasible_y_vals_map;
//
//        // find all feasible actions,
//        // but do not push them into the queue
//
//        // shift
//        if (j < NWORDS) {
//          const MultiRaw &multi = sent.msuper[j];
//          for (MultiRaw::const_iterator k = multi.begin(); k!= multi.end(); ++k) {
//            const Cat *cat = cats.markedup[k->raw];
//
//            if(!cat) {
//              cerr << "SHIFT action error: attempted to load category without markedup "
//                  << k->raw << endl;
//              exit (EXIT_FAILURE);
//            }
//
//            assert(m_lex_cat_ind_map.find(k->raw) != m_lex_cat_ind_map.end());
//            feasible_y_vals.push_back(m_lex_cat_ind_map[k->raw]);
//            SuperCat *stack_top_cat = SuperCat::Lexical(chart.pool, j+1, cat, 0);
//            assert(feasible_y_vals_map.insert(make_pair(m_lex_cat_ind_map[k->raw],
//                                                        make_pair(0, stack_top_cat))).second);
//          } // end for
//        } // end if
//
//        // unary
//        if (hypo->GetStackTopSuperCat() != 0) {
//
//          const SuperCat *sc = hypo->GetStackTopSuperCat();
//          vector<SuperCat *> tmp;
//          tmp.clear();
//
//          if (!sc->IsLex() && !sc->IsTr()) {
//            unary_tc(hypo->GetStackTopSuperCat(), false, tmp);
//            unary_tr(sc, tmp);
//          }
//          else if (sc->IsLex() && !sc->IsTr()) {
//            unary_tr(sc, tmp);
//          }
//          else if (!sc->IsLex() && sc->IsTr()) { /* do nothing */ }
//          else if (sc->IsLex() && sc->IsTr()) { /* do nothing */ }
//
//          for (vector<SuperCat *>::iterator j = tmp.begin(); j < tmp.end(); ++j) {
//            SuperCat *stack_top_cat = *j;
//            assert(stack_top_cat->left);
//            feasible_y_vals.push_back(stack_top_cat->m_unary_id + m_unary_base_count);
//            assert(feasible_y_vals_map.insert(make_pair(stack_top_cat->m_unary_id + m_unary_base_count,
//                                                        make_pair(1, stack_top_cat))).second);
//          }
//        }
//
//        // reduce
//        if (hypo->GetPrvStack() != 0 && hypo->GetPrvStack()->GetStackTopSuperCat() != 0
//            && hypo->GetStackTopSuperCat() != 0) {
//
//          results.clear();
//          if (!cfg.seen_rules() ||
//              rule_instances(hypo->GetPrvStack()->GetStackTopSuperCat()->GetCat(), hypo->GetStackTopSuperCat()->GetCat()))
//            rules(hypo->GetPrvStack()->GetStackTopSuperCat(), hypo->GetStackTopSuperCat(),
//                  cfg.eisner_nf(), cfg.seen_rules(), cfg.question_rules(), results);
//
//          if (results.size() > 0) {
//            for (vector<SuperCat *>::iterator k = results.begin(); k < results.end(); ++k) {
//
//              SuperCat *stack_top_cat = *k;
//
//              if (stack_top_cat->m_reduce_id == 0) {
//                feasible_y_vals.push_back(m_reduce_base_count + 1);
//                assert(feasible_y_vals_map.insert(make_pair(m_reduce_base_count + 1,
//                                                            make_pair(2, stack_top_cat))).second);
//              } else {
//                feasible_y_vals.push_back(m_reduce_base_count + stack_top_cat->m_reduce_id + 1);
//                assert(feasible_y_vals_map.insert(make_pair(m_reduce_base_count + stack_top_cat->m_reduce_id + 1,
//                                                            make_pair(2, stack_top_cat))).second);
//              }
//            }
//          }
//        }
//
//        if (feasible_y_vals.size() > 0) {
//          hypo_cache.emplace_back(hypo);
//          hypo->m_action_score_vec.set_size(1, feasible_y_vals.size());
//          hypo->m_action_score_vec.zeros();
//        }
//
//
//
//        if (feasible_y_vals.size() > 0) {
//          // has feasible actions, then
//          // score current context
//
//          //mat new_hidden_states;
//          hypo->m_action_score_vec.set_size(1, feasible_y_vals.size());
//          hypo->m_action_score_vec.zeros();
//          const mat &prev_hidden_stats = hypo->IsStartHypo() ? m_h_tm1 : hypo->GetPrevHypo()->m_hidden_states;
//          score_context_beam_search_xf1(hypo->m_context_vec, feasible_y_vals, hypo->m_action_score_vec,
//                                        prev_hidden_stats, hypo->m_hidden_states, hypo->m_converted_context_vec);
//          vector<double> temp(feasible_y_vals.begin(), feasible_y_vals.end());
//          hypo->m_feasible_action_ids = mat(temp);
//
//          double total_score = 0.0;
//          unordered_map<size_t, pair<size_t, SuperCat*> >::const_iterator action_iter;
//          //size_t action_id = 0; // id of the output unit
//
//          for (size_t action_ind = 0; action_ind < feasible_y_vals.size(); ++action_ind) {
//            // TODO this is currently a naive implementation
//            // ideally, we don't have to expand every
//            // feasible action into the beam??
//
//            action_iter = feasible_y_vals_map.find(feasible_y_vals[action_ind]);
//            assert(action_iter != feasible_y_vals_map.end());
//
//            pair<size_t, SuperCat*> feasible_action = action_iter->second;
//            total_score = hypo->GetTotalScore() + log(hypo->m_action_score_vec(action_ind));  // log softmax probabilities
//            //total_score /= (double) (hypo->GetTotalActionCount() + 1);
//
//            // do the action
//            // shift
//            if (feasible_action.first == SHIFT) {
//              assert(j < NWORDS);
//
//              assert(feasible_action.second->cat);
//              //feasible_action.second->m_word_vec_ind = word_inds_vec[j];
//              ShiftReduceAction *action_shift = new ShiftReduceAction(SHIFT, feasible_action.second);
//              HypothesisTuple *new_hypo_tuple = new HypothesisTuple(hypo, action_shift, total_score, i, feasible_y_vals[action_ind]);
//              hypoQueue.push(new_hypo_tuple);
//
//              // unary
//            } else if (feasible_action.first == UNARY) {
//
//              assert(feasible_action.second->left);
//              //assert(hypo->GetStackTopSuperCat()->m_word_vec_ind != -1);
//              //feasible_action.second->m_word_vec_ind = hypo->GetStackTopSuperCat()->m_word_vec_ind;
//              ShiftReduceAction *action_unary = new ShiftReduceAction(UNARY, feasible_action.second);
//              HypothesisTuple *new_hypo_tuple = new HypothesisTuple(hypo, action_unary, total_score, i, feasible_y_vals[action_ind]);
//              hypoQueue.push(new_hypo_tuple);
//
//              // reduce
//            } else if (feasible_action.first == COMBINE) {
//
//              assert(hypo->GetPrvStack() != 0 && hypo->GetPrvStack()->GetStackTopSuperCat() != 0
//                     && hypo->GetStackTopSuperCat() != 0);
//
//              assert(feasible_action.second);
//              ++m_total_reduce_count;
//              //              compose_word_vec(hypo->GetPrvStack()->GetStackTopSuperCat()->m_word_vec_ind,
//              //                             hypo->GetStackTopSuperCat()->m_word_vec_ind, feasible_action.second);
//              ShiftReduceAction *action_combine = new ShiftReduceAction(COMBINE, feasible_action.second);
//              HypothesisTuple *new_hypo_tuple = new HypothesisTuple(hypo, action_combine, total_score, i, feasible_y_vals[action_ind]);
//              hypoQueue.push(new_hypo_tuple);
//
//            } else {
//              cerr << "best action not recognized...\n";
//              exit (EXIT_FAILURE);
//            }
//          }
//
//        } else {
//          // no more feasible actions, candidate output
//          kbest_output_queue.push(hypo);
//
//          if (candidate_output == 0 ||
//              (hypo->GetTotalScore() / hypo->GetTotalActionCount()) >
//          (candidate_output->GetTotalScore() / candidate_output->GetTotalActionCount())) {
//            candidate_output = hypo;
//          }
//        }
//      } // end for
//
//      if (!hypoQueue.empty()) {
//        best_hypo = 0;
//        const ShiftReduceHypothesis *gold_hypo = 0;
//        ShiftReduceAction *gold_action = 0;
//        // apply beam
//        construct_hypo(hypoQueue, m_lattice, best_hypo, gold_hypo, gold_action, gold_finished);
//      }
//
//    } // end while
//
//    double top_f1 = calc_f1(candidate_output, sent_id, train);
//    cerr << "top output score: " << candidate_output->GetTotalScore() << " f1: " << top_f1 << endl;
//
//    size_t ind = 0;
//    vector<size_t> action_count_vec;
//    action_count_vec.reserve(kbest_output_queue.size());
//
//    vector<const ShiftReduceHypothesis*> kbest_output_vec;
//    kbest_output_vec.reserve(kbest_output_queue.size());
//    seq_score_vec.reserve(kbest_output_queue.size());
//    f1_vec.reserve(kbest_output_queue.size());
//
//    while (!kbest_output_queue.empty()) {
//      const ShiftReduceHypothesis *hypo = kbest_output_queue.top();
//      kbest_output_vec.emplace_back(hypo);
//
//      seq_score_vec.emplace_back(hypo->GetTotalScore());
//      double f1 = calc_f1(hypo, sent_id, train);
//      f1_vec.emplace_back(f1);
//      cerr << "score: " << hypo->GetTotalScore() << " f1: " << f1 << endl;
//
//      if (oracle && f1 > top_f1) {
//        candidate_output = hypo;
//        top_f1 = f1;
//      }
//
//      action_count_vec.emplace_back(hypo->GetTotalActionCount());
//      kbest_output_queue.pop();
//      ++ind;
//      //if (ind == k) break;
//    }
//    //assert(ind <= k);
//    assert(ind == f1_vec.size());
//    assert(ind == seq_score_vec.size());
//    assert(ind == kbest_output_vec.size());
//    total_parses += ind;
//    colvec seq_score_colvec(seq_score_vec);
//    colvec f1_colvec(f1_vec);
//    colvec seq_prob_colvec(ind);
//    colvec xf1_colvec(ind);
//
//    // normalize
//    seq_prob_colvec = soft_max(seq_score_colvec);
//    // calculate xf1
//    xf1_colvec.fill(accu(seq_prob_colvec % f1_colvec));
//    // calculate p(seq)*(xf1-seq_f1)
//    mat pxf1f1 = seq_prob_colvec % (xf1_colvec - f1_colvec);
//
//    cerr << "xf1: " << xf1_colvec(0) << endl;
//    total_xf1 += xf1_colvec(0);
//
//    // debug, grad checking
//    //    mat m_emb_cp = m_emb;
//    //    mat m_suf_cp = m_suf;
//    //    mat m_supercat_emb_cp = m_supercat_emb;
//    //double bp_grad_emb00 = 0.0;
//
//    if (run_training) {
//      if (num_threads > 1) {
//        vector<mat> grad_wx_mats(ind, mat(m_wx.n_rows, m_wx.n_cols, fill::zeros));
//        vector<mat> grad_wh_mats(ind, mat(m_wh.n_rows, m_wh.n_cols, fill::zeros));
//        vector<mat> grad_wy_mats(ind, mat(m_wy.n_rows, m_wy.n_cols, fill::zeros));
//
//        mat grad_wx_total(m_wx.n_rows, m_wx.n_cols, fill::zeros);
//        mat grad_wh_total(m_wh.n_rows, m_wh.n_cols, fill::zeros);
//        mat grad_wy_total(m_wy.n_rows, m_wy.n_cols, fill::zeros);
//
//        vector<vector<mat>> all_delta_x_mats;
//        all_delta_x_mats.reserve(ind);
//        for (size_t i = 0; i < ind; ++i) {
//          all_delta_x_mats.emplace_back(vector<mat>(action_count_vec[i], mat(m_wx.n_rows, 1, fill::zeros)));
//        }
//
//        // entry point for xf1 training
//        m_h_tm1.zeros();
//        size_t hypo_ind = 0;
//        while (hypo_ind < ind) {
//          std::vector<thread> threads;
//          for (size_t i = 0; i < num_threads; ++i) {
//            size_t hypo_ind_cp = hypo_ind;
//            threads.push_back(thread([&, hypo_ind_cp]{train_xf1_mt(ref(pxf1f1(hypo_ind_cp)), ref(kbest_output_vec[hypo_ind_cp]),
//                                                                   ref(grad_wx_mats[hypo_ind_cp]),
//                                                                   ref(grad_wh_mats[hypo_ind_cp]),
//                                                                   ref(grad_wy_mats[hypo_ind_cp]),
//                                                                   ref(all_delta_x_mats[hypo_ind_cp]));}));
//            ++hypo_ind;
//            if (hypo_ind == ind)
//              break;
//          }
//
//          for (thread &th : threads) {
//            th.join();
//          }
//        }
//
//        for (size_t i = 0; i < ind; ++i) {
//          grad_wx_total += grad_wx_mats[i];
//          grad_wh_total += grad_wh_mats[i];
//          grad_wy_total += grad_wy_mats[i];
//        }
//
//        cerr << "bp_grad_wx: " << grad_wx_total(12,12) << endl;
//        cerr << "bp_grad_wh: " << grad_wh_total(12,12) << endl;
//        cerr << "bp_grad_wy: " << grad_wy_total(0,0) << endl;
//
//        m_wh -= m_lr*grad_wh_total;
//        m_wy -= m_lr*grad_wy_total;
//        m_wx -= m_lr*grad_wx_total;
//
//        // update embeddings, in the single-thread
//        // version this is done in the bptt function
//        for (size_t h = 0; h < ind; ++h) {
//          const size_t steps = kbest_output_vec[h]->GetTotalActionCount();
//          //cerr << "steps: " << steps << endl;
//          assert(steps == all_delta_x_mats[h].size());
//          const ShiftReduceHypothesis *curr_hypo = kbest_output_vec[h];
//          for (int i = steps - 1; i >= 0; --i) {
//            //const vector<size_t> &word_inds = context_batch[i][0];
//            //const vector<size_t> &suf_inds = context_batch[i][1];
//            const vector<size_t> &word_inds = curr_hypo->GetPrevHypo()->m_context_vec[0];
//            const vector<size_t> &suf_inds = curr_hypo->GetPrevHypo()->m_context_vec[1];
//            assert(word_inds.size() == suf_inds.size());
//
//            // all start inds must be set here
//            int start = 0;
//            int start2 = m_de*word_inds.size(); // dim of word emb x num of embs
//            int start3 = start2 + m_ds*word_inds.size();
//            //int start4 = 0;
//
//            for (size_t k = 0; k < word_inds.size(); ++k) {
//              //cerr << "n_rows: " << all_delta_x_mats[h][i] << endl;
//              //cerr << "mt: " << word_inds[k] << endl;
//              //all_delta_x_mats[h][i].rows(start, start + m_de - 1).print(cerr);
//
//              m_emb.col(word_inds[k]) -= m_lr*all_delta_x_mats[h][i].rows(start, start + m_de - 1);
//              //if (word_inds[k] == 0)
//              //bp_grad_emb00 += all_delta_x_mats[h][i].rows(start, start + m_de - 1)(0);
//              //m_lr*delta_x_vals[i].rows(start, start + m_de - 1);
//              //cerr << "suf: " << suf_inds[k] << endl;
//              m_suf.col(suf_inds[k]) -= m_lr*all_delta_x_mats[h][i].rows(start2, start2 + m_ds - 1);
//              //m_lr*delta_x_vals[i].rows(start2, start2 + m_ds - 1);
//              //      m_cap.col(cap_inds[k]) -=
//              //          m_lr*delta_x_vals[i].rows(start3, start3 + m_dc - 1);
//
//              start += m_de;
//              start2 += m_ds;
//            }
//
//            if (m_use_supercat_feats) {
//              //assert(context_batch[i].size() >= 3);
//              assert(curr_hypo->GetPrevHypo()->m_context_vec.size() >= 3);
//              assert(start2 == start3);
//
//              //const vector<size_t> &supercat_inds = context_batch[i][2];
//              const vector<size_t> &supercat_inds = curr_hypo->GetPrevHypo()->m_context_vec[2];
//              //start4 = start3 + m_dsuper*supercat_inds.size();
//
//              for (size_t k = 0; k < supercat_inds.size(); ++k) {
//                //cerr << "super: " << supercat_inds[k] << endl;
//                m_supercat_emb.col(supercat_inds[k]) -=  m_lr*all_delta_x_mats[h][i].rows(start3, start3 + m_dsuper - 1);
//                //    m_lr*delta_x_vals[i].rows(start3, start3 + m_dsuper - 1);
//                start3 += m_dsuper;
//              }
//            }
//
//            curr_hypo = curr_hypo->GetPrevHypo();
//          }
//          assert(curr_hypo->IsStartHypo());
//        }
//      } else {
//        //        mat grad_wx;
//        //        mat grad_wh;
//        //        mat grad_wy;
//        //        grad_wx.copy_size(m_wx);
//        //        grad_wh.copy_size(m_wh);
//        //        grad_wy.copy_size(m_wy);
//        //        grad_wx.zeros();
//        //        grad_wh.zeros();
//        //        grad_wy.zeros();
//
//        vector<mat> grad_wx_mats(ind, mat(m_wx.n_rows, m_wx.n_cols, fill::zeros));
//        vector<mat> grad_wh_mats(ind, mat(m_wh.n_rows, m_wh.n_cols, fill::zeros));
//        vector<mat> grad_wy_mats(ind, mat(m_wy.n_rows, m_wy.n_cols, fill::zeros));
//
//        mat grad_wx_total(m_wx.n_rows, m_wx.n_cols, fill::zeros);
//        mat grad_wh_total(m_wh.n_rows, m_wh.n_cols, fill::zeros);
//        mat grad_wy_total(m_wy.n_rows, m_wy.n_cols, fill::zeros);
//
//        m_h_tm1.zeros();
//        // entry point for xf1 training
//        for (size_t hypo_ind = 0; hypo_ind < ind; ++hypo_ind) {
//          train_xf1(pxf1f1(hypo_ind), kbest_output_vec[hypo_ind],
//                    grad_wx_mats[hypo_ind], grad_wh_mats[hypo_ind], grad_wy_mats[hypo_ind]);
//        }
//        //        for (size_t hypo_ind = 0; hypo_ind < ind; ++hypo_ind) {
//        //          train_xf1(pxf1f1(hypo_ind), kbest_output_vec[hypo_ind], grad_wx, grad_wh, grad_wy);
//        //        }
//
//        for (size_t i = 0; i < ind; ++i) {
//          grad_wx_total += grad_wx_mats[i];
//          grad_wh_total += grad_wh_mats[i];
//          grad_wy_total += grad_wy_mats[i];
//        }
//
//        cerr << "bp_grad_wx: " << grad_wx_total(12,12) << endl;
//        cerr << "bp_grad_wh: " << grad_wh_total(12,12) << endl;
//        cerr << "bp_grad_wy: " << grad_wy_total(0,0) << endl;
//
//        m_wh -= m_lr*grad_wh_total;
//        m_wy -= m_lr*grad_wy_total;
//        m_wx -= m_lr*grad_wx_total;
//
//
//        //        cerr << "bp_grad_wx: " << grad_wx(12,12) << endl;
//        //        cerr << "bp_grad_wh: " << grad_wh(12,12) << endl;
//        //        cerr << "bp_grad_wy: " << grad_wy(0,0) << endl;
//        //
//        //        m_wh -= m_lr*grad_wh;
//        //        m_wy -= m_lr*grad_wy;
//        //        m_wx -= m_lr*grad_wx;
//      }
//    }
//
//    //debug, grad checking
//    //    double bp_grad_emb00 = (m_emb_cp(0,0) - m_emb(0,0))/m_lr;
//    //    double bg_grad_emb03 = (m_emb_cp(0,3) - m_emb(0,3))/m_lr;
//    //    double bp_grad_suf00 = (m_suf_cp(0,0) - m_suf(0,0))/m_lr;
//    //    double bg_grad_suf05 = (m_suf_cp(0,5) - m_suf(0,5))/m_lr;
//    //    double bp_grad_super09 = (m_supercat_emb_cp(0,9) - m_supercat_emb(0,9))/m_lr;
//    //    double bg_grad_super26 = (m_supercat_emb_cp(0,26) - m_supercat_emb(0,26))/m_lr;
//    //
//    //    cerr << "bp_grad_emb00: " << bp_grad_emb00 << endl;
//    //    cerr << "bg_grad_emb03: " << bg_grad_emb03 << endl;
//    //    cerr << "bp_grad_suf00: " << bp_grad_suf00 << endl;
//    //    cerr << "bg_grad_suf05: " << bg_grad_suf05 << endl;
//    //    cerr << "bp_grad_super09: " << bp_grad_super09 << endl;
//    //    cerr << "bp_grad_super26: " << bg_grad_super26 << endl;
//
//    if (candidate_output != 0)
//      return candidate_output;
//
//    return 0;
//  }
//
//  catch (NLP::Exception e) {
//    //throw NLP::ParseError(e.msg, nsentences);
//    cerr << e.msg << endl;
//    return 0;
//  }
//}
void
RNNParser::_Impl::precompute_position(unordered_map<size_t, size_t> &map,
                                      const size_t &k,
                                      vector<size_t> &top_emb_inds) {
  unordered_map<size_t, size_t> unique_freq_map;
  for (auto it = map.begin(); it != map.end(); ++it) {
    auto it2 = unique_freq_map.find(it->second);
    if (it2 == unique_freq_map.end()) {
      unique_freq_map.insert({it->second, 1});
    } else {
      it2->second += 1;
    }
  }

  vector<size_t> top_freq;
  top_freq.reserve(unique_freq_map.size());
  for (auto it = unique_freq_map.begin(); it != unique_freq_map.end(); ++it) {
    top_freq.emplace_back(it->first);
  }
  sort(top_freq.begin(), top_freq.end());
  reverse(top_freq.begin(), top_freq.end());

  // debug
  for (size_t &val : top_freq) {
    cerr << val << " ";
  }
  cerr << endl;

  if (top_freq.size() > k)
    top_freq.resize(k);

  unordered_map<size_t, size_t> top_freq_map;
  for (size_t &freq : top_freq) {
    assert(top_freq_map.insert({freq, freq}).second);
  }

  for (auto it = map.begin(); it != map.end(); ++it) {
    if (top_freq_map.find(it->second) != top_freq_map.end())
      top_emb_inds.push_back(it->first);
  }

  //todo can further resize top_emb_inds if needed
}


void
RNNParser::_Impl::precompute_all_positions(unordered_map<size_t, unordered_map<size_t, size_t>> &map,
                                           const size_t &k,
                                           const bool word) {
  for (auto it = map.begin(); it != map.end(); ++it) {
    // for each position
    vector<size_t> top_emb_inds; // inds in this vec are unique
    precompute_position(it->second, k, top_emb_inds);

    if (word) {
      // get wx subview for position it->first
      size_t off_set = it->first*m_de; // word_pos * m_de
      mat m_wx_sub = m_wx.submat(off_set, 0, off_set + (m_de - 1), m_wx.n_cols - 1);

      auto position_it = m_word_emb_lh_cache_val.find(it->first);
      if (position_it != m_word_emb_lh_cache_val.end()) {
        for (size_t &ind : top_emb_inds) {
          auto emb_it = position_it->second.find(ind);
          if (emb_it == position_it->second.end()) {
            mat lh_val = (m_emb.col(ind).t() * m_dropout_success_prob)*m_wx_sub;
            assert(position_it->second.insert({ind, lh_val}).second);
          }
        }

      } else {

        // compute lh_val for top emb_inds at this position
        unordered_map<size_t, mat> ind_lh_cache;
        for (size_t &ind : top_emb_inds) {
          mat lh_val = (m_emb.col(ind).t() * m_dropout_success_prob)*m_wx_sub;
          assert(ind_lh_cache.insert({ind, lh_val}).second);
        }
        assert(m_word_emb_lh_cache_val.insert({it->first, ind_lh_cache}).second);
      }
    } else { // precompute pos
      // get wx subview for position it->first
      size_t off_set = m_de*m_feature_count;
      size_t off_set2 = it->first*m_ds;
      mat m_wx_sub = m_wx.submat(off_set+off_set2, 0, off_set + off_set2 + (m_ds - 1), m_wx.n_cols - 1);

      auto position_it = m_suf_emb_lh_cache_val.find(it->first);
      if (position_it != m_suf_emb_lh_cache_val.end()) {
        for (size_t &ind : top_emb_inds) {
          auto emb_it = position_it->second.find(ind);
          if (emb_it == position_it->second.end()) {
            mat lh_val = (m_suf.col(ind).t() * m_dropout_success_prob)*m_wx_sub;
            assert(position_it->second.insert({ind, lh_val}).second);
          }
        }
      } else {
        // compute lh_val for top emb_inds at this position
        unordered_map<size_t, mat> ind_lh_cache;
        for (size_t &ind : top_emb_inds) {
          mat lh_val = (m_suf.col(ind).t() * m_dropout_success_prob)*m_wx_sub;
          assert(ind_lh_cache.insert({ind, lh_val}).second);
        }
        assert(m_suf_emb_lh_cache_val.insert({it->first, ind_lh_cache}).second);
      }
    }
  }
}


void
RNNParser::_Impl::precompute(const size_t &k) {
  precompute_all_positions(m_word_feat_emb_ind_map, k, true);
  precompute_all_positions(m_suf_feat_emb_ind_map, k, false);

  convert_n_save(m_word_emb_lh_cache_val, true);
  convert_n_save(m_suf_emb_lh_cache_val, false);
}


void
RNNParser::_Impl::convert_n_save(const unordered_map<size_t, unordered_map<size_t, mat>> &map,
                                 const bool word) {
  assert(map.size() == m_feature_count);
  size_t total_rows = 0;
  for (auto it = map.begin(); it != map.end(); ++it) {
    total_rows += it->second.size();
  }
  // each row is: position, emb_id, lh_val (eg, 0 5 mat)
  mat all(total_rows, 1 + 1 + m_nh);
  size_t i = 0;
  for (auto it = map.begin(); it != map.end(); ++it) {
    for (auto it2 = it->second.begin(); it2 != it->second.end(); ++it2) {
      all(i, 0) = it->first;
      all(i, 1) = it2->first;
      all(i, span(2, all.n_cols - 1)) = it2->second;
      ++i;
    }
  }
  assert(i == total_rows);
  if (word)
    all.save("word_feat_emb_ind_map");
  else
    all.save("suf_feat_emb_ind_map");
}


void
RNNParser::_Impl::load_lh_cache(const string &path,
                                unordered_map<size_t, unordered_map<size_t, mat>> &map) {
  mat all;
  all.load(path);
  for (size_t i = 0; i < all.n_rows; ++i) {
    mat lh = all(i, span(2, all.n_cols - 1));
    auto it = map.find(all(i, 0));
    if (it == map.end()) {
      assert(map.insert({size_t(all(i, 0)), unordered_map<size_t, mat>({{size_t(all(i, 1)), lh}})}).second);
    } else {
      assert(it->second.insert({size_t(all(i, 1)), lh}).second);
    }
  }
}


double
RNNParser::_Impl::sr_parse_beam_search_train_xf1_grad_check_super(NLP::Taggers::RnnTagger &rnn_super,
                                                                  const size_t k, const size_t sent_id,
                                                                  const bool oracle, const bool train) {
  try {
    // xf1 training related
    //debug
    m_avg_unique_prev_hypo_count = 0.0;
    vector<double> seq_score_vec;
    vector<double> f1_vec;
    m_total_reduce_count = 0; // reset total reduce count
    m_h_tm1.zeros();   // reset hidden states
    const ShiftReduceHypothesis *best_hypo; // the highest-scored hypo on each level
    //    const ShiftReduceHypothesis *gold_hypo;
    //    vector<ShiftReduceAction*> &gold_tree_actions = *m_goldTreeVec.at(sent_id);
    //    vector<pair<size_t, size_t> > &gold_tree_actions_rnn_fmt = m_gold_tree_vec_rnn[sent_id];
    //    size_t gold_tree_size = gold_tree_actions.size();
    //    assert(gold_tree_actions.size() == gold_tree_actions_rnn_fmt.size());
    //    ShiftReduceAction *gold_action = 0;
    //    pair<size_t, size_t> gold_action_rnn_fmt;
    //size_t gold_action_id = 0;
    bool gold_finished = false;
    std::priority_queue<ShiftReduceHypothesis*, vector<ShiftReduceHypothesis*>, Hypo_comp> kbest_output_queue;

    if (sent.words.size() > cfg.maxwords())
      throw NLP::ParseError("sentence length exceeds maximum number of words for parser", nsentences);

    const size_t NWORDS = sent.words.size();
    const ShiftReduceHypothesis *candidate_output = 0;
    //    Words words;
    //    raws2words(sent.words, words);
    //    Words tags;
    //    raws2words(sent.pos, tags);

    // inds from the input sentence
    vector<size_t> word_inds_vec;
    vector<size_t> suf_inds_vec;

    words2word_inds(sent.words, sent.pos, word_inds_vec, suf_inds_vec);
    //    if (cfg.use_pos())
    //      pos2pos_inds(sent.pos, suf_inds_vec);
    //    else
    //      suf2suf_inds(sent_rnn_fmt.pos, suf_inds_vec);
    assert(word_inds_vec.size() == suf_inds_vec.size());

    nsentences++;
    SuperCat::nsupercats = 0;

    //const MultiRaws &input = sent.msuper;

    m_lattice = new ShiftReduceLattice(NWORDS, m_beamSize, MAX_UNARY_ACTIONS);
    ShiftReduceHypothesis *start_hypo = new ShiftReduceHypothesis(0, 0, 0, 0, 0, 0, 0.0, false, 0, 0, 0, 0, 0.0, 0);
    start_hypo->SetStartHypoFlag();
    m_lattice->Add(0, start_hypo);
    //start_hypo->m_hidden_states = m_h_tm1; // gets zero vec
    //gold_hypo = m_lattice->GetLatticeArray(0); // gold == start
    size_t startIndex;

    cerr << "sent: " << sent_id << endl;

    // all context, target, and feasible target values
    // for this sentence
    //vector<vector<vector<size_t> > > x_vals_sent;
    //vector<size_t> y_vals_sent;
    vector<vector<size_t> > feasible_y_vals_sent;

    //debug
    double total_steps = 0.0;
    // set start and end indexes of the lattice
    while (m_lattice->GetEndIndex() == 0 ||
        m_lattice->GetEndIndex() > m_lattice->GetPrevEndIndex()) {
      ++total_steps;
      if (m_lattice->GetEndIndex() != 0)
        startIndex = m_lattice->GetPrevEndIndex() + 1;
      else
        startIndex = 0;
      m_lattice->SetPrevEndIndex(m_lattice->GetEndIndex());

      //      if (gold_action_id < gold_tree_size)
      //      {
      //        gold_action = gold_tree_actions[gold_action_id];
      //        assert(gold_action);
      //        //cerr << "gold: " << gold_action->GetAction() << " "
      //        //<< gold_action->GetCat()->out_novar_noX_noNB_SR_str() << endl;
      //        gold_action_rnn_fmt = gold_tree_actions_rnn_fmt[gold_action_id];
      //        assert(gold_action_rnn_fmt.first == gold_action->GetAction());
      //      }

      HypothesisPQueue hypoQueue;
      for (size_t i = startIndex; i <= m_lattice->GetEndIndex(); ++i) {
        //assert(m_lattice->GetEndIndex() - startIndex == 0);
        ShiftReduceHypothesis *hypo = const_cast<ShiftReduceHypothesis *>(m_lattice->GetLatticeArray(i));
        size_t j = hypo->GetNextInputIndex();

        //        if (hypo->IsFinished(NWORDS, m_allowFragTree)) {
        //          if (candidate_output == 0 || (hypo->GetTotalScore() / hypo->GetTotalActionCount())
        //               > (candidate_output->GetTotalScore() / candidate_output->GetTotalActionCount())) {
        //            candidate_output = hypo;
        //          }
        //          kbest_output_queue.push(hypo);
        //        }

        //vector<vector<size_t>> context_vec;
        // inds of the current context
        vector<size_t> word_inds_ret;
        vector<size_t> suf_inds_ret;
        vector<size_t> supercat_inds_ret;
        vector<size_t> phrase_emb_ind_ret;
        vector<size_t> child_phrase_emb_ind_ret; // dummy, won't be used at test time
        hypo->m_context_vec.reserve(4); // TODO: remove this hard-coded 4
        word_inds_ret.reserve(m_feature_count);
        suf_inds_ret.reserve(m_feature_count);
        supercat_inds_ret.reserve(m_supercat_feature_count);

        //word_inds_ret.clear();
        //suf_inds_ret.clear();
        //supercat_inds_ret.clear();
        //phrase_emb_ind_ret.clear();
        //child_phrase_emb_ind_ret.clear();

        ContextLoader context_loader(m_cat_ind, m_head_limit, false);
        context_loader.load_context(hypo, word_inds_ret,
                                    suf_inds_ret, supercat_inds_ret,
                                    phrase_emb_ind_ret, child_phrase_emb_ind_ret,
                                    word_inds_vec, suf_inds_vec,
                                    m_cats_map, cats, cfg.use_phrase_emb_feats(), false);
        assert(word_inds_ret.size() == suf_inds_ret.size());
        assert(word_inds_ret.size() == m_feature_count);
        assert(supercat_inds_ret.size() == m_supercat_feature_count);

        hypo->m_context_vec.emplace_back(word_inds_ret);
        hypo->m_context_vec.emplace_back(suf_inds_ret);
        hypo->m_context_vec.emplace_back(supercat_inds_ret);
        hypo->m_context_vec.emplace_back(phrase_emb_ind_ret);

        // precomputation related
        //        if (cfg.precompute()) {
        //          for (size_t w_i = 0; w_i < word_inds_ret.size(); ++w_i) {
        //            // for each word position
        //            auto iter = m_word_feat_emb_ind_map.find(w_i);
        //            if (iter == m_word_feat_emb_ind_map.end()) {
        //              m_word_feat_emb_ind_map.insert({w_i, unordered_map<size_t, size_t>({{word_inds_ret[w_i], 1}})});
        //            } else {
        //              // find this emb ind
        //              auto iter2 = iter->second.find(word_inds_ret[w_i]);
        //
        //              if (iter2 == iter->second.end()) {
        //                assert(iter->second.insert({word_inds_ret[w_i], 1}).second);
        //              } else {
        //                iter2->second += 1;
        //              }
        //            }
        //
        //            // for each suf position
        //            auto iter3 = m_suf_feat_emb_ind_map.find(w_i);
        //            if (iter3 == m_suf_feat_emb_ind_map.end()) {
        //              m_suf_feat_emb_ind_map.insert({w_i, unordered_map<size_t, size_t>({{suf_inds_ret[w_i], 1}})});
        //            } else {
        //              // find this emb ind
        //              auto iter4 = iter3->second.find(suf_inds_ret[w_i]);
        //
        //              if (iter4 == iter3->second.end()) {
        //                assert(iter3->second.insert({suf_inds_ret[w_i], 1}).second);
        //              } else {
        //                iter4->second += 1;
        //              }
        //            }
        //          }
        //        }

        //cerr << hypo->m_context_vec[0][0] << endl;
        //x_vals_sent.push_back(context_vec);

        //bool found_gold_action = false;
        //size_t gold_action_class = 0;

        // feasible target values for the current context
        // the map is a check to make sure all the feasible
        // target values for the current context are unique
        // feasible_y_vals_map: key: output class (action id), value: (S/R/U, result cat)
        vector<size_t> feasible_y_vals;
        unordered_map<size_t, pair<size_t, SuperCat*> > feasible_y_vals_map;

        // find all feasible actions,
        // but do not push them into the queue

        // shift
        if (j < NWORDS) {
          const MultiRaw &multi = sent.msuper[j];
          for (MultiRaw::const_iterator k = multi.begin(); k!= multi.end(); ++k) {
            const Cat *cat = cats.markedup[k->raw];

            if(!cat) {
              cerr << "SHIFT action error: attempted to load category without markedup "
                  << k->raw << endl;
              exit (EXIT_FAILURE);
            }

            assert(m_lex_cat_ind_map.find(k->raw) != m_lex_cat_ind_map.end());
            feasible_y_vals.push_back(m_lex_cat_ind_map[k->raw]);
            SuperCat *stack_top_cat = SuperCat::Lexical(chart.pool, j+1, cat, 0);
            //assert(m_lex_cat_ind_map[k->raw] == k->ind); //todo check this for xf1 super training
            assert(feasible_y_vals_map.insert(make_pair(m_lex_cat_ind_map[k->raw],
                                                        make_pair(0, stack_top_cat))).second);
          } // end for
        } // end if

        // unary
        if (hypo->GetStackTopSuperCat() != 0) {

          const SuperCat *sc = hypo->GetStackTopSuperCat();
          vector<SuperCat *> tmp;
          tmp.clear();

          if (!sc->IsLex() && !sc->IsTr()) {
            unary_tc(hypo->GetStackTopSuperCat(), false, tmp);
            unary_tr(sc, tmp);
          }
          else if (sc->IsLex() && !sc->IsTr()) {
            unary_tr(sc, tmp);
          }
          else if (!sc->IsLex() && sc->IsTr()) { /* do nothing */ }
          else if (sc->IsLex() && sc->IsTr()) { /* do nothing */ }

          for (vector<SuperCat *>::iterator j = tmp.begin(); j < tmp.end(); ++j) {
            SuperCat *stack_top_cat = *j;
            assert(stack_top_cat->left);
            feasible_y_vals.push_back(stack_top_cat->m_unary_id + m_unary_base_count);
            assert(feasible_y_vals_map.insert(make_pair(stack_top_cat->m_unary_id + m_unary_base_count,
                                                        make_pair(1, stack_top_cat))).second);
          }
        }

        // reduce
        if (hypo->GetPrvStack() != 0 && hypo->GetPrvStack()->GetStackTopSuperCat() != 0
            && hypo->GetStackTopSuperCat() != 0) {

          results.clear();
          if (!cfg.seen_rules() ||
              rule_instances(hypo->GetPrvStack()->GetStackTopSuperCat()->GetCat(), hypo->GetStackTopSuperCat()->GetCat()))
            rules(hypo->GetPrvStack()->GetStackTopSuperCat(), hypo->GetStackTopSuperCat(),
                  cfg.eisner_nf(), cfg.seen_rules(), cfg.question_rules(), results);

          if (results.size() > 0) {
            for (vector<SuperCat *>::iterator k = results.begin(); k < results.end(); ++k) {

              SuperCat *stack_top_cat = *k;

              if (stack_top_cat->m_reduce_id == 0) {
                feasible_y_vals.push_back(m_reduce_base_count + 1);
                assert(feasible_y_vals_map.insert(make_pair(m_reduce_base_count + 1,
                                                            make_pair(2, stack_top_cat))).second);
              } else {
                feasible_y_vals.push_back(m_reduce_base_count + stack_top_cat->m_reduce_id + 1);
                assert(feasible_y_vals_map.insert(make_pair(m_reduce_base_count + stack_top_cat->m_reduce_id + 1,
                                                            make_pair(2, stack_top_cat))).second);
              }
            }
          }
        }

        if (feasible_y_vals.size() > 0) {
          // has feasible actions, then
          // score current context

          //mat new_hidden_states;
          hypo->m_action_score_vec.set_size(1, feasible_y_vals.size());
          hypo->m_action_score_vec.zeros();
          const mat &prev_hidden_stats = hypo->IsStartHypo() ? m_h_tm1 : hypo->GetPrevHypo()->m_hidden_states;
          score_context_beam_search_xf1(hypo->m_context_vec, feasible_y_vals, hypo->m_action_score_vec,
                                        prev_hidden_stats, hypo->m_hidden_states, hypo->m_converted_context_vec);
          vector<double> temp(feasible_y_vals.begin(), feasible_y_vals.end());
          hypo->m_feasible_action_ids = mat(temp);
          //hypo->m_hidden_states_set = true;

          double total_score = 0.0;
          unordered_map<size_t, pair<size_t, SuperCat*> >::const_iterator action_iter;
          //size_t action_id = 0; // id of the output unit

          for (size_t action_ind = 0; action_ind < feasible_y_vals.size(); ++action_ind) {
            // TODO this is currently a naive implementation
            // ideally, we don't have to expand every
            // feasible action into the beam??

            action_iter = feasible_y_vals_map.find(feasible_y_vals[action_ind]);
            assert(action_iter != feasible_y_vals_map.end());

            pair<size_t, SuperCat*> feasible_action = action_iter->second;
            total_score = hypo->GetTotalScore() + log(hypo->m_action_score_vec(action_ind));  // log softmax probabilities
            //debug
            //cerr << "total score: " << total_score << endl;
            //total_score /= (double) (hypo->GetTotalActionCount() + 1);

            // do the action
            // shift
            if (feasible_action.first == SHIFT) {
              assert(j < NWORDS);

              assert(feasible_action.second->cat);
              //feasible_action.second->m_word_vec_ind = word_inds_vec[j];
              ShiftReduceAction *action_shift = new ShiftReduceAction(SHIFT, feasible_action.second);
              HypothesisTuple *new_hypo_tuple = new HypothesisTuple(hypo, action_shift, total_score, i, feasible_y_vals[action_ind]);
              hypoQueue.push(new_hypo_tuple);

              // unary
            } else if (feasible_action.first == UNARY) {

              assert(feasible_action.second->left);
              //assert(hypo->GetStackTopSuperCat()->m_word_vec_ind != -1);
              //feasible_action.second->m_word_vec_ind = hypo->GetStackTopSuperCat()->m_word_vec_ind;
              ShiftReduceAction *action_unary = new ShiftReduceAction(UNARY, feasible_action.second);
              HypothesisTuple *new_hypo_tuple = new HypothesisTuple(hypo, action_unary, total_score, i, feasible_y_vals[action_ind]);
              hypoQueue.push(new_hypo_tuple);

              // reduce
            } else if (feasible_action.first == COMBINE) {

              assert(hypo->GetPrvStack() != 0 && hypo->GetPrvStack()->GetStackTopSuperCat() != 0
                     && hypo->GetStackTopSuperCat() != 0);

              assert(feasible_action.second);
              ++m_total_reduce_count;
              //              compose_word_vec(hypo->GetPrvStack()->GetStackTopSuperCat()->m_word_vec_ind,
              //                             hypo->GetStackTopSuperCat()->m_word_vec_ind, feasible_action.second);
              ShiftReduceAction *action_combine = new ShiftReduceAction(COMBINE, feasible_action.second);
              HypothesisTuple *new_hypo_tuple = new HypothesisTuple(hypo, action_combine, total_score, i, feasible_y_vals[action_ind]);
              hypoQueue.push(new_hypo_tuple);

            } else {
              cerr << "best action not recognized...\n";
              exit (EXIT_FAILURE);
            }
          }

        } else {
          // no more feasible actions, candidate output
          kbest_output_queue.push(hypo);

          if (candidate_output == 0 ||
              (hypo->GetTotalScore() / hypo->GetTotalActionCount()) >
          (candidate_output->GetTotalScore() / candidate_output->GetTotalActionCount())) {
            candidate_output = hypo;
          }
        }
      } // end for

      if (!hypoQueue.empty()) {
        best_hypo = 0;
        const ShiftReduceHypothesis *gold_hypo = 0;
        ShiftReduceAction *gold_action = 0;
        // apply beam
        construct_hypo(hypoQueue, m_lattice, best_hypo, gold_hypo, gold_action, gold_finished);
      }

    } // end while

    double top_f1 = calc_f1(candidate_output, sent_id, train);
    cerr << "top output score: " << candidate_output->GetTotalScore() << " f1: " << top_f1 << endl;

    size_t ind = 0;
    vector<size_t> action_count_vec;
    action_count_vec.reserve(kbest_output_queue.size());

    vector<const ShiftReduceHypothesis*> kbest_output_vec;
    kbest_output_vec.reserve(kbest_output_queue.size());
    seq_score_vec.reserve(kbest_output_queue.size());
    f1_vec.reserve(kbest_output_queue.size());

    while (!kbest_output_queue.empty()) {
      const ShiftReduceHypothesis *hypo = kbest_output_queue.top();
      kbest_output_vec.emplace_back(hypo);

      seq_score_vec.emplace_back(hypo->GetTotalScore());
      double f1 = calc_f1(hypo, sent_id, train);
      f1_vec.emplace_back(f1);
      cerr << "score: " << hypo->GetTotalScore() << " f1: " << f1 << endl;

      if (oracle && f1 > top_f1) {
        candidate_output = hypo;
        top_f1 = f1;
      }

      action_count_vec.emplace_back(hypo->GetTotalActionCount());
      kbest_output_queue.pop();
      ++ind;
      //if (ind == k) break;
    }
    //assert(ind <= k);
    assert(ind == f1_vec.size());
    assert(ind == seq_score_vec.size());
    assert(ind == kbest_output_vec.size());
    //total_parses += ind;
    colvec seq_score_colvec(seq_score_vec);
    colvec f1_colvec(f1_vec);
    colvec seq_prob_colvec(ind);
    colvec xf1_colvec(ind);

    // normalize
    seq_prob_colvec = soft_max(seq_score_colvec);
    // calculate xf1
    xf1_colvec.fill(accu(seq_prob_colvec % f1_colvec));
    // calculate p(seq)*(xf1-seq_f1)
    mat pxf1f1 = seq_prob_colvec % (xf1_colvec - f1_colvec);

    cerr << "parser xf1: " << xf1_colvec(0) << endl;
    //total_xf1 += xf1_colvec(0);

    //debug, grad checking
//    mat m_emb_cp = m_emb;
//    mat m_suf_cp = m_suf;
//    mat m_supercat_emb_cp = m_supercat_emb;

    // xf1 supertagging
    //if (run_training && cfg.xf1_super()) {
      // get all tag sequences, ie all inds of tags
      vector<vector<size_t>> all_tag_seq;
      all_tag_seq.reserve(kbest_output_vec.size());
      for (size_t i = 0; i < ind; ++i) {
        vector<size_t> one_tag_seq;
        one_tag_seq.reserve(sent.words.size());
        const ShiftReduceHypothesis *hypo = kbest_output_vec[i];
        while (true) {
          if (hypo->GetStackTopAction() == SHIFT) {
            auto cat_it = m_lex_ind_cat_map.find(hypo->m_action_id);
            assert(cat_it != m_lex_ind_cat_map.end());
            auto cat_it2 = rnn_super.m_lex_cat_str_ind_map.find(cat_it->second);
            assert(cat_it2 != rnn_super.m_lex_cat_str_ind_map.end());
            //cerr << "tag ind: " << cat_it2->second << endl;
            one_tag_seq.emplace_back(cat_it2->second);
          }
          hypo = hypo->GetPrevHypo();
          if (hypo->IsStartHypo())
            break;
        }
        assert(one_tag_seq.size() == sent.words.size());
        std::reverse(one_tag_seq.begin(), one_tag_seq.end());
        all_tag_seq.emplace_back(one_tag_seq);
      }

      // compute total log prob for all tag sequences
      mat all_tag_seq_prob(kbest_output_vec.size(), 1, fill::zeros);
      for (size_t i = 0; i < ind; ++i) {
        mat tag_prob_vals(1, sent.words.size(), fill::zeros);
        assert(sent.all_ly_vals.size() == sent.words.size());
        for (size_t k = 0; k < sent.words.size(); ++k) {
          tag_prob_vals(k) = sent.all_ly_vals[k](all_tag_seq[i][k]);
        }
        all_tag_seq_prob(i) = accu(log(tag_prob_vals));
      }

      // normalize
      all_tag_seq_prob = soft_max(all_tag_seq_prob);
      // compute pxf1f1
      mat tagger_xf1(kbest_output_vec.size(), 1, fill::zeros);
      tagger_xf1.fill(accu(all_tag_seq_prob % f1_colvec));
      mat tagger_pxf1f1 = all_tag_seq_prob % (tagger_xf1 - f1_colvec);
      return -tagger_xf1(0);

      // each word position keeps a map
      // key: tag ind val: set of hypos uses that tag
      // (ind must be consistent with the inds in the tagger, not the parser)
//      vector<unordered_map<size_t, vector<size_t>>> word_position_tag_hypos_map_vec;
//      word_position_tag_hypos_map_vec.reserve(sent.words.size());
//
//      for (size_t i = 0; i < sent.words.size(); ++i) {
//        // for each word position
//        unordered_map<size_t, vector<size_t>> map;
//        for (size_t j = 0; j < ind; ++j) {
//          // for each hypo, get tag_ind at word position i
//          size_t tag_ind = all_tag_seq[j][i];
//          auto it = map.find(tag_ind);
//          if (it != map.end()) {
//            it->second.emplace_back(j);
//          } else {
//            map.emplace(tag_ind, vector<size_t>(1,j));
//          }
//        }
//        word_position_tag_hypos_map_vec.emplace_back(map);
//      }

      //rnn_super.train_xf1_bi(sent, tagger_pxf1f1, all_tag_seq, word_position_tag_hypos_map_vec);

  //  }
    // }
    //}

    //debug, grad checking
//    double bp_grad_emb00 = (m_emb_cp(0,0) - m_emb(0,0))/m_lr;
//    double bg_grad_emb03 = (m_emb_cp(0,3) - m_emb(0,3))/m_lr;
//    double bp_grad_suf00 = (m_suf_cp(0,0) - m_suf(0,0))/m_lr;
//    double bg_grad_suf05 = (m_suf_cp(0,5) - m_suf(0,5))/m_lr;
//    double bp_grad_super09 = (m_supercat_emb_cp(0,9) - m_supercat_emb(0,9))/m_lr;
//    double bg_grad_super26 = (m_supercat_emb_cp(0,26) - m_supercat_emb(0,26))/m_lr;
//
//    cerr << "bp_grad_emb00: " << bp_grad_emb00 << endl;
//    cerr << "bg_grad_emb03: " << bg_grad_emb03 << endl;
//    cerr << "bp_grad_suf00: " << bp_grad_suf00 << endl;
//    cerr << "bg_grad_suf05: " << bg_grad_suf05 << endl;
//    cerr << "bp_grad_super09: " << bp_grad_super09 << endl;
//    cerr << "bp_grad_super26: " << bg_grad_super26 << endl;

    //debug
    //    cerr << "avg prev hypo count: " << m_avg_unique_prev_hypo_count/total_steps << endl;
    //    m_total_unique_prev_hypo_count +=  m_avg_unique_prev_hypo_count/total_steps;
    //    cerr << "total prev hypo count: " << m_total_unique_prev_hypo_count/1913.0 << endl;

//    if (candidate_output != 0)
//      return candidate_output;
//
//    return 0;
  } catch (NLP::Exception e) {
    //throw NLP::ParseError(e.msg, nsentences);
    cerr << e.msg << endl;
    return 0;
  }
}

const ShiftReduceHypothesis*
RNNParser::_Impl::sr_parse_beam_search_train_xf1(NLP::Taggers::RnnTagger &rnn_super,
                                                 const size_t k, const size_t sent_id,
                                                 const bool oracle, const bool train,
                                                 double &total_xf1,
                                                 double &total_parses,
                                                 const size_t &num_threads,
                                                 const bool &bptt_graph,
                                                 const bool &run_training) {
  try {
    // xf1 training related
    //debug
    m_avg_unique_prev_hypo_count = 0.0;
    vector<double> seq_score_vec;
    vector<double> f1_vec;
    m_total_reduce_count = 0; // reset total reduce count
    m_h_tm1.zeros();   // reset hidden states
    const ShiftReduceHypothesis *best_hypo; // the highest-scored hypo on each level
    //    const ShiftReduceHypothesis *gold_hypo;
    //    vector<ShiftReduceAction*> &gold_tree_actions = *m_goldTreeVec.at(sent_id);
    //    vector<pair<size_t, size_t> > &gold_tree_actions_rnn_fmt = m_gold_tree_vec_rnn[sent_id];
    //    size_t gold_tree_size = gold_tree_actions.size();
    //    assert(gold_tree_actions.size() == gold_tree_actions_rnn_fmt.size());
    //    ShiftReduceAction *gold_action = 0;
    //    pair<size_t, size_t> gold_action_rnn_fmt;
    //size_t gold_action_id = 0;
    bool gold_finished = false;
    std::priority_queue<ShiftReduceHypothesis*, vector<ShiftReduceHypothesis*>, Hypo_comp> kbest_output_queue;

    if (sent.words.size() > cfg.maxwords())
      throw NLP::ParseError("sentence length exceeds maximum number of words for parser", nsentences);

    const size_t NWORDS = sent.words.size();
    const ShiftReduceHypothesis *candidate_output = 0;
    //    Words words;
    //    raws2words(sent.words, words);
    //    Words tags;
    //    raws2words(sent.pos, tags);

    // inds from the input sentence
    vector<size_t> word_inds_vec;
    vector<size_t> suf_inds_vec;

    words2word_inds(sent.words, sent.pos, word_inds_vec, suf_inds_vec);
    //    if (cfg.use_pos())
    //      pos2pos_inds(sent.pos, suf_inds_vec);
    //    else
    //      suf2suf_inds(sent_rnn_fmt.pos, suf_inds_vec);
    assert(word_inds_vec.size() == suf_inds_vec.size());

    nsentences++;
    SuperCat::nsupercats = 0;

    //const MultiRaws &input = sent.msuper;

    m_lattice = new ShiftReduceLattice(NWORDS, m_beamSize, MAX_UNARY_ACTIONS);
    ShiftReduceHypothesis *start_hypo = new ShiftReduceHypothesis(0, 0, 0, 0, 0, 0, 0.0, false, 0, 0, 0, 0, 0.0, 0);
    start_hypo->SetStartHypoFlag();
    m_lattice->Add(0, start_hypo);
    //start_hypo->m_hidden_states = m_h_tm1; // gets zero vec
    //gold_hypo = m_lattice->GetLatticeArray(0); // gold == start
    size_t startIndex;

    cerr << "sent: " << sent_id << endl;

    // all context, target, and feasible target values
    // for this sentence
    //vector<vector<vector<size_t> > > x_vals_sent;
    //vector<size_t> y_vals_sent;
    vector<vector<size_t> > feasible_y_vals_sent;

    //debug
    double total_steps = 0.0;
    // set start and end indexes of the lattice
    while (m_lattice->GetEndIndex() == 0 ||
        m_lattice->GetEndIndex() > m_lattice->GetPrevEndIndex()) {
      ++total_steps;
      if (m_lattice->GetEndIndex() != 0)
        startIndex = m_lattice->GetPrevEndIndex() + 1;
      else
        startIndex = 0;
      m_lattice->SetPrevEndIndex(m_lattice->GetEndIndex());

      //      if (gold_action_id < gold_tree_size)
      //      {
      //        gold_action = gold_tree_actions[gold_action_id];
      //        assert(gold_action);
      //        //cerr << "gold: " << gold_action->GetAction() << " "
      //        //<< gold_action->GetCat()->out_novar_noX_noNB_SR_str() << endl;
      //        gold_action_rnn_fmt = gold_tree_actions_rnn_fmt[gold_action_id];
      //        assert(gold_action_rnn_fmt.first == gold_action->GetAction());
      //      }

      HypothesisPQueue hypoQueue;
      for (size_t i = startIndex; i <= m_lattice->GetEndIndex(); ++i) {
        //assert(m_lattice->GetEndIndex() - startIndex == 0);
        ShiftReduceHypothesis *hypo = const_cast<ShiftReduceHypothesis *>(m_lattice->GetLatticeArray(i));
        size_t j = hypo->GetNextInputIndex();

        //        if (hypo->IsFinished(NWORDS, m_allowFragTree)) {
        //          if (candidate_output == 0 || (hypo->GetTotalScore() / hypo->GetTotalActionCount())
        //               > (candidate_output->GetTotalScore() / candidate_output->GetTotalActionCount())) {
        //            candidate_output = hypo;
        //          }
        //          kbest_output_queue.push(hypo);
        //        }

        //vector<vector<size_t>> context_vec;
        // inds of the current context
        vector<size_t> word_inds_ret;
        vector<size_t> suf_inds_ret;
        vector<size_t> supercat_inds_ret;
        vector<size_t> phrase_emb_ind_ret;
        vector<size_t> child_phrase_emb_ind_ret; // dummy, won't be used at test time
        hypo->m_context_vec.reserve(4); // TODO: remove this hard-coded 4
        word_inds_ret.reserve(m_feature_count);
        suf_inds_ret.reserve(m_feature_count);
        supercat_inds_ret.reserve(m_supercat_feature_count);

        //word_inds_ret.clear();
        //suf_inds_ret.clear();
        //supercat_inds_ret.clear();
        //phrase_emb_ind_ret.clear();
        //child_phrase_emb_ind_ret.clear();

        ContextLoader context_loader(m_cat_ind, m_head_limit, false);
        context_loader.load_context(hypo, word_inds_ret,
                                    suf_inds_ret, supercat_inds_ret,
                                    phrase_emb_ind_ret, child_phrase_emb_ind_ret,
                                    word_inds_vec, suf_inds_vec,
                                    m_cats_map, cats, cfg.use_phrase_emb_feats(), false);
        assert(word_inds_ret.size() == suf_inds_ret.size());
        assert(word_inds_ret.size() == m_feature_count);
        assert(supercat_inds_ret.size() == m_supercat_feature_count);

        hypo->m_context_vec.emplace_back(word_inds_ret);
        hypo->m_context_vec.emplace_back(suf_inds_ret);
        hypo->m_context_vec.emplace_back(supercat_inds_ret);
        hypo->m_context_vec.emplace_back(phrase_emb_ind_ret);

        // precomputation related
        //        if (cfg.precompute()) {
        //          for (size_t w_i = 0; w_i < word_inds_ret.size(); ++w_i) {
        //            // for each word position
        //            auto iter = m_word_feat_emb_ind_map.find(w_i);
        //            if (iter == m_word_feat_emb_ind_map.end()) {
        //              m_word_feat_emb_ind_map.insert({w_i, unordered_map<size_t, size_t>({{word_inds_ret[w_i], 1}})});
        //            } else {
        //              // find this emb ind
        //              auto iter2 = iter->second.find(word_inds_ret[w_i]);
        //
        //              if (iter2 == iter->second.end()) {
        //                assert(iter->second.insert({word_inds_ret[w_i], 1}).second);
        //              } else {
        //                iter2->second += 1;
        //              }
        //            }
        //
        //            // for each suf position
        //            auto iter3 = m_suf_feat_emb_ind_map.find(w_i);
        //            if (iter3 == m_suf_feat_emb_ind_map.end()) {
        //              m_suf_feat_emb_ind_map.insert({w_i, unordered_map<size_t, size_t>({{suf_inds_ret[w_i], 1}})});
        //            } else {
        //              // find this emb ind
        //              auto iter4 = iter3->second.find(suf_inds_ret[w_i]);
        //
        //              if (iter4 == iter3->second.end()) {
        //                assert(iter3->second.insert({suf_inds_ret[w_i], 1}).second);
        //              } else {
        //                iter4->second += 1;
        //              }
        //            }
        //          }
        //        }

        //cerr << hypo->m_context_vec[0][0] << endl;
        //x_vals_sent.push_back(context_vec);

        //bool found_gold_action = false;
        //size_t gold_action_class = 0;

        // feasible target values for the current context
        // the map is a check to make sure all the feasible
        // target values for the current context are unique
        // feasible_y_vals_map: key: output class (action id), value: (S/R/U, result cat)
        vector<size_t> feasible_y_vals;
        unordered_map<size_t, pair<size_t, SuperCat*> > feasible_y_vals_map;

        // find all feasible actions,
        // but do not push them into the queue

        // shift
        if (j < NWORDS) {
          const MultiRaw &multi = sent.msuper[j];
          for (MultiRaw::const_iterator k = multi.begin(); k!= multi.end(); ++k) {
            const Cat *cat = cats.markedup[k->raw];

            if(!cat) {
              cerr << "SHIFT action error: attempted to load category without markedup "
                  << k->raw << endl;
              exit (EXIT_FAILURE);
            }

            assert(m_lex_cat_ind_map.find(k->raw) != m_lex_cat_ind_map.end());
            feasible_y_vals.push_back(m_lex_cat_ind_map[k->raw]);
            SuperCat *stack_top_cat = SuperCat::Lexical(chart.pool, j+1, cat, 0);
            //assert(m_lex_cat_ind_map[k->raw] == k->ind); //todo check this for xf1 super training
            assert(feasible_y_vals_map.insert(make_pair(m_lex_cat_ind_map[k->raw],
                                                        make_pair(0, stack_top_cat))).second);
          } // end for
        } // end if

        // unary
        if (hypo->GetStackTopSuperCat() != 0) {

          const SuperCat *sc = hypo->GetStackTopSuperCat();
          vector<SuperCat *> tmp;
          tmp.clear();

          if (!sc->IsLex() && !sc->IsTr()) {
            unary_tc(hypo->GetStackTopSuperCat(), false, tmp);
            unary_tr(sc, tmp);
          }
          else if (sc->IsLex() && !sc->IsTr()) {
            unary_tr(sc, tmp);
          }
          else if (!sc->IsLex() && sc->IsTr()) { /* do nothing */ }
          else if (sc->IsLex() && sc->IsTr()) { /* do nothing */ }

          for (vector<SuperCat *>::iterator j = tmp.begin(); j < tmp.end(); ++j) {
            SuperCat *stack_top_cat = *j;
            assert(stack_top_cat->left);
            feasible_y_vals.push_back(stack_top_cat->m_unary_id + m_unary_base_count);
            assert(feasible_y_vals_map.insert(make_pair(stack_top_cat->m_unary_id + m_unary_base_count,
                                                        make_pair(1, stack_top_cat))).second);
          }
        }

        // reduce
        if (hypo->GetPrvStack() != 0 && hypo->GetPrvStack()->GetStackTopSuperCat() != 0
            && hypo->GetStackTopSuperCat() != 0) {

          results.clear();
          if (!cfg.seen_rules() ||
              rule_instances(hypo->GetPrvStack()->GetStackTopSuperCat()->GetCat(), hypo->GetStackTopSuperCat()->GetCat()))
            rules(hypo->GetPrvStack()->GetStackTopSuperCat(), hypo->GetStackTopSuperCat(),
                  cfg.eisner_nf(), cfg.seen_rules(), cfg.question_rules(), results);

          if (results.size() > 0) {
            for (vector<SuperCat *>::iterator k = results.begin(); k < results.end(); ++k) {

              SuperCat *stack_top_cat = *k;

              if (stack_top_cat->m_reduce_id == 0) {
                feasible_y_vals.push_back(m_reduce_base_count + 1);
                assert(feasible_y_vals_map.insert(make_pair(m_reduce_base_count + 1,
                                                            make_pair(2, stack_top_cat))).second);
              } else {
                feasible_y_vals.push_back(m_reduce_base_count + stack_top_cat->m_reduce_id + 1);
                assert(feasible_y_vals_map.insert(make_pair(m_reduce_base_count + stack_top_cat->m_reduce_id + 1,
                                                            make_pair(2, stack_top_cat))).second);
              }
            }
          }
        }

        if (feasible_y_vals.size() > 0) {
          // has feasible actions, then
          // score current context

          //mat new_hidden_states;
          hypo->m_action_score_vec.set_size(1, feasible_y_vals.size());
          hypo->m_action_score_vec.zeros();
          const mat &prev_hidden_stats = hypo->IsStartHypo() ? m_h_tm1 : hypo->GetPrevHypo()->m_hidden_states;
          score_context_beam_search_xf1(hypo->m_context_vec, feasible_y_vals, hypo->m_action_score_vec,
                                        prev_hidden_stats, hypo->m_hidden_states, hypo->m_converted_context_vec);
          vector<double> temp(feasible_y_vals.begin(), feasible_y_vals.end());
          hypo->m_feasible_action_ids = mat(temp);
          //hypo->m_hidden_states_set = true;

          double total_score = 0.0;
          unordered_map<size_t, pair<size_t, SuperCat*> >::const_iterator action_iter;
          //size_t action_id = 0; // id of the output unit

          for (size_t action_ind = 0; action_ind < feasible_y_vals.size(); ++action_ind) {
            // TODO this is currently a naive implementation
            // ideally, we don't have to expand every
            // feasible action into the beam??

            action_iter = feasible_y_vals_map.find(feasible_y_vals[action_ind]);
            assert(action_iter != feasible_y_vals_map.end());

            pair<size_t, SuperCat*> feasible_action = action_iter->second;
            total_score = hypo->GetTotalScore() + log(hypo->m_action_score_vec(action_ind));  // log softmax probabilities
            //debug
            //cerr << "total score: " << total_score << endl;
            //total_score /= (double) (hypo->GetTotalActionCount() + 1);

            // do the action
            // shift
            if (feasible_action.first == SHIFT) {
              assert(j < NWORDS);

              assert(feasible_action.second->cat);
              //feasible_action.second->m_word_vec_ind = word_inds_vec[j];
              ShiftReduceAction *action_shift = new ShiftReduceAction(SHIFT, feasible_action.second);
              HypothesisTuple *new_hypo_tuple = new HypothesisTuple(hypo, action_shift, total_score, i, feasible_y_vals[action_ind]);
              hypoQueue.push(new_hypo_tuple);

              // unary
            } else if (feasible_action.first == UNARY) {

              assert(feasible_action.second->left);
              //assert(hypo->GetStackTopSuperCat()->m_word_vec_ind != -1);
              //feasible_action.second->m_word_vec_ind = hypo->GetStackTopSuperCat()->m_word_vec_ind;
              ShiftReduceAction *action_unary = new ShiftReduceAction(UNARY, feasible_action.second);
              HypothesisTuple *new_hypo_tuple = new HypothesisTuple(hypo, action_unary, total_score, i, feasible_y_vals[action_ind]);
              hypoQueue.push(new_hypo_tuple);

              // reduce
            } else if (feasible_action.first == COMBINE) {

              assert(hypo->GetPrvStack() != 0 && hypo->GetPrvStack()->GetStackTopSuperCat() != 0
                     && hypo->GetStackTopSuperCat() != 0);

              assert(feasible_action.second);
              ++m_total_reduce_count;
              //              compose_word_vec(hypo->GetPrvStack()->GetStackTopSuperCat()->m_word_vec_ind,
              //                             hypo->GetStackTopSuperCat()->m_word_vec_ind, feasible_action.second);
              ShiftReduceAction *action_combine = new ShiftReduceAction(COMBINE, feasible_action.second);
              HypothesisTuple *new_hypo_tuple = new HypothesisTuple(hypo, action_combine, total_score, i, feasible_y_vals[action_ind]);
              hypoQueue.push(new_hypo_tuple);

            } else {
              cerr << "best action not recognized...\n";
              exit (EXIT_FAILURE);
            }
          }

        } else {
          // no more feasible actions, candidate output
          kbest_output_queue.push(hypo);

          if (candidate_output == 0 ||
              (hypo->GetTotalScore() / hypo->GetTotalActionCount()) >
          (candidate_output->GetTotalScore() / candidate_output->GetTotalActionCount())) {
            candidate_output = hypo;
          }
        }
      } // end for

      if (!hypoQueue.empty()) {
        best_hypo = 0;
        const ShiftReduceHypothesis *gold_hypo = 0;
        ShiftReduceAction *gold_action = 0;
        // apply beam
        construct_hypo(hypoQueue, m_lattice, best_hypo, gold_hypo, gold_action, gold_finished);
      }

    } // end while

    double top_f1 = calc_f1(candidate_output, sent_id, train);
    cerr << "top output score: " << candidate_output->GetTotalScore() << " f1: " << top_f1 << endl;

    size_t ind = 0;
    vector<size_t> action_count_vec;
    action_count_vec.reserve(kbest_output_queue.size());

    vector<const ShiftReduceHypothesis*> kbest_output_vec;
    kbest_output_vec.reserve(kbest_output_queue.size());
    seq_score_vec.reserve(kbest_output_queue.size());
    f1_vec.reserve(kbest_output_queue.size());

    while (!kbest_output_queue.empty()) {
      const ShiftReduceHypothesis *hypo = kbest_output_queue.top();
      kbest_output_vec.emplace_back(hypo);

      seq_score_vec.emplace_back(hypo->GetTotalScore());
      double f1 = calc_f1(hypo, sent_id, train);
      f1_vec.emplace_back(f1);
      cerr << "score: " << hypo->GetTotalScore() << " f1: " << f1 << endl;

      if (oracle && f1 > top_f1) {
        candidate_output = hypo;
        top_f1 = f1;
      }

      action_count_vec.emplace_back(hypo->GetTotalActionCount());
      kbest_output_queue.pop();
      ++ind;
      //if (ind == k) break;
    }
    //assert(ind <= k);
    assert(ind == f1_vec.size());
    assert(ind == seq_score_vec.size());
    assert(ind == kbest_output_vec.size());
    total_parses += ind;
    colvec seq_score_colvec(seq_score_vec);
    colvec f1_colvec(f1_vec);
    colvec seq_prob_colvec(ind);
    colvec xf1_colvec(ind);

    // normalize
    seq_prob_colvec = soft_max(seq_score_colvec);
    // calculate xf1
    xf1_colvec.fill(accu(seq_prob_colvec % f1_colvec));
    // calculate p(seq)*(xf1-seq_f1)
    mat pxf1f1 = seq_prob_colvec % (xf1_colvec - f1_colvec);

    cerr << "xf1: " << xf1_colvec(0) << endl;
    total_xf1 += xf1_colvec(0);

    //debug, grad checking, parser
//    mat m_emb_cp = m_emb;
//    mat m_suf_cp = m_suf;
//    mat m_supercat_emb_cp = m_supercat_emb;

    //debug, grad checking, tagger
//    mat m_wx_cp = rnn_super.m_wx;
//    mat m_wh_cp = rnn_super.m_wh;
//    mat m_wy_cp = rnn_super.m_wy;
//    mat m_wx_back_cp = rnn_super.m_wx_back;
//    mat m_wh_back_cp = rnn_super.m_wh_back;
//    mat m_emb_cp = rnn_super.m_emb;
//    mat m_suf_cp = rnn_super.m_suf;
//    mat m_cap_cp = rnn_super.m_cap;

    // xf1 supertagging
    if (run_training && cfg.xf1_super()) {
      // get all tag sequences, ie all inds of tags
      vector<vector<size_t>> all_tag_seq;
      all_tag_seq.reserve(kbest_output_vec.size());
      for (size_t i = 0; i < ind; ++i) {
        //all_tag_seq[i].reserve(sent.words.size());
        vector<size_t> one_tag_seq;
        one_tag_seq.reserve(sent.words.size());
        const ShiftReduceHypothesis *hypo = kbest_output_vec[i];
        while (true) {
          if (hypo->GetStackTopAction() == SHIFT) {
            auto cat_it = m_lex_ind_cat_map.find(hypo->m_action_id);
            assert(cat_it != m_lex_ind_cat_map.end());
            auto cat_it2 = rnn_super.m_lex_cat_str_ind_map.find(cat_it->second);
            assert(cat_it2 != rnn_super.m_lex_cat_str_ind_map.end());
            one_tag_seq.emplace_back(cat_it2->second);
          }
          hypo = hypo->GetPrevHypo();
          if (hypo->IsStartHypo())
            break;
        }
//        assert(all_tag_seq[i].size() == sent.words.size());
//        std::reverse(all_tag_seq[i].begin(), all_tag_seq[i].end());
        assert(one_tag_seq.size() == sent.words.size());
        std::reverse(one_tag_seq.begin(), one_tag_seq.end());
        all_tag_seq.emplace_back(one_tag_seq);
      }

      // compute total log prob for all tag sequences
      mat all_tag_seq_prob(kbest_output_vec.size(), 1, fill::zeros);
      for (size_t i = 0; i < ind; ++i) {
        mat tag_prob_vals(1, sent.words.size(), fill::zeros);
        assert(sent.all_ly_vals.size() == sent.words.size());
        for (size_t k = 0; k < sent.words.size(); ++k) {
          tag_prob_vals(k) = sent.all_ly_vals[k](all_tag_seq[i][k]);
        }
        all_tag_seq_prob(i) = accu(log(tag_prob_vals));
      }

      // normalize
      all_tag_seq_prob = soft_max(all_tag_seq_prob);
      // compute pxf1f1
      mat tagger_xf1(kbest_output_vec.size(), 1, fill::zeros);
      tagger_xf1.fill(accu(all_tag_seq_prob % f1_colvec));
      mat tagger_pxf1f1 = all_tag_seq_prob % (tagger_xf1 - f1_colvec);

      // each word position keeps a map
      // key: tag ind val: set of hypos uses that tag
      // (ind must be consistent with the inds in the tagger, not the parser)
      vector<unordered_map<size_t, vector<size_t>>> word_position_tag_hypos_map_vec;
      word_position_tag_hypos_map_vec.reserve(sent.words.size());

      for (size_t i = 0; i < sent.words.size(); ++i) {
        // for each word position
        unordered_map<size_t, vector<size_t>> map;
        for (size_t j = 0; j < ind; ++j) {
          // for each hypo, get tag_ind at word position i
          size_t tag_ind = all_tag_seq[j][i];
          auto it = map.find(tag_ind);
          if (it != map.end()) {
            it->second.emplace_back(j);
          } else {
            map.emplace(tag_ind, vector<size_t>(1,j));
          }
        }
        word_position_tag_hypos_map_vec.emplace_back(map);
      }

      rnn_super.train_xf1_bi(sent, tagger_pxf1f1, all_tag_seq, word_position_tag_hypos_map_vec);

    } else if (run_training && bptt_graph) {
      cerr << "bptt graph...\n";
      assert(!cfg.xf1_super()); // no concurrent training for now
      // mark all hypos that are part of the kbest list
      for (size_t i = 0; i < ind; ++i) {
        mark_nodes(kbest_output_vec[i], i);
      }

      // traverse lattice and do bptt over the DAG in lattice
      for (int i = m_lattice->GetEndIndex(); i >= 0; --i) {
        const ShiftReduceHypothesis *hypo = m_lattice->GetLatticeArray(i);
        if (hypo->m_in_kbest && hypo->m_child_hypos.size() > 0) {
          mat &total_child_delta_h = hypo->m_total_child_delta_h;
          total_child_delta_h.set_size(m_nh, 1);
          total_child_delta_h.zeros();
          mat &total_delta_y = hypo->m_total_delta_y;
          total_delta_y.set_size(m_wy.n_cols, 1);
          total_delta_y.zeros(); // from all kbest hypo derived from this hidden state

          // bool softmax_prob_retrived = false;
          // aggregrate delta_h from all children
          // which are part of the kbest list
          for (auto &child : hypo->m_child_hypos) {
            if (child->m_in_kbest) {
              //TODO armadillo += problem
              total_child_delta_h = total_child_delta_h + child->m_delta_h_vec;
            }
          }

          // aggregate delta_y; each action emerging from the current hypo that
          // eventually leads to a complete hypo in the final kbest list will
          // have a contribution for delta_y
          // first,
          mat delta_y_vals(m_wy.n_cols, 1, fill::zeros);
          for (size_t j = 0; j < hypo->m_feasible_action_ids.n_elem; ++j) {
            delta_y_vals(hypo->m_feasible_action_ids(j)) = hypo->m_action_score_vec(j);
          }

          for (auto it = hypo->m_action_kbest_hypo_inds_map.begin(); it != hypo->m_action_kbest_hypo_inds_map.end(); ++it) {
            double action_score = delta_y_vals(it->first);
            mat delta_y_vals_one_ind = delta_y_vals;
            //    delta_y_vals[i] *= -action_score;
            //    delta_y_vals[i](y_ind_batch[i]) = action_score * (1.0 - action_score);
            //    delta_y_vals[i] *= (product * (1.0/action_score));
            delta_y_vals_one_ind *= -1.0;
            delta_y_vals_one_ind(it->first) = (1.0 - action_score);

            for (size_t ind : it->second) {
              mat delta_y_vals_one_ind_cp = delta_y_vals_one_ind;
              delta_y_vals_one_ind_cp *= pxf1f1(ind);
              // TODO try not to use += here, apparently armadillo has some
              // efficiency issues with it
              total_delta_y = total_delta_y + delta_y_vals_one_ind_cp;
            }
          }

          // compute delta_h for the current hypo
          mat one = ones<mat>(m_h_tm1.n_rows, m_h_tm1.n_cols);
          mat h_deriv_vals =  hypo->m_hidden_states % (one - hypo->m_hidden_states); // assumes sigmoid
          hypo->m_delta_h_vec = h_deriv_vals.t() % (m_wh*total_child_delta_h + m_wy*total_delta_y);
        } // end if
      } // end for

      mat grad_wh(m_wh.n_rows, m_wh.n_cols, fill::zeros);
      mat grad_wy(m_wy.n_rows, m_wy.n_cols, fill::zeros);
      for (int i = m_lattice->GetEndIndex(); i >= 0; --i) {
        const ShiftReduceHypothesis *hypo = m_lattice->GetLatticeArray(i);
        if (hypo->m_in_kbest && hypo->m_child_hypos.size() > 0) {
          grad_wh = grad_wh + (hypo->m_hidden_states.t() * hypo->m_total_child_delta_h.t());
          //m_wh -= m_lr*grad;
          grad_wy = grad_wy + (hypo->m_hidden_states.t() * hypo->m_total_delta_y.t());
          //m_wy -= m_lr*grad;
        }
      }

      mat grad_wx(m_wx.n_rows, m_wx.n_cols, fill::zeros);
      for (int i = m_lattice->GetEndIndex(); i >= 0; --i) {
        const ShiftReduceHypothesis *hypo = m_lattice->GetLatticeArray(i);
        if (hypo->m_in_kbest && hypo->m_child_hypos.size() > 0) {
          // no need to check if children are in kbest
          // if has children => current hypo in kbest
          // then compute grad_wx using delta_h stored
          // at current hypo
          // assuming dropout was used
          // wrong: mat delta_x = m_dropout_vec % (m_wx*hypo->m_total_child_delta_h);
          mat delta_x = m_dropout_vec % (m_wx*hypo->m_delta_h_vec);
          grad_wx += hypo->m_converted_context_vec*hypo->m_delta_h_vec.t();

          const vector<size_t> &word_inds = hypo->m_context_vec[0];
          const vector<size_t> &suf_inds = hypo->m_context_vec[1];

          assert(word_inds.size() == suf_inds.size());

          // all start inds must be set here
          int start = 0;
          int start2 = m_de*word_inds.size(); // dim of word emb x num of embs
          int start3 = start2 + m_ds*word_inds.size();
          //    int start4 = 0;

          for (size_t k = 0; k < word_inds.size(); ++k) {
            //cerr << "single t: " << word_inds[k] << endl;
            //delta_x_vals[i].rows(start, start + m_de - 1).print(cerr);
            m_emb.col(word_inds[k]) -=
                m_lr*delta_x.rows(start, start + m_de - 1);
            m_suf.col(suf_inds[k]) -=
                m_lr*delta_x.rows(start2, start2 + m_ds - 1);
            //      m_cap.col(cap_inds[k]) -=
            //          m_lr*delta_x_vals[i].rows(start3, start3 + m_dc - 1);
            start += m_de;
            start2 += m_ds;
          }

          if (m_use_supercat_feats) {
            assert(hypo->m_context_vec.size() >= 3);
            assert(start2 == start3);

            const vector<size_t> &supercat_inds = hypo->m_context_vec[2];
            //start4 = start3 + m_dsuper*supercat_inds.size();

            for (size_t k = 0; k < supercat_inds.size(); ++k) {
              m_supercat_emb.col(supercat_inds[k]) -=
                  m_lr*delta_x.rows(start3, start3 + m_dsuper - 1);
              start3 += m_dsuper;
            }
          }

        }
      }
      cerr << "update wx wh wy..." << endl;
      m_wx -= m_lr*grad_wx;
      m_wh -= m_lr*grad_wh;
      m_wy -= m_lr*grad_wy;
      cerr << "graph bp_grad_wx: " << grad_wx(12,12) << endl;
      cerr << "graph bp_grad_wh: " << grad_wh(12,12) << endl;
      cerr << "graph bp_grad_wy: " << grad_wy(0,0) << endl;


    } else if (run_training && !bptt_graph) {
      cerr << "naive xf1 bptt" << endl;
      assert(!cfg.xf1_super()); // no concurrent training for now
      if (num_threads > 1) {
        vector<mat> grad_wx_mats(ind, mat(m_wx.n_rows, m_wx.n_cols, fill::zeros));
        vector<mat> grad_wh_mats(ind, mat(m_wh.n_rows, m_wh.n_cols, fill::zeros));
        vector<mat> grad_wy_mats(ind, mat(m_wy.n_rows, m_wy.n_cols, fill::zeros));

        mat grad_wx_total(m_wx.n_rows, m_wx.n_cols, fill::zeros);
        mat grad_wh_total(m_wh.n_rows, m_wh.n_cols, fill::zeros);
        mat grad_wy_total(m_wy.n_rows, m_wy.n_cols, fill::zeros);

        vector<vector<mat>> all_delta_x_mats;
        all_delta_x_mats.reserve(ind);
        for (size_t i = 0; i < ind; ++i) {
          all_delta_x_mats.emplace_back(vector<mat>(action_count_vec[i], mat(m_wx.n_rows, 1, fill::zeros)));
        }

        // entry point for xf1 training
        m_h_tm1.zeros();
        size_t hypo_ind = 0;
        while (hypo_ind < ind) {
          std::vector<thread> threads;
          for (size_t i = 0; i < num_threads; ++i) {
            size_t hypo_ind_cp = hypo_ind;
            threads.push_back(thread([&, hypo_ind_cp]{train_xf1_mt(ref(pxf1f1(hypo_ind_cp)), ref(kbest_output_vec[hypo_ind_cp]),
                                                                   ref(grad_wx_mats[hypo_ind_cp]),
                                                                   ref(grad_wh_mats[hypo_ind_cp]),
                                                                   ref(grad_wy_mats[hypo_ind_cp]),
                                                                   ref(all_delta_x_mats[hypo_ind_cp]));}));
            ++hypo_ind;
            if (hypo_ind == ind)
              break;
          }

          for (thread &th : threads) {
            th.join();
          }
        }

        for (size_t i = 0; i < ind; ++i) {
          grad_wx_total += grad_wx_mats[i];
          grad_wh_total += grad_wh_mats[i];
          grad_wy_total += grad_wy_mats[i];
        }

        cerr << "bp_grad_wx: " << grad_wx_total(12,12) << endl;
        cerr << "bp_grad_wh: " << grad_wh_total(12,12) << endl;
        cerr << "bp_grad_wy: " << grad_wy_total(0,0) << endl;

        m_wh -= m_lr*grad_wh_total;
        m_wy -= m_lr*grad_wy_total;
        m_wx -= m_lr*grad_wx_total;

        // update embeddings, in the single-thread
        // version this is done in the bptt function
        for (size_t h = 0; h < ind; ++h) {
          const size_t steps = kbest_output_vec[h]->GetTotalActionCount();
          //cerr << "steps: " << steps << endl;
          assert(steps == all_delta_x_mats[h].size());
          const ShiftReduceHypothesis *curr_hypo = kbest_output_vec[h];
          for (int i = steps - 1; i >= 0; --i) {
            //const vector<size_t> &word_inds = context_batch[i][0];
            //const vector<size_t> &suf_inds = context_batch[i][1];
            const vector<size_t> &word_inds = curr_hypo->GetPrevHypo()->m_context_vec[0];
            const vector<size_t> &suf_inds = curr_hypo->GetPrevHypo()->m_context_vec[1];
            assert(word_inds.size() == suf_inds.size());

            // all start inds must be set here
            int start = 0;
            int start2 = m_de*word_inds.size(); // dim of word emb x num of embs
            int start3 = start2 + m_ds*word_inds.size();
            //int start4 = 0;

            for (size_t k = 0; k < word_inds.size(); ++k) {
              //cerr << "n_rows: " << all_delta_x_mats[h][i] << endl;
              //cerr << "mt: " << word_inds[k] << endl;
              //all_delta_x_mats[h][i].rows(start, start + m_de - 1).print(cerr);

              m_emb.col(word_inds[k]) -= m_lr*all_delta_x_mats[h][i].rows(start, start + m_de - 1);
              //if (word_inds[k] == 0)
              //bp_grad_emb00 += all_delta_x_mats[h][i].rows(start, start + m_de - 1)(0);
              //m_lr*delta_x_vals[i].rows(start, start + m_de - 1);
              //cerr << "suf: " << suf_inds[k] << endl;
              m_suf.col(suf_inds[k]) -= m_lr*all_delta_x_mats[h][i].rows(start2, start2 + m_ds - 1);
              //m_lr*delta_x_vals[i].rows(start2, start2 + m_ds - 1);
              //      m_cap.col(cap_inds[k]) -=
              //          m_lr*delta_x_vals[i].rows(start3, start3 + m_dc - 1);

              start += m_de;
              start2 += m_ds;
            }

            if (m_use_supercat_feats) {
              //assert(context_batch[i].size() >= 3);
              assert(curr_hypo->GetPrevHypo()->m_context_vec.size() >= 3);
              assert(start2 == start3);

              //const vector<size_t> &supercat_inds = context_batch[i][2];
              const vector<size_t> &supercat_inds = curr_hypo->GetPrevHypo()->m_context_vec[2];
              //start4 = start3 + m_dsuper*supercat_inds.size();

              for (size_t k = 0; k < supercat_inds.size(); ++k) {
                //cerr << "super: " << supercat_inds[k] << endl;
                m_supercat_emb.col(supercat_inds[k]) -=  m_lr*all_delta_x_mats[h][i].rows(start3, start3 + m_dsuper - 1);
                //    m_lr*delta_x_vals[i].rows(start3, start3 + m_dsuper - 1);
                start3 += m_dsuper;
              }
            }

            curr_hypo = curr_hypo->GetPrevHypo();
          }
          assert(curr_hypo->IsStartHypo());
        }
      } else {
        //        mat grad_wx;
        //        mat grad_wh;
        //        mat grad_wy;
        //        grad_wx.copy_size(m_wx);
        //        grad_wh.copy_size(m_wh);
        //        grad_wy.copy_size(m_wy);
        //        grad_wx.zeros();
        //        grad_wh.zeros();
        //        grad_wy.zeros();

        vector<mat> grad_wx_mats(ind, mat(m_wx.n_rows, m_wx.n_cols, fill::zeros));
        vector<mat> grad_wh_mats(ind, mat(m_wh.n_rows, m_wh.n_cols, fill::zeros));
        vector<mat> grad_wy_mats(ind, mat(m_wy.n_rows, m_wy.n_cols, fill::zeros));

        mat grad_wx_total(m_wx.n_rows, m_wx.n_cols, fill::zeros);
        mat grad_wh_total(m_wh.n_rows, m_wh.n_cols, fill::zeros);
        mat grad_wy_total(m_wy.n_rows, m_wy.n_cols, fill::zeros);

        m_h_tm1.zeros();
        // entry point for xf1 training
        for (size_t hypo_ind = 0; hypo_ind < ind; ++hypo_ind) {
          train_xf1(pxf1f1(hypo_ind), kbest_output_vec[hypo_ind],
                    grad_wx_mats[hypo_ind], grad_wh_mats[hypo_ind], grad_wy_mats[hypo_ind]);
        }
        //        for (size_t hypo_ind = 0; hypo_ind < ind; ++hypo_ind) {
        //          train_xf1(pxf1f1(hypo_ind), kbest_output_vec[hypo_ind], grad_wx, grad_wh, grad_wy);
        //        }

        for (size_t i = 0; i < ind; ++i) {
          grad_wx_total += grad_wx_mats[i];
          grad_wh_total += grad_wh_mats[i];
          grad_wy_total += grad_wy_mats[i];
        }

        cerr << "bp_grad_wx: " << grad_wx_total(12,12) << endl;
        cerr << "bp_grad_wh: " << grad_wh_total(12,12) << endl;
        cerr << "bp_grad_wy: " << grad_wy_total(0,0) << endl;

        m_wh -= m_lr*grad_wh_total;
        m_wy -= m_lr*grad_wy_total;
        m_wx -= m_lr*grad_wx_total;


        //        cerr << "bp_grad_wx: " << grad_wx(12,12) << endl;
        //        cerr << "bp_grad_wh: " << grad_wh(12,12) << endl;
        //        cerr << "bp_grad_wy: " << grad_wy(0,0) << endl;
        //
        //        m_wh -= m_lr*grad_wh;
        //        m_wy -= m_lr*grad_wy;
        //        m_wx -= m_lr*grad_wx;
      }
    }
    // }
    //}

    //debug, grad checking, parser
//    double bp_grad_emb00 = (m_emb_cp(0,0) - m_emb(0,0))/m_lr;
//    double bg_grad_emb03 = (m_emb_cp(0,3) - m_emb(0,3))/m_lr;
//    double bp_grad_suf00 = (m_suf_cp(0,0) - m_suf(0,0))/m_lr;
//    double bg_grad_suf05 = (m_suf_cp(0,5) - m_suf(0,5))/m_lr;
//    double bp_grad_super09 = (m_supercat_emb_cp(0,9) - m_supercat_emb(0,9))/m_lr;
//    double bg_grad_super26 = (m_supercat_emb_cp(0,26) - m_supercat_emb(0,26))/m_lr;
//
//    cerr << "bp_grad_emb00: " << bp_grad_emb00 << endl;
//    cerr << "bg_grad_emb03: " << bg_grad_emb03 << endl;
//    cerr << "bp_grad_suf00: " << bp_grad_suf00 << endl;
//    cerr << "bg_grad_suf05: " << bg_grad_suf05 << endl;
//    cerr << "bp_grad_super09: " << bp_grad_super09 << endl;
//    cerr << "bp_grad_super26: " << bg_grad_super26 << endl;

    //debug, grad checking, tagger
//    double bp_grad_wx = (m_wx_cp(12,12) - rnn_super.m_wx(12,12))/rnn_super.m_lr;
//    double bp_grad_wh = (m_wh_cp(12,12) - rnn_super.m_wh(12,12))/rnn_super.m_lr;
//    double bp_grad_wy = (m_wy_cp(0,63) - rnn_super.m_wy(0,63))/rnn_super.m_lr;
//
//    double bp_grad_wx_back = (m_wx_back_cp(12,12) - rnn_super.m_wx_back(12,12))/rnn_super.m_lr;
//    double bp_grad_wh_back = (m_wh_back_cp(12,12) - rnn_super.m_wh_back(12,12))/rnn_super.m_lr;
//
//    double bp_grad_emb00 = (m_emb_cp(0,0) - rnn_super.m_emb(0,0))/rnn_super.m_lr;
//    double bp_grad_emb05 = (m_emb_cp(0,5) - rnn_super.m_emb(0,5))/rnn_super.m_lr;
//    double bp_grad_suf00 = (m_suf_cp(0,0) - rnn_super.m_suf(0,0))/rnn_super.m_lr;
//    double bp_grad_suf3209 = (m_suf_cp(3,209) - rnn_super.m_suf(3,209))/rnn_super.m_lr;
//    double bp_grad_cap00 = (m_cap_cp(0,0) - rnn_super.m_cap(0,0))/rnn_super.m_lr;
//    double bp_grad_cap01 = (m_cap_cp(0,1) - rnn_super.m_cap(0,1))/rnn_super.m_lr;
//
//    cerr << "bp_grad_wx: " << bp_grad_wx << endl;
//    cerr << "bp_grad_wh: " << bp_grad_wh << endl;
//    cerr << "bp_grad_wh_back: " << bp_grad_wh_back << endl;
//    cerr << "bp_grad_wx_back: " << bp_grad_wx_back << endl;
//    cerr << "bp_grad_wy: " << bp_grad_wy << endl;
//
//    cerr << "bp_grad_emb00: " << bp_grad_emb00 << endl;
//    cerr << "bp_grad_emb05: " << bp_grad_emb05 << endl;
//    cerr << "bp_grad_suf00: " << bp_grad_suf00 << endl;
//    cerr << "bp_grad_suf3209: " << bp_grad_suf3209 << endl;
//    cerr << "bp_grad_cap00: " << bp_grad_cap00 << endl;
//    cerr << "bp_grad_cap01: " << bp_grad_cap01 << endl;

    //debug
    //    cerr << "avg prev hypo count: " << m_avg_unique_prev_hypo_count/total_steps << endl;
    //    m_total_unique_prev_hypo_count +=  m_avg_unique_prev_hypo_count/total_steps;
    //    cerr << "total prev hypo count: " << m_total_unique_prev_hypo_count/1913.0 << endl;

    if (candidate_output != 0)
      return candidate_output;

    return 0;
  } catch (NLP::Exception e) {
    //throw NLP::ParseError(e.msg, nsentences);
    cerr << e.msg << endl;
    return 0;
  }
}


// given a hypo from kbest list,
// mark all prev_hypos of it in the lattice
void
RNNParser::_Impl::mark_nodes(const ShiftReduceHypothesis *&hypo, const size_t &ind) {
  hypo->m_in_kbest = true;
  const ShiftReduceHypothesis *curr_hypo = hypo;
  const ShiftReduceHypothesis *prev_hypo = curr_hypo->GetPrevHypo();
  hypo->m_delta_h_vec.set_size(m_nh, 1);
  hypo->m_delta_h_vec.zeros();
  size_t total_marks = 1;
  while (true) {
    ++total_marks;
    auto iter = prev_hypo->m_action_kbest_hypo_inds_map.find(curr_hypo->m_action_id);
    if (iter != prev_hypo->m_action_kbest_hypo_inds_map.end()) {
      iter->second.push_back(ind);
    } else {
      prev_hypo->m_action_kbest_hypo_inds_map.insert({curr_hypo->m_action_id, vector<size_t>(1,ind)});
    }
    prev_hypo->m_in_kbest = true;
    if (prev_hypo->IsStartHypo()) {
      break;
    }
    curr_hypo = prev_hypo;
    prev_hypo = prev_hypo->GetPrevHypo();
  }
  assert(total_marks = hypo->m_totalActionCount + 1);
}


// bptt over one action sequence for xf1 training
void
RNNParser::_Impl::train_xf1(double &product, const ShiftReduceHypothesis *&hypo,
                            mat &grad_wx, mat &grad_wh, mat &grad_wy) {
  size_t total_steps = hypo->GetTotalActionCount();
  //cerr << "total_steps: " << total_steps << endl;   //debug
  mat h_vals[total_steps + 1];
  //m_h_tm1.zeros(); moved up
  h_vals[0] = m_h_tm1;
  mat h_deriv_vals[total_steps];
  mat y_vals[total_steps];
  mat y_all_ind_vals[total_steps]; // all feasible ids at each step for all steps
  size_t y_ind_vals[total_steps]; // the feasible ids for all steps, one id per step
  mat x_vals[total_steps]; // concatenated embeddings
  vector<vector<size_t>> context_vecs[total_steps]; // inds for the embeddings

  const ShiftReduceHypothesis *current_hypo = hypo;
  assert(current_hypo->GetPrevHypo());
  for (int i = total_steps; i > 0; --i) {
    h_vals[i] = current_hypo->GetPrevHypo()->m_hidden_states;
    mat one = ones<mat>(m_h_tm1.n_rows, m_h_tm1.n_cols);
    h_deriv_vals[i - 1] = h_vals[i] % (one - h_vals[i]);

    y_ind_vals[i - 1] = current_hypo->m_action_id;
    y_all_ind_vals[i - 1] = current_hypo->GetPrevHypo()->m_feasible_action_ids;
    y_vals[i - 1] = current_hypo->GetPrevHypo()->m_action_score_vec; // for feasible actions only
    assert(y_all_ind_vals[i - 1].n_elem == y_vals[i - 1].n_elem);

    x_vals[i - 1] = current_hypo->GetPrevHypo()->m_converted_context_vec;
    context_vecs[i - 1] = current_hypo->GetPrevHypo()->m_context_vec;
    current_hypo = current_hypo->GetPrevHypo();
  }
  assert(current_hypo->IsStartHypo());

  if (!cfg.xf1_mini_batch()) {
    train_xf1_bptt(total_steps, product, context_vecs,
                   x_vals, h_vals, h_deriv_vals,
                   y_vals, y_ind_vals, y_all_ind_vals,
                   grad_wx, grad_wh, grad_wy);
  } else {
    vector<mat> x_val_batch;
    vector<vector<vector<size_t>>> context_batch;
    vector<mat> h_val_batch;
    vector<mat> h_deriv_val_batch;
    vector<mat> y_val_batch;
    vector<size_t> y_ind_batch;
    vector<mat> y_all_ind_batch;

    x_val_batch.reserve(m_bs);
    context_batch.reserve(m_bs);
    h_val_batch.reserve(m_bs);
    h_deriv_val_batch.reserve(m_bs);
    y_val_batch.reserve(m_bs);
    y_ind_batch.reserve(m_bs);
    y_all_ind_batch.reserve(m_bs);

    for (size_t i = 0; i < total_steps; ++i) {
      x_val_batch.emplace_back(x_vals[i]);
      context_batch.emplace_back(context_vecs[i]);

      h_val_batch.emplace_back(h_vals[i]);
      h_deriv_val_batch.emplace_back(h_deriv_vals[i]);

      y_ind_batch.emplace_back(y_ind_vals[i]);
      y_val_batch.emplace_back(y_vals[i]);
      y_all_ind_batch.emplace_back(y_all_ind_vals[i]);
      assert(y_all_ind_vals[i].n_elem == y_vals[i].n_elem);

      if ((i + 1) % m_bs == 0 || i == total_steps - 1) {
        assert(total_steps = x_val_batch.size());
        h_val_batch.emplace_back(h_vals[i+1]);
        train_xf1_bptt(x_val_batch.size(), product, context_batch,
                       x_val_batch, h_val_batch, h_deriv_val_batch,
                       y_val_batch, y_ind_batch, y_all_ind_batch,
                       grad_wx, grad_wh, grad_wy);
        x_val_batch.clear();
        context_batch.clear();
        h_val_batch.clear();
        h_deriv_val_batch.clear();
        y_val_batch.clear();
        y_ind_batch.clear();
        y_all_ind_batch.clear();
      }
    }
  }
}


void
RNNParser::_Impl::train_xf1_bptt(const size_t steps, double &product, const vector<vector<size_t>> *context_batch,
                                 const mat *x_vals, const mat *h_vals,
                                 const mat *h_deriv_vals, const mat *y_val_batch,
                                 const size_t *y_ind_batch, const mat *y_all_ind_batch,
                                 mat &grad_wx, mat &grad_wh, mat &grad_wy) {
  mat delta_y_vals[steps];
  mat delta_h_vals[steps];
  mat delta_x_vals[steps];

  for (size_t i = 0; i < steps; ++i) {
    delta_y_vals[i] = zeros<mat>(m_wy.n_cols, 1);
    for (size_t j = 0; j < y_all_ind_batch[i].n_elem; ++j) {
      delta_y_vals[i](y_all_ind_batch[i](j)) = y_val_batch[i](j);
    }

    double action_score = delta_y_vals[i](y_ind_batch[i]);
    //    delta_y_vals[i] *= -action_score;
    //    delta_y_vals[i](y_ind_batch[i]) = action_score * (1.0 - action_score);
    //    delta_y_vals[i] *= (product * (1.0/action_score));
    delta_y_vals[i] *= -1.0;
    delta_y_vals[i](y_ind_batch[i]) = (1.0 - action_score);
    delta_y_vals[i] *= product;
  }

  // bptt multi
  delta_h_vals[steps - 1] = h_deriv_vals[steps - 1].t() % (m_wy*delta_y_vals[steps - 1]);

  for (size_t i = steps; i > 1; --i) {
    delta_h_vals[i - 2] = h_deriv_vals[i - 2].t() %
        ((m_wh*delta_h_vals[i - 1]) + m_wy*(delta_y_vals[i - 2]));
  }

  //double back_grad_wh = 0.0;
  for (size_t i = 0; i < steps; ++i) {
    mat grad = h_vals[i].t()*delta_h_vals[i].t();
    //cerr << "grad_test: " << grad(12,12) << endl;
    grad_wh += grad;
    //back_grad_wh += grad(0,0);
  }

  //double back_grad_wy = 0.0;
  for (size_t i = 1; i <= steps; ++i) {
    mat grad = h_vals[i].t()*delta_y_vals[i - 1].t();
    grad_wy += grad;
    //back_grad_wy += grad(0,0);
  }

  for (size_t i = 0; i < steps; ++i) {
    if (cfg.use_dropout()) {
      delta_x_vals[i] = m_dropout_vec % (m_wx*delta_h_vals[i]);
    } else {
      delta_x_vals[i] = m_wx*delta_h_vals[i];
    }
  }

  //double back_grad_wx = 0.0;
  for (size_t i = 0; i < steps; ++i) {
    mat grad = x_vals[i]*delta_h_vals[i].t();
    //grad_wx += grad; //TODO: armadillo in_place problem
    grad_wx = grad_wx + grad;
    //back_grad_wx += grad(0,0);
  }

  for (size_t i = 0; i < steps; ++i) {

    const vector<size_t> &word_inds = context_batch[i][0];
    const vector<size_t> &suf_inds = context_batch[i][1];

    assert(word_inds.size() == suf_inds.size());

    // all start inds must be set here
    int start = 0;
    int start2 = m_de*word_inds.size(); // dim of word emb x num of embs
    int start3 = start2 + m_ds*word_inds.size();
    //    int start4 = 0;

    for (size_t k = 0; k < word_inds.size(); ++k) {
      //cerr << "single t: " << word_inds[k] << endl;
      //delta_x_vals[i].rows(start, start + m_de - 1).print(cerr);
      m_emb.col(word_inds[k]) -=
          m_lr*delta_x_vals[i].rows(start, start + m_de - 1);
      m_suf.col(suf_inds[k]) -=
          m_lr*delta_x_vals[i].rows(start2, start2 + m_ds - 1);
      //      m_cap.col(cap_inds[k]) -=
      //          m_lr*delta_x_vals[i].rows(start3, start3 + m_dc - 1);


      start += m_de;
      start2 += m_ds;
    }

    if (m_use_supercat_feats) {

      assert(context_batch[i].size() >= 3);
      assert(start2 == start3);

      const vector<size_t> &supercat_inds = context_batch[i][2];
      //start4 = start3 + m_dsuper*supercat_inds.size();

      for (size_t k = 0; k < supercat_inds.size(); ++k) {
        m_supercat_emb.col(supercat_inds[k]) -=
            m_lr*delta_x_vals[i].rows(start3, start3 + m_dsuper - 1);
        start3 += m_dsuper;
      }
    }
  }
}


void
RNNParser::_Impl::train_xf1_bptt(const size_t steps, double &product, const vector<vector<vector<size_t>>> &context_batch,
                                 const vector<mat> &x_vals, const vector<mat> &h_vals,
                                 const vector<mat> &h_deriv_vals, const vector<mat> &y_val_batch,
                                 const vector<size_t> &y_ind_batch, const vector<mat> &y_all_ind_batch,
                                 mat &grad_wx, mat &grad_wh, mat &grad_wy) {
  mat delta_y_vals[steps];
  mat delta_h_vals[steps];
  mat delta_x_vals[steps];

  for (size_t i = 0; i < steps; ++i) {
    delta_y_vals[i] = zeros<mat>(m_wy.n_cols, 1);
    for (size_t j = 0; j < y_all_ind_batch[i].n_elem; ++j) {
      delta_y_vals[i](y_all_ind_batch[i](j)) = y_val_batch[i](j);
    }

    double action_score = delta_y_vals[i](y_ind_batch[i]);
    //    delta_y_vals[i] *= -action_score;
    //    delta_y_vals[i](y_ind_batch[i]) = action_score * (1.0 - action_score);
    //    delta_y_vals[i] *= (product * (1.0/action_score));
    delta_y_vals[i] *= -1.0;
    delta_y_vals[i](y_ind_batch[i]) = (1.0 - action_score);
    delta_y_vals[i] *= product;
  }

  // bptt multi
  delta_h_vals[steps - 1] = h_deriv_vals[steps - 1].t() % (m_wy*delta_y_vals[steps - 1]);

  for (size_t i = steps; i > 1; --i) {
    delta_h_vals[i - 2] = h_deriv_vals[i - 2].t() %
        ((m_wh*delta_h_vals[i - 1]) + m_wy*(delta_y_vals[i - 2]));
  }

  //double back_grad_wh = 0.0;
  for (size_t i = 0; i < steps; ++i) {
    mat grad = h_vals[i].t()*delta_h_vals[i].t();
    //cerr << "grad_test: " << grad(12,12) << endl;
    grad_wh += grad;
    //back_grad_wh += grad(0,0);
  }

  //double back_grad_wy = 0.0;
  for (size_t i = 1; i <= steps; ++i) {
    mat grad = h_vals[i].t()*delta_y_vals[i - 1].t();
    grad_wy += grad;
    //back_grad_wy += grad(0,0);
  }

  for (size_t i = 0; i < steps; ++i) {
    if (cfg.use_dropout()) {
      delta_x_vals[i] = m_dropout_vec % (m_wx*delta_h_vals[i]);
    } else {
      delta_x_vals[i] = m_wx*delta_h_vals[i];
    }
  }

  //double back_grad_wx = 0.0;
  for (size_t i = 0; i < steps; ++i) {
    mat grad = x_vals[i]*delta_h_vals[i].t();
    grad_wx += grad;
    //back_grad_wx += grad(0,0);
  }

  // grad check
  //double bp_grad_emb = 0.0;
  //double bp_grad_suf = 0.0;
  //  double bp_grad_cap = 0.0;
  //double bp_grad_super = 0.0;

  for (size_t i = 0; i < steps; ++i) {

    const vector<size_t> &word_inds = context_batch[i][0];
    const vector<size_t> &suf_inds = context_batch[i][1];

    assert(word_inds.size() == suf_inds.size());

    // all start inds must be set here
    int start = 0;
    int start2 = m_de*word_inds.size(); // dim of word emb x num of embs
    int start3 = start2 + m_ds*word_inds.size();
    //    int start4 = 0;

    for (size_t k = 0; k < word_inds.size(); ++k) {
      //cerr << "single t: " << word_inds[k] << endl;
      //delta_x_vals[i].rows(start, start + m_de - 1).print(cerr);
      m_emb.col(word_inds[k]) -=
          m_lr*delta_x_vals[i].rows(start, start + m_de - 1);
      //if (word_inds[k] == 40235)
      //bp_grad_emb += delta_x_vals[i].rows(start, start + m_de - 1)[0];
      m_suf.col(suf_inds[k]) -=
          m_lr*delta_x_vals[i].rows(start2, start2 + m_ds - 1);
      //if (suf_inds[k] == 1)
      //bp_grad_suf += delta_x_vals[i].rows(start2, start2 + m_ds - 1)[0];
      //      m_cap.col(cap_inds[k]) -=
      //          m_lr*delta_x_vals[i].rows(start3, start3 + m_dc - 1);
      //      if (cap_inds[k]  == 0)
      //        bp_grad_cap += delta_x_vals[i].rows(start3, start3 + m_dc - 1)[0];

      start += m_de;
      start2 += m_ds;
    }

    if (m_use_supercat_feats) {
      assert(context_batch[i].size() >= 3);
      assert(start2 == start3);

      const vector<size_t> &supercat_inds = context_batch[i][2];
      //start4 = start3 + m_dsuper*supercat_inds.size();

      for (size_t k = 0; k < supercat_inds.size(); ++k) {
        m_supercat_emb.col(supercat_inds[k]) -=
            m_lr*delta_x_vals[i].rows(start3, start3 + m_dsuper - 1);
        // if (supercat_inds[k] == 3)
        //bp_grad_super += delta_x_vals[i].rows(start3, start3 + m_dsuper - 1)[0];

        start3 += m_dsuper;
      }
    }
  }
}



void
RNNParser::_Impl::train_xf1_grad_check(double &product, const ShiftReduceHypothesis *&hypo,
                                       mat &grad_wx, mat &grad_wh, mat &grad_wy, double &bp_grad_emb00) {
  size_t total_steps = hypo->GetTotalActionCount();
  //cerr << "total_steps: " << total_steps << endl;   //debug
  mat h_vals[total_steps + 1];
  //m_h_tm1.zeros(); moved up
  h_vals[0] = m_h_tm1;
  mat h_deriv_vals[total_steps];
  mat y_vals[total_steps];
  mat y_all_ind_vals[total_steps]; // all feasible ids at each step for all steps
  size_t y_ind_vals[total_steps]; // the feasible ids for all steps, one id per step
  mat x_vals[total_steps]; // concatenated embeddings
  vector<vector<size_t>> context_vecs[total_steps]; // inds for the embeddings

  const ShiftReduceHypothesis *current_hypo = hypo;
  assert(current_hypo->GetPrevHypo());
  for (int i = total_steps; i > 0; --i) {
    h_vals[i] = current_hypo->GetPrevHypo()->m_hidden_states;
    mat one = ones<mat>(m_h_tm1.n_rows, m_h_tm1.n_cols);
    h_deriv_vals[i - 1] = h_vals[i] % (one - h_vals[i]);

    y_ind_vals[i - 1] = current_hypo->m_action_id;
    y_all_ind_vals[i - 1] = current_hypo->GetPrevHypo()->m_feasible_action_ids;
    y_vals[i - 1] = current_hypo->GetPrevHypo()->m_action_score_vec; // for feasible actions only
    assert(y_all_ind_vals[i - 1].n_elem == y_vals[i - 1].n_elem);

    x_vals[i - 1] = current_hypo->GetPrevHypo()->m_converted_context_vec;
    context_vecs[i - 1] = current_hypo->GetPrevHypo()->m_context_vec;
    current_hypo = current_hypo->GetPrevHypo();
  }
  assert(current_hypo->IsStartHypo());

  train_xf1_bptt_grad_check(total_steps, product, context_vecs,
                            x_vals, h_vals, h_deriv_vals,
                            y_vals, y_ind_vals, y_all_ind_vals,
                            grad_wx, grad_wh, grad_wy, bp_grad_emb00);
}

void
RNNParser::_Impl::train_xf1_bptt_grad_check(const size_t steps, double &product, const vector<vector<size_t>> *context_batch,
                                            const mat *x_vals, const mat *h_vals,
                                            const mat *h_deriv_vals, const mat *y_val_batch,
                                            const size_t *y_ind_batch, const mat *y_all_ind_batch,
                                            mat &grad_wx, mat &grad_wh, mat &grad_wy, double &bp_grad_emb) {
  mat delta_y_vals[steps];
  mat delta_h_vals[steps];
  mat delta_x_vals[steps];

  for (size_t i = 0; i < steps; ++i) {
    delta_y_vals[i] = zeros<mat>(m_wy.n_cols, 1);
    for (size_t j = 0; j < y_all_ind_batch[i].n_elem; ++j) {
      delta_y_vals[i](y_all_ind_batch[i](j)) = y_val_batch[i](j);
    }

    double action_score = delta_y_vals[i](y_ind_batch[i]);
    //    delta_y_vals[i] *= -action_score;
    //    delta_y_vals[i](y_ind_batch[i]) = action_score * (1.0 - action_score);
    //    delta_y_vals[i] *= (product * (1.0/action_score));
    delta_y_vals[i] *= -1.0;
    delta_y_vals[i](y_ind_batch[i]) = (1.0 - action_score);
    delta_y_vals[i] *= product;
  }

  // bptt multi
  delta_h_vals[steps - 1] = h_deriv_vals[steps - 1].t() % (m_wy*delta_y_vals[steps - 1]);

  for (size_t i = steps; i > 1; --i) {
    delta_h_vals[i - 2] = h_deriv_vals[i - 2].t() %
        ((m_wh*delta_h_vals[i - 1]) + m_wy*(delta_y_vals[i - 2]));
  }

  //double back_grad_wh = 0.0;
  for (size_t i = 0; i < steps; ++i) {
    mat grad = h_vals[i].t()*delta_h_vals[i].t();
    //cerr << "grad_test: " << grad(12,12) << endl;
    grad_wh += grad;
    //back_grad_wh += grad(0,0);
  }

  //double back_grad_wy = 0.0;
  for (size_t i = 1; i <= steps; ++i) {
    mat grad = h_vals[i].t()*delta_y_vals[i - 1].t();
    grad_wy += grad;
    //back_grad_wy += grad(0,0);
  }

  for (size_t i = 0; i < steps; ++i) {
    delta_x_vals[i] = m_wx*delta_h_vals[i];
  }

  //double back_grad_wx = 0.0;
  for (size_t i = 0; i < steps; ++i) {
    mat grad = x_vals[i]*delta_h_vals[i].t();
    grad_wx += grad;
    //back_grad_wx += grad(0,0);
  }

  // grad check
  //double bp_grad_emb = 0.0;
  //double bp_grad_suf = 0.0;
  //  double bp_grad_cap = 0.0;
  //double bp_grad_super = 0.0;

  for (size_t i = 0; i < steps; ++i) {

    const vector<size_t> &word_inds = context_batch[i][0];
    const vector<size_t> &suf_inds = context_batch[i][1];

    assert(word_inds.size() == suf_inds.size());

    // all start inds must be set here
    int start = 0;
    int start2 = m_de*word_inds.size(); // dim of word emb x num of embs
    int start3 = start2 + m_ds*word_inds.size();
    //    int start4 = 0;

    for (size_t k = 0; k < word_inds.size(); ++k) {
      //cerr << "single t: " << word_inds[k] << endl;
      //delta_x_vals[i].rows(start, start + m_de - 1).print(cerr);
      m_emb.col(word_inds[k]) -=
          m_lr*delta_x_vals[i].rows(start, start + m_de - 1);
      if (word_inds[k] == 0)
        bp_grad_emb += delta_x_vals[i].rows(start, start + m_de - 1)(0);
      m_suf.col(suf_inds[k]) -=
          m_lr*delta_x_vals[i].rows(start2, start2 + m_ds - 1);
      //if (suf_inds[k] == 1)
      //bp_grad_suf += delta_x_vals[i].rows(start2, start2 + m_ds - 1)[0];
      //      m_cap.col(cap_inds[k]) -=
      //          m_lr*delta_x_vals[i].rows(start3, start3 + m_dc - 1);
      //      if (cap_inds[k]  == 0)
      //        bp_grad_cap += delta_x_vals[i].rows(start3, start3 + m_dc - 1)[0];

      start += m_de;
      start2 += m_ds;
    }

    if (m_use_supercat_feats) {

      assert(context_batch[i].size() >= 3);
      assert(start2 == start3);

      const vector<size_t> &supercat_inds = context_batch[i][2];
      //start4 = start3 + m_dsuper*supercat_inds.size();

      for (size_t k = 0; k < supercat_inds.size(); ++k) {
        m_supercat_emb.col(supercat_inds[k]) -=
            m_lr*delta_x_vals[i].rows(start3, start3 + m_dsuper - 1);
        // if (supercat_inds[k] == 3)
        //bp_grad_super += delta_x_vals[i].rows(start3, start3 + m_dsuper - 1)[0];

        start3 += m_dsuper;
      }
    }
  }
}

//// bptt over one action sequence for xf1 training
//void
//RNNParser::_Impl::train_xf1(double &product, const ShiftReduceHypothesis *&hypo,
//                            mat &grad_wx, mat &grad_wh, mat &grad_wy) {
//  cerr << "train_xf1\n";
//  size_t total_steps = hypo->GetTotalActionCount();
//  mat h_vals[total_steps + 1];
//  //m_h_tm1.zeros(); moved up
//  h_vals[0] = m_h_tm1;
//  mat h_deriv_vals[total_steps];
//  mat y_vals[total_steps];
//  mat y_all_ind_vals[total_steps]; // all feasible ids at each step for all steps
//  size_t y_ind_vals[total_steps]; // the feasible ids for all steps, one id per step
//  mat x_vals[total_steps]; // concatenated embeddings
//  vector<vector<size_t>> context_vecs[total_steps]; // inds for the embeddings
//
//  const ShiftReduceHypothesis *current_hypo = hypo;
//  assert(current_hypo->GetPrevHypo());
//  for (size_t i = total_steps; i > 0; --i) {
//    h_vals[i] = current_hypo->GetPrevHypo()->m_hidden_states;
//    mat one = ones<mat>(m_h_tm1.n_rows, m_h_tm1.n_cols);
//    h_deriv_vals[i - 1] = h_vals[i] % (one - h_vals[i]);
//
//    y_ind_vals[i - 1] = current_hypo->m_action_id;
//    y_all_ind_vals[i - 1] = current_hypo->GetPrevHypo()->m_feasible_action_ids;
//    y_vals[i - 1] = current_hypo->GetPrevHypo()->m_action_score_vec; // for feasible actions only
//    assert(y_all_ind_vals[i - 1].n_elem == y_vals[i - 1].n_elem);
//
//    x_vals[i - 1] = current_hypo->GetPrevHypo()->m_converted_context_vec;
//    context_vecs[i - 1] = current_hypo->GetPrevHypo()->m_context_vec;
//    current_hypo = current_hypo->GetPrevHypo();
//  }
//  assert(current_hypo->IsStartHypo());
//
//  //  vector<vector<mat>> all_x_val_batches;
//  //  vector<vector<mat>> all_h_val_batches;
//  //  vector<vector<mat>> all_h_deriv_val_batches;
//  //  vector<vector<mat>> all_y_val_batches;
//  //  vector<vector<size_t>> all_y_ind_val_batches;
//  //  vector<vector<mat>> all_y_all_ind_val_batches;
//
//  vector<mat> x_val_batch;
//  vector<vector<vector<size_t>>> context_batch;
//  vector<mat> h_val_batch;
//  vector<mat> h_deriv_val_batch;
//  vector<mat> y_val_batch;
//  vector<size_t> y_ind_batch;
//  vector<mat> y_all_ind_batch;
//
//  for (size_t i = 0; i < total_steps; ++i) {
//    x_val_batch.push_back(x_vals[i]);
//    context_batch.push_back(context_vecs[i]);
//
//    h_val_batch.push_back(h_vals[i]);
//    h_deriv_val_batch.push_back(h_deriv_vals[i]);
//
//    y_ind_batch.push_back(y_ind_vals[i]);
//    y_val_batch.push_back(y_vals[i]);
//    y_all_ind_batch.push_back(y_all_ind_vals[i]);
//    assert(y_all_ind_vals[i].n_elem == y_vals[i].n_elem);
//
//    //if ((i + 1) % m_bs == 0 || i == total_steps - 1) {
//    if (i == total_steps - 1) {
//      assert(total_steps = x_val_batch.size());
//      h_val_batch.push_back(h_vals[i+1]);
//      train_xf1_bptt(product, context_batch,
//                     x_val_batch, h_val_batch, h_deriv_val_batch,
//                     y_val_batch, y_ind_batch, y_all_ind_batch,
//                     grad_wx, grad_wh, grad_wy);
//    }
//  }
//}
//
//
//void
//RNNParser::_Impl::train_xf1_bptt(double &product, const vector<vector<vector<size_t>>> &context_batch,
//                                 const vector<mat> &x_vals, const vector<mat> &h_vals, const vector<mat> &h_deriv_vals,
//                                 const vector<mat> &y_val_batch, const vector<size_t> &y_ind_batch, const vector<mat> &y_all_ind_batch,
//                                 mat &grad_wx, mat &grad_wh, mat &grad_wy) {
//  size_t steps = x_vals.size();
//  assert(steps = context_batch.size());
//  mat delta_y_vals[steps];
//  mat delta_h_vals[steps];
//  mat delta_x_vals[steps];
//
//  for (size_t i = 0; i < steps; ++i) {
//    delta_y_vals[i] = zeros<mat>(m_wy.n_cols, 1);
//    for (size_t j = 0; j < y_all_ind_batch[i].n_elem; ++j) {
//      delta_y_vals[i](y_all_ind_batch[i](j)) = y_val_batch[i](j);
//    }
//
//    double action_score = delta_y_vals[i](y_ind_batch[i]);
//    //    delta_y_vals[i] *= -action_score;
//    //    delta_y_vals[i](y_ind_batch[i]) = action_score * (1.0 - action_score);
//    //    delta_y_vals[i] *= (product * (1.0/action_score));
//    delta_y_vals[i] *= -1.0;
//    delta_y_vals[i](y_ind_batch[i]) = (1.0 - action_score);
//    delta_y_vals[i] *= product;
//  }
//
//  // bptt multi
//  delta_h_vals[steps - 1] = h_deriv_vals[steps - 1].t() % (m_wy*delta_y_vals[steps - 1]);
//
//  for (size_t i = steps; i > 1; --i) {
//    delta_h_vals[i - 2] = h_deriv_vals[i - 2].t() %
//        ((m_wh*delta_h_vals[i - 1]) + m_wy*(delta_y_vals[i - 2]));
//  }
//
//  //double back_grad_wh = 0.0;
//  for (size_t i = 0; i < steps; ++i) {
//    mat grad = h_vals[i].t()*delta_h_vals[i].t();
//    //cerr << "grad_test: " << grad(12,12) << endl;
//    grad_wh += grad;
//    //back_grad_wh += grad(0,0);
//  }
//
//  //double back_grad_wy = 0.0;
//  for (size_t i = 1; i <= steps; ++i) {
//    mat grad = h_vals[i].t()*delta_y_vals[i - 1].t();
//    grad_wy += grad;
//    //back_grad_wy += grad(0,0);
//  }
//
//  for (size_t i = 0; i < steps; ++i) {
//    delta_x_vals[i] = m_wx*delta_h_vals[i];
//  }
//
//  //double back_grad_wx = 0.0;
//  for (size_t i = 0; i < steps; ++i) {
//    mat grad = x_vals[i]*delta_h_vals[i].t();
//    grad_wx += grad;
//    //back_grad_wx += grad(0,0);
//  }
//
//  // grad check
//  //double bp_grad_emb = 0.0;
//  //double bp_grad_suf = 0.0;
//  //  double bp_grad_cap = 0.0;
//  //double bp_grad_super = 0.0;
//
//  for (size_t i = 0; i < steps; ++i) {
//
//    const vector<size_t> &word_inds = context_batch[i][0];
//    const vector<size_t> &suf_inds = context_batch[i][1];
//
//    assert(word_inds.size() == suf_inds.size());
//
//    // all start inds must be set here
//    int start = 0;
//    int start2 = m_de*word_inds.size(); // dim of word emb x num of embs
//    int start3 = start2 + m_ds*word_inds.size();
//    //    int start4 = 0;
//
//    for (size_t k = 0; k < word_inds.size(); ++k) {
//      m_emb.col(word_inds[k]) -=
//          m_lr*delta_x_vals[i].rows(start, start + m_de - 1);
//      //if (word_inds[k] == 40235)
//      //bp_grad_emb += delta_x_vals[i].rows(start, start + m_de - 1)[0];
//      m_suf.col(suf_inds[k]) -=
//          m_lr*delta_x_vals[i].rows(start2, start2 + m_ds - 1);
//      //if (suf_inds[k] == 1)
//      //bp_grad_suf += delta_x_vals[i].rows(start2, start2 + m_ds - 1)[0];
//      //      m_cap.col(cap_inds[k]) -=
//      //          m_lr*delta_x_vals[i].rows(start3, start3 + m_dc - 1);
//      //      if (cap_inds[k]  == 0)
//      //        bp_grad_cap += delta_x_vals[i].rows(start3, start3 + m_dc - 1)[0];
//
//      start += m_de;
//      start2 += m_ds;
//    }
//
//    if (m_use_supercat_feats) {
//
//      assert(context_batch[i].size() >= 3);
//      assert(start2 == start3);
//
//      const vector<size_t> &supercat_inds = context_batch[i][2];
//      //start4 = start3 + m_dsuper*supercat_inds.size();
//
//      for (size_t k = 0; k < supercat_inds.size(); ++k) {
//        m_supercat_emb.col(supercat_inds[k]) -=
//            m_lr*delta_x_vals[i].rows(start3, start3 + m_dsuper - 1);
//        // if (supercat_inds[k] == 3)
//        //bp_grad_super += delta_x_vals[i].rows(start3, start3 + m_dsuper - 1)[0];
//
//        start3 += m_dsuper;
//      }
//    }
//  }
//}
//void
//RNNParser::_Impl::xf1_mt(const size_t num_threads,
//                         const size_t ind,
//                         const size_t max_action_count,
//                         const mat &pxf1f1,
//                         vector<const ShiftReduceHypothesis*> &kbest_output_vec) {
//
//
//}



// bptt over one action sequence for xf1 training
void
RNNParser::_Impl::train_xf1_mt(const double &product, const ShiftReduceHypothesis *&hypo,
                               mat &grad_wx, mat &grad_wh, mat &grad_wy,
                               vector<mat> &delta_x_all_steps) {
  size_t total_steps = hypo->GetTotalActionCount();
  mat h_vals[total_steps + 1];
  //m_h_tm1.zeros(); moved up
  h_vals[0] = m_h_tm1;
  mat h_deriv_vals[total_steps];
  mat y_vals[total_steps];
  mat y_all_ind_vals[total_steps]; // all feasible ids at each step for all steps
  size_t y_ind_vals[total_steps]; // the feasible ids for all steps, one id per step
  mat x_vals[total_steps]; // concatenated embeddings
  vector<vector<size_t>> context_vecs[total_steps]; // inds for the embeddings

  const ShiftReduceHypothesis *current_hypo = hypo;
  assert(current_hypo->GetPrevHypo());
  for (int i = total_steps; i > 0; --i) {
    h_vals[i] = current_hypo->GetPrevHypo()->m_hidden_states;
    mat one = ones<mat>(m_h_tm1.n_rows, m_h_tm1.n_cols);
    h_deriv_vals[i - 1] = h_vals[i] % (one - h_vals[i]);

    y_ind_vals[i - 1] = current_hypo->m_action_id;
    y_all_ind_vals[i - 1] = current_hypo->GetPrevHypo()->m_feasible_action_ids;
    y_vals[i - 1] = current_hypo->GetPrevHypo()->m_action_score_vec; // for feasible actions only
    assert(y_all_ind_vals[i - 1].n_elem == y_vals[i - 1].n_elem);

    x_vals[i - 1] = current_hypo->GetPrevHypo()->m_converted_context_vec;
    //context_vecs[i - 1] = current_hypo->GetPrevHypo()->m_context_vec;
    current_hypo = current_hypo->GetPrevHypo();
  }
  assert(current_hypo->IsStartHypo());

  if (!cfg.xf1_mini_batch()) {
    train_xf1_bptt_mt(total_steps, product, context_vecs,
                      x_vals, h_vals, h_deriv_vals,
                      y_vals, y_ind_vals, y_all_ind_vals,
                      grad_wx, grad_wh, grad_wy, delta_x_all_steps, 0);
  } else {
    vector<mat> x_val_batch;
    //vector<vector<vector<size_t>>> context_batch;
    vector<mat> h_val_batch;
    vector<mat> h_deriv_val_batch;
    vector<mat> y_val_batch;
    vector<size_t> y_ind_batch;
    vector<mat> y_all_ind_batch;

    x_val_batch.reserve(m_bs);
    //context_batch.reserve(m_bs);
    h_val_batch.reserve(m_bs);
    h_deriv_val_batch.reserve(m_bs);
    y_val_batch.reserve(m_bs);
    y_ind_batch.reserve(m_bs);
    y_all_ind_batch.reserve(m_bs);

    size_t batch_id = 0;
    for (size_t i = 0; i < total_steps; ++i) {
      x_val_batch.push_back(x_vals[i]);
      //context_batch.push_back(context_vecs[i]);

      h_val_batch.push_back(h_vals[i]);
      h_deriv_val_batch.push_back(h_deriv_vals[i]);

      y_ind_batch.push_back(y_ind_vals[i]);
      y_val_batch.push_back(y_vals[i]);
      y_all_ind_batch.push_back(y_all_ind_vals[i]);
      assert(y_all_ind_vals[i].n_elem == y_vals[i].n_elem);

      //bool condition = cfg.xf1_mini_batch() ? ((i + 1) % m_bs == 0 || i == total_steps - 1) : i == total_steps - 1;
      if ((i + 1) % m_bs == 0 || i == total_steps - 1) {
        assert(total_steps = x_val_batch.size());
        h_val_batch.push_back(h_vals[i+1]);
        train_xf1_bptt_mt(x_val_batch.size(), product,
                          x_val_batch, h_val_batch, h_deriv_val_batch,
                          y_val_batch, y_ind_batch, y_all_ind_batch,
                          grad_wx, grad_wh, grad_wy, delta_x_all_steps, batch_id);
        x_val_batch.clear();
        //context_batch.clear();
        h_val_batch.clear();
        h_deriv_val_batch.clear();
        y_val_batch.clear();
        y_ind_batch.clear();
        y_all_ind_batch.clear();
        ++batch_id;
      }
    }
  }
}


void
RNNParser::_Impl::train_xf1_bptt_mt(const size_t &steps, const double &product, const vector<vector<size_t>> *context_batch,
                                    const mat *x_vals, const mat *h_vals,
                                    const mat *h_deriv_vals, const mat *y_val_batch,
                                    const size_t *y_ind_batch, const mat *y_all_ind_batch,
                                    mat &grad_wx, mat &grad_wh, mat &grad_wy,
                                    vector<mat> &delta_x_all_steps, const size_t &batch_id) {
  //size_t steps = x_vals.size();
  //assert(steps = context_batch.size());
  mat delta_y_vals[steps];
  mat delta_h_vals[steps];
  //mat delta_x_vals[steps];

  for (size_t i = 0; i < steps; ++i) {
    delta_y_vals[i] = zeros<mat>(m_wy.n_cols, 1);
    for (size_t j = 0; j < y_all_ind_batch[i].n_elem; ++j) {
      delta_y_vals[i](y_all_ind_batch[i](j)) = y_val_batch[i](j);
    }

    double action_score = delta_y_vals[i](y_ind_batch[i]);
    //    delta_y_vals[i] *= -action_score;
    //    delta_y_vals[i](y_ind_batch[i]) = action_score * (1.0 - action_score);
    //    delta_y_vals[i] *= (product * (1.0/action_score));
    delta_y_vals[i] *= -1.0;
    delta_y_vals[i](y_ind_batch[i]) = (1.0 - action_score);
    delta_y_vals[i] *= product;
  }

  // bptt multi
  delta_h_vals[steps - 1] = h_deriv_vals[steps - 1].t() % (m_wy*delta_y_vals[steps - 1]);

  for (size_t i = steps; i > 1; --i) {
    delta_h_vals[i - 2] = h_deriv_vals[i - 2].t() %
        ((m_wh*delta_h_vals[i - 1]) + m_wy*(delta_y_vals[i - 2]));
  }

  //double back_grad_wh = 0.0;
  for (size_t i = 0; i < steps; ++i) {
    mat grad = h_vals[i].t()*delta_h_vals[i].t();
    //cerr << "grad_test: " << grad(12,12) << endl;
    grad_wh += grad;
    //back_grad_wh += grad(0,0);
  }

  //double back_grad_wy = 0.0;
  for (size_t i = 1; i <= steps; ++i) {
    mat grad = h_vals[i].t()*delta_y_vals[i - 1].t();
    grad_wy += grad;
    //back_grad_wy += grad(0,0);
  }

  for (size_t i = 0; i < steps; ++i) {
    //delta_x_vals[i] = m_wx*delta_h_vals[i];
    if (cfg.xf1_mini_batch()) {
      if (cfg.use_dropout())
        delta_x_all_steps[m_bs*batch_id + i] = m_dropout_vec % (m_wx*delta_h_vals[i]);
      else
        delta_x_all_steps[m_bs*batch_id + i] = m_wx*delta_h_vals[i];
    } else {
      if (cfg.use_dropout())
        delta_x_all_steps[i] = m_dropout_vec % (m_wx*delta_h_vals[i]);
      else
        delta_x_all_steps[i] = m_wx*delta_h_vals[i];
    }
  }

  for (size_t i = 0; i < steps; ++i) {
    mat grad = x_vals[i]*delta_h_vals[i].t();
    grad_wx += grad;
  }

  //  for (size_t i = 0; i < steps; ++i) {
  //
  //    const vector<size_t> &word_inds = context_batch[i][0];
  //    const vector<size_t> &suf_inds = context_batch[i][1];
  //
  //    assert(word_inds.size() == suf_inds.size());

  // all start inds must be set here
  //    int start = 0;
  //    int start2 = m_de*word_inds.size(); // dim of word emb x num of embs
  //    int start3 = start2 + m_ds*word_inds.size();
  //    int start4 = 0;

  //    for (size_t k = 0; k < word_inds.size(); ++k) {
  //      m_emb.col(word_inds[k]) -=
  //          m_lr*delta_x_vals[i].rows(start, start + m_de - 1);
  //      //if (word_inds[k] == 40235)
  //      //bp_grad_emb += delta_x_vals[i].rows(start, start + m_de - 1)[0];
  //      m_suf.col(suf_inds[k]) -=
  //          m_lr*delta_x_vals[i].rows(start2, start2 + m_ds - 1);
  //      //if (suf_inds[k] == 1)
  //      //bp_grad_suf += delta_x_vals[i].rows(start2, start2 + m_ds - 1)[0];
  //      //      m_cap.col(cap_inds[k]) -=
  //      //          m_lr*delta_x_vals[i].rows(start3, start3 + m_dc - 1);
  //      //      if (cap_inds[k]  == 0)
  //      //        bp_grad_cap += delta_x_vals[i].rows(start3, start3 + m_dc - 1)[0];
  //
  //      start += m_de;
  //      start2 += m_ds;
  //    }
  //
  //    if (m_use_supercat_feats) {
  //
  //      assert(context_batch[i].size() >= 3);
  //      assert(start2 == start3);
  //
  //      const vector<size_t> &supercat_inds = context_batch[i][2];
  //      //start4 = start3 + m_dsuper*supercat_inds.size();
  //
  //      for (size_t k = 0; k < supercat_inds.size(); ++k) {
  //        m_supercat_emb.col(supercat_inds[k]) -=
  //            m_lr*delta_x_vals[i].rows(start3, start3 + m_dsuper - 1);
  //        // if (supercat_inds[k] == 3)
  //        //bp_grad_super += delta_x_vals[i].rows(start3, start3 + m_dsuper - 1)[0];
  //
  //        start3 += m_dsuper;
  //      }
  //    }
  //  }
}


void
RNNParser::_Impl::train_xf1_bptt_mt(const size_t &steps, const double &product,
                                    const vector<mat> &x_vals, const vector<mat> &h_vals,
                                    const vector<mat> &h_deriv_vals, const vector<mat> &y_val_batch,
                                    const vector<size_t> &y_ind_batch, const vector<mat> &y_all_ind_batch,
                                    mat &grad_wx, mat &grad_wh, mat &grad_wy,
                                    vector<mat> &delta_x_all_steps, const size_t &batch_id) {
  //size_t steps = x_vals.size();
  //assert(steps = context_batch.size());
  mat delta_y_vals[steps];
  mat delta_h_vals[steps];
  //mat delta_x_vals[steps];

  for (size_t i = 0; i < steps; ++i) {
    delta_y_vals[i] = zeros<mat>(m_wy.n_cols, 1);
    for (size_t j = 0; j < y_all_ind_batch[i].n_elem; ++j) {
      delta_y_vals[i](y_all_ind_batch[i](j)) = y_val_batch[i](j);
    }

    double action_score = delta_y_vals[i](y_ind_batch[i]);
    //    delta_y_vals[i] *= -action_score;
    //    delta_y_vals[i](y_ind_batch[i]) = action_score * (1.0 - action_score);
    //    delta_y_vals[i] *= (product * (1.0/action_score));
    delta_y_vals[i] *= -1.0;
    delta_y_vals[i](y_ind_batch[i]) = (1.0 - action_score);
    delta_y_vals[i] *= product;
  }

  // bptt multi
  delta_h_vals[steps - 1] = h_deriv_vals[steps - 1].t() % (m_wy*delta_y_vals[steps - 1]);

  for (size_t i = steps; i > 1; --i) {
    delta_h_vals[i - 2] = h_deriv_vals[i - 2].t() %
        ((m_wh*delta_h_vals[i - 1]) + m_wy*(delta_y_vals[i - 2]));
  }

  //double back_grad_wh = 0.0;
  for (size_t i = 0; i < steps; ++i) {
    mat grad = h_vals[i].t()*delta_h_vals[i].t();
    //cerr << "grad_test: " << grad(12,12) << endl;
    grad_wh += grad;
    //back_grad_wh += grad(0,0);
  }

  //double back_grad_wy = 0.0;
  for (size_t i = 1; i <= steps; ++i) {
    mat grad = h_vals[i].t()*delta_y_vals[i - 1].t();
    grad_wy += grad;
    //back_grad_wy += grad(0,0);
  }

  for (size_t i = 0; i < steps; ++i) {
    //delta_x_vals[i] = m_wx*delta_h_vals[i];
    if (cfg.xf1_mini_batch()) {
      if (cfg.use_dropout())
        delta_x_all_steps[m_bs*batch_id + i] = m_dropout_vec % (m_wx*delta_h_vals[i]);
      else
        delta_x_all_steps[m_bs*batch_id + i] = m_wx*delta_h_vals[i];
    } else {
      if (cfg.use_dropout())
        delta_x_all_steps[i] = m_dropout_vec % (m_wx*delta_h_vals[i]);
      else
        delta_x_all_steps[i] = m_wx*delta_h_vals[i];
    }
  }

  //cerr << "size: " << delta_x_all_steps.size() << endl;


  //double back_grad_wx = 0.0;
  for (size_t i = 0; i < steps; ++i) {
    mat grad = x_vals[i]*delta_h_vals[i].t();
    grad_wx += grad;
    //back_grad_wx += grad(0,0);
  }

  // grad check
  //double bp_grad_emb = 0.0;
  //double bp_grad_suf = 0.0;
  //  double bp_grad_cap = 0.0;
  //double bp_grad_super = 0.0;

  //  for (size_t i = 0; i < steps; ++i) {
  //
  //    const vector<size_t> &word_inds = context_batch[i][0];
  //    const vector<size_t> &suf_inds = context_batch[i][1];
  //
  //    assert(word_inds.size() == suf_inds.size());

  // all start inds must be set here
  //    int start = 0;
  //    int start2 = m_de*word_inds.size(); // dim of word emb x num of embs
  //    int start3 = start2 + m_ds*word_inds.size();
  //    int start4 = 0;

  //    for (size_t k = 0; k < word_inds.size(); ++k) {
  //      m_emb.col(word_inds[k]) -=
  //          m_lr*delta_x_vals[i].rows(start, start + m_de - 1);
  //      //if (word_inds[k] == 40235)
  //      //bp_grad_emb += delta_x_vals[i].rows(start, start + m_de - 1)[0];
  //      m_suf.col(suf_inds[k]) -=
  //          m_lr*delta_x_vals[i].rows(start2, start2 + m_ds - 1);
  //      //if (suf_inds[k] == 1)
  //      //bp_grad_suf += delta_x_vals[i].rows(start2, start2 + m_ds - 1)[0];
  //      //      m_cap.col(cap_inds[k]) -=
  //      //          m_lr*delta_x_vals[i].rows(start3, start3 + m_dc - 1);
  //      //      if (cap_inds[k]  == 0)
  //      //        bp_grad_cap += delta_x_vals[i].rows(start3, start3 + m_dc - 1)[0];
  //
  //      start += m_de;
  //      start2 += m_ds;
  //    }
  //
  //    if (m_use_supercat_feats) {
  //
  //      assert(context_batch[i].size() >= 3);
  //      assert(start2 == start3);
  //
  //      const vector<size_t> &supercat_inds = context_batch[i][2];
  //      //start4 = start3 + m_dsuper*supercat_inds.size();
  //
  //      for (size_t k = 0; k < supercat_inds.size(); ++k) {
  //        m_supercat_emb.col(supercat_inds[k]) -=
  //            m_lr*delta_x_vals[i].rows(start3, start3 + m_dsuper - 1);
  //        // if (supercat_inds[k] == 3)
  //        //bp_grad_super += delta_x_vals[i].rows(start3, start3 + m_dsuper - 1)[0];
  //
  //        start3 += m_dsuper;
  //      }
  //    }
  //  }
}



RNNParser::RNNParser(void){};

RNNParser::RNNParser(const RNNConfig &cfg, Sentence &sent,
                     Categories &cats, const size_t srbeam, const string &config_dir, const bool train){
  _impl = new RNNParser::_Impl(cfg, sent, cats, srbeam, config_dir, train);
}

RNNParser::~RNNParser(void){ delete _impl; }


void
RNNParser::load_gold_trees(const std::string &filename) {
  try {
    return _impl->load_gold_trees(filename);
  } catch (...) {
    exit (EXIT_FAILURE);
  }
}

void
RNNParser::Clear() {
  _impl->Clear();
}

void
RNNParser::clear_xf1() {
  _impl->clear_xf1();
}


void
RNNParser::sr_parse_train(const double BETA, const size_t sent) {
  _impl->sr_parse_train(BETA, sent);
}


void
RNNParser::sr_parse_train_max_margin(const double BETA, const size_t sent) {
  _impl->sr_parse_max_margin(BETA, sent);
}

//const ShiftReduceHypothesis*
//RNNParser::sr_parse(const double BETA, const size_t sent) {
//  return _impl->sr_parse(BETA, sent);
//}


const ShiftReduceHypothesis*
RNNParser::sr_parse_beam_search(const size_t k, const size_t sent, const bool oracle, const bool train) {
  return _impl->sr_parse_beam_search(k, sent, oracle, train);
}


void
RNNParser::load_lex_cat_dict(const string &filename) {
  return _impl->load_lex_cat_dict(filename);
}


void
RNNParser::load_pos_str_ind_map(const string &filename) {
  try {
    return _impl->load_pos_str_ind_map(filename);
  } catch (runtime_error) {
    exit(EXIT_FAILURE);
  }
}

void
RNNParser::load_gold_trees_rnn_fmt(const string &filename) {
  try {
    return _impl->load_gold_trees_rnn_fmt(filename);
  } catch (runtime_error) {
    exit(EXIT_FAILURE);
  }
}

void
RNNParser::train(const size_t total_epochs, const string &model_path) {
  _impl->train(total_epochs, model_path);
}

void
RNNParser::re_init_mats(const string &wx, const string &wh, const string &wy,
                        const string &emb, const string &suf, const string &cap,
                        const string &super_emb, const string &comp_mat) {
  _impl->re_init_mats(wx, wh, wy, emb, suf, super_emb, comp_mat);
}

void
RNNParser::load_supercat_map(const string &file) {
  _impl->load_supercat_map(file);
}

void
RNNParser::dump_supercat_map(string &file) {
  _impl->dump_supercat_map(file);
}

void
RNNParser::save_weights(const size_t epoch, const string &model_path) {
  _impl->save_weights(epoch, model_path);
}

void RNNParser::get_deps(const CCG::SuperCat *sc, const ulong id,
                         double &gold, double &total, const int FMT, bool train) {
  _impl->get_deps(sc, id, gold, total, FMT, train);
}

double
RNNParser::get_total_sent_gold_deps(const size_t id, const bool train) {
  return _impl->get_total_sent_gold_deps(id, train);
}


void
RNNParser::init_xf1_training(bool train) {
  _impl->init_xf1_training(train);
}

void
RNNParser::xf1_grad_check(NLP::Taggers::RnnTagger &rnn_super, const size_t k, const size_t sent, double &total_parses, const size_t num_threads) {
  _impl->xf1_grad_check(rnn_super, k, sent, total_parses, num_threads);
}


void
RNNParser::xf1_grad_check_super(NLP::Taggers::RnnTagger &rnn_super, const size_t k, const size_t sent, double &total_parses) {
  _impl->xf1_grad_check_super(rnn_super, k, sent, total_parses);
}

const ShiftReduceHypothesis*
RNNParser::sr_parse_beam_search_train_xf1(NLP::Taggers::RnnTagger &rnn_super,
                                          const size_t k, const size_t sent,
                                          const bool oracle, const bool train,
                                          double &total_xf1, double &total_parses,
                                          const size_t &num_threads,
                                          const bool &bptt_graph,
                                          const bool &run_training) {
  return _impl->sr_parse_beam_search_train_xf1(rnn_super, k, sent, oracle, train, total_xf1, total_parses, num_threads,
                                               bptt_graph, run_training);
}

void RNNParser::precompute(const size_t &k) {
  _impl->precompute(k);
}

//const ShiftReduceHypothesis*
//RNNParser::sr_parse_beam_search_train_xf1_mt(const size_t k, const size_t sent,
//                                             const bool oracle, const bool train,
//                                             double &total_xf1, double &total_parses,
//                                             const size_t &num_threads,
//                                             const bool run_training) {
//  return _impl->sr_parse_beam_search_train_xf1_mt(k, sent, oracle, train, total_xf1, total_parses, num_threads, run_training);
//}


//void
//RNNParser::load_emb_mat(const string &file) {
//  try {
//  _impl->load_emb_mat(_impl->m_emb, file);
//  } catch (runtime_error &err) {
//    cerr << err.what() << ", exit now." << endl;
//    exit(EXIT_FAILURE);
//  }
//}
//
//void
//RNNParser::load_emb_mat_full(const string &file) {
//  try {
//  _impl->load_emb_mat(_impl->m_emb_all, file, false);
//  } catch (runtime_error &err) {
//    cerr << err.what() << ", exit now." << endl;
//    exit(EXIT_FAILURE);
//  }
//}


} }
