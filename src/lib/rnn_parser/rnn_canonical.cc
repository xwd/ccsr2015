// C&C NLP tools
// Copyright (c) Universities of Edinburgh, Oxford and Sydney
// Copyright (c) James R. Curran
//
// This software is covered by a non-commercial use licence.
// See LICENCE.txt for the full text of the licence.
//
// If LICENCE.txt is not included in this distribution
// please email candc@it.usyd.edu.au to obtain a copy.

#include "base.h"

#include "hashtable/base.h"
#include "hashtable/frame.h"

#include "parser/fixed.h"
#include "parser/atom.h"
#include "parser/feature.h"
#include "parser/varid.h"
#include "parser/category.h"
#include "parser/variable.h"
#include "rnn_parser/rnn_canonical.h"

#include "share.h"

using namespace std;

namespace NLP { namespace CCG {

using namespace HashTable;

class _RNNCanonEntry {
protected:
  const Cat *_cat;
  _RNNCanonEntry *_next;
  size_t _ind;
public:
  _RNNCanonEntry(const Cat *cat, _RNNCanonEntry *next, const size_t ind):
      _cat(cat), _next(next), _ind(ind) {}
  ~_RNNCanonEntry(void) { /* do nothing */ }

  void *operator new(size_t size, Pool *pool) { return (void *)pool->alloc(size); }
  void operator delete(void *, Pool *pool) { /* do nothing */ }

  const Cat *canonical(void) const { return _cat; }

  const size_t ind(void) { return _ind; }

  bool equal(const Cat *cat){
    // eq() is less stringent than ==
    // eq() doesn't look at variables
    return eqSR(cat, _cat);
  }

  _RNNCanonEntry *find(const Cat *cat){
    for(_RNNCanonEntry *l = this; l != 0; l = l->_next)
      if(l->equal(cat))
        return l;
    return 0;
  }

 _RNNCanonEntry *next() const {
   return _next;
 }

  ulong nchained(void){
    return _next ? _next->nchained() + 1 : 1;
  }
};

typedef Frame<_RNNCanonEntry,LARGE,MEDIUM> _ImplBase;
class RNNCanonical::_Impl: public _ImplBase, public Shared {
public:
  _Impl(const std::string &name): _ImplBase(name) {}
  virtual ~_Impl(void) {}

  const Cat *add(const Cat *cat, const size_t ind){
    ulong bucket = cat->rhash % _NBUCKETS;
    Entry *entry = _buckets[bucket]->find(cat);
    if(entry)
      return entry->canonical();

    entry = new (_ent_pool) Entry(cat, _buckets[bucket], ind);
    _buckets[bucket] = entry;
    ++size;

    return cat;
  }

//  const Cat *get(const Cat *cat) const{
//    ulong bucket = cat->rhash % _NBUCKETS;
//    Entry *entry = _buckets[bucket]->find(cat);
//    if(entry)
//      return entry->canonical();
//
//    return 0;
//  }

  const size_t get_ind(const Cat *cat) const{
    ulong bucket = cat->rhash % _NBUCKETS;
    Entry *entry = _buckets[bucket]->find(cat);
    if(entry) {
      assert(entry->ind() >= 2);
      return entry->ind();
    }

    return 1;
  }

  const size_t dump(std::ofstream &out) const {
    size_t total = 0;
    for(ulong i = 0; i < _NBUCKETS; ++i){
      if(_buckets[i]){
        for(Entry *entry = _buckets[i]; entry != 0; entry = entry->next()) {
          out << entry->canonical()->out_novar_noX_noNB_SR_str() << " "
              << entry->ind() << std::endl;
          ++total;
        }
      }
    }
    return total;
  }

};

RNNCanonical::RNNCanonical(void):
    _impl(new _Impl("rnn_canonical")) {}

RNNCanonical::RNNCanonical(const RNNCanonical &other):
    _impl(share(other._impl)) {}

RNNCanonical &
RNNCanonical::operator=(RNNCanonical &other){
  if(_impl != other._impl){
    release(_impl);
    _impl = share(other._impl);
  }

  return *this;
}

RNNCanonical::~RNNCanonical(void){
  release(_impl);
}

std::size_t
RNNCanonical::size(void) const {
  return _impl->size;
}

const Cat *
RNNCanonical::add(const Cat *cat, const size_t ind){
  return _impl->add(cat, ind);
}

const size_t
RNNCanonical::get_ind(const Cat *cat) const{
  return _impl->get_ind(cat);
}

const size_t
RNNCanonical::dump(std::ofstream &out) const {
  return _impl->dump(out);
}


} }
