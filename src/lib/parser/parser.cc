// C&C NLP tools
// Copyright (c) Universities of Edinburgh, Oxford and Sydney
// Copyright (c) James R. Curran
//
// This software is covered by a non-commercial use licence.
// See LICENCE.txt for the full text of the licence.
//
// If LICENCE.txt is not included in this distribution
// please email candc@it.usyd.edu.au to obtain a copy.

#include "parser/_parser.h"
#include "parser/ShiftReduceHypothesis.h"
#include "parser/ShiftReduceHypothesisQueue.h"
#include <stdlib.h>
#include "parser/integration.h"

using namespace std;

namespace NLP { namespace CCG {

using namespace CCG;
using namespace NLP::Tree;

//Pool *Parser::s_shiftReduceFeaturePool(new Pool(1 << 20));

Parser::Config::Config(const OpPath *base, const std::string &name, const std::string &desc)
: Directory(name, desc, base),
  cats(*this, "cats", "parser category directory", "//cats", &path),
  markedup(*this, "markedup", "parser markedup file", "//markedup", &cats),
  weights(*this, "weights", "parser model weights file", "//weights", &path),
  rules(*this, "rules", "parser rules file", "//rules", &path),
  maxwords(*this, SPACE, "maxwords", "maximum sentence length the parser will accept", 250),
  maxsupercats(*this, "maxsupercats", "maximum number of supercats before the parse explodes", 300000),
  alt_markedup(*this, SPACE, "alt_markedup", "use the alternative markedup categories (marked with !)", false),
  seen_rules(*this, "seen_rules", "only accept category combinations seen in the training data", true),
  extra_rules(*this, "extra_rules", "use additional punctuation and unary type-changing rules", true),
  question_rules(*this, "question_rules", "activate the unary rules that only apply to questions", false),
  eisner_nf(*this, "eisner_nf", "only accept composition when the Eisner (1996) constraints are met", true),
  partial_gold(*this, SPACE, "partial_gold", "used for generating feature forests for training", false),
  beam(*this, "beam_ratio", "(not fully tested)", 0.0),

  allowFrag(*this, "allowFrag", "allow fragmented output", true) {}

void
Parser::_Impl::_load_rules(const std::string &filename){
  cerr << "loading rules: " << filename << endl;
  ulong nlines = 0;
  if(filename == "")
    return;

  ifstream in(filename.c_str());
  if(!in)
    throw NLP::IOException("could not open rules file", filename);

  comment += read_preface(filename, in, nlines);

  string cat1str, cat2str;
  while(in >> cat1str >> cat2str){
    try {
      const Cat *cat1 = cats.canonize(cat1str.c_str());
      const Cat *cat2 = cats.canonize(cat2str.c_str());

      rule_instances.insert(cat1, cat2);
    }catch(NLP::Exception e){
      throw NLP::IOException("error parsing category in rule instantiation", filename);
    }
  }
  cerr << "total binary rules: " << rule_instances.size() << endl;
}


void Parser::_Impl::LoadModel(const std::string &filename)
{

  ifstream in(filename.c_str());
  if(!in)
    NLP::IOException("could not open features file", filename);

  size_t totalFeatureCount = 0;
  size_t type;
  while (in >> type)
  {
    ++totalFeatureCount;
    m_shiftReduceFeatures->SetWeights(in, type);
  }
  m_shiftReduceFeatures->CheckWeights(totalFeatureCount);
  std::cerr << "SR Load Model done: " << filename << endl;
}


void
Parser::_Impl::_load_features(const std::string &filename){

  //std::cerr << "_load_features in parser.cc" << std::endl;
  ulong nlines = 0;
  ifstream in(filename.c_str());
  if(!in)
    NLP::IOException("could not open features file", filename);

  comment += read_preface(filename, in, nlines);

  nfeatures = 1;
  string tmp;
  ulong freq;
  for(char c; in >> freq >> c; ++nfeatures){
    switch(c){
    case 'a': cat_feats.load(in, filename, nfeatures, LEX_WORD); break;
    case 'b': cat_feats.load(in, filename, nfeatures, LEX_POS); break;
    case 'c': cat_feats.load(in, filename, nfeatures, ROOT); break;
    case 'd': cat_feats.load(in, filename, nfeatures, ROOT_WORD); break;
    case 'e': cat_feats.load(in, filename, nfeatures, ROOT_POS); break;

    case 'f': dep_feats.load(in, filename, nfeatures, DEP_WORD); break;
    case 'g': dep_feats.load(in, filename, nfeatures, DEP_POS); break;
    case 'h': dep_feats.load(in, filename, nfeatures, DEP_WORD_POS); break;
    case 'i': dep_feats.load(in, filename, nfeatures, DEP_POS_WORD); break;

    case 'x': genrule_feats.load(in, filename, nfeatures, GEN_RULE); break;
    case 'm': rule_feats.load(in, filename, nfeatures, URULE); break;
    case 'n': rule_feats.load(in, filename, nfeatures, BRULE); break;

    case 'p': rule_head_feats.load(in, filename, nfeatures, URULE_HEAD); break;
    case 'q': rule_head_feats.load(in, filename, nfeatures, BRULE_HEAD); break;
    case 'r': rule_head_feats.load(in, filename, nfeatures, URULE_POS); break;
    case 's': rule_head_feats.load(in, filename, nfeatures, BRULE_POS); break;

    case 't': rule_dep_feats.load(in, filename, nfeatures, BRULE_HEAD_HEAD); break;
    case 'u': rule_dep_feats.load(in, filename, nfeatures, BRULE_POS_HEAD); break;
    case 'v': rule_dep_feats.load(in, filename, nfeatures, BRULE_HEAD_POS); break;
    case 'w': rule_dep_feats.load(in, filename, nfeatures, BRULE_POS_POS); break;

    case 'F': rule_dep_dist_feats.load(in, filename, nfeatures, DIST_ADJ_HEAD); break;
    case 'G': rule_dep_dist_feats.load(in, filename, nfeatures, DIST_VERBS_HEAD); break;
    case 'H': rule_dep_dist_feats.load(in, filename, nfeatures, DIST_PUNCT_HEAD); break;
    case 'I': rule_dep_dist_feats.load(in, filename, nfeatures, DIST_ADJ_POS); break;
    case 'J': rule_dep_dist_feats.load(in, filename, nfeatures, DIST_VERBS_POS); break;
    case 'K': rule_dep_dist_feats.load(in, filename, nfeatures, DIST_PUNCT_POS); break;

    case 'L': dep_dist_feats.load(in, filename, nfeatures, DIST_ADJ_HEAD); break;
    case 'M': dep_dist_feats.load(in, filename, nfeatures, DIST_VERBS_HEAD); break;
    case 'N': dep_dist_feats.load(in, filename, nfeatures, DIST_PUNCT_HEAD); break;
    case 'P': dep_dist_feats.load(in, filename, nfeatures, DIST_ADJ_POS); break;
    case 'Q': dep_dist_feats.load(in, filename, nfeatures, DIST_VERBS_POS); break;
    case 'R': dep_dist_feats.load(in, filename, nfeatures, DIST_PUNCT_POS); break;

    default:
      throw NLP::IOException("unexpected feature type in load features", filename, nfeatures);
    }
  }
  --nfeatures;
}

ulong
Parser::_Impl::get_feature(const std::string &filename, const std::string &line, vector<long> &rules) const {
  istringstream in(line);

  //std::cerr << "get_feature in parser.cc" << std::endl;

  char c;
  in >> c;
  std::string tmp;

  switch(c){
  case 'a': return cat_feats.get_id(in, filename, LEX_WORD, rules);
  case 'b': return cat_feats.get_id(in, filename, LEX_POS, rules);
  case 'c': return cat_feats.get_id(in, filename, ROOT, rules);
  case 'd': return cat_feats.get_id(in, filename, ROOT_WORD, rules);
  case 'e': return cat_feats.get_id(in, filename, ROOT_POS, rules);

  case 'f': return dep_feats.get_id(in, filename, DEP_WORD, rules);
  case 'g': return dep_feats.get_id(in, filename, DEP_POS, rules);
  case 'h': return dep_feats.get_id(in, filename, DEP_WORD_POS, rules);
  case 'i': return dep_feats.get_id(in, filename, DEP_POS_WORD, rules);

  case 'x': return genrule_feats.get_id(in, filename, GEN_RULE, rules);

  case 'm': return rule_feats.get_id(in, filename, URULE, rules);
  case 'n': return rule_feats.get_id(in, filename, BRULE, rules);

  case 'p': return rule_head_feats.get_id(in, filename, URULE_HEAD, rules);
  case 'q': return rule_head_feats.get_id(in, filename, BRULE_HEAD, rules);
  case 'r': return rule_head_feats.get_id(in, filename, URULE_POS, rules);
  case 's': return rule_head_feats.get_id(in, filename, BRULE_POS, rules);

  case 't': return rule_dep_feats.get_id(in, filename, BRULE_HEAD_HEAD, rules);
  case 'u': return rule_dep_feats.get_id(in, filename, BRULE_POS_HEAD, rules);
  case 'v': return rule_dep_feats.get_id(in, filename, BRULE_HEAD_POS, rules);
  case 'w': return rule_dep_feats.get_id(in, filename, BRULE_POS_POS, rules);

  case 'F': return rule_dep_dist_feats.get_id(in, filename, DIST_ADJ_HEAD, rules);
  case 'G': return rule_dep_dist_feats.get_id(in, filename, DIST_VERBS_HEAD, rules);
  case 'H': return rule_dep_dist_feats.get_id(in, filename, DIST_PUNCT_HEAD, rules);
  case 'I': return rule_dep_dist_feats.get_id(in, filename, DIST_ADJ_POS, rules);
  case 'J': return rule_dep_dist_feats.get_id(in, filename, DIST_VERBS_POS, rules);
  case 'K': return rule_dep_dist_feats.get_id(in, filename, DIST_PUNCT_POS, rules);

  case 'L': return dep_dist_feats.get_id(in, filename, DIST_ADJ_HEAD, rules);
  case 'M': return dep_dist_feats.get_id(in, filename, DIST_VERBS_HEAD, rules);
  case 'N': return dep_dist_feats.get_id(in, filename, DIST_PUNCT_HEAD, rules);
  case 'P': return dep_dist_feats.get_id(in, filename, DIST_ADJ_POS, rules);
  case 'Q': return dep_dist_feats.get_id(in, filename, DIST_VERBS_POS, rules);
  case 'R': return dep_dist_feats.get_id(in, filename, DIST_PUNCT_POS, rules);
  default:
    throw NLP::IOException("unexpected feature type in load feature", filename, nfeatures);
  }
}

void
Parser::_Impl::_load_weights(const std::string &filename){
  ifstream in(filename.c_str());
  if(!in)
    NLP::IOException("could not open features file", filename);

  weights = new double[nfeatures];
  ulong nread = 0;
  double weight;

  while(in >> weight){
    if(nread == nfeatures)
      throw NLP::IOException("number of features is less than the number of weights", filename, nread + 1);
    weights[nread++] = weight;
  }

  if(!in.eof())
    throw NLP::IOException("error reading weight value", filename, nread + 1);

  if(nread != nfeatures)
    throw NLP::IOException("number of features is greater than the number of weights", filename, nread + 1);
}

Parser::_Impl::_Impl(const Config &cfg, Sentence &sent, Categories &cats, const std::vector<double> &perceptronId, const bool train, const size_t srbeam, ulong load)
: cfg(cfg), sent(sent),
  nsentences(0),
  cats(cats),
  lexicon("lexicon", cfg.lexicon()),
  cat_feats(cats, lexicon),
  rule_feats(cats),
  rule_head_feats(cats, lexicon),
  rule_dep_feats(cats, lexicon),
  rule_dep_dist_feats(cats, lexicon),
  dep_feats(cats, lexicon),
  dep_dist_feats(cats, lexicon),
  genrule_feats(cats),
  weights(0),
  chart(cats, cfg.extra_rules(), cfg.maxwords()),
  rules(chart.pool, cats.markedup, cfg.extra_rules(), rule_instances),

  NP(cats.markedup["NP"]), NbN(cats.markedup["N\\N"]), NPbNP(cats.markedup["NP\\NP"]),
  SbS(cats.markedup["S\\S"]), SfS(cats.markedup["S/S"]),
  SbNPbSbNP(cats.markedup["(S\\NP)\\(S\\NP)"]),
  SbNPfSbNP(cats.markedup["(S\\NP)/(S\\NP)"]), SfSbSfS(cats.markedup["(S/S)\\(S/S)"]),
  SbNPbSbNPbSbNPbSbNP(cats.markedup["((S\\NP)\\(S\\NP))\\((S\\NP)\\(S\\NP))"]),
  NPfNPbNP(cats.markedup["NP/(NP\\NP)"]),

  //m_shiftReduceFeaturePool(new Pool(1 << 20)),
  m_shiftReduceFeatures(new ShiftReduceFeature(cats, lexicon)),
  //m_allowFragTree(cfg.allowFragTree),
  //m_allowFragAndComplete(cfg.allowFragAndComplete),
  m_allowFragTree(cfg.allowFrag()),
  //m_allowFragAndComplete(false),

  m_hasGoldAction(false),
  m_hasOutsideRule(false),
  m_lattice(0),
  m_beamSize(srbeam),
  m_perceptronId(perceptronId),
  m_train(train)

{
  cerr << "parser constructor parser.cc" << endl;
  cerr << "allowFragTree: " << m_allowFragTree << endl;
  cerr << "SR beam size: " << m_beamSize << endl;
  cerr << "is training: " << NLP::CCG::Integration::s_train << endl;
  cerr << "eisner: " << cfg.eisner_nf() << endl;

  if(load >= LOAD_FEATURES){
    _load_features(cfg.features());

    if(load >= LOAD_WEIGHTS){
      _load_weights(cfg.weights());

      cat_feats.set_weights(weights);
      rule_feats.set_weights(weights);
      rule_head_feats.set_weights(weights);
      rule_dep_feats.set_weights(weights);
      rule_dep_dist_feats.set_weights(weights);
      dep_feats.set_weights(weights);
      dep_dist_feats.set_weights(weights);
    }

    if(cfg.seen_rules() && cfg.eisner_nf())
    {
      string path = cfg.path() + string("//rules_nb_num_et");
      _load_rules(path);
    }

    if (cfg.seen_rules() && !cfg.eisner_nf())
    {
      string path = cfg.path() + string("//rules_nb_num_ef");
      _load_rules(path);
    }
  }

  sent.words.reserve(cfg.maxwords());
  sent.pos.reserve(cfg.maxwords());
  sent.msuper.reserve(cfg.maxwords());

  std::cerr << "parser _Impl constructor in parser.cc done" << std::endl;
  std::cerr << "lexicon size: " << lexicon.size() << std::endl;
}

void
Parser::_Impl::combine(Cell &left, Cell &right, long pos, long span){
  results.resize(0);
  for(Cell::iterator i = left.begin(); i != left.end(); ++i)
    for(Cell::iterator j = right.begin(); j != right.end(); ++j)
      if(!cfg.seen_rules() || rule_instances((*i)->cat, (*j)->cat))
        rules(*i, *j, cfg.eisner_nf(), cfg.seen_rules(), cfg.question_rules(), results);

  chart.add(pos, span, results);
}

void Parser::_Impl::Clear()
{
  chart.reset();
  chart.pool->clear();
  sent.reset();
  if (!m_train) delete m_lattice;

  if (m_train)
  {
    delete m_shiftReduceFeatures;
    m_shiftReduceFeatures = new ShiftReduceFeature(cats, lexicon);
  }
}


void Parser::_Impl::clear_xf1() {
	chart.pool->clear();
	sent.reset();
  delete m_lattice;
}


void Parser::_Impl::LoadGoldTree(const std::string &filename)
{
  std::cerr << "Parser::_Impl::LoadGoldTree start" << std::endl;
  size_t treeId = 0;

  ifstream in(filename.c_str());
  if(!in)
    throw NLP::IOException("could not open gold action file", filename);

  vector<ShiftReduceAction*> *tree = new vector<ShiftReduceAction*>;
  while (in)
  {
    string s;
    if (!getline(in, s)) break;
    if (s == "###")
    {
      if (tree->size() > 0)
        m_goldTreeVec.push_back(tree);
      ++treeId;
      tree = new vector<ShiftReduceAction*>;
      continue;
    }

    istringstream ss(s);
    vector<string> node;
    while (ss)
    {
      string s;
      if (!getline(ss, s, ' ')) break;
      node.push_back(s);
    }
    const Cat *cat = cats.markedup[node.at(0)];
    if(!cat)
    {
      try
      {
        cat = cats.canonize(node.at(0).c_str());
      }
      catch(NLP::Exception e)
      {
        cerr << "parse cat failed: " << node.at(0) << endl;
        throw NLP::IOException("attempting to parse category failed for non-SHIFT");
      }
    }
    assert(cat);
    ShiftReduceAction *action = new ShiftReduceAction(atoi(node.at(1).c_str()), cat);
    tree->push_back(action);
  }
  if (tree->size() > 0) m_goldTreeVec.push_back(tree);
  cerr << "total number of gold tress: " << m_goldTreeVec.size() << endl;
}

const ShiftReduceHypothesis* Parser::_Impl::ShiftReduceParse(const double BETA, bool qu_parsing)
{
  try
  {
    // training related
    vector<ShiftReduceAction*> nullVec;
    const ShiftReduceHypothesis *bestHypo; // the highest-scored hypo on each level
    const ShiftReduceHypothesis *goldHypo;
    //vector<ShiftReduceAction*> &goldTreeActions = m_train ? *m_goldTreeVec.at(202) : nullVec;
    vector<ShiftReduceAction*> &goldTreeActions = m_train ? *m_goldTreeVec.at(Integration::s_perceptronId[0] - 1) : nullVec;
    size_t goldTreeSize = goldTreeActions.size();
    ShiftReduceAction *goldAction = 0;
    size_t goldActionId = 0;
    bool goldFinished = false;

    assert(sent.words.size() != 0);
    if(sent.words.size() > cfg.maxwords())
      return 0;
    //throw NLP::ParseError("sentence length exceeds maximum number of words for parser", nsentences);

    const ShiftReduceHypothesis *candidateOutput = 0;
    Words words;
    raws2words(sent.words, words);
    Words tags;
    raws2words(sent.pos, tags);
    nsentences++;
    SuperCat::nsupercats = 0;
    const long NWORDS = sent.words.size();
    //cerr << "sent length: " << NWORDS << endl;
    const MultiRaws &input = sent.msuper;

    m_lattice = new ShiftReduceLattice(NWORDS, m_beamSize, MAX_UNARY_ACTIONS);
    ShiftReduceHypothesis *startHypo = new ShiftReduceHypothesis(0, 0, 0, 0, 0, 0, 0.0, false, 0, 0, 0, 0, 0.0, 0);
    startHypo->SetStartHypoFlag();
    m_lattice->Add(0, startHypo);
    if (m_train) goldHypo = m_lattice->GetLatticeArray(0); // gold == start
    size_t startIndex;

    //cerr << "sentence ID: " << Integration::s_perceptronId[0] << endl;

    // set start and end indexes of the lattice
    while (m_lattice->GetEndIndex() == 0 || m_lattice->GetEndIndex() > m_lattice->GetPrevEndIndex())
    {
      if (m_lattice->GetEndIndex() != 0)
        startIndex = m_lattice->GetPrevEndIndex() + 1;
      else
        startIndex = 0;
      m_lattice->SetPrevEndIndex(m_lattice->GetEndIndex());

      //cerr << "action ID: " << goldActionId << endl;

      HypothesisPQueue hypoQueue;
      for (size_t i = startIndex; i <= m_lattice->GetEndIndex(); ++i) {
        const ShiftReduceHypothesis *hypo = m_lattice->GetLatticeArray(i);
        size_t j = hypo->GetNextInputIndex();
        if (hypo->IsFinished(NWORDS, m_allowFragTree))
        {
          if (candidateOutput == 0 || hypo->GetTotalScore() > candidateOutput->GetTotalScore())
            candidateOutput = hypo;
        }
        ShiftReduceContext context;
        context.LoadContext(hypo, words, tags);

        // shift
        if (j < static_cast<size_t>(NWORDS))
        {
          const MultiRaw &multi = input[j];
          //double prob_cutoff = multi[0].score*BETA;
          for (MultiRaw::const_iterator k = multi.begin(); k!= multi.end(); ++k)
          {
            //if(k->score < prob_cutoff)
            //continue;
            const Cat *cat = cats.markedup[k->raw];
            if(!cat)
            {
              //cerr << "sent ID: " << Integration::s_perceptronId[0];
              throw NLP::ParseError(": SHIFT action error: attempted to load category without markedup " + k->raw);
            }
            SuperCat *stackTopCat = SuperCat::Lexical(chart.pool, j+1, cat, 0);
            assert(stackTopCat->cat);
            //maybe change this to non-pointer type
            ShiftReduceAction *actionShift = new ShiftReduceAction(SHIFT, stackTopCat);

            double actionScore = GetOrUpdateWeight(false, false, context, *actionShift, words, tags);
            double hypoTotalScore = hypo->GetTotalScore() + actionScore;
            HypothesisTuple *newHypoTuple = new HypothesisTuple(hypo, actionShift, hypoTotalScore, i);
            hypoQueue.push(newHypoTuple);

            //delete actionShift;
            //delete newHypoTuple;
          } // end for
        } // end if

        // unary
        if (hypo->GetStackTopSuperCat() != 0)
        {
          const SuperCat *sc = hypo->GetStackTopSuperCat();
          vector<SuperCat *> tmp;
          tmp.clear();
          if (!sc->IsLex() && !sc->IsTr())
          {
            UnaryLex(hypo->GetStackTopSuperCat(), qu_parsing, tmp);
            UnaryTr(sc, tmp);
          }
          else if (sc->IsLex() && !sc->IsTr())
          {
            UnaryTr(sc, tmp);
          }
          else if (!sc->IsLex() && sc->IsTr()) { /* do nothing */ }
          else if (sc->IsLex() && sc->IsTr()) { /* do nothing */ }

          for (vector<SuperCat *>::iterator j = tmp.begin(); j < tmp.end(); ++j)
          {
            SuperCat *stackTopCat = *j;
            assert(stackTopCat->left);
            ShiftReduceAction *actionUnary = new ShiftReduceAction(UNARY, stackTopCat);

            double actionScore = GetOrUpdateWeight(false, false, context, *actionUnary, words, tags);
            double hypoTotalScore = hypo->GetTotalScore() + actionScore;
            HypothesisTuple *newHypoTuple = new HypothesisTuple(hypo, actionUnary, hypoTotalScore, i);
            hypoQueue.push(newHypoTuple);
          }
        }

        // combine
        if (hypo->GetPrvStack() != 0 && hypo->GetPrvStack()->GetStackTopSuperCat() != 0
            && hypo->GetStackTopSuperCat() != 0)
        {
          //if (hypo == goldHypo) cerr << "*gold*" << endl;
          //cerr << "reduce: " << hypo->GetPrvStack()->GetStackTopSuperCat()->GetCat()->out_novar_noX_noNB_SR_str() << " and "
          //<< hypo->GetStackTopSuperCat()->GetCat()->out_novar_noX_noNB_SR_str() << endl;
          results.clear();
          if (!cfg.seen_rules() ||
              rule_instances(hypo->GetPrvStack()->GetStackTopSuperCat()->GetCat(), hypo->GetStackTopSuperCat()->GetCat()))
            rules(hypo->GetPrvStack()->GetStackTopSuperCat(), hypo->GetStackTopSuperCat(),
                cfg.eisner_nf(), cfg.seen_rules(), cfg.question_rules(), results);
          if (results.size() > 0)
          {
            for (vector<SuperCat *>::iterator k = results.begin(); k < results.end(); ++k)
            {
              SuperCat *stackTopCat = *k;
              //cerr << "result: " << stackTopCat->GetCat()->out_novar_noX_noNB_SR_str() << endl << endl;
              ShiftReduceAction *actionCombine = new ShiftReduceAction(COMBINE, stackTopCat);

              double actionScore = GetOrUpdateWeight(false, false, context, *actionCombine, words, tags);
              double hypoTotalScore = hypo->GetTotalScore() + actionScore;
              HypothesisTuple *newHypoTuple = new HypothesisTuple(hypo, actionCombine, hypoTotalScore, i);
              hypoQueue.push(newHypoTuple);
            }
          }
        }
      } // end for

      bestHypo = 0; // the highest-scored hypo on each level
      if (m_train && goldActionId < goldTreeSize)
      {
        goldAction = goldTreeActions[goldActionId];
        assert(goldAction);
      }
      if (goldActionId >= goldTreeSize) goldFinished = true;
      ConstructHypothesis(hypoQueue, m_lattice, bestHypo, goldHypo, goldAction, goldFinished);

      /*   //debug
	    if (m_train && m_hasOutsideRule && goldActionId < goldTreeSize)
	    {
		cerr << "sent ID: " << m_perceptronId[0] << " ###has outside rule### " << *goldAction <<  endl;
		cerr << "gold action is: ";
		if (goldAction->GetAction() == 0)
		    cerr << *goldAction << endl;
		if (goldAction->GetAction() == 1)
		{
		    ShiftReduceAction *prevAction = goldTreeActions[goldActionId - 1];
		    cerr << "1: " << "from " << prevAction->GetCat()->out_novar_noX_noNB_SR_str() << "to "
			    << goldAction->GetCat()->out_novar_noX_noNB_SR_str() << endl;
		}
		if (goldAction->GetAction() == 2)
		{
		    cerr << "2: " << goldHypo->GetPrvStack()->GetStackTopSuperCat()->GetCat()->out_novar_noX_noNB_SR_str() << " and "
			    << goldHypo->GetStackTopSuperCat()->GetCat()->out_novar_noX_noNB_SR_str() << endl;
		}
		cerr << endl << "at " << goldActionId + 1 << " out of " << goldTreeSize << " gold actions for sentence ID: " << Integration::s_perceptronId[0] << endl << endl;

		return 0;
	    }*/

      //assert(bestHypo != 0); best hypo *could* be 0

      // early update
      //std::cerr << "has gold action?: " << m_hasGoldAction << std::endl;
      // it's possible to have missed the gold action, e.g., goes beyond the number of
      // gold actions, but one previously updated candidate output already matches the gold
      // thus the need for the third condition
      if (m_train && !m_hasGoldAction && candidateOutput != goldHypo && goldActionId < goldTreeSize)
      {
        cerr << "########## Early Update, sentenceID: " << m_perceptronId[0] << endl;
        cerr << "found: " << goldActionId << " out of " << goldTreeSize << " gold actions" << endl;
        cerr << "faild to find gold action: " << goldAction->GetAction() << " " << goldAction->GetCat()->out_novar_noX_noNB_SR_str() << endl;
        cerr << "gold action is: ";
        if (goldAction->GetAction() == 0)
          cerr << *goldAction << endl;
        if (goldAction->GetAction() == 1)
        {
          ShiftReduceAction *prevAction = goldTreeActions[goldActionId - 1];
          cerr << "1: " << "from " << prevAction->GetCat()->out_novar_noX_noNB_SR_str() << "to "
              << goldAction->GetCat()->out_novar_noX_noNB_SR_str() << endl;
        }
        if (goldAction->GetAction() == 2)
        {
          cerr << "2: " << goldHypo->GetPrvStack()->GetStackTopSuperCat()->GetCat()->out_novar_noX_noNB_SR_str() << " and "
              << goldHypo->GetStackTopSuperCat()->GetCat()->out_novar_noX_noNB_SR_str() << endl;
        }

        assert(goldAction->GetCat());
        SuperCat *goldSuperCat = SuperCat::Wrap(chart.pool, goldAction->GetCat());
        assert(goldSuperCat);

        if (bestHypo == 0 || (candidateOutput && candidateOutput->GetTotalScore() > bestHypo->GetTotalScore()))
          bestHypo = candidateOutput;
        assert(bestHypo != 0);
        // not all gold actions have been found
        Action(goldHypo, goldAction->GetAction(), m_lattice, goldSuperCat, 0.0);
        goldHypo = m_lattice->GetLatticeArray(m_lattice->GetEndIndex());
        PerceptronUpdates(bestHypo, goldHypo, words, tags);
        std::cerr << "########## done early update" << std::endl;
        // hack exit
        //chart.reset();
        sent.reset();
        delete m_lattice;

        return 0;
      }
      if (m_train) ++goldActionId;
    } // end while

    // final update, if no early update
    if (m_train)
    {
      if (candidateOutput != goldHypo)
      {
        cerr << "######### late update" << endl;
        PerceptronUpdates(candidateOutput, goldHypo, words, tags);
      }
      else
      {
        cerr << "sentence: " << m_perceptronId[0] << "###### output correct #####" << endl;
      }
      sent.reset();
      delete m_lattice;
    }

    if (candidateOutput != 0)
      return candidateOutput;

    return 0;
  }
  catch(NLP::Exception e)
  {
    //throw NLP::ParseError(e.msg, nsentences);
    cerr << e.msg << endl;
    return 0;
  }
}

void Parser::_Impl::ConstructHypothesis(HypothesisPQueue &hypoQueue, ShiftReduceLattice *lattice,
    const ShiftReduceHypothesis *&bestHypo, const ShiftReduceHypothesis *&goldHypo,
    ShiftReduceAction *goldAction, const bool goldFinished) {
  bool hasGoldAction;
  if (m_train)
  {
    m_hasGoldAction = false;
    m_hasOutsideRule = false;
    hasGoldAction = false;
  }

  size_t size = hypoQueue.size() > m_beamSize ? m_beamSize : hypoQueue.size();
  for (size_t beam = 0; beam < size; ++beam) {
    HypothesisTuple *tuple = hypoQueue.top();
    const ShiftReduceHypothesis *hypo = tuple->GetCurrentHypo();
    assert(hypo);
    double totalScore = tuple->GetHypoTotalScore();
    size_t actionId = tuple->GetAction()->GetAction();
    const SuperCat *superCat = tuple->GetAction()->GetSuperCat();
    ShiftReduceAction action(actionId, superCat);

    Action(hypo, actionId, m_lattice, superCat, totalScore);

    if (m_train && !goldFinished)
    {
      const ShiftReduceHypothesis *newHypo = m_lattice->GetLatticeArray(m_lattice->GetEndIndex());
      assert(newHypo);
      if (bestHypo == 0 || newHypo->GetTotalScore() > bestHypo->GetTotalScore())
      {
        assert(m_lattice->GetEndIndex() != 0);
        bestHypo = newHypo;
        assert(bestHypo != 0);
      }
      if (hypo == goldHypo && action == *goldAction)
      {
        goldHypo = newHypo;
        m_hasGoldAction = true;
        hasGoldAction = true;
      }
    }
    hypoQueue.pop();
    delete tuple;
  } // end for

  // make this into a function of the PQ class?
  if (!hypoQueue.empty())
    assert(size == m_beamSize);
  else
    assert(size <= m_beamSize);
  while(!hypoQueue.empty())
  {
    HypothesisTuple *tuple = hypoQueue.top();
    hypoQueue.pop();
    delete tuple;
  }
  m_hasOutsideRule = !hasGoldAction;
  assert(hypoQueue.size() == 0);
}


void Parser::_Impl::Action(const ShiftReduceHypothesis *hypo, size_t action, ShiftReduceLattice *lattice,
    const SuperCat *supercat, double totalScore) {
  assert(supercat);
  assert(hypo);
  switch (action)
  {
//  case SHIFT:
//    m_lattice->Add(m_lattice->GetEndIndex() + 1, hypo->Shift(m_lattice->GetEndIndex() + 1, supercat,
//        hypo->GetUnaryActionCount(), totalScore));
//    m_lattice->IncrementEndIndex();
//    break;
//  case UNARY:
//    m_lattice->Add(m_lattice->GetEndIndex() + 1, hypo->Unary(m_lattice->GetEndIndex() + 1, supercat,
//        hypo->GetUnaryActionCount() + 1, totalScore));
//    m_lattice->IncrementEndIndex();
//    break;
//  case COMBINE:
//    m_lattice->Add(m_lattice->GetEndIndex() + 1, hypo->Combine(m_lattice->GetEndIndex() + 1, supercat,
//        hypo->GetUnaryActionCount(), totalScore));
//    m_lattice->IncrementEndIndex();
//    break;
  }
}

double Parser::_Impl::GetOrUpdateWeight(bool update, bool gold,
    const ShiftReduceContext &context, const ShiftReduceAction &action,
    const Words &words, const Words &tags) {
  return m_shiftReduceFeatures->GetOrUpdateWeight(update, gold, context, action, words, tags);
}

void Parser::_Impl::PerceptronUpdates(const ShiftReduceHypothesis *bestHypo, const ShiftReduceHypothesis *correctHypo,
    const Words &words, const Words &tags)
{
  assert(bestHypo);
  assert(correctHypo);
  PerceptronUpdate(bestHypo, false, words, tags);
  PerceptronUpdate(correctHypo, true, words, tags);
}

void Parser::_Impl::PerceptronUpdate(const ShiftReduceHypothesis *hypo, bool gold,
    const Words &words, const Words &tags)
{
  const ShiftReduceHypothesis *currentHypo = hypo;
  const ShiftReduceHypothesis *prevHypo = currentHypo->GetPrevHypo();
  //size_t totalActions = 0;
  while (prevHypo != NULL)
  {
    assert(prevHypo);
    ShiftReduceContext context;
    context.LoadContext(prevHypo, words, tags);

    ShiftReduceAction action(currentHypo->GetStackTopAction(), currentHypo->GetStackTopSuperCat());
    GetOrUpdateWeight(true, gold, context, action, words, tags);

    currentHypo = prevHypo;
    prevHypo = prevHypo->GetPrevHypo();
    assert(currentHypo != prevHypo);
  }
  assert(prevHypo == NULL);
}

void Parser::_Impl::SaveWeights(std::ofstream &out)
{
  m_shiftReduceFeatures->SaveWeights(out);
}

void Parser::_Impl::_add_lex(const Cat *cat, const SuperCat *sc, bool replace, RuleID ruleid, vector<SuperCat *> &tmp)
{
  SuperCat *new_sc = SuperCat::LexRule(chart.pool, cat, SuperCat::LEX, sc, replace, ruleid);
  new_sc->SetLex();
  assert(new_sc->left != 0);
  //cerr << "LEX" << endl;
  tmp.push_back(new_sc);
}

void Parser::_Impl::UnaryLex(const SuperCat *sc, bool qu_parsing, vector<SuperCat *> &tmp)
{
  // tmp will be the union of sc and lex'ed sc
  //tmp.push_back(sc);

  const Cat *cat = sc->cat;

  //cerr << "unary lex: " << cat->out_novar_noX_noNB_SR_str() << endl;

  if(cat->is_N())
    _add_lex(NP, sc, false, 1, tmp);
  else if(cfg.extra_rules.get_value() && cat->is_NP())
    _add_lex(NPfNPbNP, sc, false, 11, tmp);
  else if(cat->is_SbNP()){
    RuleID ruleid = 0;
    switch(cat->res->feature){
    case Features::DCL:
      if(!cfg.extra_rules.get_value())
        //continue;
        break;
      ruleid = 12;
      break;
    case Features::PSS:
      if(cfg.extra_rules.get_value()){
        _add_lex(SbNPbSbNP, sc, false, 17, tmp);
        _add_lex(SfS, sc, false, 13, tmp);
      }
      if(qu_parsing)
        _add_lex(NbN, sc, true, 95, tmp);
      ruleid = 2;
      break;
    case Features::NG:
      _add_lex(SbNPbSbNP, sc, false, 4, tmp);
      _add_lex(SfS, sc, false, 5, tmp);
      if(cfg.extra_rules.get_value()){
        _add_lex(SbNPfSbNP, sc, false, 18, tmp);
        _add_lex(SbS, sc, false, 16, tmp);
        _add_lex(NP, sc, false, 20, tmp);
      }
      ruleid = 3;
      break;
    case Features::ADJ:
      // given this a 93 id to match the gen_lex rule
      _add_lex(SbNPbSbNP, sc, false, 93, tmp);
      if(cfg.extra_rules.get_value())
        _add_lex(SfS, sc, false, 15, tmp);
      if(qu_parsing)
        _add_lex(NbN, sc, true, 94, tmp);
      ruleid = 6;
      break;
    case Features::TO:
      _add_lex(SbNPbSbNP, sc, false, 8, tmp);
      _add_lex(NbN, sc, true, 9, tmp);
      if(cfg.extra_rules.get_value())
        _add_lex(SfS, sc, false, 14, tmp);
      ruleid = 7;
      break;
    default:
      break;
      //continue;
    }
    _add_lex(NPbNP, sc, true, ruleid, tmp);
  }else if(cat->is_SfNP() && cat->res->has_dcl())
    _add_lex(NPbNP, sc, true, 10, tmp);
  else if(cfg.extra_rules.get_value() && cat->is_StobNPfNP())
    _add_lex(NPbNP, sc, true, 19, tmp);
  else if(cfg.extra_rules.get_value() && cat->is_Sdcl()){
    _add_lex(NPbNP, sc, false, 21, tmp);
    _add_lex(SbS, sc, false, 22, tmp);
  }
}

void Parser::_Impl::UnaryTr(const SuperCat *supercat, std::vector<SuperCat *> &tmp)
{
  //const size_t tmpSize = tmp.size();
  //for(size_t i = 0; i < tmpSize; ++i){
  //SuperCat *sc = tmp[i];
  const Cat *cat = supercat->cat;

  const TRCats *trcats = 0;
  if(cat->is_NP())
    trcats = &cats.trNP;
  else if(cat->is_AP())
    trcats = &cats.trAP;
  else if(cat->is_PP())
    trcats = &cats.trPP;
  else if(cat->is_StobNP())
    trcats = &cats.trVP_to;
  else { }

  if (trcats != 0)
  {
    for(TRCats::const_iterator j = trcats->begin(); j != trcats->end(); ++j)
    {
      //cerr << "TypeRaise" << endl;
      SuperCat *new_sc = SuperCat::TypeRaise(chart.pool, *j, SuperCat::TR, supercat, 11);
      assert(new_sc->left);
      new_sc->SetTr();
      //cerr << "tr cat: " << *new_sc->cat << endl;
      tmp.push_back(new_sc);
    }
  }
  //}
}

bool
Parser::_Impl::parse(const double BETA){
  try {
    nsentences++;
    SuperCat::nsupercats = 0;

    if(sent.words.size() > cfg.maxwords())
      throw NLP::ParseError("sentence length exceeds maximum number of words for parser", nsentences);

    chart.load(sent, BETA, true, cfg.question_rules());

    Words words;
    raws2words(sent.words, words);

    Words tags;
    raws2words(sent.pos, tags);

    const long NWORDS = sent.words.size();

    // doesn't make sense to apply beam to leaf cells
    // since the supertagger already provides a beam at this level
    // but still need to calculate beam scores to be used at higher levels
    if(cfg.beam() > 0.0)
      for(long i = 0; i < NWORDS; ++i)
        calc_beam_scores(chart(i,1), words, tags);

    for(long j = 2; j <= NWORDS; ++j){
      for(long i = j - 2; i >= 0; --i){
        for(long k = i + 1; k < j; ++k){
          try {
            combine(chart(i, k - i), chart(k, j - k), i, j - i);
          }catch(NLP::Exception e){
            throw NLP::ParseError(e.what(), nsentences);
          }

          if(SuperCat::nsupercats > cfg.maxsupercats())
            return false;
        }

        if(cfg.beam() > 0.0)
          if(j - i < NWORDS){
            calc_beam_scores(chart(i, j - i), words, tags);
            apply_beam(chart(i, j - i), cfg.beam());
          }

        if(j - i < NWORDS){
          chart.lex(i, j - i, cfg.question_rules());
          chart.tr(i, j - i);
        }

        if(SuperCat::nsupercats > cfg.maxsupercats())
          return false;
      }
    }

    return true;
  }catch(NLP::Exception e){
    throw NLP::ParseError(e.msg, nsentences);
  }
}

void
Parser::_Impl::calc_root_canonical(SuperCat *sc, const Words &words, const Words &tags){
  for(SuperCat *equiv = sc; equiv; equiv = const_cast<SuperCat *>(equiv->next)){
    calc_score(equiv, words, tags);
    equiv->score += cat_feats.score(equiv, words, tags, ROOT);
  }

  sc->marker = 1;
}

void
Parser::_Impl::calc_score(SuperCat *sc, const Words &words, const Words &tags){
  if(sc->left){
    if(sc->right)
      calc_score_binary(sc, words, tags);
    else
      calc_score_unary(sc, words, tags);
  }else
    calc_score_leaf(sc, words, tags);
}

void
Parser::_Impl::calc_score_canonical(SuperCat *sc, const Words &words, const Words &tags){
  if(sc->marker)
    return;

  for(SuperCat *equiv = sc; equiv; equiv = const_cast<SuperCat *>(equiv->next))
    calc_score(equiv, words, tags);
}

double
Parser::_Impl::score_binary_feats(SuperCat *sc, const Words &words, const Words &tags){
  double score = 0.0;

  score += dep_feats.score(sc, words, tags, DEP_WORD_POS);
  score += dep_dist_feats.score(sc, words, tags, DEP_WORD_POS);
  score += rule_feats.score(sc, words, tags, BRULE);
  score += rule_head_feats.score(sc, words, tags, BRULE);
  score += genrule_feats.score(sc, words, tags, GEN_RULE);  //last 3 arguments don't get used
  score += rule_dep_feats.score(sc, words, tags, BRULE);
  score += rule_dep_dist_feats.score(sc, words, tags, BRULE);

  return score;
}

void
Parser::_Impl::calc_score_binary(SuperCat *sc, const Words &words, const Words &tags){
  sc->marker = 1;

  calc_score_canonical(const_cast<SuperCat *>(sc->left), words, tags);
  calc_score_canonical(const_cast<SuperCat *>(sc->right), words, tags);

  sc->score = score_binary_feats(sc, words, tags);
}

void
Parser::_Impl::calc_score_unary(SuperCat *sc, const Words &words, const Words &tags){
  sc->marker = 1;

  sc->score = rule_feats.score(sc, words, tags, URULE);
  sc->score += rule_head_feats.score(sc, words, tags, URULE);
  sc->score += genrule_feats.score(sc, words, tags, GEN_RULE); // sc: last 3 args not used

  calc_score_canonical(const_cast<SuperCat *>(sc->left), words, tags);
}

void
Parser::_Impl::calc_score_leaf(SuperCat *sc, const Words &words, const Words &tags){
  sc->marker = 1;
  sc->score = cat_feats.score(sc, words, tags, LEX);
}

bool
Parser::_Impl::calc_scores(void){
  Cell &root = chart.root();
  if(root.size() == 0)
    return false;

  Words words;
  raws2words(sent.words, words);

  Words tags;
  raws2words(sent.pos, tags);

  for(Cell::iterator i = root.begin(); i != root.end(); ++i)
    calc_root_canonical(*i, words, tags);

  return true;
}

ulong
Parser::_Impl::dependency_marker(const Filled *filled, const Variable *vars) const {
  Hash h(filled->head);
  h += filled->filler;
  h += filled->rel;
  h += filled->rule + filled->lrange;
  return h.value();
}

void
Parser::_Impl::raws2words(const vector<std::string> &raw, Words &words) const {
  words.resize(0);
  words.reserve(sent.words.size());
  for(vector<std::string>::const_iterator i = raw.begin(); i != raw.end(); ++i)
    words.push_back(lexicon[*i]);
}

//print_data() function is obsolete
bool
Parser::_Impl::print_data(ostream &out, ulong id){
  Cell &root = chart.root();
  if(root.size() == 0)
    return 0;

  const long NWORDS = sent.words.size();

  // check for S[dcl] and mark active nodes
  bool Sdcl = false;
  ulong ndisj = 0;
  for(Cell::iterator i = root.begin(); i != root.end(); ++i)
    if((*i)->cat->is_Sdcl()){
      Sdcl = true;
      (*i)->mark_active_disj(ndisj);
    }

  if(!Sdcl)
    return 0;

  // print sentence and all lexical categories
  for(long i = 0; i < NWORDS; ++i){
    if(i > 0)
      out << ' ';
    out << sent.words[i] << '|' << sent.pos[i];
  }
  out << '\n';

  for(long i = 0; i < NWORDS; ++i){
    Cell &cell = chart(i, 1);
    ulong c = 0;
    for(Cell::iterator j = cell.begin(); j != cell.end(); ++j)
      if(!((*j)->left)){
        if(c > 0)
          out << ' ';
        (*j)->cat->out_novar_noX(out, false);
        c++;
      }
    out << '\n';
  }

  // print unambiguous lexical categories
  for(long i = 0; i < NWORDS; ++i){
    Cell &cell = chart(i, 1);
    ulong ncats = 0;
    for(Cell::iterator j = cell.begin(); j != cell.end(); ++j)
      if((*j)->marker == 1 && !((*j)->left))
        ncats++;

    if(ncats <= 0)
      throw NLP::IOException("number of lexical cats is zero");
    else if(ncats == 1){
      for(Cell::iterator j = cell.begin(); j != cell.end(); ++j)
        // marker can now also take the value 2
        if((*j)->marker == 1 && !((*j)->left)){
          out << i << ' ';
          (*j)->cat->out_novar_noX(out, false);
          out << '\n';
          break;
        }
    }
  }
  out << '\n';

  return 1;
}

const SuperCat *
Parser::_Impl::best(Decoder &decoder){
  return decoder.best(chart);
}

// counts the number of derivations each dependency occurs in --
// relies on having a uniform weights file
bool
Parser::_Impl::deps_count(ostream &out){
  calc_scores();
  inside_outside.calc(chart);

  // TODO allow an option to choose between these
  // top version outputs the words rather than numbers
  //  depscores.dump(out, cats.markedup, cats.relations, sentence);
  inside_outside.depscores.dump(out);
  out << '\n';

  return 1;
}


double
Parser::_Impl::calc_stats(ulong &nequiv, ulong &ntotal){
  return inside_outside.calc_stats(chart, nequiv, ntotal);
}


Parser::Parser(void){};

Parser::Parser(const Config &cfg, Sentence &sent, Categories &cats,
               const std::vector<double> &perceptronId,
               const bool train, const size_t srbeam, ulong load){
  _impl = new Parser::_Impl(cfg, sent, cats, perceptronId, train, srbeam, load);
}

Parser::~Parser(void){ delete _impl; }

bool Parser::parse(double BETA){ return _impl->parse(BETA); }
void Parser::LoadGoldTree(const std::string &filename) { return _impl->LoadGoldTree(filename); }
void Parser::LoadModel(const std::string &filename) { return _impl->LoadModel(filename); }
void Parser::Clear() { _impl->Clear(); }
void Parser::clear_xf1() { _impl->clear_xf1(); }


const ShiftReduceHypothesis *Parser::ShiftReduceParse(const double BETA, bool qu_parsing)
{
  return _impl->ShiftReduceParse(BETA, qu_parsing);
}

void Parser::SaveWeights(std::ofstream &out)
{
  return _impl->SaveWeights(out);
}

bool Parser::deps_count(ostream &out){ return _impl->deps_count(out); }

bool Parser::count_rules(void){
  return _impl->count_rules();
}

void Parser::print_rules(std::ostream &out) const {
  _impl->rule_attrs.print_entries(out);
}

bool Parser::print_forest(InsideOutside &inside_outside, ostream &out, ulong id,
    const vector<ulong> &correct, const vector<long> &rules){
  return _impl->print_forest(inside_outside, out, id, correct, rules);
}

bool Parser::calc_scores(void){ return _impl->calc_scores(); }

const SuperCat *Parser::best(Decoder &decoder){
  return _impl->best(decoder);
}

ulong Parser::get_feature(const std::string &filename, const std::string &line,
    std::vector<long> &rules) const {
  return _impl->get_feature(filename, line, rules);
}

bool Parser::is_partial_gold(void) const {
  return _impl->cfg.partial_gold();
}

double Parser::calc_stats(ulong &nequiv, ulong &ntotal){
  return _impl->calc_stats(nequiv, ntotal);
}

} }
