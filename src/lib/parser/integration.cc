// C&C NLP tools
// Copyright (c) Universities of Edinburgh, Oxford and Sydney
// Copyright (c) James R. Curran
//
// This software is covered by a non-commercial use licence.
// See LICENCE.txt for the full text of the licence.
//
// If LICENCE.txt is not included in this distribution
// please email candc@it.usyd.edu.au to obtain a copy.

#include "base.h"

#include "config/config.h"

#include "pool.h"

#include "model/model.h"

#include "io/format.h"
#include "config/format.h"

#include "io/reader.h"
#include "io/writer.h"

#include "tagger/tagdict.h"
#include "tagger/tagsetdict.h"
#include "tagger/tagger.h"
#include "tagger/super.h"

#include "parser/parser.h"
#include "parser/decoder_factory.h"
#include "parser/printer.h"

#include "parser/integration.h"

#include "parser/ShiftReduceHypothesis.h"

/*
#include <iostream>
#include <fstream>
 */

using namespace std;

namespace NLP { namespace CCG {


const static ulong DEF_START = 0;

Integration::Betas
def_betas(void){
	Integration::Betas def(5);

	def[0] = 0.075;
	def[1] = 0.03;
	def[2] = 0.01;
	def[3] = 0.005;
	def[4] = 0.001;

	return def;
}

Integration::DictCutoffs
def_dict_cutoffs(void){
	Integration::DictCutoffs def(5);

	def[0] = 20;
	def[1] = 20;
	def[2] = 20;
	def[3] = 20;
	def[4] = 150;

	return def;
}


Integration::Config::Config(const std::string &name, const std::string &desc)
: Cfg(name, desc),
	start(*this, "start_level", "the first level of beta/dict_cutoff to use", DEF_START),
	betas(*this, "betas", "the super tagging beta levels", def_betas()),
	dict_cutoffs(*this, "dict_cutoffs", "the super tagging dict_cutoff levels", def_dict_cutoffs()){}

void
Integration::Config::check(void){
	Cfg::check();

	if(betas().size() != dict_cutoffs().size())
		throw ConfigError("there must be the same number of beta and dict_cutoff levels", "betas");

	if(start() >= betas().size())
		throw ConfigError("the start_level must be less than the number of beta and dict_cutoff levels", "start_level");
}

Integration::Integration(Integration::Config &int_cfg,
		Super::Config &super_cfg,
		Parser::Config &parser_cfg,
		Sentence &sent, const bool train, const size_t srbeam, ulong load)
: START(int_cfg.start()), BETAS(int_cfg.betas()),
	MIN_BETA(*std::min_element(BETAS.begin(), BETAS.end())),
	DICT_CUTOFFS(int_cfg.dict_cutoffs()),
	super(super_cfg),
	cats(parser_cfg.cats(), parser_cfg.markedup(), parser_cfg.alt_markedup()),
	m_srbeam(srbeam),
	parser(parser_cfg, sent, cats, s_perceptronId, train, m_srbeam, load),
	nsentences(0), nwords(0), nexceptions(0),
	nfail_nospan(0), nfail_explode(0), nfail_nospan_explode(0),
	nfail_explode_nospan(0), nsuccesses(BETAS.size(), 0){}



Integration::Integration(Integration::Config &int_cfg,
		Super::Config &super_cfg,
		Parser::Config &parser_cfg)
: START(int_cfg.start()), BETAS(int_cfg.betas()),
	MIN_BETA(*std::min_element(BETAS.begin(), BETAS.end())),
	DICT_CUTOFFS(int_cfg.dict_cutoffs()),
	super(super_cfg),
	cats(parser_cfg.cats(), parser_cfg.markedup(), parser_cfg.alt_markedup()),
	m_srbeam(0),
	nsentences(0), nwords(0), nexceptions(0),
	nfail_nospan(0), nfail_explode(0), nfail_nospan_explode(0),
	nfail_explode_nospan(0), nsuccesses(BETAS.size(), 0){}


Integration::Integration(Integration::Config &int_cfg,
    Super::Config &super_cfg,
    RNNParser::RNNConfig &parser_cfg,
    Sentence &sent, const bool train, const size_t srbeam, ulong load)
: START(int_cfg.start()), BETAS(int_cfg.betas()),
  MIN_BETA(*std::min_element(BETAS.begin(), BETAS.end())),
  DICT_CUTOFFS(int_cfg.dict_cutoffs()),
  super(super_cfg),
  cats(parser_cfg.cats(), parser_cfg.markedup(), parser_cfg.alt_markedup()),
  m_srbeam(srbeam),
  nsentences(0), nwords(0), nexceptions(0),
  nfail_nospan(0), nfail_explode(0), nfail_nospan_explode(0),
  nfail_explode_nospan(0), nsuccesses(BETAS.size(), 0){}

/*
    m_int_cfg(int_cfg),
    m_super_cfg(super_cfg),
    m_parser_cfg(parser_cfg),
    m_sent(sent)*/

/*void Integration::DestoryParser()
{
    parser.~Parser();
    parser = Parser(m_parser_cfg, m_sent, cats, Parser::LOAD_NONE);
}*/

//void Integration::ClearParser()
//{
//	parser.Clear();
//}

//void Integration::LoadGoldTree(const std::string &filename)
//{
//	parser.LoadGoldTree(filename);
//}
//
//void Integration::LoadModel(const std::string &filename)
//{
//	parser.LoadModel(filename);
//}

//void Integration::SaveWeights(std::string &path)
//{
//	ofstream modelOutput;
//	modelOutput.open(path.c_str());
//	parser.SaveWeights(modelOutput);
//	modelOutput.close();
//}

void Integration::LoadSupercats(const std::string &filename)
{
	std::cerr << "Integration::LoadSupercats start" << std::endl;
	ifstream in(filename.c_str());
	if(!in)
		throw NLP::IOException("could not open supercats file", filename);
	std::vector<std::vector<std::string>* >* sent =  new std::vector<std::vector<std::string>* >;
	size_t sentcount = 0;
	size_t catcount = 0;
	while (in)
	{
		string s;
		if (!getline(in, s)) break;
		if (s.empty())
		{
			if (sent->size() > 0)
				m_supercatsVec.push_back(sent);
			++sentcount;
			sent = new std::vector<std::vector<std::string>* >;
			continue;
		}

		istringstream ss(s);
		size_t count = 0;
		std::vector<std::string>* word = new vector<string>;
		while (ss)
		{
			string s;
			if (!getline(ss, s, ' ')) break;
			++count;
			if (count <= 2) continue;
			//const Cat *cat = cats.markedup[s];
			//if (!cat)
			//{
			//	std::cerr << "sent ID: " << sentcount + 1 << " " << s << " not in markedup" << std::endl;
			//	continue;
			// }
			++catcount;
			word->push_back(s);
		}
		assert(word->size() > 0);
		sent->push_back(word);
	}
	std::cerr << "total super: " << catcount << " total sent: " << sentcount << std::endl;
	std::cerr << "done" << std::endl;
}


//bool Integration::ShiftReduceParse(const bool train, Sentence &sent, Decoder &decoder, Printer &printer, bool allowFrag,
//		IO::Output &out, IO::Output &trace, const Format FORMAT, const bool USE_SUPER, const bool TRACE)
//{
//	double beta = 0.0001;
//	ulong dict_cutoff = 20;
//	if (!train)
//	{
//		try
//		{
//			if(sent.words.size() == 1)
//			{
//				std::cerr << "size = 1" << std::endl;
//				super.mtag(sent, NLP::Tagger::FWDBWD, dict_cutoff, beta);
//				sent.cats.clear();
//				printer.parsed(0, sent, beta, dict_cutoff);
//				lexical(sent, out, FORMAT);
//				out.stream << "\n";
//				return true;
//
//				/*		std::cerr << "size = 1" << std::endl;
//		super.mtag(sent, NLP::Tagger::FWDBWD, dict_cutoff, beta);
//		force_gold_super(sent);
//		//sent.cats.clear();
//		printer.parsed(0, sent, beta, dict_cutoff);
//		lexical(sent, out, FORMAT);
//		out.stream << "\n";
//		return true;*/
//			}
//			else
//			{
//				super.mtag(sent, NLP::Tagger::FWDBWD, dict_cutoff, beta);
//				const ShiftReduceHypothesis *output = parser.ShiftReduceParse(beta, false);
//
//				if (!output)
//				{
//					parser.Clear();
//					return false;
//				}
//
//				assert(output != 0);
//				size_t stackSize = output->GetStackSize();
//
//				if (TRACE)
//				{
//					const ShiftReduceHypothesis *currentHypo = output;
//					size_t action_count = currentHypo->GetTotalActionCount();
//					size_t unary_count = currentHypo->GetUnaryActionCount();
//					trace.stream << "sent: " << Integration::s_perceptronId[0] << " stackSize: " << stackSize;
//					trace.stream << " sent length: " << sent.words.size() << " "
//							<< " unary action count: " << unary_count  << " "
//							<< " action count: " << action_count;
//					trace.stream << "\n";
//					lexical(sent, trace, FORMAT);
//					trace.stream << "\n";
//
//					//const ShiftReduceHypothesis *prevHypo = currentHypo->GetPrevHypo();
//					//size_t totalActions = 0;
//					for (size_t i = 0; i < action_count; ++i)
//					{
//						//assert(prevHypo);
//						//currentHypo = prevHypo;
//						trace.stream << currentHypo->GetStackTopAction() << " "  << currentHypo->GetStackTopSuperCat()->GetCat()->out_novar_noX_noNB_SR_str();
//						trace.stream << "\n";
//
//						currentHypo = currentHypo->GetPrevHypo();
//						//if (currentHypo->GetStackSize() == 0)
//						//break;
//						//assert(currentHypo != prevHypo);
//					}
//					trace.stream << "\n";
//					//assert(prevHypo == NULL);
//				}
//
//				std::vector<const SuperCat *> roots;
//				vector<const SuperCat *>::const_reverse_iterator rit;
//				roots.push_back(output->GetStackTopSuperCat());
//
//				//std::cerr << "final stack cat: " << output->GetStackTopSuperCat()->GetCat()->out_novar_noX_noNB_SR_str() << std::endl;
//				//std::cerr << "sent ID: " << Integration::s_perceptronId[0] << " stackSize: " << stackSize << std::endl;
//				for (size_t i = 0; i < stackSize - 1; ++i)
//				{
//					//std::cerr << "output->GetPrvStack(): " << output->GetPrvStack()->GetStackTopSuperCat()->GetCat()->out_novar_noX_noNB_SR_str() << std::endl;
//					output = output->GetPrvStack();
//					roots.push_back(output->GetStackTopSuperCat());
//				}
//
//				//std::vector<const CCG::Cat *> catsCopy = sent.cats;
//				//sent.cats.clear();
//				for (rit = roots.rbegin(); rit < roots.rend(); ++rit)
//				{
//					//std::cerr << "printer.parsed cat: " << (*rit)->GetCat()->out_novar_noX_noNB_SR_str() << std::endl;
//					printer.parsed(*rit, sent, beta, dict_cutoff);
//
//					//ulong nequiv, ntotal;
//					//double logderivs = parser.calc_stats(nequiv, ntotal);
//					//printer.stats(logderivs, nequiv, ntotal);
//				}
//				// maybe not needed, check
//				//if (sent.cats.size() == 0)
//				//sent.cats = catsCopy;
//				lexical(sent, out, FORMAT);
//				out.stream << "\n";
//
//				parser.Clear();
//				return true;
//			}
//		}
//		catch(NLP::ParseError e)
//		{
//			++nexceptions;
//			printer.error(e.msg, sent, beta, dict_cutoff);
//			return false;
//		}
//	}
//	else
//	{
//		try
//		{
//			//ulong last_dict_cutoff = 0;
//			++nsentences;
//			nwords += sent.words.size();
//			//super.mtag(sent, NLP::Tagger::FWDBWD, dict_cutoff, beta);
//			std::vector<std::vector<std::string>* > &sentcats = *m_supercatsVec[static_cast<size_t>(Integration::s_perceptronId[0] - 1)];
//			sent.msuper.resize(sentcats.size());
//			assert(sentcats.size() == sent.words.size());
//			for (size_t i = 0; i < sentcats.size(); ++i)
//			{
//				std::vector<std::string> &wordcats = *sentcats[i];
//				MultiRaw &cats = sent.msuper[i];
//				cats.resize(0);
//				for (size_t j = 0; j < wordcats.size(); ++j)
//					cats.push_back(ScoredRaw(wordcats[j], 0.0));
//			}
//			//force_gold_super(sent);
//			const ShiftReduceHypothesis *output = parser.ShiftReduceParse(beta, false);
//			if (output) return true;
//			return false;
//		}
//		catch(NLP::ParseError e)
//		{
//			++nexceptions;
//			printer.error(e.msg, sent, beta, dict_cutoff);
//			return false;
//		}
//	}
//}


//bool Integration::ShiftReduceParseRnn(const bool train, Sentence &sent, Decoder &decoder,
//		                                  Printer &printer, bool allowFrag,
//		                                  IO::Output &out, IO::Output &trace, const Format FORMAT,
//		                                  const vector<vector<std::pair<std::string, double> > > &tags_rnn,
//		                                  const bool USE_SUPER, const bool TRACE)
//{
//	double beta = 0.08;
//	ulong dict_cutoff = 20;
//	if (!train)
//	{
//		try
//		{
//			if(sent.words.size() == 1)
//			{
//				std::cerr << "size = 1" << std::endl;
//				super.mtag(sent, NLP::Tagger::FWDBWD, dict_cutoff, beta);
//				sent.cats.clear();
//				printer.parsed(0, sent, beta, dict_cutoff);
//				lexical(sent, out, FORMAT);
//				out.stream << "\n";
//				return true;
//			}
//			else
//			{
//				//super.mtag(sent, NLP::Tagger::FWDBWD, dict_cutoff, beta);
//        sent.msuper.resize(tags_rnn.size());
//        assert(tags_rnn.size() == sent.words.size());
//	      for (size_t i = 0; i < tags_rnn.size(); ++i) {
//	        MultiRaw &mraw = sent.msuper[i];
//	        mraw.resize(0);
//	        MultiRaw temp;
//
//	        for (size_t j = 0; j < tags_rnn[i].size(); ++j)
//	          temp.push_back(ScoredRaw(tags_rnn[i][j].first, tags_rnn[i][j].second));
//
//	        std::sort(temp.begin(), temp.end());
//	        assert(temp[0].score >= temp[1].score);
//
//	        double cut_off = temp[0].score*beta;
//
//	        for (size_t j = 0; j < temp.size(); ++j) {
//            if (temp[j].score < cut_off)
//            	continue;
//            mraw.push_back(ScoredRaw(temp[j].raw, temp[j].score));
//	        }
//	      }
//
//				const ShiftReduceHypothesis *output = parser.ShiftReduceParse(beta, false);
//
//				if (!output)
//				{
//					parser.Clear();
//					return false;
//				}
//
//				assert(output != 0);
//				size_t stackSize = output->GetStackSize();
//
//				std::vector<const SuperCat *> roots;
//				vector<const SuperCat *>::const_reverse_iterator rit;
//				roots.push_back(output->GetStackTopSuperCat());
//
//				//std::cerr << "final stack cat: " << output->GetStackTopSuperCat()->GetCat()->out_novar_noX_noNB_SR_str() << std::endl;
//				//std::cerr << "sent ID: " << Integration::s_perceptronId[0] << " stackSize: " << stackSize << std::endl;
//				for (size_t i = 0; i < stackSize - 1; ++i)
//				{
//					//std::cerr << "output->GetPrvStack(): " << output->GetPrvStack()->GetStackTopSuperCat()->GetCat()->out_novar_noX_noNB_SR_str() << std::endl;
//					output = output->GetPrvStack();
//					roots.push_back(output->GetStackTopSuperCat());
//				}
//
//				//std::vector<const CCG::Cat *> catsCopy = sent.cats;
//				//sent.cats.clear();
//				for (rit = roots.rbegin(); rit < roots.rend(); ++rit)
//				{
//					//std::cerr << "printer.parsed cat: " << (*rit)->GetCat()->out_novar_noX_noNB_SR_str() << std::endl;
//					printer.parsed(*rit, sent, beta, dict_cutoff);
//
//					//ulong nequiv, ntotal;
//					//double logderivs = parser.calc_stats(nequiv, ntotal);
//					//printer.stats(logderivs, nequiv, ntotal);
//				}
//				// maybe not needed, check
//				//if (sent.cats.size() == 0)
//				//sent.cats = catsCopy;
//				lexical(sent, out, FORMAT);
//				out.stream << "\n";
//
//				parser.Clear();
//				return true;
//			}
//		}
//		catch(NLP::ParseError e)
//		{
//			++nexceptions;
//			printer.error(e.msg, sent, beta, dict_cutoff);
//			return false;
//		}
//	}
//	else
//	{
//		try
//		{
//			//ulong last_dict_cutoff = 0;
//			++nsentences;
//			nwords += sent.words.size();
//			//super.mtag(sent, NLP::Tagger::FWDBWD, dict_cutoff, beta);
//			std::vector<std::vector<std::string>* > &sentcats = *m_supercatsVec[static_cast<size_t>(Integration::s_perceptronId[0] - 1)];
//			sent.msuper.resize(sentcats.size());
//			assert(sentcats.size() == sent.words.size());
//			for (size_t i = 0; i < sentcats.size(); ++i)
//			{
//				std::vector<std::string> &wordcats = *sentcats[i];
//				MultiRaw &cats = sent.msuper[i];
//				cats.resize(0);
//				for (size_t j = 0; j < wordcats.size(); ++j)
//					cats.push_back(ScoredRaw(wordcats[j], 0.0));
//			}
//			//force_gold_super(sent);
//			const ShiftReduceHypothesis *output = parser.ShiftReduceParse(beta, false);
//			if (output) return true;
//			return false;
//		}
//		catch(NLP::ParseError e)
//		{
//			++nexceptions;
//			printer.error(e.msg, sent, beta, dict_cutoff);
//			return false;
//		}
//	}
//}


//bool
//Integration::parse(Sentence &sent, Decoder &decoder, Printer &printer, const bool USE_SUPER){
//	double beta = 0.0;
//	ulong dict_cutoff = 0;
//
//	try {
//
//		ulong last_dict_cutoff = 0;
//		int trial = START;
//		int last = START;
//
//		++nsentences;
//		nwords += sent.words.size();
//
//		while(1){
//			beta = BETAS[trial];
//			dict_cutoff = DICT_CUTOFFS[trial];
//
//			if(USE_SUPER && dict_cutoff != last_dict_cutoff)
//				super.mtag(sent, NLP::Tagger::FWDBWD, dict_cutoff, MIN_BETA);
//			last_dict_cutoff = dict_cutoff;
//
//			if(sent.words.size() == 1){
//				++nsuccesses[trial];
//				printer.parsed(0, sent, beta, dict_cutoff);
//				return true;
//			}else if(parser.parse(beta)){
//				parser.calc_scores();
//
//				const SuperCat *root = 0;
//				if((root = parser.best(decoder)) != 0){
//					++nsuccesses[trial];
//					printer.parsed(root, sent, beta, dict_cutoff);
//
//					ulong nequiv, ntotal;
//					double logderivs = parser.calc_stats(nequiv, ntotal);
//					printer.stats(logderivs, nequiv, ntotal);
//
//					return true;
//				}else{
//					printer.attempted("nospan", sent, beta, dict_cutoff);
//					if(trial - last < 0){
//						++nfail_explode_nospan;
//						printer.failed("explode/nospan", sent, beta, dict_cutoff);
//						return false;
//					}
//
//					last = trial++;
//					if(trial == static_cast<long>(BETAS.size())){
//						++nfail_nospan;
//						printer.failed("no span", sent, beta, dict_cutoff);
//						return false;
//					}
//				}
//			}else{
//				printer.attempted("exploded", sent, beta, dict_cutoff);
//				if(trial - last > 0){
//					++nfail_nospan_explode;
//					printer.failed("nospan/explode", sent, beta, dict_cutoff);
//					return false;
//				}
//				last = trial--;
//				if(trial == -1){
//					++nfail_explode;
//					printer.failed("explode", sent, beta, dict_cutoff);
//					return false;
//				}
//			}
//		}
//	}catch(NLP::ParseError e){
//		++nexceptions;
//		printer.error(e.msg, sent, beta, dict_cutoff);
//		return false;
//	}
//}

} }
