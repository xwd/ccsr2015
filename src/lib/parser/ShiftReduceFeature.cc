#include "parser/ShiftReduceFeature.h"
#include "parser/ShiftReduceContext.h"
#include "parser/GraphFeature.h"
#include "tree/_baseimpl.h"

#include "parser/categories.h"

#include <iostream>
#include <fstream>

using namespace std;

namespace NLP
{
namespace CCG
{

using namespace NLP::HashTable;
using namespace NLP::Tree;

double ShiftReduceFeature::GetOrUpdateWeight(bool update, bool correct, const ShiftReduceContext &context, const ShiftReduceAction &action,
    const Words &words, const Words &tags)
{
  double weight = 0.0;

  // 1
  if (context.s0w.size() > 0)
  {
    assert(context.s0p.size() > 0 && context.s0w.size() == context.s0p.size());
    assert(context.s0c != 0);
    std::vector<Word>::const_iterator iterw;
    std::vector<Word>::const_iterator iterp;
    assert(context.s0w.size() == context.s0p.size());
    for (iterw = context.s0w.begin(), iterp = context.s0p.begin();
        iterw != context.s0w.end(), iterp != context.s0p.end();
        ++iterw, ++iterp)
    {
      // s0wp, s0wc
      Word s0w = *iterw;
      Word s0p = *iterp;
      weight += swpAttrs.GetOrUpdateWeight(update, correct, s0wp, s0w, s0p, action);
      weight += swcAttrs.GetOrUpdateWeight(update, correct, s0wc, s0w, context.s0c->cat, action);
      // s0pc
      weight += spcAttrs.GetOrUpdateWeight(update, correct, s0pc, s0p, context.s0c->cat, action);
    }
    // s0c_
    weight += scAttrs.GetOrUpdateWeight(update, correct, s0c_, context.s0c->cat, action);
  }

  if (context.s1w.size() > 0)
  {
    assert(context.s1p.size() > 0 && context.s1w.size() == context.s1p.size());
    assert(context.s1c != 0);
    std::vector<Word>::const_iterator iterw;
    std::vector<Word>::const_iterator iterp;
    for (iterw = context.s1w.begin(), iterp = context.s1p.begin();
        iterw != context.s1w.end(), iterp != context.s1p.end();
        ++iterw, ++iterp)
    {
      // s1wp, s1wc
      Word s1w = *iterw;
      Word s1p = *iterp;
      weight += swpAttrs.GetOrUpdateWeight(update, correct, s1wp, s1w, s1p, action);
      weight += swcAttrs.GetOrUpdateWeight(update, correct, s1wc, s1w, context.s1c->cat, action);
      // s1pc
      weight += spcAttrs.GetOrUpdateWeight(update, correct, s1pc, s1p, context.s1c->cat, action);
    }
    // s1c_
    weight += scAttrs.GetOrUpdateWeight(update, correct, s1c_, context.s1c->cat, action);
  }

  if (context.s2p.size() > 0)
  {
    assert(context.s2c != 0);
    assert(context.s2w.size() == context.s2p.size());
    // s2pc
    for (std::vector<Word>::const_iterator iter = context.s2p.begin(); iter != context.s2p.end(); ++iter)
    {
      Word s2p = *iter;
      weight += spcAttrs.GetOrUpdateWeight(update, correct, s2pc, s2p, context.s2c->cat, action);
    }
    // s2wc
    for (std::vector<Word>::const_iterator iter = context.s2w.begin(); iter != context.s2w.end(); ++iter)
    {
      Word s2w = *iter;
      weight += swcAttrs.GetOrUpdateWeight(update, correct, s2wc, s2w, context.s2c->cat, action);
    }
  }

  if (context.s3p.size() > 0)
  {
    assert(context.s3c != 0);
    assert(context.s3w.size() == context.s3p.size());
    // s3wc
    for (std::vector<Word>::const_iterator iter = context.s3w.begin(); iter != context.s3w.end(); ++iter)
    {
      Word s3w = *iter;
      weight += swcAttrs.GetOrUpdateWeight(update, correct, s3wc, s3w, context.s3c->cat, action);
    }
    // s3pc
    for (std::vector<Word>::const_iterator iter = context.s3p.begin(); iter != context.s3p.end(); ++iter)
    {
      Word s3p = *iter;
      weight += spcAttrs.GetOrUpdateWeight(update, correct, s3pc, s3p, context.s3c->cat, action);
    }
  }

  // 2
  if (context.q0w != 0)
  {
    assert(context.q0p != 0);
    weight += qwpAttrs.GetOrUpdateWeight(update, correct, q0wp, *context.q0w, *context.q0p, action);
  }
  if (context.q1w != 0)
  {
    assert(context.q1p != 0);
    weight += qwpAttrs.GetOrUpdateWeight(update, correct, q1wp, *context.q1w, *context.q1p, action);
  }
  if (context.q2w != 0)
  {
    assert(context.q2p != 0);
    weight += qwpAttrs.GetOrUpdateWeight(update, correct, q2wp, *context.q2w, *context.q2p, action);
  }
  if (context.q3w != 0)
  {
    assert(context.q3p != 0);
    weight += qwpAttrs.GetOrUpdateWeight(update, correct, q3wp, *context.q3w, *context.q3p, action);
  }

  // 3
  if (context.s0lc != 0)
  {
    assert(context.s0rc != 0);
    for (std::vector<Word>::const_iterator iter = context.s0lp.begin(); iter != context.s0lp.end(); ++iter)
    {
      Word s0lp = *iter;
      weight += slurpcAttrs.GetOrUpdateWeight(update, correct, s0lpc, s0lp, context.s0lc, action);
    }
    for (std::vector<Word>::const_iterator iter = context.s0lw.begin(); iter != context.s0lw.end(); ++iter)
    {
      Word s0lw = *iter;
      weight += slurwcAttrs.GetOrUpdateWeight(update, correct, s0lwc, s0lw, context.s0lc, action);
    }
  }

  if (context.s0rc != 0)
  {
    assert(context.s0lc != 0);
    for (std::vector<Word>::const_iterator iter = context.s0rp.begin(); iter != context.s0rp.end(); ++iter)
    {
      Word s0rp = *iter;
      weight += slurpcAttrs.GetOrUpdateWeight(update, correct, s0rpc, s0rp, context.s0rc, action);
    }
    for (std::vector<Word>::const_iterator iter = context.s0rw.begin(); iter != context.s0rw.end(); ++iter)
    {
      Word s0rw = *iter;
      weight += slurwcAttrs.GetOrUpdateWeight(update, correct, s0rwc, s0rw, context.s0rc, action);
    }
  }

  if (context.s0uc != 0)
  {
    for (std::vector<Word>::const_iterator iter = context.s0up.begin(); iter != context.s0up.end(); ++iter)
    {
      Word s0up = *iter;
      weight += slurpcAttrs.GetOrUpdateWeight(update, correct, s0upc, s0up, context.s0uc, action);
    }
    for (std::vector<Word>::const_iterator iter = context.s0uw.begin(); iter != context.s0uw.end(); ++iter)
    {
      Word s0uw = *iter;
      weight += slurwcAttrs.GetOrUpdateWeight(update, correct, s0uwc, s0uw, context.s0uc, action);
    }
  }

  if (context.s1lc != 0)
  {
    assert(context.s1rc !=0);
    for (std::vector<Word>::const_iterator iter = context.s1lp.begin(); iter != context.s1lp.end(); ++iter)
    {
      Word s1lp = *iter;
      weight += slurpcAttrs.GetOrUpdateWeight(update, correct, s1lpc, s1lp, context.s1lc, action);
    }
    for (std::vector<Word>::const_iterator iter = context.s1lw.begin(); iter != context.s1lw.end(); ++iter)
    {
      Word s1lw = *iter;
      weight += slurwcAttrs.GetOrUpdateWeight(update, correct, s1lwc, s1lw, context.s1lc, action);
    }
  }

  // 3
  if (context.s1rc != 0)
  {
    assert(context.s1lc != 0);
    for (std::vector<Word>::const_iterator iter = context.s1rp.begin(); iter != context.s1rp.end(); ++iter)
    {
      Word s1rp = *iter;
      weight += slurpcAttrs.GetOrUpdateWeight(update, correct, s1rpc, s1rp, context.s1rc, action);
    }
    for (std::vector<Word>::const_iterator iter = context.s1rw.begin(); iter != context.s1rw.end(); ++iter)
    {
      Word s1rw = *iter;
      weight += slurwcAttrs.GetOrUpdateWeight(update, correct, s1rwc, s1rw, context.s1rc, action);
    }
  }

  if (context.s1uc != 0)
  {
    for (std::vector<Word>::const_iterator iter = context.s1up.begin(); iter != context.s1up.end(); ++iter)
    {
      Word s1up = *iter;
      weight += slurpcAttrs.GetOrUpdateWeight(update, correct, s1upc, s1up, context.s1uc, action);
    }
    for (std::vector<Word>::const_iterator iter = context.s1uw.begin(); iter != context.s1uw.end(); ++iter)
    {
      Word s1uw = *iter;
      weight += slurwcAttrs.GetOrUpdateWeight(update, correct, s1uwc, s1uw, context.s1uc, action);
    }
  }

  // 4
  if (context.s0c != 0 && context.s1c != 0)
  {
    std::vector<Word>::const_iterator iters0w;
    std::vector<Word>::const_iterator iters1w;
    for (iters0w = context.s0w.begin(); iters0w != context.s0w.end(); ++iters0w)
    {
      Word s0w = *iters0w;
      // s0wcs1wc
      for (std::vector<Word>::const_iterator iters1w = context.s1w.begin(); iters1w != context.s1w.end(); ++iters1w)
      {
        Word s1w = *iters1w;
        weight += s0wcs1wcAttrs.GetOrUpdateWeight(update, correct, s0wcs1wc, s0w, context.s0c->cat, s1w, context.s1c->cat, action);
      }
      // s0ws1c
      weight += s0ws1cAttrs.GetOrUpdateWeight(update, correct, s0ws1c, s0w, context.s1c->cat, action);
    }

    // s0cs1w
    for (std::vector<Word>::const_iterator iter = context.s1w.begin(); iter != context.s1w.end(); ++iter)
    {
      Word s1w = *iter;
      weight += s0cs1wAttrs.GetOrUpdateWeight(update, correct, s0cs1w, context.s0c->cat, s1w, action);
    }
    // s0cs1c
    weight += s0cs1cAttrs.GetOrUpdateWeight(update, correct, s0cs1c, context.s0c->cat, context.s1c->cat, action);
  }

  if (context.s0c != 0 && context.q0w != 0)
  {
    for (std::vector<Word>::const_iterator iterw = context.s0w.begin(); iterw != context.s0w.end(); ++iterw)
    {
      Word s0w = *iterw;
      assert(context.q0p != 0);
      weight += swcqwpAttrs.GetOrUpdateWeight(update, correct, s0wcq0wp, s0w, context.s0c->cat, *context.q0w, *context.q0p, action);
      weight += swcqpAttrs.GetOrUpdateWeight(update, correct, s0wcq0p, s0w, context.s0c->cat, *context.q0p, action);
    }

    assert(context.q0p != 0);
    weight += scqwpAttrs.GetOrUpdateWeight(update, correct, s0cq0wp, context.s0c->cat, *context.q0w, *context.q0p, action);
    weight += scqpAttrs.GetOrUpdateWeight(update, correct, s0cq0p, context.s0c->cat, *context.q0p, action);
  }

  if (context.s1c != 0 && context.q0w != 0)
  {
    for (std::vector<Word>::const_iterator iter = context.s1w.begin(); iter != context.s1w.end(); ++iter)
    {
      Word s1w = *iter;
      assert(context.q0p != 0);
      weight += swcqwpAttrs.GetOrUpdateWeight(update, correct, s1wcq0wp, s1w, context.s1c->cat, *context.q0w, *context.q0p, action);
      weight += swcqpAttrs.GetOrUpdateWeight(update, correct, s1wcq0p, s1w, context.s1c->cat, *context.q0p, action);
    }
    assert(context.q0p != 0);
    weight += scqwpAttrs.GetOrUpdateWeight(update, correct, s1cq0wp, context.s1c->cat, *context.q0w, *context.q0p, action);
    weight += scqpAttrs.GetOrUpdateWeight(update, correct, s1cq0p, context.s1c->cat, *context.q0p, action);
  }

  // 5

  // s0ps1pq0p
  if (context.s0p.size() > 0 && context.s1p.size() > 0 && context.q0p != 0)
  {
    assert(context.s0c != 0 && context.s1c != 0 && context.q0w != 0);
    for (std::vector<Word>::const_iterator iters0p = context.s0p.begin(); iters0p != context.s0p.end(); ++iters0p)
    {
      Word s0p = *iters0p;
      for (std::vector<Word>::const_iterator iters1p = context.s1p.begin(); iters1p != context.s1p.end(); ++iters1p)
      {
        Word s1p = *iters1p;
        weight += s0psqpqspAttrs.GetOrUpdateWeight(update, correct, s0ps1pq0p, s0p, s1p, *context.q0p, action);
      }
    }
  }

  // s0pq0pq1p
  if (context.s0p.size() > 0 && context.q0p != 0 && context.q1p != 0)
  {
    assert(context.s0c != 0);
    for (std::vector<Word>::const_iterator iter = context.s0p.begin(); iter != context.s0p.end(); ++iter)
    {
      Word s0p = *iter;
      weight += s0psqpqspAttrs.GetOrUpdateWeight(update, correct, s0pq0pq1p, s0p, *context.q0p, *context.q1p, action);
    }
  }

  if (context.s0p.size() > 0 && context.s1p.size() > 0 && context.s2p.size() > 0)
  {
    // s0ps1ps2p
    for (std::vector<Word>::const_iterator iters0p = context.s0p.begin(); iters0p != context.s0p.end(); ++iters0p)
    {
      Word s0p = *iters0p;
      for (std::vector<Word>::const_iterator iters1p = context.s1p.begin(); iters1p != context.s1p.end(); ++iters1p)
      {
        Word s1p = *iters1p;
        for (std::vector<Word>::const_iterator iters2p = context.s2p.begin(); iters2p != context.s2p.end(); ++iters2p)
        {
          Word s2p = *iters2p;
          weight += s0psqpqspAttrs.GetOrUpdateWeight(update, correct, s0ps1ps2p, s0p, s1p, s2p, action);
        }
      }
    }
  }

  if (context.s0c != 0)
  {
    for (std::vector<Word>::const_iterator iterw = context.s0w.begin(); iterw != context.s0w.end(); ++iterw)
    {
      Word s0w = *iterw;
      // s0wcs1cq0p
      if (context.s1c != 0 && context.q0p != 0)
        weight += s0wcs1cq0pAttrs.GetOrUpdateWeight(update, correct, s0wcs1cq0p, s0w, context.s0c->cat, context.s1c->cat, *context.q0p, action);
      // s0wcq0pq1p
      if (context.q0p != 0 && context.q1p != 0)
        weight += s0wcq0pq1pAttrs.GetOrUpdateWeight(update, correct, s0wcq0pq1p, s0w, context.s0c->cat, *context.q0p, *context.q1p, action);
      // s0wcs1cs2c
      if (context.s1c != 0 && context.s2c != 0)
        weight += s0wcs1cs2cAttrs.GetOrUpdateWeight(update, correct, s0wcs1cs2c, s0w, context.s0c->cat, context.s1c->cat, context.s2c->cat, action);
    }

    // s0cs1wcq0p
    if (context.s1c != 0 && context.q0p != 0)
    {
      for (std::vector<Word>::const_iterator iterw = context.s1w.begin(); iterw != context.s1w.end(); ++iterw)
      {
        Word s1w = *iterw;
        weight += s0cs1wcq0pAttrs.GetOrUpdateWeight(update, correct, s0cs1wcq0p, context.s0c->cat, s1w, context.s1c->cat, *context.q0p, action);
      }
    }

    if (context.s1c != 0 && context.q0w != 0)
    {
      // s0cs1cq0wp
      weight += s0cs1cq0wpAttrs.GetOrUpdateWeight(update, correct, s0cs1cq0wp, context.s0c->cat, context.s1c->cat, *context.q0w, *context.q0p, action);
      // s0cs1cq0p
      weight += s0cs1cq0pAttrs.GetOrUpdateWeight(update, correct, s0cs1cq0p, context.s0c->cat, context.s1c->cat, *context.q0p, action);
    }

    if (context.q0w != 0 && context.q1w != 0)
    {
      assert(context.q0p != 0);
      assert(context.q1p != 0);
      weight += s0cq0wpq1pAttrs.GetOrUpdateWeight(update, correct, s0cq0wpq1p, context.s0c->cat, *context.q0w, *context.q0p, *context.q1p, action);
      weight += s0cq0pq1wpAttrs.GetOrUpdateWeight(update, correct, s0cq0pq1wp, context.s0c->cat, *context.q0p, *context.q1w, *context.q1p, action);
      weight += s0cq0pq1pAttrs.GetOrUpdateWeight(update, correct, s0cq0pq1p, context.s0c->cat, *context.q0p, *context.q1p, action);
    }

    if (context.s1c != 0 && context.s2c != 0)
    {
      for (std::vector<Word>::const_iterator iters1w = context.s1w.begin(); iters1w != context.s1w.end(); ++iters1w)
      {
        Word s1w = *iters1w;
        weight += s0cs1wcs2cAttrs.GetOrUpdateWeight(update, correct, s0cs1wcs2c, context.s0c->cat, s1w, context.s1c->cat, context.s2c->cat, action);
      }
      for (std::vector<Word>::const_iterator iters2w = context.s2w.begin(); iters2w != context.s2w.end(); ++iters2w)
      {
        Word s2w = *iters2w;
        weight += s0cs1cs2wcAttrs.GetOrUpdateWeight(update, correct, s0cs1cs2wc, context.s0c->cat, context.s1c->cat, s2w, context.s2c->cat, action);
      }
      weight += s0cs1cs2cAttrs.GetOrUpdateWeight(update, correct, s0cs1cs2c, context.s0c->cat, context.s1c->cat, context.s2c->cat, action);
    }
  } // end s0c

  // 6
  if (context.s0c != 0 && context.s0lc != 0 && context.s0rc != 0)
  {
    // s0cs0lcs0rc
    weight += s01cs01lcs01rcAttrs.GetOrUpdateWeight(update, correct, s0cs0lcs0rc, context.s0c->cat, context.s0lc, context.s0rc, action);
    // s0cs0rcq0p
    if (context.q0p != 0)
      weight += s0cs0rcq0pAttrs.GetOrUpdateWeight(update, correct, s0cs0rcq0p, context.s0c->cat, context.s0rc, *context.q0p, action);
    if (context.q0w != 0)
      // s0cs0rcq0w
      weight += s0cS0lrcq0s1wAttrs.GetOrUpdateWeight(update, correct, s0cs0rcq0w, context.s0c->cat, context.s0rc, *context.q0w, action);
  }

  // s0cs0lcs1c
  if (context.s0c != 0 && context.s0lc != 0 && context.s1c != 0)
  {
    assert(context.s0rc != 0);
    weight += s0cs0ls1cs1s1rcAttrs.GetOrUpdateWeight(update, correct, s0cs0lcs1c, context.s0c->cat, context.s0lc, context.s1c->cat, action);
  }

  // s0cs1cs1rc
  if (context.s0c != 0 && context.s1c != 0 && context.s1rc != 0)
  {
    assert(context.s1lc != 0);
    weight += s0cs0ls1cs1s1rcAttrs.GetOrUpdateWeight(update, correct, s0cs1cs1rc, context.s0c->cat, context.s1c->cat, context.s1rc, action);
  }

//  // s1cs1lcs1rc
//  if (context.s1lc != 0 && context.s1rc != 0)
//  {
//    assert(context.s1c != 0);
//    weight += s01cs01lcs01rcAttrs.GetOrUpdateWeight(update, correct, s1cs1lcs1rc, context.s1c->cat, context.s1lc, context.s1rc, action);
//  }

  // s0cs0lcs1w
  if (context.s0lc != 0)
  {
    assert(context.s0rc != 0 && context.s0c != 0);
    for (std::vector<Word>::const_iterator iterw = context.s1w.begin(); iterw != context.s1w.end(); ++iterw)
    {
      Word s1w = *iterw;
      weight += s0cS0lrcq0s1wAttrs.GetOrUpdateWeight(update, correct, s0cs0lcs1w, context.s0c->cat, context.s0lc, s1w, action);
    }
  }

  // s0ws1cs1rc
  if (context.s0w.size() > 0 && context.s1c != 0 && context.s1rc != 0)
  {
    assert(context.s1lc != 0);
    for (std::vector<Word>::const_iterator iterw = context.s0w.begin(); iterw != context.s0w.end(); ++iterw)
    {
      Word s0w = *iterw;
      weight += s0ws1cs1rcAttrs.GetOrUpdateWeight(update, correct, s0ws1cs1rc, s0w, context.s1c->cat, context.s1rc, action);
    }
  }

  return weight;
}


const Cat *ShiftReduceFeature::GetCat(std::string tmp)
{
  const Cat *cat = cats.markedup[tmp];
  if(!cat)
  {
    try
    {
      cat = cats.canonize(tmp.c_str());
    }
    catch(NLP::Exception e)
    {
      throw NLP::IOException("attempting to parse category failed when loading model: ShiftReduceFeature::GetCat()");
    }
  }
  return cat;
}


bool ShiftReduceFeature::CheckWeights(size_t total)
{
  assert(swpAttrs.size() == m_featureTypeCount[0]);
  assert(scAttrs.size() == m_featureTypeCount[1]);
  assert(spcAttrs.size() == m_featureTypeCount[2]);
  assert(swcAttrs.size() == m_featureTypeCount[3]);
  assert(qwpAttrs.size() == m_featureTypeCount[4]);
  assert(slurpcAttrs.size() == m_featureTypeCount[5]);
  assert(slurwcAttrs.size() == m_featureTypeCount[6]);
  assert(s0wcs1wcAttrs.size() == m_featureTypeCount[7]);
  assert(s0cs1wAttrs.size() == m_featureTypeCount[8]);
  assert(s0ws1cAttrs.size() == m_featureTypeCount[9]);
  assert(s0cs1cAttrs.size() == m_featureTypeCount[10]);
  assert(swcqwpAttrs.size() == m_featureTypeCount[11]);
  assert(scqwpAttrs.size() == m_featureTypeCount[12]);
  assert(swcqpAttrs.size() == m_featureTypeCount[13]);
  assert(scqpAttrs.size() == m_featureTypeCount[14]);
  assert(s0wcs1cq0pAttrs.size() == m_featureTypeCount[15]);
  assert(s0cs1wcq0pAttrs.size() == m_featureTypeCount[16]);
  assert(s0cs1cq0wpAttrs.size() == m_featureTypeCount[17]);
  assert(s0cs1cq0pAttrs.size() == m_featureTypeCount[18]);
  assert(s0wcq0pq1pAttrs.size() == m_featureTypeCount[19]);
  assert(s0cq0wpq1pAttrs.size() == m_featureTypeCount[20]);
  assert(s0cq0pq1wpAttrs.size() == m_featureTypeCount[21]);
  assert(s0cq0pq1pAttrs.size() == m_featureTypeCount[22]);
  assert(s0wcs1cs2cAttrs.size() == m_featureTypeCount[23]);
  assert(s0cs1wcs2cAttrs.size() == m_featureTypeCount[24]);
  assert(s0cs1cs2wcAttrs.size() == m_featureTypeCount[25]);
  assert(s0cs1cs2cAttrs.size() == m_featureTypeCount[26]);
  assert(s0psqpqspAttrs.size() == m_featureTypeCount[27]);
  assert(s01cs01lcs01rcAttrs.size() == m_featureTypeCount[28]);
  assert(s0cs0rcq0pAttrs.size() == m_featureTypeCount[29]);
  assert(s0cS0lrcq0s1wAttrs.size() == m_featureTypeCount[30]);
  assert(s0cs0ls1cs1s1rcAttrs.size() == m_featureTypeCount[31]);
  assert(s0ws1cs1rcAttrs.size() == m_featureTypeCount[32]);

  size_t _total = 0;
  for (size_t i = 0; i < 33; ++i)
    _total += m_featureTypeCount[i];
  assert(_total == total);
  cerr << "done feature loading check" << endl;
  return true;
}

void ShiftReduceFeature::SetWeights(istream &in, size_t type)
{

  size_t actionId;
  string acatStr;
  double weightT;
  double weight;

  switch(type)
  {

  case 1:
  case 2:
  {
    string word;
    string tag;

    in >> word >> tag >> actionId >> acatStr >> weightT >> weight;

    Word wordValue = lexicon[word];
    Word tagValue = lexicon[tag];

    const Cat *acat = GetCat(acatStr);
    ShiftReduceAction action(actionId, acat);

    SwpValue swpValue(type, wordValue, tagValue, action);
    swpAttrs.SetFeature(swpValue, weightT, weight);

    ++m_featureTypeCount[0];
    break;
  }
  case 3:
  case 4:
  {
    string catStr;

    in >> catStr >> actionId >> acatStr >> weightT >> weight;

    const Cat *cat = GetCat(catStr);

    const Cat *acat = GetCat(acatStr);
    ShiftReduceAction action(actionId, acat);

    ScValue scValue(type, cat, action);
    scAttrs.SetFeature(scValue, weightT, weight);

    ++m_featureTypeCount[1];
    break;
  }

  case 5:
  case 6:
  case 7:
  case 8:
  {
    string tag;
    string catStr;

    in >> tag >> catStr >> actionId >> acatStr >> weightT >> weight;

    Word tagValue = lexicon[tag];
    const Cat *cat = GetCat(catStr);

    const Cat *acat = GetCat(acatStr);
    ShiftReduceAction action(actionId, acat);

    SpcValue spcValue(type, tagValue, cat, action);
    spcAttrs.SetFeature(spcValue, weightT, weight);

    ++m_featureTypeCount[2];
    break;
  }
  case 9:
  case 10:
  case 11:
  case 12:
  {
    string word;
    string catStr;

    in >> word >> catStr >> actionId >> acatStr >> weightT >> weight;

    Word wordValue = lexicon[word];
    const Cat *cat = GetCat(catStr);

    const Cat *acat = GetCat(acatStr);
    ShiftReduceAction action(actionId, acat);

    SwcValue swcValue(type, wordValue, cat, action);
    swcAttrs.SetFeature(swcValue, weightT, weight);

    ++m_featureTypeCount[3];
    break;
  }

  case 13:
  case 14:
  case 15:
  case 16:
  {
    string word;
    string tag;

    in >> word >> tag >> actionId >> acatStr >> weightT >> weight;

    Word wordValue = lexicon[word];
    Word tagValue = lexicon[tag];

    const Cat *acat = GetCat(acatStr);
    ShiftReduceAction action(actionId, acat);

    QwpValue qwpValue(type, wordValue, tagValue, action);
    qwpAttrs.SetFeature(qwpValue, weightT, weight);

    ++m_featureTypeCount[4];
    break;
  }

  case 17:
  case 19:
  case 21:
  case 23:
  case 25:
  case 27:
  {
    string tag;
    string catStr;

    in >> tag >> catStr >> actionId >> acatStr >> weightT >> weight;

    Word tagValue = lexicon[tag];
    const Cat *cat = GetCat(catStr);

    const Cat *acat = GetCat(acatStr);
    ShiftReduceAction action(actionId, acat);

    SlurpcValue slurpcValue(type, tagValue, cat, action);
    slurpcAttrs.SetFeature(slurpcValue, weightT, weight);

    ++m_featureTypeCount[5];
    break;
  }

  case 18:
  case 20:
  case 22:
  case 24:
  case 26:
  case 28:
  {
    string word;
    string catStr;

    in >> word >> catStr >> actionId >> acatStr >> weightT >> weight;

    Word wordValue = lexicon[word];
    const Cat *cat = GetCat(catStr);

    const Cat *acat = GetCat(acatStr);
    ShiftReduceAction action(actionId, acat);

    SlurwcValue slurwcValue(type, wordValue, cat, action);
    slurwcAttrs.SetFeature(slurwcValue, weightT, weight);

    ++m_featureTypeCount[6];
    break;
  }

  case 29:
  {
    string s0w;
    string s0c;
    string s1w;
    string s1c;

    in >> s0w >> s0c >> s1w >> s1c >> actionId >> acatStr >> weightT >> weight;

    Word s0w_ = lexicon[s0w];
    const Cat *s0c_ = GetCat(s0c);
    Word s1w_ = lexicon[s1w];
    const Cat *s1c_ = GetCat(s1c);

    const Cat *acat = GetCat(acatStr);
    ShiftReduceAction action(actionId, acat);

    S0wcS1wcValue s0wcS1wcValue(type, s0w_, s0c_, s1w_, s1c_, action);
    s0wcs1wcAttrs.SetFeature(s0wcS1wcValue, weightT, weight);

    ++m_featureTypeCount[7];
    break;
  }

  case 30:
  {
    string s0c;
    string s1w;

    in >> s0c >> s1w >> actionId >> acatStr >> weightT >> weight;

    const Cat *s0c_ = GetCat(s0c);
    Word s1w_ = lexicon[s1w];

    const Cat *acat = GetCat(acatStr);
    ShiftReduceAction action(actionId, acat);

    S0cS1wValue s0cS1wValue(type, s0c_, s1w_, action);
    s0cs1wAttrs.SetFeature(s0cS1wValue, weightT, weight);

    ++m_featureTypeCount[8];
    break;
  }

  case 31:
  {
    string s0w;
    string s1c;

    in >> s0w >> s1c >> actionId >> acatStr >> weightT >> weight;

    Word s0w_ = lexicon[s0w];
    const Cat *s1c_ = GetCat(s1c);

    const Cat *acat = GetCat(acatStr);
    ShiftReduceAction action(actionId, acat);

    S0wS1cValue s0wS1cValue(type, s0w_, s1c_, action);
    s0ws1cAttrs.SetFeature(s0wS1cValue, weightT, weight);

    ++m_featureTypeCount[9];
    break;
  }

  case 32:
  {
    string s0c;
    string s1c;

    in >> s0c >> s1c >> actionId >> acatStr >> weightT >> weight;

    const Cat *s0c_ = GetCat(s0c);
    const Cat *s1c_ = GetCat(s1c);

    const Cat *acat = GetCat(acatStr);
    ShiftReduceAction action(actionId, acat);

    S0cS1cValue s0cS1cValue(type, s0c_, s1c_, action);
    s0cs1cAttrs.SetFeature(s0cS1cValue, weightT, weight);

    ++m_featureTypeCount[10];
    break;
  }

  case 33:
  case 37:
  {
    string sw;
    string sc;
    string qw;
    string qp;

    in >> sw >> sc >> qw >> qp >> actionId >> acatStr >> weightT >> weight;

    Word sw_ = lexicon[sw];
    const Cat *sc_ = GetCat(sc);
    Word qw_ = lexicon[qw];
    Word qp_ = lexicon[qp];

    const Cat *acat = GetCat(acatStr);
    ShiftReduceAction action(actionId, acat);
    SwcQwpValue swcQwpValue(type, sw_, sc_, qw_, qp_, action);
    swcqwpAttrs.SetFeature(swcQwpValue, weightT, weight);

    ++m_featureTypeCount[11];
    break;
  }


  case 34:
  case 38:
  {
    string sc;
    string qw;
    string qp;

    in >> sc >> qw >> qp >> actionId >> acatStr >> weightT >> weight;

    const Cat *sc_ = GetCat(sc);
    Word qw_ = lexicon[qw];
    Word qp_ = lexicon[qp];

    const Cat *acat = GetCat(acatStr);
    ShiftReduceAction action(actionId, acat);

    ScQwpValue scQwpValue(type, sc_, qw_, qp_, action);
    scqwpAttrs.SetFeature(scQwpValue, weightT, weight);

    ++m_featureTypeCount[12];
    break;
  }

  case 35:
  case 39:
  {
    string sw;
    string sc;
    string qp;

    in >> sw >> sc >> qp >> actionId >> acatStr >> weightT >> weight;

    Word sw_ = lexicon[sw];
    const Cat *sc_ = GetCat(sc);
    Word qp_ = lexicon[qp];

    const Cat *acat = GetCat(acatStr);
    ShiftReduceAction action(actionId, acat);

    SwcQpValue swcQpValue(type, sw_, sc_, qp_, action);
    swcqpAttrs.SetFeature(swcQpValue, weightT, weight);

    ++m_featureTypeCount[13];
    break;
  }

  case 36:
  case 40:
  {
    string sc;
    string qp;

    in >> sc >> qp >> actionId >> acatStr >> weightT >> weight;

    const Cat *sc_ = GetCat(sc);
    Word qp_ = lexicon[qp];

    const Cat *acat = GetCat(acatStr);
    ShiftReduceAction action(actionId, acat);

    ScQpValue scQpValue(type, sc_, qp_, action);
    scqpAttrs.SetFeature(scQpValue, weightT, weight);

    ++m_featureTypeCount[14];
    break;
  }

  // 5
  case 41:
  {
    string s0w;
    string s0c;
    string s1c;
    string q0p;

    in >> s0w >> s0c >> s1c >> q0p >> actionId >> acatStr >> weightT >> weight;

    Word s0w_ = lexicon[s0w];
    const Cat *s0c_ = GetCat(s0c);
    const Cat *s1c_ = GetCat(s1c);
    Word q0p_ = lexicon[q0p];

    const Cat *acat = GetCat(acatStr);
    ShiftReduceAction action(actionId, acat);

    S0wcS1cQ0pValue s0wcS1cQ0pValue(type, s0w_, s0c_, s1c_, q0p_, action);
    s0wcs1cq0pAttrs.SetFeature(s0wcS1cQ0pValue, weightT, weight);

    ++m_featureTypeCount[15];
    break;
  }
  case 42:
  {
    string s0c;
    string s1w;
    string s1c;
    string q0p;

    in >> s0c >> s1w >> s1c >> q0p >> actionId >> acatStr >> weightT >> weight;

    const Cat *s0c_ = GetCat(s0c);
    Word s1w_ = lexicon[s1w];
    const Cat *s1c_ = GetCat(s1c);
    Word q0p_ = lexicon[q0p];

    const Cat *acat = GetCat(acatStr);
    ShiftReduceAction action(actionId, acat);

    S0cS1wcQ0pValue s0cS1wcQ0pValue(type, s0c_, s1w_ , s1c_, q0p_, action);
    s0cs1wcq0pAttrs.SetFeature(s0cS1wcQ0pValue, weightT, weight);

    ++m_featureTypeCount[16];
    break;
  }
  case 43:
  {
    string s0c;
    string s1c;
    string q0w;
    string q0p;

    in >> s0c >> s1c >> q0w >> q0p >> actionId >> acatStr >> weightT >> weight;

    const Cat *s0c_ = GetCat(s0c);
    const Cat *s1c_ = GetCat(s1c);
    Word q0w_ = lexicon[q0w];
    Word q0p_ = lexicon[q0p];

    const Cat *acat = GetCat(acatStr);
    ShiftReduceAction action(actionId, acat);
    S0cS1cQ0wpValue s0cS1cQ0wpValue(type, s0c_, s1c_, q0w_, q0p_, action);
    s0cs1cq0wpAttrs.SetFeature(s0cS1cQ0wpValue, weightT, weight);

    ++m_featureTypeCount[17];
    break;
  }
  case 44:
  {
    string s0c;
    string s1c;
    string q0p;

    in >> s0c >> s1c >> q0p >> actionId >> acatStr >> weightT >> weight;

    const Cat *s0c_ = GetCat(s0c);
    const Cat *s1c_ = GetCat(s1c);
    Word q0p_ = lexicon[q0p];

    const Cat *acat = GetCat(acatStr);
    ShiftReduceAction action(actionId, acat);

    S0cS1cQ0pValue s0cS1cQ0pValue(type, s0c_, s1c_, q0p_, action);
    s0cs1cq0pAttrs.SetFeature(s0cS1cQ0pValue, weightT, weight);

    ++m_featureTypeCount[18];
    break;
  }
  case 48:
  {
    string s0w;
    string s0c;
    string q0p;
    string q1p;

    in >> s0w >> s0c >> q0p >> q1p >> actionId >> acatStr >> weightT >> weight;

    Word s0w_ = lexicon[s0w];
    const Cat *s0c_ = GetCat(s0c);
    Word q0p_ = lexicon[q0p];
    Word q1p_ = lexicon[q1p];

    const Cat *acat = GetCat(acatStr);
    ShiftReduceAction action(actionId, acat);

    S0wcQ0pQ1pValue s0wcQ0pQ1pValue(type, s0w_, s0c_, q0p_, q1p_, action);
    s0wcq0pq1pAttrs.SetFeature(s0wcQ0pQ1pValue, weightT, weight);

    ++m_featureTypeCount[19];
    break;
  }
  case 49:
  {
    string s0c;
    string q0w;
    string q0p;
    string q1p;

    in >> s0c >> q0w >> q0p >> q1p >> actionId >> acatStr >> weightT >> weight;

    const Cat *s0c_ = GetCat(s0c);
    Word q0w_ = lexicon[q0w];
    Word q0p_ = lexicon[q0p];
    Word q1p_ = lexicon[q1p];

    const Cat *acat = GetCat(acatStr);
    ShiftReduceAction action(actionId, acat);

    S0cQ0wpQ1pValue s0cQ0wpQ1pValue(type, s0c_, q0w_, q0p_, q1p_, action);
    s0cq0wpq1pAttrs.SetFeature(s0cQ0wpQ1pValue, weightT, weight);

    ++m_featureTypeCount[20];
    break;
  }
  case 50:
  {
    string s0c;
    string q0p;
    string q1w;
    string q1p;

    in >> s0c >> q0p >> q1w >> q1p >> actionId >> acatStr >> weightT >> weight;

    const Cat *s0c_ = GetCat(s0c);
    Word q0p_ = lexicon[q0p];
    Word q1w_ = lexicon[q1w];
    Word q1p_ = lexicon[q1p];

    const Cat *acat = GetCat(acatStr);
    ShiftReduceAction action(actionId, acat);

    S0cQ0pQ1wpValue s0cQ0pQ1wpValue(type, s0c_, q0p_, q1w_, q1p_, action);
    s0cq0pq1wpAttrs.SetFeature(s0cQ0pQ1wpValue, weightT, weight);

    ++m_featureTypeCount[21];
    break;
  }
  case 51:
  {
    string s0c;
    string q0p;
    string q1p;

    in >> s0c >> q0p >> q1p >> actionId >> acatStr >> weightT >> weight;

    const Cat *s0c_ = GetCat(s0c);
    Word q0p_ = lexicon[q0p];
    Word q1p_ = lexicon[q1p];

    const Cat *acat = GetCat(acatStr);
    ShiftReduceAction action(actionId, acat);

    S0cQ0pQ1pValue s0cQ0pQ1pValue(type, s0c_, q0p_, q1p_, action);
    s0cq0pq1pAttrs.SetFeature(s0cQ0pQ1pValue, weightT, weight);

    ++m_featureTypeCount[22];
    break;
  }
  case 52:
  {
    string s0w;
    string s0c;
    string s1c;
    string s2c;

    in >> s0w >> s0c >> s1c >> s2c >> actionId >> acatStr >> weightT >> weight;

    Word s0w_ = lexicon[s0w];
    const Cat *s0c_ = GetCat(s0c);
    const Cat *s1c_ = GetCat(s1c);
    const Cat *s2c_ = GetCat(s2c);

    const Cat *acat = GetCat(acatStr);
    ShiftReduceAction action(actionId, acat);
    S0wcS1cS2cValue s0wcS1cS2cValue(type, s0w_, s0c_, s1c_, s2c_, action);
    s0wcs1cs2cAttrs.SetFeature(s0wcS1cS2cValue, weightT, weight);

    ++m_featureTypeCount[23];
    break;
  }
  case 53:
  {
    string s0c;
    string s1w;
    string s1c;
    string s2c;

    in >> s0c >> s1w >> s1c >> s2c >> actionId >> acatStr >> weightT >> weight;

    const Cat *s0c_ = GetCat(s0c);
    Word s1w_ = lexicon[s1w];
    const Cat *s1c_ = GetCat(s1c);
    const Cat *s2c_ = GetCat(s2c);

    const Cat *acat = GetCat(acatStr);
    ShiftReduceAction action(actionId, acat);

    S0cS1wcS2cValue s0cS1wcS2cValue(type, s0c_, s1w_, s1c_, s2c_, action);
    s0cs1wcs2cAttrs.SetFeature(s0cS1wcS2cValue, weightT, weight);

    ++m_featureTypeCount[24];
    break;
  }
  case 54:
  {
    string s0c;
    string s1c;
    string s2w;
    string s2c;

    in >> s0c >> s1c >> s2w >> s2c >> actionId >> acatStr >> weightT >> weight;

    const Cat *s0c_ = GetCat(s0c);
    const Cat *s1c_ = GetCat(s1c);
    Word s2w_ = lexicon[s2w];
    const Cat *s2c_ = GetCat(s2c);

    const Cat *acat = GetCat(acatStr);
    ShiftReduceAction action(actionId, acat);

    S0cS1cS2wcValue s0cS1cS2wcValue(type, s0c_, s1c_, s2w_, s2c_, action);
    s0cs1cs2wcAttrs.SetFeature(s0cS1cS2wcValue, weightT, weight);

    ++m_featureTypeCount[25];
    break;
  }
  case 55:
  {
    string s0c;
    string s1c;
    string s2c;

    in >> s0c >> s1c >> s2c >> actionId >> acatStr >> weightT >> weight;

    const Cat *s0c_ = GetCat(s0c);
    const Cat *s1c_ = GetCat(s1c);
    const Cat *s2c_ = GetCat(s2c);

    const Cat *acat = GetCat(acatStr);
    ShiftReduceAction action(actionId, acat);

    S0cS1cS2cValue s0cS1cS2cValue(type, s0c_, s1c_, s2c_, action);
    s0cs1cs2cAttrs.SetFeature(s0cS1cS2cValue, weightT, weight);

    ++m_featureTypeCount[26];
    break;
  }

  case 45:
  case 46:
  case 47:
  {
    string p1;
    string p2;
    string p3;

    in >> p1 >> p2 >> p3 >> actionId >> acatStr >> weightT >> weight;

    Word p1_ = lexicon[p1];
    Word p2_ = lexicon[p2];
    Word p3_ = lexicon[p3];

    const Cat *acat = GetCat(acatStr);
    ShiftReduceAction action(actionId, acat);
    S0pSQpQSpValue s0pSQpQSpValue(type, p1_, p2_, p3_, action);
    s0psqpqspAttrs.SetFeature(s0pSQpQSpValue, weightT, weight);

    ++m_featureTypeCount[27];
    break;
  }

  // 6

  case 56:
  case 57:
  {
    string cat1;
    string cat2;
    string cat3;

    in >> cat1 >> cat2 >> cat3 >> actionId >> acatStr >> weightT >> weight;

    const Cat *cat1_ = GetCat(cat1);
    const Cat *cat2_ = GetCat(cat2);
    const Cat *cat3_ = GetCat(cat3);

    const Cat *acat = GetCat(acatStr);
    ShiftReduceAction action(actionId, acat);
    S01cS01LcS01rcValue s01cS01LcS01rcValue(type, cat1_, cat2_, cat3_, action);
    s01cs01lcs01rcAttrs.SetFeature(s01cS01LcS01rcValue, weightT, weight);

    ++m_featureTypeCount[28];
    break;
  }

  case 58:
  {
    string s0c;
    string s0rc;
    string q0p;

    in >> s0c >> s0rc >> q0p >> actionId >> acatStr >> weightT >> weight;

    const Cat *s0c_ = GetCat(s0c);
    const Cat *s0rc_ = GetCat(s0rc);
    Word q0p_ = lexicon[q0p];

    const Cat *acat = GetCat(acatStr);
    ShiftReduceAction action(actionId, acat);

    S0cS0RcQ0pValue s0cS0RcQ0pValue(type, s0c_, s0rc_, q0p_, action);
    s0cs0rcq0pAttrs.SetFeature(s0cS0RcQ0pValue, weightT, weight);

    ++m_featureTypeCount[29];
    break;
  }

  case 59:
  case 60:
  {
    string s0c;
    string s0lrc;
    string q0s1w;

    in >> s0c >> s0lrc >> q0s1w >> actionId >> acatStr >> weightT >> weight;

    const Cat *s0c_ = GetCat(s0c);
    const Cat *s0lrc_ = GetCat(s0lrc);
    Word q0s1w_ = lexicon[q0s1w];

    const Cat *acat = GetCat(acatStr);
    ShiftReduceAction action(actionId, acat);
    S0cS0LRcQ0S1wValue s0cS0LRcQ0S1wValue(type, s0c_, s0lrc_, q0s1w_, action);
    s0cS0lrcq0s1wAttrs.SetFeature(s0cS0LRcQ0S1wValue, weightT, weight);

    ++m_featureTypeCount[30];
    break;
  }

  case 61:
  case 62:
  {
    string cat0;
    string cat1;
    string cat2;

    in >> cat0 >> cat1 >> cat2 >> actionId >> acatStr >> weightT >> weight;

    const Cat *cat0_ = GetCat(cat0);
    const Cat *cat1_ = GetCat(cat1);
    const Cat *cat2_ = GetCat(cat2);

    const Cat *acat = GetCat(acatStr);
    ShiftReduceAction action(actionId, acat);
    S0cS0LS1cS1S1RcValue s0cS0LS1cS1S1RcValue(type, cat0_, cat1_, cat2_, action);
    s0cs0ls1cs1s1rcAttrs.SetFeature(s0cS0LS1cS1S1RcValue, weightT, weight);

    ++m_featureTypeCount[31];
    break;
  }

  case 63:
  {
    string s0w;
    string s1c;
    string s1rc;

    in >> s0w >> s1c >> s1rc >> actionId >> acatStr >> weightT >> weight;

    Word s0w_ = lexicon[s0w];
    const Cat *s1c_ = GetCat(s1c);
    const Cat *s1rc_ = GetCat(s1rc);

    const Cat *acat = GetCat(acatStr);
    ShiftReduceAction action(actionId, acat);

    S0wS1cS1RcValue s0wS1cS1RcValue(type, s0w_, s1c_, s1rc_, action);
    s0ws1cs1rcAttrs.SetFeature(s0wS1cS1RcValue, weightT, weight);

    ++m_featureTypeCount[32];
    break;
  }


  default:
    throw NLP::IOException("unexpected feature type in feature loading, ShiftReduceFeature::SetWeight()");
  }

}



bool ShiftReduceFeature::SaveWeights(std::ofstream &out)
{

  std::cerr << "in ShiftReduceFeature::SaveWeights()" << std::endl;

  // 1
  swpAttrs.SaveWeights(out);
  scAttrs.SaveWeights(out);
  spcAttrs.SaveWeights(out);
  swcAttrs.SaveWeights(out);

  // 2
  qwpAttrs.SaveWeights(out);

  // 3
  slurpcAttrs.SaveWeights(out);
  slurwcAttrs.SaveWeights(out);

  // 4
  s0wcs1wcAttrs.SaveWeights(out);
  s0cs1wAttrs.SaveWeights(out);
  s0ws1cAttrs.SaveWeights(out);
  s0cs1cAttrs.SaveWeights(out);

  swcqwpAttrs.SaveWeights(out);
  scqwpAttrs.SaveWeights(out);
  swcqpAttrs.SaveWeights(out);
  scqpAttrs.SaveWeights(out);

  // 5
  s0wcs1cq0pAttrs.SaveWeights(out);
  s0cs1wcq0pAttrs.SaveWeights(out);
  s0cs1cq0wpAttrs.SaveWeights(out);
  s0cs1cq0pAttrs.SaveWeights(out);
  s0psqpqspAttrs.SaveWeights(out);
  s0wcq0pq1pAttrs.SaveWeights(out);
  s0cq0wpq1pAttrs.SaveWeights(out);
  s0cq0pq1wpAttrs.SaveWeights(out);
  s0cq0pq1pAttrs.SaveWeights(out);
  s0wcs1cs2cAttrs.SaveWeights(out);
  s0cs1wcs2cAttrs.SaveWeights(out);
  s0cs1cs2wcAttrs.SaveWeights(out);
  s0cs1cs2cAttrs.SaveWeights(out);

  // 6
  s01cs01lcs01rcAttrs.SaveWeights(out);
  s0cs0rcq0pAttrs.SaveWeights(out);
  s0cS0lrcq0s1wAttrs.SaveWeights(out);
  s0cs0ls1cs1s1rcAttrs.SaveWeights(out);
  s0ws1cs1rcAttrs.SaveWeights(out);

  size_t totalfeatures = 0;

  // 1
  totalfeatures += swpAttrs.size();
  totalfeatures += scAttrs.size();
  totalfeatures += spcAttrs.size();
  totalfeatures += swcAttrs.size();

  // 2
  totalfeatures += qwpAttrs.size();

  // 3
  totalfeatures += slurpcAttrs.size();
  totalfeatures += slurwcAttrs.size();

  // 4
  totalfeatures += s0wcs1wcAttrs.size();
  totalfeatures += s0cs1wAttrs.size();
  totalfeatures += s0ws1cAttrs.size();
  totalfeatures += s0cs1cAttrs.size();

  totalfeatures += swcqwpAttrs.size();
  totalfeatures += scqwpAttrs.size();
  totalfeatures += swcqpAttrs.size();
  totalfeatures += scqpAttrs.size();

  // 5
  totalfeatures += s0wcs1cq0pAttrs.size();
  totalfeatures += s0cs1wcq0pAttrs.size();
  totalfeatures += s0cs1cq0wpAttrs.size();
  totalfeatures += s0cs1cq0pAttrs.size();
  totalfeatures += s0psqpqspAttrs.size();
  totalfeatures += s0wcq0pq1pAttrs.size();
  totalfeatures += s0cq0wpq1pAttrs.size();
  totalfeatures += s0cq0pq1wpAttrs.size();
  totalfeatures += s0cq0pq1pAttrs.size();
  totalfeatures += s0wcs1cs2cAttrs.size();
  totalfeatures += s0cs1wcs2cAttrs.size();
  totalfeatures += s0cs1cs2wcAttrs.size();
  totalfeatures += s0cs1cs2cAttrs.size();

  // 6
  totalfeatures += s01cs01lcs01rcAttrs.size();
  totalfeatures += s0cs0rcq0pAttrs.size();
  totalfeatures += s0cS0lrcq0s1wAttrs.size();
  totalfeatures += s0cs0ls1cs1s1rcAttrs.size();
  totalfeatures += s0ws1cs1rcAttrs.size();

  cerr << "saving weights done, total feature count: " << totalfeatures << endl;
  return true;
}


// 1


class SwpAttributes::Impl: public AttributesImpl<SwpValue, MEDIUM, TITANIC>, public Shared {
public:
  Impl(void): AttributesImpl<SwpValue, MEDIUM, TITANIC>("SwpAttributes"){}
};


SwpAttributes::SwpAttributes(void): _impl(new Impl) {}
SwpAttributes::SwpAttributes(SwpAttributes &other): _impl(share(other._impl)) {}
SwpAttributes::~SwpAttributes(void){ release(_impl); delete _impl; }

size_t SwpAttributes::size(void) const { return _impl->size; }
void SwpAttributes::SetFeature(const SwpValue &value, double weightTotal, double weight)
{
  _impl->insertSR(value, weightTotal, weight);
}

void SwpAttributes::SaveWeights(std::ofstream &out)
{
  _impl->SaveWeights(out);
}

double SwpAttributes::GetOrUpdateWeight(bool update, bool correct,
    const size_t type, const Word wordValue, const Word tagValue,
    const ShiftReduceAction &action)
{
  double weight = 0.0;

  SwpValue swpValue(type, wordValue, tagValue, action);
  if (update)
  {
    _impl->PerceptronUpdate(correct, swpValue);
  }
  else
  {
    weight = _impl->find_weightSR(swpValue);
  }

  return weight;
}

class ScAttributes::Impl: public AttributesImpl<ScValue, MEDIUM, TITANIC>, public Shared {
public:
  Impl(void): AttributesImpl<ScValue, MEDIUM, TITANIC>("ScAttributes"){}
};


ScAttributes::ScAttributes(void): _impl(new Impl) {}
ScAttributes::ScAttributes(ScAttributes &other): _impl(share(other._impl)) {}
ScAttributes::~ScAttributes(void){ release(_impl); delete _impl; }

size_t ScAttributes::size(void) const { return _impl->size; }
void ScAttributes::SetFeature(const ScValue &value, double weightTotal, double weight){
  _impl->insertSR(value, weightTotal, weight);
}

void ScAttributes::SaveWeights(std::ofstream &out)
{
  _impl->SaveWeights(out);
}

double ScAttributes::GetOrUpdateWeight(bool update, bool correct,
    const size_t type, const Cat *cat,
    const ShiftReduceAction &action)
{
  double weight = 0.0;

  ScValue scValue(type, cat, action);
  if (update)
  {
    _impl->PerceptronUpdate(correct, scValue);
  }
  else
  {
    weight = _impl->find_weightSR(scValue);
  }

  return weight;
}


class SpcAttributes::Impl: public AttributesImpl<SpcValue, MEDIUM, TITANIC>, public Shared {
public:
  Impl(void): AttributesImpl<SpcValue, MEDIUM, TITANIC>("SpcAttributes"){}
};


SpcAttributes::SpcAttributes(void): _impl(new Impl) {}
SpcAttributes::SpcAttributes(SpcAttributes &other): _impl(share(other._impl)) {}
SpcAttributes::~SpcAttributes(void){ release(_impl); delete _impl; }

size_t SpcAttributes::size(void) const { return _impl->size; }
void SpcAttributes::SetFeature(const SpcValue &value, double weightTotal, double weight){
  _impl->insertSR(value, weightTotal, weight);
}

void SpcAttributes::SaveWeights(std::ofstream &out)
{
  _impl->SaveWeights(out);
}

double SpcAttributes::GetOrUpdateWeight(bool update, bool correct,
    const size_t type, const Word tagValue, const Cat *cat,
    const ShiftReduceAction &action)
{
  double weight = 0.0;

  SpcValue spcValue(type, tagValue, cat, action);
  if (update)
  {
    _impl->PerceptronUpdate(correct, spcValue);
  }
  else
  {
    weight = _impl->find_weightSR(spcValue);
  }

  return weight;
}


class SwcAttributes::Impl: public AttributesImpl<SwcValue, MEDIUM, UPPERLIMIT>, public Shared {
public:
  Impl(void): AttributesImpl<SwcValue, MEDIUM, UPPERLIMIT>("SwcAttributes"){}
};


SwcAttributes::SwcAttributes(void): _impl(new Impl) {}
SwcAttributes::SwcAttributes(SwcAttributes &other): _impl(share(other._impl)) {}
SwcAttributes::~SwcAttributes(void){ release(_impl); delete _impl; }

size_t SwcAttributes::size(void) const { return _impl->size; }
void SwcAttributes::SetFeature(const SwcValue &value, double weightTotal, double weight){
  _impl->insertSR(value, weightTotal, weight);
}

void SwcAttributes::SaveWeights(std::ofstream &out)
{
  _impl->SaveWeights(out);
}

double SwcAttributes::GetOrUpdateWeight(bool update, bool correct,
    const size_t type, const Word wordValue, const Cat *cat,
    const ShiftReduceAction &action)
{

  double weight = 0.0;

  SwcValue swcValue(type, wordValue, cat, action);
  if (update)
  {
    _impl->PerceptronUpdate(correct, swcValue);
  }
  else
  {
    weight = _impl->find_weightSR(swcValue);
  }

  return weight;
}


// 2

class QwpAttributes::Impl: public AttributesImpl<QwpValue, MEDIUM, UPPERLIMIT>, public Shared {
public:
  Impl(void): AttributesImpl<QwpValue, MEDIUM, UPPERLIMIT>("QwpAttributes"){}
};


QwpAttributes::QwpAttributes(void): _impl(new Impl) {}
QwpAttributes::QwpAttributes(QwpAttributes &other): _impl(share(other._impl)) {}
QwpAttributes::~QwpAttributes(void){ release(_impl); delete _impl; }

size_t QwpAttributes::size(void) const { return _impl->size; }
void QwpAttributes::SetFeature(const QwpValue &value, double weightTotal, double weight){
  _impl->insertSR(value, weightTotal, weight);
}

void QwpAttributes::SaveWeights(std::ofstream &out)
{
  _impl->SaveWeights(out);
}

double QwpAttributes::GetOrUpdateWeight(bool update, bool correct,
    const size_t type, const Word wordValue, const Word tagValue,
    const ShiftReduceAction &action)
{

  double weight = 0.0;
  QwpValue qwpValue(type, wordValue, tagValue, action);
  if (update)
  {
    _impl->PerceptronUpdate(correct, qwpValue);
  }
  else
  {
    weight = _impl->find_weightSR(qwpValue);
  }

  return weight;
}



// 3

class SlurpcAttributes::Impl: public AttributesImpl<SlurpcValue, MEDIUM, TITANIC>, public Shared {
public:
  Impl(void): AttributesImpl<SlurpcValue, MEDIUM, TITANIC>("SlurpcAttributes"){}
};

SlurpcAttributes::SlurpcAttributes(void): _impl(new Impl) {}
SlurpcAttributes::SlurpcAttributes(SlurpcAttributes &other): _impl(share(other._impl)) {}
SlurpcAttributes::~SlurpcAttributes(void){ release(_impl); delete _impl; }

size_t SlurpcAttributes::size(void) const { return _impl->size; }
void SlurpcAttributes::SetFeature(const SlurpcValue &value, double weightTotal, double weight){
  _impl->insertSR(value, weightTotal, weight);
}

void SlurpcAttributes::SaveWeights(std::ofstream &out)
{
  _impl->SaveWeights(out);
}

double SlurpcAttributes::GetOrUpdateWeight(bool update, bool correct,
    const size_t type, const Word tagValue, const Cat *cat,
    const ShiftReduceAction &action)
{
  double weight = 0.0;

  SlurpcValue slurpcValue(type, tagValue, cat, action);
  if (update)
  {
    _impl->PerceptronUpdate(correct, slurpcValue);
  }
  else
  {
    weight = _impl->find_weightSR(slurpcValue);
  }

  return weight;
}


class SlurwcAttributes::Impl: public AttributesImpl<SlurwcValue, MEDIUM, UPPERLIMIT>, public Shared {
public:
  Impl(void): AttributesImpl<SlurwcValue, MEDIUM, UPPERLIMIT>("SlurwcAttributes"){}
};

SlurwcAttributes::SlurwcAttributes(void): _impl(new Impl) {}
SlurwcAttributes::SlurwcAttributes(SlurwcAttributes &other): _impl(share(other._impl)) {}
SlurwcAttributes::~SlurwcAttributes(void){ release(_impl); delete _impl; }

size_t SlurwcAttributes::size(void) const { return _impl->size; }
void SlurwcAttributes::SetFeature(const SlurwcValue &value, double weightTotal, double weight){
  _impl->insertSR(value, weightTotal, weight);
}

void SlurwcAttributes::SaveWeights(std::ofstream &out)
{
  _impl->SaveWeights(out);
}

double SlurwcAttributes::GetOrUpdateWeight(bool update, bool correct,
    const size_t type, const Word wordValue, const Cat *cat,
    const ShiftReduceAction &action)
{
  double weight = 0.0;

  SlurwcValue slurwcValue(type, wordValue, cat, action);
  if (update)
  {
    _impl->PerceptronUpdate(correct, slurwcValue);
  }
  else
  {
    weight = _impl->find_weightSR(slurwcValue);
  }

  return weight;
}


// 4

class S0wcS1wcAttributes::Impl: public AttributesImpl<S0wcS1wcValue, MEDIUM, UPPERLIMIT>, public Shared {
public:
  Impl(void): AttributesImpl<S0wcS1wcValue, MEDIUM, UPPERLIMIT>("S0wcS1wcAttributes"){}
};

S0wcS1wcAttributes::S0wcS1wcAttributes(void): _impl(new Impl) {}
S0wcS1wcAttributes::S0wcS1wcAttributes(S0wcS1wcAttributes &other): _impl(share(other._impl)) {}
S0wcS1wcAttributes::~S0wcS1wcAttributes(void){ release(_impl); delete _impl; }

size_t S0wcS1wcAttributes::size(void) const { return _impl->size; }

void S0wcS1wcAttributes::SetFeature(const S0wcS1wcValue &value, double weightTotal, double weight)
{
  _impl->insertSR(value, weightTotal, weight);
}

void S0wcS1wcAttributes::SaveWeights(std::ofstream &out)
{
  _impl->SaveWeights(out);
}

double S0wcS1wcAttributes::GetOrUpdateWeight(bool update, bool correct,
    const size_t type, const Word s0w, const Cat *s0c, const Word s1w, const Cat *s1c,
    const ShiftReduceAction &action)
{
  double weight = 0.0;
  S0wcS1wcValue s0wcS1wcValue(type, s0w, s0c, s1w, s1c, action);
  if (update)
  {
    _impl->PerceptronUpdate(correct, s0wcS1wcValue);
  }
  else
  {
    weight = _impl->find_weightSR(s0wcS1wcValue);
  }

  return weight;

}


class S0cS1wAttributes::Impl: public AttributesImpl<S0cS1wValue, MEDIUM, TITANIC>, public Shared {
public:
  Impl(void): AttributesImpl<S0cS1wValue, MEDIUM, TITANIC>("S0cS1wAttributes"){}
};

S0cS1wAttributes::S0cS1wAttributes(void): _impl(new Impl) {}
S0cS1wAttributes::S0cS1wAttributes(S0cS1wAttributes &other): _impl(share(other._impl)) {}
S0cS1wAttributes::~S0cS1wAttributes(void){ release(_impl); delete _impl; }

size_t S0cS1wAttributes::size(void) const { return _impl->size; }

void S0cS1wAttributes::SetFeature(const S0cS1wValue &value, double weightTotal, double weight)
{
  _impl->insertSR(value, weightTotal, weight);
}

void S0cS1wAttributes::SaveWeights(std::ofstream &out)
{
  _impl->SaveWeights(out);
}

double S0cS1wAttributes::GetOrUpdateWeight(bool update, bool correct,
    const size_t type, const Cat *catS0, const Word wordValueS1,
    const ShiftReduceAction &action)
{
  double weight = 0.0;
  S0cS1wValue s0cS1wValue(type, catS0, wordValueS1, action);
  if (update)
  {
    _impl->PerceptronUpdate(correct, s0cS1wValue);
  }
  else
  {
    weight = _impl->find_weightSR(s0cS1wValue);
  }
  return weight;
}


class S0wS1cAttributes::Impl: public AttributesImpl<S0wS1cValue, MEDIUM, TITANIC>, public Shared {
public:
  Impl(void): AttributesImpl<S0wS1cValue, MEDIUM, TITANIC>("S0wS1cAttributes"){}
};

S0wS1cAttributes::S0wS1cAttributes(void): _impl(new Impl) {}
S0wS1cAttributes::S0wS1cAttributes(S0wS1cAttributes &other): _impl(share(other._impl)) {}
S0wS1cAttributes::~S0wS1cAttributes(void){ release(_impl); delete _impl; }

size_t S0wS1cAttributes::size(void) const { return _impl->size; }
void S0wS1cAttributes::SaveWeights(std::ofstream &out)
{
  _impl->SaveWeights(out);
}
void S0wS1cAttributes::SetFeature(const S0wS1cValue &value, double weightTotal, double weight)
{
  _impl->insertSR(value, weightTotal, weight);
}

double S0wS1cAttributes::GetOrUpdateWeight(bool update, bool correct,
    const size_t type, const Word wordValueS0, const Cat *catS1,
    const ShiftReduceAction &action)
{
  double weight = 0.0;
  S0wS1cValue s0wS1cValue(type, wordValueS0, catS1, action);
  if (update)
  {
    _impl->PerceptronUpdate(correct, s0wS1cValue);
  }
  else
  {
    weight = _impl->find_weightSR(s0wS1cValue);
  }
  return weight;
}


class S0cS1cAttributes::Impl: public AttributesImpl<S0cS1cValue, MEDIUM, TITANIC>, public Shared {
public:
  Impl(void): AttributesImpl<S0cS1cValue, MEDIUM, TITANIC>("S0cS1cAttributes"){}
};

S0cS1cAttributes::S0cS1cAttributes(void): _impl(new Impl) {}
S0cS1cAttributes::S0cS1cAttributes(S0cS1cAttributes &other): _impl(share(other._impl)) {}
S0cS1cAttributes::~S0cS1cAttributes(void){ release(_impl); delete _impl; }

size_t S0cS1cAttributes::size(void) const { return _impl->size; }
void S0cS1cAttributes::SaveWeights(std::ofstream &out)
{
  _impl->SaveWeights(out);
}
void S0cS1cAttributes::SetFeature(const S0cS1cValue &value, double weightTotal, double weight)
{
  _impl->insertSR(value, weightTotal, weight);
}

double S0cS1cAttributes::GetOrUpdateWeight(bool update, bool correct,
    const size_t type, const Cat *catS0, const Cat *catS1,
    const ShiftReduceAction &action)
{
  double weight = 0.0;
  S0cS1cValue s0cS1cValue(type, catS0, catS1, action);
  if (update)
  {
    _impl->PerceptronUpdate(correct, s0cS1cValue);
  }
  else
  {
    weight = _impl->find_weightSR(s0cS1cValue);
  }

  return weight;
}


class SwcQwpAttributes::Impl: public AttributesImpl<SwcQwpValue, MEDIUM, UPPERLIMIT>, public Shared {
public:
  Impl(void): AttributesImpl<SwcQwpValue, MEDIUM, UPPERLIMIT>("SwcQwpAttributes"){}
};

SwcQwpAttributes::SwcQwpAttributes(void): _impl(new Impl) {}
SwcQwpAttributes::SwcQwpAttributes(SwcQwpAttributes &other): _impl(share(other._impl)) {}
SwcQwpAttributes::~SwcQwpAttributes(void){ release(_impl); delete _impl; }

size_t SwcQwpAttributes::size(void) const { return _impl->size; }
void SwcQwpAttributes::SaveWeights(std::ofstream &out)
{
  _impl->SaveWeights(out);
}
void SwcQwpAttributes::SetFeature(const SwcQwpValue &value, double weightTotal, double weight)
{
  _impl->insertSR(value, weightTotal, weight);
}

double SwcQwpAttributes::GetOrUpdateWeight(bool update, bool correct,
    const size_t type, const Word wordValueS, const Cat *catS, const Word wordValueQ, const Word tagValueQ,
    const ShiftReduceAction &action)
{
  double weight = 0.0;

  SwcQwpValue swcQwpValue(type, wordValueS, catS, wordValueQ, tagValueQ, action);
  if (update)
  {
    _impl->PerceptronUpdate(correct, swcQwpValue);
  }
  else
  {
    weight = _impl->find_weightSR(swcQwpValue);
  }

  return weight;
}


class ScQwpAttributes::Impl: public AttributesImpl<ScQwpValue, MEDIUM, TITANIC>, public Shared {
public:
  Impl(void): AttributesImpl<ScQwpValue, MEDIUM, TITANIC>("ScQwpAttributes"){}
};

ScQwpAttributes::ScQwpAttributes(void): _impl(new Impl) {}
ScQwpAttributes::ScQwpAttributes(ScQwpAttributes &other): _impl(share(other._impl)) {}
ScQwpAttributes::~ScQwpAttributes(void){ release(_impl); delete _impl; }

size_t ScQwpAttributes::size(void) const { return _impl->size; }
void ScQwpAttributes::SaveWeights(std::ofstream &out)
{
  _impl->SaveWeights(out);
}
void ScQwpAttributes::SetFeature(const ScQwpValue &value, double weightTotal, double weight)
{
  _impl->insertSR(value, weightTotal, weight);
}

double ScQwpAttributes::GetOrUpdateWeight(bool update, bool correct,
    const size_t type, const Cat *catS, const Word wordValueQ, const Word tagValueQ,
    const ShiftReduceAction &action)
{
  double weight = 0.0;
  ScQwpValue scQwpValue(type, catS, wordValueQ, tagValueQ, action);
  if (update)
  {
    _impl->PerceptronUpdate(correct, scQwpValue);
  }
  else
  {
    weight = _impl->find_weightSR(scQwpValue);
  }
  return weight;
}


class SwcQpAttributes::Impl: public AttributesImpl<SwcQpValue, MEDIUM, UPPERLIMIT>, public Shared {
public:
  Impl(void): AttributesImpl<SwcQpValue, MEDIUM, UPPERLIMIT>("SwcQpAttributes"){}
};

SwcQpAttributes::SwcQpAttributes(void): _impl(new Impl) {}
SwcQpAttributes::SwcQpAttributes(SwcQpAttributes &other): _impl(share(other._impl)) {}
SwcQpAttributes::~SwcQpAttributes(void){ release(_impl); delete _impl; }

size_t SwcQpAttributes::size(void) const { return _impl->size; }
void SwcQpAttributes::SaveWeights(std::ofstream &out)
{
  _impl->SaveWeights(out);
}
void SwcQpAttributes::SetFeature(const SwcQpValue &value, double weightTotal, double weight)
{
  _impl->insertSR(value, weightTotal, weight);
}

double SwcQpAttributes::GetOrUpdateWeight(bool update, bool correct,
    const size_t type, const Word wordValueS, const Cat *catS, const Word tagValueQ,
    const ShiftReduceAction &action)
{
  double weight = 0.0;

  SwcQpValue swcQpValue(type, wordValueS, catS, tagValueQ, action);
  if (update)
  {
    _impl->PerceptronUpdate(correct, swcQpValue);
  }
  else
  {
    weight = _impl->find_weightSR(swcQpValue);
  }

  return weight;
}



class ScQpAttributes::Impl: public AttributesImpl<ScQpValue, MEDIUM, TITANIC>, public Shared {
public:
  Impl(void): AttributesImpl<ScQpValue, MEDIUM, TITANIC>("ScQpAttributes"){}
};

ScQpAttributes::ScQpAttributes(void): _impl(new Impl) {}
ScQpAttributes::ScQpAttributes(ScQpAttributes &other): _impl(share(other._impl)) {}
ScQpAttributes::~ScQpAttributes(void){ release(_impl); delete _impl; }

size_t ScQpAttributes::size(void) const { return _impl->size; }
void ScQpAttributes::SaveWeights(std::ofstream &out)
{
  _impl->SaveWeights(out);
}
void ScQpAttributes::SetFeature(const ScQpValue &value, double weightTotal, double weight)
{
  _impl->insertSR(value, weightTotal, weight);
}

double ScQpAttributes::GetOrUpdateWeight(bool update, bool correct,
    const size_t type, const Cat *catS, const Word tagValueQ,
    const ShiftReduceAction &action)
{
  double weight = 0.0;

  ScQpValue scQpValue(type, catS, tagValueQ, action);
  if (update)
  {
    _impl->PerceptronUpdate(correct, scQpValue);
  }
  else
  {
    weight = _impl->find_weightSR(scQpValue);
  }

  return weight;
}


// 5

class S0wcS1cQ0pAttributes::Impl: public AttributesImpl<S0wcS1cQ0pValue, MEDIUM, UPPERLIMIT>, public Shared {
public:
  Impl(void): AttributesImpl<S0wcS1cQ0pValue, MEDIUM, UPPERLIMIT>("S0wcS1cQ0pAttributes"){}
};

S0wcS1cQ0pAttributes::S0wcS1cQ0pAttributes(void): _impl(new Impl) {}
S0wcS1cQ0pAttributes::S0wcS1cQ0pAttributes(S0wcS1cQ0pAttributes &other): _impl(share(other._impl)) {}
S0wcS1cQ0pAttributes::~S0wcS1cQ0pAttributes(void){ release(_impl); delete _impl; }

size_t S0wcS1cQ0pAttributes::size(void) const { return _impl->size; }
void S0wcS1cQ0pAttributes::SaveWeights(std::ofstream &out)
{
  _impl->SaveWeights(out);
}
void S0wcS1cQ0pAttributes::SetFeature(const S0wcS1cQ0pValue &value, double weightTotal, double weight)
{
  _impl->insertSR(value, weightTotal, weight);
}

double S0wcS1cQ0pAttributes::GetOrUpdateWeight(bool update, bool correct,
    const size_t type, const Word wordValueS0, const Cat *catS0, const Cat *catS1, const Word tagValueQ0,
    const ShiftReduceAction &action)
{
  double weight = 0.0;

  S0wcS1cQ0pValue s0wcS1cQ0pValue(type, wordValueS0, catS0, catS1, tagValueQ0, action);
  if (update)
  {
    _impl->PerceptronUpdate(correct, s0wcS1cQ0pValue);
  }
  else
  {
    weight = _impl->find_weightSR(s0wcS1cQ0pValue);
  }

  return weight;
}


class S0cS1wcQ0pAttributes::Impl: public AttributesImpl<S0cS1wcQ0pValue, MEDIUM, TITANIC>, public Shared {
public:
  Impl(void): AttributesImpl<S0cS1wcQ0pValue, MEDIUM, TITANIC>("S0cS1wcQ0pAttributes"){}
};

S0cS1wcQ0pAttributes::S0cS1wcQ0pAttributes(void): _impl(new Impl) {}
S0cS1wcQ0pAttributes::S0cS1wcQ0pAttributes(S0cS1wcQ0pAttributes &other): _impl(share(other._impl)) {}
S0cS1wcQ0pAttributes::~S0cS1wcQ0pAttributes(void){ release(_impl); delete _impl; }

size_t S0cS1wcQ0pAttributes::size(void) const { return _impl->size; }
void S0cS1wcQ0pAttributes::SaveWeights(std::ofstream &out)
{
  _impl->SaveWeights(out);
}
void S0cS1wcQ0pAttributes::SetFeature(const S0cS1wcQ0pValue &value, double weightTotal, double weight)
{
  _impl->insertSR(value, weightTotal, weight);
}

double S0cS1wcQ0pAttributes::GetOrUpdateWeight(bool update, bool correct,
    const size_t type, const Cat *catS0, const Word wordValueS1, const Cat *catS1, const Word tagValueQ0,
    const ShiftReduceAction &action)
{
  double weight = 0.0;

  S0cS1wcQ0pValue s0cS1wcQ0pValue(type, catS0, wordValueS1, catS1, tagValueQ0, action);
  if (update)
  {
    _impl->PerceptronUpdate(correct, s0cS1wcQ0pValue);
  }
  else
  {
    weight = _impl->find_weightSR(s0cS1wcQ0pValue);
  }

  return weight;
}


class S0cS1cQ0wpAttributes::Impl: public AttributesImpl<S0cS1cQ0wpValue, MEDIUM, TITANIC>, public Shared {
public:
  Impl(void): AttributesImpl<S0cS1cQ0wpValue, MEDIUM, TITANIC>("S0cS1cQ0wpAttributes"){}
};

S0cS1cQ0wpAttributes::S0cS1cQ0wpAttributes(void): _impl(new Impl) {}
S0cS1cQ0wpAttributes::S0cS1cQ0wpAttributes(S0cS1cQ0wpAttributes &other): _impl(share(other._impl)) {}
S0cS1cQ0wpAttributes::~S0cS1cQ0wpAttributes(void){ release(_impl); delete _impl; }

size_t S0cS1cQ0wpAttributes::size(void) const { return _impl->size; }
void S0cS1cQ0wpAttributes::SaveWeights(std::ofstream &out)
{
  _impl->SaveWeights(out);
}
void S0cS1cQ0wpAttributes::SetFeature(const S0cS1cQ0wpValue &value, double weightTotal, double weight)
{
  _impl->insertSR(value, weightTotal, weight);
}

double S0cS1cQ0wpAttributes::GetOrUpdateWeight(bool update, bool correct,
    const size_t type, const Cat *catS0, const Cat *catS1, const Word wordValueQ0, const Word tagValueQ0,
    const ShiftReduceAction &action)
{
  double weight = 0.0;

  S0cS1cQ0wpValue s0cS1cQ0wpValue(type, catS0, catS1, wordValueQ0, tagValueQ0, action);
  if (update)
  {
    _impl->PerceptronUpdate(correct, s0cS1cQ0wpValue);
  }
  else
  {
    weight = _impl->find_weightSR(s0cS1cQ0wpValue);
  }

  return weight;
}


class S0cS1cQ0pAttributes::Impl: public AttributesImpl<S0cS1cQ0pValue, MEDIUM, TITANIC>, public Shared {
public:
  Impl(void): AttributesImpl<S0cS1cQ0pValue, MEDIUM, TITANIC>("S0cS1cQ0pAttributes"){}
};

S0cS1cQ0pAttributes::S0cS1cQ0pAttributes(void): _impl(new Impl) {}
S0cS1cQ0pAttributes::S0cS1cQ0pAttributes(S0cS1cQ0pAttributes &other): _impl(share(other._impl)) {}
S0cS1cQ0pAttributes::~S0cS1cQ0pAttributes(void){ release(_impl); delete _impl; }

size_t S0cS1cQ0pAttributes::size(void) const { return _impl->size; }
void S0cS1cQ0pAttributes::SaveWeights(std::ofstream &out)
{
  _impl->SaveWeights(out);
}
void S0cS1cQ0pAttributes::SetFeature(const S0cS1cQ0pValue &value, double weightTotal, double weight)
{
  _impl->insertSR(value, weightTotal, weight);
}

double S0cS1cQ0pAttributes::GetOrUpdateWeight(bool update, bool correct,
    const size_t type, const Cat *catS0, const Cat *catS1, const Word tagValueQ0,
    const ShiftReduceAction &action)
{
  double weight = 0.0;

  S0cS1cQ0pValue s0cS1cQ0pValue(type, catS0, catS1, tagValueQ0, action);
  if (update)
  {
    _impl->PerceptronUpdate(correct, s0cS1cQ0pValue);
  }
  else
  {
    weight = _impl->find_weightSR(s0cS1cQ0pValue);
  }

  return weight;
}


class S0pSQpQSpAttributes::Impl: public AttributesImpl<S0pSQpQSpValue, MEDIUM, TITANIC>, public Shared {
public:
  Impl(void): AttributesImpl<S0pSQpQSpValue, MEDIUM, TITANIC>("S0pSQpQSpAttributes"){}
};

S0pSQpQSpAttributes::S0pSQpQSpAttributes(void): _impl(new Impl) {}
S0pSQpQSpAttributes::S0pSQpQSpAttributes(S0pSQpQSpAttributes &other): _impl(share(other._impl)) {}
S0pSQpQSpAttributes::~S0pSQpQSpAttributes(void){ release(_impl); delete _impl; }

size_t S0pSQpQSpAttributes::size(void) const { return _impl->size; }
void S0pSQpQSpAttributes::SaveWeights(std::ofstream &out)
{
  _impl->SaveWeights(out);
}
void S0pSQpQSpAttributes::SetFeature(const  S0pSQpQSpValue &value, double weightTotal, double weight)
{
  _impl->insertSR(value, weightTotal, weight);
}

double S0pSQpQSpAttributes::GetOrUpdateWeight(bool update, bool correct,
    const size_t type, const Word tagValueS0, const Word tagValueSQ, const Word tagValueQS,
    const ShiftReduceAction &action)
{
  double weight = 0.0;

  S0pSQpQSpValue s0pSQpQSpValue(type, tagValueS0, tagValueSQ, tagValueQS, action);
  if (update)
  {
    _impl->PerceptronUpdate(correct, s0pSQpQSpValue);
  }
  else
  {
    weight = _impl->find_weightSR(s0pSQpQSpValue);
  }

  return weight;
}


class S0wcQ0pQ1pAttributes::Impl: public AttributesImpl<S0wcQ0pQ1pValue, MEDIUM, TITANIC>, public Shared {
public:
  Impl(void): AttributesImpl<S0wcQ0pQ1pValue, MEDIUM, TITANIC>("S0wcQ0pQ1pAttributes"){}
};

S0wcQ0pQ1pAttributes::S0wcQ0pQ1pAttributes(void): _impl(new Impl) {}
S0wcQ0pQ1pAttributes::S0wcQ0pQ1pAttributes(S0wcQ0pQ1pAttributes &other): _impl(share(other._impl)) {}
S0wcQ0pQ1pAttributes::~S0wcQ0pQ1pAttributes(void){ release(_impl); delete _impl; }

size_t S0wcQ0pQ1pAttributes::size(void) const { return _impl->size; }
void S0wcQ0pQ1pAttributes::SaveWeights(std::ofstream &out)
{
  _impl->SaveWeights(out);
}
void S0wcQ0pQ1pAttributes::SetFeature(const S0wcQ0pQ1pValue &value, double weightTotal, double weight)
{
  _impl->insertSR(value, weightTotal, weight);
}
double S0wcQ0pQ1pAttributes::GetOrUpdateWeight(bool update, bool correct,
    const size_t type, const Word wordValueS0, const Cat *catS0, const Word tagValueQ0, const Word tagValueQ1,
    const ShiftReduceAction &action)
{
  double weight = 0.0;

  S0wcQ0pQ1pValue s0wcQ0pQ1pValue(type, wordValueS0, catS0, tagValueQ0, tagValueQ1, action);
  if (update)
  {
    _impl->PerceptronUpdate(correct, s0wcQ0pQ1pValue);
  }
  else
  {
    weight = _impl->find_weightSR(s0wcQ0pQ1pValue);
  }

  return weight;
}


class S0cQ0wpQ1pAttributes::Impl: public AttributesImpl<S0cQ0wpQ1pValue, MEDIUM, TITANIC>, public Shared {
public:
  Impl(void): AttributesImpl<S0cQ0wpQ1pValue, MEDIUM, TITANIC>("S0cQ0wpQ1pAttributes"){}
};

S0cQ0wpQ1pAttributes::S0cQ0wpQ1pAttributes(void): _impl(new Impl) {}
S0cQ0wpQ1pAttributes::S0cQ0wpQ1pAttributes(S0cQ0wpQ1pAttributes &other): _impl(share(other._impl)) {}
S0cQ0wpQ1pAttributes::~S0cQ0wpQ1pAttributes(void){ release(_impl); delete _impl; }

size_t S0cQ0wpQ1pAttributes::size(void) const { return _impl->size; }
void S0cQ0wpQ1pAttributes::SaveWeights(std::ofstream &out)
{
  _impl->SaveWeights(out);
}
void S0cQ0wpQ1pAttributes::SetFeature(const S0cQ0wpQ1pValue &value, double weightTotal, double weight)
{
  _impl->insertSR(value, weightTotal, weight);
}

double S0cQ0wpQ1pAttributes::GetOrUpdateWeight(bool update, bool correct,
    const size_t type, const Cat *catS0, const Word wordValueQ0, const Word tagValueQ0, const Word tagValueQ1,
    const ShiftReduceAction &action)
{
  double weight = 0.0;

  S0cQ0wpQ1pValue s0cQ0wpQ1pValue(type, catS0, wordValueQ0, tagValueQ0, tagValueQ1, action);
  if (update)
  {
    _impl->PerceptronUpdate(correct, s0cQ0wpQ1pValue);
  }
  else
  {
    weight = _impl->find_weightSR(s0cQ0wpQ1pValue);
  }

  return weight;
}


class S0cQ0pQ1wpAttributes::Impl: public AttributesImpl<S0cQ0pQ1wpValue, MEDIUM, TITANIC>, public Shared {
public:
  Impl(void): AttributesImpl<S0cQ0pQ1wpValue, MEDIUM, TITANIC>("S0cQ0pQ1wpAttributes"){}
};

S0cQ0pQ1wpAttributes::S0cQ0pQ1wpAttributes(void): _impl(new Impl) {}
S0cQ0pQ1wpAttributes::S0cQ0pQ1wpAttributes(S0cQ0pQ1wpAttributes &other): _impl(share(other._impl)) {}
S0cQ0pQ1wpAttributes::~S0cQ0pQ1wpAttributes(void){ release(_impl); delete _impl; }

size_t S0cQ0pQ1wpAttributes::size(void) const { return _impl->size; }
void S0cQ0pQ1wpAttributes::SaveWeights(std::ofstream &out)
{
  _impl->SaveWeights(out);
}
void S0cQ0pQ1wpAttributes::SetFeature(const S0cQ0pQ1wpValue &value, double weightTotal, double weight)
{
  _impl->insertSR(value, weightTotal, weight);
}

double S0cQ0pQ1wpAttributes::GetOrUpdateWeight(bool update, bool correct,
    const size_t type, const Cat *catS0, const Word tagValueQ0, const Word wordValueQ1, const Word tagValueQ1,
    const ShiftReduceAction &action)
{
  double weight = 0.0;

  S0cQ0pQ1wpValue s0cQ0pQ1wpValue(type, catS0, tagValueQ0, wordValueQ1, tagValueQ1, action);
  if (update)
  {
    _impl->PerceptronUpdate(correct, s0cQ0pQ1wpValue);
  }
  else
  {
    weight = _impl->find_weightSR(s0cQ0pQ1wpValue);
  }

  return weight;
}


class S0cQ0pQ1pAttributes::Impl: public AttributesImpl<S0cQ0pQ1pValue, MEDIUM, TITANIC>, public Shared {
public:
  Impl(void): AttributesImpl<S0cQ0pQ1pValue, MEDIUM, TITANIC>("S0cQ0pQ1pAttributes"){}
};

S0cQ0pQ1pAttributes::S0cQ0pQ1pAttributes(void): _impl(new Impl) {}
S0cQ0pQ1pAttributes::S0cQ0pQ1pAttributes(S0cQ0pQ1pAttributes &other): _impl(share(other._impl)) {}
S0cQ0pQ1pAttributes::~S0cQ0pQ1pAttributes(void){ release(_impl); delete _impl; }

size_t S0cQ0pQ1pAttributes::size(void) const { return _impl->size; }
void S0cQ0pQ1pAttributes::SaveWeights(std::ofstream &out)
{
  _impl->SaveWeights(out);
}
void S0cQ0pQ1pAttributes::SetFeature(const S0cQ0pQ1pValue &value, double weightTotal, double weight)
{
  _impl->insertSR(value, weightTotal, weight);
}

double S0cQ0pQ1pAttributes::GetOrUpdateWeight(bool update, bool correct,
    const size_t type, const Cat *catS0, const Word tagValueQ0, const Word tagValueQ1,
    const ShiftReduceAction &action)
{
  double weight = 0.0;

  S0cQ0pQ1pValue s0cQ0pQ1pValue(type, catS0, tagValueQ0, tagValueQ1, action);
  if (update)
  {
    _impl->PerceptronUpdate(correct, s0cQ0pQ1pValue);
  }
  else
  {
    weight = _impl->find_weightSR(s0cQ0pQ1pValue);
  }

  return weight;
}


class S0wcS1cS2cAttributes::Impl: public AttributesImpl<S0wcS1cS2cValue, MEDIUM, TITANIC>, public Shared {
public:
  Impl(void): AttributesImpl<S0wcS1cS2cValue, MEDIUM, TITANIC>("S0wcS1cS2cAttributes"){}
};

S0wcS1cS2cAttributes::S0wcS1cS2cAttributes(void): _impl(new Impl) {}
S0wcS1cS2cAttributes::S0wcS1cS2cAttributes(S0wcS1cS2cAttributes &other): _impl(share(other._impl)) {}
S0wcS1cS2cAttributes::~S0wcS1cS2cAttributes(void){ release(_impl); delete _impl; }

size_t S0wcS1cS2cAttributes::size(void) const { return _impl->size; }
void S0wcS1cS2cAttributes::SaveWeights(std::ofstream &out)
{
  _impl->SaveWeights(out);
}
void S0wcS1cS2cAttributes::SetFeature(const S0wcS1cS2cValue &value, double weightTotal, double weight)
{
  _impl->insertSR(value, weightTotal, weight);
}

double S0wcS1cS2cAttributes::GetOrUpdateWeight(bool update, bool correct,
    const size_t type, const Word wordValueS0, const Cat *catS0, const Cat *catS1, const Cat *catS2,
    const ShiftReduceAction &action)
{
  double weight = 0.0;

  S0wcS1cS2cValue s0wcS1cS2cValue(type, wordValueS0, catS0, catS1, catS2, action);
  if (update)
  {
    _impl->PerceptronUpdate(correct, s0wcS1cS2cValue);
  }
  else
  {
    weight = _impl->find_weightSR(s0wcS1cS2cValue);
  }

  return weight;
}


class S0cS1wcS2cAttributes::Impl: public AttributesImpl<S0cS1wcS2cValue, MEDIUM, TITANIC>, public Shared {
public:
  Impl(void): AttributesImpl<S0cS1wcS2cValue, MEDIUM, TITANIC>("S0cS1wcS2cAttributes"){}
};

S0cS1wcS2cAttributes::S0cS1wcS2cAttributes(void): _impl(new Impl) {}
S0cS1wcS2cAttributes::S0cS1wcS2cAttributes(S0cS1wcS2cAttributes &other): _impl(share(other._impl)) {}
S0cS1wcS2cAttributes::~S0cS1wcS2cAttributes(void){ release(_impl); delete _impl; }

size_t S0cS1wcS2cAttributes::size(void) const { return _impl->size; }
void S0cS1wcS2cAttributes::SaveWeights(std::ofstream &out)
{
  _impl->SaveWeights(out);
}
void S0cS1wcS2cAttributes::SetFeature(const S0cS1wcS2cValue &value, double weightTotal, double weight)
{
  _impl->insertSR(value, weightTotal, weight);
}

double S0cS1wcS2cAttributes::GetOrUpdateWeight(bool update, bool correct,
    const size_t type, const Cat *catS0, const Word wordValueS1, const Cat *catS1, const Cat *catS2,
    const ShiftReduceAction &action)
{
  double weight = 0.0;

  S0cS1wcS2cValue s0cS1wcS2cValue(type, catS0, wordValueS1, catS1, catS2, action);
  if (update)
  {
    _impl->PerceptronUpdate(correct, s0cS1wcS2cValue);
  }
  else
  {
    weight = _impl->find_weightSR(s0cS1wcS2cValue);
  }

  return weight;
}


class S0cS1cS2wcAttributes::Impl: public AttributesImpl<S0cS1cS2wcValue, MEDIUM, TITANIC>, public Shared {
public:
  Impl(void): AttributesImpl<S0cS1cS2wcValue, MEDIUM, TITANIC>("S0cS1cS2wcAttributes"){}
};

S0cS1cS2wcAttributes::S0cS1cS2wcAttributes(void): _impl(new Impl) {}
S0cS1cS2wcAttributes::S0cS1cS2wcAttributes(S0cS1cS2wcAttributes &other): _impl(share(other._impl)) {}
S0cS1cS2wcAttributes::~S0cS1cS2wcAttributes(void){ release(_impl); delete _impl; }

size_t S0cS1cS2wcAttributes::size(void) const { return _impl->size; }
void S0cS1cS2wcAttributes::SaveWeights(std::ofstream &out)
{
  _impl->SaveWeights(out);
}
void S0cS1cS2wcAttributes::SetFeature(const S0cS1cS2wcValue &value, double weightTotal, double weight)
{
  _impl->insertSR(value, weightTotal, weight);
}

double S0cS1cS2wcAttributes::GetOrUpdateWeight(bool update, bool correct,
    const size_t type, const Cat *catS0, const Cat *catS1, const Word wordValueS2, const Cat *catS2,
    const ShiftReduceAction &action)
{
  double weight = 0.0;

  S0cS1cS2wcValue s0cS1cS2wcValue(type, catS0, catS1, wordValueS2, catS2, action);
  if (update)
  {
    _impl->PerceptronUpdate(correct, s0cS1cS2wcValue);
  }
  else
  {
    weight = _impl->find_weightSR(s0cS1cS2wcValue);
  }

  return weight;
}


class S0cS1cS2cAttributes::Impl: public AttributesImpl<S0cS1cS2cValue, MEDIUM, TITANIC>, public Shared {
public:
  Impl(void): AttributesImpl<S0cS1cS2cValue, MEDIUM, TITANIC>("S0cS1cS2cAttributes"){}
};

S0cS1cS2cAttributes::S0cS1cS2cAttributes(void): _impl(new Impl) {}
S0cS1cS2cAttributes::S0cS1cS2cAttributes(S0cS1cS2cAttributes &other): _impl(share(other._impl)) {}
S0cS1cS2cAttributes::~S0cS1cS2cAttributes(void){ release(_impl); delete _impl; }

size_t S0cS1cS2cAttributes::size(void) const { return _impl->size; }
void S0cS1cS2cAttributes::SaveWeights(std::ofstream &out)
{
  _impl->SaveWeights(out);
}
void S0cS1cS2cAttributes::SetFeature(const S0cS1cS2cValue &value, double weightTotal, double weight)
{
  _impl->insertSR(value, weightTotal, weight);
}

double S0cS1cS2cAttributes::GetOrUpdateWeight(bool update, bool correct,
    const size_t type, const Cat *catS0, const Cat *catS1, const Cat *catS2,
    const ShiftReduceAction &action)
{
  double weight = 0.0;

  S0cS1cS2cValue s0cS1cS2cValue(type, catS0, catS1, catS2, action);
  if (update)
  {
    _impl->PerceptronUpdate(correct, s0cS1cS2cValue);
  }
  else
  {
    weight = _impl->find_weightSR(s0cS1cS2cValue);
  }

  return weight;
}

// 6

class S01cS01LcS01rcAttributes::Impl: public AttributesImpl<S01cS01LcS01rcValue, MEDIUM, TITANIC>, public Shared {
public:
  Impl(void): AttributesImpl<S01cS01LcS01rcValue, MEDIUM, TITANIC>("S01cS01LcS01rcAttributes"){}
};

S01cS01LcS01rcAttributes::S01cS01LcS01rcAttributes(void): _impl(new Impl) {}
S01cS01LcS01rcAttributes::S01cS01LcS01rcAttributes(S01cS01LcS01rcAttributes &other): _impl(share(other._impl)) {}
S01cS01LcS01rcAttributes::~S01cS01LcS01rcAttributes(void){ release(_impl); delete _impl; }

size_t S01cS01LcS01rcAttributes::size(void) const { return _impl->size; }
void S01cS01LcS01rcAttributes::SaveWeights(std::ofstream &out)
{
  _impl->SaveWeights(out);
}
void S01cS01LcS01rcAttributes::SetFeature(const S01cS01LcS01rcValue &value, double weightTotal, double weight)
{
  _impl->insertSR(value, weightTotal, weight);
}

double S01cS01LcS01rcAttributes::GetOrUpdateWeight(bool update, bool correct,
    const size_t type, const Cat *cat0, const Cat *cat1, const Cat *cat2,
    const ShiftReduceAction &action)
{
  double weight = 0.0;

  S01cS01LcS01rcValue s01cS01LcS01rcValue(type, cat0, cat1, cat2, action);
  if (update)
  {
    _impl->PerceptronUpdate(correct, s01cS01LcS01rcValue);
  }
  else
  {
    weight = _impl->find_weightSR(s01cS01LcS01rcValue);
  }

  return weight;
}


class S0cS0RcQ0pAttributes::Impl: public AttributesImpl<S0cS0RcQ0pValue, MEDIUM, TITANIC>, public Shared {
public:
  Impl(void): AttributesImpl<S0cS0RcQ0pValue, MEDIUM, TITANIC>("S0cS0RcQ0pAttributes"){}
};

S0cS0RcQ0pAttributes::S0cS0RcQ0pAttributes(void): _impl(new Impl) {}
S0cS0RcQ0pAttributes::S0cS0RcQ0pAttributes(S0cS0RcQ0pAttributes &other): _impl(share(other._impl)) {}
S0cS0RcQ0pAttributes::~S0cS0RcQ0pAttributes(void){ release(_impl); delete _impl; }

size_t S0cS0RcQ0pAttributes::size(void) const { return _impl->size; }
void S0cS0RcQ0pAttributes::SaveWeights(std::ofstream &out)
{
  _impl->SaveWeights(out);
}
void S0cS0RcQ0pAttributes::SetFeature(const S0cS0RcQ0pValue &value, double weightTotal, double weight)
{
  _impl->insertSR(value, weightTotal, weight);
}

double S0cS0RcQ0pAttributes::GetOrUpdateWeight(bool update, bool correct,
    const size_t type, const Cat *s0c, const Cat *s0rc, const Word q0p,
    const ShiftReduceAction &action)
{
  double weight = 0.0;

  S0cS0RcQ0pValue s0cS0RcQ0pValue(type, s0c, s0rc, q0p, action);
  if (update)
  {
    _impl->PerceptronUpdate(correct, s0cS0RcQ0pValue);
  }
  else
  {
    weight = _impl->find_weightSR(s0cS0RcQ0pValue);
  }

  return weight;
}


class S0cS0LRcQ0S1wAttributes::Impl: public AttributesImpl<S0cS0LRcQ0S1wValue, MEDIUM, TITANIC>, public Shared {
public:
  Impl(void): AttributesImpl<S0cS0LRcQ0S1wValue, MEDIUM, TITANIC>("S0cS0LRcQ0S1wAttributes"){}
};

S0cS0LRcQ0S1wAttributes::S0cS0LRcQ0S1wAttributes(void): _impl(new Impl) {}
S0cS0LRcQ0S1wAttributes::S0cS0LRcQ0S1wAttributes(S0cS0LRcQ0S1wAttributes &other): _impl(share(other._impl)) {}
S0cS0LRcQ0S1wAttributes::~S0cS0LRcQ0S1wAttributes(void){ release(_impl); delete _impl; }

size_t S0cS0LRcQ0S1wAttributes::size(void) const { return _impl->size; }
void S0cS0LRcQ0S1wAttributes::SaveWeights(std::ofstream &out)
{
  _impl->SaveWeights(out);
}
void S0cS0LRcQ0S1wAttributes::SetFeature(const S0cS0LRcQ0S1wValue &value, double weightTotal, double weight)
{
  _impl->insertSR(value, weightTotal, weight);
}

double S0cS0LRcQ0S1wAttributes::GetOrUpdateWeight(bool update, bool correct,
    const size_t type, const Cat *s0c, const Cat *s0lrc, Word q0s1w,
    const ShiftReduceAction &action)
{
  double weight = 0.0;

  S0cS0LRcQ0S1wValue s0cS0LRcQ0S1w(type, s0c, s0lrc, q0s1w, action);
  if (update)
  {
    _impl->PerceptronUpdate(correct, s0cS0LRcQ0S1w);
  }
  else
  {
    weight = _impl->find_weightSR(s0cS0LRcQ0S1w);
  }

  return weight;
}


class S0cS0LS1cS1S1RcAttributes::Impl: public AttributesImpl<S0cS0LS1cS1S1RcValue, MEDIUM, TITANIC>, public Shared {
public:
  Impl(void): AttributesImpl<S0cS0LS1cS1S1RcValue, MEDIUM, TITANIC>("S0cS0LS1cS1S1RcAttributes"){}
};

S0cS0LS1cS1S1RcAttributes::S0cS0LS1cS1S1RcAttributes(void): _impl(new Impl) {}
S0cS0LS1cS1S1RcAttributes::S0cS0LS1cS1S1RcAttributes(S0cS0LS1cS1S1RcAttributes &other): _impl(share(other._impl)) {}
S0cS0LS1cS1S1RcAttributes::~S0cS0LS1cS1S1RcAttributes(void){ release(_impl); delete _impl; }

size_t S0cS0LS1cS1S1RcAttributes::size(void) const { return _impl->size; }
void S0cS0LS1cS1S1RcAttributes::SaveWeights(std::ofstream &out)
{
  _impl->SaveWeights(out);
}
void S0cS0LS1cS1S1RcAttributes::SetFeature(const S0cS0LS1cS1S1RcValue &value, double weightTotal, double weight)
{
  _impl->insertSR(value, weightTotal, weight);
}

double S0cS0LS1cS1S1RcAttributes::GetOrUpdateWeight(bool update, bool correct,
    const size_t type, const Cat *cat0, const Cat *cat1, const Cat *cat2,
    const ShiftReduceAction &action)
{
  double weight = 0.0;

  S0cS0LS1cS1S1RcValue s0cS0LS1cS1S1RcValue(type, cat0, cat1, cat2, action);
  if (update)
  {
    _impl->PerceptronUpdate(correct, s0cS0LS1cS1S1RcValue);
  }
  else
  {
    weight = _impl->find_weightSR(s0cS0LS1cS1S1RcValue);
  }

  return weight;
}


class S0wS1cS1RcAttributes::Impl: public AttributesImpl<S0wS1cS1RcValue, MEDIUM, TITANIC>, public Shared {
public:
  Impl(void): AttributesImpl<S0wS1cS1RcValue, MEDIUM, TITANIC>("S0wS1cS1RcAttributes"){}
};

S0wS1cS1RcAttributes::S0wS1cS1RcAttributes(void): _impl(new Impl) {}
S0wS1cS1RcAttributes::S0wS1cS1RcAttributes(S0wS1cS1RcAttributes &other): _impl(share(other._impl)) {}
S0wS1cS1RcAttributes::~S0wS1cS1RcAttributes(void){ release(_impl); delete _impl; }

size_t S0wS1cS1RcAttributes::size(void) const { return _impl->size; }
void S0wS1cS1RcAttributes::SaveWeights(std::ofstream &out)
{
  _impl->SaveWeights(out);
}
void S0wS1cS1RcAttributes::SetFeature(const S0wS1cS1RcValue &value, double weightTotal, double weight)
{
  _impl->insertSR(value, weightTotal, weight);
}

double S0wS1cS1RcAttributes::GetOrUpdateWeight(bool update, bool correct,
    const size_t type, const Word s0w, const Cat *s1c, const Cat *s1rc,
    const ShiftReduceAction &action)
{
  double weight = 0.0;

  S0wS1cS1RcValue s0wS1cS1RcValue(type, s0w, s1c, s1rc, action);
  if (update)
  {
    _impl->PerceptronUpdate(correct, s0wS1cS1RcValue);
  }
  else
  {
    weight = _impl->find_weightSR(s0wS1cS1RcValue);
  }

  return weight;
}


} // namespace
} // namespace
