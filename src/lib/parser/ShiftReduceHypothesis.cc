#include "parser/ShiftReduceHypothesis.h"
#include "parser/ShiftReduceLattice.h"
#include "parser/_parser.h"
//#include "lstm_parser/context_load.h"

namespace NLP { namespace CCG {

class SuperCat;

const ShiftReduceHypothesis*
ShiftReduceHypothesis::Shift(size_t index, const SuperCat *superCat,
                             size_t unaryActionCount, const double totalScore, const size_t action_id,
                             const double action_score,
                             Expression &act_log_prob) const {
  assert(superCat);
  size_t nextInputIndex = m_nextInputIndex + 1;
  size_t stackSize = m_stackSize + 1;
  size_t totalActionCount = m_totalActionCount + 1;
  size_t total_shift_count = m_total_shift_count + 1;
  ShiftReduceHypothesis *newHypo
  = new ShiftReduceHypothesis(superCat, this, this,
      nextInputIndex, unaryActionCount, stackSize, totalScore, m_finished,
      SHIFT, totalActionCount, total_shift_count, action_id, action_score, index);
//  newHypo->m_c_stack_pointers.reserve(totalActionCount + 1);
//  newHypo->m_w_stack_pointers.reserve(totalActionCount + 1);
//  newHypo->m_s_stack_pointers.reserve(totalActionCount + 1);
//  newHypo->m_a_stack_pointers.reserve(totalActionCount + 1);
//  newHypo->m_p_stack_pointers.reserve(totalActionCount + 1);
  newHypo->m_total_log_prob = m_total_log_prob + act_log_prob;
  return newHypo;
}

const ShiftReduceHypothesis*
ShiftReduceHypothesis::Unary(size_t index, const SuperCat *superCat,
                             size_t unaryActionCount, const double totalScore,
                             const size_t action_id, const double action_score,
                             Expression &act_log_prob) const {
  assert(superCat);
  size_t totalActionCount = m_totalActionCount + 1;
  ShiftReduceHypothesis *newHypo
  = new ShiftReduceHypothesis(superCat, this,
      m_prevStack, m_nextInputIndex, unaryActionCount, m_stackSize, totalScore, m_finished,
      UNARY, totalActionCount, m_total_shift_count, action_id, action_score, index);
//  newHypo->m_c_stack_pointers.reserve(totalActionCount + 1);
//  newHypo->m_w_stack_pointers.reserve(totalActionCount + 1);
//  newHypo->m_s_stack_pointers.reserve(totalActionCount + 1);
//  newHypo->m_a_stack_pointers.reserve(totalActionCount + 1);
//  newHypo->m_p_stack_pointers.reserve(totalActionCount + 1);
  newHypo->m_total_log_prob = m_total_log_prob + act_log_prob;
  return newHypo;
}

const ShiftReduceHypothesis*
ShiftReduceHypothesis::Combine(size_t index, const SuperCat *superCat,
                               size_t unaryActionCount, const double totalScore,
                               const size_t action_id, const double action_score,
                               Expression &act_log_prob) const {
  assert(superCat);
  size_t stackSize = m_stackSize - 1;
  size_t totalActionCount = m_totalActionCount + 1;
  ShiftReduceHypothesis *newHypo
  = new ShiftReduceHypothesis(superCat, this, m_prevStack->GetPrvStack(),
      m_nextInputIndex, unaryActionCount, stackSize, totalScore, m_finished,
      COMBINE, totalActionCount, m_total_shift_count, action_id, action_score, index);
//  newHypo->m_c_stack_pointers.reserve(totalActionCount + 1);
//  newHypo->m_w_stack_pointers.reserve(totalActionCount + 1);
//  newHypo->m_s_stack_pointers.reserve(totalActionCount + 1);
//  newHypo->m_a_stack_pointers.reserve(totalActionCount + 1);
//  newHypo->m_p_stack_pointers.reserve(totalActionCount + 1);
  newHypo->m_total_log_prob = m_total_log_prob + act_log_prob;
  return newHypo;
}


/*void ShiftReduceHypothesis::Finish(size_t index, ShiftReduceLattice *lattice)
{
    m_finished = true;
    lattice->Add(index, this);
}*/


} }



