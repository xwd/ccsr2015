// C&C NLP tools
// Copyright (c) Universities of Edinburgh, Oxford and Sydney
// Copyright (c) James R. Curran
//
// This software is covered by a non-commercial use licence.
// See LICENCE.txt for the full text of the licence.
//
// If LICENCE.txt is not included in this distribution
// please email candc@it.usyd.edu.au to obtain a copy.

#include "parser/ShiftReduceHypothesis.h"
#include "parser/ShiftReduceHypothesisQueue.h"
#include <stdlib.h>
#include <fstream>
#include "lstm_parser/_lstm_parser.h"
#include "lstm_parser/context_load.h"
#include <time.h>
#include <thread>
#include <functional>

//#include "cnn/nodes.h"
//#include "cnn/cnn.h"
//#include "cnn/training.h"
//#include "cnn/timing.h"
//#include "cnn/rnn.h"
//#include "cnn/gru.h"
//#include "cnn/lstm.h"
//#include "cnn/dict.h"
//#include "cnn/expr.h"

namespace NLP { namespace CCG {

using namespace std;
using namespace NLP::Tree;
using namespace cnn;
using namespace cnn::expr;

//Pool *Parser::s_shiftReduceFeaturePool(new Pool(1 << 20));

LSTMParser::LSTMConfig::LSTMConfig(const OpPath *base, const std::string &name, const std::string &desc)
: Directory(name, desc, base),
  cats(*this, "cats", "parser category directory", "//cats", &path),
  markedup(*this, "markedup", "parser markedup file", "//markedup", &cats),
  weights(*this, "weights", "parser model weights file", "//weights", &path),
  rules(*this, "rules", "parser rules file", "//rules", &path),
  maxwords(*this, SPACE, "maxwords", "maximum sentence length the parser will accept", 250),
  maxsupercats(*this, "maxsupercats", "maximum number of supercats before the parse explodes", 300000),
  alt_markedup(*this, SPACE, "alt_markedup", "use the alternative markedup categories (marked with !)", false),
  seen_rules(*this, "seen_rules", "only accept category combinations seen in the training data", false),
  extra_rules(*this, "extra_rules", "use additional punctuation and unary type-changing rules", true),
  question_rules(*this, "question_rules", "activate the unary rules that only apply to questions", false),
  eisner_nf(*this, "eisner_nf", "only accept composition when the Eisner (1996) constraints are met", false),
  partial_gold(*this, SPACE, "partial_gold", "used for generating feature forests for training", false),
  beam(*this, "beam_ratio", "(not fully tested)", 0.0),
  allowFrag(*this, "allowFrag", "allow fragmented output", true),
  de(*this, SPACE, "dim_emb", "word embedding dimension", 100),
  dp(*this, SPACE, "dim_suf", "pos embedding dimension", 50),
  dsuper(*this, SPACE, "dim_super", "supercat embedding dimension", 50),
  daction(*this, SPACE, "dim_action", "action embedding dimension", 50),
  nh(*this, SPACE, "n_hidden", "lstm hidden layer size", 128),
  th(*this, SPACE, "t_hidden", "target hidden layer size", 80),
  layers(*this, SPACE, "layers", "lstm layers", 1),
  head_limit(*this, SPACE, "head_limit", "max number of heads per feature", 1),
  pdrop(*this, "pdrop", "pdrop", 0.3),
  //lr(*this, "lr", "learning rate", 0.01),
  use_super(*this, "use_super", "use supercat embeddings", false),
  use_act(*this, "use_act", "use action embeddings", false),
  use_pos(*this, "use_pos", "use pos embeddings", false),
  use_biqueue(*this, "use_biqueue", "bidirectional queue", false),
  use_comp(*this, "use_comp", "compose word, super, action and pos", false),
  feeding(*this, "feeding", "feed hidden states from pos to action to super to word stacks", false),
  att(*this, "att", "use attention", false),
  att_all(*this, "att_all", "use attention on all features", false),
  att_prev_context(*this, "att_prev_context", "att_all() + attend on previous att context", false),
  use_rnn_super(*this, "use_rnn_super", "use rnn supertagger instead of lstm", false),
  word_emb_file(*this, "word_emb_file", "word embedding file to use", "emb_turian_100_scaled"),
  pos_emb_file(*this, "pos_emb_file", "word2vec pre-trained pos emb file", "wsj0221+nanc+wiki1st3.model.pos_emb"),
  super_emb_file(*this, "super_emb_file", "word2vec pre-trained super emb file", "wsj0221+nanc+wiki1.model.super_emb"),
  xf1_super(*this, "xf1_super", "xf1_super training", false),
  load_pretrained(*this, "load_pretrained", "enable this only if training from scratch", false),
  xf1_dropout(*this, "xf1_dropout", "dropout when doing xF1 training", false) {}
  //rnn_model_base_dir(*this, "rnn_model_dir", "path to save trained rnn model", "./") {}

void
LSTMParser::_Impl::_load_rules(const std::string &filename){
  cerr << "loading rules: " << filename << endl;
  ulong nlines = 0;
  if(filename == "")
    return;

  ifstream in(filename.c_str());
  if(!in)
    throw NLP::IOException("could not open rules file", filename);

  comment += read_preface(filename, in, nlines);

  string cat1str, cat2str;
  while(in >> cat1str >> cat2str){
    try {
      const Cat *cat1 = cats.canonize(cat1str.c_str());
      const Cat *cat2 = cats.canonize(cat2str.c_str());

      rule_instances.insert(cat1, cat2);
    }catch(NLP::Exception e){
      throw NLP::IOException("error parsing category in rule instantiation", filename);
    }
  }
  cerr << "total binary rules: " << rule_instances.size() << endl;
}

void
LSTMParser::_Impl::_load_features(const std::string &filename){

  //std::cerr << "_load_features in parser.cc" << std::endl;
  ulong nlines = 0;
  ifstream in(filename.c_str());
  if(!in)
    NLP::IOException("could not open features file", filename);

  comment += read_preface(filename, in, nlines);

  nfeatures = 1;
  string tmp;
  ulong freq;
  for(char c; in >> freq >> c; ++nfeatures){
    switch(c){
    case 'a': cat_feats.load(in, filename, nfeatures, LEX_WORD); break;
    case 'b': cat_feats.load(in, filename, nfeatures, LEX_POS); break;
    case 'c': cat_feats.load(in, filename, nfeatures, ROOT); break;
    case 'd': cat_feats.load(in, filename, nfeatures, ROOT_WORD); break;
    case 'e': cat_feats.load(in, filename, nfeatures, ROOT_POS); break;

    case 'f': dep_feats.load(in, filename, nfeatures, DEP_WORD); break;
    case 'g': dep_feats.load(in, filename, nfeatures, DEP_POS); break;
    case 'h': dep_feats.load(in, filename, nfeatures, DEP_WORD_POS); break;
    case 'i': dep_feats.load(in, filename, nfeatures, DEP_POS_WORD); break;

    case 'x': genrule_feats.load(in, filename, nfeatures, GEN_RULE); break;
    case 'm': rule_feats.load(in, filename, nfeatures, URULE); break;
    case 'n': rule_feats.load(in, filename, nfeatures, BRULE); break;

    case 'p': rule_head_feats.load(in, filename, nfeatures, URULE_HEAD); break;
    case 'q': rule_head_feats.load(in, filename, nfeatures, BRULE_HEAD); break;
    case 'r': rule_head_feats.load(in, filename, nfeatures, URULE_POS); break;
    case 's': rule_head_feats.load(in, filename, nfeatures, BRULE_POS); break;

    case 't': rule_dep_feats.load(in, filename, nfeatures, BRULE_HEAD_HEAD); break;
    case 'u': rule_dep_feats.load(in, filename, nfeatures, BRULE_POS_HEAD); break;
    case 'v': rule_dep_feats.load(in, filename, nfeatures, BRULE_HEAD_POS); break;
    case 'w': rule_dep_feats.load(in, filename, nfeatures, BRULE_POS_POS); break;

    case 'F': rule_dep_dist_feats.load(in, filename, nfeatures, DIST_ADJ_HEAD); break;
    case 'G': rule_dep_dist_feats.load(in, filename, nfeatures, DIST_VERBS_HEAD); break;
    case 'H': rule_dep_dist_feats.load(in, filename, nfeatures, DIST_PUNCT_HEAD); break;
    case 'I': rule_dep_dist_feats.load(in, filename, nfeatures, DIST_ADJ_POS); break;
    case 'J': rule_dep_dist_feats.load(in, filename, nfeatures, DIST_VERBS_POS); break;
    case 'K': rule_dep_dist_feats.load(in, filename, nfeatures, DIST_PUNCT_POS); break;

    case 'L': dep_dist_feats.load(in, filename, nfeatures, DIST_ADJ_HEAD); break;
    case 'M': dep_dist_feats.load(in, filename, nfeatures, DIST_VERBS_HEAD); break;
    case 'N': dep_dist_feats.load(in, filename, nfeatures, DIST_PUNCT_HEAD); break;
    case 'P': dep_dist_feats.load(in, filename, nfeatures, DIST_ADJ_POS); break;
    case 'Q': dep_dist_feats.load(in, filename, nfeatures, DIST_VERBS_POS); break;
    case 'R': dep_dist_feats.load(in, filename, nfeatures, DIST_PUNCT_POS); break;

    default:
      throw NLP::IOException("unexpected feature type in load features", filename, nfeatures);
    }
  }
  --nfeatures;
}

void
LSTMParser::_Impl::load_lex_cat_dict(const string &raw_cats_from_markedup) {
  cerr << "load_lex_cat_dict..." << endl;
  ifstream in(raw_cats_from_markedup.c_str());

  if (!in) {
    cerr << "no such file: " << raw_cats_from_markedup << endl;
    exit (EXIT_FAILURE);
  }

  string s;
  while (getline(in, s)) {
    istringstream ss(s);
    string cat_str;
    unsigned ind = 0;
    if (!(ss >> cat_str >> ind)) {
      cerr << "somthing is wrong in: " << raw_cats_from_markedup << endl;
      exit (EXIT_FAILURE);
      break;
    }
    int ind_in_markedup = cats.markedup.index(cat_str);
    assert(ind_in_markedup != -1);
    assert(m_lex_cat_ind_map.insert(make_pair(cat_str, ind)).second);
    assert(m_lex_ind_cat_map.insert(make_pair(ind, cat_str)).second);
  }
  cerr << "number of raw markedup cats in markedup_raw_ind_dict file: "
      << m_lex_cat_ind_map.size() << endl;
  cerr << "number of raw markedup cats in cats.markedup: "
      << cats.plain_markedup_count << endl;
  assert(m_lex_cat_ind_map.size() == 580);
  assert(m_lex_cat_ind_map.size() == cats.plain_markedup_count);
  assert(m_lex_ind_cat_map.size() == m_lex_cat_ind_map.size());
}

LSTMParser::_Impl::_Impl(cnn::Model &model, cnn::Model &t_model,
                         const LSTMConfig &cfg,
                         Sentence &sent,
                         Categories &cats,
                         const unsigned srbeam,
                         const string &config_dir,
                         const unsigned &train,
                         const bool &integrate)
: cfg(cfg), sent(sent),
  nsentences(0),
  cats(cats),
  lexicon("lexicon", cfg.lexicon()),
  cat_feats(cats, lexicon),
  rule_feats(cats),
  rule_head_feats(cats, lexicon),
  rule_dep_feats(cats, lexicon),
  rule_dep_dist_feats(cats, lexicon),
  dep_feats(cats, lexicon),
  dep_dist_feats(cats, lexicon),
  genrule_feats(cats),
  weights(0),
  chart(cats, cfg.extra_rules(), cfg.maxwords()),
  rules(chart.pool, cats.markedup, cfg.extra_rules(), rule_instances),

  NP(cats.markedup["NP"]), NbN(cats.markedup["N\\N"]), NPbNP(cats.markedup["NP\\NP"]),
  SbS(cats.markedup["S\\S"]), SfS(cats.markedup["S/S"]),
  SbNPbSbNP(cats.markedup["(S\\NP)\\(S\\NP)"]),
  SbNPfSbNP(cats.markedup["(S\\NP)/(S\\NP)"]), SfSbSfS(cats.markedup["(S/S)\\(S/S)"]),
  SbNPbSbNPbSbNPbSbNP(cats.markedup["((S\\NP)\\(S\\NP))\\((S\\NP)\\(S\\NP))"]),
  NPfNPbNP(cats.markedup["NP/(NP\\NP)"]),

  //m_shiftReduceFeatures(new ShiftReduceFeature(cats, lexicon)),
  m_allowFragTree(cfg.allowFrag()),
  //m_allowFragAndComplete(false),

  m_hasGoldAction(false),
  m_hasOutsideRule(false),
  m_lattice(0),
  m_beamSize(srbeam),
  m_train(train),
  m_dcorr(0.0),
  m_dtags(0.0),
  // m_cat_ind is initialized to 1, it gets
  // incremented for the very first seen cat
  // whose ind will be 2, and the actual 0th
  // column in the cat embedding matrix is reserved
  // for the empty cat feature, and the actual 1st
  // reserved for unk: in context_load.h
  // const static unsigned empty_supercat_feat_ind = 0;
  // const static unsigned unk_supercat_ind = 1;
  //m_cat_ind(1),
  // this gets incremented for the first phrase emb, 0th is for empty feat
  m_total_reduce_count(0) {
  cerr << "parser constructor parser.cc" << endl;
  cerr << "allowFragTree: " << m_allowFragTree << endl;
  cerr << "SR beam size: " << m_beamSize << endl;
  cerr << "is training: " << train << endl;
  cerr << "eisner: " << cfg.eisner_nf() << endl;

  m_queue_l2rbuilder = 0;
  m_queue_r2lbuilder = 0;
  m_w_stackbuilder = 0;
  m_s_stackbuilder = 0;
  m_a_stackbuilder = 0;
  m_p_stackbuilder = 0;
  m_c_stackbuilder = 0;

  if(cfg.seen_rules() && cfg.eisner_nf()) {
    string path = cfg.path() + string("//rules_nb_num_et");
    _load_rules(path);
  }

  if (cfg.seen_rules() && !cfg.eisner_nf()) {
    string path = cfg.path() + string("//rules_nb_num_ef");
    _load_rules(path);
  }

  sent.words.reserve(cfg.maxwords());
  sent.pos.reserve(cfg.maxwords());
  sent.msuper.reserve(cfg.maxwords());

  // rnn related parameters

  m_nclasses = 608;
  m_vocsize = 0;
  // there are 580 (0 - 579) lexical categoreis in total
  // unary rule ids go from 1 to 18, so unary rule
  // id 1 will get the output class unary_base_count + 1
  // similarly for reduce
  m_unary_base_count = 579;
  m_reduce_base_count = 597;

  // at both training and test time, load the emb_file (word, pos etc.)
  // to get the mapping from str to ind

  // word embeddings
  ifstream emb_file(config_dir + "/emb_turian_100_scaled");
  if (!emb_file) {
    cerr << "no such file " << config_dir << "/emb_turian_100_scaled" << endl;
    exit (EXIT_FAILURE);
  }
  try {
    load_emb_mat(config_dir + "/emb_turian_100_scaled"); //todo, remove hardcoded file
  } catch (runtime_error e) {
    cerr << e.what() << endl;
    exit(EXIT_FAILURE);
  }
  p_w = model.add_lookup_parameters(m_vocsize, {cfg.de()});
  if (cfg.load_pretrained()) {
    for (auto it : m_ind_pretrained_emb_map)
      p_w->Initialize(it.first, it.second);
  }
  unordered_map<string, unsigned>::const_iterator iter = m_full_wordstr_emb_map.find("***PADDING***");
  assert(iter != m_full_wordstr_emb_map.end());
  m_UNK_ALPHNUM_LOWER_IND = m_vocsize - 1;
  m_UNK_ALPHNUM_UPPER_IND = m_vocsize - 2;
  m_UNK_NON_ALPHNUM_IND = m_vocsize - 3;
  m_EMPTY_WORD_IND = m_vocsize - 4; // for empty head
  m_PAD_WORD_IND = 0; // ***PADDING***

  // pos embeddings
  //if (cfg.use_pos()) {
    //load_pos_str_ind_map(config_dir + "/pos_str_ind_map.txt");
    const string word2vec_pos_emb = config_dir + "/" + cfg.pos_emb_file();
    load_pos_emb_mat(word2vec_pos_emb);
    p_pos = model.add_lookup_parameters(m_total_pos, {cfg.dp()});
    if (cfg.load_pretrained()) {
      for (auto it : m_ind_pos_emb_map)
        p_pos->Initialize(it.first, it.second);
    }
    // last two cols are padding and unk
    m_EMPTY_POS_IND = m_total_pos - 3;
    m_PAD_POS_IND = m_total_pos - 2;
    m_UNK_POS_IND = m_total_pos - 1;
    cerr << "m_EMPTY_POS_IND " << m_EMPTY_POS_IND << endl;
    cerr << "m_PAD_POS_IND " << m_PAD_POS_IND << endl;
    cerr << "m_UNK_POS_IND " << m_UNK_POS_IND << endl;
  //}

  // supercat embeddings
  //if (cfg.use_super()) {
    // ## load_supercat_map first, then load_super_emb_mat (not the other way around)
    // for the very first training epoch, load label_ind.txt (the same file as used for rnn_super, except with
    // NNOONNEE cat removed)
    // and this fixes the order of the lex supercats in m_cats_map (rnn_parser.cc) for subsequent
    // training epochs (this is different from markedup_raw_ind, which is only used for finding feasilbe actions)
    p_super = model.add_lookup_parameters(5000, {cfg.dsuper()}); //todo: 5000 is hardcoded
    if (cfg.load_pretrained()) {
      load_supercat_map(config_dir + "/label_ind.txt", true);
      const string word2vec_super_emb = config_dir + "/" + cfg.super_emb_file();
      // also for the very first epoch only -- load pre-trained super embeddings
      // in subsequent epochs, directly load the dumped matrix
      load_super_emb_mat(word2vec_super_emb);
      for (auto it : m_ind_super_emb_map)
        p_super->Initialize(it.first, it.second);
      assert(m_ind_super_emb_map.size() != 0);
      assert(m_ind_super_emb_map.find(m_ind_super_emb_map.size() + 1) != m_ind_super_emb_map.end());
      assert(m_ind_super_emb_map.find(m_ind_super_emb_map.size() + 2) == m_ind_super_emb_map.end());
    }
    m_PAD_SUPER_IND = 0;
    m_UNK_SUPER_IND = 1;
  //}

  // action embeddings
  //if (cfg.use_act()) {
    p_action = model.add_lookup_parameters(4, {cfg.daction()});
    m_SHIFT_IND = 0;
    m_UNARY_IND = 1;
    m_REDUCE_IND = 2;
    m_PAD_ACTION_IND = 3;
  //}

  unsigned acth_dim = cfg.nh();
  if (cfg.use_super() && cfg.use_act() && cfg.use_pos()) {
    acth_dim = 4*cfg.nh();
  } else if (cfg.use_super() && cfg.use_act() && !cfg.use_pos()) {
    acth_dim = 3*cfg.nh();
  } else if (cfg.use_super() && !(cfg.use_act()) && !cfg.use_pos()) {
    acth_dim = 2*cfg.nh();
  }

  if (cfg.use_biqueue()) acth_dim += 2*cfg.nh();
  else acth_dim += cfg.nh();
  if (cfg.use_comp() || cfg.att()) acth_dim += cfg.nh();
  if (cfg.att_all()) acth_dim += 4*cfg.nh();
  p_acth = model.add_parameters({cfg.th(), acth_dim});
  p_acth_bias = model.add_parameters({cfg.th()});
  p_act = model.add_parameters({m_nclasses, cfg.th()});
  p_act_bias = model.add_parameters({m_nclasses});

  //if (cfg.use_comp() && cfg.att()) {
  if (cfg.att())
    p_att = model.add_parameters({1, acth_dim}); //w,s,a,p,c, att only at c_stackbuilder
  // already trained a model with p_att, leave it here for now in order to load the model
  //}

  if (cfg.att_all()) {
    assert(!cfg.use_comp() && !cfg.att());
    // att all feats
    p_att_trans_w = model.add_parameters({cfg.nh(), cfg.nh()});
    p_att_trans_s = model.add_parameters({cfg.nh(), cfg.nh()});
    p_att_trans_a = model.add_parameters({cfg.nh(), cfg.nh()});
    p_att_trans_p = model.add_parameters({cfg.nh(), cfg.nh()});
    p_att_trans_q = model.add_parameters({cfg.nh(), 2*cfg.nh()});
    // att weights
    //unsigned att_in_dim = cfg.nh() + 2*cfg.nh(); // todo assumes biqueue
    p_att_w = model.add_parameters({1, cfg.nh()});
    p_att_s = model.add_parameters({1, cfg.nh()});
    p_att_a = model.add_parameters({1, cfg.nh()});
    p_att_p = model.add_parameters({1, cfg.nh()});
    p_att_c = model.add_parameters({1, cfg.nh()});
  }

  if (cfg.att_prev_context()) {
    assert(cfg.att_all() && !cfg.use_comp() && !cfg.att());
    p_w_att_trans_prev_context = model.add_parameters({cfg.nh(), cfg.nh()});
    p_w_att_trans_prev_context2 = model.add_parameters({cfg.nh(), cfg.nh()});
    p_s_att_trans_prev_context = model.add_parameters({cfg.nh(), cfg.nh()});
    p_s_att_trans_prev_context2 = model.add_parameters({cfg.nh(), cfg.nh()});
    p_a_att_trans_prev_context = model.add_parameters({cfg.nh(), cfg.nh()});
    p_a_att_trans_prev_context2 = model.add_parameters({cfg.nh(), cfg.nh()});
    p_p_att_trans_prev_context = model.add_parameters({cfg.nh(), cfg.nh()});
    p_p_att_trans_prev_context2 = model.add_parameters({cfg.nh(), cfg.nh()});
  }

  m_config_dir = config_dir;
  cerr << "parser _Impl constructor in parser.cc done" << endl;
  cerr << "lexicon size: " << lexicon.size() << endl;

  // tagger related
  load_supercat_map(config_dir + "/label_ind_with_nnoonnee.txt", false, true);
  cerr << "tagger m_t_cats_map_str.size(): " << m_t_cats_map_str.size() << endl;
  m_T_UNKNWON_SUPERCAT_IND = m_t_cats_map_str.find("NNOONNEE")->second;
  assert(m_T_UNKNWON_SUPERCAT_IND == 425);
  m_t_tag_size = m_t_cats_map_str.size();
  cerr << "total number of lex cat classes for tagger (including NNOONNEE): " << m_t_cats_map_str.size() << endl;

  load_suf_str_ind_map(config_dir + "/suf.txt");

  m_T_UNK_ALPHNUM_LOWER_IND = m_t_vocab_size - 1;
  m_T_UNK_ALPHNUM_UPPER_IND = m_t_vocab_size - 2;
  m_T_UNK_NON_ALPHNUM_IND = m_t_vocab_size - 3;
  m_T_UNK_SUFFIX_IND = m_suf_str_ind_map.size();

  const unsigned pretrained_dim = 100;
  const unsigned suf_dim = 20;
  const unsigned tag_hidden_dim = 200;
  const unsigned hidden_dim = 256;
  const double tpdrop = 0.3;
  const bool att = true;
  m_tpdrop = tpdrop;

  cnn::Model &tagger_model = t_model;

  pt_w = tagger_model.add_lookup_parameters(m_t_vocab_size, {pretrained_dim});
  pt_suf = tagger_model.add_lookup_parameters(m_suf_str_ind_map.size() + 1, {suf_dim});
  //  for (auto it : m_ind_pretrained_emb_map)
  //    pt_w->Initialize(it.first, it.second);
  if (att) {
    pt_l2ah = tagger_model.add_parameters({tag_hidden_dim, hidden_dim}); // l 2 att hidden
    pt_r2ah = tagger_model.add_parameters({tag_hidden_dim, hidden_dim}); // r 2 att hidden
    pt_c2ah = tagger_model.add_parameters({tag_hidden_dim, hidden_dim*2}); // context 2 att hidden
    pt_tbias = tagger_model.add_parameters({tag_hidden_dim});
    pt_th2t = tagger_model.add_parameters({m_t_tag_size, tag_hidden_dim});
    pt_att_w = tagger_model.add_parameters({1, hidden_dim*2*2}); // two directions and two places (target position plus another position)
    pt_l2th = tagger_model.add_parameters({m_t_tag_size, hidden_dim});
    pt_r2th = tagger_model.add_parameters({m_t_tag_size, hidden_dim});
    pt_c2t = tagger_model.add_parameters({m_t_tag_size, hidden_dim*2});
  } else {
    pt_l2th = tagger_model.add_parameters({m_t_tag_size, hidden_dim});
    pt_r2th = tagger_model.add_parameters({m_t_tag_size, hidden_dim});
  }
  pt_thbias = tagger_model.add_parameters({m_t_tag_size});
}

void
LSTMParser::_Impl::load_suf_str_ind_map(const string &file) {

  cerr << "loading suf str ind map\n";
  ifstream in(file.c_str());
  if(!in) {
    cerr << "could not open suf_str_ind file " << file << endl;
    exit(EXIT_FAILURE);
  }
  string suf;
  size_t temp;
  size_t total = 0;
  while (in >> suf >> temp) {
    m_suf_str_ind_map.insert(make_pair(suf, total));
    ++total;
  }
  cerr << "total suffix count (from sec0221): " << total << endl;
}

void
LSTMParser::_Impl::load_emb_mat(const string &filename) {
  cerr << "loading emb mat..., " << filename << endl;
  ifstream in(filename.c_str());
  if (!in) {
    cerr << "no such file: " << filename;
    exit (EXIT_FAILURE);
  }

  unsigned total = 0;
  string line;
  while (in) {
    if (!getline(in, line)) break;
    istringstream iss(line);
    unsigned first_space_pos = line.find(" ");
    string word = line.substr(0, first_space_pos);
    string emb_vec = line.substr(first_space_pos + 1);
    vector<float> v(cfg.de(), 0);
    istringstream lin(emb_vec);
    for (unsigned i = 0; i < cfg.de(); ++i)
      lin >> v[i];
    if (total == 0)
      assert(word == "***PADDING***");
    assert(m_full_wordstr_emb_map.insert(make_pair(word, total)).second);
    assert(m_ind_pretrained_emb_map.insert(make_pair(total, v)).second);
    ++total;
  }

  std::mt19937_64 engine;
  std::uniform_real_distribution<double> uni(-1.0, 1.0);
  auto gen = std::bind(uni, engine);
  for (unsigned i = 0; i < 4; ++i) { //todo: 4 is hardcoded
    vector<float> vec(cfg.de());
    generate(begin(vec), end(vec), gen);
    assert(m_ind_pretrained_emb_map.insert(make_pair(total + i, vec)).second);
  }

  m_vocsize = total + 4; //todo: 4 is hardcoded
  m_t_vocab_size = total + 3; // for tagger
  cerr << "total emb (no unks) " << total << endl;
}

void
LSTMParser::_Impl::load_pos_emb_mat(const string &filename) {
  cerr << "loading pos emb mat..., " << filename << endl;
  ifstream in(filename.c_str());
  if (!in)
    throw runtime_error("no such file: " + filename);

  string line;
  unsigned total = 0;
  while (in) {
    if (!getline(in, line)) break;
    istringstream iss(line);
    unsigned first_space_pos = line.find(" ");
    string pos = line.substr(0, first_space_pos);
    string emb_vec = line.substr(first_space_pos + 1); // get till the end
    vector<float> v(cfg.dp(), 0);
    istringstream lin(emb_vec);
    for (unsigned i = 0; i < cfg.dp(); ++i)
      lin >> v[i];
    assert(m_pos_str_ind_map.insert(make_pair(pos, total)).second);
    assert(m_ind_pos_emb_map.insert(make_pair(total, v)).second);
    ++total;
  }

  std::mt19937_64 engine;
  std::uniform_real_distribution<double> uni(-1.0, 1.0);
  auto gen = std::bind(uni, engine);
  for (unsigned i = 0; i < 3; ++i) { //todo: 3 hardcoded
    vector<float> vec(cfg.dp());
    generate(begin(vec), end(vec), gen);
    assert(m_ind_pos_emb_map.insert(make_pair(total + i, vec)).second);
  }

  m_total_pos = total + 3; //todo: 3 hardcoded
  cerr << "total pos emb (+ empty, padding and unk): " << m_total_pos  << endl;
}

void
LSTMParser::_Impl::load_super_emb_mat(const string &filename) {
  cerr << "loading super emb mat..., " << filename << endl;
  ifstream in(filename.c_str());
  if (!in)
    throw runtime_error("no such file: " + filename);

  string line;
  unsigned total = 1;
  while (in) {
    if (!getline(in, line)) break;
    istringstream iss(line);

    ++total;
    unsigned first_space_pos = line.find(" ");
    string super = line.substr(0, first_space_pos);
    string emb_vec = line.substr(first_space_pos + 1); // get till the end

    //const Cat *cat = cats.markedup[super];
    if (super.find_last_of("\\") != string::npos || super.find_last_of("/") != string::npos) {
      //cerr << cat << endl;
      super.insert(0, 1, '(');
      super.append(")");
      //cerr << cat << endl;
    }
    //cerr << super << " " << m_cats_map_str.find(super)->second << endl;
    //cerr << total << endl;
    assert(m_cats_map_str.find(super)->second == total);
    vector<float> v(cfg.dp(), 0);
    istringstream lin(emb_vec);
    for (unsigned i = 0; i < cfg.dp(); ++i)
      lin >> v[i];
    assert(m_ind_super_emb_map.insert({total, v}).second);
  }
  //m_total_super = total + 1;
  cerr << "total super emb (no pad and unk): " << total - 1 << endl;
}

void LSTMParser::_Impl::Clear() {
  chart.reset();
  chart.pool->clear();
  sent.reset();
  if (m_train != 2) delete m_lattice;
  if (m_train) {
    //delete m_shiftReduceFeatures;
    //m_shiftReduceFeatures = new ShiftReduceFeature(cats, lexicon);
  }
}

void LSTMParser::_Impl::clear_xf1()
{
  sent.reset();
  chart.pool->clear();
  delete m_lattice;
}

void LSTMParser::_Impl::load_gold_trees(const std::string &filename) {
  std::cerr << "Parser::_Impl::load_gold_trees start" << std::endl;
  unsigned treeId = 0;

  ifstream in(filename.c_str());
  if(!in)
    throw runtime_error("no such file: " + filename);

  vector<ShiftReduceAction*> *tree = new vector<ShiftReduceAction*>;
  while (in) {
    string s;
    if (!getline(in, s)) break;
    if (s == "###") {
      if (tree->size() > 0)
        m_goldTreeVec.push_back(tree);
      ++treeId;
      tree = new vector<ShiftReduceAction*>;
      continue;
    }
    istringstream ss(s);
    vector<string> node;
    while (ss) {
      string s;
      if (!getline(ss, s, ' ')) break;
      node.push_back(s);
    }

    const Cat *cat = cats.markedup[node.at(0)];
    if(!cat) {
      try {
        cat = cats.canonize(node.at(0).c_str());
      } catch(NLP::Exception e) {
        cerr << "parse cat failed: " << node.at(0) << endl;
        throw NLP::IOException("attempting to parse category failed for non-SHIFT");
      }
    }

    assert(cat);
    ShiftReduceAction *action = new ShiftReduceAction(atoi(node.at(1).c_str()), cat);
    tree->push_back(action);
  }
  if (tree->size() > 0) m_goldTreeVec.push_back(tree);
  cerr << "total number of gold tress: " << m_goldTreeVec.size() << endl;
}

void
LSTMParser::_Impl::load_gold_trees_rnn_fmt(const std::string &filename) {
  std::cerr << "Parser::_Impl::load_gold_trees_rnn_fmt..." << std::endl;
  unsigned treeId = 0;

  ifstream in(filename.c_str());
  if (!in)
    throw runtime_error("no such file: " + filename);

  vector<pair<unsigned, unsigned>> tree;
  while (in)
  {
    string s;
    if (!getline(in, s)) break;
    if (s == "###")
    {
      if (treeId > 0)
        assert(tree.size() != 0);
      if (tree.size() > 0)
        m_gold_tree_vec_rnn.push_back(tree);
      ++treeId;
      tree.clear();
      continue;
    }

    istringstream ss(s);
    vector<string> node;
    while (ss)
    {
      string s;
      if (!getline(ss, s, ' ')) break;
      node.push_back(s);
    }

    int action_id = atoi(node.at(1).c_str());
    int res_cat_id = 0; // for all reduce actions, this will be zero
    if (action_id != 2) {
      assert(action_id == 0 || action_id == 1);
      res_cat_id = atoi(node.at(0).c_str());
    }

    tree.push_back(make_pair((unsigned)action_id, (unsigned)res_cat_id));
  }

  if (tree.size() > 0)
    m_gold_tree_vec_rnn.push_back(tree);
  cerr << "total number of gold trees rnn format: " << m_gold_tree_vec_rnn.size() << endl;

  //  // unit test
  //
  //  for (unsigned i = 0; i < m_gold_tree_vec_rnn.size(); ++i) {
  //    for (unsigned j = 0; j < m_gold_tree_vec_rnn[i].size(); ++j) {
  //      cerr << m_gold_tree_vec_rnn[i][j].first << " " << m_gold_tree_vec_rnn[i][j].second << endl;
  //    }
  //    cerr << endl;
  //  }
}

void LSTMParser::_Impl::construct_hypo(ComputationGraph &cg,
                                       const vector<unsigned> &word_inds,
                                       const vector<unsigned> &pos_inds,
                                       HypothesisPQueue &hypoQueue,
                                       const ShiftReduceHypothesis *&bestHypo, const ShiftReduceHypothesis *&goldHypo,
                                       ShiftReduceAction *goldAction, const bool goldFinished) {
  if (m_train == 2) m_hasGoldAction = false;
  unsigned size = hypoQueue.size() > m_beamSize ? m_beamSize : hypoQueue.size();

  for (unsigned beam = 0; beam < size; ++beam) {
    HypothesisTuple *tuple = hypoQueue.top();
    const ShiftReduceHypothesis *hypo = tuple->GetCurrentHypo();
    assert(hypo);
    double totalScore = tuple->GetHypoTotalScore();
    unsigned actionId = tuple->GetAction()->GetAction();
    const SuperCat *superCat = tuple->GetAction()->GetSuperCat();
    ShiftReduceAction action(actionId, superCat);

    _action(cg, hypo, actionId, superCat, totalScore, word_inds, pos_inds, tuple->m_act_log_prob, tuple->m_action_id, tuple->m_action_score);

    if (m_train == 2 && !goldFinished) {
      const ShiftReduceHypothesis *newHypo = m_lattice->GetLatticeArray(m_lattice->GetEndIndex());
      assert(newHypo);
      if (bestHypo == 0 || newHypo->GetTotalScore() > bestHypo->GetTotalScore()) {
        assert(m_lattice->GetEndIndex() != 0);
        bestHypo = newHypo;
        assert(bestHypo != 0);
      }
      if (hypo == goldHypo && action == *goldAction) {
        goldHypo = newHypo;
        m_hasGoldAction = true;
      }
    }
    hypoQueue.pop();
    delete tuple;
  } // end for

  // make this into a function of the PQ class?
  if (!hypoQueue.empty())
    assert(size == m_beamSize);
  else
    assert(size <= m_beamSize);

  while(!hypoQueue.empty())
  {
    HypothesisTuple *tuple = hypoQueue.top();
    hypoQueue.pop();
    delete tuple;
  }
  assert(hypoQueue.size() == 0);
}

void LSTMParser::_Impl::_action(ComputationGraph &cg,
                                const ShiftReduceHypothesis *&hypo,
                                unsigned action,
                                const SuperCat *supercat, const double totalScore,
                                const vector<unsigned> &word_inds,
                                const vector<unsigned> &pos_inds,
                                Expression &act_log_prob,
                                const unsigned action_id,
                                const double action_score) {
  assert(supercat);
  assert(hypo);
  switch (action) {
  case SHIFT:
    m_lattice->Add(m_lattice->GetEndIndex() + 1, hypo->Shift(m_lattice->GetEndIndex() + 1, supercat,
                   hypo->GetUnaryActionCount(), totalScore, action_id, action_score, act_log_prob));
    m_lattice->IncrementEndIndex();
    break;
  case UNARY:
    m_lattice->Add(m_lattice->GetEndIndex() + 1, hypo->Unary(m_lattice->GetEndIndex() + 1, supercat,
                   hypo->GetUnaryActionCount() + 1, totalScore, action_id, action_score, act_log_prob));
    m_lattice->IncrementEndIndex();
    break;
  case COMBINE:
    m_lattice->Add(m_lattice->GetEndIndex() + 1, hypo->Combine(m_lattice->GetEndIndex() + 1, supercat,
                   hypo->GetUnaryActionCount(), totalScore, action_id, action_score, act_log_prob));
    m_lattice->IncrementEndIndex();
    break;
  }
  update_context(cg, hypo, action, supercat, word_inds, pos_inds);
}

void LSTMParser::_Impl::update_context(ComputationGraph &cg,
                                       const ShiftReduceHypothesis *&hypo,
                                       unsigned action,
                                       const SuperCat *&sc,
                                       const vector<unsigned> &word_inds,
                                       const vector<unsigned> &pos_inds) {
  ShiftReduceHypothesis *last = const_cast<ShiftReduceHypothesis *>(m_lattice->GetLatticeArray(m_lattice->GetEndIndex()));
  assert(last);
  cnn::RNNPointer cur_w_state = hypo->m_lstm_w_state;
  cnn::RNNPointer cur_s_state;
  cnn::RNNPointer cur_a_state;
  cnn::RNNPointer cur_p_state;
  cnn::RNNPointer cur_c_state;
  if (cfg.use_super()) { cur_s_state = hypo->m_lstm_s_state; }
  if (cfg.use_act()) { cur_a_state = hypo->m_lstm_a_state; }
  if (cfg.use_pos()) { cur_p_state = hypo->m_lstm_p_state; }
  if (cfg.use_comp()) { cur_c_state = hypo->m_c_stack_pointers.back(); }
  vector<unsigned> sw_vec;
  vector<unsigned> sp_vec;
  unsigned w_ind = 0;
  unsigned p_ind = 0;
  unsigned a_ind = 0;
  if (action == SHIFT) {
    w_ind = hypo->GetNextInputIndex();
    assert(hypo->GetNextInputIndex() < pos_inds.size());
    p_ind = pos_inds[hypo->GetNextInputIndex()];
    a_ind = m_SHIFT_IND;
  } else {
    assert(action == UNARY || action == COMBINE);
    get_values(true, &sc->vars[sc->cat->var], sw_vec, sp_vec, word_inds, pos_inds);
    assert(sw_vec.size() == sp_vec.size());
    if (sw_vec.size() > 0) {
      w_ind = sw_vec[0];
      p_ind = sp_vec[0];
    } else {
      w_ind = m_EMPTY_WORD_IND;
      p_ind = m_EMPTY_POS_IND;
    }
  }

  if (action == UNARY) a_ind = m_UNARY_IND;
  if (action == COMBINE) a_ind = m_REDUCE_IND;

  Expression s_back, a_back, p_back;
  if (cfg.use_pos()) {
    p_back = m_p_stackbuilder->add_input(cur_p_state, lookup(cg, p_pos, p_ind));
    last->update_p_state(m_p_stackbuilder->state());
  }
  if (cfg.use_act()) {
    Expression input;
    if (cfg.feeding()) input = concatenate({lookup(cg, p_action, a_ind), p_back});
    else  input = lookup(cg, p_action, a_ind);
    a_back = m_a_stackbuilder->add_input(cur_a_state, input);
    last->update_a_state(m_a_stackbuilder->state());
  }
  if (cfg.use_super()) {
    unsigned s_ind = canonize2ind(m_train, sc->cat, m_cats_map_str);
    Expression input;
    if (cfg.feeding())input = concatenate({lookup(cg, p_super, s_ind), a_back});
    else input = lookup(cg, p_super, s_ind);
    s_back = m_s_stackbuilder->add_input(cur_s_state, input);
    last->update_s_state(m_s_stackbuilder->state());
  }
  Expression w_input;
  if (cfg.feeding()) w_input = concatenate({lookup(cg, p_w, w_ind), s_back});
  else w_input = lookup(cg, p_w, w_ind);
  m_w_stackbuilder->add_input(cur_w_state, w_input);
  last->update_w_state(m_w_stackbuilder->state());

  if (cfg.use_comp()) {
//    last->m_c_stackbuilder->add_input(cur_c_state, concatenate({w_back, s_back, a_back, p_back}));
//    last->m_c_stack_pointers = hypo->m_c_stack_pointers;
//    assert(last->m_c_stack_pointers.size() == hypo->m_c_stack_pointers.size());
//    last->m_c_stack_pointers.emplace_back(last->m_c_stackbuilder->state());
  }

  if (cfg.att_all()) {
    last->m_w_stack_pointers = hypo->m_w_stack_pointers;
    last->m_w_stack_pointers.emplace_back(m_w_stackbuilder->state());
    last->m_s_stack_pointers = hypo->m_s_stack_pointers;
    last->m_s_stack_pointers.emplace_back(m_s_stackbuilder->state());
    last->m_a_stack_pointers = hypo->m_a_stack_pointers;
    last->m_a_stack_pointers.emplace_back(m_a_stackbuilder->state());
    last->m_p_stack_pointers = hypo->m_p_stack_pointers;
    last->m_p_stack_pointers.emplace_back(m_p_stackbuilder->state());
  }
}

void LSTMParser::_Impl::_add_lex(const Cat *cat, const SuperCat *sc,
                                 bool replace, RuleID ruleid,
                                 vector<SuperCat *> &tmp,
                                 const unsigned tc_id) {
  SuperCat *new_sc = SuperCat::LexRule(chart.pool, cat, SuperCat::LEX, sc, replace, ruleid);
  new_sc->SetLex();
  assert(new_sc->left != 0);
  new_sc->m_unary_id = tc_id;
  tmp.push_back(new_sc);
}

void LSTMParser::_Impl::unary_tc(const SuperCat *sc,
                                bool qu_parsing,
                                vector<SuperCat *> &tmp) {
  // tmp will be the union of sc and lex'ed sc
  //tmp.push_back(sc);

  const Cat *cat = sc->cat;

  //cerr << "unary lex: " << cat->out_novar_noX_noNB_SR_str() << endl;

  if(cat->is_N())
    _add_lex(NP, sc, false, 1, tmp, 2);
  else if(cfg.extra_rules.get_value() && cat->is_NP())
    _add_lex(NPfNPbNP, sc, false, 11, tmp, 4);
  else if(cat->is_SbNP()){
    RuleID ruleid = 0;
    switch(cat->res->feature){
    case Features::DCL:
      if(!cfg.extra_rules.get_value())
        //continue;
        break;
      ruleid = 12;
      break;
    case Features::PSS:
      if(cfg.extra_rules.get_value()){
        _add_lex(SbNPbSbNP, sc, false, 17, tmp, 6);
        _add_lex(SfS, sc, false, 13, tmp, 7);
      }
      if(qu_parsing)
        _add_lex(NbN, sc, true, 95, tmp);
      ruleid = 2;
      break;
    case Features::NG:
      _add_lex(SbNPbSbNP, sc, false, 4, tmp, 6);
      _add_lex(SfS, sc, false, 5, tmp, 7);
      if(cfg.extra_rules.get_value()){
        _add_lex(SbNPfSbNP, sc, false, 18, tmp, 5);
        _add_lex(SbS, sc, false, 16, tmp, 8);
        _add_lex(NP, sc, false, 20, tmp, 2);
      }
      ruleid = 3;
      break;
    case Features::ADJ:
      // given this a 93 id to match the gen_lex rule
      _add_lex(SbNPbSbNP, sc, false, 93, tmp, 6);
      if(cfg.extra_rules.get_value())
        _add_lex(SfS, sc, false, 15, tmp, 7);
      if(qu_parsing)
        _add_lex(NbN, sc, true, 94, tmp);
      ruleid = 6;
      break;
    case Features::TO:
      _add_lex(SbNPbSbNP, sc, false, 8, tmp, 6);
      _add_lex(NbN, sc, true, 9, tmp, 1);
      if(cfg.extra_rules.get_value())
        _add_lex(SfS, sc, false, 14, tmp, 7);
      ruleid = 7;
      break;
    default:
      break;
      //continue;
    }
    _add_lex(NPbNP, sc, true, ruleid, tmp, 3);
  }else if(cat->is_SfNP() && cat->res->has_dcl())
    _add_lex(NPbNP, sc, true, 10, tmp, 3);
  else if(cfg.extra_rules.get_value() && cat->is_StobNPfNP())
    _add_lex(NPbNP, sc, true, 19, tmp, 3);
  else if(cfg.extra_rules.get_value() && cat->is_Sdcl()){
    _add_lex(NPbNP, sc, false, 21, tmp, 3);
    _add_lex(SbS, sc, false, 22, tmp, 8);
  }
}

void LSTMParser::_Impl::unary_tr(const SuperCat *supercat,
                                std::vector<SuperCat *> &tmp) {
  //const unsigned tmpSize = tmp.size();
  //for(unsigned i = 0; i < tmpSize; ++i){
  //SuperCat *sc = tmp[i];
  const Cat *cat = supercat->cat;

  const TRCats *trcats = 0;
  if(cat->is_NP())
    trcats = &cats.trNP;
  else if(cat->is_AP())
    trcats = &cats.trAP;
  else if(cat->is_PP())
    trcats = &cats.trPP;
  else if(cat->is_StobNP())
    trcats = &cats.trVP_to;
  else { }

  if (trcats != 0)
  {
    for(TRCats::const_iterator j = trcats->begin(); j != trcats->end(); ++j)
    {
      //cerr << "TypeRaise" << endl;
      SuperCat *new_sc = SuperCat::TypeRaise(chart.pool, *j, SuperCat::TR, supercat, 11);
      assert(new_sc->left);
      new_sc->SetTr();
      new_sc->m_unary_id = j->ind;
      //cerr << "tr cat: " << *new_sc->cat << endl;
      tmp.push_back(new_sc);
    }
  }
  //}
}

void
LSTMParser::_Impl::raws2words(const vector<std::string> &raw,
                             Words &words) const {
  words.resize(0);
  words.reserve(sent.words.size());
  for(vector<std::string>::const_iterator i = raw.begin(); i != raw.end(); ++i)
    words.push_back(lexicon[*i]);
}

void
LSTMParser::_Impl::words2word_inds(const vector<std::string> &raw,
                                   const vector<std::string> &pos,
                                   std::vector<unsigned> &word_inds_vec,
                                   std::vector<unsigned> &suf_inds_vec) const {
  word_inds_vec.reserve(sent.words.size());
  suf_inds_vec.reserve(sent.words.size());
  assert(raw.size() == pos.size());
  assert(raw.size() == sent.words.size());
  for(unsigned i = 0; i < raw.size(); ++i) {
    vector<unsigned> word_inds = word2vec(raw[i]);
    word_inds_vec.emplace_back(word_inds[0]);
    //if (cfg.use_pos()) {
      auto iter = m_pos_str_ind_map.find(pos[i]);
      assert(iter != m_pos_str_ind_map.end());
      assert(iter->second <= m_total_pos - 4);
      suf_inds_vec.emplace_back(iter->second);
    //}
  }
}

void
LSTMParser::_Impl::clean_str(string &word, vector<string> &ret) const {

  string word_copy1 = word;
  if (!is_lower(word)) {
    std::transform(word_copy1.begin(), word_copy1.end(), word_copy1.begin(), ::tolower);
    ret.push_back(word_copy1);
  }

  string word_copy2 = word;
  replace_all_substrs(word_copy2, ",", "");
  replace_all_substrs(word_copy2, ".", "");
  replace_all_substrs(word_copy2, "-", "");
  replace_all_substrs(word_copy2, " ", "");
  replace_all_substrs(word_copy2, "\\/", "");

  if (all_of(word_copy2.begin(), word_copy2.end(), ::isdigit)) {
    ret.push_back("0");
  } else if (word.find('-') != string::npos) {
    unsigned last_pos = word.find_last_of('-');
    assert(last_pos != string::npos);
    string sub_str = word.substr(last_pos + 1);
    ret.push_back(sub_str);

    if (!is_lower(sub_str)) {
      std::transform(sub_str.begin(), sub_str.end(), sub_str.begin(), ::tolower);
      ret.push_back(sub_str);
    }

  } else {
    size_t pos = word.find("\\/");
    if (pos != string::npos) {
      size_t last_pos = word.find_last_of("/");
      string sub_str = word.substr(last_pos + 1);
      ret.push_back(sub_str);

      if (!is_lower(sub_str)) {
        std::transform(sub_str.begin(), sub_str.end(), sub_str.begin(), ::tolower);
        ret.push_back(sub_str);
      }
    }
  }

}

string
LSTMParser::_Impl::get_suf(const string &word) const {
  string word_copy = word;
  //cerr << "word: " << word << endl;
  replace_all_substrs(word_copy, ",", "");
  replace_all_substrs(word_copy, ".", "");
  replace_all_substrs(word_copy, "-", "");
  replace_all_substrs(word_copy, " ", "");
  replace_all_substrs(word_copy, "\\/", "");
  if (all_of(word_copy.begin(), word_copy.end(), ::isdigit))
    return "0";
  else {
    if (word.length() >= 2)
      return word.substr(word.length() - 2);
    else
      return word;
  }
}

void
LSTMParser::_Impl::replace_all_substrs(string &str,
                                      const string &substr,
                                      const string &re) const {
  string::size_type n = 0;

  while ((n = str.find(substr, n)) != std::string::npos) {
    str.replace(n, substr.size(), re);
    n += substr.size();
  }
}


vector<unsigned>
LSTMParser::_Impl::word2vec(const string &word) const {
  vector<unsigned> word_vec;
  string original_word = word;
  string word_copy = word;
  //
  //  if (m_lowercase_words)
  //    std::transform(word_copy.begin(), word_copy.end(), word_copy.begin(), ::tolower);

  unordered_map<string, unsigned>::const_iterator iter = m_full_wordstr_emb_map.find(word_copy);

  if (iter != m_full_wordstr_emb_map.end()) {
    word_vec.push_back(iter->second);
  } else {
    // not in pre-trained embeddings, try to back off
    bool backed_off = false;
    vector<string> back_off_strs;
    clean_str(original_word, back_off_strs);
    for (unsigned i = 0; i < back_off_strs.size(); ++i) {
      iter = m_full_wordstr_emb_map.find(back_off_strs[i]);
      if (iter != m_full_wordstr_emb_map.end()) {
        backed_off = true;
        word_vec.push_back(iter->second);
        break;
      }
    }

    if (!backed_off) {
      // unknown word
      if (is_alphnum(original_word)) {
        if (islower(original_word[0]))
          word_vec.push_back(m_UNK_ALPHNUM_LOWER_IND);
        else
          word_vec.push_back(m_UNK_ALPHNUM_UPPER_IND);
      } else {
        word_vec.push_back(m_UNK_NON_ALPHNUM_IND);
      }
    }

  }

//  unordered_map<string, unsigned>::const_iterator iter2 = m_suf_str_ind_map.find(get_suf(original_word));
//  if (iter2 != m_suf_str_ind_map.end())
//    word_vec.push_back(iter2->second);
//  else
//    word_vec.push_back(m_UNK_SUFFIX_IND);

  //  if (islower(original_word[0]))
  //    word_vec.push_back(0);
  //  else
  //    word_vec.push_back(1);

  return word_vec;
}

bool
LSTMParser::_Impl::is_lower(const string &word) const {
  for (unsigned i = 0; i < word.length(); ++i) {
    if (isupper(word[i]))
      return false;
  }

  return true;
}


bool
LSTMParser::_Impl::is_alphnum(const string &word) const {
  for (unsigned i = 0; i < word.length(); ++i) {
    if (!isalnum(word[i]))
      return false;
  }

  return true;

}

void
LSTMParser::_Impl::load_supercat_map(const string &file, bool init, bool tagger) {
  cerr << "loading supercat_map " << file << endl;
  ifstream in(file.c_str());
  if(!in) {
    cerr << "could not open features file " << file << endl;
    exit(EXIT_FAILURE);
  }
  string cat;
  unsigned ind;
  unsigned total = 1;
  while (in >> cat >> ind) {
    ++total;
    if (!tagger) {
      if (init) {
        if (cat.find_last_of("\\") != string::npos || cat.find_last_of("/") != string::npos) {
          //cerr << cat << endl;
          cat.insert(0, 1, '(');
          cat.append(")");
          //cerr << cat << endl;
        }
        assert(m_cats_map_str.insert({cat, total}).second);
      } else {
        assert(m_cats_map_str.insert({cat, ind}).second);
      }
    } else {
      assert(m_t_cats_map_str.insert({cat, ind}).second);
      assert(m_label2rawtag_map.insert({ind, cat}).second);
    }
  }
  if (!tagger)
    cerr << "total supercat count (from RNN parser training features): " << m_cats_map_str.size() << endl;
  else
    cerr << "total lex cat including NNOONNEE for super model: " << m_t_cats_map_str.size() << endl;
}

//void
//LSTMParser::_Impl::load_pos_str_ind_map(const string &file) {
//
//  cerr << "loading pos str ind map\n";
//
//  ifstream in(file.c_str());
//  if(!in) {
//    cerr << "could not open pos_str_ind file " << file << endl;
//    exit(EXIT_FAILURE);
//  }
//
//  string pos;
//  unsigned ind;
//  unsigned total = 1;
//
//  while (in >> pos >> ind) {
//    ++total;
//    m_pos_str_ind_map.insert(make_pair(pos, total));
//  }
//
//  cerr << "total pos tag count (from sec0221): " << m_pos_str_ind_map.size() << endl;
//  // pos ind should start from 2, 0 is reserved for empty_pos feature, and 1 for unk_pos
//  // (there shouldn't be unk_pos, but this is for keeping pos feats consistent with suffix feats)
//}

//void
//LSTMParser::_Impl::dump_supercat_map(const string &file) {
//  unsigned total_cats = 0;
//  ofstream catmap_output;
//  catmap_output.open(file.c_str());
//  total_cats = m_cats_map.dump(catmap_output);
//  catmap_output.close();
//  cerr << "total supercat count: " << total_cats << endl;
//}

void
LSTMParser::_Impl::dump_supercat_map(const string &file) {
  ofstream catmap_output;
  catmap_output.open(file.c_str());
  for (auto i : m_cats_map_str) {
    catmap_output << i.first << " " << i.second << endl;
  }
  catmap_output.flush();
  catmap_output.close();
  cerr << "total supercat count: " << m_cats_map_str.size() << endl;
}

bool LSTMParser::_Impl::ignore_dep(const CCG::Filled *&filled) {

  if (m_deps_ignore_map_rule.find((ulong)filled->rule)
      != m_deps_ignore_map_rule.end())
    return true;

  int marked_up = cats.markedup.index((string) cats.relations[filled->rel].cat);
  assert(marked_up != -1);
  if (m_deps_ignore_map_1.find(make_tuple((ulong)marked_up,
                                          cats.relations[filled->rel].jslot,
                                          (ulong)filled->rule)) != m_deps_ignore_map_1.end())
    return true;

  if (m_deps_ignore_map_2.find(make_tuple(sent.words[filled->head - 1],
                                          (ulong)marked_up, cats.relations[filled->rel].jslot,
                                          (ulong)filled->rule)) != m_deps_ignore_map_2.end())
    return true;

  if (m_deps_ignore_map_3.find(make_tuple(sent.words[filled->head - 1],
                                          (ulong)marked_up,
                                          cats.relations[filled->rel].jslot,
                                          sent.words[filled->filler - 1],
                                          (ulong)filled->rule)) != m_deps_ignore_map_3.end())
    return true;

  return false;
}


void
LSTMParser::_Impl::init_xf1_training(bool train) {
  if (train)
    load_gold_deps(m_config_dir + "/wsj02-21.ccgbank_deps", true);
  else
    load_gold_deps(m_config_dir + "/wsj00.ccgbank_deps", false);
  init_ignore_rules();
  load_dep_ignore_list(m_config_dir + "/deps_ignore_cat_slot_rule");
  load_dep_ignore_list2(m_config_dir + "/deps_ignore_pred_cat_slot_rule");
  load_dep_ignore_list3(m_config_dir + "/deps_ignore_pred_cat_slot_arg_rule");
}


void
LSTMParser::_Impl::load_gold_deps(const string &filename, bool train) {
  // note: there are sentences in wsj0221_ccgbank_deps
  //       which have zero deps

  unsigned total_sents = 0;
  if (train)
    total_sents = 39604;
  else
    total_sents = 1913;

  const ulong BASE = 10000;

  ifstream in(filename.c_str());
  if(!in) {
    cerr << "no such file: " << filename << endl;
    exit (EXIT_FAILURE);
  }
  string s;
  ulong sent_dep_count = 0;
  ulong empty = 0;

  while (getline(in, s)) {
    if (s.find("# this") != string::npos ||
        s.find("# src/") != string::npos)
      continue;

    if (s.empty()) {
      ++empty;
      if (empty >= 2 && empty <= total_sents + 1) {
        if (train)
          assert(m_ccg_gold_dep_count_map_train.insert(make_pair(empty - 2, sent_dep_count)).second);
        else
          assert(m_ccg_gold_dep_count_map_dev.insert(make_pair(empty - 2, sent_dep_count)).second);
      }
      sent_dep_count = 0;
      continue;
    }

    ++sent_dep_count;
    istringstream ss(s);
    string head_str;
    string plain_markedup_str;
    string arg_str;

    ulong head = 0;
    int markedup = 0;
    ulong slot = 0;
    ulong arg = 0;

    if(ss >> head_str >> plain_markedup_str >> slot >> arg_str) {
      head = stoi(head_str.substr(head_str.find("_") + 1));

      int ind = cats.markedup.index(plain_markedup_str);
      if (ind != -1) {
        // convert plain markedup index to markedup index
        // since they have different indexes in cats.markedup
        markedup = cats.markedup.index(cats.markedup.markedup(plain_markedup_str));
        assert(markedup != -1);
      } else {
        auto iter = m_unk_markedup_map.find(plain_markedup_str);
        if (iter != m_unk_markedup_map.end()) {
          markedup = iter->second;
        } else {
          markedup = m_unk_markedup_map.size() + BASE;
          m_unk_markedup_map.insert(make_pair(plain_markedup_str, m_unk_markedup_map.size() + BASE));
        }
      }

      arg = stoi(arg_str.substr(arg_str.find("_") + 1));

      CCGDep dep(empty - 1, head, (ulong)markedup, slot, arg);
      if (train)
        assert(m_ccg_dep_map_train.insert(make_pair(dep, 0)).second);
      else
        assert(m_ccg_dep_map_dev.insert(make_pair(dep, 0)).second);
    }
    else {
      cerr << "could not interpret dependency\n";
      exit(EXIT_FAILURE);
    }
  }

  if (train)
    assert(m_ccg_gold_dep_count_map_train.size() == 39604);
  else
    assert(m_ccg_gold_dep_count_map_dev.size() == 1913);
}


void
LSTMParser::_Impl::init_ignore_rules() {
  m_deps_ignore_map_rule.insert({{7, 0}, {11, 0}, {12, 0},
    {14, 0}, {15, 0}, {16, 0},
    {17, 0}, {51, 0}, {52, 0},
    {56, 0}, {91, 0}, {92, 0},
    {95, 0}, {96, 0}, {98, 0}});
}


void
LSTMParser::_Impl::load_dep_ignore_list(const std::string &filename) {
  ifstream in(filename.c_str());
  if (!in) {
    cerr << "no such file: " << filename << endl;
    exit(EXIT_FAILURE);
  }
  string s;
  while (getline(in, s)) {
    //cout << s << endl;
    if (s.find("#") == 0) {
      continue;
    }

    istringstream ss(s);
    string cat_str;
    int cat = 0;
    ulong slot = 0;
    ulong rule_id = 0;
    if(ss >> cat_str >> slot >> rule_id) {
      cat = cats.markedup.index(cat_str);
      if (cat == -1) {
        cerr << "unrecognized markedup cat in " <<
            filename << ", exit now...\n";
        exit(EXIT_FAILURE);
      }
      assert(m_deps_ignore_map_1.insert(make_pair(make_tuple((ulong)cat, slot, rule_id), 0)).second);
    } else {
      cerr << "something is wrong with deps_ignore file !!!\n";
      exit(EXIT_FAILURE);
    }
  }
}


void
LSTMParser::_Impl::load_dep_ignore_list2(const std::string &filename) {
  ifstream in(filename.c_str());
  if (!in) {
    cerr << "no such file: " << filename << endl;
    exit(EXIT_FAILURE);
  }
  string s;
  while (getline(in, s)) {
    //cout << s << endl;
    if (s.find("#") == 0) {
      continue;
    }

    istringstream ss(s);
    string pred;
    string cat_str;
    int cat = 0;
    ulong slot = 0;
    ulong rule_id = 0;
    if(ss >> pred >> cat_str >> slot >> rule_id) {
      cat = cats.markedup.index(cat_str);
      if (cat == -1) {
        cerr << "unrecognized markedup cat in " <<
            filename << ", exit now...\n";
        exit(EXIT_FAILURE);
      }
      assert(m_deps_ignore_map_2.insert(make_pair(make_tuple(pred, (ulong)cat, slot, rule_id), 0)).second);
    } else {
      cerr << "something is wrong deps_ignore file !!!\n";
      exit(EXIT_FAILURE);
    }
  }
}


void
LSTMParser::_Impl::load_dep_ignore_list3(const std::string &filename) {

  //  int id1 = m_cats.markedup.index("(S[pt]\\NP)/(S[ng]\\NP)");
  //  int id2 = m_cats.markedup.index("((S[pt]{_}\\NP{Y}<1>){_}/(S[ng]{Z}<2>\\NP{Y*}){Z}){_}");
  //
  //  cout << "id1: " << id1 << endl;
  //  cout << "id2: " << id2 << endl;

  ifstream in(filename.c_str());
  if (!in) {
    cerr << "no such file: " << filename << endl;
    exit(EXIT_FAILURE);
  }
  string s;
  while (getline(in, s)) {
    //cout << s << endl;
    if (s.find("#") == 0) {
      continue;
    }

    istringstream ss(s);
    string pred;
    string cat_str;
    int cat = 0;
    ulong slot = 0;
    string arg;
    ulong rule_id = 0;
    if(ss >> pred >> cat_str >> slot >> arg >> rule_id) {
      cat = cats.markedup.index(cat_str);
      if (cat == -1) {
        cerr << "unrecognized markedup cat in " <<
            filename << ", exit now...\n";
      }
      assert(m_deps_ignore_map_3.insert(make_pair(make_tuple(pred, (ulong)cat, slot, arg, rule_id), 0)).second);
    } else {
      cerr << "something is wrong deps_ignore file !!!\n";
      exit(EXIT_FAILURE);
    }
  }
}


float
LSTMParser::_Impl::calc_f1(const ShiftReduceHypothesis *&output, const unsigned id, bool train) {
  // sent id should start from 0
  float gold_total = 0.0;
  float total_correct = 0.0;
  float total_returned = 0.0;

  gold_total = get_total_sent_gold_deps(id, train);
  //assert(gold_total != 0); could be 0 in CCGBank
  unsigned stackSize = output->GetStackSize();
  std::vector<const SuperCat *> roots;
  vector<const SuperCat *>::const_reverse_iterator rit;
  roots.push_back(output->GetStackTopSuperCat());
  const ShiftReduceHypothesis *prev_stack = output;

  for (unsigned i = 0; i < stackSize - 1; ++i) {
    prev_stack = prev_stack->GetPrvStack();
    roots.push_back(prev_stack->GetStackTopSuperCat());
  }

  for (rit = roots.rbegin(); rit < roots.rend(); ++rit) {
    //printer.parsed(*rit, sent, BETA, 0);
    get_deps(*rit, id, total_correct, total_returned, 0, train);
  }
  cerr << "correct: " << total_correct;
  cerr << " total: " << total_returned;
  cerr << " gold total: " << gold_total << endl;
  assert(total_correct <= gold_total);
  float p = total_correct/total_returned;
  float r = total_correct/gold_total;
  float f1 = (p == 0.0) || (r == 0.0) ? 0.0 : (2.0*p*r)/(p + r);
  fprintf(stderr, "p: %f, r: %f, f1: %f\n", p, r, f1);
  assert(f1 <= 1.0);

  //  total_correct += gold;
  //  total_returned += total;
  //  total_gold += gold_total;

  return f1;
}


void
LSTMParser::_Impl::get_deps(const CCG::SuperCat *sc, const ulong id,
                            float &gold, float &total, const int FMT, bool train) {
  if(sc->left){
    assert(sc->left);
    get_deps(sc->left, id, gold, total, FMT, train);
    if(sc->right){
      assert(sc->right);
      get_deps(sc->right, id, gold, total, FMT, train);
    }
  }

  pair<float, float> res;
  if (train)
    res = count_gold_deps(sc, id, true);
  else
    res = count_gold_deps(sc, id, false);
  gold += res.first;
  total += res.second;
}


pair<float, float>
LSTMParser::_Impl::count_gold_deps(const SuperCat *sc, const ulong id, bool train) {
  float total = 0.0;
  float gold = 0.0;
  for(const Filled *filled = sc->filled; filled; filled = filled->next) {
    if (ignore_dep(filled)) {
      continue;
    }
    ++total;
    CCGDep dep(id, filled->head,
               cats.markedup.index((string) cats.relations[filled->rel].cat),
               cats.relations[filled->rel].jslot, filled->filler);
    if (train) {
      if (m_ccg_dep_map_train.find(dep) != m_ccg_dep_map_train.end())
        ++gold;
    } else {
      if (m_ccg_dep_map_dev.find(dep) != m_ccg_dep_map_dev.end())
        ++gold;
    }
  }

  return make_pair(gold, total);
}

float
LSTMParser::_Impl::get_total_sent_gold_deps(const unsigned id, const bool train) {
  if (train) {
    assert(m_ccg_gold_dep_count_map_train.find(id) != m_ccg_gold_dep_count_map_train.end());
    return m_ccg_gold_dep_count_map_train.find(id)->second;
  } else {
    assert(m_ccg_gold_dep_count_map_dev.find(id) != m_ccg_gold_dep_count_map_dev.end());
    return m_ccg_gold_dep_count_map_dev.find(id)->second;
  }
}

vector<unsigned>
LSTMParser::_Impl::word2vec_t(string &word) const {
  vector<unsigned> word_vec;
  string original_word = word;
  string word_copy = word;

  std::transform(word_copy.begin(), word_copy.end(), word_copy.begin(), ::tolower);
  unordered_map<string, unsigned>::const_iterator iter = m_full_wordstr_emb_map.find(word_copy);

  if (iter != m_full_wordstr_emb_map.end()) {
    word_vec.push_back(iter->second);
  } else {
    // not in pre-trained embeddings, try to back off
    bool backed_off = false;
    vector<string> back_off_strs;
    clean_str(original_word, back_off_strs);
    for (size_t i = 0; i < back_off_strs.size(); ++i) {
      iter = m_full_wordstr_emb_map.find(back_off_strs[i]);
      if (iter != m_full_wordstr_emb_map.end()) {
        backed_off = true;
        word_vec.push_back(iter->second);
        break;
      }
    }

    if (!backed_off) {
      // unknown word
      if (is_alphnum(original_word)) {
        if (islower(original_word[0]))
          word_vec.push_back(m_T_UNK_ALPHNUM_LOWER_IND);
        else
          word_vec.push_back(m_T_UNK_ALPHNUM_UPPER_IND);
      } else {
        word_vec.push_back(m_T_UNK_NON_ALPHNUM_IND);
      }
    }
  }

  auto iter2 = m_suf_str_ind_map.find(get_suf(original_word));
  if (iter2 != m_suf_str_ind_map.end()) {
    word_vec.push_back(iter2->second);
    //cerr << "suf: " << iter2->second << endl;
  }
  else
    word_vec.push_back(m_T_UNK_SUFFIX_IND);

  assert(word_vec[0] < m_t_vocab_size + 3);
  return word_vec;
}

void
LSTMParser::_Impl::sent2contextwin(const vector<vector<unsigned>> &sent,
                                   vector<vector<vector<unsigned>>> &contextwins) {
  const unsigned dwin = 1;
  assert(dwin % 2 == 1);
  size_t pad_len = dwin/2;
  vector<unsigned> unk_vec;
  unk_vec.push_back(0);
  unk_vec.push_back(m_T_UNK_SUFFIX_IND);

  for (size_t i = 0; i < sent.size(); ++i) {
    vector<vector<unsigned> > contextwin;
    for (size_t j = i, k = 0; j < i + dwin; ++j, ++k) {
      if (j < pad_len || j >= pad_len + sent.size()) {
        contextwin.push_back(unk_vec);
      } else {
        contextwin.push_back(sent[j - pad_len]);
      }
    }
    contextwins.push_back(contextwin);
  }
}

//void
//LSTMParser::_Impl::read_data(IO::ReaderFactory &reader,
//                             vector<pair<vector<vector<size_t>>, vector<size_t>>> &data,
//                             size_t &total_sents,
//                             size_t &ntokens,
//                             double &in_pretrained) {
//  Sentence sent;
//  while(reader.next(sent)) {
//    ++total_sents;
//    vector<string> &sent_words = sent.words;
//    vector<string> &sent_cats = sent.super;
//    vector<vector<size_t>> x; // input
//    vector<size_t> y; // target
//    ntokens += sent.words.size();
//
//    data.push_back(convert_sent(in_pretrained));
//    sent.reset();
//  }
//}

pair<vector<vector<unsigned>>, vector<unsigned>>
LSTMParser::_Impl::convert_sent(const bool debug) {
  vector<vector<unsigned>> x; // input
  vector<unsigned> y; // target
  for (size_t i = 0; i < sent.words.size(); ++i) {
    x.push_back(word2vec_t(sent.words[i]));

    // gold super supplied for debugging lstm super
    if (debug) {
      auto iter = m_t_cats_map_str.find(sent.super[i]);
      if (iter != m_t_cats_map_str.end())
        y.push_back(iter->second);
      else
        y.push_back(m_T_UNKNWON_SUPERCAT_IND);
    } else {
      y.push_back(m_T_UNKNWON_SUPERCAT_IND);
    }
  }
  assert(x.size() == y.size());
  return make_pair(x, y);
}

Expression
LSTMParser::_Impl::build_tagging_graph(LSTMBuilder &l2rbuilder,
                                       LSTMBuilder &r2lbuilder,
                                       vector<vector<vector<unsigned>>> &sent_contextwins,
                                       vector<unsigned> &tags,
                                       ComputationGraph& cg,
                                       bool test, const float &beta) {
  const unsigned slen = sent_contextwins.size();
  l2rbuilder.new_graph(cg);  // reset RNN builder for new graph
  l2rbuilder.start_new_sequence();
  r2lbuilder.new_graph(cg);  // reset RNN builder for new graph
  r2lbuilder.start_new_sequence();
  Expression i_l2th = parameter(cg, pt_l2th);
  Expression i_r2th = parameter(cg, pt_r2th);
  Expression i_thbias = parameter(cg, pt_thbias);
  //Expression i_th2t = parameter(cg, p_th2t);
  //Expression i_tbias = parameter(cg, p_tbias);
  vector<Expression> errs;
  vector<Expression> i_words(slen);
  vector<Expression> fwds(slen);
  vector<Expression> revs(slen);
  vector<vector<Expression>> contexts(slen);

//  if (!eval) {
//    l2rbuilder.set_dropout(pdrop);
//    r2lbuilder.set_dropout(pdrop);
//  } else {
    l2rbuilder.disable_dropout();
    r2lbuilder.disable_dropout();
  //}

  // read sequence from left to right
  for (size_t t = 0; t < slen; ++t) {
    for (size_t i = 0; i < 1; ++i) {
      contexts[t].push_back(lookup(cg, pt_w, sent_contextwins[t][i][0]));
      contexts[t].push_back(lookup(cg, pt_suf, sent_contextwins[t][i][1]));
    }
    i_words[t] = concatenate(contexts[t]); // concate all emb cols into a single COL vec
    //i_words[t] = lookup(cg, p_w, sent_contextwins[t][0][0]);
    fwds[t] = l2rbuilder.add_input(i_words[t]);
  }

  // read sequence from right to left
  //r2lbuilder.add_input(lookup(cg, p_w, m_full_wordstr_emb_map["***PADDING***"]));
  for (size_t t = 0; t < slen; ++t) {
    revs[slen - t - 1] = r2lbuilder.add_input(i_words[slen - t - 1]);
  }

  for (int t = 0; t < slen; ++t) {
    m_dtags++;
    Expression i_t = affine_transform({i_thbias, i_l2th, fwds[t], i_r2th, revs[t]});
    Expression softmax_dist = softmax(i_t);
    vector<float> dist = as_vector(cg.incremental_forward());
    assert(dist.size() == m_t_tag_size);
    auto iter = max_element(dist.begin(), dist.begin() + dist.size() - 1);
    float best = -9e99;
    size_t besti = 5000;
    for (size_t i = 0; i < dist.size() - 1; ++i) {
      if (dist[i] > best) {
        best = dist[i];
        besti = i;
      }
    }
    //cerr << *iter << dist[besti] << endl;
    assert(*iter == dist[besti]);
//    if (m_label2rawtag_map.find(besti)->second == sent.super[t])
//      m_dcorr++;
    float cut_off = *iter;
    MultiRaw &mraw = sent.msuper[t];
    mraw.clear();
    cut_off *= beta;
    assert(cut_off >= 0.0);
    // the mapping between string cats and inds
    // are different in the tagger and the parser
    // the parser loads the cats from the markedup file
    //bool has_gold = false;
    for (size_t k = 0; k < m_t_tag_size - 1; ++k) {
      if (dist[k] >= cut_off) {
        auto iter = m_label2rawtag_map.find(k);
        assert(iter != m_label2rawtag_map.end());
//        if (true && iter->second == sent.super[t]) {
//          //has_gold = true;
//          //++m_dcorr;
//        }
        mraw.push_back(ScoredRaw(iter->second, dist[k]));
      }
    }
    assert(mraw.size() > 0);

    //      if (m_train == 2 && !has_gold) {
    //        // supply gold tag at training time (for greedy training only)
    //        mraw.push_back(ScoredRaw(sent.super[t], 1.0));
    //      }

    if (beta == 0.0) assert(mraw.size() == m_t_tag_size - 1);
    sort(mraw.begin(), mraw.end());
    //}
    //if (tags[t] != m_UNKNWON_SUPERCAT_IND) {
    Expression i_err = pickneglogsoftmax(i_t, tags[t]); // tags contains gold tag indexes
    // i_err is just -sum{t_i log(y_i)} without the sum, i.e., neg log likelihood for one position
    errs.push_back(i_err);
  }
  //m_all_sent_dist.push_back(sent_dist);
  return sum(errs);
}

Expression
LSTMParser::_Impl::build_local_attention_tagging_graph(LSTMBuilder &l2rbuilder,
                                                       LSTMBuilder &r2lbuilder,
                                                       vector<vector<vector<unsigned>>> &sent_contextwins,
                                                       vector<unsigned> &tags,
                                                       ComputationGraph& cg,
                                                       bool test, const float &beta, const bool debug) {
  //std::unordered_map<std::pair<int, int>, Expression, KeyHash, KeyEqual> att_weights_map;
  const int slen = sent_contextwins.size();
  l2rbuilder.new_graph(cg);  // reset RNN builder for new graph
  l2rbuilder.start_new_sequence();
  r2lbuilder.new_graph(cg);  // reset RNN builder for new graph
  r2lbuilder.start_new_sequence();

  Expression i_l2ah = parameter(cg, pt_l2ah); // left(forward) hidden of target position 2 att_hidden
  Expression i_r2ah = parameter(cg, pt_r2ah);
  Expression i_c2ah = parameter(cg, pt_c2ah); // context hidden to att_hidden
  Expression i_tbias = parameter(cg, pt_tbias); // att_hidden bias
  Expression i_th2t = parameter(cg, pt_th2t); // tag output layer
  Expression att_w = parameter(cg, pt_att_w);
  Expression i_thbias = parameter(cg, pt_thbias); // final bias
  Expression i_l2t = parameter(cg, pt_l2th);
  Expression i_r2t = parameter(cg, pt_r2th);
  Expression i_c2t = parameter(cg, pt_c2t);
  vector<Expression> errs;
  vector<Expression> i_words(slen);
  vector<Expression> fwds(slen);
  vector<Expression> revs(slen);
  vector<vector<Expression>> contexts(slen);
  vector<vector<Expression>> unnormal_align_weights(slen);
  int half_win = std::floor(5/2);

//  if (!test) {
//    l2rbuilder.set_dropout(pdrop);
//    r2lbuilder.set_dropout(pdrop);
//  } else {
    l2rbuilder.disable_dropout();
    r2lbuilder.disable_dropout();
 // }

  // read sequence from left to right
  const unsigned dwin = 1; //todo
  for (int t = 0; t < slen; ++t) {
    for (unsigned i = 0; i < dwin; ++i) {
      contexts[t].push_back(lookup(cg, pt_w, sent_contextwins[t][i][0]));
      contexts[t].push_back(lookup(cg, pt_suf, sent_contextwins[t][i][1]));
    }
    i_words[t] = concatenate(contexts[t]); // concate all emb cols into a single COL vec
    //i_words[t] = lookup(cg, p_w, sent_contextwins[t][0][0]);
    fwds[t] = l2rbuilder.add_input(i_words[t]);
  }

  // read sequence from right to left
  //r2lbuilder.add_input(lookup(cg, p_w, m_full_wordstr_emb_map["***PADDING***"]));
  for (int t = 0; t < slen; ++t) {
    revs[slen - t - 1] = r2lbuilder.add_input(i_words[slen - t - 1]);
  }

  // target t always on the right
  const unsigned hidden_dim = 256; //todo
  for (int t = 0; t < slen; ++t) {
    unnormal_align_weights[t].reserve(2*half_win);
    for (int t1 = t - half_win; t1 <= t + half_win; ++t1) {
      if (t1 == t) continue;
      Expression w;
      if (t1 < 0 || t1 >= slen) {
        w = att_w * concatenate({zeroes(cg, {hidden_dim*2}), fwds[t], revs[t]});
      } else {
        w = att_w * concatenate({fwds[t1], revs[t1], fwds[t], revs[t]});
      }
      unnormal_align_weights[t].push_back(w);
    }
  }

  vector<Expression> hidden_concat_at_all_t;
  hidden_concat_at_all_t.reserve(slen);
  for (int t = 0; t < slen; ++t) {
    hidden_concat_at_all_t.emplace_back(concatenate({fwds[t], revs[t]}));
  }
  Expression hidden_concat = concatenate_cols(hidden_concat_at_all_t);
  vector<vector<float>> sent_dist;
  for (int t = 0; t < slen; ++t) {
    m_dtags++;
    vector<Expression> padding;
    vector<unsigned> ts;
    ts.reserve(5 - 1);
    for (int t1 = t - half_win; t1 <= t + half_win; ++t1) {
      if (t1 == t) continue;
      if (t1 < 0 || t1 >= slen) {
        padding.push_back(zeroes(cg, {hidden_dim*2}));
        continue;
      }
      ts.push_back(t1);
    }

    assert(unnormal_align_weights[t].size() == 5 - 1);
    Expression normal_align_weights = softmax(concatenate(unnormal_align_weights[t]));
//    display_value(unnormal_align_weights[t][0], cg, "unnormal");
//    display_value(unnormal_align_weights[t][1], cg, "unnormal");
    //display_value(concatenate(unnormal_align_weights[t]), cg, "unnormal_concat");
    Expression context_cols = select_cols(hidden_concat, ts);
    if (t < half_win)
      context_cols = concatenate_cols({concatenate_cols(padding), context_cols});
    else if (t + half_win >= slen)
      context_cols = concatenate_cols({context_cols, concatenate_cols(padding)});
    Expression context_v = context_cols * normal_align_weights;
    Expression i_th, i_t;
    const bool att_h = true;
    if (att_h) {
      i_th = rectify(affine_transform({i_tbias, i_l2ah, fwds[t], i_r2ah, revs[t], i_c2ah, context_v}));
    //if (!eval) { i_th = dropout(i_th, pdrop); }
      i_t = affine_transform({i_thbias, i_th2t, i_th});
    } else {
      i_t = affine_transform({i_thbias, i_l2t, fwds[t], i_r2t, revs[t], i_c2t, context_v});
    }

    Expression softmax_dist;
//    if (test) {
//      // test for 1-best, for debugging only
//      softmax_dist = softmax(i_t);
//      vector<float> dist = as_vector(cg.incremental_forward());
//      sent_dist.push_back(dist);
//      float best = -9e99;
//      size_t besti = 5000;
//      for (size_t i = 0; i < dist.size(); ++i) {
//        if (dist[i] > best) {
//          best = dist[i];
//          besti = i;
//        }
//      }
//      if (tags[t] == besti)
//        ++m_dcorr;
//    } else {
      softmax_dist = softmax(i_t);
      vector<float> dist = as_vector(cg.incremental_forward());
      assert(dist.size() == m_t_tag_size);
      auto iter = max_element(dist.begin(), dist.begin() + dist.size() - 1);
      float cut_off = *iter;
      MultiRaw &mraw = sent.msuper[t];
      mraw.clear();
      cut_off *= beta;
      assert(cut_off >= 0.0);
      // the mapping between string cats and inds
      // are different in the tagger and the parser
      // the parser loads the cats from the markedup file
      //bool has_gold = false;
      for (size_t k = 0; k < m_t_tag_size - 1; ++k) {
        if (dist[k] >= cut_off) {
          auto iter = m_label2rawtag_map.find(k);
          assert(iter != m_label2rawtag_map.end());
          if (debug && iter->second == sent.super[t]) {
            //has_gold = true;
            ++m_dcorr;
          }
          mraw.push_back(ScoredRaw(iter->second, dist[k]));
        }
      }
      assert(mraw.size() > 0);

//      if (m_train == 2 && !has_gold) {
//        // supply gold tag at training time (for greedy training only)
//        mraw.push_back(ScoredRaw(sent.super[t], 1.0));
//      }

      if (beta == 0.0) assert(mraw.size() == m_t_tag_size - 1);
      sort(mraw.begin(), mraw.end());
    //}
    //if (tags[t] != m_UNKNWON_SUPERCAT_IND) {
    Expression i_err = pickneglogsoftmax(i_t, tags[t]); // tags contains gold tag indexes
    // i_err is just -sum{t_i log(y_i)} without the sum, i.e., neg log likelihood for one position
    errs.push_back(i_err);
  }
  //m_all_sent_dist.push_back(sent_dist);
  return sum(errs);
}

void LSTMParser::_Impl::calc_tagging_acc() {
  cerr << "supertagging total correct: " << m_dcorr <<  " total tags evaludated: " << m_dtags << endl;
  cerr << "supertagging acc: " << m_dcorr / m_dtags << endl;
}

void LSTMParser::_Impl::gen_context(ComputationGraph &cg,
                                    ShiftReduceHypothesis* &hypo,
                                    vector<cnn::RNNPointer> &state_ps,
                                    unsigned type,
                                    Expression &i_att_trans,
                                    Expression &i_att_trans_q,
                                    Expression &i_att,
                                    Expression &queue_state,
                                    Expression &i_pre_att_trans,
                                    Expression &i_pre_att_trans2,
                                    const Expression &prev_context,
                                    Expression &context,
                                    std::unordered_map<int, Expression> &state_map) {
  cnn::LSTMBuilder *stackbuilder = nullptr;
  if (type == 0) { stackbuilder = m_w_stackbuilder; }
  if (type == 1) { stackbuilder = m_s_stackbuilder; }
  if (type == 2) { stackbuilder = m_a_stackbuilder; }
  if (type == 3) { stackbuilder = m_p_stackbuilder; }
  assert(stackbuilder);
  size_t count = 0;
  vector<Expression> h_states;
  unsigned limit = 100; // upper bound 511
  for (auto &p : state_ps) {
    if (++count > limit) break; //todo cnn concatenate_cols doesn't handle >= 512 cols
    if (cfg.pdrop() != 0.0 && m_train >= 2) {
      auto iter = state_map.find(int(p));
      if (iter != state_map.end()) {
        h_states.push_back(iter->second);
      } else {
        Expression state_dropout = dropout(stackbuilder->get_h(p).back(), cfg.pdrop());
        h_states.push_back(state_dropout);
        assert(state_map.insert({int(p), state_dropout}).second);
      }
    } else {
      h_states.push_back(stackbuilder->get_h(p).back());
    }
  }
  if (count < limit) {
    assert(h_states.size() == hypo->GetTotalActionCount() + 1); // +1 for padding
    assert(count == (unsigned)hypo->m_w_stack_pointers.size());
  } else {
    assert(hypo->m_w_stack_pointers.size() >= limit);
  }
  Expression one = input(cg, 1.0);
  unsigned rep_len = count >= limit ? limit : (unsigned)hypo->m_w_stack_pointers.size();
  Expression zeros = zeroes(cg, {1, rep_len});
  Expression ones = colwise_add(zeros, one);
  //display_value(ones, cg, "ones");
  Expression queue_trans = i_att_trans_q*queue_state;
  Expression queue_trans_repeat = queue_trans*ones;
  Expression exp1;
  Expression state_mat = concatenate_cols(h_states);
  if (cfg.att_all() && cfg.att_prev_context()) {
    Expression exp = i_pre_att_trans*prev_context;
    Expression pre_context_trans_repeat = exp*ones;
    exp1 = tanh(i_att_trans*state_mat + queue_trans_repeat + pre_context_trans_repeat);
  } else {
    exp1 = tanh(i_att_trans*state_mat + queue_trans_repeat);
  }
  Expression temp = i_att*exp1;
  Expression weights = softmax(transpose(temp));
  if (cfg.att_all() && cfg.att_prev_context())
    context = state_mat*weights + tanh(i_pre_att_trans2*prev_context);
  else context = state_mat*weights;
}

void
LSTMParser::_Impl::lstm_tag(cnn::LSTMBuilder &t_l2rbuilder,
                            cnn::LSTMBuilder &t_r2lbuilder,
                            ComputationGraph& cg,
                            const float beta) {
  // do lstm supertagging
  vector<vector<vector<unsigned>>> contextwins;
  //pair<vector<vector<unsigned>>, vector<unsigned>> xy = convert_sent(true);
  pair<vector<vector<unsigned>>, vector<unsigned>> xy = convert_sent(false);
  assert(xy.first.size() == sent.words.size());
  sent2contextwin(xy.first, contextwins);
  //build_local_attention_tagging_graph(t_l2rbuilder, t_r2lbuilder, contextwins, xy.second, cg, false, beta, true);
  build_local_attention_tagging_graph(t_l2rbuilder, t_r2lbuilder, contextwins, xy.second, cg, false, beta);
  //build_tagging_graph(t_l2rbuilder, t_r2lbuilder, contextwins, xy.second, cg, false, beta);
  //return 0; //when only testing the tagger, set the last field above to true, and uncomment this line
}

const ShiftReduceHypothesis*
LSTMParser::_Impl::sr_parse_beam_search_xf1(cnn::LSTMBuilder &t_l2rbuilder,
                                            cnn::LSTMBuilder &t_r2lbuilder,
                                            ComputationGraph& cg,
                                            const unsigned sent_id, const float BETA,
                                            float &total_xf1, float &total_parses,
                                            float &total_acts, float &total_correct,
                                            const bool oracle) {
  try {
    // xf1 training related
    cout << "sent: " << sent_id << endl;
    const unsigned slen = sent.words.size();
    if (slen > cfg.maxwords())
      throw NLP::ParseError("sentence length exceeds maximum number of words for parser", nsentences);
    nsentences++;

    SuperCat::nsupercats = 0;
    vector<Expression> seq_score_vec;
    vector<Expression> f1_vec;
    vector<float> arma_seq_score_vec;
    vector<float> arma_f1_vec;
    m_total_reduce_count = 0; // reset total reduce count
    unsigned total_shift_count = 0;

    const ShiftReduceHypothesis *best_hypo; // the highest-scored hypo on each level
    const ShiftReduceHypothesis *gold_hypo;
    vector<ShiftReduceAction*> *gold_tree_actions = 0;
    vector<pair<unsigned, unsigned>> gold_tree_actions_rnn_fmt;
    unsigned gold_tree_size = 0;
    if (m_train == 2) { // greedy training
      gold_tree_actions = m_goldTreeVec.at(sent_id);
      gold_tree_actions_rnn_fmt = m_gold_tree_vec_rnn[sent_id];
      gold_tree_size = gold_tree_actions->size();
      assert(gold_tree_actions->size() == gold_tree_actions_rnn_fmt.size());
      total_acts += gold_tree_size;
    }
    ShiftReduceAction *gold_action = 0;
    pair<unsigned, unsigned> gold_action_rnn_fmt;
    unsigned gold_action_id = 0;
    bool gold_finished = false;

    const ShiftReduceHypothesis *candidate_output = 0;
    std::priority_queue<ShiftReduceHypothesis*, vector<ShiftReduceHypothesis*>, Hypo_comp> kbest_output_queue;

    // inds from the input sentence
    vector<unsigned> word_inds_vec;
    vector<unsigned> pos_inds_vec;
    words2word_inds(sent.words, sent.pos, word_inds_vec, pos_inds_vec);
    assert(word_inds_vec.size() == pos_inds_vec.size());

    m_lattice = new ShiftReduceLattice(slen, m_beamSize, MAX_UNARY_ACTIONS);
    ShiftReduceHypothesis *start_hypo = new ShiftReduceHypothesis(0, 0, 0, 0, 0, 0, 0.0, false, 0, 0, 0, 0, 0.0, 0);
    start_hypo->SetStartHypoFlag();
    Expression _0 = input(cg, 0.0);
    start_hypo->m_total_log_prob = _0;
    m_lattice->Add(0, start_hypo);
    gold_hypo = m_lattice->GetLatticeArray(0); // gold == start

    vector<Expression> log_probs;
    Expression i_acth = parameter(cg, p_acth);
    Expression i_acth_bias = parameter(cg, p_acth_bias);
    Expression i_act = parameter(cg, p_act);
    Expression i_act_bias = parameter(cg, p_act_bias);
    Expression i_att;
    if (cfg.att())
      i_att = parameter(cg, p_att);

    Expression i_att_trans_w;
    Expression i_att_trans_s;
    Expression i_att_trans_a;
    Expression i_att_trans_p;
    Expression i_att_trans_q;
    Expression i_att_w_trans_prev;
    Expression i_att_w_trans_prev2;
    Expression i_att_s_trans_prev;
    Expression i_att_s_trans_prev2;
    Expression i_att_a_trans_prev;
    Expression i_att_a_trans_prev2;
    Expression i_att_p_trans_prev;
    Expression i_att_p_trans_prev2;
    Expression i_att_w;
    Expression i_att_s;
    Expression i_att_a;
    Expression i_att_p;
    if (cfg.att_all()) {
      i_att_trans_w = parameter(cg, p_att_trans_w);
      i_att_trans_s = parameter(cg, p_att_trans_s);
      i_att_trans_a = parameter(cg, p_att_trans_a);
      i_att_trans_p = parameter(cg, p_att_trans_p);
      i_att_trans_q = parameter(cg, p_att_trans_q);
      i_att_w = parameter(cg, p_att_w);
      i_att_s = parameter(cg, p_att_s);
      i_att_a = parameter(cg, p_att_a);
      i_att_p = parameter(cg, p_att_p);
    }
    if (cfg.att_prev_context()) {
      i_att_w_trans_prev = parameter(cg, p_w_att_trans_prev_context);
      i_att_w_trans_prev2 = parameter(cg, p_w_att_trans_prev_context2);
      i_att_s_trans_prev = parameter(cg, p_s_att_trans_prev_context);
      i_att_s_trans_prev2 = parameter(cg, p_s_att_trans_prev_context2);
      i_att_a_trans_prev = parameter(cg, p_a_att_trans_prev_context);
      i_att_a_trans_prev2 = parameter(cg, p_a_att_trans_prev_context2);
      i_att_p_trans_prev = parameter(cg, p_p_att_trans_prev_context);
      i_att_p_trans_prev2 = parameter(cg, p_p_att_trans_prev_context2);
    }

    vector<Expression> i_words(slen + 1);
    vector<Expression> fwds(slen + 1);
    vector<Expression> revs(slen + 1);

    m_queue_l2rbuilder->new_graph(cg);
    m_queue_l2rbuilder->start_new_sequence();
    m_queue_r2lbuilder->new_graph(cg);
    m_queue_r2lbuilder->start_new_sequence();

    m_w_stackbuilder->new_graph(cg);
    m_w_stackbuilder->start_new_sequence();
    m_s_stackbuilder->new_graph(cg);
    m_s_stackbuilder->start_new_sequence();
    m_a_stackbuilder->new_graph(cg);
    m_a_stackbuilder->start_new_sequence();
    m_p_stackbuilder->new_graph(cg);
    m_p_stackbuilder->start_new_sequence();
//    m_c_stackbuilder->new_graph(cg);
//    m_c_stackbuilder->start_new_sequence();

    if (m_train == 2 || (m_train == 3 && cfg.xf1_dropout())) {
      m_queue_l2rbuilder->set_dropout(cfg.pdrop());
      m_queue_r2lbuilder->set_dropout(cfg.pdrop());
      m_w_stackbuilder->set_dropout(cfg.pdrop());
      m_s_stackbuilder->set_dropout(cfg.pdrop());
      m_a_stackbuilder->set_dropout(cfg.pdrop());
      m_p_stackbuilder->set_dropout(cfg.pdrop());
      //m_c_stackbuilder->set_dropout(cfg.pdrop());
    } else {
      m_queue_l2rbuilder->disable_dropout();
      m_queue_r2lbuilder->disable_dropout();
      m_w_stackbuilder->disable_dropout();
      m_s_stackbuilder->disable_dropout();
      m_a_stackbuilder->disable_dropout();
      m_p_stackbuilder->disable_dropout();
      //m_c_stackbuilder->disable_dropout();
    }

    // add padding symbols
    Expression p_back, a_back, s_back;
    if (cfg.use_pos()) {
      p_back = m_p_stackbuilder->add_input(lookup(cg, p_pos, m_PAD_POS_IND));
      start_hypo->update_p_state(m_p_stackbuilder->state());
    }
    if (cfg.use_act()) {
      Expression input;
      if (cfg.feeding()) input = concatenate({lookup(cg, p_action, m_PAD_ACTION_IND), p_back});
      else input = lookup(cg, p_action, m_PAD_ACTION_IND);
      a_back = m_a_stackbuilder->add_input(input);
      start_hypo->update_a_state(m_a_stackbuilder->state());
    }
    if (cfg.use_super()) {
      Expression input;
      if (cfg.feeding()) input = concatenate({lookup(cg, p_super, m_PAD_SUPER_IND), a_back});
      else input = lookup(cg, p_super, m_PAD_SUPER_IND);
      s_back = m_s_stackbuilder->add_input(input);
      start_hypo->update_s_state(m_s_stackbuilder->state());
    }
    Expression w_input;
    if (cfg.feeding()) w_input = concatenate({lookup(cg, p_w, m_PAD_WORD_IND), s_back});
    else w_input = lookup(cg, p_w, m_PAD_WORD_IND);
    m_w_stackbuilder->add_input(w_input);
    start_hypo->update_w_state(m_w_stackbuilder->state());

    if (cfg.use_comp()) {
      vector<Expression> c;
      c.push_back(m_w_stackbuilder->back());
      c.push_back(m_s_stackbuilder->back());
      c.push_back(m_a_stackbuilder->back());
      c.push_back(m_p_stackbuilder->back());
      m_c_stackbuilder->add_input(concatenate(c));
      start_hypo->m_c_stack_pointers.push_back(m_c_stackbuilder->state());
    }
    if (cfg.att_all()) {
      start_hypo->m_w_stack_pointers.push_back(m_w_stackbuilder->state());
      start_hypo->m_s_stack_pointers.push_back(m_s_stackbuilder->state());
      start_hypo->m_a_stack_pointers.push_back(m_a_stackbuilder->state());
      start_hypo->m_p_stack_pointers.push_back(m_p_stackbuilder->state());
      Expression zeros = zeroes(cg, {cfg.nh(), 1});
      start_hypo->m_w_stack_contexts.push_back(zeros);
      start_hypo->m_s_stack_contexts.push_back(zeros);
      start_hypo->m_a_stack_contexts.push_back(zeros);
      start_hypo->m_p_stack_contexts.push_back(zeros);
      m_w_pointer_droppedout_state_map.clear();
      m_s_pointer_droppedout_state_map.clear();
      m_a_pointer_droppedout_state_map.clear();
      m_p_pointer_droppedout_state_map.clear();
    }

    // read input
    Expression w, p;
    for (unsigned t = 0; t <= slen; ++t) {
      if (t == slen) w = lookup(cg, p_w, m_PAD_WORD_IND);
      else w = lookup(cg, p_w, word_inds_vec[t]);
      if (cfg.use_pos()) {
        if (t == slen) p = lookup(cg, p_pos, m_PAD_POS_IND);
        else p = lookup(cg, p_pos, pos_inds_vec[t]);
        i_words[t] = concatenate({w, p});
      } else {
        i_words[t] = w;
      }
      fwds[t] = m_queue_l2rbuilder->add_input(i_words[t]);
      //display_value(fwds[t],cg, "fwds[t]");
      //display_value(queue_l2rbuilder.back(), cg, "back()");
    }

    for (unsigned t = 0; t <= slen; ++t) {
      revs[slen - t] = m_queue_r2lbuilder->add_input(i_words[slen - t]);
    }

    //debug
    double total_steps = 0.0;
    unsigned startIndex = 0;
    // set start and end indexes of the lattice
    while (m_lattice->GetEndIndex() == 0 || m_lattice->GetEndIndex() > m_lattice->GetPrevEndIndex()) {
      ++total_steps;
      if (m_lattice->GetEndIndex() != 0)
        startIndex = m_lattice->GetPrevEndIndex() + 1;
      else
        startIndex = 0;
      m_lattice->SetPrevEndIndex(m_lattice->GetEndIndex());

      if (m_train == 2) { // greedy training
        if (gold_action_id < gold_tree_size) {
          gold_action = gold_tree_actions->at(gold_action_id);
          assert(gold_action);
          //cerr << "gold: " << gold_action->GetAction() << " " << gold_action->GetCat()->out_novar_noX_noNB_SR_str() << endl;
          gold_action_rnn_fmt = gold_tree_actions_rnn_fmt[gold_action_id];
          assert(gold_action_rnn_fmt.first == gold_action->GetAction());
        }
      }

      HypothesisPQueue hypoQueue;
      for (unsigned i = startIndex; i <= m_lattice->GetEndIndex(); ++i) {
        ShiftReduceHypothesis *hypo = const_cast<ShiftReduceHypothesis *>(m_lattice->GetLatticeArray(i));
        unsigned j = (unsigned) hypo->GetNextInputIndex();
        assert(j <= slen);

        // feasible target values for the current context
        // the map is a check to make sure all the feasible
        // target values for the current context are unique
        // feasible_y_vals_map: key: output class (action id), value: (S/R/U, result cat)
        vector<unsigned> feasible_y_vals;
        unordered_map<unsigned, pair<unsigned, SuperCat*>> feasible_y_vals_map;

        // find all feasible actions,
        // but do not push them into the queue

        // shift
        if (j < slen) {
          const MultiRaw &multi = sent.msuper[j];
          for (MultiRaw::const_iterator k = multi.begin(); k!= multi.end(); ++k) {
            const Cat *cat = cats.markedup[k->raw];

            if(!cat) {
              cerr << "SHIFT action error: attempted to load category without markedup " << k->raw << endl;
              exit (EXIT_FAILURE);
            }

            assert(m_lex_cat_ind_map.find(k->raw) != m_lex_cat_ind_map.end());
            feasible_y_vals.push_back(m_lex_cat_ind_map[k->raw]);
            SuperCat *stack_top_cat = SuperCat::Lexical(chart.pool, j+1, cat, 0);
            assert(feasible_y_vals_map.insert(make_pair(m_lex_cat_ind_map[k->raw], make_pair(0, stack_top_cat))).second);
          } // end for
        } // end if

        // unary
        if (hypo->GetStackTopSuperCat() != 0) {
          const SuperCat *sc = hypo->GetStackTopSuperCat();
          vector<SuperCat *> tmp;
          tmp.clear();

          if (!sc->IsLex() && !sc->IsTr()) {
            unary_tc(hypo->GetStackTopSuperCat(), false, tmp);
            unary_tr(sc, tmp);
          }
          else if (sc->IsLex() && !sc->IsTr()) {
            unary_tr(sc, tmp);
          }
          else if (!sc->IsLex() && sc->IsTr()) { /* do nothing */ }
          else if (sc->IsLex() && sc->IsTr()) { /* do nothing */ }

          for (vector<SuperCat *>::iterator j = tmp.begin(); j < tmp.end(); ++j) {
            SuperCat *stack_top_cat = *j;
            assert(stack_top_cat->left);
            feasible_y_vals.push_back(stack_top_cat->m_unary_id + m_unary_base_count);
            assert(feasible_y_vals_map.insert(make_pair(stack_top_cat->m_unary_id + m_unary_base_count,
                                                        make_pair(1, stack_top_cat))).second);
          }
        }

        // reduce
        if (hypo->GetPrvStack() != 0 && hypo->GetPrvStack()->GetStackTopSuperCat() != 0
            && hypo->GetStackTopSuperCat() != 0) {
          results.clear();
          if (!cfg.seen_rules() ||
              rule_instances(hypo->GetPrvStack()->GetStackTopSuperCat()->GetCat(), hypo->GetStackTopSuperCat()->GetCat()))
            rules(hypo->GetPrvStack()->GetStackTopSuperCat(), hypo->GetStackTopSuperCat(),
                  cfg.eisner_nf(), cfg.seen_rules(), cfg.question_rules(), results);

          if (results.size() > 0) {
            for (vector<SuperCat *>::iterator k = results.begin(); k < results.end(); ++k) {

              SuperCat *stack_top_cat = *k;

              if (stack_top_cat->m_reduce_id == 0) {
                feasible_y_vals.push_back(m_reduce_base_count + 1);
                assert(feasible_y_vals_map.insert(make_pair(m_reduce_base_count + 1,
                                                            make_pair(2, stack_top_cat))).second);
              } else {
                feasible_y_vals.push_back(m_reduce_base_count + stack_top_cat->m_reduce_id + 1);
                assert(feasible_y_vals_map.insert(make_pair(m_reduce_base_count + stack_top_cat->m_reduce_id + 1,
                                                            make_pair(2, stack_top_cat))).second);
              }
            }
          }
        }

        bool found_gold_action = false;
        if (m_train == 2 && gold_action_id < gold_tree_size && feasible_y_vals.size() == 0) {
          cerr << "greedy training failed, feasible_y_vals.size() == 0" << endl;
          assert(feasible_y_vals.size() > 0);
        }

        if (feasible_y_vals.size() > 0) {
          // has feasible actions, score current context
          Expression state_concat;
          Expression w_state = m_w_stackbuilder->get_h(hypo->m_lstm_w_state).back();
          Expression s_state;
          Expression a_state;
          Expression p_state;
          Expression c_state;
          if (cfg.use_super()) s_state = m_s_stackbuilder->get_h(hypo->m_lstm_s_state).back();
          if (cfg.use_act()) a_state = m_a_stackbuilder->get_h(hypo->m_lstm_a_state).back();
          if (cfg.use_pos()) p_state = m_p_stackbuilder->get_h(hypo->m_lstm_p_state).back();
          if (cfg.use_comp() && !cfg.att()) c_state = m_c_stackbuilder->get_h(hypo->m_c_stack_pointers.back()).back();

          if (cfg.pdrop() != 0.0 && (m_train == 2 || (m_train == 3 && cfg.xf1_dropout()))) {
            w_state = dropout(w_state, cfg.pdrop());
            if (cfg.att_all()) assert(m_w_pointer_droppedout_state_map.insert({(int)hypo->m_lstm_w_state, w_state}).second);
            if (cfg.use_super()) {
              s_state = dropout(s_state, cfg.pdrop());
              if (cfg.att_all()) assert(m_s_pointer_droppedout_state_map.insert({(int)hypo->m_lstm_s_state, s_state}).second);
            }
            if (cfg.use_act()) {
              a_state = dropout(a_state, cfg.pdrop());
              if (cfg.att_all()) assert(m_a_pointer_droppedout_state_map.insert({(int)hypo->m_lstm_a_state, a_state}).second);
            }
            if (cfg.use_pos()) {
              p_state = dropout(p_state, cfg.pdrop());
              if (cfg.att_all()) assert(m_p_pointer_droppedout_state_map.insert({(int)hypo->m_lstm_p_state, p_state}).second);
            }
            if (cfg.use_comp() && !cfg.att()) { c_state = dropout(c_state, cfg.pdrop()); }
          }

          if (cfg.use_super() && cfg.use_act() && cfg.use_pos()) {
            state_concat = concatenate({w_state, s_state, a_state, p_state});
          } else if (cfg.use_super() && cfg.use_act() && !cfg.use_pos()) {
            state_concat = concatenate({w_state, s_state, a_state});
          } else if (cfg.use_super() && !(cfg.use_act()) && !cfg.use_pos()) {
            state_concat = concatenate({w_state, s_state});
          } else {
            state_concat = w_state;
          }

          if (cfg.use_biqueue()) state_concat = concatenate({state_concat, fwds[j], revs[j]});
          else state_concat = concatenate({state_concat, fwds[j]});
          if (cfg.use_comp() && !cfg.att())
            state_concat = concatenate({c_state, state_concat});

          // attention
          if (cfg.att()) {
            vector<Expression> a_weights_unnormal;
            vector<Expression> c_h_states;
            Expression state_matrix;
            Expression context;
            Expression a_weights_normal;
            size_t count = 0;
            for (auto &p : hypo->m_c_stack_pointers) {
              if (++count > 511) break; //todo cnn concatenate_cols doesn't handle >= 512 cols
              c_h_states.push_back(m_c_stackbuilder->get_h(p).back());
              Expression w = i_att * concatenate({m_c_stackbuilder->get_h(p).back(), state_concat});
              a_weights_unnormal.push_back(w);
            }
            if (count != 512) {
              assert(c_h_states.size() == hypo->m_c_stack_pointers.size());
              assert(c_h_states.size() == hypo->GetTotalActionCount() + 1); // +1 for padding
            }
            state_matrix = concatenate_cols(c_h_states);
            a_weights_normal = softmax(concatenate(a_weights_unnormal));
            context = state_matrix * a_weights_normal;
            state_concat = concatenate({context, state_concat});
          } else if (cfg.att_all()) {
            assert(!cfg.att() && !cfg.use_comp());
            Expression queue_state = concatenate({fwds[j], revs[j]}); //todo: assumes biqueue
            Expression w_context, s_context, a_context, p_context;
            const ShiftReduceHypothesis *prev_hypo = hypo->IsStartHypo() ? hypo : hypo->GetPrevHypo();
            gen_context(cg, hypo, hypo->m_w_stack_pointers, 0, i_att_trans_w, i_att_trans_q, i_att_w, queue_state, i_att_w_trans_prev, i_att_w_trans_prev2, prev_hypo->m_w_stack_contexts.back(), w_context, m_w_pointer_droppedout_state_map);
            gen_context(cg, hypo, hypo->m_s_stack_pointers, 1, i_att_trans_s, i_att_trans_q, i_att_s, queue_state, i_att_s_trans_prev, i_att_s_trans_prev2, prev_hypo->m_s_stack_contexts.back(), s_context, m_s_pointer_droppedout_state_map);
            gen_context(cg, hypo, hypo->m_a_stack_pointers, 2, i_att_trans_a, i_att_trans_q, i_att_a, queue_state, i_att_a_trans_prev, i_att_a_trans_prev2, prev_hypo->m_a_stack_contexts.back(), a_context, m_a_pointer_droppedout_state_map);
            gen_context(cg, hypo, hypo->m_p_stack_pointers, 3, i_att_trans_p, i_att_trans_q, i_att_p, queue_state, i_att_p_trans_prev, i_att_p_trans_prev2, prev_hypo->m_p_stack_contexts.back(), p_context, m_p_pointer_droppedout_state_map);
            if (cfg.att_prev_context()) {
              hypo->m_w_stack_contexts.push_back(w_context);
              hypo->m_s_stack_contexts.push_back(s_context);
              hypo->m_a_stack_contexts.push_back(a_context);
              hypo->m_p_stack_contexts.push_back(p_context);
            }
            Expression context_concat = concatenate({w_context, s_context, a_context, p_context});
            state_concat = concatenate({context_concat, state_concat});
            //if (cfg.pdrop() != 0.0 && m_train >= 2) state_concat = dropout(state_concat, cfg.pdrop());
          }

          Expression acth = rectify(affine_transform({i_acth_bias, i_acth, state_concat}));
          //if (m_train == 2) { acth = dropout(acth, cfg.pdrop()); } //todo
          Expression act = affine_transform({i_act_bias, i_act, acth});
          Expression adist = log_softmax(act, feasible_y_vals);
          //display_value(adist, cg, "adist");
          vector<float> adist_v = as_vector(cg.incremental_forward());
          assert(adist_v.size() == m_nclasses);

          double total_score = 0.0;
          if (m_train == 2) {
            unsigned gold_action_class = 0;
            create_n_push_goldact_tuple(gold_action, i, hypo, slen, j, gold_action_rnn_fmt, hypoQueue, found_gold_action, gold_action_class, total_shift_count);
            assert(feasible_y_vals_map.find(gold_action_class) != feasible_y_vals_map.end());
            assert(found_gold_action);
            log_probs.push_back(pick(adist, gold_action_class));

            double best_score = adist_v[feasible_y_vals[0]];
            unsigned best_a = feasible_y_vals[0];
            for (unsigned i = 1; i < feasible_y_vals.size(); ++i) {
              if (adist_v[feasible_y_vals[i]] > best_score) {
                best_score = adist_v[feasible_y_vals[i]];
                best_a = feasible_y_vals[i];
              }
            }
            if (best_a == gold_action_class) ++total_correct;
          } else {
            unordered_map<unsigned, pair<unsigned, SuperCat*> >::const_iterator action_iter;
            for (unsigned action_ind = 0; action_ind < feasible_y_vals.size(); ++action_ind) {
              // TODO this is currently a naive implementation
              // ideally, we don't have to expand every
              // feasible action into the beam??
              total_score = 0.0;
              action_iter = feasible_y_vals_map.find(feasible_y_vals[action_ind]);
              assert(action_iter != feasible_y_vals_map.end());
              pair<unsigned, SuperCat*> feasible_action = action_iter->second;
              total_score = hypo->GetTotalScore() + adist_v[feasible_y_vals[action_ind]];
              Expression act_log_prob = pick(adist, feasible_y_vals[action_ind]);

              // shift
              if (feasible_action.first == SHIFT) {
                assert(j < slen);
                assert(feasible_action.second->cat);
                //feasible_action.second->m_word_vec_ind = word_inds_vec[j];
                ShiftReduceAction *action_shift = new ShiftReduceAction(SHIFT, feasible_action.second);
                HypothesisTuple *new_hypo_tuple = new HypothesisTuple(hypo, action_shift, total_score, i, feasible_y_vals[action_ind], act_log_prob);
                hypoQueue.push(new_hypo_tuple);

                // unary
              } else if (feasible_action.first == UNARY) {
                assert(feasible_action.second->left);
                //assert(hypo->GetStackTopSuperCat()->m_word_vec_ind != -1);
                //feasible_action.second->m_word_vec_ind = hypo->GetStackTopSuperCat()->m_word_vec_ind;
                ShiftReduceAction *action_unary = new ShiftReduceAction(UNARY, feasible_action.second);
                HypothesisTuple *new_hypo_tuple = new HypothesisTuple(hypo, action_unary, total_score, i, feasible_y_vals[action_ind], act_log_prob);
                hypoQueue.push(new_hypo_tuple);

                // reduce
              } else if (feasible_action.first == COMBINE) {

                assert(hypo->GetPrvStack() != 0 && hypo->GetPrvStack()->GetStackTopSuperCat() != 0
                       && hypo->GetStackTopSuperCat() != 0);
                assert(feasible_action.second);
                ++m_total_reduce_count;
                //              compose_word_vec(hypo->GetPrvStack()->GetStackTopSuperCat()->m_word_vec_ind,
                //                             hypo->GetStackTopSuperCat()->m_word_vec_ind, feasible_action.second);
                ShiftReduceAction *action_combine = new ShiftReduceAction(COMBINE, feasible_action.second);
                HypothesisTuple *new_hypo_tuple = new HypothesisTuple(hypo, action_combine, total_score, i, feasible_y_vals[action_ind], act_log_prob);
                hypoQueue.push(new_hypo_tuple);

              } else {
                cerr << "best action not recognized...\n";
                exit (EXIT_FAILURE);
              }
            }
          }
        } else {
          // no more feasible actions, candidate output
          kbest_output_queue.push(hypo);

          if (candidate_output == 0 || (hypo->GetTotalScore() / hypo->GetTotalActionCount()) >
                 (candidate_output->GetTotalScore() / candidate_output->GetTotalActionCount())) {
            candidate_output = hypo;
          }
        }
      } // end for

      if (m_train == 2) assert(!hypoQueue.empty());
      if (!hypoQueue.empty()) {
        best_hypo = 0;
        // apply beam
        construct_hypo(cg, word_inds_vec, pos_inds_vec, hypoQueue, best_hypo, gold_hypo, gold_action, gold_finished);
      }
      if (m_train == 2 && gold_action_id < gold_tree_size)
        ++gold_action_id;
      if (m_train == 2 && gold_action_id == gold_tree_size) {
        assert(total_shift_count == slen);
        gold_finished = true;
        //display_value(log_probs[0], cg, "xxx");
        Expression tot_neglogprob = -sum(log_probs);
        //display_value(tot_neglogprob, cg, "yyy");
        assert(tot_neglogprob.pg != nullptr);
        cout << "### output correct ###" << endl;
        sent.reset();
        chart.pool->clear();
        delete m_lattice;
        return 0;
      }
    } // end while

    if (m_train == 1 || m_train == 3) {
      bool dev_or_train = (m_train == 3) ? true : false;
      float top_f1 = calc_f1(candidate_output, sent_id, dev_or_train);
      cerr << "top output score: " << candidate_output->GetTotalScore() << " f1: " << top_f1 << endl;
      vector<const ShiftReduceHypothesis*> kbest_output_vec;
      kbest_output_vec.reserve(kbest_output_queue.size());
      seq_score_vec.reserve(kbest_output_queue.size());
      f1_vec.reserve(kbest_output_queue.size());

      unsigned ind = 0;
      while (!kbest_output_queue.empty()) {
        const ShiftReduceHypothesis *hypo = kbest_output_queue.top();
        kbest_output_vec.emplace_back(hypo);
        seq_score_vec.emplace_back(hypo->m_total_log_prob);
        arma_seq_score_vec.emplace_back(hypo->GetTotalScore()); //debug
        //display_value(hypo->m_total_log_prob, cg, "debug: hypo->m_total_log_prob exp value");
        float f1 = calc_f1(hypo, sent_id, dev_or_train);
        Expression f1_input = input(cg, f1);
        f1_vec.emplace_back(f1_input);
        arma_f1_vec.emplace_back(f1);
        cerr << "score: " << hypo->GetTotalScore() << " f1: " << f1 << endl;
        if (m_train == 1 && oracle && f1 > top_f1) {
          candidate_output = hypo;
          top_f1 = f1;
        }
        kbest_output_queue.pop();
        ++ind;
        //if (ind == k) break;
      }
      //assert(ind <= k);
      assert(ind == f1_vec.size());
      assert(ind == seq_score_vec.size());
      assert(ind == kbest_output_vec.size());
      total_parses += ind;

      // debug
      fcolvec seq_score_colvec(arma_seq_score_vec);
      fcolvec f1_colvec(arma_f1_vec);
      fcolvec seq_prob_colvec(ind);
      fcolvec xf1_colvec(ind);
      // normalize
      seq_prob_colvec = soft_max(seq_score_colvec);
      // calculate xf1
      xf1_colvec.fill(accu(seq_prob_colvec % f1_colvec));
      cerr << "arma xf1: " << -xf1_colvec(0) << endl;

      // normalize
      Expression seq_prob_vec = softmax(concatenate(seq_score_vec));
      //display_value(seq_prob_vec, cg, "seq_prob_vec");
      // calculate xf1
      Expression xf1 = -sum_cols(transpose(cwise_multiply(seq_prob_vec, concatenate(f1_vec))));
      assert(xf1.pg != nullptr);
      //display_value(xf1, cg, "cnn xf1: ");
      total_xf1 += xf1_colvec(0);
    }

    if (candidate_output != 0)
      return candidate_output;
    return 0;
  } catch (NLP::Exception e) {
    //throw NLP::ParseError(e.msg, nsentences);
    cerr << e.msg << endl;
    return 0;
  }
}

fmat
LSTMParser::_Impl::soft_max(const fmat &y) {
//  mat res = exp(y)/accu(exp(y));
//  return res;
  fmat dev = y - y.max();
  return exp(dev)/accu(exp(dev));
}


void LSTMParser::_Impl::create_n_push_goldact_tuple(ShiftReduceAction *&gold_action,
                                                    const unsigned i, ShiftReduceHypothesis *&hypo,
                                                    const unsigned NWORDS, const unsigned j,
                                                    const pair<unsigned, unsigned> &gold_action_rnn_fmt,
                                                    HypothesisPQueue &hypoQueue,
                                                    bool &found_gold_action,
                                                    unsigned &gold_action_class,
                                                    unsigned &total_shift_count) {
  if (gold_action_rnn_fmt.first == SHIFT) {
    assert(j < NWORDS);
    const Cat *cat = cats.markedup[m_lex_ind_cat_map[gold_action_rnn_fmt.second]];
    assert(cat);
    SuperCat *stackTopCat = SuperCat::Lexical(chart.pool, j+1, cat, 0);
    assert(stackTopCat->cat);
    //stackTopCat->m_word_vec_ind = word_inds_vec[j];
    ShiftReduceAction *actionShift = new ShiftReduceAction(SHIFT, stackTopCat);
    HypothesisTuple *newHypoTuple = new HypothesisTuple(hypo, actionShift, 0.0, i);
    hypoQueue.push(newHypoTuple);

    found_gold_action = true;
    gold_action_class = gold_action_rnn_fmt.second;
    ++total_shift_count;

    // unary
  } else if (gold_action_rnn_fmt.first == UNARY) {
    const SuperCat *sc = hypo->GetStackTopSuperCat();
    vector<SuperCat *> tmp;
    tmp.clear();
    if (!sc->IsLex() && !sc->IsTr()) {
      unary_tc(hypo->GetStackTopSuperCat(), false, tmp);
      unary_tr(sc, tmp);
    } else if (sc->IsLex() && !sc->IsTr()) {
      unary_tr(sc, tmp);
    } else if (!sc->IsLex() && sc->IsTr()) {
      /* do nothing */
    } else if (sc->IsLex() && sc->IsTr()) {
      /* do nothing */
    }

    for (vector<SuperCat *>::iterator iter = tmp.begin(); iter < tmp.end(); ++iter) {
      SuperCat *stack_top_cat = *iter;
      assert(stack_top_cat->left);
      if (stack_top_cat->m_unary_id == gold_action_rnn_fmt.second) {
        found_gold_action = true;
        gold_action_class = gold_action_rnn_fmt.second + m_unary_base_count;
        //stack_top_cat->m_word_vec = hypo->GetStackTopSuperCat()->m_word_vec;
        //stack_top_cat->m_word_vec_ind = hypo->GetStackTopSuperCat()->m_word_vec_ind;
        ShiftReduceAction *actionUnary = new ShiftReduceAction(UNARY, stack_top_cat);
        HypothesisTuple *newHypoTuple = new HypothesisTuple(hypo, actionUnary, 0.0, i);
        hypoQueue.push(newHypoTuple);
      }
    }

    // reduce
  } else if (gold_action_rnn_fmt.first == COMBINE) {
    assert(hypo->GetPrvStack() != 0 && hypo->GetPrvStack()->GetStackTopSuperCat() != 0 && hypo->GetStackTopSuperCat() != 0);
    results.clear();
    if(!cfg.seen_rules() || rule_instances(hypo->GetPrvStack()->GetStackTopSuperCat()->cat, hypo->GetStackTopSuperCat()->cat)) {
      rules(hypo->GetPrvStack()->GetStackTopSuperCat(), hypo->GetStackTopSuperCat(), cfg.eisner_nf(), cfg.seen_rules(), cfg.question_rules(), results);
    }
    assert (results.size() > 0); // have to have at least one result for a gold tree
    for (vector<SuperCat *>::iterator k = results.begin(); k < results.end(); ++k) {
      SuperCat *stack_top_cat = *k;
      if (eqSR(stack_top_cat->GetCat(), gold_action->GetCat())) {
        found_gold_action = true;
        ++m_total_reduce_count;
        //compose_word_vec(hypo->GetPrvStack()->GetStackTopSuperCat()->m_word_vec_ind,
        //                 hypo->GetStackTopSuperCat()->m_word_vec_ind, stack_top_cat);
        ShiftReduceAction *actionCombine = new ShiftReduceAction(COMBINE, stack_top_cat);
        HypothesisTuple *newHypoTuple = new HypothesisTuple(hypo, actionCombine, 0.0, i);
        hypoQueue.push(newHypoTuple);
        if (stack_top_cat->m_reduce_id == 0) {
          gold_action_class = m_reduce_base_count + 1;
        } else {
          gold_action_class = m_reduce_base_count + stack_top_cat->m_reduce_id + 1;
        }
      }
    }
  } else {
    cerr << "gold action not recognized...\n";
    exit (EXIT_FAILURE);
  }
}

//void
//LSTMParser::_Impl::load_pos_str_ind_map(const string &file) {
//
//  cerr << "loading pos str ind map\n";
//
//  ifstream in(file.c_str());
//  if(!in) {
//    cerr << "could not open pos_str_ind file " << file << endl;
//    exit(EXIT_FAILURE);
//  }
//
//  string pos;
//  size_t ind;
//  size_t total = 1;
//
//  while (in >> pos >> ind) {
//    ++total;
//    m_pos_str_ind_map.insert(make_pair(pos, total));
//  }
//
//  cerr << "total pos tag count (from sec0221): " << m_pos_str_ind_map.size() << endl;
//  // pos ind should start from 2, 0 is reserved for empty_pos feature, and 1 for unk_pos
//  // (there shouldn't be unk_pos, but this is for keeping pos feats consistent with suffix feats)
//}

LSTMParser::LSTMParser(void){};

LSTMParser::LSTMParser(cnn::Model &model, cnn::Model &t_model, const LSTMConfig &cfg,
                       Sentence &sent,
                       Categories &cats, const unsigned srbeam, const string &config_dir,
                       const unsigned &train, const bool integrate){
  _impl = new LSTMParser::_Impl(model, t_model, cfg,
                                sent, cats, srbeam, config_dir, train, integrate);
}

void LSTMParser::_Impl::set_stackbuiler_pts(cnn::LSTMBuilder &queue_l2rbuilder,
                                            cnn::LSTMBuilder &queue_r2lbuilder,
                                            cnn::LSTMBuilder &w_stackbuilder,
                                            cnn::LSTMBuilder &s_stackbuilder,
                                            cnn::LSTMBuilder &a_stackbuilder,
                                            cnn::LSTMBuilder &p_stackbuilder) {
  m_queue_l2rbuilder = &queue_l2rbuilder;
  m_queue_r2lbuilder = &queue_r2lbuilder;
  m_w_stackbuilder = &w_stackbuilder;
  m_s_stackbuilder = &s_stackbuilder;
  m_a_stackbuilder = &a_stackbuilder;
  m_p_stackbuilder = &p_stackbuilder;
  //m_c_stackbuilder = &c_stackbuilder;
}

LSTMParser::~LSTMParser(void){ delete _impl; }


void
LSTMParser::load_gold_trees(const std::string &filename) {
  try {
    return _impl->load_gold_trees(filename);
  } catch (...) {
    exit (EXIT_FAILURE);
  }
}

void
LSTMParser::Clear() {
  _impl->Clear();
}

void
LSTMParser::clear_xf1() {
  _impl->clear_xf1();
}


//void
//LSTMParser::sr_parse_train(const double BETA, const unsigned sent) {
//  _impl->sr_parse_train(BETA, sent);
//}


//const ShiftReduceHypothesis*
//LSTMParser::sr_parse_beam_search(const unsigned k, const unsigned sent, const bool oracle, const bool train) {
//  return _impl->sr_parse_beam_search(k, sent, oracle, train);
//}


void
LSTMParser::load_lex_cat_dict(const string &filename) {
  return _impl->load_lex_cat_dict(filename);
}


//void
//LSTMParser::load_pos_str_ind_map(const string &filename) {
//  try {
//    return _impl->load_pos_str_ind_map(filename);
//  } catch (runtime_error) {
//    exit(EXIT_FAILURE);
//  }
//}

void
LSTMParser::load_gold_trees_rnn_fmt(const string &filename) {
  try {
    return _impl->load_gold_trees_rnn_fmt(filename);
  } catch (runtime_error) {
    exit(EXIT_FAILURE);
  }
}

//void
//LSTMParser::train(const unsigned total_epochs, const string &model_path) {
//  _impl->train(total_epochs, model_path);
//}

void
LSTMParser::load_supercat_map(const string &file) {
  _impl->load_supercat_map(file);
}

void
LSTMParser::dump_supercat_map(string &file) {
  _impl->dump_supercat_map(file);
}

//void
//LSTMParser::save_weights(const unsigned epoch, const string &model_path) {
//  _impl->save_weights(epoch, model_path);
//}

void LSTMParser::get_deps(const CCG::SuperCat *sc, const ulong id,
                         float &gold, float &total, const int FMT, bool train) {
  _impl->get_deps(sc, id, gold, total, FMT, train);
}

double
LSTMParser::get_total_sent_gold_deps(const unsigned id, const bool train) {
  return _impl->get_total_sent_gold_deps(id, train);
}


void
LSTMParser::init_xf1_training(bool train) {
  _impl->init_xf1_training(train);
}

void
LSTMParser::change_mode(const unsigned mode) {
  _impl->m_train = mode;
}

void
LSTMParser::lstm_tag(cnn::LSTMBuilder &t_l2rbuilder,
                     cnn::LSTMBuilder &t_r2lbuilder,
                     ComputationGraph& cg,
                     const float beta) {
  _impl->lstm_tag(t_l2rbuilder, t_r2lbuilder, cg, beta);
}
const ShiftReduceHypothesis*
LSTMParser::sr_parse_beam_search_xf1(cnn::LSTMBuilder &t_l2rbuilder,
                                     cnn::LSTMBuilder &t_r2lbuilder,
                                     ComputationGraph& cg,
                                     const unsigned sent_id, const float BETA,
                                     float &total_xf1, float &total_parses,
                                     float &total_acts, float &total_correct,
                                     const bool oracle) {
  return _impl->sr_parse_beam_search_xf1(t_l2rbuilder, t_r2lbuilder,
                                         cg, sent_id, BETA, total_xf1, total_parses, total_acts, total_correct, oracle);
}

void LSTMParser::set_stackbuiler_pts(cnn::LSTMBuilder &queue_l2rbuilder,
                                     cnn::LSTMBuilder &queue_r2lbuilder,
                                     cnn::LSTMBuilder &w_stackbuilder,
                                     cnn::LSTMBuilder &s_stackbuilder,
                                     cnn::LSTMBuilder &a_stackbuilder,
                                     cnn::LSTMBuilder &p_stackbuilder) {
  _impl->set_stackbuiler_pts(queue_l2rbuilder, queue_r2lbuilder, w_stackbuilder, s_stackbuilder, a_stackbuilder, p_stackbuilder);
}

void LSTMParser::calc_tagging_acc() {
  return _impl->calc_tagging_acc();
}

} }
