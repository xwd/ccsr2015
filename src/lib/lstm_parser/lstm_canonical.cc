// C&C NLP tools
// Copyright (c) Universities of Edinburgh, Oxford and Sydney
// Copyright (c) James R. Curran
//
// This software is covered by a non-commercial use licence.
// See LICENCE.txt for the full text of the licence.
//
// If LICENCE.txt is not included in this distribution
// please email candc@it.usyd.edu.au to obtain a copy.

#include "base.h"

#include "hashtable/base.h"
#include "hashtable/frame.h"

#include "parser/fixed.h"
#include "parser/atom.h"
#include "parser/feature.h"
#include "parser/varid.h"
#include "parser/category.h"
#include "parser/variable.h"
#include "lstm_parser/lstm_canonical.h"

#include "share.h"

using namespace std;

namespace NLP { namespace CCG {

using namespace HashTable;

class _LSTMCanonEntry {
protected:
  const Cat *_cat;
  _LSTMCanonEntry *_next;
  unsigned _ind;
public:
  _LSTMCanonEntry(const Cat *cat, _LSTMCanonEntry *next, const unsigned ind):
      _cat(cat), _next(next), _ind(ind) {}
  ~_LSTMCanonEntry(void) { /* do nothing */ }

  void *operator new(size_t size, Pool *pool) { return (void *)pool->alloc(size); }
  void operator delete(void *, Pool *pool) { /* do nothing */ }

  const Cat *canonical(void) const { return _cat; }

  const unsigned ind(void) { return _ind; }

  bool equal(const Cat *cat){
    cerr << "_cat: " << _cat->out_novar_noX_noNB_SR_str() << endl;
    // eq() is less stringent than ==
    // eq() doesn't look at variables
    return eqSR(cat, _cat);
  }

  _LSTMCanonEntry *find(const Cat *cat){
    for(_LSTMCanonEntry *l = this; l != 0; l = l->_next)
      if(l->equal(cat))
        return l;
    return 0;
  }

 _LSTMCanonEntry *next() const {
   return _next;
 }

  ulong nchained(void){
    return _next ? _next->nchained() + 1 : 1;
  }
};

typedef Frame<_LSTMCanonEntry,LARGE,MEDIUM> _ImplBase;
class LSTMCanonical::_Impl: public _ImplBase, public Shared {
public:
  _Impl(const std::string &name): _ImplBase(name) {}
  virtual ~_Impl(void) {}

  const unsigned add(const Cat *cat, const unsigned ind){
    ulong bucket = cat->rhash % _NBUCKETS;
    Entry *entry = _buckets[bucket]->find(cat);
    if(entry)
      return entry->ind();

    entry = new (_ent_pool) Entry(cat, _buckets[bucket], ind);
    _buckets[bucket] = entry;

    assert(ind == size + 2);
    ++size;
    return ind;
  }

  const unsigned add(const Cat *cat){
    ulong bucket = cat->rhash % _NBUCKETS;
    Entry *entry = _buckets[bucket]->find(cat);
    if(entry)
      return entry->ind();

    entry = new (_ent_pool) Entry(cat, _buckets[bucket], size+2);
    _buckets[bucket] = entry;

    unsigned ind = size + 2;
    ++size;
    return ind;
  }

//  const Cat *get(const Cat *cat) const{
//    ulong bucket = cat->rhash % _NBUCKETS;
//    Entry *entry = _buckets[bucket]->find(cat);
//    if(entry)
//      return entry->canonical();
//
//    return 0;
//  }

  const unsigned get_ind(const Cat *cat) const{
    ulong bucket = cat->rhash % _NBUCKETS;
    cerr << cat->out_novar_noX_noNB_SR_str() << " bucket " << bucket << endl;

    Entry *entry = _buckets[bucket]->find(cat);
    if(entry) {
      assert(entry->ind() >= 2);
      return entry->ind();
    }

    return 1;
  }

  const unsigned dump(std::ofstream &out) const {
    unsigned total = 0;
    for(ulong i = 0; i < _NBUCKETS; ++i){
      if(_buckets[i]){
        for(Entry *entry = _buckets[i]; entry != 0; entry = entry->next()) {
          out << entry->canonical()->out_novar_noX_noNB_SR_str() << " "
              << entry->ind() << std::endl;
          ++total;
        }
      }
    }
    return total;
  }

};

LSTMCanonical::LSTMCanonical(void):
    _impl(new _Impl("rnn_canonical")) {}

LSTMCanonical::LSTMCanonical(const LSTMCanonical &other):
    _impl(share(other._impl)) {}

LSTMCanonical &
LSTMCanonical::operator=(LSTMCanonical &other){
  if(_impl != other._impl){
    release(_impl);
    _impl = share(other._impl);
  }

  return *this;
}

LSTMCanonical::~LSTMCanonical(void){
  release(_impl);
}

std::size_t
LSTMCanonical::size(void) const {
  return _impl->size;
}

const unsigned
LSTMCanonical::add(const Cat *cat, const unsigned ind){
  return _impl->add(cat, ind);
}

const unsigned
LSTMCanonical::add(const Cat *cat){
  return _impl->add(cat);
}

const unsigned
LSTMCanonical::get_ind(const Cat *cat) const{
  return _impl->get_ind(cat);
}

const size_t
LSTMCanonical::dump(std::ofstream &out) const {
  return _impl->dump(out);
}

} }
