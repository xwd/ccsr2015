#include "cnn/nodes.h"
#include "cnn/cnn.h"
#include "cnn/training.h"
#include "cnn/timing.h"
#include "cnn/rnn.h"
#include "cnn/gru.h"
#include "cnn/lstm.h"
#include "cnn/dict.h"
#include "cnn/expr.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <random>

#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>

#include "lstm_tagger.h"

using namespace std;
using namespace cnn;
using namespace NLP::Tagger;

namespace NLP {
namespace Taggers {

LSTMTagger::LSTMTagger(const string &model_dir, const string &emb_file,
                       const size_t &de, const size_t &dh, const size_t &dwin, const size_t &layers,
                       const double &pdrop, bool att, const size_t& att_type, const bool att_cache, const bool att_h):
    att(att), pdrop(pdrop), layers(layers), pretrained_dim(de), suf_dim(20), hidden_dim(dh),
    dwin(dwin), tag_size(0), vocab_size(0), local_att_win(5), att_type(att_type), att_cache(att_cache), att_h(att_h) {

  if (att)
    tag_hidden_dim = 200;

  load_ind2rawtag_map(model_dir); // loads from label_ind.txt
  tag_size = m_label2rawtag_map.size();
  load_pretrained_word_emb(emb_file);

  load_suf_str_ind_map(model_dir + "/suf.txt");

  // some constants
  auto iter = m_lex_cat_str_ind_map.find("NNOONNEE");
  assert(iter != m_lex_cat_str_ind_map.end());
  m_UNKNWON_SUPERCAT_IND = iter->second;
  // tag_size = 426 (0-425, including NNOONNEE)
  assert(m_UNKNWON_SUPERCAT_IND == tag_size - 1);
  // vocab size = total pretrained + 3
  m_UNK_ALPHNUM_LOWER_IND = vocab_size - 1;
  m_UNK_ALPHNUM_UPPER_IND = vocab_size - 2;
  m_UNK_NON_ALPHNUM_IND = vocab_size - 3;
  m_UNK_SUFFIX_IND = m_suf_str_ind_map.size();
}

LSTMTagger::~LSTMTagger(void) {}

void LSTMTagger::read_data(IO::ReaderFactory &reader,
                           vector<pair<vector<vector<size_t>>, vector<size_t>>> &data,
                           size_t &total_sents,
                           size_t &ntokens,
                           double &in_pretrained) {
  Sentence sent;
  while(reader.next(sent)) {
    ++total_sents;
    vector<string> &sent_words = sent.words;
    vector<string> &sent_cats = sent.super;
    vector<vector<size_t>> x; // input
    vector<size_t> y; // target
    ntokens += sent.words.size();

    for (size_t i = 0; i < sent.words.size(); ++i) {
      x.push_back(word2vec(sent_words[i]));

      if (x.back()[0] < size_t(vocab_size - 3))
        ++in_pretrained;

      auto iter = m_lex_cat_str_ind_map.find(sent_cats[i]);
      if (iter != m_lex_cat_str_ind_map.end())
        y.push_back(iter->second);
      else
        y.push_back(m_UNKNWON_SUPERCAT_IND);
    }
    assert(x.size() == y.size());
    data.push_back(make_pair(x, y));
    sent.reset();
  }
}

void LSTMTagger::eval_msuper(IO::ReaderFactory &reader,
                             const float beta) {
  Sentence sent;
  reader.reset();
  vector<MultiRaws> all_res;
  vector<size_t> sent_gold_labels;
  vector<vector<size_t> > all_gold_labels;

  while(reader.next(sent)) {
    vector<string> &sent_cats = sent.super;
    //sent_gold_labels.reserve(sent.words.size());

    for (size_t i = 0; i < sent.words.size(); ++i) {
      auto iter = m_lex_cat_str_ind_map.find(sent_cats[i]);
      if (iter != m_lex_cat_str_ind_map.end())
        sent_gold_labels.push_back(iter->second);
      else
        sent_gold_labels.push_back(m_UNKNWON_SUPERCAT_IND);
    }
    assert(sent_gold_labels.size() == sent.words.size());
    all_gold_labels.push_back(sent_gold_labels);
    sent.reset();
    sent_gold_labels.clear();
  }

  vector<float> beta_level_vec;
  vector<float> beta_level({beta});
  vector<float> beta_levels({0.09, 0.08, 0.075, 0.07, 0.06, 0.05, 0.04, 0.03, 0.025, 0.02, 0.01, 0.009,
    0.005, 0.004, 0.003, 0.0025, 0.002, 0.0015, 0.0013, 0.001, 0.0005, 0.00045, 0.0004, 0.00035, 0.00025,
    0.0002, 0.0001, 0.00009, 0.00008, 0.00007, 0.00006, 0.000058, 0.000057, 0.000056, 0.000055,
    0.000054, 0.000053, 0.00005, 0.00004, 0.00003, 0.000028, 0.000025, 0.000022, 0.000021, 0.00002,
    0.000018, 0.000015, 0.000013, 0.000012, 0.000011, 0.00001,
    0.000009, 0.000008, 0.000007, 0.000006, 0.000005, 0.000004, 0.000003, 0.000002,
    0.000001});
  double total_tags = 0.0;
  double total_correct_tags = 0.0;
  double total_correct_sent = 0.0;
  double sent_correct = 0.0;
  double total_mtags = 0.0;

  cerr << std::fixed;
  beta_level_vec = (beta == 0.0) ? beta_levels : beta_level;
  for (size_t b = 0; b < beta_level_vec.size(); ++b) {
    all_res.clear();
    float _beta = beta_level_vec[b];
    cerr  << "beta: " << _beta << endl;
    reader.reset();
    int sent_id = -1;
    while(reader.next(sent)) {
      ++sent_id;
      assert(m_all_sent_dist[sent_id].size() == sent.words.size());
      sent.msuper.resize(sent.words.size());
      for (size_t t = 0; t < sent.words.size(); ++t) {
        MultiRaw &mraw = sent.msuper[t];
        vector<float> &dist = m_all_sent_dist[sent_id][t];
        assert(dist.size() == tag_size);
        float max = dist[0];
        size_t bestv = 5000;
        for (size_t v = 0; v <= dist.size() - 1; ++v){
          if (dist[v] > max) {
            max = dist[v];
            bestv = v;
          }
        }
        float cut_off = max * _beta;
        assert(cut_off >= 0.0);
        for (size_t k = 0; k <= tag_size - 1; ++k) {
          if (dist[k] >= cut_off) {
            auto iter = m_label2rawtag_map.find(k);
            assert(iter != m_label2rawtag_map.end());
            mraw.push_back(ScoredRaw(iter->second, dist[k]));
          }
        }
        // print
        if (_beta > 0.0) {
          cout << sent.words[t] << " " << sent.pos[t] << " " << sent.msuper[t].size() << " ";
          for (size_t j = 0; j < sent.msuper[t].size(); ++j) {
            if (sent.msuper[t][j].score >= cut_off) {
              cout << fixed << setprecision(20) << sent.msuper[t][j].raw << " " << sent.msuper[t][j].score;
            if (j < sent.msuper[t].size() - 1)
              cout << " ";
            }
          }
          cout << endl;
        }
      }
      if (_beta > 0.0) cout << endl;
      all_res.push_back(sent.msuper);
    }
    assert(all_res.size() == all_gold_labels.size());
    total_tags = 0.0;
    total_correct_tags = 0.0;
    total_correct_sent = 0.0;
    total_mtags = 0.0;
    for (size_t k = 0; k < all_gold_labels.size(); ++k) {
      total_tags += all_gold_labels[k].size();
      assert(all_gold_labels[k].size() == all_res[k].size());
      sent_correct = 0.0;

      for (size_t l = 0; l < all_gold_labels[k].size(); ++l) {
        string raw_tag = m_label2rawtag_map.find(all_gold_labels[k][l])->second;
        total_mtags += all_res[k][l].size();
        for (size_t m = 0; m < all_res[k][l].size(); ++m) {
          if (raw_tag == all_res[k][l][m].raw) {
            ++total_correct_tags;
            ++sent_correct;
          }
        }
      }
      if (sent_correct == all_gold_labels[k].size())
        ++total_correct_sent;
    }
    assert(total_correct_tags <= total_tags);
    cerr << "-- total correct tags: " << total_correct_tags << endl;
    cerr << "-- mtag accuracy: " <<  total_correct_tags/total_tags << endl;
    cerr << "-- mtag sent acc: " << total_correct_sent/all_res.size() << endl;
    cerr << "-- mtag avg number of tags: " << total_mtags/total_tags << endl;
  }
}

Expression LSTMTagger::build_tagging_graph(LSTMBuilder &l2rbuilder,
                                           LSTMBuilder &r2lbuilder,
                                           vector<vector<vector<size_t>>> &sent_contextwins,
                                           vector<size_t>& tags,
                                           ComputationGraph& cg,
                                           cnn::Model &model,
                                           double &correct, double &ntagged,
                                           bool eval) {
  const unsigned slen = sent_contextwins.size();
  l2rbuilder.new_graph(cg);  // reset RNN builder for new graph
  l2rbuilder.start_new_sequence();
  r2lbuilder.new_graph(cg);  // reset RNN builder for new graph
  r2lbuilder.start_new_sequence();
  Expression i_l2th = parameter(cg, p_l2th);
  Expression i_r2th = parameter(cg, p_r2th);
  Expression i_thbias = parameter(cg, p_thbias);
  //Expression i_th2t = parameter(cg, p_th2t);
  //Expression i_tbias = parameter(cg, p_tbias);
  vector<Expression> errs;
  vector<Expression> i_words(slen);
  vector<Expression> fwds(slen);
  vector<Expression> revs(slen);
  vector<vector<Expression>> contexts(slen);

  if (!eval) {
    l2rbuilder.set_dropout(pdrop);
    r2lbuilder.set_dropout(pdrop);
  } else {
    l2rbuilder.disable_dropout();
    r2lbuilder.disable_dropout();
  }

  // read sequence from left to right
  for (size_t t = 0; t < slen; ++t) {
    for (size_t i = 0; i < dwin; ++i) {
      contexts[t].push_back(lookup(cg, p_w, sent_contextwins[t][i][0]));
      contexts[t].push_back(lookup(cg, p_suf, sent_contextwins[t][i][1]));
    }
    i_words[t] = concatenate(contexts[t]); // concate all emb cols into a single COL vec
    //i_words[t] = lookup(cg, p_w, sent_contextwins[t][0][0]);
    fwds[t] = l2rbuilder.add_input(i_words[t]);
  }

  // read sequence from right to left
  //r2lbuilder.add_input(lookup(cg, p_w, m_full_wordstr_emb_map["***PADDING***"]));
  for (size_t t = 0; t < slen; ++t) {
    revs[slen - t - 1] = r2lbuilder.add_input(i_words[slen - t - 1]);
  }

  vector<vector<float>> sent_dist;
  for (size_t t = 0; t < slen; ++t) {
    ntagged++;
    //Expression i_th = tanh(affine_transform( { i_thbias, i_l2th, fwds[t], i_r2th, revs[t] }));
    Expression i_t = affine_transform({i_thbias, i_l2th, fwds[t], i_r2th, revs[t]});
    //i_l2th: 15x20, fwds[t]: 20x1 -> 15x1
    //if (!eval) { i_th = dropout(i_th, pdrop); }
    //Expression i_t = affine_transform( { i_tbias, i_th2t, i_th }); //10x15 15x1 (i_th2t: 10x15, 10: #tags, 15: tag hidden size)
    // i_t is a column vec
    if (eval) {
      Expression softmax_dist = softmax(i_t); // compiler warning here, but this is needed
      vector<float> dist = as_vector(cg.incremental_forward());
      sent_dist.push_back(dist);
      float best = -9e99;
      size_t besti = 5000;
      for (size_t i = 0; i < dist.size(); ++i) {
        if (dist[i] > best) {
          best = dist[i];
          besti = i;
        }
      }
      if (tags[t] == besti)
        correct++;
    }
    //if (tags[t] != m_UNKNWON_SUPERCAT_IND) {
    Expression i_err = pickneglogsoftmax(i_t, tags[t]); // tags contains gold tag indexes
    // i_err is just -sum{t_i log(y_i)} without the sum, i.e., neg log likelihood for one position
    errs.push_back(i_err);
  }
  m_all_sent_dist.push_back(sent_dist);
  return sum(errs);
}

Expression LSTMTagger::build_global_attention_tagging_graph(LSTMBuilder &l2rbuilder,
                                                            LSTMBuilder &r2lbuilder,
                                                            vector<vector<vector<size_t>>> &sent_contextwins,
                                                            vector<size_t>& tags,
                                                            ComputationGraph& cg,
                                                            cnn::Model &model,
                                                            double &correct, double &ntagged,
                                                            bool eval) {
  const unsigned slen = sent_contextwins.size();
  l2rbuilder.new_graph(cg);  // reset RNN builder for new graph
  l2rbuilder.start_new_sequence();
  r2lbuilder.new_graph(cg);  // reset RNN builder for new graph
  r2lbuilder.start_new_sequence();
//  Expression i_l2th = parameter(cg, p_l2th);
//  Expression i_r2th = parameter(cg, p_r2th);
  Expression i_l2ah = parameter(cg, p_l2ah);
  Expression i_r2ah = parameter(cg, p_r2ah);
  Expression i_c2ah = parameter(cg, p_c2ah);
  Expression i_tbias = parameter(cg, p_tbias);
  Expression i_th2t = parameter(cg, p_th2t);
  Expression att_w = parameter(cg, p_att_w);
  Expression i_thbias = parameter(cg, p_thbias);
  vector<Expression> i_words(slen);
  vector<vector<Expression>> contexts(slen);
  vector<Expression> fwds(slen);
  vector<Expression> revs(slen);
  vector<Expression> errs;
  vector<vector<Expression>> unnormal_align_weights(slen);

  if (!eval) {
    l2rbuilder.set_dropout(pdrop);
    r2lbuilder.set_dropout(pdrop);
  } else {
    l2rbuilder.disable_dropout();
    r2lbuilder.disable_dropout();
  }

  // read sequence from left to right
  for (unsigned t = 0; t < slen; ++t) {
    for (unsigned i = 0; i < dwin; ++i) {
      contexts[t].push_back(lookup(cg, p_w, sent_contextwins[t][i][0]));
      contexts[t].push_back(lookup(cg, p_suf, sent_contextwins[t][i][1]));
    }
    i_words[t] = concatenate(contexts[t]); // concate all emb cols into a single COL vec
    //i_words[t] = lookup(cg, p_w, sent_contextwins[t][0][0]);
    fwds[t] = l2rbuilder.add_input(i_words[t]);
  }

  // read sequence from right to left
  //r2lbuilder.add_input(lookup(cg, p_w, m_full_wordstr_emb_map["***PADDING***"]));
  for (size_t t = 0; t < slen; ++t) {
    revs[slen - t - 1] = r2lbuilder.add_input(i_words[slen - t - 1]);
  }

//  for (unsigned t = 0; t < slen; ++t) {
//    for (unsigned t1 = 0; t1 < slen; ++t1) {
//      if (t1 == t) continue;
//      if (t1 < t) {
//        // use cached value
//        assert(unnormal_align_weights[t1].size() == slen - 1);
//        unnormal_align_weights[t].push_back(unnormal_align_weights[t1][t - 1]);
//      } else {
//        // no cache, compute
//        unnormal_align_weights[t].push_back(att_w * concatenate({fwds[t], revs[t], fwds[t1], revs[t1]}));
//      }
//    }
//  }


  for (int t = 0; t < slen; ++t) {
    unnormal_align_weights[t].reserve(slen - 1);
    for (int t1 = 0; t1 < slen; ++t1) {
      if (t1 == t) continue;
      Expression w = att_w * concatenate({fwds[t1], revs[t1], fwds[t], revs[t]});
      unnormal_align_weights[t].emplace_back(w);
    }
  }

  vector<Expression> hidden_concat_at_all_t;
  hidden_concat_at_all_t.reserve(slen);
  for (size_t t = 0; t < slen; ++t) {
    hidden_concat_at_all_t.emplace_back(concatenate({fwds[t], revs[t]}));
  }
  Expression hidden_concat = concatenate_cols(hidden_concat_at_all_t);
  vector<unsigned> ts(slen);
  iota(ts.begin(), ts.end(), 0);
  assert(ts.size() == slen);
  vector<vector<float>> sent_dist;
  for (size_t t = 0; t < slen; ++t) {
    ntagged++;
    assert(unnormal_align_weights[t].size() == slen - 1);
    Expression context_v;
    if (slen == 1) {
      context_v = zeroes(cg, {2*hidden_dim, 1});
    } else {
      vector<unsigned> ts_ = ts;
      ts_.erase(ts_.begin() + t);
      Expression normal_align_weights = softmax(concatenate(unnormal_align_weights[t]));
      Expression context_cols = select_cols(hidden_concat, ts_);
      context_v = context_cols * normal_align_weights;
    }
    Expression i_th = rectify(affine_transform({i_tbias, i_l2ah, fwds[t], i_r2ah, revs[t], i_c2ah, context_v}));
    //if (!eval) { i_th = dropout(i_th, pdrop); }
    Expression i_t = affine_transform({i_thbias, i_th2t, i_th});

    Expression softmax_dist;
    if (eval) {
      softmax_dist = softmax(i_t);
      vector<float> dist = as_vector(cg.incremental_forward());
      sent_dist.push_back(dist);
      float best = -9e99;
      size_t besti = 5000;
      for (size_t i = 0; i < dist.size(); ++i) {
        if (dist[i] > best) {
          best = dist[i];
          besti = i;
        }
      }
      if (tags[t] == besti)
        correct++;
    }
    //if (tags[t] != m_UNKNWON_SUPERCAT_IND) {
    Expression i_err = pickneglogsoftmax(i_t, tags[t]); // tags contains gold tag indexes
    // i_err is just -sum{t_i log(y_i)} without the sum, i.e., neg log likelihood for one position
    errs.push_back(i_err);
  }
  m_all_sent_dist.push_back(sent_dist);
  return sum(errs);
}

Expression LSTMTagger::build_local_attention_tagging_graph(LSTMBuilder &l2rbuilder,
                                                           LSTMBuilder &r2lbuilder,
                                                           vector<vector<vector<size_t>>> &sent_contextwins,
                                                           vector<size_t>& tags,
                                                           ComputationGraph& cg,
                                                           cnn::Model &model,
                                                           double &correct, double &ntagged,
                                                           bool eval) {
  std::unordered_map<std::pair<int, int>, Expression, KeyHash, KeyEqual> att_weights_map;
  const int slen = sent_contextwins.size();
  l2rbuilder.new_graph(cg);  // reset RNN builder for new graph
  l2rbuilder.start_new_sequence();
  r2lbuilder.new_graph(cg);  // reset RNN builder for new graph
  r2lbuilder.start_new_sequence();
//  Expression i_l2th = parameter(cg, p_l2th);
//  Expression i_r2th = parameter(cg, p_r2th);
  Expression i_l2ah = parameter(cg, p_l2ah); // left(forward) hidden of target position 2 att_hidden
  Expression i_r2ah = parameter(cg, p_r2ah);
  Expression i_c2ah = parameter(cg, p_c2ah); // context hidden to att_hidden
  Expression i_tbias = parameter(cg, p_tbias); // att_hidden bias
  Expression i_th2t = parameter(cg, p_th2t); // tag output layer
  Expression att_w = parameter(cg, p_att_w);
  Expression i_thbias = parameter(cg, p_thbias); // final bias
  Expression i_l2t = parameter(cg, p_l2th);
  Expression i_r2t = parameter(cg, p_r2th);
  Expression i_c2t = parameter(cg, p_c2t);
  vector<Expression> errs;
  vector<Expression> i_words(slen);
  vector<Expression> fwds(slen);
  vector<Expression> revs(slen);
  vector<vector<Expression>> contexts(slen);
  vector<vector<Expression>> unnormal_align_weights(slen);
  int half_win = std::floor(local_att_win/2);

  if (!eval) {
    l2rbuilder.set_dropout(pdrop);
    r2lbuilder.set_dropout(pdrop);
  } else {
    l2rbuilder.disable_dropout();
    r2lbuilder.disable_dropout();
  }

  // read sequence from left to right
  for (int t = 0; t < slen; ++t) {
    for (unsigned i = 0; i < dwin; ++i) {
      contexts[t].push_back(lookup(cg, p_w, sent_contextwins[t][i][0]));
      contexts[t].push_back(lookup(cg, p_suf, sent_contextwins[t][i][1]));
    }
    i_words[t] = concatenate(contexts[t]); // concate all emb cols into a single COL vec
    //i_words[t] = lookup(cg, p_w, sent_contextwins[t][0][0]);
    fwds[t] = l2rbuilder.add_input(i_words[t]);
  }

  // read sequence from right to left
  //r2lbuilder.add_input(lookup(cg, p_w, m_full_wordstr_emb_map["***PADDING***"]));
  for (int t = 0; t < slen; ++t) {
    revs[slen - t - 1] = r2lbuilder.add_input(i_words[slen - t - 1]);
  }

  if (att_cache) {
    // target on the left or right
    for (int t = 0; t < slen; ++t) {
      unnormal_align_weights[t].reserve(2*half_win);
      for (int t1 = t - half_win; t1 <= t + half_win; ++t1) {
        if (t1 == t) continue;
        int x, y = 0;
        if (t1 < t) { x = t1; y = t; }
        else { x = t; y = t1; }
        auto iter = att_weights_map.find(make_pair(x, y));
        if (iter == att_weights_map.end()) {
          Expression w;
          if (t1 < 0) {
            w = att_w * concatenate({zeroes(cg, {hidden_dim*2}), fwds[t], revs[t]});
          } else if (t1 >= slen) {
            w = att_w * concatenate({fwds[t], revs[t], zeroes(cg, {hidden_dim*2})});
          } else {
            w = att_w * concatenate({fwds[x], revs[x], fwds[y], revs[y]});
          }
          att_weights_map.insert({make_pair(x, y), w});
          unnormal_align_weights[t].push_back(w);
        } else {
          unnormal_align_weights[t].push_back(iter->second);
          //display_value(unnormal_align_weights[t].back(), cg, "123");
        }
      }
    }
  } else {
    // target t always on the right
    for (int t = 0; t < slen; ++t) {
      unnormal_align_weights[t].reserve(2*half_win);
      for (int t1 = t - half_win; t1 <= t + half_win; ++t1) {
        if (t1 == t) continue;
        Expression w;
        if (t1 < 0 || t1 >= slen) {
          w = att_w * concatenate({zeroes(cg, {hidden_dim*2}), fwds[t], revs[t]});
        } else {
          w = att_w * concatenate({fwds[t1], revs[t1], fwds[t], revs[t]});
        }
        unnormal_align_weights[t].push_back(w);
      }
    }
  }

  vector<Expression> hidden_concat_at_all_t;
  hidden_concat_at_all_t.reserve(slen);
  for (int t = 0; t < slen; ++t) {
    hidden_concat_at_all_t.emplace_back(concatenate({fwds[t], revs[t]}));
  }
  Expression hidden_concat = concatenate_cols(hidden_concat_at_all_t);
  vector<vector<float>> sent_dist;
  for (int t = 0; t < slen; ++t) {
    ntagged++;
    vector<Expression> padding;
    vector<unsigned> ts;
    ts.reserve(local_att_win - 1);
    for (int t1 = t - half_win; t1 <= t + half_win; ++t1) {
      if (t1 == t) continue;
      if (t1 < 0 || t1 >= slen) {
        padding.push_back(zeroes(cg, {hidden_dim*2}));
        continue;
      }
      ts.push_back(t1);
    }

    assert(unnormal_align_weights[t].size() == local_att_win - 1);
    Expression normal_align_weights = softmax(concatenate(unnormal_align_weights[t]));
//    display_value(unnormal_align_weights[t][0], cg, "unnormal");
//    display_value(unnormal_align_weights[t][1], cg, "unnormal");
    //display_value(concatenate(unnormal_align_weights[t]), cg, "unnormal_concat");
    Expression context_cols = select_cols(hidden_concat, ts);
    if (t < half_win)
      context_cols = concatenate_cols({concatenate_cols(padding), context_cols});
    else if (t + half_win >= slen)
      context_cols = concatenate_cols({context_cols, concatenate_cols(padding)});
    Expression context_v = context_cols * normal_align_weights;
    Expression i_th, i_t;
    if (att_h) {
      i_th = rectify(affine_transform({i_tbias, i_l2ah, fwds[t], i_r2ah, revs[t], i_c2ah, context_v}));
    //if (!eval) { i_th = dropout(i_th, pdrop); }
      i_t = affine_transform({i_thbias, i_th2t, i_th});
    } else {
      i_t = affine_transform({i_thbias, i_l2t, fwds[t], i_r2t, revs[t], i_c2t, context_v});
    }

    Expression softmax_dist;
    if (eval) {
      softmax_dist = softmax(i_t);
      vector<float> dist = as_vector(cg.incremental_forward());
      sent_dist.push_back(dist);
      float best = -9e99;
      size_t besti = 5000;
      for (size_t i = 0; i < dist.size(); ++i) {
        if (dist[i] > best) {
          best = dist[i];
          besti = i;
        }
      }
      if (tags[t] == besti)
        correct++;
    }
    //if (tags[t] != m_UNKNWON_SUPERCAT_IND) {
    Expression i_err = pickneglogsoftmax(i_t, tags[t]); // tags contains gold tag indexes
    // i_err is just -sum{t_i log(y_i)} without the sum, i.e., neg log likelihood for one position
    errs.push_back(i_err);
  }
  m_all_sent_dist.push_back(sent_dist);
  return sum(errs);
}

void
LSTMTagger::decode(IO::ReaderFactory &reader,
                   const string &model_dir,
                   const string &fname,
                   const float beta) {
  cnn::Initialize(512);
  cnn::Model model;

  LSTMBuilder l2rbuilder(layers, (pretrained_dim + suf_dim)*dwin, hidden_dim, &model);
  LSTMBuilder r2lbuilder(layers, (pretrained_dim + suf_dim)*dwin, hidden_dim, &model);

  p_w = model.add_lookup_parameters(vocab_size, {pretrained_dim});
  p_suf = model.add_lookup_parameters(m_suf_str_ind_map.size() + 1, {suf_dim});
//  for (auto it : m_ind_pretrained_emb_map)
//    p_w->Initialize(it.first, it.second);
  if (att) {
    p_l2ah = model.add_parameters({tag_hidden_dim, hidden_dim}); // l 2 att hidden
    p_r2ah = model.add_parameters({tag_hidden_dim, hidden_dim}); // r 2 att hidden
    p_c2ah = model.add_parameters({tag_hidden_dim, hidden_dim*2}); // context 2 att hidden
    p_tbias = model.add_parameters({tag_hidden_dim});
    p_th2t = model.add_parameters({tag_size, tag_hidden_dim});
    p_att_w = model.add_parameters({1, hidden_dim*2*2}); // two directions and two places (target position plus another position)
    p_l2th = model.add_parameters({tag_size, hidden_dim});
    p_r2th = model.add_parameters({tag_size, hidden_dim});
    p_c2t = model.add_parameters({tag_size, hidden_dim*2});
  } else {
    p_l2th = model.add_parameters({tag_size, hidden_dim});
    p_r2th = model.add_parameters({tag_size, hidden_dim});
  }
  p_thbias = model.add_parameters({tag_size});

  cerr << "model params: " << model_dir + "/" + fname << endl;
  ifstream in(model_dir + "/" + fname);
  boost::archive::text_iarchive ia(in);
  ia >> model;

  Sentence sent;
  vector<pair<vector<vector<size_t>>, vector<size_t>>> dev;
  size_t ntokens00 = 0;
  size_t total_dev_sents = 0;
  double in_pretrained = 0.0;

  read_data(reader, dev, total_dev_sents, ntokens00, in_pretrained);
  cerr << "total tokens in dev data: " << ntokens00 << endl;
  cerr << "pre-trained embedding coverage on dev data: " << in_pretrained / (double)ntokens00 << endl;
  cerr << "total dev sents: " << total_dev_sents << endl;

  double dloss = 0;
  double dtags = 0;
  double dcorr = 0;
  //p_th2t->scale_parameters(pdrop);
  for (auto &sent : dev) {
    ComputationGraph cg;
    vector<vector<vector<size_t>>> contextwins;
    sent2contextwin(sent.first, contextwins);
    if (att && att_type == 0)
      build_local_attention_tagging_graph(l2rbuilder, r2lbuilder, contextwins, sent.second, cg, model, dcorr, dtags, true);
    else if (att && att_type == 1)
      build_global_attention_tagging_graph(l2rbuilder, r2lbuilder, contextwins, sent.second, cg, model, dcorr, dtags, true);
    else
      build_tagging_graph(l2rbuilder, r2lbuilder, contextwins, sent.second, cg, model, dcorr, dtags, true);
    //build_tagging_graph(l2rbuilder, r2lbuilder, contextwins, sent.second, cg, model, dcorr, dtags, true);
    dloss += as_scalar(cg.forward());
  }
  //p_th2t->scale_parameters(1/pdrop);
  cerr << "total correct: " << dcorr <<  " total tags evaluated: " << dtags << endl;
  cerr  << "E = " << (dloss / dtags) << " ppl=" << exp(dloss / dtags)  << " acc=" << (dcorr / dtags) << ' ' << endl;
  eval_msuper(reader, beta);
}

void LSTMTagger::run(IO::ReaderFactory &reader,
                     IO::ReaderFactory &reader_dev,
                     const string& model_dir,
                     const string &model_fname) {
  cnn::Initialize(512);
  cnn::Model model;

  LSTMBuilder l2rbuilder(layers, (pretrained_dim + suf_dim)*dwin, hidden_dim, &model);
  LSTMBuilder r2lbuilder(layers, (pretrained_dim + suf_dim)*dwin, hidden_dim, &model);

  p_w = model.add_lookup_parameters(vocab_size, {pretrained_dim});
  p_suf = model.add_lookup_parameters(m_suf_str_ind_map.size() + 1, {suf_dim});
  for (auto it : m_ind_pretrained_emb_map)
    p_w->Initialize(it.first, it.second);
  if (att) {
    p_l2ah = model.add_parameters({tag_hidden_dim, hidden_dim}); // l 2 att hidden
    p_r2ah = model.add_parameters({tag_hidden_dim, hidden_dim}); // r 2 att hidden
    p_c2ah = model.add_parameters({tag_hidden_dim, hidden_dim*2}); // context 2 att hidden
    p_tbias = model.add_parameters({tag_hidden_dim});
    p_th2t = model.add_parameters({tag_size, tag_hidden_dim});
    p_att_w = model.add_parameters({1, hidden_dim*2*2}); // two directions and two places (target position plus another position)
    p_l2th = model.add_parameters({tag_size, hidden_dim});
    p_r2th = model.add_parameters({tag_size, hidden_dim});
    p_c2t = model.add_parameters({tag_size, hidden_dim*2});
  } else {
    p_l2th = model.add_parameters({tag_size, hidden_dim});
    p_r2th = model.add_parameters({tag_size, hidden_dim});
  }
  p_thbias = model.add_parameters({tag_size});
  // TODO: this should be tbias and the above should be th (do not touch this now, since
  // the baseline model has no tag_hidden layer, and has been trained with p_thbias at the
  // output layer already)

  bool use_momentum = false;
  sgd = nullptr;
  if (use_momentum)
    sgd = new MomentumSGDTrainer(&model);
  else {
    sgd = new SimpleSGDTrainer(&model);
    sgd->eta = 0.1;
    sgd->eta_decay = 0.08;
  }

  Sentence sent;
  vector<pair<vector<vector<size_t>>, vector<size_t>>> training, dev;
  size_t ntokens0221 = 0;
  size_t ntokens00 = 0;
  size_t total_train_sents = 0;
  size_t total_dev_sents = 0;
  double in_pretrained = 0.0;

  read_data(reader, training, total_train_sents, ntokens0221, in_pretrained);
  cerr << "total tokens in training data: " << ntokens0221 << endl;
  cerr << "pre-trained embedding coverage on training data: " << in_pretrained / (double)ntokens0221 << endl;
  cerr << "total training sents: " << total_train_sents << endl;

  in_pretrained = 0.0;
  read_data(reader_dev, dev, total_dev_sents, ntokens00, in_pretrained);
  cerr << "total tokens in dev data: " << ntokens00 << endl;
  cerr << "pre-trained embedding coverage on dev data: " << in_pretrained / (double)ntokens00 << endl;
  cerr << "total dev sents: " << total_dev_sents << endl;

  ostringstream os;
  os << "tagger" << '_' << layers << '_' << pretrained_dim << '_' << hidden_dim
      << "-pid" << getpid() << ".params";
  string fname;
  if (model_fname == "")
    fname = os.str();
  else
    fname = model_fname;
  cerr << "Parameters will be written to: " << fname << endl;
  float best = 0.0;

//  ifstream in(model_dir);
//  boost::archive::text_iarchive ia(in);
//  ia >> model;

  unsigned report_every_i = training.size();
  unsigned dev_every_i_reports = 1;
  unsigned si = training.size();
  vector<unsigned> order(training.size());
  for (unsigned i = 0; i < order.size(); ++i)
    order[i] = i;
  bool first = true;
  int report = 0;
  unsigned lines = 0;
  while (1) {
    Timer iteration("completed in");
    double loss = 0;
    double ttags = 0;
    double correct = 0;
    for (unsigned i = 0; i < report_every_i; ++i) {
      if (si == training.size()) {
        si = 0;
        if (first) {
          first = false;
        } else {
          sgd->update_epoch();
        }
        cerr << "**SHUFFLE\n";
        shuffle(order.begin(), order.end(), *rndeng);
      }

      // build graph for this instance
      ComputationGraph cg;
      auto& sent = training[order[si]];
      ++si;
      //cerr << "sent: " << si << endl;
      vector<vector<vector<size_t>>> contextwins;
      sent2contextwin(sent.first, contextwins);
      if (att && att_type == 0)
        build_local_attention_tagging_graph(l2rbuilder, r2lbuilder, contextwins, sent.second, cg, model, correct, ttags, false);
      else if (att && att_type == 1)
        build_global_attention_tagging_graph(l2rbuilder, r2lbuilder, contextwins, sent.second, cg, model, correct, ttags, false);
      else
        build_tagging_graph(l2rbuilder, r2lbuilder, contextwins, sent.second, cg, model, correct, ttags, false);
      loss += as_scalar(cg.forward());
      cg.backward();
      if (sgd->epoch < 10)
        sgd->eta = 0.1;
      sgd->update(1.0);
      ++lines;
    }
    sgd->status();
    cerr << " E = " << (loss / ttags) << " ppl=" << exp(loss / ttags)
        << " (acc=" << (correct / ttags) << ") ";

    // show score on dev data?
    report++;
    if (report % dev_every_i_reports == 0) {
      double dloss = 0.0;
      double dtags = 0.0;
      double dcorr = 0.0;
      double acc = 0.0;
      //if (att) p_th2t->scale_parameters(pdrop);
      for (auto &sent : dev) {
        ComputationGraph cg;
        vector<vector<vector<size_t>>> contextwins;
        sent2contextwin(sent.first, contextwins);
        if (att && att_type == 0)
          build_local_attention_tagging_graph(l2rbuilder, r2lbuilder, contextwins, sent.second, cg, model, dcorr, dtags, true);
        else if (att && att_type == 1)
          build_global_attention_tagging_graph(l2rbuilder, r2lbuilder, contextwins, sent.second, cg, model, dcorr, dtags, true);
        else
          build_tagging_graph(l2rbuilder, r2lbuilder, contextwins, sent.second, cg, model, dcorr, dtags, true);
        //build_tagging_graph(l2rbuilder, r2lbuilder, contextwins, sent.second, cg, model, dcorr, dtags, true);
        dloss += as_scalar(cg.forward());
      }
      //if (att) p_th2t->scale_parameters(1/pdrop);
      acc = dcorr / dtags;
      if (acc > best) {
        best = acc;
        ofstream out(fname);
        boost::archive::text_oarchive oa(out);
        oa << model;
      }
      cerr << "\n***DEV [epoch=" << (lines / (double) training.size())
          << "] E = " << (dloss / dtags) << " ppl=" << exp(dloss / dtags)
          << " acc=" << (dcorr / dtags) << ' ';
    }
  }
  delete sgd;
}

void LSTMTagger::load_ind2rawtag_map(const string& model_dir) {
  string file = model_dir + "/label_ind.txt";
  ifstream in(file.c_str());
  if (!in) {
    cerr << "no such file " << file << endl;
    exit (EXIT_FAILURE);
  }
  string line;
  while (in) {
    if (!getline(in, line)) break;
    istringstream iss(line);
    string label;
    int ind;
    while (iss >> label >> ind) {
      m_label2rawtag_map.insert(make_pair(ind, label));
      m_lex_cat_str_ind_map.insert(make_pair(label, ind));
    }
    assert(m_label2rawtag_map.size() == m_lex_cat_str_ind_map.size());
  }
  cerr << "total number of lex cat classes (including NNOONNEE): " << m_label2rawtag_map.size() << endl;
}

void LSTMTagger::load_pretrained_word_emb(const string& file) {
  cerr << "loading emb mat..., " << file << endl;
  ifstream in(file.c_str());
  size_t total = 0;

  if (!in) {
    cerr << "no such file: " << file;
    exit (EXIT_FAILURE);
  }

  string line;
  while (in) {
    if (!getline(in, line)) break;
    istringstream iss(line);

    size_t first_space_pos = line.find(" ");
    string word = line.substr(0, first_space_pos);
    string emb_vec = line.substr(first_space_pos + 1);

    vector<float> v(pretrained_dim, 0);
    istringstream lin(emb_vec);
    for (size_t i = 0; i < pretrained_dim; ++i)
      lin >> v[i];
    if (total == 0)
      assert(word == "***PADDING***");
    assert(m_full_wordstr_emb_map.insert(make_pair(word, total)).second);
    m_ind_pretrained_emb_map.insert(make_pair(total, v));
    ++total;
  }

  std::mt19937_64 engine;
  std::uniform_real_distribution<double> uni(-1.0, 1.0);
  auto gen = std::bind(uni, engine);

  for (size_t i = 0; i < 3; ++i) {
    vector<float> vec(pretrained_dim);
    generate(begin(vec), end(vec), gen);
    m_ind_pretrained_emb_map.insert(make_pair(total + i, vec));
  }

  vocab_size = total + 3;
  cerr << "total emb (no unks) " << total << endl;
}

void
LSTMTagger::load_suf_str_ind_map(const string &file) {

  cerr << "loading suf str ind map\n";
  ifstream in(file.c_str());
  if(!in) {
    cerr << "could not open suf_str_ind file " << file << endl;
    exit(EXIT_FAILURE);
  }
  string suf;
  size_t temp;
  size_t total = 0;
  while (in >> suf >> temp) {
    m_suf_str_ind_map.insert(make_pair(suf, total));
    ++total;
  }
  cerr << "total suffix count (from sec0221): " << total << endl;
}


vector<size_t> LSTMTagger::word2vec(string &word) const {
  vector<size_t> word_vec;
  string original_word = word;
  string word_copy = word;

  std::transform(word_copy.begin(), word_copy.end(), word_copy.begin(), ::tolower);
  unordered_map<string, size_t>::const_iterator iter = m_full_wordstr_emb_map.find(word_copy);

  if (iter != m_full_wordstr_emb_map.end()) {
    word_vec.push_back(iter->second);
  } else {
    // not in pre-trained embeddings, try to back off
    bool backed_off = false;
    vector<string> back_off_strs;
    clean_str(original_word, back_off_strs);
    for (size_t i = 0; i < back_off_strs.size(); ++i) {
      iter = m_full_wordstr_emb_map.find(back_off_strs[i]);
      if (iter != m_full_wordstr_emb_map.end()) {
        backed_off = true;
        word_vec.push_back(iter->second);
        break;
      }
    }

    if (!backed_off) {
      // unknown word
      if (is_alphnum(original_word)) {
        if (islower(original_word[0]))
          word_vec.push_back(m_UNK_ALPHNUM_LOWER_IND);
        else
          word_vec.push_back(m_UNK_ALPHNUM_UPPER_IND);
      } else {
        word_vec.push_back(m_UNK_NON_ALPHNUM_IND);
      }
    }
  }

  unordered_map<string, size_t>::const_iterator iter2 = m_suf_str_ind_map.find(get_suf(original_word));
  if (iter2 != m_suf_str_ind_map.end()) {
    word_vec.push_back(iter2->second);
    //cerr << "suf: " << iter2->second << endl;
  }
  else
    word_vec.push_back(m_UNK_SUFFIX_IND);

//  if (islower(original_word[0]))
//    word_vec.push_back(0);
//  else
//    word_vec.push_back(1);

  assert(word_vec[0] < vocab_size + 3);
  return word_vec;
}

void LSTMTagger::clean_str(string &word, vector<string> &ret) const {
  string word_copy1 = word;
  if (!is_lower(word)) {
    std::transform(word_copy1.begin(), word_copy1.end(), word_copy1.begin(), ::tolower);
    ret.push_back(word_copy1);
  }

  string word_copy2 = word;
  replace_all_substrs(word_copy2, ",", "");
  replace_all_substrs(word_copy2, ".", "");
  replace_all_substrs(word_copy2, "-", "");
  replace_all_substrs(word_copy2, " ", "");
  replace_all_substrs(word_copy2, "\\/", "");

  if (all_of(word_copy2.begin(), word_copy2.end(), ::isdigit)) {
    ret.push_back("0");
  } else if (word.find('-') != string::npos) {
    size_t last_pos = word.find_last_of('-');
    assert(last_pos != string::npos);
    string sub_str = word.substr(last_pos + 1);
    ret.push_back(sub_str);

    if (!is_lower(sub_str)) {
      std::transform(sub_str.begin(), sub_str.end(), sub_str.begin(), ::tolower);
      ret.push_back(sub_str);
    }

  } else {
    size_t pos = word.find("\\/");
    if (pos != string::npos) {
      size_t last_pos = word.find_last_of("/");
      string sub_str = word.substr(last_pos + 1);
      ret.push_back(sub_str);

      if (!is_lower(sub_str)) {
        std::transform(sub_str.begin(), sub_str.end(), sub_str.begin(), ::tolower);
        ret.push_back(sub_str);
      }
    }
  }
}

void LSTMTagger::replace_all_substrs(string &str,
                                     const string &substr,
                                     const string &re) const {
  string::size_type n = 0;

  while ((n = str.find(substr, n)) != std::string::npos) {
    str.replace(n, substr.size(), re);
    n += substr.size();
  }
}

bool LSTMTagger::is_lower(const string &word) const {
  for (size_t i = 0; i < word.length(); ++i) {
    if (isupper(word[i]))
      return false;
  }
  return true;
}


bool LSTMTagger::is_alphnum(const string &word) const {
  for (size_t i = 0; i < word.length(); ++i) {
    if (!isalnum(word[i]))
      return false;
  }
  return true;
}

string LSTMTagger::get_suf(const string &word) const {
  string word_copy = word;
  replace_all_substrs(word_copy, ",", "");
  replace_all_substrs(word_copy, ".", "");
  replace_all_substrs(word_copy, "-", "");
  replace_all_substrs(word_copy, " ", "");
  replace_all_substrs(word_copy, "\\/", "");
  if (all_of(word_copy.begin(), word_copy.end(), ::isdigit))
    return "0";
  else {
    if (word.length() >= 2)
      return word.substr(word.length() - 2);
    else
      return word;
  }
}

void LSTMTagger::sent2contextwin(const vector<vector<size_t>> &sent,
                                 vector<vector<vector<size_t>>> &contextwins) {
  assert(dwin % 2 == 1);
  size_t pad_len = dwin/2;
  vector<size_t> unk_vec;
  unk_vec.push_back(0);
  unk_vec.push_back(m_UNK_SUFFIX_IND);

  for (size_t i = 0; i < sent.size(); ++i) {
    vector<vector<size_t> > contextwin;
    for (size_t j = i, k = 0; j < i + dwin; ++j, ++k) {
      if (j < pad_len || j >= pad_len + sent.size()) {
        contextwin.push_back(unk_vec);
      } else {
        contextwin.push_back(sent[j - pad_len]);
      }
    }
    contextwins.push_back(contextwin);
  }
}


} }
