// Copyright (c) Cambridge University Computer Lab
// Copyright (c) Wenduan Xu

#include <algorithm>
#include <sstream>
#include <string>
#include <unordered_map>
#include <stdexcept>
#include <random>
#include <cassert>
#include <time.h>
#include <limits>
#include <cstdlib>
#include <stdlib.h>
#include <queue>
#include "rnn_tagger.h"

using namespace std;
using namespace arma;
using namespace NLP::Tagger;

// assumes the padding embedding *UNKNOWN* is in the embedding file, and change its
// name in the embedding file to ***PADDING***
// other three types of unknowns (upper lower, non-alphnum) are the last three columns
// in the embedding matrix

// suf ind should start from 0, last col is reserved for unknown suf

// in (raw_lex_cat, ind) file, ind starts from 0

// when tagging at test time, DO NOT output NNOONNEE tag

// NNOONNEE tag should always be placed at the end of label_ind.txt

namespace NLP { namespace Taggers {

// for testing after a model has been trained
//RnnTagger::RnnTagger(int nh, int vocsize, int nclasses, int cs, int ws, int suffix_count,
//                     const string &wx, const string &wh, const string &wy,
//                     const string &emb, const string &suf, const string &cap)
//: m_cs(cs), m_ws(ws), m_vocsize(vocsize), m_suffix_count(suffix_count),
//  m_nclasses(nclasses), m_ntokens0221(0), m_ntokens00(0), m_ntokens23(0) {
//  m_wx.load(wx);
//  m_wh.load(wh);
//  m_wy.load(wy);
//  m_emb.load(emb);
//  m_suf.load(suf);
//  m_cap.load(cap);
//
//  m_h_tm1.set_size(1, nh);
//  m_h_tm1.zeros();
//  m_y.set_size(1, nclasses);
//  m_y.zeros();
//
//  load_ind2rawtag_map();
//}

// for training a model
RnnTagger::RnnTagger(size_t de, size_t ds, size_t dc,
                     size_t cs, size_t nh, size_t bs,
                     const string &all_untouched_embeddings,
                     const string &suf_str_ind_file,
                     const string &model_dir,
                     bool lower_case_words,
                     size_t nepochs,
                     bool dropout,
                     double dropout_success_prob,
                     bool verbose,
                     double lr,
                     double clip,
                     string act,
                     bool bi)
: m_de(de), m_ds(ds), m_dc(dc), m_cs(cs), m_nh(nh), m_bs(bs), m_model_dir(model_dir),
  m_lowercase_words(lower_case_words), m_nepoch(nepochs), m_dropout(dropout),
  m_dropout_success_prob(dropout_success_prob), m_verbose(verbose), m_lr(lr),
  m_act(act), m_clip(clip), m_bi(bi) {

  cout << "time precision (machine dependent): " << chrono::high_resolution_clock::period::den << endl;
  cerr << "para: " << endl;
  cerr << "lr: " << lr << endl;
  cerr << "nh: " << nh << endl;
  cerr << "de: " << de << endl;
  cerr << "ds: " << ds << endl;
  cerr << "dc: " << dc << endl;
  cerr << "cs: " << cs << endl;
  cerr << "bs: " << bs << endl;
  cerr << "dropout: " << dropout << endl;
  cerr << "dropout success prob: " << m_dropout_success_prob << endl;

  load_ind2rawtag_map(model_dir);
  m_nclasses = m_label2rawtag_map.size();

  std::mt19937_64 engine;
  std::uniform_real_distribution<double> uni(-1.0, 1.0);
  //std::uniform_real_distribution<double> uni_bi(-0.01, 0.01);

  m_vocsize = 0;
  string line;
  ifstream emb_file(all_untouched_embeddings);
  if (!emb_file) {
    cerr << "no such file " << all_untouched_embeddings << endl;
    exit (EXIT_FAILURE);
  }
  while (getline(emb_file, line))
    ++m_vocsize;

  m_emb.set_size(m_de, m_vocsize + 3);
  m_emb.imbue( [&]() { return uni(engine); } );
  m_emb = m_emb * 0.2;

  try {
    load_emb_mat(m_emb, all_untouched_embeddings);
  } catch (runtime_error e) {
    cerr << e.what() << endl;
    exit(EXIT_FAILURE);
  }

  unordered_map<string, size_t>::const_iterator iter = m_full_wordstr_emb_map.find("***PADDING***");
  assert(iter != m_full_wordstr_emb_map.end());

  load_suf_str_ind_map(suf_str_ind_file);
  m_suffix_count = m_suf_str_ind_map.size();
  m_suf.set_size(ds, m_suffix_count + 1);
  m_suf.imbue( [&]() { return uni(engine); } );
  m_suf *= 0.2;

  m_cap.set_size(m_dc, 2);
  m_cap.imbue( [&]() { return uni(engine); } );
  m_cap *= 0.2;

  std::uniform_real_distribution<double> uni1(-2.0 / double((m_de + m_ds + m_dc) * m_cs) , 2.0 / double((m_de + m_ds + m_dc) * m_cs));
  m_wx.set_size((m_de + m_ds + m_dc)*m_cs, m_nh);
  m_wx.imbue( [&]() { return uni1(engine); } );
  //m_wx *= 0.2;

  std::uniform_real_distribution<double> uni2(-2.0 / double(m_nh), 2.0 / double(m_nh));
  if (m_bi) {
    m_wy.set_size(m_nh*2, m_nclasses);
    m_wy.imbue( [&]() { return uni2(engine); } );
  } else {
    m_wy.set_size(m_nh, m_nclasses);
    m_wy.imbue( [&]() { return uni2(engine); } );
  }
  //m_wy *= 0.2;

  std::uniform_real_distribution<double> uni3(-2.0 / double(m_nh), 2.0 / double(m_nh));
  m_wh.set_size(m_nh, m_nh);
  m_wh.imbue( [&]() { return uni3(engine); } );
  //m_wh *= 0.2;

  m_h_tm1.set_size(1, m_nh);
  m_h_tm1.zeros();

  if (m_bi) {
    m_h_tm1_back.set_size(1, m_nh);
    m_h_tm1_back.zeros();

    std::uniform_real_distribution<double> uni1(-2.0 / double((m_de + m_ds + m_dc) * m_cs) , 2.0 / double((m_de + m_ds + m_dc) * m_cs));
    m_wx_back.set_size((m_de + m_ds + m_dc)*m_cs, m_nh);
    m_wx_back.imbue( [&]() { return uni1(engine); } );

    std::uniform_real_distribution<double> uni2(-2.0 / double(m_nh), 2.0 / double(m_nh));
    m_wh_back.set_size(m_nh, m_nh);
    m_wh_back.imbue( [&]() { return uni2(engine); } );
  }

  // some constants
  m_UNK_SUFFIX_IND = m_suffix_count; // last col in m_suf
  iter = m_lex_cat_str_ind_map.find("NNOONNEE");
  assert(iter != m_lex_cat_str_ind_map.end());
  m_UNKNWON_SUPERCAT_IND = iter->second;
  m_WORD_PAD_IND = 0; // assumes the 1st col is ***PADDING***
  m_UNK_ALPHNUM_LOWER_IND = m_vocsize;
  m_UNK_ALPHNUM_UPPER_IND = m_vocsize + 1;
  m_UNK_NON_ALPHNUM_IND = m_vocsize + 2;

  // dropout scaling at test time
  m_dropout_vec.set_size(m_wx.n_rows, 1);
  m_dropout_vec.fill(m_dropout_success_prob);

  cerr << "m_UNK_SUFFIX_IND: " << m_UNK_SUFFIX_IND << endl;
  cerr << "m_UNKNWON_SUPERCAT_IND: " << m_UNKNWON_SUPERCAT_IND << endl;
  cerr << "m_WORD_PAD_IND: " << m_WORD_PAD_IND << endl;
  cerr << "m_UNK_ALPHNUM_LOWER_IND: " << m_UNK_ALPHNUM_LOWER_IND << endl;
  cerr << "m_UNK_ALPHNUM_UPPER_IND: " << m_UNK_ALPHNUM_UPPER_IND << endl;
  cerr << "m_UNK_NON_ALPHNUM_IND: " << m_UNK_NON_ALPHNUM_IND << endl;

  cerr << "m_emb size: " << m_emb.n_rows << " " << m_emb.n_cols << endl;
  cerr << "m_suf size: " << m_suf.n_rows << " " << m_suf.n_cols << endl;
  cerr << "m_cap size: " << m_cap.n_rows << " " << m_cap.n_cols << endl;

  if (m_act == "relu") {
    std::normal_distribution<double> dist(0.0, 0.01);

    m_wx.imbue( [&]() { return dist(engine); } );
    m_wh.imbue( [&]() { return dist(engine); } );
    m_wy.imbue( [&]() { return dist(engine); } );
    //m_wh = eye<mat>(m_nh, m_nh);
    //m_wh *= 0.01;
    m_suf.imbue( [&]() { return dist(engine); } );
    m_cap.imbue( [&]() { return dist(engine); } );

    m_wx_back.imbue( [&]() { return dist(engine); } );
    m_wh_back.imbue( [&]() { return dist(engine); } );
    //m_wh_back = eye<mat>(m_nh, m_nh);
    //m_wh_back *= 0.01;
  }
}


RnnTagger::RnnTagger(const string &model_dir,
                     const string &all_untouched_embeddings,
                     const string &suf_str_ind_file,
                     size_t output_fmt,
                     double dropout_success_prob,
                     bool dropout, string act, bool bi, bool init_weights,
                     double lr)
: m_de(50), m_ds(5), m_dc(5), m_cs(7), m_nh(200), m_bs(500), m_model_dir(model_dir),
  m_lowercase_words(true), m_nepoch(0), m_dropout(dropout),
  m_dropout_success_prob(dropout_success_prob),
  m_verbose(false), m_lr(lr), m_act(act), m_clip(0.0), m_bi(bi), m_out_fmt(output_fmt) {

  cerr << "RnnTagger init" << endl;
  cerr << "lr: " << m_lr << endl;

  load_ind2rawtag_map(model_dir);
  m_nclasses = m_label2rawtag_map.size();

  if (init_weights) {
    std::string wx_mat = model_dir + "/wx.mat";
    std::string wh_mat = model_dir + "/wh.mat";
    std::string wy_mat = model_dir + "/wy.mat";
    std::string emb_mat = model_dir + "/emb.mat";
    std::string suf_mat = model_dir + "/suf.mat";
    std::string cap_mat = model_dir + "/cap.mat";

    m_emb.load(emb_mat);
    m_suf.load(suf_mat);
    m_cap.load(cap_mat);
    m_wx.load(wx_mat);
    m_wy.load(wy_mat);
    m_wh.load(wh_mat);

    std::string wx_back_mat;
    std::string wh_back_mat;
    if (m_bi) {
      wx_back_mat = model_dir + "/wx_back.mat";
      wh_back_mat = model_dir + "/wh_back.mat";
      m_wx_back.load(wx_back_mat);
      m_wh_back.load(wh_back_mat);
    }
  }

  m_vocsize = m_emb.n_cols - 3;

  // untouched pre-trained embeddings need
  // to be loaded at test time into a mapping
  // from words to indexes
  load_emb_word_ind_map(model_dir + "/" + all_untouched_embeddings);

  unordered_map<string, size_t>::const_iterator iter = m_full_wordstr_emb_map.find("***PADDING***");
  assert(iter != m_full_wordstr_emb_map.end());

  load_suf_str_ind_map(model_dir + "/" + suf_str_ind_file);
  m_suffix_count = m_suf_str_ind_map.size();

  m_h_tm1.set_size(1, m_wh.n_rows);
  m_h_tm1.zeros();

  // some constants
  m_UNK_SUFFIX_IND = m_suffix_count; // last col in m_suf
  iter = m_lex_cat_str_ind_map.find("NNOONNEE");
  assert(iter != m_lex_cat_str_ind_map.end());
  m_UNKNWON_SUPERCAT_IND = iter->second;
  m_WORD_PAD_IND = 0; // assumes the 1st col (with index 0) is ***PADDING***
  m_UNK_ALPHNUM_LOWER_IND = m_vocsize;
  m_UNK_ALPHNUM_UPPER_IND = m_vocsize + 1;
  m_UNK_NON_ALPHNUM_IND = m_vocsize + 2;

  // dropout scaling at test time
  m_dropout_vec.set_size(m_wx.n_rows, 1);
  m_dropout_vec.fill(m_dropout_success_prob);

  cerr << "m_UNK_SUFFIX_IND: " << m_UNK_SUFFIX_IND << endl;
  cerr << "m_UNKNWON_SUPERCAT_IND: " << m_UNKNWON_SUPERCAT_IND << endl;
  cerr << "m_WORD_PAD_IND: " << m_WORD_PAD_IND << endl;
  cerr << "m_UNK_ALPHNUM_LOWER_IND: " << m_UNK_ALPHNUM_LOWER_IND << endl;
  cerr << "m_UNK_ALPHNUM_UPPER_IND: " << m_UNK_ALPHNUM_UPPER_IND << endl;
  cerr << "m_UNK_NON_ALPHNUM_IND: " << m_UNK_NON_ALPHNUM_IND << endl;

  cerr << "m_emb size: " << m_emb.n_rows << " " << m_emb.n_cols << endl;
  cerr << "m_suf size: " << m_suf.n_rows << " " << m_suf.n_cols << endl;
  cerr << "m_cap size: " << m_cap.n_rows << " " << m_cap.n_cols << endl;
}


RnnTagger::~RnnTagger(void){}

mat RnnTagger::sigmoid(const mat &m) {
  mat one = ones<mat>(m.n_rows, m.n_cols);
  return one/(one + exp(-m));
}


mat RnnTagger::soft_max(const mat &y) {
//  mat res = exp(y)/accu(exp(y));
//  return res;
  mat dev = y - y.max();
  return exp(dev)/accu(exp(dev));
}


// log-softmax (numerically stable version of above)
mat RnnTagger::soft_max_log(const mat &y) {
  mat dev = y - y.max();
  return exp(dev - log(accu(exp(dev))));
}


double RnnTagger::x_ent_multi(const mat&y, const mat&t) {
  return -accu(t % log(y));
}


void RnnTagger::re_init_mats(const string &wx, const string &wh, const string &wy,
                             const string &emb, const string &suf, const string &cap) {

  m_wx.reset();
  m_wh.reset();
  m_wy.reset();
  m_emb.reset();
  m_suf.reset();
  m_cap.reset();

  m_wx.load(wx);
  m_wh.load(wh);
  m_wy.load(wy);
  m_emb.load(emb);
  m_suf.load(suf);
  m_cap.load(cap);
}


//void
//RnnTagger::classify(const vector<vector<vector<size_t> > > &contextwins,
//                         const vector<size_t> &gold_labels,
//                         vector<size_t> &sent_res, double &err) {
//  m_h_tm1.zeros();
//  uword ind;
//  for (size_t i = 0; i < contextwins.size(); ++i) {
//    colvec x;
//    colvec x0;
//    colvec x1;
//    colvec x2;
//
//    for (size_t j = 0; j < m_cs; ++j) {
//      x0 = join_cols(x0, m_emb.col(contextwins[i][j][0]));
//      x1 = join_cols(x1, m_suf.col(contextwins[i][j][1]));
//      x2 = join_cols(x2, m_cap.col(contextwins[i][j][2]));
//    }
//
//    x = join_cols(join_cols(x0, x1), x2);
//    mat lh = sigmoid(x.t()*m_wx + m_h_tm1*m_wh);
//    mat ly = soft_max(lh*m_wy);
//    m_y(gold_labels[i]) = 1.0;
//    err += x_ent_multi(ly, m_y);
//    m_y(gold_labels[i]) = 0.0;
//    ly.max(ind);
//    sent_res.push_back(ind);
//    m_h_tm1 = lh;
//  }
//}


void RnnTagger::classify_multi(const vector<vector<vector<size_t> > > &contextwins,
                               Sentence &sent, double BETA) {
  // multi-tagging and beta beam

  m_h_tm1.zeros();
  //sent.msuper.reserve(contextwins.size());
  unordered_map<size_t, string>::const_iterator iter = m_label2rawtag_map.begin();
  sent.msuper.resize(sent.words.size());

  for (size_t i = 0; i < contextwins.size(); ++i) {
    colvec x;
    colvec x0;
    colvec x1;
    colvec x2;

    for (size_t j = 0; j < m_cs; ++j) {
      x0 = join_cols(x0, m_emb.col(contextwins[i][j][0]));
      x1 = join_cols(x1, m_suf.col(contextwins[i][j][1]));
      x2 = join_cols(x2, m_cap.col(contextwins[i][j][2]));
    }

    x = join_cols(join_cols(x0, x1), x2);
    if (m_dropout)
      x = x % m_dropout_vec;
    mat lh = sigmoid(x.t()*m_wx + m_h_tm1*m_wh);
    mat ly = soft_max(lh*m_wy);

    uword ind = 0;
    assert(ly.n_cols == m_nclasses);
    ly.cols(0, ly.n_cols - 2).max(ind); // -2 to ignore the NNOONNEE cat not in m_lex_cat_str_ind_map
    MultiRaw &mraw = sent.msuper[i];
    mraw.resize(0);
    double cut_off = ly(ind) * BETA;
    assert(cut_off >= 0.0);

    for (size_t k = 0; k < ly.n_cols - 1; ++k) {
      if (ly(k) >= cut_off) {
        iter = m_label2rawtag_map.find(k);
        assert(iter != m_label2rawtag_map.end());
        //        if (m_use_candc_tagdicts) {
        //          if (m_super.exists(sent.words[i], iter->second, sent.pos[i], m_candc_tagdict_cutoff, m_candc_tagdict_pos_backoff))
        //            mraw.push_back(ScoredRaw(iter->second, ly(k)));
        //        } else {
        mraw.push_back(ScoredRaw(iter->second, ly(k)));
        //  }
      }
    }

    if (BETA == 0.0)
      assert(mraw.size() == m_nclasses - 1);

    sort(mraw.begin(), mraw.end());
    m_h_tm1 = lh;

    //    if (m_xf1_training) {
    //      sent.all_ly_vals.emplace_back(ly);
    //    }
  }
}


void RnnTagger::classify_multi_bi(const vector<vector<vector<size_t>>> &contextwins,
                                  Sentence &sent, double BETA) {
  m_h_tm1.zeros();
  sent.msuper.resize(sent.words.size());
  uword ind = 0;
  //  vector<mat> x_vals;
  //  vector<mat> lh_vals;
  //  x_vals.reserve(contextwins.size());
  //  lh_vals.reserve(contextwins.size() + 1);
  //  lh_vals.emplace_back(m_h_tm1);
  //  mat lh_vals_back[contextwins.size() + 1];
  //  lh_vals_back[contextwins.size()] = m_h_tm1;

  m_x_vals.clear();
  m_lh_vals.clear();
  m_lh_vals_back.clear();
  m_lh_deriv_vals.clear();
  m_lh_deriv_vals_back.clear(); // todo, not needed if not doing xf1

  m_x_vals.reserve(contextwins.size());
  m_lh_vals.reserve(contextwins.size() + 1);
  m_lh_vals.emplace_back(m_h_tm1);
  m_lh_vals_back.resize(contextwins.size() + 1, m_h_tm1);

  m_contextwins.clear();
  m_contextwins = contextwins;

  //m_h_tm1.zeros();
  //uword ind;
  //vector<mat> x_vals;
  //vector<mat> lh_vals;
  //  x_vals.reserve(contextwins.size());
  //  lh_vals.reserve(contextwins.size() + 1);
  //  lh_vals.emplace_back(m_h_tm1);
  //  mat lh_vals_back[contextwins.size() + 1];
  //  lh_vals_back[contextwins.size()] = m_h_tm1;

  for (size_t i = 0; i < contextwins.size(); ++i) {
    colvec x;
    colvec x0;
    colvec x1;
    colvec x2;

    for (size_t j = 0; j < m_cs; ++j) {

      x0 = join_cols(x0, m_emb.col(contextwins[i][j][0]));
      x1 = join_cols(x1, m_suf.col(contextwins[i][j][1]));
      x2 = join_cols(x2, m_cap.col(contextwins[i][j][2]));
    }

    x = join_cols(join_cols(x0, x1), x2);
    if (m_dropout)
      x = x % m_dropout_vec;
    m_x_vals.emplace_back(x);
    if (m_act == "relu") {
      m_lh_vals.emplace_back(x.t()*m_wx + m_lh_vals[i]*m_wh);
      m_lh_vals[i+1].transform( [](arma::mat::elem_type& val){ return val > 0 ? val : 0.0;} );
    } else {
      m_lh_vals.emplace_back(sigmoid(x.t()*m_wx + m_lh_vals[i]*m_wh));
    }
  }

  for (int i = contextwins.size() - 1; i >=0; --i) {
    if (m_act == "relu") {
      m_lh_vals_back[i] = (m_x_vals[i].t()*m_wx_back + m_lh_vals_back[i + 1]*m_wh_back);
      m_lh_vals_back[i].transform( [](arma::mat::elem_type& val){ return val > 0 ? val : 0.0;} );
    } else {
      m_lh_vals_back[i] = (sigmoid(m_x_vals[i].t()*m_wx_back + m_lh_vals_back[i + 1]*m_wh_back));
    }
  }

  sent.all_ly_vals.clear();
  sent.all_ly_vals.reserve(250);
  for (size_t i = 1; i <= contextwins.size(); ++i) {
    mat ly = soft_max(join_rows(m_lh_vals[i], m_lh_vals_back[i-1])*m_wy);

    assert(ly.n_cols == m_nclasses);
    ly.cols(0, ly.n_cols - 2).max(ind); // -2 to ignore the NNOONNEE cat not in m_lex_cat_str_ind_map
    MultiRaw &mraw = sent.msuper[i-1];
    mraw.clear();

    double cut_off = ly(ind) * BETA;
    assert(cut_off >= 0.0);

    // the mapping between string cats and inds
    // are different in the tagger and the parser
    // the parser loads the cats from the markedup file
    for (size_t k = 0; k < ly.n_cols - 1; ++k) {
      if (ly(k) >= cut_off) {
        auto iter = m_label2rawtag_map.find(k);
        assert(iter != m_label2rawtag_map.end());
        mraw.push_back(ScoredRaw(iter->second, ly(k)));
      }
    }

    if (BETA == 0.0)
      assert(mraw.size() == m_nclasses - 1);

    sort(mraw.begin(), mraw.end());

    //todo if (m_xf1_training) {
    sent.all_ly_vals.emplace_back(ly);
    //}
  }
}


void RnnTagger::mtag_file(IO::ReaderFactory &reader, double BETA) {
  Sentence sent;
  sent.reset();
  reader.reset();
  while(reader.next(sent)) {
    mtag_sent(sent, BETA);
    sent.reset();
  }
}


void RnnTagger::eval_mtag(IO::ReaderFactory &reader) {

  tag_file(reader, true);

  Sentence sent;

  vector<MultiRaws> all_res;
  vector<size_t> sent_gold_labels;
  vector<vector<size_t> > all_gold_labels;

  reader.reset();
  sent.reset();
  while(reader.next(sent)) {
    vector<string> &sent_cats = sent.super;
    //sent_gold_labels.reserve(sent.words.size());

    for (size_t i = 0; i < sent.words.size(); ++i) {
      unordered_map<string, size_t>::const_iterator iter = m_lex_cat_str_ind_map.find(sent_cats[i]);
      if (iter != m_lex_cat_str_ind_map.end())
        sent_gold_labels.push_back(iter->second);
      else
        sent_gold_labels.push_back(m_UNKNWON_SUPERCAT_IND);
    }

    //cerr << sent.super.size() << " " <<  sent.words.size() << " " << sent_gold_labels.size() << endl;
    assert(sent_gold_labels.size() == sent.words.size());
    all_gold_labels.push_back(sent_gold_labels);
    sent.reset();
    sent_gold_labels.clear();
  }

  double beta_levels[] = {0.09, 0.08, 0.075, 0.07, 0.06, 0.05, 0.04, 0.03, 0.025, 0.02, 0.01, 0.009,
      0.005, 0.004, 0.003, 0.0025, 0.002, 0.0015, 0.0013, 0.001, 0.0005, 0.00045, 0.0004, 0.00035, 0.00025, 0.0002, 0.0001, 0.00001, 0.000001};
  vector<double> beta_levels_vec(beta_levels, beta_levels + sizeof(beta_levels) / sizeof(double));
  double beta = 0.0;
  double total_tags = 0.0;
  double total_correct_tags = 0.0;
  double total_correct_sent = 0.0;
  double sent_correct = 0.0;
  double total_mtags = 0.0;

  for (size_t b = 0; b < beta_levels_vec.size(); ++b) {
    all_res.clear();
    beta = beta_levels_vec[b];
    printf ("beta %f\n", beta);

    reader.reset();
    while(reader.next(sent)) {
      mtag_sent(sent, beta);
      all_res.push_back(sent.msuper);
      sent.reset();
    }

    assert(all_res.size() == all_gold_labels.size());

    total_tags = 0.0;
    total_correct_tags = 0.0;
    total_correct_sent = 0.0;
    total_mtags = 0.0;

    for (size_t k = 0; k < all_gold_labels.size(); ++k) {
      total_tags += all_gold_labels[k].size();
      assert(all_gold_labels[k].size() == all_res[k].size());
      sent_correct = 0.0;

      for (size_t l = 0; l < all_gold_labels[k].size(); ++l) {
        string raw_tag = m_label2rawtag_map.find(all_gold_labels[k][l])->second;

        total_mtags += all_res[k][l].size();

        for (size_t m = 0; m < all_res[k][l].size(); ++m) {
          if (raw_tag == all_res[k][l][m].raw) {
            ++total_correct_tags;
            ++sent_correct;
          }
        }
      }

      if (sent_correct == all_gold_labels[k].size())
        ++total_correct_sent;
    }

    assert(total_correct_tags <= total_tags);

    printf ("-- mtag accuracy: %f\n",
            total_correct_tags/total_tags);
    printf ("-- mtag sent acc: %f  \n",
            total_correct_sent/all_res.size());
    printf ("-- mtag avg number of tags: %f  \n",
            total_mtags/total_tags);

  }

}


//void
//RnnTagger::mtag_train_data(vector<vector<vector<vector<int> > > > &all_sent_contextwins) {
//  double err = 0.0;
//  vector<vector<ScoredInd> > sent_res;
//  vector<vector<vector<ScoredInd> > > all_res;
//
//  vector<double> sent_res_1best_score;
//  vector<vector<double> > all_res_1best_score;
//
//  vector<vector<vector<int> > > contextwins;
//  const vector<vector<vector<int> > > *gold_words = &m_train_words;
//  const vector<vector<int> > *gold_labels = &m_train_labels;
//
//  //for (size_t i = 0; i < gold_words->size(); ++i) {
//  for (int i = 0; i < 99; ++i) {
//    contextwins.clear();
//    sent_res.clear();
//    sent_res_1best_score.clear();
//    sent2contextwin(gold_words->at(i), contextwins, m_vocsize + 4 - 1, m_suffix_count);
//    all_sent_contextwins.push_back(contextwins);
//    classify_emb_multi_0221(contextwins, gold_labels->at(i),
//                       sent_res, sent_res_1best_score, err);
//    all_res_1best_score.push_back(sent_res_1best_score);
//    all_res.push_back(sent_res);
//  }
//
//  m_all_res_train = all_res;
//
////  double total = 0.0;
////  double correct = 0.0;
////  // 0.06, 0.05, 0.04, 0.03, 0.025, 0.02, 0.01, 0.009, 0.005, 0.001, 0.0001
////  double beta_levels[] = {1.0};
////  vector<double> beta_levels_vec(beta_levels, beta_levels + sizeof(beta_levels) / sizeof(double));
////  double beta = 0.0;
////  int gold_tag = 0;
////  //cout << beta_levels_vec.size() << endl;
////  for (size_t b = 0; b < beta_levels_vec.size(); ++b) {
////    total = 0.0;
////    correct = 0.0;
////    beta = beta_levels_vec[b];
////    printf ("beta %f\n", beta);
////    for (size_t k = 0; k < gold_labels->size(); ++k) {
////      total += gold_labels->at(k).size();
////      assert(gold_labels->at(k).size() == all_res[k].size());
////      for (size_t l = 0; l < gold_labels->at(k).size(); ++l) {
////        gold_tag = gold_labels->at(k)[l];
////        for (size_t m = 0; m < all_res[k][l].size(); ++m) {
////          if (all_res[k][l][m].score >= beta*all_res_1best_score[k][l] &&
////              gold_tag == all_res[k][l][m].tag)
////            ++correct;
////        }
////      }
////    }
////
////    assert(correct <= total);
////    printf ("-- 0221 1-best accuracy: %f\n", correct/total);
////  }
//}

//void
//RnnTagger::classify(const vector<vector<vector<size_t> > > &contextwins,
//                    vector<size_t> &sent_res) {
//  m_h_tm1.zeros();
//  uword ind;
//
//  for (size_t i = 0; i < contextwins.size(); ++i) {
//    colvec x;
//    colvec x0;
//    colvec x1;
//    colvec x2;
//
//    for (size_t j = 0; j < m_cs; ++j) {
//
//      x0 = join_cols(x0, m_emb.col(contextwins[i][j][0]));
//      x1 = join_cols(x1, m_suf.col(contextwins[i][j][1]));
//      x2 = join_cols(x2, m_cap.col(contextwins[i][j][2]));
//    }
//
//    x = join_cols(join_cols(x0, x1), x2);
//    mat lh = sigmoid(x.t()*m_wx + m_h_tm1*m_wh);
//    mat ly = soft_max(lh*m_wy);
//
//    ly.max(ind);
//    if (ind == m_UNKNWON_SUPERCAT_IND)
//      ly.cols(0, ly.n_cols - 1).max(ind);
//    sent_res.push_back(ind);
//
//    m_h_tm1 = lh;
//  }
//}


//void
//RnnTagger::classify_multi(const vector<vector<vector<size_t> > > &contextwins,
//                          vector<size_t> &sent_res) {
//  m_h_tm1.zeros();
//  uword ind;
//
//  for (size_t i = 0; i < contextwins.size(); ++i) {
//    colvec x;
//    colvec x0;
//    colvec x1;
//    colvec x2;
//
//    for (size_t j = 0; j < m_cs; ++j) {
//
//      x0 = join_cols(x0, m_emb.col(contextwins[i][j][0]));
//      x1 = join_cols(x1, m_suf.col(contextwins[i][j][1]));
//      x2 = join_cols(x2, m_cap.col(contextwins[i][j][2]));
//    }
//
//    x = join_cols(join_cols(x0, x1), x2);
//    mat lh = sigmoid(x.t()*m_wx + m_h_tm1*m_wh);
//    mat ly = soft_max(lh*m_wy);
//
//    ly.max(ind);
//    if (ind == m_UNKNWON_SUPERCAT_IND)
//      ly.cols(0, ly.n_cols - 1).max(ind);
//    sent_res.push_back(ind);
//
//    m_h_tm1 = lh;
//  }
//}


pair<mat, mat>
RnnTagger::calc_lh_proj_emb(size_t step, const vector<vector<size_t> > &word_vecs,
                            mat *x_vals) {
  colvec x;
  colvec x0;
  colvec x1;
  colvec x2;

  for (size_t i = 0; i < m_cs; ++i) {

    //    cerr << word_vecs[i][0] << endl;
    //    cerr << word_vecs[i][1] << endl;
    //    cerr << word_vecs[i][2] << endl;

    x0 = join_cols(x0, m_emb.col(word_vecs[i][0]));
    x1 = join_cols(x1, m_suf.col(word_vecs[i][1]));
    x2 = join_cols(x2, m_cap.col(word_vecs[i][2]));
  }

  x = join_cols(join_cols(x0, x1), x2);
  x_vals[step] = x;
  mat lh;
  mat lh_deriv;
  if (m_act == "relu") {
    lh =  x.t()*m_wx + m_h_tm1*m_wh;
    cerr << "bp: lh_vals before: " << endl;
    lh.print(cerr);
    lh.transform( [](arma::mat::elem_type& val){ return val > 0 ? val : 0.0;} );
    lh_deriv = lh;
    lh_deriv.transform( [](arma::mat::elem_type& val){ return val > 0 ? 1.0 : 0.0;} );
    cerr << "bp: lh_vals after: " << endl;
    lh.print(cerr);
  } else {
    lh = sigmoid(x.t()*m_wx + m_h_tm1*m_wh);
    mat one = ones<mat>(lh.n_rows, lh.n_cols);
    lh_deriv = lh%(one - lh);
  }

  pair<mat, mat> res(lh, lh_deriv);

  m_h_tm1 = lh;
  return res;
}


void
RnnTagger::sent2contextwin(const vector<vector<size_t> > &sent,
                           vector<vector<vector<size_t> > > &contextwins) {
  assert(m_cs % 2 == 1);
  size_t pad_len = m_cs/2;
  vector<size_t> unk_vec;
  unk_vec.push_back(m_WORD_PAD_IND);
  unk_vec.push_back(m_UNK_SUFFIX_IND);
  unk_vec.push_back(0);

  for (size_t i = 0; i < sent.size(); ++i) {
    vector<vector<size_t> > contextwin;
    for (size_t j = i, k = 0; j < i + m_cs; ++j, ++k) {
      if (j < pad_len || j >= pad_len + sent.size()) {
        contextwin.push_back(unk_vec);
      } else {
        contextwin.push_back(sent[j - pad_len]);
      }
    }
    contextwins.push_back(contextwin);
  }
}


//void
//RnnTagger::Test_kbest() {
//  vector<vector<ScoredInd> > sent_res_ind_sorted;
//  vector<ScoredInd> sent1_res;
//  vector<ScoredInd> sent2_res;
//  vector<ScoredInd> sent3_res;
//  ScoredInd one1(3, 0.5);
//  ScoredInd one2(1, 0.4);
//  ScoredInd one3(2, 0.3);
//  ScoredInd two1(2, 0.5);
//  ScoredInd two2(1, 0.3);
//  ScoredInd two3(3, 0.2);
//  ScoredInd three1(1, 0.5);
//  ScoredInd three2(2, 0.2);
//  ScoredInd three3(3, 0.1);
//  sent1_res.push_back(one1);
//  sent1_res.push_back(one2);
//  sent1_res.push_back(one3);
//  sent2_res.push_back(two1);
//  sent2_res.push_back(two2);
//  sent2_res.push_back(two3);
//  sent3_res.push_back(three1);
//  sent3_res.push_back(three2);
//  sent3_res.push_back(three3);
//  sent_res_ind_sorted.push_back(sent1_res);
//  sent_res_ind_sorted.push_back(sent2_res);
//  sent_res_ind_sorted.push_back(sent3_res);
//
//  vector<vector<pair<unsigned int, string> > > res = gen_kbest(sent_res_ind_sorted, 20);
//  double score = 0.0;
//  for (unsigned int i = 0; i < res.size(); ++i) {
//    score = 0.0;
//    for (unsigned int j = 0; j < res[i].size(); ++j) {
//      printf (" %d ", res[i][j].first);
//      score += sent_res_ind_sorted[j][res[i][j].first].score;
//    }
//    cout << score << endl;
//  }
//}


//// k-best tag sequence generation using cube-pruning
//// tags for each word in the input are sorted in
//// descending order of their scores
//vector<vector<pair<unsigned int, string> > >
//RnnTagger::gen_kbest(const vector<vector<ScoredInd> > &sent_res_ind_sorted,
//    const unsigned int k) {
//  unsigned int sent_len = sent_res_ind_sorted.size();
//  vector<vector<pair<unsigned int, string> > > res;
//  priority_queue<ScoreIndPair, vector<ScoreIndPair>, paircomp> queue;
//  vector<unsigned int> first_ind_vec(sent_res_ind_sorted.size(), 0);
//  ScoreIndPair first(0.0, first_ind_vec);
//  first.first = get_total_score(sent_res_ind_sorted, first_ind_vec);
//  queue.push(first);
//  unordered_map<vector<unsigned int>, int, container_hash<vector<unsigned int>>> non_duplicates;
//
//  while (res.size() < k && !queue.empty()) {
//    ScoreIndPair top(queue.top().first, queue.top().second);
//    queue.pop();
//
//    vector<pair<unsigned int, string> > kth_best_vec;
//    for (unsigned int j = 0; j < sent_len; ++j) {
//      assert(m_label2rawtag_map.find(sent_res_ind_sorted[j][top.second[j]].tag)
//          != m_label2rawtag_map.end());
//      kth_best_vec.push_back(make_pair(sent_res_ind_sorted[j][top.second[j]].tag,
//          m_label2rawtag_map.find(sent_res_ind_sorted[j][top.second[j]].tag)->second));
//      //kth_best_vec.push_back(top.second[j]);
//    }
//    res.push_back(kth_best_vec);
//    if (res.size() == k) break;
//
//    for (unsigned int i = 0; i < sent_len; ++i) {
//      ScoreIndPair succ(0.0, top.second);
//      if (succ.second[i] + 1 < sent_res_ind_sorted[i].size())
//        succ.second[i] += 1;
//      else
//        continue;
//      if (non_duplicates.insert(make_pair(succ.second, 0)).second) {
//        succ.first = get_total_score(sent_res_ind_sorted, succ.second);
//        queue.push(succ);
//      }
//    }
//  }
//
//  assert(res.size() == k);
//
//  return res;
//}
//
//// utility func for kbest generation
//// returns the total score of a tag sequence given the tag index at
//// each word position in the multi-tagging result
//double
//RnnTagger::get_total_score(const vector<vector<ScoredInd> > &sent_res_ind_sorted,
//    const vector<unsigned int> &ind_vec) {
//  assert(ind_vec.size() == sent_res_ind_sorted.size());
//  double total_score = 0.0;
//  for (unsigned int i = 0; i < sent_res_ind_sorted.size(); ++i) {
//    total_score += sent_res_ind_sorted[i][ind_vec[i]].score;
//  }
//  return total_score;
//}


vector<vector<vector<int> > >
RnnTagger::load_words(const string &filename) {
  ifstream in(filename.c_str());
  if (!in)
    throw runtime_error("no such file: " + filename);
  string line;
  vector<vector<vector<int> > > data;
  vector<vector<int> > sent;
  while (getline(in, line)) {
    if (line.empty()) {
      data.push_back(sent);
      sent.clear();
      continue;
    }
    vector<int> word;
    int ind;
    istringstream iss(line);
    while (iss >> ind) {
      word.push_back(ind);
    }
    assert(word.size() == 3);
    sent.push_back(word);
  }
  return data;
}


vector<vector<int> >
RnnTagger::load_labels(const string &filename, size_t &ntokens) {
  ifstream in(filename.c_str());
  if (!in)
    throw runtime_error("no such file: " + filename);
  string line;
  vector<vector<int> > data;
  while (in) {
    if (!getline(in, line)) break;
    istringstream iss(line);
    vector<int> labels;
    int label;
    while (iss >> label) {
      labels.push_back(label);
    }
    ntokens += labels.size();
    data.push_back(labels);
  }
  return data;
}


void
RnnTagger::load_ind2rawtag_map(const std::string &model_dir) {
  string file = model_dir + "/label_ind.txt";
  ifstream in(file.c_str());
  if (!in) {
    cerr << "no such file " << file << endl;
    exit (EXIT_FAILURE);
  }
  string line;
  while (in) {
    if (!getline(in, line)) break;
    istringstream iss(line);
    string label;
    int ind;
    while (iss >> label >> ind) {
      m_label2rawtag_map.insert(make_pair(ind, label));
      m_lex_cat_str_ind_map.insert(make_pair(label, ind));
    }
  }

  cerr << "total number of lex cat classes: " << m_label2rawtag_map.size() << endl;
}


void
RnnTagger::load_emb_word_ind_map(const string &filename) {

  cerr << "loading emb word ind...(the same untounced emb file used for training), "
      << filename << endl;

  ifstream in(filename.c_str());
  size_t total = 0;

  if (!in)
    throw runtime_error("no such file: " + filename);
  string line;

  while (in) {
    if (!getline(in, line)) break;
    istringstream iss(line);

    size_t first_space_pos = line.find(" ");
    string word = line.substr(0, first_space_pos);
    string emb_vec = line.substr(first_space_pos + 1); // get till the end

    if (total == 0)
      assert(word == "***PADDING***");
    assert(m_full_wordstr_emb_map.insert(make_pair(word, total)).second);
    ++total;
  }

  cerr << "total emb: " << total << endl;

  // test
  //emb.col(0).print(cerr, "word emb 0th col");
}


void
RnnTagger::load_emb_mat(mat &emb, const string &filename) {

  cerr << "loading emb mat..., "
      << filename << endl;

  ifstream in(filename.c_str());
  size_t total = 0;

  if (!in)
    throw runtime_error("no such file: " + filename);
  string line;

  while (in) {
    if (!getline(in, line)) break;
    istringstream iss(line);

    size_t first_space_pos = line.find(" ");
    string word = line.substr(0, first_space_pos);
    string emb_vec = line.substr(first_space_pos + 1); // get till the end

    colvec temp(emb_vec);
    emb.col(total) = temp;
    if (total == 0)
      assert(word == "***PADDING***");
    assert(m_full_wordstr_emb_map.insert(make_pair(word, total)).second);
    ++total;
  }

  cerr << "total emb: " << total << endl;

  // test
  //emb.col(0).print(cerr, "word emb 0th col");
}


void
RnnTagger::contextwin2minibatch(const vector<vector<vector<size_t> > > &sent_cw,
                                vector<vector<vector<vector<size_t> > > > &minibatch_all) const {
  vector<vector<vector<size_t> > > minibatch;
  for (size_t i = 0; i < sent_cw.size(); ++i) {
    minibatch.push_back(sent_cw[i]);
    if ((i + 1) % m_bs == 0 || i == sent_cw.size() - 1) {
      minibatch_all.push_back(minibatch);
      minibatch.clear();
    }
  }
}


void
RnnTagger::labels2minibatch(const vector<size_t> &labels,
                            vector<vector<size_t> > &minibatch_all) const {
  vector<size_t> minibatch;
  for (size_t i = 0; i < labels.size(); ++i) {
    minibatch.push_back(labels[i]);
    if ((i + 1) % m_bs == 0 || i == labels.size() - 1) {
      minibatch_all.push_back(minibatch);
      minibatch.clear();
    }
  }
}


// entry point for training
void RnnTagger::train(IO::ReaderFactory &reader, IO::ReaderFactory &reader_dev) {

  Sentence sent;

  vector<vector<vector<size_t> > > train_words;
  vector<vector<size_t> > train_labels;
  size_t ntokens0221 = 0;
  size_t total_train_sents = 0;
  double in_pretrained = 0.0;

  while(reader.next(sent)) {
    ++total_train_sents;
    vector<string> &sent_words = sent.words;
    vector<string> &sent_cats = sent.super;
    vector<vector<size_t> > sent_word_vecs;
    vector<size_t> sent_cat_inds;

    ntokens0221 += sent.words.size();

    for (size_t i = 0; i < sent.words.size(); ++i) {
      sent_word_vecs.push_back(word2vec(sent_words[i]));

      if (sent_word_vecs[i][0] < m_vocsize)
        ++in_pretrained;

      unordered_map<string, size_t>::const_iterator iter = m_lex_cat_str_ind_map.find(sent_cats[i]);
      if (iter != m_lex_cat_str_ind_map.end())
        sent_cat_inds.push_back(iter->second);
      else
        sent_cat_inds.push_back(m_UNKNWON_SUPERCAT_IND);
    }

    train_words.push_back(sent_word_vecs);
    train_labels.push_back(sent_cat_inds);
    sent.reset();
  }

  cerr << "total tokens in training data: " << ntokens0221 << endl;
  cerr << "pre-trained embedding coverage on training data: " << in_pretrained / (double)ntokens0221 << endl;

  cerr << "total training sents: " << total_train_sents << endl;
  cerr << "train_words size: " << train_words.size() << endl;
  cerr << "total_labels size: " << train_labels.size() << endl;

  assert(train_words.size() == train_labels.size());
  assert(train_labels.size() == total_train_sents);

  vector<size_t> train_data_inds;
  for (size_t i = 0; i < train_words.size(); ++i) {
    train_data_inds.push_back(i);
  }

  vector<vector<vector<size_t> > > contextwins;
  vector<vector<vector<vector<size_t> > > > minibatch_contextwins;
  vector<vector<size_t> > minibatch_labels;
  mat y = mat(1, m_nclasses, fill::zeros);

  time_t tstart;
  time_t tend;
  double secs = 0.0;

  cerr << "training started...\n";
  double total_err = 0.0;

  for (size_t i = 0; i < m_nepoch; ++i) {

    auto seed = chrono::high_resolution_clock::now().time_since_epoch().count();
    std::mt19937_64 engine(seed);
    time(&tstart);
    shuffle(begin(train_data_inds), std::end(train_data_inds), engine);
    total_err = 0.0;
    size_t j = 0;
    for (size_t s = 0; s < train_data_inds.size(); ++s) {
      j = train_data_inds[s];
      assert(train_words[j].size() == train_labels[j].size());

      m_h_tm1.zeros() ;
      contextwins.clear();
      minibatch_contextwins.clear();
      minibatch_labels.clear();
      sent2contextwin(train_words[j], contextwins);
      contextwin2minibatch(contextwins, minibatch_contextwins);
      labels2minibatch(train_labels[j], minibatch_labels);

      mat m_wx_old = m_wx;
      for (size_t k = 0; k < minibatch_contextwins.size(); ++k) {
        assert(minibatch_contextwins[k].size() == minibatch_labels[k].size());

        if (m_bi) {
          total_err += train_bptt_multi_dropout_bi(minibatch_contextwins[k], minibatch_labels[k], y);
        } else {
          if (m_dropout)
            total_err += train_bptt_multi_dropout(minibatch_contextwins[k], minibatch_labels[k], y);
          else
            total_err += train_bptt_multi_emb(minibatch_contextwins[k], minibatch_labels[k], y);
        }
      }

      time(&tend);
      if (m_verbose) {
        printf (">> epoch %lu, %f completed in %f secs <<\r", i,
                (double(s + 1)/(double)train_words.size())*100, difftime(tend, tstart));
        fflush(stdout);
      }
    }

    time(&tend);
    secs = difftime(tend, tstart);
    fprintf (stderr, "\n>> time 0221: %f secs\n", secs);
    fprintf (stderr, ">> epoch %lu, err 0221: %f\n", i, total_err/(double)ntokens0221);

    time(&tstart);
    tag_file(reader_dev, true);
    time(&tend);
    fprintf (stderr, ">> 00 eval time %f\n", difftime(tend, tstart));

    // save model
    m_wx.save(m_model_dir + "/wx." + to_string(i) + ".mat");
    m_wh.save(m_model_dir + "/wh." + to_string(i) + ".mat");
    m_wy.save(m_model_dir + "/wy." + to_string(i) + ".mat");
    m_suf.save(m_model_dir + "/suffix." + to_string(i) + ".mat");
    m_cap.save(m_model_dir + "/cap." + to_string(i) + ".mat");
    m_emb.save(m_model_dir + "/emb." + to_string(i) + ".mat");
    if (m_bi) {
      m_wx_back.save(m_model_dir + "/wx_back." + to_string(i) + ".mat");
      m_wh_back.save(m_model_dir + "/wh_back." + to_string(i) + ".mat");
    }
  }

}


void RnnTagger::save_xf1_model(const size_t &i, const string &model_dir) {
  // save model
  m_wx.save(model_dir + "/wx." + to_string(i) + ".mat");
  m_wh.save(model_dir + "/wh." + to_string(i) + ".mat");
  m_wy.save(model_dir + "/wy." + to_string(i) + ".mat");
  m_suf.save(model_dir + "/suffix." + to_string(i) + ".mat");
  m_cap.save(model_dir + "/cap." + to_string(i) + ".mat");
  m_emb.save(model_dir + "/emb." + to_string(i) + ".mat");
  if (m_bi) {
    m_wx_back.save(model_dir + "/wx_back." + to_string(i) + ".mat");
    m_wh_back.save(model_dir + "/wh_back." + to_string(i) + ".mat");
  }
}


// entry point for training
void RnnTagger::train_grad_check(IO::ReaderFactory &reader, IO::ReaderFactory &reader_dev) {

  Sentence sent;

  vector<vector<vector<size_t> > > train_words;
  vector<vector<size_t> > train_labels;
  size_t ntokens0221 = 0;
  size_t total_train_sents = 0;
  double in_pretrained = 0.0;

  while(reader.next(sent)) {
    ++total_train_sents;
    vector<string> &sent_words = sent.words;
    vector<string> &sent_cats = sent.super;
    vector<vector<size_t> > sent_word_vecs;
    vector<size_t> sent_cat_inds;

    ntokens0221 += sent.words.size();

    for (size_t i = 0; i < sent.words.size(); ++i) {
      sent_word_vecs.push_back(word2vec(sent_words[i]));

      if (sent_word_vecs[i][0] < m_vocsize)
        ++in_pretrained;

      unordered_map<string, size_t>::const_iterator iter = m_lex_cat_str_ind_map.find(sent_cats[i]);
      if (iter != m_lex_cat_str_ind_map.end())
        sent_cat_inds.push_back(iter->second);
      else
        sent_cat_inds.push_back(m_UNKNWON_SUPERCAT_IND);
    }

    train_words.push_back(sent_word_vecs);
    train_labels.push_back(sent_cat_inds);
    sent.reset();
  }

  cerr << "total tokens in training data: " << ntokens0221 << endl;
  cerr << "pre-trained embedding coverage on training data: " << in_pretrained / (double)ntokens0221 << endl;

  cerr << "total training sents: " << total_train_sents << endl;
  cerr << "train_words size: " << train_words.size() << endl;
  cerr << "total_labels size: " << train_labels.size() << endl;

  assert(train_words.size() == train_labels.size());
  assert(train_labels.size() == total_train_sents);

  vector<size_t> train_data_inds;
  for (size_t i = 0; i < train_words.size(); ++i) {
    train_data_inds.push_back(i);
  }

  vector<vector<vector<size_t> > > contextwins;
  vector<vector<vector<vector<size_t> > > > minibatch_contextwins;
  vector<vector<size_t> > minibatch_labels;
  mat y = mat(1, m_nclasses, fill::zeros);

  time_t tstart;
  time_t tend;
  double secs = 0.0;

  cerr << "training started...\n";
  double total_err = 0.0;

  mat m_wx_old = m_wx;
  mat m_wh_old = m_wh;
  mat m_wy_old = m_wy;
  mat m_wx_back_old = m_wx_back;
  mat m_wh_back_old = m_wh_back;
  mat m_emb_old = m_emb;
  mat m_suf_old = m_suf;
  mat m_cap_old = m_cap;

  for (size_t i = 0; i < m_nepoch; ++i) {

    auto seed = chrono::high_resolution_clock::now().time_since_epoch().count();
    std::mt19937_64 engine(seed);
    time(&tstart);
    //debug
    //shuffle(begin(train_data_inds), std::end(train_data_inds), engine);
    total_err = 0.0;
    size_t j = 0;
    for (size_t s = 0; s < train_data_inds.size(); ++s) {
      j = train_data_inds[s];
      assert(train_words[j].size() == train_labels[j].size());

      m_h_tm1.zeros() ;
      contextwins.clear();
      minibatch_contextwins.clear();
      minibatch_labels.clear();
      sent2contextwin(train_words[j], contextwins);
      contextwin2minibatch(contextwins, minibatch_contextwins);
      labels2minibatch(train_labels[j], minibatch_labels);

      for (size_t k = 0; k < minibatch_contextwins.size(); ++k) {
        assert(minibatch_contextwins[k].size() == minibatch_labels[k].size());

        // debug, grad_check,
        // bidirectional grad_check only works with a m_bs > all sent length
        train_bptt_multi_dropout_bi(minibatch_contextwins[k], minibatch_labels[k], y);
        //        if (m_dropout)
        //          total_err += train_bptt_multi_dropout(minibatch_contextwins[k], minibatch_labels[k], y);
        //        else
        //          total_err += train_bptt_multi_emb(minibatch_contextwins[k], minibatch_labels[k], y);
      }

      mat temp = m_wx_old - m_wx;
      cerr << "bp_grad_wx(0,0): " << temp(0,0)/m_lr << endl;
      temp = m_wx_back_old - m_wx_back;
      cerr << "bp_grad_wx_back(0,0): " << temp(0,0)/m_lr << endl;
      temp = m_wh_old - m_wh;
      cerr << "bp_grad_wh(0,0): " << temp(0,0)/m_lr << endl;
      temp = m_wh_back_old - m_wh_back;
      cerr << "bp_grad_wh_back(0,0): " << temp(0,0)/m_lr << endl;
      temp = m_wy_old - m_wy;
      cerr << "bp_grad_wy(0,0): " << temp(0,0)/m_lr << endl;
      temp = m_emb_old - m_emb;
      cerr << "bp_grad_emb(0,0): " << temp(0,0)/m_lr << endl;
      temp = m_suf_old - m_suf;
      cerr << "bp_grad_suf(0,0): " << temp(0,0)/m_lr << endl;
      temp = m_cap_old - m_cap;
      cerr << "bp_grad_cap_wx(0,0): " << temp(0,0)/m_lr << endl;
      m_wx = m_wx_old;
      m_wh = m_wh_old;
      m_wy = m_wy_old;
      m_wx_back = m_wx_back_old;
      m_wh_back = m_wh_back_old;
      m_emb = m_emb_old;
      m_suf = m_suf_old;
      m_cap = m_cap_old;

      time(&tend);
      if (m_verbose) {
        printf (">> epoch %lu, %f completed in %f secs <<\r", i,
                (double(s + 1)/(double)train_words.size())*100, difftime(tend, tstart));
        fflush(stdout);
      }
    }

    time(&tend);
    secs = difftime(tend, tstart);
    fprintf (stderr, "\n>> time 0221: %f secs\n", secs);
    fprintf (stderr, ">> epoch %lu, err 0221: %f\n", i, total_err/(double)ntokens0221);

    time(&tstart);
    tag_file(reader_dev, true);
    time(&tend);
    fprintf (stderr, ">> 00 eval time %f\n", difftime(tend, tstart));

    // save model
    //    m_wx.save(m_model_dir + "/wx." + to_string(i) + ".mat");
    //    m_wh.save(m_model_dir + "/wh." + to_string(i) + ".mat");
    //    m_wy.save(m_model_dir + "/wy." + to_string(i) + ".mat");
    //    m_suf.save(m_model_dir + "/suffix." + to_string(i) + ".mat");
    //    m_cap.save(m_model_dir + "/cap." + to_string(i) + ".mat");
    //    m_emb.save(m_model_dir + "/emb." + to_string(i) + ".mat");
  }

}



// entry point for training
void RnnTagger::train_mt(IO::ReaderFactory &reader,
                         IO::ReaderFactory &reader_dev,
                         const size_t &thread_id,
                         const string &temp_dir) {

  Sentence sent;

  vector<vector<vector<size_t> > > train_words;
  vector<vector<size_t> > train_labels;
  size_t ntokens0221 = 0;
  size_t total_train_sents = 0;
  double in_pretrained = 0.0;

  reader.reset();
  while(reader.next(sent)) {
    ++total_train_sents;
    vector<string> &sent_words = sent.words;
    vector<string> &sent_cats = sent.super;
    vector<vector<size_t> > sent_word_vecs;
    vector<size_t> sent_cat_inds;

    ntokens0221 += sent.words.size();

    for (size_t i = 0; i < sent.words.size(); ++i) {
      sent_word_vecs.push_back(word2vec(sent_words[i]));

      if (sent_word_vecs[i][0] < m_vocsize)
        ++in_pretrained;

      unordered_map<string, size_t>::const_iterator iter = m_lex_cat_str_ind_map.find(sent_cats[i]);
      if (iter != m_lex_cat_str_ind_map.end())
        sent_cat_inds.push_back(iter->second);
      else
        sent_cat_inds.push_back(m_UNKNWON_SUPERCAT_IND);
    }

    train_words.push_back(sent_word_vecs);
    train_labels.push_back(sent_cat_inds);
    sent.reset();
  }

  cerr << "total tokens in training data: " << ntokens0221 << endl;
  cerr << "pre-trained embedding coverage on training data: " << in_pretrained / (double)ntokens0221 << endl;

  cerr << "total training sents: " << total_train_sents << endl;
  cerr << "train_words size: " << train_words.size() << endl;
  cerr << "total_labels size: " << train_labels.size() << endl;

  assert(train_words.size() == train_labels.size());
  assert(train_labels.size() == total_train_sents);

  vector<size_t> train_data_inds;
  for (size_t i = 0; i < train_words.size(); ++i) {
    train_data_inds.push_back(i);
  }

  vector<vector<vector<size_t> > > contextwins;
  vector<vector<vector<vector<size_t> > > > minibatch_contextwins;
  vector<vector<size_t> > minibatch_labels;
  mat y = mat(1, m_nclasses, fill::zeros);

  time_t tstart;
  time_t tend;
  double secs = 0.0;

  cerr << "training started...\n";
  double total_err = 0.0;

  for (size_t i = 0; i < 1; ++i) {

    auto seed = chrono::high_resolution_clock::now().time_since_epoch().count();
    std::mt19937_64 engine(seed);
    time(&tstart);
    shuffle(begin(train_data_inds), std::end(train_data_inds), engine);
    total_err = 0.0;
    size_t j = 0;
    for (size_t s = 0; s < train_data_inds.size(); ++s) {
      j = train_data_inds[s];
      assert(train_words[j].size() == train_labels[j].size());

      m_h_tm1.zeros() ;
      contextwins.clear();
      minibatch_contextwins.clear();
      minibatch_labels.clear();
      sent2contextwin(train_words[j], contextwins);
      contextwin2minibatch(contextwins, minibatch_contextwins);
      labels2minibatch(train_labels[j], minibatch_labels);

      for (size_t k = 0; k < minibatch_contextwins.size(); ++k) {
        assert(minibatch_contextwins[k].size() == minibatch_labels[k].size());

        //if (m_dropout)
        total_err += train_bptt_multi_dropout(minibatch_contextwins[k], minibatch_labels[k], y);
        //else
        // total_err += train_bptt_multi_emb(minibatch_contextwins[k], minibatch_labels[k], y);
      }

      time(&tend);
      if (m_verbose) {
        printf (">> epoch %lu, %f completed in %f secs <<\r", i,
                (double(s + 1)/(double)train_words.size())*100, difftime(tend, tstart));
        fflush(stdout);
      }
    }

    time(&tend);
    secs = difftime(tend, tstart);
    fprintf (stderr, "\n>> time 0221: %f secs\n", secs);
    fprintf (stderr, ">> epoch %lu, err 0221: %f\n", i, total_err/(double)ntokens0221);

    //    time(&tstart);
    //    tag_file(reader_dev, true);
    //    time(&tend);
    //    fprintf (stderr, ">> 00 eval time %f\n", difftime(tend, tstart));

    //    // save model
    m_wx.save(temp_dir + "/" + to_string(thread_id) + ".wx.mat");
    m_wh.save(temp_dir + "/" + to_string(thread_id) + ".wh.mat");
    m_wy.save(temp_dir + "/" + to_string(thread_id) + ".wy.mat");
    m_suf.save(temp_dir + "/" + to_string(thread_id) + ".suf.mat");
    m_cap.save(temp_dir + "/" + to_string(thread_id) + ".cap.mat");
    m_emb.save(temp_dir + "/" + to_string(thread_id) + ".emb.mat");
  }

}


// bptt multi bs number of steps, each step corresponds to a contextwin
double
RnnTagger::train_bptt_multi_emb(const vector<vector<vector<size_t> > > &contextwins,
                                const vector<size_t> &label_batch,
                                mat &y) {
  size_t cs = m_cs;
  size_t ds = m_ds;
  size_t dc = m_dc;
  size_t de = m_de;
  size_t steps = contextwins.size();

  double err = 0.0;

  mat lh_vals[steps + 1];
  lh_vals[0] = m_h_tm1;
  mat lh_deriv_vals[steps];
  mat ly_vals[steps];
  mat x_vals[steps];

  mat delta_h_vals[steps];
  mat delta_x_vals[steps];

  for (size_t i = 0; i < steps; ++i) {
    pair<mat,  mat> vals =
        calc_lh_proj_emb(i, contextwins[i], x_vals);
    mat lh = vals.first;
    mat lh_deriv = vals.second;
    ly_vals[i] = soft_max(lh*m_wy);
    lh_vals[i + 1] = lh;
    lh_deriv_vals[i] = lh_deriv;
  }

  for (size_t i = 0; i < steps; ++i) {
    y[label_batch[i]] = 1.0;
    err += x_ent_multi(ly_vals[i], y);
    ly_vals[i](label_batch[i]) -= 1.0;
    y[label_batch[i]] = 0.0;
  }

  // bptt multi
  delta_h_vals[steps - 1] = lh_deriv_vals[steps - 1].t()
                                                                                                                                                                                                                              % (m_wy*ly_vals[steps - 1].t());
  for (size_t i = steps; i > 1; --i) {
    delta_h_vals[i - 2] = lh_deriv_vals[i - 2].t() %
        ((m_wh*delta_h_vals[i - 1]) + m_wy*(ly_vals[i - 2].t()));
  }

  for (size_t i = 0; i < steps; ++i) {
    mat grad = lh_vals[i].t()*delta_h_vals[i].t();
    m_wh -= m_lr*grad;
  }

  for (size_t i = 1; i <= steps; ++i) {
    mat grad = lh_vals[i].t()*ly_vals[i - 1];
    m_wy -= m_lr*grad;
  }

  for (size_t i = 0; i < steps; ++i) {
    delta_x_vals[i] = m_wx*delta_h_vals[i];
  }

  for (size_t i = 0; i < steps; ++i) {
    mat grad = x_vals[i]*delta_h_vals[i].t();
    m_wx -= m_lr*grad;
  }

  for (size_t i = 0; i < steps; ++i) {
    int start = 0;
    int start2 = de*cs;
    int start3 = start2 + ds*cs;
    for (size_t k = 0; k < cs; ++k) {
      m_emb.col(contextwins[i][k][0]) -=
          m_lr*delta_x_vals[i].rows(start, start + de - 1);
      m_suf.col(contextwins[i][k][1]) -=
          m_lr*delta_x_vals[i].rows(start2, start2 + ds - 1);
      m_cap.col(contextwins[i][k][2]) -=
          m_lr*delta_x_vals[i].rows(start3, start3 + dc - 1);
      start += de;
      start2 += ds;
      start3 += dc;
    }
  }

  m_h_tm1 = lh_vals[steps];
  return err;
}


pair<mat, mat>
RnnTagger::calc_lh_proj_dropout(size_t step, const vector<vector<size_t> > &word_vecs,
                                mat *x_vals, mat *x_mask_vals) {
  colvec x;
  colvec x0;
  colvec x1;
  colvec x2;

  // for dropout
  auto seed = chrono::high_resolution_clock::now().time_since_epoch().count();
  std::bernoulli_distribution dist(m_dropout_success_prob);
  std::default_random_engine engine(seed);

  for (size_t i = 0; i < m_cs; ++i) {
    x0 = join_cols(x0, m_emb.col(word_vecs[i][0]));
    x1 = join_cols(x1, m_suf.col(word_vecs[i][1]));
    x2 = join_cols(x2, m_cap.col(word_vecs[i][2]));
  }

  x = join_cols(join_cols(x0, x1), x2);

  // for dropout
  if (m_dropout) {
    colvec mask(x.n_rows, x.n_cols);
    mask.imbue( [&]() { return dist(engine); } );
    x_mask_vals[step] = mask;
    x = x % mask;
  }
  x_vals[step] = x;

  mat lh;
  mat lh_deriv;
  if (m_act == "relu") {
    lh = x.t()*m_wx + m_h_tm1*m_wh;
    lh.transform( [](arma::mat::elem_type& val){ return val > 0 ? val : 0.0;} );
    lh_deriv = lh;
    lh_deriv.transform( [](arma::mat::elem_type& val){ return val > 0 ? 1.0 : 0.0;} );
  } else {
    lh = sigmoid(x.t()*m_wx + m_h_tm1*m_wh);
    mat one = ones<mat>(lh.n_rows, lh.n_cols);
    lh_deriv = lh%(one - lh);
  }
  pair<mat, mat> res(lh, lh_deriv);

  m_h_tm1 = lh;
  return res;
}


double
RnnTagger::train_bptt_multi_dropout_bi(const vector<vector<vector<size_t> > > &contextwins,
                                       const vector<size_t> &label_batch,
                                       mat &y) {
  size_t cs = m_cs;
  size_t ds = m_ds;
  size_t dc = m_dc;
  size_t de = m_de;
  size_t steps = contextwins.size();

  double err = 0.0;

  mat ly_vals[steps];
  mat x_vals[steps];

  // forward
  mat lh_vals[steps + 1];
  lh_vals[0] = m_h_tm1;
  mat lh_deriv_vals[steps];

  // backward
  mat lh_vals_back[steps + 1];
  lh_vals_back[steps] = m_h_tm1_back;
  mat lh_deriv_vals_back[steps];

  // for dropout
  mat x_mask_vals[steps];

  mat delta_h_vals[steps];
  mat delta_h_vals_back[steps];
  mat delta_x_vals[steps];
  mat delta_x_vals_back[steps];

  for (size_t i = 0; i < steps; ++i) {
    pair<mat,  mat> vals =
        calc_lh_proj_dropout(i, contextwins[i], x_vals, x_mask_vals);
    mat lh = vals.first;
    mat lh_deriv = vals.second;
    lh_vals[i + 1] = lh;
    lh_deriv_vals[i] = lh_deriv;
  }

  // backward
  for (int i = steps - 1; i >= 0; --i) {
    if (m_act == "relu") {
      lh_vals_back[i] = (x_vals[i].t())*m_wx_back + lh_vals_back[i+1]*m_wh_back;
      lh_vals_back[i].transform( [](arma::mat::elem_type& val){ return val > 0 ? val : 0.0;} );
      lh_deriv_vals_back[i] = lh_vals_back[i];
      lh_deriv_vals_back[i].transform( [](arma::mat::elem_type& val){ return val > 0 ? 1.0 : 0.0;} );
    } else {
      lh_vals_back[i] = sigmoid((x_vals[i].t())*m_wx_back + lh_vals_back[i+1]*m_wh_back);
      mat one = ones<mat>(m_h_tm1.n_rows, m_h_tm1.n_cols);
      lh_deriv_vals_back[i] = lh_vals_back[i] % (one - lh_vals_back[i]);
    }
  }

  // predict
  for (size_t i = 0; i < steps; ++i) {
    y[label_batch[i]] = 1.0;
    //    soft_max_log(join_rows(lh_vals[i+1], lh_vals_back[i])*m_wy).print(cerr);
    //    cerr << "---------------\n";
    //    soft_max(join_rows(lh_vals[i+1], lh_vals_back[i])*m_wy).print(cerr);
    ly_vals[i] = soft_max(join_rows(lh_vals[i+1], lh_vals_back[i])*m_wy);
    err += x_ent_multi(ly_vals[i], y);
    //cerr << "err: " << err << endl;
    ly_vals[i](label_batch[i]) -= 1.0;
    y[label_batch[i]] = 0.0;
  }

  // bptt
  mat delta_y_vals_at_h[steps];
  for (size_t i = 0; i < steps; ++i) {
    delta_y_vals_at_h[i] = m_wy*(ly_vals[i].t());
  }

  delta_h_vals[steps - 1] = lh_deriv_vals[steps - 1].t() % (delta_y_vals_at_h[steps - 1].rows(0, m_nh - 1));
  for (int i = steps; i > 1; --i) {
    delta_h_vals[i - 2] = lh_deriv_vals[i - 2].t() %
        ((m_wh*delta_h_vals[i - 1]) + delta_y_vals_at_h[i - 2].rows(0, m_nh - 1));
  }

  // backward
  delta_h_vals_back[0] = lh_deriv_vals_back[0].t() % (delta_y_vals_at_h[0].rows(m_nh, delta_y_vals_at_h[0].n_rows - 1));
  for (size_t i = 1; i < steps; ++i) {
    delta_h_vals_back[i] = lh_deriv_vals_back[i].t() %
        ((m_wh_back*delta_h_vals_back[i - 1]) + delta_y_vals_at_h[i].rows(m_nh, delta_y_vals_at_h[0].n_rows - 1));
  }

  for (size_t i = 0; i < steps; ++i) {
    mat grad = lh_vals[i].t()*delta_h_vals[i].t();
    mat grad_back = lh_vals_back[i+1].t()*delta_h_vals_back[i].t();

    if (m_act == "relu" && m_clip != 0.0) {
      grad.transform( [&](arma::mat::elem_type& val) -> double { if (val < -m_clip) return -m_clip; else if (val > m_clip) return m_clip; else return val;} );
      grad_back.transform( [&](arma::mat::elem_type& val) -> double { if (val < -m_clip) return -m_clip; else if (val > m_clip) return m_clip; else return val;} );
    }
    m_wh -= m_lr * grad;
    m_wh_back -= m_lr * grad_back;
  }

  for (size_t i = 1; i <= steps; ++i) {
    mat grad = (join_rows(lh_vals[i], lh_vals_back[i - 1]).t())*ly_vals[i - 1];

    if (m_act == "relu" && m_clip != 0.0) {
      grad.transform( [&](arma::mat::elem_type& val) -> double { if (val < -m_clip) return -m_clip; else if (val > m_clip) return m_clip; else return val;} );
    }
    m_wy -= m_lr * grad;

    //    if (grad.max() >= 10)
    //      cerr << "grad wy explode 10\n";
    //    if (grad.min() <= -10)
    //      cerr << "grad wy vanish -10\n";
    //
    //    if (grad.max() >= 1)
    //      cerr << "grad wy explode 1\n";
    //    if (grad.min() <= -1)
    //      cerr << "grad wy vanish -1\n";

  }

  if (!m_dropout) {
    for (size_t i = 0; i < steps; ++i) {
      delta_x_vals[i] = (m_wx*delta_h_vals[i]);
    }

    for (size_t i = 0; i < steps; ++i) {
      delta_x_vals_back[i] = (m_wx_back*delta_h_vals_back[i]);
    }
  } else {

    for (size_t i = 0; i < steps; ++i) {
      delta_x_vals[i] = (m_wx*delta_h_vals[i]) % x_mask_vals[i];
    }

    for (size_t i = 0; i < steps; ++i) {
      delta_x_vals_back[i] = (m_wx_back*delta_h_vals_back[i]) % x_mask_vals[i];
    }
  }

  for (size_t i = 0; i < steps; ++i) {
    mat grad = x_vals[i]*delta_h_vals[i].t();
    mat grad_back = x_vals[i]*delta_h_vals_back[i].t();

    if (m_act == "relu" && m_clip != 0.0) {
      grad.transform( [&](arma::mat::elem_type& val) -> double { if (val < -m_clip) return -m_clip; else if (val > m_clip) return m_clip; else return val;} );
      grad_back.transform( [&](arma::mat::elem_type& val) -> double { if (val < -m_clip) return -m_clip; else if (val > m_clip) return m_clip; else return val;} );
    }

    //    if (grad.max() >= 10)
    //      cerr << "grad wx explode 10\n";
    //    if (grad.min() <= -10)
    //      cerr << "grad wx vanish -10\n";
    //
    //    if (grad.max() >= 1)
    //      cerr << "grad wx explode 1\n";
    //    if (grad.min() <= -1)
    //      cerr << "grad wx vanish -1\n";

    //    if (grad.max() >= 10)
    //      cerr << "grad wx_back explode 10\n";
    //    if (grad.min() <= -10)
    //      cerr << "grad wx_back vanish -10\n";
    //
    //    if (grad.max() >= 1)
    //      cerr << "grad wx_back explode 1\n";
    //    if (grad.min() <= -1)
    //      cerr << "grad wx_back vanish -1\n";
    m_wx -= m_lr * grad;
    m_wx_back -= m_lr * grad_back;
  }

  for (size_t i = 0; i < steps; ++i) {
    mat delta_x_vals_total = delta_x_vals[i] + delta_x_vals_back[i];
    int start = 0;
    int start2 = de*cs;
    int start3 = start2 + ds*cs;
    for (size_t k = 0; k < cs; ++k) {
      m_emb.col(contextwins[i][k][0]) -=
          m_lr*delta_x_vals_total.rows(start, start + de - 1);
      m_suf.col(contextwins[i][k][1]) -=
          m_lr*delta_x_vals_total.rows(start2, start2 + ds - 1);
      m_cap.col(contextwins[i][k][2]) -=
          m_lr*delta_x_vals_total.rows(start3, start3 + dc - 1);
      start += de;
      start2 += ds;
      start3 += dc;
    }
  }

  m_h_tm1 = lh_vals[steps];
  return err;
}


double
RnnTagger::train_bptt_multi_dropout_bi_global_norm_clip(const vector<vector<vector<size_t> > > &contextwins,
                                                        const vector<size_t> &label_batch,
                                                        mat &y) {
  size_t cs = m_cs;
  size_t ds = m_ds;
  size_t dc = m_dc;
  size_t de = m_de;
  size_t steps = contextwins.size();

  double err = 0.0;

  mat ly_vals[steps];
  mat x_vals[steps];

  // forward
  mat lh_vals[steps + 1];
  lh_vals[0] = m_h_tm1;
  mat lh_deriv_vals[steps];

  // backward
  mat lh_vals_back[steps + 1];
  lh_vals_back[steps] = m_h_tm1_back;
  mat lh_deriv_vals_back[steps];

  // for dropout
  mat x_mask_vals[steps];

  mat delta_h_vals[steps];
  mat delta_h_vals_back[steps];
  mat delta_x_vals[steps];
  mat delta_x_vals_back[steps];

  for (size_t i = 0; i < steps; ++i) {
    pair<mat,  mat> vals =
        calc_lh_proj_dropout(i, contextwins[i], x_vals, x_mask_vals);
    mat lh = vals.first;
    mat lh_deriv = vals.second;
    lh_vals[i + 1] = lh;
    lh_deriv_vals[i] = lh_deriv;
  }

  // backward
  for (int i = steps - 1; i >= 0; --i) {
    if (m_act == "relu") {
      lh_vals_back[i] = (x_vals[i].t())*m_wx_back + lh_vals_back[i+1]*m_wh_back;
      lh_vals_back[i].transform( [](arma::mat::elem_type& val){ return val > 0 ? val : 0.0;} );
      lh_deriv_vals_back[i] = lh_vals_back[i];
      lh_deriv_vals_back[i].transform( [](arma::mat::elem_type& val){ return val > 0 ? 1.0 : 0.0;} );
    } else {
      lh_vals_back[i] = sigmoid((x_vals[i].t())*m_wx_back + lh_vals_back[i+1]*m_wh_back);
      mat one = ones<mat>(m_h_tm1.n_rows, m_h_tm1.n_cols);
      lh_deriv_vals_back[i] = lh_vals_back[i] % (one - lh_vals_back[i]);
    }
  }

  // predict
  for (size_t i = 0; i < steps; ++i) {
    y[label_batch[i]] = 1.0;
    //    soft_max_log(join_rows(lh_vals[i+1], lh_vals_back[i])*m_wy).print(cerr);
    //    cerr << "---------------\n";
    //    soft_max(join_rows(lh_vals[i+1], lh_vals_back[i])*m_wy).print(cerr);
    ly_vals[i] = soft_max_log(join_rows(lh_vals[i+1], lh_vals_back[i])*m_wy);
    err += x_ent_multi(ly_vals[i], y);
    cerr << "err: " << err << endl;
    ly_vals[i](label_batch[i]) -= 1.0;
    y[label_batch[i]] = 0.0;
  }

  // bptt
  mat delta_y_vals_at_h[steps];
  for (size_t i = 0; i < steps; ++i) {
    delta_y_vals_at_h[i] = m_wy*(ly_vals[i].t());
  }

  delta_h_vals[steps - 1] = lh_deriv_vals[steps - 1].t() % (delta_y_vals_at_h[steps - 1].rows(0, m_nh - 1));
  for (int i = steps; i > 1; --i) {
    delta_h_vals[i - 2] = lh_deriv_vals[i - 2].t() %
        ((m_wh*delta_h_vals[i - 1]) + delta_y_vals_at_h[i - 2].rows(0, m_nh - 1));
  }

  // backward
  delta_h_vals_back[0] = lh_deriv_vals_back[0].t() % (delta_y_vals_at_h[0].rows(m_nh, delta_y_vals_at_h[0].n_rows - 1));
  for (size_t i = 1; i < steps; ++i) {
    delta_h_vals_back[i] = lh_deriv_vals_back[i].t() %
        ((m_wh_back*delta_h_vals_back[i - 1]) + delta_y_vals_at_h[i].rows(m_nh, delta_y_vals_at_h[0].n_rows - 1));
  }

  mat wh_grad(m_wh.n_rows, m_wh.n_cols, fill::zeros);
  mat wh_grad_back(m_wh.n_rows, m_wh.n_cols, fill::zeros);

  for (size_t i = 0; i < steps; ++i) {
    wh_grad += (lh_vals[i].t()*delta_h_vals[i].t());
    wh_grad_back += (lh_vals_back[i+1].t()*delta_h_vals_back[i].t());
    //m_wh -= m_lr * grad;
    //m_wh_back -= m_lr * grad_back;
  }

  mat wy_grad(m_wy.n_rows, m_wy.n_cols, fill::zeros);
  for (size_t i = 1; i <= steps; ++i) {
    wy_grad += ((join_rows(lh_vals[i], lh_vals_back[i - 1]).t())*ly_vals[i - 1]);
    //m_wy -= m_lr * grad;
  }

  if (!m_dropout) {
    for (size_t i = 0; i < steps; ++i) {
      delta_x_vals[i] = (m_wx*delta_h_vals[i]);
    }

    for (size_t i = 0; i < steps; ++i) {
      delta_x_vals_back[i] = (m_wx_back*delta_h_vals_back[i]);
    }
  } else {

    for (size_t i = 0; i < steps; ++i) {
      delta_x_vals[i] = (m_wx*delta_h_vals[i]) % x_mask_vals[i];
    }

    for (size_t i = 0; i < steps; ++i) {
      delta_x_vals_back[i] = (m_wx_back*delta_h_vals_back[i]) % x_mask_vals[i];
    }
  }

  mat wx_grad(m_wx.n_rows, m_wx.n_cols, fill::zeros);
  mat wx_grad_back(m_wx.n_rows, m_wx.n_cols, fill::zeros);

  for (size_t i = 0; i < steps; ++i) {
    wx_grad += (x_vals[i]*delta_h_vals[i].t());
    wx_grad_back += (x_vals[i]*delta_h_vals_back[i].t());
    //m_wx -= m_lr * grad;
    //m_wx_back -= m_lr * grad;
  }

  // compute global norm
  double global_norm = sqrt( accu(pow(vectorise(wh_grad),2)) + accu(pow(vectorise(wh_grad_back),2)) +
                             accu(pow(vectorise(wx_grad),2)) + accu(pow(vectorise(wx_grad_back),2)) +
                             accu(pow(vectorise(wy_grad),2)) );

  if (global_norm >= m_clip) {
    double f = m_clip/global_norm;
    wh_grad *= f;
    wh_grad_back *= f;
    wx_grad *= f;
    wx_grad_back *= f;
    wy_grad *= f;
  }

  m_wh -= m_lr*wh_grad;
  m_wh_back -= m_lr*wh_grad_back;
  m_wx -= m_lr*wx_grad;
  m_wx_back -= m_lr*wx_grad_back;
  m_wy -= m_lr*wy_grad;

  for (size_t i = 0; i < steps; ++i) {
    mat delta_x_vals_total = delta_x_vals[i] + delta_x_vals_back[i];
    int start = 0;
    int start2 = de*cs;
    int start3 = start2 + ds*cs;
    for (size_t k = 0; k < cs; ++k) {
      m_emb.col(contextwins[i][k][0]) -=
          m_lr*delta_x_vals_total.rows(start, start + de - 1);
      m_suf.col(contextwins[i][k][1]) -=
          m_lr*delta_x_vals_total.rows(start2, start2 + ds - 1);
      m_cap.col(contextwins[i][k][2]) -=
          m_lr*delta_x_vals_total.rows(start3, start3 + dc - 1);
      start += de;
      start2 += ds;
      start3 += dc;
    }
  }

  m_h_tm1 = lh_vals[steps];
  return err;
}


void
RnnTagger::run_bptt_xf1_bi(const vector<mat> &ly_vals) {
  // bptt, ly_vals == delta_y_vals (delta_y_vals usually directly stored at ly_vals, inplace modification)
  size_t steps = m_contextwins.size();
  assert(steps == ly_vals.size());
  mat delta_h_vals[steps];
  mat delta_h_vals_back[steps];
  mat delta_x_vals[steps];
  mat delta_x_vals_back[steps];
  m_lh_deriv_vals.resize(steps, m_h_tm1);
  m_lh_deriv_vals_back.resize(steps, m_h_tm1);
  mat delta_y_vals_at_h[steps];

  //  size_t cs = m_cs;
  //  size_t ds = m_ds;
  //  size_t dc = m_dc;
  //  size_t de = m_de;

  //double err = 0.0;

  //  mat ly_vals[steps];
  //  mat x_vals[steps];

  // forward
  //  mat lh_vals[steps + 1];
  //  lh_vals[0] = m_h_tm1;
  //  mat lh_deriv_vals[steps];

  // backward
  //  mat lh_vals_back[steps + 1];
  //  lh_vals_back[steps] = m_h_tm1_back;
  //  mat lh_deriv_vals_back[steps];

  mat one = ones<mat>(m_h_tm1.n_rows, m_h_tm1.n_cols);
  for (size_t i = 0; i < steps; ++i) {
    //    pair<mat,  mat> vals =
    //        calc_lh_proj_dropout(i, contextwins[i], x_vals, x_mask_vals);
    //    mat lh = vals.first;
    //    mat lh_deriv = vals.second;
    //    lh_vals[i + 1] = lh;
    if (m_act == "relu") {
      m_lh_deriv_vals[i] = m_lh_vals[i+1];
      m_lh_deriv_vals[i].transform( [](arma::mat::elem_type& val){ return val > 0 ? 1.0 : 0.0;} );
    } else {
      m_lh_deriv_vals[i] = m_lh_vals[i+1] % (one - m_lh_vals[i+1]);
    }
  }

  // backward
  for (int i = steps - 1; i >= 0; --i) {
    if (m_act == "relu") {
      //m_lh_vals_back[i] = (m_x_vals[i].t())*m_wx_back + m_lh_vals_back[i+1]*m_wh_back;
      m_lh_vals_back[i].transform( [](arma::mat::elem_type& val){ return val > 0 ? val : 0.0;} );
      m_lh_deriv_vals_back[i] = m_lh_vals_back[i];
      m_lh_deriv_vals_back[i].transform( [](arma::mat::elem_type& val){ return val > 0 ? 1.0 : 0.0;} );
    } else {
      //m_lh_vals_back[i] = sigmoid((m_x_vals[i].t())*m_wx_back + m_lh_vals_back[i+1]*m_wh_back);
      //mat one = ones<mat>(m_h_tm1.n_rows, m_h_tm1.n_cols);
      m_lh_deriv_vals_back[i] = m_lh_vals_back[i] % (one - m_lh_vals_back[i]);
    }
  }

  // predict
  //  for (size_t i = 0; i < steps; ++i) {
  //    y[label_batch[i]] = 1.0;
  //    //    soft_max_log(join_rows(lh_vals[i+1], lh_vals_back[i])*m_wy).print(cerr);
  //    //    cerr << "---------------\n";
  //    //    soft_max(join_rows(lh_vals[i+1], lh_vals_back[i])*m_wy).print(cerr);
  //    ly_vals[i] = soft_max_log(join_rows(lh_vals[i+1], lh_vals_back[i])*m_wy);
  //    err += x_ent_multi(ly_vals[i], y);
  //    cerr << "err: " << err << endl;
  //    ly_vals[i](label_batch[i]) -= 1.0;
  //    y[label_batch[i]] = 0.0;
  //  }

  // bptt
  for (size_t i = 0; i < steps; ++i) {
    delta_y_vals_at_h[i] = m_wy*(ly_vals[i].t());
  }

  delta_h_vals[steps - 1] = m_lh_deriv_vals[steps - 1].t() % (delta_y_vals_at_h[steps - 1].rows(0, m_nh - 1));
  for (int i = steps; i > 1; --i) {
    delta_h_vals[i - 2] = m_lh_deriv_vals[i - 2].t() %
        ((m_wh*delta_h_vals[i - 1]) + delta_y_vals_at_h[i - 2].rows(0, m_nh - 1));
  }

  // backward
  delta_h_vals_back[0] = m_lh_deriv_vals_back[0].t() % (delta_y_vals_at_h[0].rows(m_nh, delta_y_vals_at_h[0].n_rows - 1));
  for (size_t i = 1; i < steps; ++i) {
    delta_h_vals_back[i] = m_lh_deriv_vals_back[i].t() %
        ((m_wh_back*delta_h_vals_back[i - 1]) + delta_y_vals_at_h[i].rows(m_nh, delta_y_vals_at_h[0].n_rows - 1));
  }

  mat wh_grad(m_wh.n_rows, m_wh.n_cols, fill::zeros);
  mat wh_grad_back(m_wh.n_rows, m_wh.n_cols, fill::zeros);

  for (size_t i = 0; i < steps; ++i) {
    wh_grad += (m_lh_vals[i].t()*delta_h_vals[i].t());
    wh_grad_back += (m_lh_vals_back[i+1].t()*delta_h_vals_back[i].t());
    //m_wh -= m_lr * grad;
    //m_wh_back -= m_lr * grad_back;
  }

  mat wy_grad(m_wy.n_rows, m_wy.n_cols, fill::zeros);
  for (size_t i = 1; i <= steps; ++i) {
    wy_grad += ((join_rows(m_lh_vals[i], m_lh_vals_back[i - 1]).t())*ly_vals[i - 1]);
    //m_wy -= m_lr * grad;
  }

  if (!m_dropout) {
    for (size_t i = 0; i < steps; ++i) {
      delta_x_vals[i] = (m_wx*delta_h_vals[i]);
    }

    for (size_t i = 0; i < steps; ++i) {
      delta_x_vals_back[i] = (m_wx_back*delta_h_vals_back[i]);
    }
  } else {

    for (size_t i = 0; i < steps; ++i) {
      //delta_x_vals[i] = (m_wx*delta_h_vals[i]) % x_mask_vals[i];
      delta_x_vals[i] = (m_wx*delta_h_vals[i]) % m_dropout_vec;
    }

    for (size_t i = 0; i < steps; ++i) {
      //delta_x_vals_back[i] = (m_wx_back*delta_h_vals_back[i]) % x_mask_vals[i];
      delta_x_vals_back[i] = (m_wx_back*delta_h_vals_back[i]) % m_dropout_vec;
    }
  }

  mat wx_grad(m_wx.n_rows, m_wx.n_cols, fill::zeros);
  mat wx_grad_back(m_wx.n_rows, m_wx.n_cols, fill::zeros);

  for (size_t i = 0; i < steps; ++i) {
    wx_grad += (m_x_vals[i]*delta_h_vals[i].t());
    wx_grad_back += (m_x_vals[i]*delta_h_vals_back[i].t());
    //m_wx -= m_lr * grad;
    //m_wx_back -= m_lr * grad;
  }

  // compute global norm
  if (m_act == "relu") {
    double global_norm = sqrt( accu(pow(vectorise(wh_grad),2)) + accu(pow(vectorise(wh_grad_back),2)) +
                               accu(pow(vectorise(wx_grad),2)) + accu(pow(vectorise(wx_grad_back),2)) +
                               accu(pow(vectorise(wy_grad),2)) );

    if (global_norm >= m_clip) {
      double f = m_clip/global_norm;
      wh_grad *= f;
      wh_grad_back *= f;
      wx_grad *= f;
      wx_grad_back *= f;
      wy_grad *= f;
    }
  }

  m_wh -= m_lr*wh_grad;
  m_wh_back -= m_lr*wh_grad_back;
  m_wx -= m_lr*wx_grad;
  m_wx_back -= m_lr*wx_grad_back;
  m_wy -= m_lr*wy_grad;

  for (size_t i = 0; i < steps; ++i) {
    mat delta_x_vals_total = delta_x_vals[i] + delta_x_vals_back[i];
    int start = 0;
    int start2 = m_de*m_cs;
    int start3 = start2 + m_ds*m_cs;
    for (size_t k = 0; k < m_cs; ++k) {
      m_emb.col(m_contextwins[i][k][0]) -=
          m_lr*delta_x_vals_total.rows(start, start + m_de - 1);
      m_suf.col(m_contextwins[i][k][1]) -=
          m_lr*delta_x_vals_total.rows(start2, start2 + m_ds - 1);
      m_cap.col(m_contextwins[i][k][2]) -=
          m_lr*delta_x_vals_total.rows(start3, start3 + m_dc - 1);
      start += m_de;
      start2 += m_ds;
      start3 += m_dc;
    }
  }
}



double
RnnTagger::train_bptt_multi_dropout_bi_grad_check(const vector<vector<vector<size_t> > > &contextwins,
                                                  const vector<size_t> &label_batch,
                                                  mat &y) {
  size_t cs = m_cs;
  size_t ds = m_ds;
  size_t dc = m_dc;
  size_t de = m_de;
  size_t steps = contextwins.size();

  double err = 0.0;

  mat ly_vals[steps];
  mat x_vals[steps];

  // forward
  mat lh_vals[steps + 1];
  lh_vals[0] = m_h_tm1;
  mat lh_deriv_vals[steps];

  // backward
  mat lh_vals_back[steps + 1];
  lh_vals_back[steps] = m_h_tm1_back;
  mat lh_deriv_vals_back[steps];

  // for dropout
  //mat x_mask_vals[steps];
  // disable dropout for gradcheck

  mat delta_h_vals[steps];
  mat delta_h_vals_back[steps];
  mat delta_x_vals[steps];
  mat delta_x_vals_back[steps];

  for (size_t i = 0; i < steps; ++i) {
    pair<mat,  mat> vals =
        //calc_lh_proj_dropout(i, contextwins[i], x_vals, x_mask_vals);
        calc_lh_proj_emb(i, contextwins[i], x_vals);
    mat lh = vals.first;
    mat lh_deriv = vals.second;
    //ly_vals[i] = soft_max(lh*m_wy);
    lh_vals[i + 1] = lh;
    lh_deriv_vals[i] = lh_deriv;
  }

  // backward
  for (int i = steps - 1; i >= 0; --i) {
    if (m_act == "relu") {
      lh_vals_back[i] = (x_vals[i].t())*m_wx_back + lh_vals_back[i+1]*m_wh_back;
      lh_vals_back[i].transform( [](arma::mat::elem_type& val){ return val > 0 ? val : 0.0;} );
      lh_deriv_vals_back[i] = lh_vals_back[i];
      lh_deriv_vals_back[i].transform( [](arma::mat::elem_type& val){ return val > 0 ? 1.0 : 0.0;} );
    } else {
      lh_vals_back[i] = sigmoid((x_vals[i].t())*m_wx_back + lh_vals_back[i+1]*m_wh_back);
      mat one = ones<mat>(m_h_tm1.n_rows, m_h_tm1.n_cols);
      lh_deriv_vals_back[i] = lh_vals_back[i] % (one - lh_vals_back[i]);
    }
  }

  // predict
  for (size_t i = 0; i < steps; ++i) {
    y[label_batch[i]] = 1.0;
    ly_vals[i] = soft_max(join_rows(lh_vals[i+1], lh_vals_back[i])*m_wy);
    err += x_ent_multi(ly_vals[i], y);
    cerr << "_err: " << err << endl;
    ly_vals[i](label_batch[i]) -= 1.0;
    y[label_batch[i]] = 0.0;
  }

  cerr << "err: " << err << endl;

  // bptt
  mat delta_y_vals_at_h[steps];
  for (size_t i = 0; i < steps; ++i) {
    delta_y_vals_at_h[i] = m_wy*(ly_vals[i].t());
  }

  delta_h_vals[steps - 1] = lh_deriv_vals[steps - 1].t() % (delta_y_vals_at_h[steps - 1].rows(0, m_nh - 1));
  for (int i = steps; i > 1; --i) {
    delta_h_vals[i - 2] = lh_deriv_vals[i - 2].t() %
        ((m_wh*delta_h_vals[i - 1]) + delta_y_vals_at_h[i - 2].rows(0, m_nh - 1));
  }

  // backward
  delta_h_vals_back[0] = lh_deriv_vals_back[0].t() % (delta_y_vals_at_h[0].rows(m_nh, delta_y_vals_at_h[0].n_rows - 1));
  for (size_t i = 1; i < steps; ++i) {
    delta_h_vals_back[i] = lh_deriv_vals_back[i].t() %
        ((m_wh_back*delta_h_vals_back[i - 1]) + delta_y_vals_at_h[i].rows(m_nh, delta_y_vals_at_h[0].n_rows - 1));
  }

  for (size_t i = 0; i < steps; ++i) {
    mat grad = lh_vals[i].t()*delta_h_vals[i].t();
    mat grad_back = lh_vals_back[i+1].t()*delta_h_vals_back[i].t();
    m_wh -= m_lr * grad;
    m_wh_back -= m_lr * grad_back;
  }

  for (size_t i = 1; i <= steps; ++i) {
    mat grad = (join_rows(lh_vals[i], lh_vals_back[i - 1]).t())*ly_vals[i - 1];
    m_wy -= m_lr * grad;
  }

  for (size_t i = 0; i < steps; ++i) {
    delta_x_vals[i] = (m_wx*delta_h_vals[i]);
  }

  for (size_t i = 0; i < steps; ++i) {
    delta_x_vals_back[i] = (m_wx_back*delta_h_vals_back[i]);
  }

  //  for (size_t i = 0; i < steps; ++i) {
  //    delta_x_vals[i] = (m_wx*delta_h_vals[i]) % x_mask_vals[i];
  //  }
  //
  //  for (size_t i = 0; i < steps; ++i) {
  //    delta_x_vals_back[i] = (m_wx_back*delta_h_vals_back[i]) % x_mask_vals[i];
  //  }

  for (size_t i = 0; i < steps; ++i) {
    mat grad = x_vals[i]*delta_h_vals[i].t();
    m_wx -= m_lr * grad;
    grad = x_vals[i]*delta_h_vals_back[i].t();
    m_wx_back -= m_lr * grad;
  }

  for (size_t i = 0; i < steps; ++i) {
    mat delta_x_vals_total = delta_x_vals[i] + delta_x_vals_back[i];
    int start = 0;
    int start2 = de*cs;
    int start3 = start2 + ds*cs;
    for (size_t k = 0; k < cs; ++k) {
      m_emb.col(contextwins[i][k][0]) -=
          m_lr*delta_x_vals_total.rows(start, start + de - 1);
      m_suf.col(contextwins[i][k][1]) -=
          m_lr*delta_x_vals_total.rows(start2, start2 + ds - 1);
      m_cap.col(contextwins[i][k][2]) -=
          m_lr*delta_x_vals_total.rows(start3, start3 + dc - 1);
      start += de;
      start2 += ds;
      start3 += dc;
    }
  }

  m_h_tm1 = lh_vals[steps];
  return err;
}


double
RnnTagger::train_bptt_multi_dropout(const vector<vector<vector<size_t> > > &contextwins,
                                    const vector<size_t> &label_batch,
                                    mat &y) {
  size_t cs = m_cs;
  size_t ds = m_ds;
  size_t dc = m_dc;
  size_t de = m_de;
  size_t steps = contextwins.size();

  double err = 0.0;

  mat lh_vals[steps + 1];
  lh_vals[0] = m_h_tm1;
  mat lh_deriv_vals[steps];
  mat ly_vals[steps];
  mat x_vals[steps];

  // for dropout
  mat x_mask_vals[steps];

  mat delta_h_vals[steps];
  mat delta_x_vals[steps];

  for (size_t i = 0; i < steps; ++i) {
    pair<mat,  mat> vals =
        calc_lh_proj_dropout(i, contextwins[i], x_vals, x_mask_vals);
    mat lh = vals.first;
    mat lh_deriv = vals.second;
    ly_vals[i] = soft_max(lh*m_wy);
    lh_vals[i + 1] = lh;
    lh_deriv_vals[i] = lh_deriv;
  }

  for (size_t i = 0; i < steps; ++i) {
    y[label_batch[i]] = 1.0;
    err += x_ent_multi(ly_vals[i], y);
    ly_vals[i](label_batch[i]) -= 1.0;
    y[label_batch[i]] = 0.0;
  }

  // bptt multi
  delta_h_vals[steps - 1] = lh_deriv_vals[steps - 1].t()
                                                                                                                                                                                                                          % (m_wy*ly_vals[steps - 1].t());
  for (size_t i = steps; i > 1; --i) {
    delta_h_vals[i - 2] = lh_deriv_vals[i - 2].t() %
        ((m_wh*delta_h_vals[i - 1]) + m_wy*(ly_vals[i - 2].t()));
  }

  for (size_t i = 0; i < steps; ++i) {
    mat grad = lh_vals[i].t()*delta_h_vals[i].t();
    m_wh -= m_lr * grad;
  }

  for (size_t i = 1; i <= steps; ++i) {
    mat grad = lh_vals[i].t()*ly_vals[i - 1];
    m_wy -= m_lr * grad;

  }

  for (size_t i = 0; i < steps; ++i) {
    delta_x_vals[i] = (m_wx*delta_h_vals[i]) % x_mask_vals[i];
  }

  for (size_t i = 0; i < steps; ++i) {
    mat grad = x_vals[i]*delta_h_vals[i].t();
    m_wx -= m_lr * grad;
  }

  for (size_t i = 0; i < steps; ++i) {
    int start = 0;
    int start2 = de*cs;
    int start3 = start2 + ds*cs;
    for (size_t k = 0; k < cs; ++k) {
      m_emb.col(contextwins[i][k][0]) -=
          m_lr*delta_x_vals[i].rows(start, start + de - 1);
      m_suf.col(contextwins[i][k][1]) -=
          m_lr*delta_x_vals[i].rows(start2, start2 + ds - 1);
      m_cap.col(contextwins[i][k][2]) -=
          m_lr*delta_x_vals[i].rows(start3, start3 + dc - 1);
      start += de;
      start2 += ds;
      start3 += dc;
    }
  }

  m_h_tm1 = lh_vals[steps];
  return err;
}


void
RnnTagger::train_xf1_bi(Sentence &sent, const mat &tagger_pxf1f1,
                        const vector<vector<size_t>> &all_tag_seq_inds,
                        const vector<unordered_map<size_t, vector<size_t>>> &word_position_tag_hypos_map_vec) {
  assert(sent.all_ly_vals.size() == word_position_tag_hypos_map_vec.size());
  vector<mat> total_delta_y_vals_vec;
  total_delta_y_vals_vec.reserve(sent.words.size());
  size_t word_position = 0;

  for (auto &map: word_position_tag_hypos_map_vec) {
    mat total_delta_y(1, m_wy.n_cols, fill::zeros); // aggregrate delta_y at each position
    for (auto it = map.begin(); it != map.end(); ++it) {
      const size_t &tag_ind = it->first;
      mat delta_y = sent.all_ly_vals[word_position];
      delta_y *= -1.0;
      delta_y(tag_ind) = 1.0 - sent.all_ly_vals[word_position](tag_ind);

      for (auto &hypo_ind : it->second) {
        mat delta_y_one_ind = delta_y; // for each hypo using this tag
        delta_y_one_ind *= tagger_pxf1f1(hypo_ind);
        total_delta_y = total_delta_y + delta_y_one_ind;
      }
    }
    ++word_position;
    total_delta_y_vals_vec.emplace_back(total_delta_y);
  }
  run_bptt_xf1_bi(total_delta_y_vals_vec);
}


//void
//RnnTagger::tag_file(IO::ReaderFactory &reader, bool has_gold_labels) {
//
//  reader.reset();
//  Sentence sent;
//
//  vector<size_t> sent_res;
//  vector<size_t> sent_gold_labels;
//  vector<vector<size_t> > all_res;
//  vector<vector<size_t> > all_gold_labels;
//
//  double err = 0.0;
//  mat y = mat(1, m_nclasses, fill::zeros);
//
//  size_t total_eval_sents = 0;
//
//  while(reader.next(sent)) {
//    ++total_eval_sents;
//    sent_res.clear();
//    sent_gold_labels.clear();
//    sent_res.reserve(sent.words.size());
//    sent_gold_labels.reserve(sent.words.size());
//
//    if (has_gold_labels)
//      tag_sent_with_gold_labels(sent, sent_res, sent_gold_labels, err, y);
//    all_res.push_back(sent_res);
//    all_gold_labels.push_back(sent_gold_labels);
//    sent.reset();
//  }
//
//  assert(total_eval_sents = all_res.size());
//
//  if (has_gold_labels) {
//    cerr << "total sents evaluated: " << total_eval_sents << endl;
//
//    double total = 0.0;
//    double correct = 0.0;
//    for (size_t k = 0; k < all_gold_labels.size(); ++k) {
//      total += all_gold_labels[k].size();
//      for (size_t l = 0; l < all_gold_labels[k].size(); ++l) {
//        assert(all_gold_labels[k].size() == all_res[k].size());
//        if (all_gold_labels[k][l] == all_res[k][l])
//          ++correct;
//      }
//    }
//
//    assert(correct <= total);
//    cerr << "1-best accuracy: " << correct / total << endl;
//  }
//}


void
RnnTagger::tag_file(IO::ReaderFactory &reader, bool has_gold_labels) {

  reader.reset();
  Sentence sent;

  vector<size_t> sent_res;
  vector<size_t> sent_gold_labels;
  vector<vector<size_t> > all_res;
  vector<vector<size_t> > all_gold_labels;

  double err = 0.0;
  mat y = mat(1, m_nclasses, fill::zeros);

  size_t total_eval_sents = 0;

  while(reader.next(sent)) {
    ++total_eval_sents;

    if (m_bi) {
      tag_sent_bi(sent, sent_res, sent_gold_labels, err, y, has_gold_labels);
    } else {
      tag_sent(sent, sent_res, sent_gold_labels, err, y, has_gold_labels);
    }
    all_res.push_back(sent_res);

    if (m_out_fmt == 1) {
      for (size_t i = 0; i < sent_res.size(); ++i) {
        cout << sent.words[i] << "|" << sent.pos[i] << "|" << m_label2rawtag_map.find(sent_res[i])->second;
        if (i < sent_res.size() - 1)
          cout << " ";
      }
      cout << endl;
    }

    all_gold_labels.push_back(sent_gold_labels);
    sent_res.clear();
    sent_gold_labels.clear();
    sent.reset();
  }

  assert(total_eval_sents = all_res.size());

  if (has_gold_labels) {
    cerr << "total sents evaluated: " << total_eval_sents << endl;

    double total = 0.0;
    double correct = 0.0;
    for (size_t k = 0; k < all_gold_labels.size(); ++k) {
      total += all_gold_labels[k].size();
      for (size_t l = 0; l < all_gold_labels[k].size(); ++l) {
        assert(all_gold_labels[k].size() == all_res[k].size());
        if (all_gold_labels[k][l] == all_res[k][l])
          ++correct;
      }
    }

    assert(correct <= total);
    cerr << "accuracy: " << correct / total << endl;
    cerr << "err: " << err / (double)total_eval_sents << endl;
  }
}


void
RnnTagger::tag_sent(Sentence &sent,
                    vector<size_t> &sent_res,
                    vector<size_t> &sent_gold_labels,
                    double &err, mat &y, bool has_gold_labels) {

  //  vector<vector<vector<size_t> > > words;
  //  vector<vector<size_t> > labels;
  //
  //  words.clear();
  //  labels.clear();
  //  words.reserve(sent.words.size());
  //  labels.reserve(sent.words.size());

  vector<string> &sent_words = sent.words;
  vector<string> &sent_cats = sent.super;
  vector<vector<size_t> > sent_word_vecs;

  for (size_t i = 0; i < sent.words.size(); ++i) {
    sent_word_vecs.push_back(word2vec(sent_words[i]));

    unordered_map<string, size_t>::const_iterator iter = m_lex_cat_str_ind_map.find(sent_cats[i]);
    if (iter != m_lex_cat_str_ind_map.end())
      sent_gold_labels.push_back(iter->second);
    else
      sent_gold_labels.push_back(m_UNKNWON_SUPERCAT_IND);
  }

  vector<vector<vector<size_t> > > contextwins;
  sent2contextwin(sent_word_vecs, contextwins);
  classify(contextwins, sent_gold_labels, y, sent_res, err, has_gold_labels);
}


void
RnnTagger::tag_sent_bi(Sentence &sent,
                       vector<size_t> &sent_res,
                       vector<size_t> &sent_gold_labels,
                       double &err, mat &y, bool has_gold_labels) {
  vector<string> &sent_words = sent.words;
  vector<string> &sent_cats = sent.super;
  vector<vector<size_t> > sent_word_vecs;

  for (size_t i = 0; i < sent.words.size(); ++i) {
    sent_word_vecs.push_back(word2vec(sent_words[i]));

    unordered_map<string, size_t>::const_iterator iter = m_lex_cat_str_ind_map.find(sent_cats[i]);
    if (iter != m_lex_cat_str_ind_map.end())
      sent_gold_labels.push_back(iter->second);
    else
      sent_gold_labels.push_back(m_UNKNWON_SUPERCAT_IND);
  }

  vector<vector<vector<size_t> > > contextwins;
  sent2contextwin(sent_word_vecs, contextwins);
  classify_bi(contextwins, sent_gold_labels, y, sent_res, err, has_gold_labels);
}


void
RnnTagger::tag_sent_bi_grad_check(Sentence &sent,
                                  vector<size_t> &sent_res,
                                  vector<size_t> &sent_gold_labels,
                                  double &err, mat &y) {
  sent_gold_labels.clear();
  vector<string> &sent_words = sent.words;
  vector<string> &sent_cats = sent.super;
  vector<vector<size_t> > sent_word_vecs;

  for (size_t i = 0; i < sent.words.size(); ++i) {
    sent_word_vecs.push_back(word2vec(sent_words[i]));

    unordered_map<string, size_t>::const_iterator iter = m_lex_cat_str_ind_map.find(sent_cats[i]);
    if (iter != m_lex_cat_str_ind_map.end())
      sent_gold_labels.push_back(iter->second);
    else
      sent_gold_labels.push_back(m_UNKNWON_SUPERCAT_IND);
  }

  vector<vector<vector<size_t> > > contextwins;
  sent2contextwin(sent_word_vecs, contextwins);
  classify_bi(contextwins, sent_gold_labels, y, sent_res, err, true);
}


void
RnnTagger::classify(const vector<vector<vector<size_t> > > &contextwins,
                    vector<size_t> &gold_labels,
                    mat &y, vector<size_t> &sent_res, double &err, bool has_gold_labels) {
  m_h_tm1.zeros();
  uword ind = 0;

  for (size_t i = 0; i < contextwins.size(); ++i) {
    colvec x;
    colvec x0;
    colvec x1;
    colvec x2;

    for (size_t j = 0; j < m_cs; ++j) {

      x0 = join_cols(x0, m_emb.col(contextwins[i][j][0]));
      x1 = join_cols(x1, m_suf.col(contextwins[i][j][1]));
      x2 = join_cols(x2, m_cap.col(contextwins[i][j][2]));
    }

    x = join_cols(join_cols(x0, x1), x2);
    if (m_dropout)
      x = x % m_dropout_vec;
    mat lh = sigmoid(x.t()*m_wx + m_h_tm1*m_wh);
    mat ly = soft_max(lh*m_wy);

    if (has_gold_labels) {
      y(gold_labels[i]) = 1.0;
      err += x_ent_multi(ly, y);
      y(gold_labels[i]) = 0.0;
    }
    ly.max(ind);
    sent_res.push_back(ind);

    m_h_tm1 = lh;
  }
}


void RnnTagger::grad_check_bi(IO::ReaderFactory &reader, IO::ReaderFactory &reader_dev) {
  reader.reset();
  Sentence sent;

  vector<size_t> sent_res;
  vector<size_t> sent_gold_labels;
  vector<vector<size_t> > all_gold_labels;

  double err_plus = 0.0;
  double err_minus = 0.0;
  mat y = mat(1, m_nclasses, fill::zeros);

  size_t total_eval_sents = 0;

  mat m_wx_cp = m_wx;
  mat m_wh_old = m_wh;
  mat m_wy_old = m_wy;
  mat m_wx_back_old = m_wx_back;
  mat m_wh_back_old = m_wh_back;
  mat m_emb_old = m_emb;
  mat m_suf_old = m_suf;
  mat m_cap_old = m_cap;

  double c = 0.00001;
  while(reader.next(sent)) {
    err_plus = 0.0;
    err_minus = 0.0;
    ++total_eval_sents;

    m_wx(0,0) += c;
    tag_sent_bi_grad_check(sent, sent_res, sent_gold_labels, err_plus, y);
    m_wx = m_wx_cp;
    m_wx(0,0) -= c;
    tag_sent_bi_grad_check(sent, sent_res, sent_gold_labels, err_minus, y);
    m_wx = m_wx_cp;

    cerr << "+: " << err_plus << endl;
    cerr << "-: " << err_minus << endl;
    cerr << "num_grad_wx(0,0): " << (err_plus-err_minus)/(2.0*c) << endl;

    err_plus = 0.0;
    err_minus = 0.0;
    m_wx_back(0,0) += c;
    tag_sent_bi_grad_check(sent, sent_res, sent_gold_labels, err_plus, y);
    m_wx_back = m_wx_back_old;
    m_wx_back(0,0) -= c;
    tag_sent_bi_grad_check(sent, sent_res, sent_gold_labels, err_minus, y);
    m_wx_back = m_wx_back_old;
    cerr << "num_grad_wx_back(0,0): " << (err_plus-err_minus)/(2.0*c) << endl;

    err_plus = 0.0;
    err_minus = 0.0;
    m_wh(0,0) += c;
    tag_sent_bi_grad_check(sent, sent_res, sent_gold_labels, err_plus, y);
    m_wh = m_wh_old;
    m_wh(0,0) -= c;
    tag_sent_bi_grad_check(sent, sent_res, sent_gold_labels, err_minus, y);
    m_wh = m_wh_old;
    cerr << "num_grad_wh(0,0): " << (err_plus-err_minus)/(2.0*c) << endl;

    err_plus = 0.0;
    err_minus = 0.0;
    m_wh_back(0,0) += c;
    tag_sent_bi_grad_check(sent, sent_res, sent_gold_labels, err_plus, y);
    m_wh_back = m_wh_back_old;
    m_wh_back(0,0) -= c;
    tag_sent_bi_grad_check(sent, sent_res, sent_gold_labels, err_minus, y);
    m_wh_back = m_wh_back_old;
    cerr << "num_grad_wh_back(0,0): " << (err_plus-err_minus)/(2.0*c) << endl;

    err_plus = 0.0;
    err_minus = 0.0;
    m_wy(0,0) += c;
    tag_sent_bi_grad_check(sent, sent_res, sent_gold_labels, err_plus, y);
    m_wy = m_wy_old;
    m_wy(0,0) -= c;
    tag_sent_bi_grad_check(sent, sent_res, sent_gold_labels, err_minus, y);
    m_wy = m_wy_old;
    cerr << "num_grad_wy(0,0): " << (err_plus-err_minus)/(2.0*c) << endl;

    err_plus = 0.0;
    err_minus = 0.0;
    m_emb(0,0) += c;
    tag_sent_bi_grad_check(sent, sent_res, sent_gold_labels, err_plus, y);
    m_emb = m_emb_old;
    m_emb(0,0) -= c;
    tag_sent_bi_grad_check(sent, sent_res, sent_gold_labels, err_minus, y);
    m_emb = m_emb_old;
    cerr << "num_grad_emb(0,0): " << (err_plus-err_minus)/(2.0*c) << endl;

    err_plus = 0.0;
    err_minus = 0.0;
    m_suf(0,0) += c;
    tag_sent_bi_grad_check(sent, sent_res, sent_gold_labels, err_plus, y);
    m_suf = m_suf_old;
    m_suf(0,0) -= c;
    tag_sent_bi_grad_check(sent, sent_res, sent_gold_labels, err_minus, y);
    m_suf = m_suf_old;
    cerr << "num_grad_suf(0,0): " << (err_plus-err_minus)/(2.0*c) << endl;

    err_plus = 0.0;
    err_minus = 0.0;
    m_cap(0,0) += c;
    tag_sent_bi_grad_check(sent, sent_res, sent_gold_labels, err_plus, y);
    m_cap = m_cap_old;
    m_cap(0,0) -= c;
    tag_sent_bi_grad_check(sent, sent_res, sent_gold_labels, err_minus, y);
    m_cap = m_cap_old;
    cerr << "num_grad_cap(0,0): " << (err_plus-err_minus)/(2.0*c) << endl;

    sent.reset();
  }

  reader.reset();
  train_grad_check(reader, reader_dev);
}


void
RnnTagger::classify_bi(const vector<vector<vector<size_t> > > &contextwins,
                       vector<size_t> &gold_labels,
                       mat &y, vector<size_t> &sent_res, double &err, bool has_gold_labels) {
  m_h_tm1.zeros();
  uword ind = 0;
  vector<mat> x_vals;
  vector<mat> lh_vals;
  x_vals.reserve(contextwins.size());
  lh_vals.reserve(contextwins.size() + 1);
  lh_vals.emplace_back(m_h_tm1);
  mat lh_vals_back[contextwins.size() + 1];
  lh_vals_back[contextwins.size()] = m_h_tm1;

  for (size_t i = 0; i < contextwins.size(); ++i) {
    colvec x;
    colvec x0;
    colvec x1;
    colvec x2;

    for (size_t j = 0; j < m_cs; ++j) {

      x0 = join_cols(x0, m_emb.col(contextwins[i][j][0]));
      x1 = join_cols(x1, m_suf.col(contextwins[i][j][1]));
      x2 = join_cols(x2, m_cap.col(contextwins[i][j][2]));
    }

    x = join_cols(join_cols(x0, x1), x2);
    if (m_dropout)
      x = x % m_dropout_vec;
    x_vals.emplace_back(x);
    if (m_act == "relu") {
      lh_vals.emplace_back(x.t()*m_wx + lh_vals[i]*m_wh);
      lh_vals[i+1].transform( [](arma::mat::elem_type& val){ return val > 0 ? val : 0.0;} );
    } else {
      lh_vals.emplace_back(sigmoid(x.t()*m_wx + lh_vals[i]*m_wh));
    }
  }

  for (int i = contextwins.size() - 1; i >=0; --i) {
    if (m_act == "relu") {
      lh_vals_back[i] = (x_vals[i].t()*m_wx_back + lh_vals_back[i + 1]*m_wh_back);
      lh_vals_back[i].transform( [](arma::mat::elem_type& val){ return val > 0 ? val : 0.0;} );
    } else {
      lh_vals_back[i] = (sigmoid(x_vals[i].t()*m_wx_back + lh_vals_back[i + 1]*m_wh_back));
    }
    //lh_vals_back[i] = (sigmoid(x_vals[i].t()*m_wx_back + lh_vals_back[i + 1]*m_wh_back));
  }

  assert(contextwins.size() == gold_labels.size());
  for (size_t i = 1; i <= contextwins.size(); ++i) {
    mat ly = soft_max_log(join_rows(lh_vals[i], lh_vals_back[i-1])*m_wy);
    if (has_gold_labels) {
      y(gold_labels[i-1]) = 1.0;
      err += x_ent_multi(ly, y);
      y(gold_labels[i-1]) = 0.0;
    }
    ly.max(ind);
    sent_res.push_back(ind);
  }
}


//void
//RnnTagger::classify_bi_grad_check(const vector<vector<vector<size_t> > > &contextwins,
//                                  vector<size_t> &gold_labels, mat &y, double &err) {
//  m_h_tm1.zeros();
//  //uword ind;
//  vector<mat> x_vals;
//  vector<mat> lh_vals;
//  x_vals.reserve(contextwins.size());
//  lh_vals.reserve(contextwins.size() + 1);
//  lh_vals.emplace_back(m_h_tm1);
//  mat lh_vals_back[contextwins.size() + 1];
//  lh_vals_back[contextwins.size()] = m_h_tm1;
//
//  for (size_t i = 0; i < contextwins.size(); ++i) {
//    colvec x;
//    colvec x0;
//    colvec x1;
//    colvec x2;
//
//    for (size_t j = 0; j < m_cs; ++j) {
//      x0 = join_cols(x0, m_emb.col(contextwins[i][j][0]));
//      x1 = join_cols(x1, m_suf.col(contextwins[i][j][1]));
//      x2 = join_cols(x2, m_cap.col(contextwins[i][j][2]));
//    }
//
//    x = join_cols(join_cols(x0, x1), x2);
//    //    if (m_dropout)
//    //      x = x % m_dropout_vec;
//    x_vals.emplace_back(x);
//    if (m_act == "relu") {
//      lh_vals.emplace_back(x.t()*m_wx + lh_vals[i]*m_wh);
//      cerr << "lh_vals before: " << endl;
//      lh_vals[i+1].print(cerr);
//      lh_vals[i+1].transform( [](arma::mat::elem_type& val){ return val > 0 ? val : 0.0;} );
//      cerr << "lh_vals after: " << endl;
//      lh_vals[i+1].print(cerr);
//    } else {
//      lh_vals.emplace_back(sigmoid(x.t()*m_wx + lh_vals[i]*m_wh));
//    }
//  }
//
//  for (int i = contextwins.size() - 1; i >= 0; --i) {
//    if (m_act == "relu") {
//      lh_vals_back[i] = (x_vals[i].t()*m_wx_back + lh_vals_back[i + 1]*m_wh_back);
//      lh_vals_back[i].transform( [](arma::mat::elem_type& val){ return val > 0 ? val : 0.0;} );
//    } else {
//      lh_vals_back[i] = (sigmoid(x_vals[i].t()*m_wx_back + lh_vals_back[i + 1]*m_wh_back));
//    }
//  }
//
//  cerr << "contextwins.size(): " << contextwins.size() << endl;
//  cerr << "gold_labels.size(): " << gold_labels.size() << endl;
//  assert(contextwins.size() == gold_labels.size());
//  for (size_t i = 1; i <= contextwins.size(); ++i) {
//    mat ly = soft_max(join_rows(lh_vals[i], lh_vals_back[i-1])*m_wy);
//    y(gold_labels[i-1]) = 1.0;
//    err += x_ent_multi(ly, y);
//    cerr << "_err: " << err << endl;
//    y(gold_labels[i-1]) = 0.0;
//  }
//}


void
RnnTagger::mtag_sent(Sentence &sent, double BETA) {
  //
  //  vector<vector<vector<size_t> > > words;
  //  words.reserve(sent.words.size());

  vector<string> &sent_words = sent.words;
  vector<vector<size_t> > sent_word_vecs;

  for (size_t i = 0; i < sent.words.size(); ++i) {
    sent_word_vecs.push_back(word2vec(sent_words[i]));
  }

  vector<vector<vector<size_t> > > contextwins;
  sent2contextwin(sent_word_vecs, contextwins);
  if (m_bi)
    classify_multi_bi(contextwins, sent, BETA);
  else
    classify_multi(contextwins, sent, BETA);

  if (m_out_fmt == 2) {
    for (size_t i = 0; i < sent.words.size(); ++i) {
      cout << sent.words[i] << " " << sent.pos[i] << " " << sent.msuper[i].size() << " ";
      for (size_t j = 0; j < sent.msuper[i].size(); ++j) {
        if (sent.msuper[i][j].score >= BETA * sent.msuper[i][0].score) {
          cout << fixed << setprecision(20) << sent.msuper[i][j].raw << " " << sent.msuper[i][j].score;
          if (j < sent.msuper[i].size() - 1)
            cout << " ";
        }
      }
      cout << endl;
    }
    cout << endl;
  }
}

vector<size_t>
RnnTagger::word2vec(string &word) const {
  vector<size_t> word_vec;
  string original_word = word;
  string word_copy = word;

  if (m_lowercase_words)
    std::transform(word_copy.begin(), word_copy.end(), word_copy.begin(), ::tolower);

  unordered_map<string, size_t>::const_iterator iter = m_full_wordstr_emb_map.find(word_copy);

  if (iter != m_full_wordstr_emb_map.end()) {
    word_vec.push_back(iter->second);
  } else {
    // not in pre-trained embeddings, try to back off
    bool backed_off = false;
    vector<string> back_off_strs;
    clean_str(original_word, back_off_strs);
    for (size_t i = 0; i < back_off_strs.size(); ++i) {
      iter = m_full_wordstr_emb_map.find(back_off_strs[i]);
      if (iter != m_full_wordstr_emb_map.end()) {
        backed_off = true;
        word_vec.push_back(iter->second);
        break;
      }
    }

    if (!backed_off) {
      // unknown word
      if (is_alphnum(original_word)) {
        if (islower(original_word[0]))
          word_vec.push_back(m_UNK_ALPHNUM_LOWER_IND);
        else
          word_vec.push_back(m_UNK_ALPHNUM_UPPER_IND);
      } else {
        word_vec.push_back(m_UNK_NON_ALPHNUM_IND);
      }
    }

  }

  unordered_map<string, size_t>::const_iterator iter2 = m_suf_str_ind_map.find(get_suf(original_word));
  if (iter2 != m_suf_str_ind_map.end()) {
    word_vec.push_back(iter2->second);
    //cerr << "suf: " << iter2->second << endl;
  }
  else
    word_vec.push_back(m_UNK_SUFFIX_IND);

  if (islower(original_word[0]))
    word_vec.push_back(0);
  else
    word_vec.push_back(1);

  return word_vec;
}


void
RnnTagger::replace_all_substrs(string &str,
                               const string &substr,
                               const string &re) const {
  string::size_type n = 0;

  while ((n = str.find(substr, n)) != std::string::npos) {
    str.replace(n, substr.size(), re);
    n += substr.size();
  }
}


void
RnnTagger::clean_str(string &word, vector<string> &ret) const {

  string word_copy1 = word;
  if (!is_lower(word)) {
    std::transform(word_copy1.begin(), word_copy1.end(), word_copy1.begin(), ::tolower);
    ret.push_back(word_copy1);
  }

  string word_copy2 = word;
  replace_all_substrs(word_copy2, ",", "");
  replace_all_substrs(word_copy2, ".", "");
  replace_all_substrs(word_copy2, "-", "");
  replace_all_substrs(word_copy2, " ", "");
  replace_all_substrs(word_copy2, "\\/", "");

  if (all_of(word_copy2.begin(), word_copy2.end(), ::isdigit)) {
    ret.push_back("0");
  } else if (word.find('-') != string::npos) {
    size_t last_pos = word.find_last_of('-');
    assert(last_pos != string::npos);
    string sub_str = word.substr(last_pos + 1);
    ret.push_back(sub_str);

    if (!is_lower(sub_str)) {
      std::transform(sub_str.begin(), sub_str.end(), sub_str.begin(), ::tolower);
      ret.push_back(sub_str);
    }

  } else {
    size_t pos = word.find("\\/");
    if (pos != string::npos) {
      size_t last_pos = word.find_last_of("/");
      string sub_str = word.substr(last_pos + 1);
      ret.push_back(sub_str);

      if (!is_lower(sub_str)) {
        std::transform(sub_str.begin(), sub_str.end(), sub_str.begin(), ::tolower);
        ret.push_back(sub_str);
      }
    }
  }

}


string RnnTagger::get_suf(const string &word) const {
  string word_copy = word;
  //cerr << "word: " << word << endl;
  replace_all_substrs(word_copy, ",", "");
  replace_all_substrs(word_copy, ".", "");
  replace_all_substrs(word_copy, "-", "");
  replace_all_substrs(word_copy, " ", "");
  replace_all_substrs(word_copy, "\\/", "");
  if (all_of(word_copy.begin(), word_copy.end(), ::isdigit))
    return "0";
  else {
    if (word.length() >= 2)
      return word.substr(word.length() - 2);
    else
      return word;
  }
}


bool
RnnTagger::is_lower(const string &word) const {
  for (size_t i = 0; i < word.length(); ++i) {
    if (isupper(word[i]))
      return false;
  }

  return true;
}


bool
RnnTagger::is_alphnum(const string &word) const {
  for (size_t i = 0; i < word.length(); ++i) {
    if (!isalnum(word[i]))
      return false;
  }

  return true;

}

void
RnnTagger::load_suf_str_ind_map(const string &file) {

  cerr << "loading suf str ind map\n";

  ifstream in(file.c_str());
  if(!in) {
    cerr << "could not open suf_str_ind file " << file << endl;
    exit(EXIT_FAILURE);
  }

  string suf;
  size_t temp;
  size_t total = 0;

  while (in >> suf >> temp) {
    m_suf_str_ind_map.insert(make_pair(suf, total));
    ++total;
  }

  cerr << "total suffix count (from sec0221): " << total << endl;

}


} }
