// Copyright (c) Cambridge University Computer Lab
// Copyright (c) Wenduan Xu

#include "xf1_trainner.h"
#include "sentence.h"
#include "parser/filled.h"
#include "parser/print_stream.h"
#include "parser/ShiftReduceHypothesis.h"
#include <limits>

using namespace arma;

namespace NLP { namespace Taggers {


XF1Trainer::XF1Trainer(size_t nh, size_t vocsize, size_t nclasses, size_t cs, size_t ws,
    size_t suffix_count, size_t bs, size_t de, size_t ds, size_t dc,
    size_t bs_xf1,
    double lr, bool ignore_0_f1_seq,
    const string &wx, const string &wh, const string &wy,
    const string &emb, const string &suf, const string &cap,
    const unsigned int k, NLP::CCG::Parser::Config &parser_cfg,
    unsigned long load, CCG::DepsPrinter &printer,
		size_t sr_beam, const std::string srmodel,
		const std::vector<double> &perceptronId,
		double eps,
		const std::string &ind2label_file)
: m_cs(cs), m_ws(ws), m_vocsize(vocsize), m_suffix_count(suffix_count),
  m_nclasses(nclasses), m_ntokens0221(0), m_ntokens00(0), m_ntokens23(0), m_bs(bs),
  m_tagger(nh, vocsize, nclasses, cs, ws, suffix_count, wx, wh, wy, emb, suf, cap, ind2label_file),
  m_cats(parser_cfg.cats(), parser_cfg.markedup(), parser_cfg.alt_markedup()),
  m_k(k),
	m_parser(parser_cfg, m_sent, m_cats, perceptronId, false, sr_beam, load),
  m_printer(printer),
  m_lexicon("lexicon", parser_cfg.lexicon()),
  total_correct(0.0), total_returned(0.0), total_gold(0.0),
  m_de(de), m_ds(ds), m_dc(dc),
  m_bs_xf1(bs_xf1),
  m_lr(lr), m_ignore_0_f1_seq(ignore_0_f1_seq),
  m_eps(eps) {
  m_wx.set_size((de + ds + dc)*cs, nh);
  m_wh.set_size(nh, nh);
  m_wy.set_size(nh, nclasses);
  m_emb.set_size(de, vocsize + 4);
  m_suf.set_size(ds, suffix_count + 1);
  m_cap.set_size(dc, 2);
  m_h_tm1.set_size(1, nh);
  m_h_tm1.zeros();
  m_y.set_size(1, nclasses);
  m_y.zeros();

	m_parser.LoadModel(srmodel);

	cerr << "Minimum value for double: " << std::numeric_limits<double>::min() << '\n';
	cerr << "Maximum value for double: " << std::numeric_limits<double>::max() << '\n';

  std::mt19937 engine;
  std::uniform_real_distribution<double> uni(-1.0, 1.0);

//  m_emb.load(emb);
//  m_suf.load(suf);
//  m_cap.load(cap);
//
//  m_wx.load(wx);
//  m_wh.load(wh);
//  m_wy.load(wy);

  load_emb_mat(m_emb, "./reduced_emb2idx_dict", m_vocsize + 4);

//  m_emb.imbue( [&]() { return uni(engine); } );
//  m_emb *= 0.2;

  m_suf.imbue( [&]() { return uni(engine); } );
  m_suf *= 0.2;

  m_cap.imbue( [&]() { return uni(engine); } );
  m_cap *= 0.2;

  m_wx.imbue( [&]() { return uni(engine); } );
  m_wx *= 0.2;

  m_wy.imbue( [&]() { return uni(engine); } );
  m_wy *= 0.2;

  m_wh.imbue( [&]() { return uni(engine); } );
  m_wh *= 0.2;
}


XF1Trainer::~XF1Trainer(void) {}


void
XF1Trainer::init_training(const string &words, const string &labels) {
  load_gold_deps("./wsj02-21.ccgbank_deps");
  init_ignore_rules();
  load_dep_ignore_list("./deps_ignore_cat_slot_rule");
  load_dep_ignore_list2("./deps_ignore_pred_cat_slot_rule");
  load_dep_ignore_list3("./deps_ignore_pred_cat_slot_arg_rule");
  gen_kbest(words, labels);
}


void
XF1Trainer::init_xf1_eval() {
  load_gold_deps("./wsj00.ccgbank_deps", false);
  init_ignore_rules();
  load_dep_ignore_list("./deps_ignore_cat_slot_rule");
  load_dep_ignore_list2("./deps_ignore_pred_cat_slot_rule");
  load_dep_ignore_list3("./deps_ignore_pred_cat_slot_arg_rule");
  m_tagger.load_dev_test_data();
}


void
XF1Trainer::load_emb_mat(mat &emb, const string &filename, int ne) {
  ifstream in(filename.c_str());
  if (!in)
    throw runtime_error("no such file: " + filename);
  string line;
  bool odd = true;
  int ind;
  while (in) {
    if (!getline(in, line)) break;
    istringstream iss(line);
    if (odd) {
      iss >> ind;
      odd = false;
    } else {
      mat temp(line);
      if (ind == -1) {
        emb.col(ne - 1) = temp.t();
      } else {
        emb.col(ind) = temp.t();
        odd = true;
      }
    }
  }
}


//void
//XF1Trainer::re_init_mats(const string &wx, const string &wh, const string &wy,
//                         const string &emb, const string &suf, const string &cap) {
//  m_tagger.re_init_mats(wx, wh, wy, emb, suf, cap);
//}


void
XF1Trainer::re_init_mats(const string &wx, const string &wh, const string &wy,
                         const string &emb, const string &suf, const string &cap) {
//  m_tagger.re_init_mats(wx, wh, wy, emb, suf, cap);

  m_wx.load(wx);
  m_wh.load(wh);
  m_wy.load(wy);
  m_emb.load(emb);
  m_suf.load(suf);
  m_cap.load(cap);
}


void
XF1Trainer::train(IO::ReaderFactory &reader, const int FMT, const size_t epoch) {

    reader.reset();
    vector<double> f1_vals;
    vector<int> valid_seq_ids;
    double total_valid = 0.0;
    int tagger_data_ind = -1;

    double gold = 0.0;
    double total = 0.0;
    double total_xf1 = 0.0;
    double total_trained_sents = 0.0;
    double beta = 0.0;

    // testing
    double total_f1 = 0.0;

    vector<size_t> train_data_inds;
    for (size_t i = 0; i < 39604; ++i) {
      train_data_inds.push_back(i);
    }

    if (epoch > 0) {
      std::mt19937 engine;
      std::shuffle(begin(train_data_inds), std::end(train_data_inds), engine);
    }

    size_t i = 0;
    for (size_t s = 0; s < 39604; ++s) {
    //for (size_t s = 0; s < train_data_inds.size(); ++s) {
    	i = train_data_inds[s];
      cerr << endl;
      cerr << "sent id: " << i + 1 << " out of 39604" <<  endl;

      if (epoch == 0) {
      	reader.next(m_sent);

      	if (m_sent.words.size() > 250) {
      	  cerr << "sent length > 250, skipping...\n";
      	  continue;
      	}

        // i+1 below, since i starts from 1 in m_skip_ids etc.
        if (m_skip_ids_map.find(i + 1) != m_skip_ids_map.end())
          continue;

      	tagger_data_ind += 1;
      	assert(m_sent.words.size() == m_all_kbest[tagger_data_ind][0].size());
      	if (m_sent.words.size() == 1 ||
      			m_ccg_gold_dep_count_map_train.find(i + 1)->second == 0)
      		continue;

      	f1_vals.clear();
      	valid_seq_ids.clear();

      	assert(m_sent.words.size() == m_all_kbest[tagger_data_ind][0].size());
      	assert(m_ind_length_tagger_ind_map.insert(make_pair(i, make_pair(m_sent.words.size(), tagger_data_ind))).second);

      	for (size_t j = 0; j < m_all_kbest[tagger_data_ind].size(); ++j) {
      		// for each sequence
      		size_t sent_size = m_all_kbest[tagger_data_ind][j].size();
      		assert(sent_size == m_sent.words.size());
      		m_sent.msuper.resize(sent_size);

      		for (size_t k = 0; k < sent_size; ++k) {
      			// for each word in this sequence
      			MultiRaw &mraw = m_sent.msuper[k];
      			mraw.resize(0);
      			mraw.push_back(ScoredRaw(m_all_kbest[tagger_data_ind][j][k].second, 1.0));
      			//cerr << " " << ScoredRaw(m_all_kbest[tagger_data_ind][j][k].second, 1.0).raw;
      		}

      		gold = 0.0;
      		total = 0.0;
      		const NLP::CCG::ShiftReduceHypothesis *output = m_parser.ShiftReduceParse(beta, false);

      		if(output) {
      			++total_valid;
      			cerr << "k: " << j << endl;
      			valid_seq_ids.push_back(j);
      			assert(output != 0);

      			size_t stackSize = output->GetStackSize();
      			std::vector<const NLP::CCG::SuperCat *> roots;
      			vector<const NLP::CCG::SuperCat *>::const_reverse_iterator rit;
      			roots.push_back(output->GetStackTopSuperCat());

      			for (size_t i = 0; i < stackSize - 1; ++i) {
      				output = output->GetPrvStack();
      				roots.push_back(output->GetStackTopSuperCat());
      			}

      			for (rit = roots.rbegin(); rit < roots.rend(); ++rit) {
      				get_deps(*rit, i + 1, gold, total, FMT);
      				//m_printer.parsed(*rit, m_sent, beta, 0);
      			}

      			// testing evaluation only
      			//m_printer.lexical(m_sent);
      			//m_printer.newline();

      			assert(gold <= total);
      			double f1 = calc_f1(gold, total, i + 1);
      			f1_vals.push_back(f1);

      			if (f1 == 0.0)
      				cerr << "sent: " << i << " seq: " << j << " f1 0.0\n";

      		} else {
      			cerr << "sent : " << i << " no parse found\n";
      			//exploded = true;
      			valid_seq_ids.push_back(-1);
      			f1_vals.push_back(0.0);
      			break;
      		}
      		m_parser.clear_xf1();
      	}

      	m_sent.reset();

      	assert(valid_seq_ids.size() == m_all_kbest[tagger_data_ind].size());
      	assert(f1_vals.size() == m_all_kbest[tagger_data_ind].size());
      	assert(f1_vals.size() == m_k);
      	//max_f1_vals += *max_element(f1_vals.begin(), f1_vals.end());
        assert(m_ind_f1vals_kbestlist_map.insert(make_pair(i, make_pair(f1_vals, m_all_kbest[tagger_data_ind]))).second);
      	//m_all_f1_vals.push_back(f1_vals);
      	//m_all_valid_seq_ids.push_back(valid_seq_ids);

      } else {
        if (m_ind_f1vals_kbestlist_map.find(i) == m_ind_f1vals_kbestlist_map.end())
          continue;
        assert(m_ind_length_tagger_ind_map.find(i) != m_ind_length_tagger_ind_map.end());
        assert(m_ind_length_tagger_ind_map[i].first == m_ind_f1vals_kbestlist_map[i].second[0].size());
        tagger_data_ind = m_ind_length_tagger_ind_map[i].second;
      }

      total_trained_sents += 1;
    	//total_f1 += *max_element(f1_vals.begin(), f1_vals.end());

      assert(m_all_tagger_data_contextwins[tagger_data_ind].size() ==
             m_ind_f1vals_kbestlist_map[i].second[0].size());

      vector<vector<vector<vector<int> > > > sent_contextwin_minibatches;
      contextwin2minibatch(m_all_tagger_data_contextwins[tagger_data_ind],
                           sent_contextwin_minibatches);

      total_xf1 += bptt_multi_multi(m_ind_f1vals_kbestlist_map[i].first,
      		                          sent_contextwin_minibatches, tagger_data_ind,
                                    m_all_kbest[tagger_data_ind][0].size());
//      bptt_multi(m_all_f1_vals[actual_sent_id],
//                 sent_contextwin_minibatches, tagger_data_ind,
//                 m_all_kbest[tagger_data_ind][0].size(),
//                 m_all_valid_seq_ids[actual_sent_id]);

    }

    cerr << "total sents used for training: " << total_trained_sents << endl;
    cerr << "total valid sequences: " << total_valid << endl;
    cerr << "avg xf1 per epoch: " << total_xf1 / total_trained_sents << endl;

//  cerr << "total: " << max_f1_vals << endl;
//  cerr << "total valid: " << total_valid << endl;
//
//  cerr << "total correct: " << total_correct << endl;
//  cerr << "total returned: " << total_returned << endl;
//  cerr << "total gold: " << total_gold << endl;
//
//  double p = total_correct/total_returned;
//  double r = total_correct/total_gold;
//  cerr << "total f1: " << (2.0*p*r)/(p + r) << endl;

}


void
XF1Trainer::train_rescore(IO::ReaderFactory &reader, const int FMT, const size_t epoch) {

    reader.reset();
    vector<double> f1_vals;
    vector<int> valid_seq_ids;
    double total_valid = 0.0;
    int tagger_data_ind = -1;
    int actual_sent_id = -1;

    double gold = 0.0;
    double total = 0.0;
    double total_xf1 = 0.0;
    double total_trained_sents = 0.0;
    double beta = 0.0;


    // testing
    double total_f1 = 0.0;
    double total_f1_rescored = 0.0;

    vector<size_t> train_data_inds;
    for (size_t i = 0; i < 39604; ++i) {
      train_data_inds.push_back(i);
    }

//    if (epoch > 0) {
//      std::mt19937 engine;
//      std::shuffle(begin(train_data_inds), std::end(train_data_inds), engine);
//    }

    size_t i = 0;
    for (size_t s = 0; s < 3; ++s) {
    //for (size_t s = 0; s < train_data_inds.size(); ++s) {
      i = train_data_inds[s];
      cerr << endl;
      cerr << "sent id: " << s << " out of 39604" <<  endl;

      //if (epoch == 0) {
        reader.next(m_sent);
        // i+1 below, since i starts from 1 in m_skip_ids etc.
        if (m_skip_ids_map.find(i + 1) != m_skip_ids_map.end())
          continue;

        tagger_data_ind += 1;
        assert(m_sent.words.size() == m_all_kbest[tagger_data_ind][0].size());
        if (m_sent.words.size() == 1 ||
            m_ccg_gold_dep_count_map_train.find(i + 1)->second == 0)
          continue;

        f1_vals.clear();
        valid_seq_ids.clear();

        assert(m_sent.words.size() == m_all_kbest[tagger_data_ind][0].size());
        for (size_t i = 0; i < m_sent.words.size(); ++i) {
          cerr << m_sent.words[i] << " ";
        }
        cerr << endl;

        for (size_t j = 0; j < m_all_kbest[tagger_data_ind].size(); ++j) {
          // for each sequence
          size_t sent_size = m_all_kbest[tagger_data_ind][j].size();
          assert(sent_size == m_sent.words.size());
          m_sent.msuper.resize(sent_size);

          for (size_t k = 0; k < sent_size; ++k) {
            // for each word in this sequence
            MultiRaw &mraw = m_sent.msuper[k];
            mraw.resize(0);
            mraw.push_back(ScoredRaw(m_all_kbest[tagger_data_ind][j][k].second, 1.0));
            cerr << " " << ScoredRaw(m_all_kbest[tagger_data_ind][j][k].second, 1.0).raw;
          }

          gold = 0.0;
          total = 0.0;
          const NLP::CCG::ShiftReduceHypothesis *output = m_parser.ShiftReduceParse(beta, false);

          if(output) {
            ++total_valid;
            cerr << "k: " << j << endl;
            valid_seq_ids.push_back(j);
            assert(output != 0);

            size_t stackSize = output->GetStackSize();
            std::vector<const NLP::CCG::SuperCat *> roots;
            vector<const NLP::CCG::SuperCat *>::const_reverse_iterator rit;
            roots.push_back(output->GetStackTopSuperCat());

            for (size_t i = 0; i < stackSize - 1; ++i) {
              output = output->GetPrvStack();
              roots.push_back(output->GetStackTopSuperCat());
            }

            for (rit = roots.rbegin(); rit < roots.rend(); ++rit) {
              get_deps(*rit, i + 1, gold, total, FMT);
              //m_printer.parsed(*rit, m_sent, beta, 0);
            }

            // testing evaluation only
            //m_printer.lexical(m_sent);
            //m_printer.newline();

            assert(gold <= total);
            double f1 = calc_f1(gold, total, i + 1);
            f1_vals.push_back(f1);

            if (f1 == 0.0)
              cerr << "sent: " << i << " seq: " << j << " f1 0.0\n";

          } else {
            cerr << "sent : " << i << " no parse found\n";
            //exploded = true;
            valid_seq_ids.push_back(-1);
            f1_vals.push_back(0.0);
            break;
          }
          m_parser.clear_xf1();
        }

        m_sent.reset();

        assert(valid_seq_ids.size() == m_all_kbest[tagger_data_ind].size());
        assert(f1_vals.size() == m_all_kbest[tagger_data_ind].size());
        assert(f1_vals.size() == m_k);
        //max_f1_vals += *max_element(f1_vals.begin(), f1_vals.end());
        m_all_f1_vals.push_back(f1_vals);
        m_all_valid_seq_ids.push_back(valid_seq_ids);

      //}

//      else {
//        unordered_map<size_t, pair<size_t, size_t>>::const_iterator itr = m_ind_map.find(i);
//        if (itr == m_ind_map.end()) {
//          continue;
//        }
//        assert(itr != m_ind_map.end());
//        tagger_data_ind = itr->second.first;
//        actual_sent_id = itr->second.second;
//        f1_vals = m_all_f1_vals[actual_sent_id];
//      }

      total_trained_sents += 1;

//      assert(m_all_valid_seq_ids[actual_sent_id].size() ==
//             m_all_kbest[tagger_data_ind].size());
//      assert(m_all_f1_vals[actual_sent_id].size() ==
//          m_all_kbest[tagger_data_ind].size());
//      assert(m_all_f1_vals[actual_sent_id].size() == m_k);

      total_f1 += f1_vals[0];

      vector<mat> x_vals;
      vector<mat> lh_vals;
      vector<mat> lh_deriv_vals;

      vector<vector<vector<vector<int> > > > sent_contextwin_minibatches;
      contextwin2minibatch(m_all_tagger_data_contextwins[tagger_data_ind],
                           sent_contextwin_minibatches);

      mat all_tag_scores(m_k, m_all_kbest[tagger_data_ind][0].size(), fill::zeros);
      m_h_tm1.zeros();
      lh_vals.push_back(m_h_tm1);

      // rescore all the tags in a kbest list (which
      // is equivalent to mtag this sentence once), and
      // memorize vals needed for bptt
      for (size_t i = 0; i < sent_contextwin_minibatches.size(); ++i) {
        // for a minibatch
        for (size_t j = 0; j < sent_contextwin_minibatches[i].size(); ++j) {
          // for a contextwin in a minibatch
          calc_lh_proj_emb(sent_contextwin_minibatches[i][j], x_vals, lh_vals, lh_deriv_vals);
          mat ly_vals = sigmoid_no_flow2(lh_vals.back()*m_wy);

          //ly_vals.print("ly_vals");

          // set the scores for all tags at this sentence position in this kbest list
          size_t ind_in_sent = i*m_bs_xf1 + j;
          for (size_t k = 0; k < m_k; ++k) {
            assert(m_all_kbest[tagger_data_ind][k].size() == m_all_kbest[tagger_data_ind][0].size());
            all_tag_scores(k, ind_in_sent) =
                ly_vals(m_all_kbest[tagger_data_ind][k][ind_in_sent].first);
          }
        }
      }

       assert(x_vals.size() == lh_vals.size() - 1);
       assert(lh_vals.size() == lh_deriv_vals.size() + 1);
       assert(lh_vals.size() == m_all_kbest[tagger_data_ind][0].size() + 1);

       colvec all_seq_scores = sum(log(all_tag_scores), 1);
       all_seq_scores.print("all_seq_scores");
       colvec all_seq_norm_scores = soft_max_new(all_seq_scores);
       all_seq_norm_scores.print(cerr, "all_seq_norm_scores");
       assert(all_seq_scores.n_rows == f1_vals.size());
       uword max_ind;
       all_seq_scores.max(max_ind);
       double rescored_max_f1 = f1_vals[max_ind];
       total_f1_rescored += rescored_max_f1;

       for (size_t i = 0; i < m_sent.words.size(); ++i) {
         cerr << m_sent.words[i] << " ";
       }
       cerr << endl;
       cerr << "sent id: " << s << " 1-best seq " << 0 << endl;
       cerr << "1-best f1: " << f1_vals[0] << endl;
       for (size_t k = 0; k < all_tag_scores.n_cols; ++k) {

         // for each word in this sequence
//         MultiRaw &mraw = m_sent.msuper[k];
//         mraw.resize(0);
//         mraw.push_back(ScoredRaw(m_all_kbest[tagger_data_ind][j][k].second, 1.0));
         cerr << " " << m_all_kbest[tagger_data_ind][0][k].second;
       }
       cerr << endl;


       cerr << "sent id: " << s << " best seq rescored " << max_ind << endl;
       cerr << "best seq rescored f1: " << f1_vals[max_ind] << endl;

       for (size_t k = 0; k < all_tag_scores.n_cols; ++k) {

         // for each word in this sequence
//         MultiRaw &mraw = m_sent.msuper[k];
//         mraw.resize(0);
//         mraw.push_back(ScoredRaw(m_all_kbest[tagger_data_ind][j][k].second, 1.0));
         cerr << " " << m_all_kbest[tagger_data_ind][max_ind][k].second;
       }
       cerr << endl;

//      total_xf1 += bptt_multi_multi(m_all_f1_vals[actual_sent_id],
//                                    sent_contextwin_minibatches, tagger_data_ind,
//                              m_all_kbest[tagger_data_ind][0].size(),
//                              m_all_valid_seq_ids[actual_sent_id]);
//      bptt_multi(m_all_f1_vals[actual_sent_id], sent_contextwin_minibatches, tagger_data_ind,
//                 m_all_kbest[tagger_data_ind][0].size(), m_all_valid_seq_ids[actual_sent_id]);

    }

    cerr << "total sents used for training: " << total_trained_sents << endl;
//    cerr << "total valid sequences: " << total_valid << endl;
//    cerr << "avg xf1 per epoch: " << total_xf1 / total_trained_sents << endl;

//  cerr << "total: " << max_f1_vals << endl;
//  cerr << "total valid: " << total_valid << endl;
    cerr << "0221 avg f1: " << total_f1/total_trained_sents << endl;
    cerr << "0221 avg f1 rescored: " << total_f1_rescored / total_trained_sents << endl;
//
//  cerr << "total correct: " << total_correct << endl;
//  cerr << "total returned: " << total_returned << endl;
//  cerr << "total gold: " << total_gold << endl;
//
//  double p = total_correct/total_returned;
//  double r = total_correct/total_gold;
//  cerr << "total f1: " << (2.0*p*r)/(p + r) << endl;

}


void
XF1Trainer::calc_lh_proj_emb_grad_check(const vector<vector<int> > &context_win,
                                        vector<mat> &lh_vals,
                                        const mat &wx) {
  colvec x;
  colvec x0;
  colvec x1;
  colvec x2;

  for (size_t i = 0; i < m_cs; ++i) {
    x0 = join_cols(x0, m_emb.col(context_win[i][0]));
    x1 = join_cols(x1, m_suf.col(context_win[i][1]));
    x2 = join_cols(x2, m_cap.col(context_win[i][2]));
  }

  x = join_cols(join_cols(x0, x1), x2);
  //x_vals.push_back(x);

  mat lh = sigmoid_no_flow2(x.t()*wx + lh_vals.back()*m_wh);
  //mat lh = arma::tanh(x.t()*wx + lh_vals.back()*m_wh);

  //mat one = ones<mat>(lh.n_rows, lh.n_cols);
  //mat lh_deriv = lh%(one - lh);

  lh_vals.push_back(lh);
}


void
XF1Trainer::calc_lh_proj_emb_grad_check_cap(const vector<vector<int> > &context_win,
                                            vector<mat> &lh_vals,
                                            const mat &cap) {
  colvec x;
  colvec x0;
  colvec x1;
  colvec x2;

  for (size_t i = 0; i < m_cs; ++i) {
    x0 = join_cols(x0, m_emb.col(context_win[i][0]));
    x1 = join_cols(x1, m_suf.col(context_win[i][1]));
    x2 = join_cols(x2, cap.col(context_win[i][2]));
  }

  x = join_cols(join_cols(x0, x1), x2);
  //x_vals.push_back(x);

  mat lh = sigmoid_no_flow2(x.t()*m_wx + lh_vals.back()*m_wh);
  //mat one = ones<mat>(lh.n_rows, lh.n_cols);
  //mat lh_deriv = lh%(one - lh);

  lh_vals.push_back(lh);
}


double
XF1Trainer::grad_check_cap(const vector<double> &f1_vals,
                       const vector<vector<vector<vector<int> > > > &contextwin_minibatches,
                       size_t sent_id, size_t sent_len) {

  const double eps = m_eps;
  vector<mat> lh_vals_plus;
  vector<mat> lh_vals_minus;
  const size_t kbest_size = f1_vals.size();
  assert(kbest_size == m_all_kbest[sent_id].size());

  mat all_tag_scores_plus(kbest_size, sent_len);
  mat all_tag_scores_minus(kbest_size, sent_len);

  mat h_tm1_plus(m_h_tm1.n_rows, m_h_tm1.n_cols, fill::zeros);
  mat h_tm1_minus(m_h_tm1.n_rows, m_h_tm1.n_cols, fill::zeros);

  lh_vals_plus.push_back(h_tm1_plus);
  lh_vals_minus.push_back(h_tm1_minus);

  mat cap_plus = m_cap;
  mat cap_minus = m_cap;
  //mat emb_grad(m_wy.n_rows, m_wy.n_cols, fill::zeros);

  cap_plus(0, 0) += eps;
  cap_minus(0, 0) -= eps;

  for (size_t i = 0; i < contextwin_minibatches.size(); ++i) {
    // for a minibatch
    for (size_t j = 0; j < contextwin_minibatches[i].size(); ++j) {
      // for a contextwin in a minibatch
      calc_lh_proj_emb_grad_check_cap(contextwin_minibatches[i][j], lh_vals_plus, cap_plus);
      mat ly_vals_plus = sigmoid_no_flow2(lh_vals_plus.back()*m_wy);
      //cerr << "ly\n";
      //ly_vals.print();

      // set the scores for all tags at this sentence position in this kbest list
      size_t ind_in_sent = i*m_bs_xf1 + j;
      for (size_t k = 0; k < kbest_size; ++k) {
        all_tag_scores_plus(k, ind_in_sent) =
            ly_vals_plus(m_all_kbest[sent_id][k][ind_in_sent].first);
      }

      h_tm1_plus = lh_vals_plus.back();

      calc_lh_proj_emb_grad_check_cap(contextwin_minibatches[i][j], lh_vals_minus, cap_minus);
      mat ly_vals_minus = sigmoid_no_flow2(lh_vals_minus.back()*m_wy);
      //cerr << "ly\n";
      //ly_vals.print();

      // set the scores for all tags at this sentence position in this kbest list
      for (size_t k = 0; k < kbest_size; ++k) {
        all_tag_scores_minus(k, ind_in_sent) =
            ly_vals_minus(m_all_kbest[sent_id][k][ind_in_sent].first);
      }

      h_tm1_minus = lh_vals_minus.back();

    }
  }

  colvec all_seq_norm_scores_plus = soft_max_new(sum(log(all_tag_scores_plus), 1));
  colvec f1_vals_vec(f1_vals);
  double xf1_plus = -sum(all_seq_norm_scores_plus % f1_vals_vec);
  cerr << "xf1_plus: " << xf1_plus << endl;

  colvec all_seq_norm_scores_minus = soft_max_new(sum(log(all_tag_scores_minus), 1));
  double xf1_minus = -sum(all_seq_norm_scores_minus % f1_vals_vec);
  cerr << "xf1_minus: " << xf1_minus << endl;

  double grad = (xf1_plus - xf1_minus)/(2.0*eps);

  cerr << "numerical grad cap: " << grad << endl;
  return grad;
}


void
XF1Trainer::calc_lh_proj_emb_grad_check_emb(const vector<vector<int> > &context_win,
                                            vector<mat> &lh_vals,
                                            const mat &emb) {
  colvec x;
  colvec x0;
  colvec x1;
  colvec x2;

  for (size_t i = 0; i < m_cs; ++i) {
    x0 = join_cols(x0, emb.col(context_win[i][0]));
    x1 = join_cols(x1, m_suf.col(context_win[i][1]));
    x2 = join_cols(x2, m_cap.col(context_win[i][2]));
  }

  x = join_cols(join_cols(x0, x1), x2);
  //x_vals.push_back(x);

  mat lh = sigmoid_no_flow2(x.t()*m_wx + lh_vals.back()*m_wh);
  //mat one = ones<mat>(lh.n_rows, lh.n_cols);
  //mat lh_deriv = lh%(one - lh);

  lh_vals.push_back(lh);
}


double
XF1Trainer::grad_check_emb(const vector<double> &f1_vals,
                       const vector<vector<vector<vector<int> > > > &contextwin_minibatches,
                       size_t sent_id, size_t sent_len) {

  const double eps = m_eps;
  vector<mat> lh_vals_plus;
  vector<mat> lh_vals_minus;
  const size_t kbest_size = f1_vals.size();
  assert(kbest_size == m_all_kbest[sent_id].size());

  mat all_tag_scores_plus(kbest_size, sent_len);
  mat all_tag_scores_minus(kbest_size, sent_len);

  mat h_tm1_plus(m_h_tm1.n_rows, m_h_tm1.n_cols, fill::zeros);
  mat h_tm1_minus(m_h_tm1.n_rows, m_h_tm1.n_cols, fill::zeros);

  lh_vals_plus.push_back(h_tm1_plus);
  lh_vals_minus.push_back(h_tm1_minus);

  mat emb_plus(m_emb);
  mat emb_minus(m_emb);
  //mat emb_grad(m_wy.n_rows, m_wy.n_cols, fill::zeros);

  emb_plus(0, 23087) += eps;
  emb_minus(0, 23087) -= eps;

  for (size_t i = 0; i < contextwin_minibatches.size(); ++i) {
    // for a minibatch
    for (size_t j = 0; j < contextwin_minibatches[i].size(); ++j) {
      // for a contextwin in a minibatch
      calc_lh_proj_emb_grad_check_emb(contextwin_minibatches[i][j], lh_vals_plus, emb_plus);
      mat ly_vals_plus = sigmoid_no_flow2(lh_vals_plus.back()*m_wy);
      //cerr << "ly\n";
      //ly_vals.print();

      // set the scores for all tags at this sentence position in this kbest list
      size_t ind_in_sent = i*m_bs_xf1 + j;
      for (size_t k = 0; k < kbest_size; ++k) {
        all_tag_scores_plus(k, ind_in_sent) =
            ly_vals_plus(m_all_kbest[sent_id][k][ind_in_sent].first);
      }

      h_tm1_plus = lh_vals_plus.back();

      calc_lh_proj_emb_grad_check_emb(contextwin_minibatches[i][j], lh_vals_minus, emb_minus);
      mat ly_vals_minus = sigmoid_no_flow2(lh_vals_minus.back()*m_wy);
      //cerr << "ly\n";
      //ly_vals.print();

      // set the scores for all tags at this sentence position in this kbest list
      for (size_t k = 0; k < kbest_size; ++k) {
        all_tag_scores_minus(k, ind_in_sent) =
            ly_vals_minus(m_all_kbest[sent_id][k][ind_in_sent].first);
      }

      h_tm1_minus = lh_vals_minus.back();

    }
  }

  colvec all_seq_norm_scores_plus = soft_max_new(sum(log(all_tag_scores_plus), 1));
  colvec f1_vals_vec(f1_vals);
  double xf1_plus = -sum(all_seq_norm_scores_plus % f1_vals_vec);
  cerr << "xf1_plus: " << xf1_plus << endl;

  colvec all_seq_norm_scores_minus = soft_max_new(sum(log(all_tag_scores_minus), 1));
  double xf1_minus = -sum(all_seq_norm_scores_minus % f1_vals_vec);
  cerr << "xf1_minus: " << xf1_minus << endl;

  double grad = (xf1_plus - xf1_minus)/(2.0*eps);

  cerr << "numerical grad emb: " << grad << endl;
  return grad;
}



mat
XF1Trainer::grad_check(const vector<double> &f1_vals,
                       const vector<vector<vector<vector<int> > > > &contextwin_minibatches,
                       size_t sent_id, size_t sent_len) {

  const double eps = m_eps;
  vector<mat> lh_vals_plus;
  vector<mat> lh_vals_minus;
  const size_t kbest_size = f1_vals.size();
  assert(kbest_size == m_all_kbest[sent_id].size());

  mat all_tag_scores_plus(kbest_size, sent_len);
  mat all_tag_scores_minus(kbest_size, sent_len);

  mat h_tm1_plus(m_h_tm1.n_rows, m_h_tm1.n_cols, fill::zeros);
  mat h_tm1_minus(m_h_tm1.n_rows, m_h_tm1.n_cols, fill::zeros);

  lh_vals_plus.push_back(h_tm1_plus);
  lh_vals_minus.push_back(h_tm1_minus);

  mat wx_plus = m_wx;
  mat wx_minus = m_wx;
  mat wx_grad(m_wy.n_rows, m_wy.n_cols, fill::zeros);

  wx_plus(0, 0) += eps;
  wx_minus(0, 0) -= eps;

  for (size_t i = 0; i < contextwin_minibatches.size(); ++i) {
    // for a minibatch
    for (size_t j = 0; j < contextwin_minibatches[i].size(); ++j) {
      // for a contextwin in a minibatch
      calc_lh_proj_emb_grad_check(contextwin_minibatches[i][j], lh_vals_plus, wx_plus);
      mat ly_vals_plus = sigmoid_no_flow2((lh_vals_plus.back()*m_wy));
      //cerr << "ly\n";
      //ly_vals.print();

      // set the scores for all tags at this sentence position in this kbest list
      size_t ind_in_sent = i*m_bs_xf1 + j;
      for (size_t k = 0; k < kbest_size; ++k) {
        all_tag_scores_plus(k, ind_in_sent) =
            ly_vals_plus(m_all_kbest[sent_id][k][ind_in_sent].first);
      }

      h_tm1_plus = lh_vals_plus.back();

      calc_lh_proj_emb_grad_check(contextwin_minibatches[i][j], lh_vals_minus, wx_minus);
      mat ly_vals_minus = sigmoid_no_flow2((lh_vals_minus.back()*m_wy));
      //cerr << "ly\n";
      //ly_vals.print();

      // set the scores for all tags at this sentence position in this kbest list
      for (size_t k = 0; k < m_all_kbest[sent_id].size(); ++k) {
        all_tag_scores_minus(k, ind_in_sent) =
            ly_vals_minus(m_all_kbest[sent_id][k][ind_in_sent].first);
      }

      h_tm1_minus = lh_vals_minus.back();

    }
  }

  colvec all_seq_norm_scores_plus = soft_max_new(sum(log(all_tag_scores_plus), 1));
  colvec f1_vals_vec(f1_vals);
  double xf1_plus = -sum(all_seq_norm_scores_plus % f1_vals_vec);
  cerr << "xf1_plus: " << xf1_plus << endl;

  colvec all_seq_norm_scores_minus = soft_max_new(sum(log(all_tag_scores_minus), 1));
  double xf1_minus = -sum(all_seq_norm_scores_minus % f1_vals_vec);
  cerr << "xf1_minus: " << xf1_minus << endl;

  wx_grad(0, 0) = (xf1_plus - xf1_minus)/(2.0*eps);

  cerr << "numerical grad wx: " << wx_grad(0, 0) << endl;
  return wx_grad;
}


void
XF1Trainer::calc_lh_proj_emb_grad_check_wy(const vector<vector<int> > &context_win,
                                        vector<mat> &lh_vals, const mat &h_tm1) {
  colvec x;
  colvec x0;
  colvec x1;
  colvec x2;

  for (size_t i = 0; i < m_cs; ++i) {
    x0 = join_cols(x0, m_emb.col(context_win[i][0]));
    x1 = join_cols(x1, m_suf.col(context_win[i][1]));
    x2 = join_cols(x2, m_cap.col(context_win[i][2]));
  }

  x = join_cols(join_cols(x0, x1), x2);
  //x_vals.push_back(x);

  mat lh = sigmoid_no_flow2(x.t()*m_wx + h_tm1*m_wh);
  //mat one = ones<mat>(lh.n_rows, lh.n_cols);
  //mat lh_deriv = lh%(one - lh);

  lh_vals.push_back(lh);
}


mat
XF1Trainer::grad_check_wy(const vector<double> &f1_vals,
                          const vector<vector<vector<vector<int> > > > &contextwin_minibatches,
                          size_t sent_id, size_t sent_len) {

  const double eps = m_eps;
  vector<mat> lh_vals_plus;
  vector<mat> lh_vals_minus;
  const size_t kbest_size = f1_vals.size();
  assert(kbest_size == m_all_kbest[sent_id].size());

  mat all_tag_scores_plus(kbest_size, sent_len);
  mat all_tag_scores_minus(kbest_size, sent_len);

  mat h_tm1_plus(m_h_tm1);
  mat h_tm1_minus(m_h_tm1);

  h_tm1_plus.zeros();
  h_tm1_minus.zeros();

  mat wy_plus(m_wy);
  mat wy_minus(m_wy);
  mat wy_grad(m_wy.n_rows, m_wy.n_cols, fill::zeros);

  wy_plus(0, 63) += eps;
  wy_minus(0, 63) -= eps;

  for (size_t i = 0; i < contextwin_minibatches.size(); ++i) {
    // for a minibatch
    assert(contextwin_minibatches[i].size() <= m_bs_xf1);
    for (size_t j = 0; j < contextwin_minibatches[i].size(); ++j) {
      // for a contextwin in a minibatch
      calc_lh_proj_emb_grad_check_wy(contextwin_minibatches[i][j], lh_vals_plus, h_tm1_plus);
      mat ly_vals_plus = sigmoid_no_flow2(lh_vals_plus.back()*wy_plus);

      // set the scores for all tags at this sentence position in this kbest list
      size_t ind_in_sent = i*m_bs_xf1 + j;
      for (size_t k = 0; k < m_all_kbest[sent_id].size(); ++k) {
        all_tag_scores_plus(k, ind_in_sent) =
            ly_vals_plus(m_all_kbest[sent_id][k][ind_in_sent].first);
      }

      h_tm1_plus = lh_vals_plus.back();

      calc_lh_proj_emb_grad_check_wy(contextwin_minibatches[i][j], lh_vals_minus, h_tm1_minus);
      mat ly_vals_minus = sigmoid_no_flow2(lh_vals_minus.back()*wy_minus);

      // set the scores for all tags at this sentence position in this kbest list
      for (size_t k = 0; k < m_all_kbest[sent_id].size(); ++k) {
        all_tag_scores_minus(k, ind_in_sent) =
            ly_vals_minus(m_all_kbest[sent_id][k][ind_in_sent].first);
      }

      h_tm1_minus = lh_vals_minus.back();

    }
  }

  colvec all_seq_norm_scores_plus = soft_max_new(sum(log(all_tag_scores_plus), 1));
  colvec f1_vals_vec(f1_vals);
  double xf1_plus = -sum(all_seq_norm_scores_plus % f1_vals_vec);
  cerr << "xf1_plus: " << xf1_plus << endl;

  colvec all_seq_norm_scores_minus = soft_max_new(sum(log(all_tag_scores_minus), 1));
  double xf1_minus = -sum(all_seq_norm_scores_minus % f1_vals_vec);
  cerr << "xf1_minus: " << xf1_minus << endl;


  double grad_wy = (xf1_plus - xf1_minus)/(2.0*eps);
  cerr << "numerical grad wy: " << grad_wy << endl;
  return wy_grad;
}


void
XF1Trainer::calc_lh_proj_emb_grad_check_wh(const vector<vector<int> > &context_win,
                                        vector<mat> &lh_vals, const mat &h_tm1, const mat &wh) {
  colvec x;
  colvec x0;
  colvec x1;
  colvec x2;

  for (size_t i = 0; i < m_cs; ++i) {
    x0 = join_cols(x0, m_emb.col(context_win[i][0]));
    x1 = join_cols(x1, m_suf.col(context_win[i][1]));
    x2 = join_cols(x2, m_cap.col(context_win[i][2]));
  }

  x = join_cols(join_cols(x0, x1), x2);
  //x_vals.push_back(x);

  mat lh = sigmoid_no_flow2(x.t()*m_wx + h_tm1*wh);
  //mat one = ones<mat>(lh.n_rows, lh.n_cols);
  //mat lh_deriv = lh%(one - lh);

  lh_vals.push_back(lh);
}


mat
XF1Trainer::grad_check_wh(const vector<double> &f1_vals,
                       const vector<vector<vector<vector<int> > > > &contextwin_minibatches,
                       size_t sent_id, size_t sent_len) {

  const double eps = m_eps;
  vector<mat> lh_vals_plus;
  vector<mat> lh_vals_minus;
  const size_t kbest_size = f1_vals.size();
  assert(kbest_size == m_all_kbest[sent_id].size());

  mat all_tag_scores_plus(kbest_size, sent_len);
  mat all_tag_scores_minus(kbest_size, sent_len);

  mat h_tm1_plus = m_h_tm1;
  mat h_tm1_minus = m_h_tm1;

  h_tm1_plus.zeros();
  h_tm1_minus.zeros();

  mat wh_plus = m_wh;
  mat wh_minus = m_wh;
  mat wh_grad(m_wy.n_rows, m_wy.n_cols, fill::zeros);

  wh_plus(0, 0) += eps;
  wh_minus(0, 0) -= eps;

  for (size_t i = 0; i < contextwin_minibatches.size(); ++i) {
    // for a minibatch
    for (size_t j = 0; j < contextwin_minibatches[i].size(); ++j) {
      // for a contextwin in a minibatch
      calc_lh_proj_emb_grad_check_wh(contextwin_minibatches[i][j], lh_vals_plus, h_tm1_plus, wh_plus);
      mat ly_vals_plus = sigmoid_no_flow2(lh_vals_plus.back()*m_wy);
      //cerr << "ly\n";
      //ly_vals.print();

      // set the scores for all tags at this sentence position in this kbest list
      size_t ind_in_sent = i*m_bs_xf1 + j;
      for (size_t k = 0; k < m_all_kbest[sent_id].size(); ++k) {
        all_tag_scores_plus(k, ind_in_sent) =
            ly_vals_plus(m_all_kbest[sent_id][k][ind_in_sent].first);
      }

      h_tm1_plus = lh_vals_plus.back();

      calc_lh_proj_emb_grad_check_wh(contextwin_minibatches[i][j], lh_vals_minus, h_tm1_minus, wh_minus);
      mat ly_vals_minus = sigmoid_no_flow2(lh_vals_minus.back()*m_wy);
      //cerr << "ly\n";
      //ly_vals.print();

      // set the scores for all tags at this sentence position in this kbest list
      for (size_t k = 0; k < m_all_kbest[sent_id].size(); ++k) {
        all_tag_scores_minus(k, ind_in_sent) =
            ly_vals_minus(m_all_kbest[sent_id][k][ind_in_sent].first);
      }

      h_tm1_minus = lh_vals_minus.back();

    }
  }

  colvec all_seq_norm_scores_plus = soft_max_new(sum(log(all_tag_scores_plus), 1));
  colvec f1_vals_vec(f1_vals);
  double xf1_plus = -sum(all_seq_norm_scores_plus % f1_vals_vec);
  cerr << "xf1_plus: " << xf1_plus << endl;

  colvec all_seq_norm_scores_minus = soft_max_new(sum(log(all_tag_scores_minus), 1));
  double xf1_minus = -sum(all_seq_norm_scores_minus % f1_vals_vec);
  cerr << "xf1_minus: " << xf1_minus << endl;

  double grad_wh = (xf1_plus -xf1_minus)/(2*eps);
  cerr << "numerical grad wh: " << grad_wh << endl;

  return wh_grad;
}



void
XF1Trainer::calc_lh_proj_emb(const vector<vector<int> > &context_win,
		                         vector<mat> &lh_vals) {
  colvec x;
  colvec x0;
  colvec x1;
  colvec x2;

  for (size_t i = 0; i < m_cs; ++i) {
    x0 = join_cols(x0, m_emb.col(context_win[i][0]));
    x1 = join_cols(x1, m_suf.col(context_win[i][1]));
    x2 = join_cols(x2, m_cap.col(context_win[i][2]));
  }

  x = join_cols(join_cols(x0, x1), x2);
  mat lh = sigmoid_no_flow2(x.t()*m_wx + lh_vals.back()*m_wh);
  lh_vals.push_back(lh);
}


void
XF1Trainer::calc_lh_proj_emb(const vector<vector<int> > &context_win,
                             vector<mat> &x_vals,
                             vector<mat> &lh_vals,
                             vector<mat> &lh_deriv_vals) {
  colvec x;
  colvec x0;
  colvec x1;
  colvec x2;

  for (size_t i = 0; i < m_cs; ++i) {
    x0 = join_cols(x0, m_emb.col(context_win[i][0]));
    x1 = join_cols(x1, m_suf.col(context_win[i][1]));
    x2 = join_cols(x2, m_cap.col(context_win[i][2]));
  }

  x = join_cols(join_cols(x0, x1), x2);
  x_vals.push_back(x);

  mat lh = sigmoid_no_flow2(x.t()*m_wx + lh_vals.back()*m_wh);
  lh.print("lh");
  mat one = ones<mat>(lh.n_rows, lh.n_cols);
  mat lh_deriv = lh%(one - lh);

  lh_vals.push_back(lh);
  lh_deriv_vals.push_back(lh_deriv);
}


void
XF1Trainer::calc_lh_proj_emb_tanh(const vector<vector<int> > &context_win,
                             vector<mat> &x_vals,
                             vector<mat> &lh_vals,
                             vector<mat> &lh_deriv_vals) {
  colvec x;
  colvec x0;
  colvec x1;
  colvec x2;

  for (size_t i = 0; i < m_cs; ++i) {
    x0 = join_cols(x0, m_emb.col(context_win[i][0]));
    x1 = join_cols(x1, m_suf.col(context_win[i][1]));
    x2 = join_cols(x2, m_cap.col(context_win[i][2]));
  }

  x = join_cols(join_cols(x0, x1), x2);
  x_vals.push_back(x);

  mat lh = arma::tanh(x.t()*m_wx + lh_vals.back()*m_wh);
  lh.print("lh");
  mat one = ones<mat>(lh.n_rows, lh.n_cols);
  mat lh_deriv = one / arma::square(arma::cosh(lh));

  lh_vals.push_back(lh);
  lh_deriv_vals.push_back(lh_deriv);
}


// vector<int>, a word
// vector<vector<int>, a context_win
// vector<vector<vector<int> >, a context_win minibatch

double
XF1Trainer::bptt_multi(const vector<double> &f1_vals,
                       const vector<vector<vector<vector<int> > > > &contextwin_minibatches,
                       size_t sent_id, size_t sent_len,
                       const vector<int> &valid_seq_ids) {

  cerr << "bptt_multi() ...\n";

  //cerr.precision(15);
  // grad checking, for testing only
  //mat wx_grad_approx1 = grad_check1(f1_vals, contextwin_minibatches, sent_id, sent_len, valid_seq_ids);
  mat wx_grad_approx = grad_check(f1_vals, contextwin_minibatches, sent_id, sent_len);
  mat wy_grad_approx = grad_check_wy(f1_vals, contextwin_minibatches, sent_id, sent_len);
  mat wh_grad_approx = grad_check_wh(f1_vals, contextwin_minibatches, sent_id, sent_len);
  double emb_grad_approx = grad_check_emb(f1_vals, contextwin_minibatches, sent_id, sent_len);
  double cap_grad_approx = grad_check_cap(f1_vals, contextwin_minibatches, sent_id, sent_len);

  const size_t kbest_size = f1_vals.size();
  assert(kbest_size == m_all_kbest[sent_id].size());

  vector<mat> x_vals;
  vector<mat> lh_vals;
  vector<mat> lh_deriv_vals;

  vector<mat> delta_h_vals(sent_len, mat(m_h_tm1.n_cols, 1, fill::zeros));
  vector<mat> delta_y_vals(sent_len, mat(m_wy.n_cols, 1, fill::zeros));

  mat all_tag_scores(kbest_size, sent_len, fill::zeros);
  m_h_tm1.zeros();
  lh_vals.push_back(m_h_tm1);

  // rescore all the tags in a kbest list (which
  // is equiv. to mtag this sentence once), and
  // memorize vals needed for bptt
  for (size_t i = 0; i < contextwin_minibatches.size(); ++i) {
    // for a minibatch
    for (size_t j = 0; j < contextwin_minibatches[i].size(); ++j) {
      // for a contextwin in a minibatch
      calc_lh_proj_emb(contextwin_minibatches[i][j], x_vals, lh_vals, lh_deriv_vals);
      mat ly_vals = sigmoid_no_flow2(lh_vals.back()*m_wy);

      // set the scores for all tags at this sentence position in this kbest list
      size_t ind_in_sent = i*m_bs_xf1 + j;
      for (size_t k = 0; k < kbest_size; ++k) {
        all_tag_scores(k, ind_in_sent) =
            ly_vals(m_all_kbest[sent_id][k][ind_in_sent].first);
      }
    }
  }

  assert(x_vals.size() == lh_vals.size() - 1);
  assert(lh_vals.size() == lh_deriv_vals.size() + 1);
  assert(lh_vals.size() == sent_len + 1);

  mat all_ones = ones<mat>(all_tag_scores.n_rows, all_tag_scores.n_cols);
  mat all_delta_y_vals = all_ones - all_tag_scores;

  colvec all_seq_norm_scores = soft_max_new(sum(log(all_tag_scores), 1));
  //all_seq_norm_scores.print(cerr);
  colvec f1_vals_vec(f1_vals);

  double xf1 = sum(all_seq_norm_scores % f1_vals_vec);
  assert(xf1 <= 1.0);
  assert(xf1 >= 0.0);
  cerr << "xf1: " << xf1 << endl;


  colvec xf1_vec(kbest_size);
  xf1_vec.fill(xf1);

  assert(all_seq_norm_scores.n_rows == f1_vals_vec.n_rows);
  assert(all_seq_norm_scores.n_cols == f1_vals_vec.n_cols);

//  cerr << "norm\n";
//  all_seq_norm_scores.print(cerr);
//  cerr << "f1\n";
//  f1_vals_vec.print(cerr);
//  cerr << "xf1\n";
//  xf1_vec.print(cerr);

  colvec err_term = -(all_seq_norm_scores % (f1_vals_vec - xf1_vec));
  assert(err_term.n_rows == all_delta_y_vals.n_rows);
  for (size_t i = 0; i < err_term.n_rows; ++i) {
    all_delta_y_vals.row(i) *= err_term(i);
  }
  //cerr << "err_term\n";
  //err_term.print(cerr);

  // actual xf1 bptt, first backprop gradients, no weight updates
  assert(sent_len - 1 >= 0);
  for (int ind_in_sent = sent_len - 1; ind_in_sent >= 0; --ind_in_sent) {
      assert(ind_in_sent < sent_len);

      for (size_t k = 0; k < kbest_size; ++k) {
        if (m_ignore_0_f1_seq && valid_seq_ids[k] == -1)
          continue;
        size_t tag_class_ind = m_all_kbest[sent_id][k][ind_in_sent].first;
        //cerr << "tag_class " << tag_class_ind << endl;
        //cerr << m_wy.col(tag_class_ind).n_rows << " " << delta_h_vals[ind_in_sent].n_rows << endl;
        assert(m_wy.col(tag_class_ind).n_rows == delta_h_vals[ind_in_sent].n_rows);
        assert(m_wy.col(tag_class_ind).n_cols == delta_h_vals[ind_in_sent].n_cols);

        //all_delta_y_vals.print();

        delta_y_vals[ind_in_sent].row(tag_class_ind) += all_delta_y_vals(k, ind_in_sent);
      }

      if (ind_in_sent == sent_len - 1) {
        delta_h_vals[ind_in_sent] = lh_deriv_vals[ind_in_sent].t() %
                                    (m_wy*delta_y_vals[ind_in_sent]);
      } else {

        delta_h_vals[ind_in_sent] = lh_deriv_vals[ind_in_sent].t() %
                                    ((m_wh*delta_h_vals[ind_in_sent + 1]) + (m_wy*delta_y_vals[ind_in_sent]));
      }

      //if (ind_in_sent == 0)
        //delta_h_vals[0].print(cerr);
    }

  // grad checking
  double wx_00 = 0.0;
  double wy_063 = 0.0;
  double wh_00 = 0.0;
  double emb_0_23087 = 0.0;
  double cap_00 = 0.0;

  //mat m_wx_copy = m_wx;
  vector<mat> delta_x_vals(sent_len, zeros<mat>(m_wx.n_rows, 1));

  for (size_t ind_in_sent = 0; ind_in_sent < sent_len; ++ind_in_sent) {
    delta_x_vals[ind_in_sent] = m_wx*delta_h_vals[ind_in_sent];
  }

  // update weights
  for (size_t ind_in_sent = 0; ind_in_sent < sent_len; ++ind_in_sent) {
    // for a contextwin in a minibatch
    //size_t ind_in_sent = i*m_bs + j;
    //size_t wy_col_ind = m_all_kbest[sent_id][l][ind_in_sent].first;

    mat grad_wh = lh_vals[ind_in_sent].t()*delta_h_vals[ind_in_sent].t();
    m_wh -= m_lr*grad_wh;
    // grad checking
    wh_00 += grad_wh(0,0);


    mat grad_wy = lh_vals[ind_in_sent + 1].t()*delta_y_vals[ind_in_sent].t();
    m_wy -= m_lr*grad_wy;
    //grad_wy.print("grad_wy");
    // grad checking
    //cerr << "grad_wy_063: " << grad_wy(0,63) << endl;
    wy_063 += grad_wy(0,63);

    mat grad_wx = x_vals[ind_in_sent]*delta_h_vals[ind_in_sent].t();
    //cerr << "grad_wx: " << grad_wx.n_rows << " " << grad_wx.n_cols << endl;
    //cerr << "wx: " << m_wx.n_rows << " " << m_wx.n_cols << endl;

    cerr << "grad_wx_00: " << grad_wx(0,0) << endl;
    m_wx -= m_lr*grad_wx;

    // grad checking
    wx_00 += grad_wx(0,0);

    //cerr << grad_wx(0,0) << endl;
    //assert(wx_updated(0,0) == grad_wx(0,0));
  }

  for (size_t i = 0; i < contextwin_minibatches.size(); ++i) {
    for (size_t j = 0; j < contextwin_minibatches[i].size(); ++j) {
      size_t ind_in_sent = i*m_bs_xf1 + j;
      int start = 0;
      int start2 = m_de*m_cs;
      int start3 = start2 + m_ds*m_cs;
      for (size_t k = 0; k < m_cs; ++k) {
        m_emb.col(contextwin_minibatches[i][j][k][0]) -=
            m_lr*delta_x_vals[ind_in_sent].rows(start, start + m_de - 1);
        if (contextwin_minibatches[i][j][k][0] == 23087)
          emb_0_23087 += delta_x_vals[ind_in_sent](start);
        m_suf.col(contextwin_minibatches[i][j][k][1]) -=
            m_lr*delta_x_vals[ind_in_sent].rows(start2, start2 + m_ds - 1);
        m_cap.col(contextwin_minibatches[i][j][k][2]) -=
            m_lr*delta_x_vals[ind_in_sent].rows(start3, start3 + m_dc - 1);
        if (contextwin_minibatches[i][j][k][2] == 0)
          cap_00 += delta_x_vals[ind_in_sent](start3);
        start += m_de;
        start2 += m_ds;
        start3 += m_dc;
      }
    }
  }

  // grad checking
  cerr << "backprop grad wx: " << wx_00 << endl;
  cerr << "backprop grad wy: " << wy_063 << endl;
  cerr << "backprop grad wh: " << wh_00 << endl;
  cerr << "backprop grad emb: " << emb_0_23087 << endl;
  cerr << "backprop grad cap: " << cap_00 << endl;

  return xf1;
}


//double
//XF1Trainer::bptt_multi(const vector<double> &f1_vals,
//                       const vector<vector<vector<vector<int> > > > &contextwin_minibatches,
//                       size_t sent_id, size_t sent_len,
//                       const vector<int> &valid_seq_ids) {
//
//  cerr << "bptt_multi() ...\n";
//
//  //cerr.precision(15);
//  // grad checking, for testing only
//  //mat wx_grad_approx1 = grad_check1(f1_vals, contextwin_minibatches, sent_id, sent_len, valid_seq_ids);
//  mat wx_grad_approx = grad_check(f1_vals, contextwin_minibatches, sent_id, sent_len, valid_seq_ids);
//  mat wy_grad_approx = grad_check_wy(f1_vals, contextwin_minibatches, sent_id, sent_len, valid_seq_ids);
//  mat wh_grad_approx = grad_check_wh(f1_vals, contextwin_minibatches, sent_id, sent_len, valid_seq_ids);
//  double emb_grad_approx = grad_check_emb(f1_vals, contextwin_minibatches, sent_id, sent_len, valid_seq_ids);
//
//  const size_t kbest_size = f1_vals.size();
//  assert(kbest_size == m_all_kbest[sent_id].size());
//
//  vector<mat> x_vals;
//  vector<mat> lh_vals;
//  vector<mat> lh_deriv_vals;
//
//  vector<mat> delta_h_vals(sent_len, mat(m_h_tm1.n_cols, 1, fill::zeros));
//  vector<mat> delta_y_vals(sent_len, mat(m_wy.n_cols, 1, fill::zeros));
//
//  mat all_tag_scores(kbest_size, sent_len, fill::zeros);
//  m_h_tm1.zeros();
//  lh_vals.push_back(m_h_tm1);
//
//  // rescore all the tags in a kbest list (which
//  // is equiv. to mtag this sentence once), and
//  // memorize vals needed for bptt
//  for (size_t i = 0; i < contextwin_minibatches.size(); ++i) {
//    // for a minibatch
//    for (size_t j = 0; j < contextwin_minibatches[i].size(); ++j) {
//      // for a contextwin in a minibatch
//      calc_lh_proj_emb(contextwin_minibatches[i][j], x_vals, lh_vals, lh_deriv_vals);
//      mat ly_vals = sigmoid(lh_vals.back()*m_wy);
//
//      // set the scores for all tags at this sentence position in this kbest list
//      size_t ind_in_sent = i*m_bs + j;
//      for (size_t k = 0; k < kbest_size; ++k) {
//        all_tag_scores(k, ind_in_sent) =
//            ly_vals(m_all_kbest[sent_id][k][ind_in_sent].first);
//      }
//    }
//  }
//
//  assert(x_vals.size() == lh_vals.size() - 1);
//  assert(lh_vals.size() == lh_deriv_vals.size() + 1);
//  assert(lh_vals.size() == sent_len + 1);
//
//  mat all_ones = ones<mat>(all_tag_scores.n_rows, all_tag_scores.n_cols);
//  mat all_delta_y_vals = all_ones - all_tag_scores;
//
//  colvec all_seq_norm_scores = soft_max_new(sum(log(all_tag_scores), 1));
//  //all_seq_norm_scores.print(cerr);
//  colvec f1_vals_vec(f1_vals);
//
//  double xf1 = sum(all_seq_norm_scores % f1_vals_vec);
//  //assert(xf1 <= 1.0);
//  //assert(xf1 >= 0.0);
//  cerr << "xf1: " << xf1 << endl;
//
//
//  colvec xf1_vec(kbest_size);
//  xf1_vec.fill(xf1);
//
//  assert(all_seq_norm_scores.n_rows == f1_vals_vec.n_rows);
//  assert(all_seq_norm_scores.n_cols == f1_vals_vec.n_cols);
//
////  cerr << "norm\n";
////  all_seq_norm_scores.print(cerr);
////  cerr << "f1\n";
////  f1_vals_vec.print(cerr);
////  cerr << "xf1\n";
////  xf1_vec.print(cerr);
//
//  colvec err_term = -(all_seq_norm_scores % (f1_vals_vec - xf1_vec));
//  assert(err_term.n_rows == all_delta_y_vals.n_rows);
//  for (size_t i = 0; i < err_term.n_rows; ++i) {
//    all_delta_y_vals.row(i) *= err_term(i);
//  }
//  //cerr << "err_term\n";
//  //err_term.print(cerr);
//
//  // actual xf1 bptt, first backprop gradients, no weight updates
//  assert(sent_len - 1 >= 0);
//  for (int ind_in_sent = sent_len - 1; ind_in_sent >= 0; --ind_in_sent) {
//      assert(ind_in_sent < sent_len);
//
//      for (size_t k = 0; k < kbest_size; ++k) {
//        if (m_ignore_0_f1_seq && valid_seq_ids[k] == -1)
//          continue;
//        size_t tag_class_ind = m_all_kbest[sent_id][k][ind_in_sent].first;
//        //cerr << "tag_class " << tag_class_ind << endl;
//        //cerr << m_wy.col(tag_class_ind).n_rows << " " << delta_h_vals[ind_in_sent].n_rows << endl;
//        assert(m_wy.col(tag_class_ind).n_rows == delta_h_vals[ind_in_sent].n_rows);
//        assert(m_wy.col(tag_class_ind).n_cols == delta_h_vals[ind_in_sent].n_cols);
//
//        //all_delta_y_vals.print();
//
//        delta_y_vals[ind_in_sent].row(tag_class_ind) += all_delta_y_vals(k, ind_in_sent);
//      }
//
//      if (ind_in_sent == sent_len - 1) {
//        delta_h_vals[ind_in_sent] = lh_deriv_vals[ind_in_sent].t() %
//                                    (m_wy*delta_y_vals[ind_in_sent]);
//      } else {
//
//        delta_h_vals[ind_in_sent] = lh_deriv_vals[ind_in_sent].t() %
//                                    ((m_wh*delta_h_vals[ind_in_sent + 1]) + (m_wy*delta_y_vals[ind_in_sent]));
//      }
//
//      //if (ind_in_sent == 0)
//        //delta_h_vals[0].print(cerr);
//    }
//  //}
//
//  // grad checking
//  double wx_00 = 0.0;
//  double wy_00 = 0.0;
//  double wh_00 = 0.0;
//  double emb_0_8859 = 0.0;
//
//  //mat m_wx_copy = m_wx;
//  vector<mat> delta_x_vals(sent_len, zeros<mat>(m_wx.n_rows, 1));
//
//  for (size_t ind_in_sent = 0; ind_in_sent < sent_len; ++ind_in_sent) {
//    delta_x_vals[ind_in_sent] = m_wx*delta_h_vals[ind_in_sent];
//  }
//
//  // update weights
//  for (size_t ind_in_sent = 0; ind_in_sent < sent_len; ++ind_in_sent) {
//    // for a contextwin in a minibatch
//    //size_t ind_in_sent = i*m_bs + j;
//    //size_t wy_col_ind = m_all_kbest[sent_id][l][ind_in_sent].first;
//
//    mat grad_wh = lh_vals[ind_in_sent].t()*delta_h_vals[ind_in_sent].t();
//    m_wh -= m_lr*grad_wh;
//    // grad checking
//    wh_00 += grad_wh(0,0);
//
//
//    mat grad_wy = lh_vals[ind_in_sent + 1].t()*delta_y_vals[ind_in_sent].t();
//    m_wy -= m_lr*grad_wy;
//    //grad_wy.print("grad_wy");
//    // grad checking
//    cerr << "grad_wy_063: " << grad_wy(0,63) << endl;
//    wy_00 += grad_wy(0,63);
//
//    mat grad_wx = x_vals[ind_in_sent]*delta_h_vals[ind_in_sent].t();
//    //cerr << "grad_wx: " << grad_wx.n_rows << " " << grad_wx.n_cols << endl;
//    //cerr << "wx: " << m_wx.n_rows << " " << m_wx.n_cols << endl;
//
//    cerr << "grad_wx_00: " << grad_wx(0,0) << endl;
//    m_wx -= m_lr*grad_wx;
//
//    // grad checking
//    wx_00 += grad_wx(0,0);
//
//    //cerr << grad_wx(0,0) << endl;
//    //assert(wx_updated(0,0) == grad_wx(0,0));
//  }
//
//  for (size_t i = 0; i < contextwin_minibatches.size(); ++i) {
//    for (size_t j = 0; j < contextwin_minibatches[i].size(); ++j) {
//      size_t ind_in_sent = i*m_bs + j;
//      int start = 0;
//      int start2 = m_de*m_cs;
//      int start3 = start2 + m_ds*m_cs;
//      for (size_t k = 0; k < m_cs; ++k) {
//        m_emb.col(contextwin_minibatches[i][j][k][0]) -=
//            m_lr*delta_x_vals[ind_in_sent].rows(start, start + m_de - 1);
//        if (contextwin_minibatches[i][j][k][0] == 8859)
//          emb_0_8859 += delta_x_vals[ind_in_sent](0);
//        m_suf.col(contextwin_minibatches[i][j][k][1]) -=
//            m_lr*delta_x_vals[ind_in_sent].rows(start2, start2 + m_ds - 1);
//        m_cap.col(contextwin_minibatches[i][j][k][2]) -=
//            m_lr*delta_x_vals[ind_in_sent].rows(start3, start3 + m_dc - 1);
//        start += m_de;
//        start2 += m_ds;
//        start3 += m_dc;
//      }
//    }
//  }
//
//  // grad checking
//  cerr << "backprop grad: " << wx_00 << endl;
//  cerr << "backprop grad: " << wy_00 << endl;
//  cerr << "backprop grad: " << wh_00 << endl;
//  cerr << "backprop grad emb: " << emb_0_8859 << endl;
//
//  return xf1;
//}



double
XF1Trainer::bptt_multi_multi(const vector<double> &f1_vals,
                       const vector<vector<vector<vector<int> > > > &contextwin_minibatches,
                       size_t sent_id, size_t sent_len) {

  cerr << "bptt_multi_multi() ...\n";
  //cerr.precision(15);
  // grad checking, for testing only
  mat wx_grad_approx = grad_check(f1_vals, contextwin_minibatches, sent_id, sent_len);
  mat wy_grad_approx = grad_check_wy(f1_vals, contextwin_minibatches, sent_id, sent_len);
  mat wh_grad_approx = grad_check_wh(f1_vals, contextwin_minibatches, sent_id, sent_len);
  double emb_grad_approx = grad_check_emb(f1_vals, contextwin_minibatches, sent_id, sent_len);
  double cap_grad_approx = grad_check_cap(f1_vals, contextwin_minibatches, sent_id, sent_len);

  const size_t kbest_size = f1_vals.size();
  assert(kbest_size == m_all_kbest[sent_id].size());

  vector<mat> x_vals;
  vector<mat> lh_vals;
  vector<mat> lh_deriv_vals;

  vector<mat> delta_h_vals(sent_len, mat(m_h_tm1.n_cols, 1, fill::zeros));
  vector<mat> delta_y_vals(sent_len, mat(m_wy.n_cols, 1, fill::zeros));

  mat all_tag_scores(kbest_size, sent_len, fill::zeros);
  m_h_tm1.zeros();
  lh_vals.push_back(m_h_tm1);

  // rescore all the tags in a kbest list (which
  // is equivalent to mtag this sentence once), and
  // memorize vals needed for bptt
  for (size_t i = 0; i < contextwin_minibatches.size(); ++i) {
    // for a minibatch
    for (size_t j = 0; j < contextwin_minibatches[i].size(); ++j) {
      // for a contextwin in a minibatch
      calc_lh_proj_emb(contextwin_minibatches[i][j], x_vals, lh_vals, lh_deriv_vals);
      mat ly_vals = sigmoid_no_flow2((lh_vals.back()*m_wy));

      // set the scores for all tags at this sentence position in this kbest list
      size_t ind_in_sent = i*m_bs_xf1 + j;
      for (size_t k = 0; k < kbest_size; ++k) {
        assert(m_all_kbest[sent_id][k].size() == sent_len);
        all_tag_scores(k, ind_in_sent) =
            ly_vals(m_all_kbest[sent_id][k][ind_in_sent].first);
      }
    }
  }

  //all_tag_scores.print("all_tag_scores");

  assert(x_vals.size() == lh_vals.size() - 1);
  assert(lh_vals.size() == lh_deriv_vals.size() + 1);
  assert(lh_vals.size() == sent_len + 1);

  mat all_ones = ones<mat>(all_tag_scores.n_rows, all_tag_scores.n_cols);
  mat all_delta_y_vals = all_ones - all_tag_scores;
  //all_delta_y_vals.print("all_delta_y_vals");

  colvec all_seq_norm_scores = soft_max_new(sum(arma::log(all_tag_scores), 1));
  //cout.precision(15);
  //all_seq_norm_scores.print("all_seq_norm_scores");
  colvec f1_vals_vec(f1_vals);

  double xf1 = sum(all_seq_norm_scores % f1_vals_vec);
  cerr << "xf1: " << xf1 << endl;
  assert(xf1 <= 1.0);
  assert(xf1 >= 0.0);

  colvec xf1_vec(kbest_size);
  xf1_vec.fill(xf1);

  assert(all_seq_norm_scores.n_rows == f1_vals_vec.n_rows);
  assert(all_seq_norm_scores.n_cols == f1_vals_vec.n_cols);

  colvec err_term = -(all_seq_norm_scores % (f1_vals_vec - xf1_vec));
  assert(err_term.n_rows == all_delta_y_vals.n_rows);

//  mat test(1, all_delta_y_vals.n_cols, fill::ones);
//  cerr << "err_term(0): " << err_term(0) << endl;
//  test.row(0) *= err_term(0);
//  test.print(cerr);

  for (size_t i = 0; i < err_term.n_rows; ++i) {
//    if (m_ignore_0_f1_seq && valid_seq_ids[i] == -1)
//      continue;
    all_delta_y_vals.row(i) *= err_term(i);
  }

  //all_delta_y_vals.print(cerr, "all_delta_y_vals");
  //err_term.print(cerr, "err_term");

  // actual xf1 bptt, first backprop gradients, no weight updates
  for (size_t i = 0; i < contextwin_minibatches.size(); ++i) {
    // for a minibatch
    for (int j = contextwin_minibatches[i].size() - 1; j >= 0; --j) {
      // for a contextwin in a minibatch
      size_t ind_in_sent = i*m_bs_xf1 + j;
      assert(ind_in_sent < sent_len);

      //cerr << "ind_in_sent: " << ind_in_sent << endl;

      for (size_t k = 0; k < kbest_size; ++k) {
        //cerr << "k: " << k << endl;
//        if (m_ignore_0_f1_seq && valid_seq_ids[k] == -1)
//          continue;
        size_t tag_class_ind = m_all_kbest[sent_id][k][ind_in_sent].first;
        assert(m_wy.col(tag_class_ind).n_rows == delta_h_vals[ind_in_sent].n_rows);
        assert(m_wy.col(tag_class_ind).n_cols == delta_h_vals[ind_in_sent].n_cols);
        delta_y_vals[ind_in_sent].row(tag_class_ind) += all_delta_y_vals(k, ind_in_sent);
      }

      //cout << "sent id: " << sent_id << endl;
      //delta_y_vals[ind_in_sent].print("delta_y_vals");

      assert(contextwin_minibatches[i].size() - 1 >= 0);
      assert(j >= 0);

      //lh_deriv_vals[ind_in_sent].print("lh_deriv_vals");

      if ((size_t)j == contextwin_minibatches[i].size() - 1) {
        delta_h_vals[ind_in_sent] = lh_deriv_vals[ind_in_sent].t() %
            (m_wy*delta_y_vals[ind_in_sent]);
      } else {
        delta_h_vals[ind_in_sent] = lh_deriv_vals[ind_in_sent].t() %
            ((m_wh*delta_h_vals[ind_in_sent + 1]) + (m_wy*delta_y_vals[ind_in_sent]));
      }

      //delta_h_vals[ind_in_sent].print("delta_h_vals");
    }
  }

  // grad checking
  double wx_00 = 0.0;
  double wy_063 = 0.0;
  double wh_00 = 0.0;
  double emb_0_23087 = 0.0;
  double cap_00 = 0.0;

  vector<mat> delta_x_vals(sent_len, zeros<mat>(m_wx.n_rows, 1));

  for (size_t i = 0; i < contextwin_minibatches.size(); ++i) {
    for (size_t j = 0; j < contextwin_minibatches[i].size(); ++j) {
      size_t ind_in_sent = i*m_bs_xf1 + j;
      delta_x_vals[ind_in_sent] = m_wx*delta_h_vals[ind_in_sent];
    }
  }

  // update weights
  for (size_t i = 0; i < contextwin_minibatches.size(); ++i) {
    for (size_t j = 0; j < contextwin_minibatches[i].size(); ++j) {

    	size_t ind_in_sent = i*m_bs_xf1 + j;

      mat grad_wh = lh_vals[ind_in_sent].t()*delta_h_vals[ind_in_sent].t();
      m_wh -= m_lr*grad_wh;
      wh_00 += grad_wh(0,0);

      //lh_vals[ind_in_sent + 1].print(cerr, "lh_vals");
      //delta_y_vals[ind_in_sent].print(cerr, "delta_y_vals");
      mat grad_wy = lh_vals[ind_in_sent + 1].t()*delta_y_vals[ind_in_sent].t();
      m_wy -= m_lr*grad_wy;
      // grad_checking
      wy_063 += grad_wy(0, 63);

      mat grad_wx = x_vals[ind_in_sent]*delta_h_vals[ind_in_sent].t();
      m_wx -= m_lr*grad_wx;
      // grad_checking
      wx_00 += grad_wx(0, 0);

      int start = 0;
      int start2 = m_de*m_cs;
      int start3 = start2 + m_ds*m_cs;
      for (size_t k = 0; k < m_cs; ++k) {
        m_emb.col(contextwin_minibatches[i][j][k][0]) -=
            m_lr*delta_x_vals[ind_in_sent].rows(start, start + m_de - 1);
        if (contextwin_minibatches[i][j][k][0] == 23087)
        	emb_0_23087 += delta_x_vals[ind_in_sent](start);
        m_suf.col(contextwin_minibatches[i][j][k][1]) -=
            m_lr*delta_x_vals[ind_in_sent].rows(start2, start2 + m_ds - 1);
        m_cap.col(contextwin_minibatches[i][j][k][2]) -=
            m_lr*delta_x_vals[ind_in_sent].rows(start3, start3 + m_dc - 1);
        if (contextwin_minibatches[i][j][k][2] == 0)
          cap_00 += delta_x_vals[ind_in_sent](start3);
        start += m_de;
        start2 += m_ds;
        start3 += m_dc;
      }
    }
  }

  cerr << "backprop grad wx: " << wx_00 << endl;
  cerr << "backprop grad wy: " << wy_063 << endl;
  cerr << "backprop grad wh: " << wh_00 << endl;
  cerr << "backprop grad emb: " << emb_0_23087 << endl;
  cerr << "backprop grad cap00: " << cap_00 << endl;

  return xf1;
}


//double
//XF1Trainer::bptt_multi_multi_tanh(const vector<double> &f1_vals,
//                       const vector<vector<vector<vector<int> > > > &contextwin_minibatches,
//                       size_t sent_id, size_t sent_len,
//                       const vector<int> &valid_seq_ids) {
//
//  cerr << "bptt_multi_multi() ...\n";
//  //cerr.precision(15);
//  // grad checking, for testing only
//  mat wx_grad_approx = grad_check(f1_vals, contextwin_minibatches, sent_id, sent_len, valid_seq_ids);
//  mat wy_grad_approx = grad_check_wy(f1_vals, contextwin_minibatches, sent_id, sent_len, valid_seq_ids);
//  mat wh_grad_approx = grad_check_wh(f1_vals, contextwin_minibatches, sent_id, sent_len, valid_seq_ids);
//  double emb_grad_approx = grad_check_emb(f1_vals, contextwin_minibatches, sent_id, sent_len, valid_seq_ids);
//  double cap_grad_approx = grad_check_cap(f1_vals, contextwin_minibatches, sent_id, sent_len, valid_seq_ids);
//
//  const size_t kbest_size = f1_vals.size();
//  assert(kbest_size == m_all_kbest[sent_id].size());
//
//  vector<mat> x_vals;
//  vector<mat> lh_vals;
//  vector<mat> lh_deriv_vals;
//
//  vector<mat> delta_h_vals(sent_len, mat(m_h_tm1.n_cols, 1, fill::zeros));
//  vector<mat> delta_y_vals(sent_len, mat(m_wy.n_cols, 1, fill::zeros));
//
//  mat all_tag_scores(kbest_size, sent_len, fill::zeros);
//  m_h_tm1.zeros();
//  lh_vals.push_back(m_h_tm1);
//
//  // rescore all the tags in a kbest list (which
//  // is equivalent to mtag this sentence once), and
//  // memorize vals needed for bptt
//  for (size_t i = 0; i < contextwin_minibatches.size(); ++i) {
//    // for a minibatch
//    for (size_t j = 0; j < contextwin_minibatches[i].size(); ++j) {
//      // for a contextwin in a minibatch
//      calc_lh_proj_emb_tanh(contextwin_minibatches[i][j], x_vals, lh_vals, lh_deriv_vals);
//      mat ly_vals = soft_max_new(lh_vals.back()*m_wy);
//
//      // set the scores for all tags at this sentence position in this kbest list
//      size_t ind_in_sent = i*m_bs + j;
//      for (size_t k = 0; k < kbest_size; ++k) {
//        assert(m_all_kbest[sent_id][k].size() == sent_len);
//        all_tag_scores(k, ind_in_sent) =
//            ly_vals(m_all_kbest[sent_id][k][ind_in_sent].first);
//      }
//    }
//  }
//
//  all_tag_scores.print("all_tag_scores");
//
//  assert(x_vals.size() == lh_vals.size() - 1);
//  assert(lh_vals.size() == lh_deriv_vals.size() + 1);
//  assert(lh_vals.size() == sent_len + 1);
//
//  mat all_ones = ones<mat>(all_tag_scores.n_rows, all_tag_scores.n_cols);
//  mat all_delta_y_vals = all_ones / arma::square(arma::cosh(all_tag_scores));
//  all_delta_y_vals.print("all_delta_y_vals");
//
//  colvec all_seq_norm_scores = soft_max_new(sum(arma::log(all_tag_scores), 1));
//  cout.precision(15);
//  all_seq_norm_scores.print("all_seq_norm_scores");
//  colvec f1_vals_vec(f1_vals);
//
//  double xf1 = sum(all_seq_norm_scores % f1_vals_vec);
//  cerr << "xf1: " << xf1 << endl;
//  assert(xf1 <= 1.0);
//  assert(xf1 >= 0.0);
//
//  colvec xf1_vec(kbest_size);
//  xf1_vec.fill(xf1);
//
//  assert(all_seq_norm_scores.n_rows == f1_vals_vec.n_rows);
//  assert(all_seq_norm_scores.n_cols == f1_vals_vec.n_cols);
//
//  colvec err_term = -(all_seq_norm_scores % (f1_vals_vec - xf1_vec));
//  assert(err_term.n_rows == all_delta_y_vals.n_rows);
//
////  mat test(1, all_delta_y_vals.n_cols, fill::ones);
////  cerr << "err_term(0): " << err_term(0) << endl;
////  test.row(0) *= err_term(0);
////  test.print(cerr);
//
//  all_delta_y_vals = all_delta_y_vals % (all_ones / (all_tag_scores));
//
//  for (size_t i = 0; i < err_term.n_rows; ++i) {
//    if (m_ignore_0_f1_seq && valid_seq_ids[i] == -1)
//      continue;
//    all_delta_y_vals.row(i) *= err_term(i);
//  }
//
//  all_delta_y_vals.print(cerr, "all_delta_y_vals");
//  err_term.print(cerr, "err_term");
//
//  // actual xf1 bptt, first backprop gradients, no weight updates
//  for (size_t i = 0; i < contextwin_minibatches.size(); ++i) {
//    // for a minibatch
//    for (int j = contextwin_minibatches[i].size() - 1; j >= 0; --j) {
//      // for a contextwin in a minibatch
//      size_t ind_in_sent = i*m_bs + j;
//      assert(ind_in_sent < sent_len);
//
//      //cerr << "ind_in_sent: " << ind_in_sent << endl;
//
//      for (size_t k = 0; k < kbest_size; ++k) {
//        //cerr << "k: " << k << endl;
//        if (m_ignore_0_f1_seq && valid_seq_ids[k] == -1)
//          continue;
//        size_t tag_class_ind = m_all_kbest[sent_id][k][ind_in_sent].first;
//        assert(m_wy.col(tag_class_ind).n_rows == delta_h_vals[ind_in_sent].n_rows);
//        assert(m_wy.col(tag_class_ind).n_cols == delta_h_vals[ind_in_sent].n_cols);
//        delta_y_vals[ind_in_sent].row(tag_class_ind) += all_delta_y_vals(k, ind_in_sent);
//      }
//
//      cout << "sent id: " << sent_id << endl;
//      delta_y_vals[ind_in_sent].print("delta_y_vals");
//
//      assert(contextwin_minibatches[i].size() - 1 >= 0);
//      assert(j >= 0);
//
//      //lh_deriv_vals[ind_in_sent].print("lh_deriv_vals");
//
//      if ((size_t)j == contextwin_minibatches[i].size() - 1) {
//        delta_h_vals[ind_in_sent] = lh_deriv_vals[ind_in_sent].t() %
//            (m_wy*delta_y_vals[ind_in_sent]);
//      } else {
//        delta_h_vals[ind_in_sent] = lh_deriv_vals[ind_in_sent].t() %
//            ((m_wh*delta_h_vals[ind_in_sent + 1]) + (m_wy*delta_y_vals[ind_in_sent]));
//      }
//
//      delta_h_vals[ind_in_sent].print("delta_h_vals");
//    }
//  }
//
//  // grad checking
//  double wx_00 = 0.0;
//  double wy_063 = 0.0;
//  double wh_00 = 0.0;
//  double emb_0_23087 = 0.0;
//  double cap_00 = 0.0;
//
//  vector<mat> delta_x_vals(sent_len, zeros<mat>(m_wx.n_rows, 1));
//
//  for (size_t i = 0; i < contextwin_minibatches.size(); ++i) {
//    for (size_t j = 0; j < contextwin_minibatches[i].size(); ++j) {
//      size_t ind_in_sent = i*m_bs + j;
//      delta_x_vals[ind_in_sent] = m_wx*delta_h_vals[ind_in_sent];
//    }
//  }
//
//  // update weights
//  for (size_t i = 0; i < contextwin_minibatches.size(); ++i) {
//    for (size_t j = 0; j < contextwin_minibatches[i].size(); ++j) {
//
//      size_t ind_in_sent = i*m_bs + j;
//
//      mat grad_wh = lh_vals[ind_in_sent].t()*delta_h_vals[ind_in_sent].t();
//      m_wh -= m_lr*grad_wh;
//      wh_00 += grad_wh(0,0);
//
//      //lh_vals[ind_in_sent + 1].print(cerr, "lh_vals");
//      //delta_y_vals[ind_in_sent].print(cerr, "delta_y_vals");
//      mat grad_wy = lh_vals[ind_in_sent + 1].t()*delta_y_vals[ind_in_sent].t();
//      m_wy -= m_lr*grad_wy;
//      // grad_checking
//      wy_063 += grad_wy(0, 63);
//
//      mat grad_wx = x_vals[ind_in_sent]*delta_h_vals[ind_in_sent].t();
//      m_wx -= m_lr*grad_wx;
//      // grad_checking
//      wx_00 += grad_wx(0, 0);
//
//      int start = 0;
//      int start2 = m_de*m_cs;
//      int start3 = start2 + m_ds*m_cs;
//      for (size_t k = 0; k < m_cs; ++k) {
//        m_emb.col(contextwin_minibatches[i][j][k][0]) -=
//            m_lr*delta_x_vals[ind_in_sent].rows(start, start + m_de - 1);
//        if (contextwin_minibatches[i][j][k][0] == 23087)
//          emb_0_23087 += delta_x_vals[ind_in_sent](start);
//        m_suf.col(contextwin_minibatches[i][j][k][1]) -=
//            m_lr*delta_x_vals[ind_in_sent].rows(start2, start2 + m_ds - 1);
//        m_cap.col(contextwin_minibatches[i][j][k][2]) -=
//            m_lr*delta_x_vals[ind_in_sent].rows(start3, start3 + m_dc - 1);
//        if (contextwin_minibatches[i][j][k][2] == 0)
//          cap_00 += delta_x_vals[ind_in_sent](start3);
//        start += m_de;
//        start2 += m_ds;
//        start3 += m_dc;
//      }
//    }
//  }
//
//  cerr << "backprop grad wx: " << wx_00 << endl;
//  cerr << "backprop grad wy: " << wy_063 << endl;
//  cerr << "backprop grad wh: " << wh_00 << endl;
//  cerr << "backprop grad emb: " << emb_0_23087 << endl;
//  cerr << "backprop grad cap00: " << cap_00 << endl;
//
//  return xf1;
//}



void
XF1Trainer::mtag(bool dev) {
  //m_tagger.re_init_mats(m_wx, m_wh, m_wy, m_emb, m_suf, m_cap);
	vector<vector<vector<vector<int> > > > all_sent_contextwins;
  m_tagger.mtag(dev);
}


mat
XF1Trainer::sigmoid(const mat &m) {
  mat one = ones<mat>(m.n_rows, m.n_cols);
  return one/(one + exp(-m));
}


mat
XF1Trainer::sigmoid_no_flow(const mat &m) {
  assert(m.n_rows == 1);
  double overflow = 20;
  mat m_copy(m);
  for (size_t i = 0; i < m_copy.n_cols; ++i) {
    if (m_copy(i) > overflow) {
      m_copy(i) = overflow;
    } else if (m_copy(i) < -overflow){
      m_copy(i) = -overflow;
    }
  }
  mat one = ones<mat>(m_copy.n_rows, m_copy.n_cols);
  mat exp_m_copy = exp(m_copy);
  return exp_m_copy/(one + exp_m_copy);
}


// ftp://ftp.sas.com/pub/neural/FAQ2.html#A_overflow
//Subject: How to avoid overflow in the logistic function?
//
//The formula for the logistic activation function is often written as:
//   netoutput = 1 / (1+exp(-netinput));
//But this formula can produce floating-point overflow in the exponential function if you program it in this simple form. To avoid overflow, you can do this:
//   if (netinput < -45) netoutput = 0;
//   else if (netinput > 45) netoutput = 1;
//   else netoutput = 1 / (1+exp(-netinput));
//The constant 45 will work for double precision on all machines that I know of, but there may be some bizarre machines where it will require some adjustment. Other activation functions can be handled similarly.


mat
XF1Trainer::sigmoid_no_flow2(const mat &m) {
  assert(m.n_rows == 1);
  mat res(m.n_rows, m.n_cols, fill::zeros);
  for (size_t i = 0; i < m.n_cols; ++i) {
    if (m(i) < -45.0)
      res(i) = 0.0;
    else if (m(i) > 45.0)
      res(i) = 1.0;
    else
      res(i) = 1.0 / (1.0 + exp(-m(i)));
  }

  return res;
}

// http://timvieira.github.io/blog/post/2014/02/11/exp-normalize-trick/
//def sigmoid(x):
//    "Numerically-stable sigmoid function."
//    if x >= 0:
//        z = exp(-x)
//        return 1 / (1 + z)
//    else:
//        # if x is less than zero then z will be small, denom can't be
//        # zero because it's 1+z.
//        z = exp(x)
//        return z / (1 + z)



mat
XF1Trainer::soft_max(const mat &y) {
  mat res = exp(y)/sum(exp(y));
  double check = accu(res);
  cerr.precision(15);
  cerr << "soft_max sum: " << check << endl;
  assert(check == 1.0);
  return res;
}


mat
XF1Trainer::soft_max_no_flow(const mat &y) {
  mat max_y(y.n_rows, y.n_cols, fill::zeros);
  max_y.fill(y.max());
  mat temp = exp(y - max_y);
  mat res = temp/sum(temp);
  //double check = sum(res);
  //cerr.precision(15);
  //if (check != 1.0)
    //cerr << "soft_max_no_flow sum: " << check << endl;
  return res;
}


// this is based on someone else's code
// Michael sent to me

colvec
XF1Trainer::soft_max_new(const colvec &y) {
  double log_tolerance = 50;
  uword max_ind;
  double max = y.max();
  y.max(max_ind);

  double cutoff = max - log_tolerance; // ignore terms smaller than cutoff
  bool have_terms = false; // true if there are terms > cutoff - except for max
  double intermediate = 0.0;
  double normalization = 0.0;

  for (size_t i = 0; i < y.n_rows; ++i) {
    if (y(i) > cutoff && i != (size_t)max_ind) {
      have_terms = true;
      intermediate += exp(y(i) - max);
    }
  }

  if (have_terms) {
    normalization = max + log(1.0 + intermediate);
  } else {
    normalization = max;
  }

  colvec normal_vec(y.n_rows, y.n_cols, fill::zeros);
  normal_vec.fill(normalization);
  return arma::exp(y - normal_vec);
}


colvec
XF1Trainer::soft_max_xf1(const colvec &col,
                         const vector<int> &valid_seq_ids) {
  colvec max(col.n_rows, col.n_cols, fill::zeros);
  max.fill(col.max());
  colvec temp = exp(col - max);
  //cerr.precision(15);
  //temp.print(cerr);
  if (m_ignore_0_f1_seq) {
    for (size_t i = 0; i < valid_seq_ids.size(); ++i) {
      if (valid_seq_ids[i] == -1)
        temp(i) = 0;
    }
  }
  //double m = col.max();
  colvec res = temp/sum(temp);
  //res.print();
  //double check = sum(res);
  //if (check != 1.0)
    //cerr << "soft_max_xf1 sum: " <<  check << endl;
  return res;
}


colvec
XF1Trainer::soft_max_xf1(const colvec &col) {
  colvec max(col.n_rows, col.n_cols, fill::zeros);
  max.fill(col.max());
  colvec temp = exp(col - max);
  //cerr.precision(15);
  //temp.print(cerr);
  //double m = col.max();
  colvec res = temp/sum(temp);
  //res.print();
  //double check = sum(res);
  //if (check != 1.0)
    //cerr << "soft_max_xf1 sum: " <<  check << endl;
  return res;
}


void
XF1Trainer::contextwin2minibatch(const vector<vector<vector<int> > > &sent_cw,
                                 vector<vector<vector<vector<int> > > > &minibatch_all) {
  vector<vector<vector<int> > > minibatch;
  for (size_t i = 0; i < sent_cw.size(); ++i) {
    minibatch.push_back(sent_cw[i]);
    if ((i + 1) % m_bs_xf1 == 0 || i == sent_cw.size() - 1) {
      minibatch_all.push_back(minibatch);
      minibatch.clear();
    }
  }
}


double
XF1Trainer::calc_f1(const double &gold,
                    const double &total, const size_t id, bool train) {
	double gold_total = 0.0;
	if (train) {
    assert(m_ccg_gold_dep_count_map_train.find(id) != m_ccg_gold_dep_count_map_train.end());
    gold_total = m_ccg_gold_dep_count_map_train.find(id)->second;
	} else {
    assert(m_ccg_gold_dep_count_map_dev.find(id) != m_ccg_gold_dep_count_map_dev.end());
    gold_total = m_ccg_gold_dep_count_map_dev.find(id)->second;
	}
  assert(gold_total != 0);
  cerr << "gold: " << gold;
  cerr << " total: " << total;
  cerr << " gold total: " << gold_total << endl;
  assert(gold <= gold_total);
  double p = gold/total;
  double r = gold/gold_total;
  double f1 = (p == 0.0) || (r == 0.0) ? 0.0 : (2.0*p*r)/(p + r);
  fprintf(stderr, "p: %f, r: %f, f1: %f\n", p, r, f1);
  assert(f1 <= 1.0);

  total_correct += gold;
  total_returned += total;
  total_gold += gold_total;

  return f1;
}


void
XF1Trainer::load_gold_deps(const string &filename, bool train) {
  // note: there are sentences in wsj0221_ccgbank_deps
  //       which have zero deps

  size_t total_sents = 0;
	if (train)
		total_sents = 39604;
	else
		total_sents = 1913;

  const ulong BASE = 10000;

  ifstream in(filename.c_str());
   if(!in) {
     cerr << "no such file: " << filename << endl;
     exit (EXIT_FAILURE);
   }
   string s;
   ulong sent_dep_count = 0;
   ulong empty = 0;

   while (getline(in, s)) {
     if (s.find("# this") != string::npos ||
         s.find("# src/") != string::npos)
       continue;

     if (s.empty()) {
       ++empty;
       if (empty >= 2 && empty <= total_sents + 1) {
      	 if (train)
           assert(m_ccg_gold_dep_count_map_train.insert(make_pair(empty - 1, sent_dep_count)).second);
      	 else
           assert(m_ccg_gold_dep_count_map_dev.insert(make_pair(empty - 1, sent_dep_count)).second);
       }
       sent_dep_count = 0;
       continue;
     }

     ++sent_dep_count;
     istringstream ss(s);
     string head_str;
     string plain_markedup_str;
     string arg_str;

     ulong head = 0;
     int markedup = 0;
     ulong slot = 0;
     ulong arg = 0;

     if(ss >> head_str >> plain_markedup_str >> slot >> arg_str) {
       head = stoi(head_str.substr(head_str.find("_") + 1));

       int ind = m_cats.markedup.index(plain_markedup_str);
       if (ind != -1) {
         // convert plain markedup index to markedup index
         // since they have different indexes in cats.markedup
         markedup = m_cats.markedup.index(m_cats.markedup.markedup(plain_markedup_str));
         assert(markedup != -1);
       } else {
         auto iter = m_unk_markedup_map.find(plain_markedup_str);
         if (iter != m_unk_markedup_map.end()) {
           markedup = iter->second;
         } else {
           markedup = m_unk_markedup_map.size() + BASE;
           m_unk_markedup_map.insert(make_pair(plain_markedup_str, m_unk_markedup_map.size() + BASE));
         }
       }

       arg = stoi(arg_str.substr(arg_str.find("_") + 1));

       CCGDep dep(empty, head, (ulong)markedup, slot, arg);
       if (train)
         assert(m_ccg_dep_map_train.insert(make_pair(dep, 0)).second);
       else
         assert(m_ccg_dep_map_dev.insert(make_pair(dep, 0)).second);
     }
     else {
       cerr << "could not interpret dependency\n";
       exit(EXIT_FAILURE);
     }
   }

   if (train)
     assert(m_ccg_gold_dep_count_map_train.size() == 39604);
   else
     assert(m_ccg_gold_dep_count_map_dev.size() == 1913);
}


void
XF1Trainer::get_deps(const CCG::SuperCat *sc, const ulong id,
                     double &gold, double &total, const int FMT, bool train) {
  if(sc->left){
    assert(sc->left);
    get_deps(sc->left, id, gold, total, FMT, train);
    if(sc->right){
      assert(sc->right);
      get_deps(sc->right, id, gold, total, FMT, train);
    }
  }

  pair<double, double> res;
  if (train)
    res = count_gold_deps(sc, id);
  else
    res = count_gold_deps_dev(sc, id);
  gold += res.first;
  total += res.second;
}


pair<double, double>
XF1Trainer::count_gold_deps(const CCG::SuperCat *sc,
		                    const ulong id) {

  double total = 0.0;
  double gold = 0.0;
  for(const CCG::Filled *filled = sc->filled; filled; filled = filled->next)
  {
    if (ignore_dep(filled)) {
      continue;
    }
    ++total;
    CCGDep dep(id, filled->head,
               m_cats.markedup.index((string) m_cats.relations[filled->rel].cat),
               m_cats.relations[filled->rel].jslot, filled->filler);
    if (m_ccg_dep_map_train.find(dep) != m_ccg_dep_map_train.end()) {
      ++gold;
    }
  }

  return make_pair(gold, total);
}


pair<double, double>
XF1Trainer::count_gold_deps_dev(const CCG::SuperCat *sc, const ulong id) {

  double total = 0.0;
  double gold = 0.0;
  for(const CCG::Filled *filled = sc->filled; filled; filled = filled->next)
  {
    if (ignore_dep(filled)) {
      continue;
    }
    ++total;
    CCGDep dep(id, filled->head,
               m_cats.markedup.index((string) m_cats.relations[filled->rel].cat),
               m_cats.relations[filled->rel].jslot, filled->filler);
    if (m_ccg_dep_map_dev.find(dep) != m_ccg_dep_map_dev.end()) {
      ++gold;
    }
  }

  return make_pair(gold, total);
}



bool XF1Trainer::ignore_dep(const CCG::Filled *&filled) {

  if (m_deps_ignore_map_rule.find((ulong)filled->rule)
      != m_deps_ignore_map_rule.end())
    return true;

  int marked_up = m_cats.markedup.index((string) m_cats.relations[filled->rel].cat);
  assert(marked_up != -1);
  if (m_deps_ignore_map_1.find(make_tuple((ulong)marked_up,
        m_cats.relations[filled->rel].jslot,
        (ulong)filled->rule)) != m_deps_ignore_map_1.end())
    return true;

  if (m_deps_ignore_map_2.find(make_tuple(m_sent.words[filled->head - 1],
        (ulong)marked_up, m_cats.relations[filled->rel].jslot,
        (ulong)filled->rule)) != m_deps_ignore_map_2.end())
    return true;

  if (m_deps_ignore_map_3.find(make_tuple(m_sent.words[filled->head - 1],
        (ulong)marked_up,
        m_cats.relations[filled->rel].jslot,
        m_sent.words[filled->filler - 1],
        (ulong)filled->rule)) != m_deps_ignore_map_3.end())
    return true;

  return false;
}


void
XF1Trainer::init_ignore_rules() {
  m_deps_ignore_map_rule.insert({{7, 0}, {11, 0}, {12, 0},
                {14, 0}, {15, 0}, {16, 0},
                {17, 0}, {51, 0}, {52, 0},
                {56, 0}, {91, 0}, {92, 0},
                {95, 0}, {96, 0}, {98, 0}});
}


void
XF1Trainer::load_dep_ignore_list(const std::string &filename) {
  ifstream in(filename.c_str());
  if (!in) {
    cerr << "no such file: " << filename << endl;
    exit(EXIT_FAILURE);
  }
  string s;
  while (getline(in, s)) {
    //cout << s << endl;
    if (s.find("#") == 0) {
      continue;
    }

    istringstream ss(s);
    string cat_str;
    int cat = 0;
    ulong slot = 0;
    ulong rule_id = 0;
    if(ss >> cat_str >> slot >> rule_id) {
     cat = m_cats.markedup.index(cat_str);
     if (cat == -1) {
       cerr << "unrecognized markedup cat in " <<
           filename << ", exit now...\n";
       exit(EXIT_FAILURE);
     }
     assert(m_deps_ignore_map_1.insert(make_pair(make_tuple((ulong)cat, slot, rule_id), 0)).second);
    } else {
      cerr << "something is wrong with deps_ignore file !!!\n";
      exit(EXIT_FAILURE);
    }
  }
}


void
XF1Trainer::load_dep_ignore_list2(const std::string &filename) {
  ifstream in(filename.c_str());
  if (!in) {
    cerr << "no such file: " << filename << endl;
    exit(EXIT_FAILURE);
  }
  string s;
  while (getline(in, s)) {
    //cout << s << endl;
    if (s.find("#") == 0) {
      continue;
    }

    istringstream ss(s);
    string pred;
    string cat_str;
    int cat = 0;
    ulong slot = 0;
    ulong rule_id = 0;
    if(ss >> pred >> cat_str >> slot >> rule_id) {
      cat = m_cats.markedup.index(cat_str);
      if (cat == -1) {
        cerr << "unrecognized markedup cat in " <<
            filename << ", exit now...\n";
        exit(EXIT_FAILURE);
      }
     assert(m_deps_ignore_map_2.insert(make_pair(make_tuple(pred, (ulong)cat, slot, rule_id), 0)).second);
    } else {
      cerr << "something is wrong deps_ignore file !!!\n";
      exit(EXIT_FAILURE);
    }
  }
}


void
XF1Trainer::load_dep_ignore_list3(const std::string &filename) {

//  int id1 = m_cats.markedup.index("(S[pt]\\NP)/(S[ng]\\NP)");
//  int id2 = m_cats.markedup.index("((S[pt]{_}\\NP{Y}<1>){_}/(S[ng]{Z}<2>\\NP{Y*}){Z}){_}");
//
//  cout << "id1: " << id1 << endl;
//  cout << "id2: " << id2 << endl;

  ifstream in(filename.c_str());
  if (!in) {
    cerr << "no such file: " << filename << endl;
    exit(EXIT_FAILURE);
  }
  string s;
  while (getline(in, s)) {
    //cout << s << endl;
    if (s.find("#") == 0) {
      continue;
    }

    istringstream ss(s);
    string pred;
    string cat_str;
    int cat = 0;
    ulong slot = 0;
    string arg;
    ulong rule_id = 0;
    if(ss >> pred >> cat_str >> slot >> arg >> rule_id) {
      cat = m_cats.markedup.index(cat_str);
      if (cat == -1) {
        cerr << "unrecognized markedup cat in " <<
            filename << ", exit now...\n";
      }
     assert(m_deps_ignore_map_3.insert(make_pair(make_tuple(pred, (ulong)cat, slot, arg, rule_id), 0)).second);
    } else {
      cerr << "something is wrong deps_ignore file !!!\n";
      exit(EXIT_FAILURE);
    }
  }
}


//void
//XF1Trainer::gen_contextwins() {
//  vector<vector<vector<int> > > contextwins;
//  for (size_t i = 0; i < 39604; ++i) {
//    contextwins.clear();
//    sent2contextwin(m_tagger.m_train_words[i], contextwins, m_vocsize + 4 - 1, m_suffix_count);
//    m_all_tagger_data_contextwins.push_back(contextwins);
//  }
//}
//
//
//void
//XF1Trainer::sent2contextwin(const vector<vector<int> > &sent,
//                            vector<vector<vector<int> > > &contextwins,
//                            int unk_word_ind, int unk_suf_ind) {
//  assert(m_cs % 2 == 1);
//  size_t pad_len = m_cs/2;
//  vector<int> unk_vec;
//  unk_vec.push_back(unk_word_ind);
//  unk_vec.push_back(unk_suf_ind);
//  unk_vec.push_back(0);
//
//  for (size_t i = 0; i < sent.size(); ++i) {
//    vector<vector<int> > contextwin;
//    for (size_t j = i, k =0; j < i + m_cs; ++j, ++k) {
//      if (j < pad_len || j >= pad_len + sent.size()) {
//        contextwin.push_back(unk_vec);
//      } else {
//        contextwin.push_back(sent[j - pad_len]);
//      }
//    }
//    contextwins.push_back(contextwin);
//  }
//}


void XF1Trainer::gen_kbest(const string &words, const string &labels) {
  cerr << "loading training data...\n";
  m_tagger.load_train_data(words, labels);
  cerr << "loading dev data...\n";
  m_tagger.load_dev_test_data();
  cerr << "mtagging training data...\n";
  m_tagger.mtag_xf1(false, m_all_tagger_data_contextwins);
  cerr << "gen_kbest training data...\n";
  for (size_t i = 0; i < m_tagger.m_all_res_xf1_train.size(); ++i) {
  //for (size_t i = 0; i < 10; ++i) {
    m_all_kbest.push_back(m_tagger.gen_kbest(m_tagger.m_all_res_xf1_train[i], m_k));
  }
  m_tagger.eval_super(true, true);
}


void XF1Trainer::gen_kbest_dev() {
  m_tagger.mtag_xf1(true, m_all_tagger_data_contextwins_dev);
	for (size_t i = 0; i < m_tagger.m_all_res_xf1_dev.size(); ++i) {
    m_all_kbest_dev.push_back(m_tagger.gen_kbest(m_tagger.m_all_res_xf1_dev[i], m_k));
	}
	cerr << "m_all_kbest_dev size: " << m_all_kbest_dev.size() << endl;
}


void XF1Trainer::eval_dev_xf1(IO::ReaderFactory &reader_dev, const int FMT) {

	gen_kbest_dev();

	for (size_t e = 0; e < 40; ++e) {

	  // eval dev by generating k-best with baseline tagger, then rescore with new model
	  // and calculate xf1
	  const string wx_mat_file = "./xf1_results_sr1/xf1_wx." + to_string(e) +  ".mat";
    const string wh_mat_file = "./xf1_results_sr1/xf1_wh." + to_string(e) +  ".mat";
    const string wy_mat_file = "./xf1_results_sr1/xf1_wy." + to_string(e) +  ".mat";
    const string emb_mat_file = "./xf1_results_sr1/xf1_emb." + to_string(e) +  ".mat";
    const string suf_mat_file = "./xf1_results_sr1/xf1_suf." + to_string(e) +  ".mat";
    const string cap_mat_file = "./xf1_results_sr1/xf1_cap." + to_string(e) +  ".mat";

	  m_wx.load(wx_mat_file);
	  m_wh.load(wh_mat_file);
	  m_wy.load(wy_mat_file);
	  m_emb.load(emb_mat_file);
	  m_suf.load(suf_mat_file);
	  m_cap.load(cap_mat_file);

	  reader_dev.reset();
	  vector<double> f1_vals;
	  double gold = 0.0;
	  double total = 0.0;
	  double beta = 0.0001;

	  double total_xf1 = 0.0;

	  for (size_t i = 0; i < 1913; ++i) {
	    cerr << "sent: " << i << endl;
	    reader_dev.next(m_sent);
	    if (m_sent.words.size() == 1 ||
	        m_ccg_gold_dep_count_map_dev.find(i + 1)->second == 0)
	      continue;
	    f1_vals.clear();
	    size_t sent_size = m_sent.words.size();

	    const size_t kbest_size = m_all_kbest_dev[i].size();

	    for (size_t j = 0; j < kbest_size; ++j) {
	      // for each sequence
	      cerr << "sent size: " << sent_size << endl;
	      cerr << "m_all_kbest_dev[i][j].size(): " << m_all_kbest_dev[i][j].size() << endl;
	      assert(sent_size == m_all_kbest_dev[i][j].size());
	      m_sent.msuper.resize(sent_size);

	      for (size_t k = 0; k < sent_size; ++k) {
	        // for each word in this sequence
	        MultiRaw &mraw = m_sent.msuper[k];
	        mraw.resize(0);
	        mraw.push_back(ScoredRaw(m_all_kbest_dev[i][j][k].second, 1.0));
	        //cerr << " " << ScoredRaw(m_all_kbest[tagger_data_ind][j][k].second, 1.0).raw;
	      }

	      gold = 0.0;
	      total = 0.0;
	      const NLP::CCG::ShiftReduceHypothesis *output = m_parser.ShiftReduceParse(beta, false);

	      if(output) {
	        cerr << "k: " << j << endl;
	        assert(output != 0);

	        size_t stackSize = output->GetStackSize();
	        std::vector<const NLP::CCG::SuperCat *> roots;
	        vector<const NLP::CCG::SuperCat *>::const_reverse_iterator rit;
	        roots.push_back(output->GetStackTopSuperCat());

	        for (size_t i = 0; i < stackSize - 1; ++i) {
	          output = output->GetPrvStack();
	          roots.push_back(output->GetStackTopSuperCat());
	        }

	        for (rit = roots.rbegin(); rit < roots.rend(); ++rit) {
	          get_deps(*rit, i + 1, gold, total, FMT, false);
	          //m_printer.parsed(*rit, m_sent, beta, 0);
	        }

	        // testing evaluation only
	        //m_printer.lexical(m_sent);
	        //m_printer.newline();

	        assert(gold <= total);
	        double f1 = calc_f1(gold, total, i + 1, false);
	        f1_vals.push_back(f1);

	        if (f1 == 0.0)
	          cerr << "sent: " << i << " seq: " << j << " f1 0.0\n";

	      } else {
	        cerr << "sent : " << i << " no parse found\n";
	        //exploded = true;
	        f1_vals.push_back(0.0);
	        break;
	      }
	      m_parser.clear_xf1();
	    }

	    assert(kbest_size == f1_vals.size());

	    vector<vector<vector<vector<int> > > > sent_contextwin_minibatches;
	    contextwin2minibatch(m_all_tagger_data_contextwins_dev[i],
	        sent_contextwin_minibatches);
	    mat all_tag_scores(kbest_size, sent_size, fill::zeros);

	    vector<mat> lh_vals;
	    m_h_tm1.zeros();
	    lh_vals.push_back(m_h_tm1);
	    for (size_t m = 0; m < sent_contextwin_minibatches.size(); ++m) {
	      // for a minibatch
	      for (size_t l = 0; l < sent_contextwin_minibatches[m].size(); ++l) {
	        // for a contextwin in a minibatch
	        calc_lh_proj_emb(sent_contextwin_minibatches[m][l], lh_vals);
	        mat ly_vals = sigmoid_no_flow2(lh_vals.back()*m_wy);

	        // set the scores for all tags at this sentence position in this kbest list
	        size_t ind_in_sent = m*m_bs_xf1 + l;
	        for (size_t n = 0; n < kbest_size; ++n) {
	          assert(m_all_kbest_dev[i][n].size() == sent_size);
	          all_tag_scores(n, ind_in_sent) =
	              ly_vals(m_all_kbest_dev[i][n][ind_in_sent].first);
	        }
	      }
	    }

	    colvec all_seq_norm_scores = soft_max_new(sum(log(all_tag_scores), 1));
	    colvec f1_vals_vec(f1_vals);

	    double xf1 = sum(all_seq_norm_scores % f1_vals_vec);
	    cerr << "xf1: " << xf1 << endl;
	    assert(xf1 <= 1.0);
	    assert(xf1 >= 0.0);
	    total_xf1 += xf1;

	    m_sent.reset();

	  }

	  cerr << "avg xf1 per epoch: " << total_xf1/(1913.0) << endl;

	}

}


void XF1Trainer::eval_dev_1best_rescore(IO::ReaderFactory &reader_dev, const int FMT) {

  gen_kbest_dev();

  for (size_t e = 0; e < 40; ++e) {

    // eval dev by generating k-best with baseline tagger, then rescore with new model
    // and calculate xf1
    const string wx_mat_file = "./xf1_results_sr/xf1_wx." + to_string(e) +  ".mat";
    const string wh_mat_file = "./xf1_results_sr/xf1_wh." + to_string(e) +  ".mat";
    const string wy_mat_file = "./xf1_results_sr/xf1_wy." + to_string(e) +  ".mat";
    const string emb_mat_file = "./xf1_results_sr/xf1_emb." + to_string(e) +  ".mat";
    const string suf_mat_file = "./xf1_results_sr/xf1_suf." + to_string(e) +  ".mat";
    const string cap_mat_file = "./xf1_results_sr/xf1_cap." + to_string(e) +  ".mat";

    m_wx.load(wx_mat_file);
    m_wh.load(wh_mat_file);
    m_wy.load(wy_mat_file);
    m_emb.load(emb_mat_file);
    m_suf.load(suf_mat_file);
    m_cap.load(cap_mat_file);

    double total_max_rescore = 0.0;
    double total_max_baseline = 0.0;

    reader_dev.reset();
    vector<double> f1_vals;
    double gold = 0.0;
    double total = 0.0;
    double beta = 0.0001;

    double total_xf1 = 0.0;

    for (size_t i = 0; i < 1913; ++i) {
      cerr << "sent: " << i << endl;
      reader_dev.next(m_sent);
      if (m_sent.words.size() == 1 ||
          m_ccg_gold_dep_count_map_dev.find(i + 1)->second == 0)
        continue;
      f1_vals.clear();
      size_t sent_size = m_sent.words.size();

      const size_t kbest_size = m_all_kbest_dev[i].size();

      for (size_t j = 0; j < kbest_size; ++j) {
        // for each sequence
        cerr << "sent size: " << sent_size << endl;
        cerr << "m_all_kbest_dev[i][j].size(): " << m_all_kbest_dev[i][j].size() << endl;
        assert(sent_size == m_all_kbest_dev[i][j].size());
        m_sent.msuper.resize(sent_size);

        for (size_t k = 0; k < sent_size; ++k) {
          // for each word in this sequence
          MultiRaw &mraw = m_sent.msuper[k];
          mraw.resize(0);
          mraw.push_back(ScoredRaw(m_all_kbest_dev[i][j][k].second, 1.0));
          //cerr << " " << ScoredRaw(m_all_kbest[tagger_data_ind][j][k].second, 1.0).raw;
        }

        gold = 0.0;
        total = 0.0;
        const NLP::CCG::ShiftReduceHypothesis *output = m_parser.ShiftReduceParse(beta, false);

        if(output) {
          cerr << "k: " << j << endl;
          assert(output != 0);

          size_t stackSize = output->GetStackSize();
          std::vector<const NLP::CCG::SuperCat *> roots;
          vector<const NLP::CCG::SuperCat *>::const_reverse_iterator rit;
          roots.push_back(output->GetStackTopSuperCat());

          for (size_t i = 0; i < stackSize - 1; ++i) {
            output = output->GetPrvStack();
            roots.push_back(output->GetStackTopSuperCat());
          }

          for (rit = roots.rbegin(); rit < roots.rend(); ++rit) {
            get_deps(*rit, i + 1, gold, total, FMT, false);
            //m_printer.parsed(*rit, m_sent, beta, 0);
          }

          // testing evaluation only
          //m_printer.lexical(m_sent);
          //m_printer.newline();

          assert(gold <= total);
          double f1 = calc_f1(gold, total, i + 1, false);
          f1_vals.push_back(f1);

          if (f1 == 0.0)
            cerr << "sent: " << i << " seq: " << j << " f1 0.0\n";

        } else {
          cerr << "sent : " << i << " no parse found\n";
          //exploded = true;
          f1_vals.push_back(0.0);
          break;
        }
        m_parser.clear_xf1();
      }

      assert(kbest_size == f1_vals.size());

      vector<vector<vector<vector<int> > > > sent_contextwin_minibatches;
      contextwin2minibatch(m_all_tagger_data_contextwins_dev[i],
          sent_contextwin_minibatches);
      mat all_tag_scores(kbest_size, sent_size, fill::zeros);

      vector<mat> lh_vals;
      m_h_tm1.zeros();
      lh_vals.push_back(m_h_tm1);
      for (size_t m = 0; m < sent_contextwin_minibatches.size(); ++m) {
        // for a minibatch
        for (size_t l = 0; l < sent_contextwin_minibatches[m].size(); ++l) {
          // for a contextwin in a minibatch
          calc_lh_proj_emb(sent_contextwin_minibatches[m][l], lh_vals);
          mat ly_vals = sigmoid_no_flow(lh_vals.back()*m_wy);

          // set the scores for all tags at this sentence position in this kbest list
          size_t ind_in_sent = m*m_bs_xf1 + l;
          for (size_t n = 0; n < kbest_size; ++n) {
            assert(m_all_kbest_dev[i][n].size() == sent_size);
            all_tag_scores(n, ind_in_sent) =
                ly_vals(m_all_kbest_dev[i][n][ind_in_sent].first);
          }
        }
      }

      colvec all_f1_vals(f1_vals);
      colvec all_seq_scores = sum(log(all_tag_scores), 1);
      //colvec all_seq_norm_scores = soft_max_new(sum(log(all_tag_scores), 1));
      uword max_ind;
      all_seq_scores.max(max_ind);
      double max_f1_score = all_f1_vals(max_ind);
      double max_f1_baseline = all_f1_vals.max();
      cerr << "max_f1_score: " << max_f1_score << endl;
      cerr << "max_f1_baseline: " << max_f1_baseline << endl;

      total_max_rescore += max_f1_score;
      total_max_baseline + max_f1_baseline;
      //double xf1 = sum(all_seq_norm_scores % f1_vals_vec);
      //cerr << "xf1: " << xf1 << endl;
      //assert(xf1 <= 1.0);
      //assert(xf1 >= 0.0);
      //total_xf1 += xf1;

      m_sent.reset();

    }

    cerr << "avg xf1 per epoch, baseline: " << total_max_baseline/(1913.0) << endl;
    cerr << "avg xf1 per epoch, rescore: " << total_max_rescore/(1913.0) << endl;

  }

}



void XF1Trainer::load_skip_ids(const string &filename) {
  cerr << "loading skip ids...\n";
  ifstream in(filename.c_str());
  if(!in) {
    cerr << "could not open skip ids file\n";
    exit(EXIT_FAILURE);
  }
  size_t id;
  while (in >> id)
    m_skip_ids_map.insert(make_pair(id, 0));
  cerr << "total skip ids: " << m_skip_ids_map.size() << endl;
}


void XF1Trainer::save_model(const string dir, size_t e) {
	cerr << "XF1Trainer saving model...\n";
  m_wx.save(dir + "/xf1_wx." + to_string(e) + ".mat");
  m_wh.save(dir +  "/xf1_wh." + to_string(e) + ".mat");
  m_wy.save(dir +  "/xf1_wy." + to_string(e) + ".mat");

  m_suf.save(dir +  "/xf1_suf." + to_string(e) + ".mat");
  m_cap.save(dir +  "/xf1_cap." + to_string(e) + ".mat");
  m_emb.save(dir +  "/xf1_emb." + to_string(e) + ".mat");
}


} }
