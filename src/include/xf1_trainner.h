// Copyright (c) Cambridge University Computer Lab
// Copyright (c) Wenduan Xu

#ifndef SRC_INCLUDE_XF1_TRAINER__
#define SRC_INCLUDE_XF1_TRAINER__

#include <armadillo>
#include <unordered_map>
#include "rnn_tagger.h"
#include "base.h"
#include "config/config.h"
#include "parser/parser.h"
#include "parser/printer.h"
#include "io/reader_factory.h"
#include "parser/supercat.h"
#include <tuple>
#include "parser/print_deps.h"

using namespace std;
using namespace arma;

namespace NLP {
namespace Taggers {

typedef ushort RelID;
typedef ushort CatID;
typedef unsigned char RuleID;
typedef unsigned short int ushort;
typedef unsigned long int ulong;

struct DepValueSimple
{

  friend std::ostream &operator<<(std::ostream&, const DepValueSimple&);

  const ulong sentId;
  const ulong head;
  const RelID rel;
  const ulong filler;
  const CatID lrange;
  const ulong rule;

  DepValueSimple(const ulong sentId, ulong head, RelID rel, ulong filler,
    CatID lrange, ulong rule)
    : sentId(sentId), head(head), rel(rel), filler(filler), lrange(lrange), rule(rule) {}

//  Hash hash(void) const {
//    Hash h(sentId);
//    h += head;
//    h += rel;
//    h += filler;
//    h += lrange;
//    h += rule;
//    return h;
//  }
//
//  bool equal(const DepValueSimple &other) const {
//    return other.sentId == sentId && other.head == head && other.rel == rel &&
//      other.filler == filler && other.lrange == lrange && other.rule == rule;
//  }
};

struct DepHash {
 unsigned long operator()(const DepValueSimple& d) const
 {
   unsigned long h = 0;
   h += d.sentId;
   h += d.head;
   h += d.rel;
   h += d.filler;
   h += d.lrange;
   h += d.rule;
   return std::hash<unsigned long>()(h);
 }
};


struct DepEqual {
 bool operator()(const DepValueSimple &d1, const DepValueSimple &d2) const
 {
   return d1.sentId == d2.sentId &&
          d1.head == d2.head &&
          d1.rel == d2.rel &&
          d1.filler == d2.filler &&
          d1.lrange == d2.lrange &&
          d1.rule == d2.rule;
 }
};


struct CCGDep {

  friend std::ostream &operator<<(std::ostream&, const CCGDep&);

  const ulong sentId;
  const ulong head;
  const ulong markedup;
  const ulong slot;
  const ulong arg;
  //const ulong rule_id;

  CCGDep(const ulong sentId, ulong head,
         ulong markedup, ulong slot, ulong arg)
    : sentId(sentId), head(head), markedup(markedup), slot(slot), arg(arg) {}

};


struct ccg_dep_hash {
 ulong operator() (const CCGDep& d) const {
   ulong h = 0;
   h += d.sentId;
   h += d.head;
   h += d.markedup;
   h += d.slot;
   h += d.arg;
   return std::hash<ulong>()(h);
 }
};


struct ccg_dep_equal {
 bool operator() (const CCGDep &d1, const CCGDep &d2) const
 {
   return d1.sentId == d2.sentId &&
          d1.head == d2.head &&
          d1.markedup == d2.markedup &&
          d1.slot == d2.slot &&
          d1.arg == d2.arg;
 }
};


struct dep_hash_1
{
  ulong operator() (const tuple<ulong, ulong, ulong> &d) const {
    ulong h = 0;
    h += hash<ulong>()(std::get<0>(d));
    h += hash<ulong>()(std::get<1>(d));
    h += hash<ulong>()(std::get<2>(d));
    return h;
  }
};


struct dep_eq_1 {
  bool operator() (const tuple<ulong, ulong, ulong> &x,
                   const tuple<ulong, ulong, ulong> &y) const {
    return std::get<0>(x) == std::get<0>(y) &&
           std::get<1>(x) == std::get<1>(y) &&
           std::get<2>(x) == std::get<2>(y);
  }
};


struct dep_hash_2 {
  ulong operator() (const tuple<string, ulong, ulong, ulong> &d) const {
    ulong h = 0;
    h += hash<string>()(std::get<0>(d));
    h += hash<ulong>()(std::get<1>(d));
    h += hash<ulong>()(std::get<2>(d));
    h += hash<ulong>()(std::get<3>(d));
    return h;
  }
};

struct dep_eq_2 {
  bool operator() (const tuple<string, ulong, ulong, ulong> &x,
                   const tuple<string, ulong, ulong, ulong> &y) const {
    return std::get<0>(x) == std::get<0>(y) &&
           std::get<1>(x) == std::get<1>(y) &&
           std::get<2>(x) == std::get<2>(y) &&
           std::get<3>(x) == std::get<3>(y);
  }
};


struct dep_hash_3 {
  ulong operator() (const tuple<string, ulong, ulong, string, ulong> &d) const {
    ulong h = 0;
    h += hash<string>()(std::get<0>(d));
    h += hash<ulong>()(std::get<1>(d));
    h += hash<ulong>()(std::get<2>(d));
    h += hash<string>()(std::get<3>(d));
    h += hash<ulong>()(std::get<4>(d));
    return h;
  }
};


struct dep_eq_3 {
  bool operator() (const tuple<string, ulong, ulong, string, ulong> &x,
                   const tuple<string, ulong, ulong, string, ulong> &y) const {
    return std::get<0>(x) == std::get<0>(y) &&
           std::get<1>(x) == std::get<1>(y) &&
           std::get<2>(x) == std::get<2>(y) &&
           std::get<3>(x) == std::get<3>(y) &&
           std::get<4>(x) == std::get<4>(y);
  }
};


class XF1Trainer {
public:

  unordered_map<int, int> m_voc;
  unordered_map<int, int> m_classes;

  vector<vector<vector<int> > > m_dev_words;
  vector<vector<int> > m_dev_labels;
  vector<vector<vector<int> > > m_test_words;
  vector<vector<int> > m_test_labels;
  unordered_map<int, string> m_label2rawtag_map;

  std::vector<std::vector<std::vector<std::pair<std::string, double> > > > m_all_res_dev;
  std::vector<std::vector<std::vector<std::pair<std::string, double> > > > m_all_res_test;

  size_t m_cs;
  size_t m_ws;
  size_t m_vocsize;
  size_t m_suffix_count;
  size_t m_nclasses;
  size_t m_ntokens0221;
  size_t m_ntokens00;
  size_t m_ntokens23;
  size_t m_bs;

  Sentence m_sent;
  RnnTagger m_tagger;
  CCG::Categories m_cats;
  size_t m_k;
  CCG::Parser m_parser;
  //CCG::Decoder &m_decoder;
  CCG::Printer &m_printer;
  // lexicon of c&c model
  Lexicon m_lexicon;

  double total_correct; // for testing evaluation only
  double total_returned; // for testing evaluation only
  double total_gold; // for testing evaluation only

  mat m_wx;
  mat m_wh;
  mat m_wy;

  mat m_emb;
  mat m_suf;
  mat m_cap;

  mat m_y;
  mat m_h_tm1;

  int m_de;
  int m_ds;
  int m_dc;
  size_t m_bs_xf1;
  double m_lr;
  bool m_ignore_0_f1_seq;
  double m_eps;

  vector<vector<double>> m_all_f1_vals;
  vector<vector<int>> m_all_valid_seq_ids;

  vector<vector<vector<pair<unsigned int, string> > > > m_all_kbest;
  vector<vector<vector<pair<unsigned int, string> > > > m_all_kbest_dev;

  //unordered_map<DepValueSimple, unsigned int, DepHash, DepEqual> m_dep_map;
  unordered_map<size_t, size_t> m_skip_ids_map;
  unordered_map<ulong, size_t> m_gold_dep_count;

  unordered_map<ulong, ushort> m_deps_ignore_map_rule;
  unordered_map<tuple<ulong, ulong, ulong>, ushort, dep_hash_1, dep_eq_1> m_deps_ignore_map_1;
  unordered_map<tuple<string, ulong, ulong, ulong>, ushort, dep_hash_2, dep_eq_2> m_deps_ignore_map_2;
  unordered_map<tuple<string, ulong, ulong, string, ulong>, ushort, dep_hash_3, dep_eq_3> m_deps_ignore_map_3;

  unordered_map<ulong, size_t> m_ccg_gold_dep_count_map_train;
  unordered_map<ulong, size_t> m_ccg_gold_dep_count_map_dev;

  unordered_map<string, size_t> m_unk_markedup_map;
  unordered_map<CCGDep, unsigned int, ccg_dep_hash, ccg_dep_equal>  m_ccg_dep_map_train;
  unordered_map<CCGDep, unsigned int, ccg_dep_hash, ccg_dep_equal>  m_ccg_dep_map_dev;

  vector<vector<vector<vector<int> > > > m_all_tagger_data_contextwins;
  vector<vector<vector<vector<int> > > > m_all_tagger_data_contextwins_dev;

  unordered_map<size_t, pair<vector<double>, vector<vector<pair<unsigned int, string> > > > > m_ind_f1vals_kbestlist_map;
  unordered_map<size_t, pair<size_t, size_t> > m_ind_length_tagger_ind_map;

  XF1Trainer(size_t nh, size_t vocsize, size_t nclasses, size_t cs, size_t ws,
      size_t suffix_count, size_t bs, size_t de, size_t ds, size_t dc, size_t bs_xf1,
      double lr, bool ignore_0_f1_seq,
      const string &wx, const string &wh, const string &wy,
      const string &emb, const string &suf, const string &cap,
      const unsigned int k, NLP::CCG::Parser::Config &parser_cfg,
      unsigned long load, CCG::DepsPrinter &printer,
      size_t sr_beam, const std::string srmodel, const std::vector<double> &perceptronId,
      double eps, const string &ind2raw_file);

  ~XF1Trainer(void);

  mat sigmoid(const mat &m);
  mat sigmoid_no_flow(const mat &m);
  mat sigmoid_no_flow2(const mat &m);
  mat soft_max(const mat &y);
  colvec soft_max_xf1(const colvec &y);
  mat soft_max_no_flow(const mat &y);
  colvec soft_max_xf1(const colvec &col,
      const vector<int> &valid_seq_ids);
  colvec soft_max_new(const colvec &y);

  double x_ent_multi(const mat&y, const mat&t);

  void load_dev_test_data();
  vector<vector<vector<int> > > load_words(const string &filename);
  vector<vector<int> > load_labels(const string &filename, int &ntokens);

  void sent2contextwin(const vector<vector<int> > &sent,
      vector<vector<vector<int> > > &contextwins,
      int unk_word_ind, int unk_suf_ind);

  void classify_emb(const vector<vector<vector<int> > > &contextwins,
      const vector<int> &gold_labels,
      vector<int> &sent_res, double &err);
  void classify_emb_multi(const vector<vector<vector<int> > > &contextwins,
      const vector<int> &gold_labels,
      vector<vector<pair<string, double> > > &sent_res,
      vector<double> &sent_res_1best_score, double &err);

  void mtag_candc(bool dev);
  void load_ind2rawrag_map();

  void init_ignore_rules();
  void load_dep_ignore_list(const std::string &filename);
  void load_dep_ignore_list2(const std::string &filename);
  void load_dep_ignore_list3(const std::string &filename);
  void test_gold_deps(const std::string &filename);
  void gen_kbest(const string &words, const string &labels);
  void gen_kbest_dev();

  void init_training(const string &words, const string &labels);
  void init_xf1_eval();
  void re_init_mats(const string &wx, const string &wh, const string &wy,
                           const string &emb, const string &suf, const string &cap);
  void train(IO::ReaderFactory &reader, int FMT, const size_t epoch);
  void train_rescore(IO::ReaderFactory &reader, const int FMT, const size_t epoch);
  pair<double, double> count_gold_deps(const CCG::SuperCat *sc, const ulong id);
  pair<double, double> count_gold_deps_dev(const CCG::SuperCat *sc, const ulong id);

  void load_skip_ids(const string &filename);
  double calc_f1(const double &gold, const double &total, const size_t id, bool train=true);
  void get_deps(const CCG::SuperCat *sc, const ulong id,
                      double &gold, double &total, const int FMT, bool train=true);
  void load_gold_deps(const string &filename, bool train=true);
  bool ignore_dep(const CCG::Filled *&filled);

  double bptt_multi(const vector<double> &f1_vals,
                    const vector<vector<vector<vector<int> > > > &contextwin_minibatches,
                    size_t sent_id, size_t sent_len,
                    const vector<int> &valid_seq_ids);

  double bptt_multi_multi(const vector<double> &f1_vals,
                     const vector<vector<vector<vector<int> > > > &contextwin_minibatches,
                     size_t sent_id, size_t sent_len);

  double bptt_multi_multi_tanh(const vector<double> &f1_vals,
                     const vector<vector<vector<vector<int> > > > &contextwin_minibatches,
                     size_t sent_id, size_t sent_len,
                     const vector<int> &valid_seq_ids);

  void contextwin2minibatch(const vector<vector<vector<int> > > &sent_cw,
                            vector<vector<vector<vector<int> > > > &minibatch_all);
  void calc_lh_proj_emb(const vector<vector<int> > &context_win,
                        vector<mat> &x_vals,
                        vector<mat> &lh_vals,
                        vector<mat> &lh_deriv_vals);
  void calc_lh_proj_emb_tanh(const vector<vector<int> > &context_win,
                          vector<mat> &x_vals,
                          vector<mat> &lh_vals,
                          vector<mat> &lh_deriv_vals);
  void calc_lh_proj_emb(const vector<vector<int> > &context_win,
  		                 vector<mat> &lh_vals);

  void mtag(bool dev);
  void save_model(const string dir, size_t e);

  mat grad_check(const vector<double> &f1_vals,
                 const vector<vector<vector<vector<int> > > > &contextwin_minibatches,
                 size_t sent_id, size_t sent_len);
  mat grad_check1(const vector<double> &f1_vals,
                 const vector<vector<vector<vector<int> > > > &contextwin_minibatches,
                 size_t sent_id, size_t sent_len);
  void calc_lh_proj_emb_grad_check(const vector<vector<int> > &context_win,
                                   vector<mat> &lh_vals,
                                   const mat &wx);

  mat grad_check_wy(const vector<double> &f1_vals,
                    const vector<vector<vector<vector<int> > > > &contextwin_minibatches,
                    size_t sent_id, size_t sent_len);

  void calc_lh_proj_emb_grad_check_wy(const vector<vector<int> > &context_win,
                                      vector<mat> &lh_vals, const mat &h_tm1);

  mat grad_check_wh(const vector<double> &f1_vals,
                    const vector<vector<vector<vector<int> > > > &contextwin_minibatches,
                    size_t sent_id, size_t sent_len);

  void calc_lh_proj_emb_grad_check_wh(const vector<vector<int> > &context_win,
                                      vector<mat> &lh_vals, const mat &h_tm1, const mat &wh);

  double grad_check_emb(const vector<double> &f1_vals,
      const vector<vector<vector<vector<int> > > > &contextwin_minibatches,
      size_t sent_id, size_t sent_len);

  void calc_lh_proj_emb_grad_check_emb(const vector<vector<int> > &context_win,
                                        vector<mat> &lh_vals, const mat &emb);

  double grad_check_cap(const vector<double> &f1_vals,
      const vector<vector<vector<vector<int> > > > &contextwin_minibatches,
      size_t sent_id, size_t sent_len);

  void calc_lh_proj_emb_grad_check_cap(const vector<vector<int> > &context_win,
                                        vector<mat> &lh_vals, const mat &emb);


  void load_emb_mat(mat &emb, const string &filename, int ne);

  void load_sr_model(const std::string &filename);

  void eval_dev_xf1(IO::ReaderFactory &reader_dev, const int FMT);
  void eval_dev_1best_rescore(IO::ReaderFactory &reader_dev, const int FMT);



};

} }




#endif /* SRC_INCLUDE_XF1_TRAINER__ */
