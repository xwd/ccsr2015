/* -*- Mode: C++; -*- */
// C&C NLP tools
// Copyright (c) Universities of Edinburgh, Oxford and Sydney
// Copyright (c) James R. Curran
//
// This software is covered by a non-commercial use licence.
// See LICENCE.txt for the full text of the licence.
//
// If LICENCE.txt is not included in this distribution
// please email candc@it.usyd.edu.au to obtain a copy.

#pragma once

#include "tagger/tagsetdict.h"
#include "tagger/tagdict.h"
#include "config/options.h"
#include "model/model.h"
#include "tagger/tagger.h"

namespace NLP {
namespace Tagger {

using namespace NLP::Config;
using namespace NLP::Model;

class Super: public Tagger {
public:
  class Config: public Tagger::Config {
  public:
    Op<ulong> category_cutoff;
    OpPath postags;
    OpPath posdict;
    OpPath easy_ccg_tagdict;

    Config(const OpPath *base = 0, Mode mode = DECODE,
        const std::string &name = "super",
        const std::string &desc = "super tagger config");
  };
  public:
  Super(Super::Config &config);
  Super(Super &other);

  virtual ~Super(void);

  // the set of POS tags the super tagger uses as features
  TagSet postags(void) const;
  // posdict stores the set of supertags permissible for a given POS tag
  TagSetDict posdict(void) const;
  TagDict easy_ccg_tagdict(void) const;
  TagDict tagdict(void) const;

  const Tags &get_permitted(const std::string &s, const std::string &p, ulong DICT_CUTOFF) const;
  bool exists(const std::string &word, const std::string &super, const std::string &pos, ulong DICT_CUTOFF, bool use_pos_backoff) const;
  bool word_in_tagdicts(const std::string &word, ulong DICT_CUTOFF, const bool use_backoff_tagdict) const;
  //const Tags & get_permitted_no_posdict(const std::string &word, ulong DICT_CUTOFF) const;
  protected:
  // private implementation trick
  class Impl;
};

}
}
