#ifndef SRC_INCLUDE_SCORED_IND_H_
#define SRC_INCLUDE_SCORED_IND_H_

#include <boost/functional/hash.hpp>

namespace NLP {

  struct ScoredInd {
    double score;
    int tag;

    ScoredInd(const ScoredInd &other)
      : score(other.score), tag(other.tag) {}
    ScoredInd(int tag, double score)
      : score(score), tag(tag){}
  };

  inline bool operator<(const ScoredInd &t1, const ScoredInd &t2){
    return t1.score > t2.score;
  }

  inline bool operator==(const ScoredInd &t1, const ScoredInd &t2){
    return t1.tag == t2.tag && t1.score == t2.score;
  }

  typedef std::pair<double, std::vector<unsigned int> > ScoreIndPair;

  class paircomp
  {
    bool reverse;
    public:
    paircomp(const bool& revparam=false) { reverse=revparam; }
    bool operator() (const ScoreIndPair &p1, const ScoreIndPair &p2) const {
      if (reverse) return (p1 > p2);
      else return (p1 < p2);
    }
  };

  template <typename Container>
  struct container_hash {
    std::size_t operator()(Container const& c) const {
      return boost::hash_range(c.begin(), c.end());
    }
  };


//  inline bool operator()(std::pair<double, std::vector<int> > &p1,
//                         std::pair<double, std::vector<int> > p2) {
//    return p1.first < p2.first;
//  }

}



#endif /* SRC_INCLUDE_SCORED_IND_H_ */
