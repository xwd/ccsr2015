/* -*- Mode: C++; -*- */
// C&C NLP tools
// Copyright (c) Universities of Edinburgh, Oxford and Sydney
// Copyright (c) James R. Curran
//
// This software is covered by a non-commercial use licence.
// See LICENCE.txt for the full text of the licence.
//
// If LICENCE.txt is not included in this distribution
// please email candc@it.usyd.edu.au to obtain a copy.

#pragma once

#include "parser/fixed.h"
#include "parser/atom.h"
#include "parser/feature.h"
#include "parser/varid.h"
#include "parser/category.h"

#include "parser/markedup.h"
#include "parser/gr_constraints.h"
#include "parser/gr.h"
#include "parser/relation.h"
#include "parser/relations.h"
#include "parser/canonical.h"
#include "parser/categories.h"

#include "parser/decoder.h"

// added
#include "config/options.h"
#include "config/config.h"
#include "rnn_tagger.h"

#include "cnn/nodes.h"
#include "cnn/cnn.h"
#include "cnn/training.h"
#include "cnn/timing.h"
#include "cnn/rnn.h"
#include "cnn/gru.h"
#include "cnn/lstm.h"
#include "cnn/dict.h"
#include "cnn/expr.h"


namespace NLP {

using namespace Config;

namespace CCG {

class InsideOutside;
class ShiftReduceHypothesis;

class LSTMParser {
public:
  class LSTMConfig: public Directory {
  public:
    OpPath cats;
    OpPath markedup;
    OpPath weights;
    OpPath rules;

    std::string lexicon(void) const { return derived_path(path, "lexicon"); }
    std::string features(void) const { return derived_path(path, "features"); }

    Op<ulong> maxwords;
    Op<ulong> maxsupercats;
    Op<bool> alt_markedup;
    Op<bool> seen_rules;
    Op<bool> extra_rules;
    Op<bool> question_rules;
    Op<bool> eisner_nf;
    Op<bool> partial_gold;
    Op<double> beam;
    Op<bool> allowFrag;
    Op<unsigned> de;
    Op<unsigned> dp;
    Op<unsigned> dsuper;
    Op<unsigned> daction;
    Op<unsigned> nh;
    Op<unsigned> th; // target hidden
    Op<unsigned> layers;
    Op<unsigned> head_limit;
    Op<double> pdrop;
    //Op<double> lr;
    Op<bool> use_super;
    Op<bool> use_act;
    Op<bool> use_pos;
    Op<bool> use_biqueue;
    Op<bool> use_comp;
    Op<bool> feeding;
    Op<bool> att;
    Op<bool> att_all;
    Op<bool> att_prev_context;
    Op<bool> use_rnn_super;
    Op<std::string> word_emb_file;
    Op<std::string> pos_emb_file;
    Op<std::string> super_emb_file;
    Op<bool> xf1_super;
    Op<bool> load_pretrained;
    Op<bool> xf1_dropout;
    //Op<std::string> rnn_model_base_dir;

    LSTMConfig(const OpPath *base = 0, const std::string &name = "lstm_parser", const std::string &desc = "lstm parser config");
  };
  public:

  static const ulong LOAD_WEIGHTS = 2;
  static const ulong LOAD_FEATURES = 1;
  static const ulong LOAD_NONE = 0;

  //static Pool *s_shiftReduceFeaturePool;

  LSTMParser();
  LSTMParser(cnn::Model &model, cnn::Model &t_model,
             const LSTMConfig &cfg,
             Sentence &sent,
             Categories &cats,
             const unsigned srbeam,
             const std::string &config_dir,
             const unsigned &train, const bool integrate);
  ~LSTMParser(void);

  void set_stackbuiler_pts(cnn::LSTMBuilder &queue_l2rbuilder,
                           cnn::LSTMBuilder &queue_r2lbuilder,
                           cnn::LSTMBuilder &w_stackbuilder,
                           cnn::LSTMBuilder &s_stackbuilder,
                           cnn::LSTMBuilder &a_stackbuilder,
                           cnn::LSTMBuilder &p_stackbuilder);

  void load_pos_str_ind_map(const std::string &filename);
  void load_lex_cat_dict(const std::string &filename);
  void load_gold_trees(const std::string &filename);
  void load_gold_trees_rnn_fmt(const std::string &filename);
  void Clear();
  void clear_xf1();

  const ShiftReduceHypothesis* sr_parse(const double BETA, const unsigned sent_id);
  const ShiftReduceHypothesis* sr_parse_beam_search(const unsigned k, const unsigned sent_id,
                                                    const bool oracle=false, const bool train=false);

  void load_supercat_map(const std::string &file);
  void dump_supercat_map(std::string &path);
  void load_emb_mat(const std::string &file);
  void load_emb_mat_full(const std::string &file);
  void save_weights(const unsigned epoch, const std::string &model_path);

  void init_xf1_training(bool train);
  void change_mode(const unsigned mode);
  void get_deps(const CCG::SuperCat *sc, const ulong id,
                float &gold, float &total, const int FMT, bool train);
  double get_total_sent_gold_deps(const unsigned id, const bool train);
  void xf1_grad_check(NLP::Taggers::RnnTagger &rnn_super, const unsigned k, const unsigned sent, double &total_parses, const unsigned num_threads);
  void xf1_grad_check_super(NLP::Taggers::RnnTagger &rnn_super, const unsigned k, const unsigned sent, double &total_parses);
  void lstm_tag(cnn::LSTMBuilder &t_l2rbuilder, cnn::LSTMBuilder &t_r2lbuilder, cnn::ComputationGraph& cg, const float beta);
  const ShiftReduceHypothesis* sr_parse_beam_search_xf1(cnn::LSTMBuilder &t_l2rbuilder,
                                                        cnn::LSTMBuilder &t_r2lbuilder,
                                                        cnn::ComputationGraph& cg,
                                                        const unsigned sent_id, const float BETA,
                                                        float &total_xf1, float &total_parses,
                                                        float &total_acts, float &total_correct,
                                                        const bool oracle);
  void precompute(const unsigned &k);
  void calc_tagging_acc();

  private:
  class _Impl;
  _Impl *_impl;
};

}
}
