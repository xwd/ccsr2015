/* -*- Mode: C++; -*- */
// C&C NLP tools
// Copyright (c) Universities of Edinburgh, Oxford and Sydney
// Copyright (c) James R. Curran
//
// This software is covered by a non-commercial use licence.
// See LICENCE.txt for the full text of the licence.
//
// If LICENCE.txt is not included in this distribution
// please email candc@it.usyd.edu.au to obtain a copy.


#pragma once

namespace NLP {
  namespace CCG {

    class LSTMCanonical {
    private:
      class _Impl;
      _Impl *_impl;
    public:
      LSTMCanonical(void);
      LSTMCanonical(const LSTMCanonical &other);

      ~LSTMCanonical(void);

      LSTMCanonical &operator=(LSTMCanonical &other);

      size_t size(void) const;

      const unsigned get_ind(const Cat *cat) const;
      //const Cat *operator[](const Cat *cat) const { return get(cat); }
      const unsigned operator[](const Cat *cat) const { return get_ind(cat); }

      const unsigned add(const Cat *cat, const unsigned ind);
      const unsigned add(const Cat *cat);
      const size_t dump(std::ofstream &out) const;

    };

  }
}

