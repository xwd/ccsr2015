#pragma once

#include <unordered_map>
#include "parser/ShiftReduceHypothesis.h"
#include <armadillo>
#include "lstm_parser/lstm_canonical.h"

using namespace std;

namespace NLP { namespace CCG {

void get_values(bool get_pos, const Variable *var,
                std::vector<unsigned> &heads,
                std::vector<unsigned> &pos,
                const vector<unsigned> &word_inds,
                const vector<unsigned> &pos_inds) {
  //double total = 0.0;
  const Position *const end = var->fillers + Variable::NFILLERS;
  for(const Position *p = var->fillers; p != end && *p != Variable::SENTINEL; ++p) {
    if(!*p)
      continue;
    //++total;
    assert(*p - 1 >= 0);
    assert((unsigned)(*p - 1) < word_inds.size());
    unsigned value = word_inds[*p - 1];
    //if(!value)
    //continue;
    // the above is not needed here
    // since unknown words are dealt with
    // in word_inds etc.
    heads.emplace_back(value);

    if (get_pos) {
      //assert(tagValues[*p - 1]);
      assert((unsigned)(*p - 1) < pos_inds.size());
      pos.emplace_back(pos_inds[*p - 1]);
    }
  }
  //cerr << "total: " << total << endl;
}

const unsigned canonize2ind(const unsigned &train,
                            const Cat *cat,
                            std::unordered_map<string, unsigned> &canonical) {
  string cat_str = cat->out_novar_noX_noNB_SR_str();
  auto iter = canonical.find(cat_str);
  unsigned ind;
  if (iter == canonical.end())
    ind = 1;
  else ind = iter->second;
  if (train < 2) {
    // at test time just return ind,
    // ind == 1 means the given cat was
    // not seen during training
    return ind;
  } else {
    // at training time, insert an unseen
    // cat into canonical
    // in RNN Canonical hashmap,
    // it should be made to reutrn unk_supercat_ind
    // for an unk supercat, it's currently set to 1
    if (ind == 1) {
      unsigned _ind = canonical.size() + 2;
      assert(canonical.insert({cat_str, _ind}).second);
      assert((size_t)_ind == canonical.size() + 1);
      return _ind;
    } else {
      assert(ind >= 2);
      return ind;
    }
  }
}

//const unsigned canonize2ind(const unsigned &train,
//                            const Cat *cat,
//                            LSTMCanonical &canonical) {
//  unsigned ind = canonical.get_ind(cat);
//  cerr << cat->out_novar_noX_noNB_SR_str() << " " << ind << endl;
//  if (train < 2) {
//    // at test time just return ind,
//    // ind == 1 means the given cat was
//    // not seen during training
//    return ind;
//  } else {
//    // at training time, insert an unseen
//    // cat into canonical
//    // in RNN Canonical hashmap,
//    // it should be made to reutrn unk_supercat_ind
//    // for an unk supercat, it's currently set to 1
//    if (ind == 1) {
//      const unsigned _ind = canonical.add(cat);
//      assert((size_t)_ind == canonical.size() + 1);
//      return _ind;
//    } else {
//      assert(ind >= 2);
//      return ind;
//    }
//  }
//}

} }
