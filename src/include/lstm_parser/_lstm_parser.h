/* -*- Mode: C++; -*- */
// C&C NLP tools
// Copyright (c) Universities of Edinburgh, Oxford and Sydney
// Copyright (c) James R. Curran
//
// This software is covered by a non-commercial use licence.
// See LICENCE.txt for the full text of the licence.
//
// If LICENCE.txt is not included in this distribution
// please email candc@it.usyd.edu.au to obtain a copy.

#pragma once
// this should be commented out when debugging
//#define ARMA_NO_DEBUG

#include "base.h"

#include "config/config.h"

#include "pool.h"

#include "lstm_parser/lstm_parser.h"

#include "parser/variable.h"
#include "parser/dependency.h"
#include "parser/distance.h"
#include "parser/filled.h"
#include "parser/supercat.h"
#include "parser/unify.h"
#include "rnn_parser/rnn_rule.h"
#include "parser/cell.h"
#include "parser/equiv.h"
#include "parser/treebank.h"
#include "parser/chart.h"
#include "parser/rule_instances.h"
#include "tree/attributes.h"
#include "parser/depscore.h"
#include "parser/feature_type.h"
#include "parser/feature_dist_type.h"
#include "parser/feature_cat.h"
#include "parser/feature_rule.h"
#include "parser/feature_rule_head.h"
#include "parser/feature_rule_dep.h"
#include "parser/feature_rule_dep_dist.h"
#include "parser/feature_dep.h"
#include "parser/feature_dep_dist.h"
#include "parser/feature_genrule.h"
#include "parser/inside_outside.h"

#include "parser/ShiftReduceLattice.h"
#include "parser/ShiftReduceHypothesisQueue.h"
#include "parser/ShiftReduceFeature.h"
#include "parser/ShiftReduceContext.h"
#include "parser/markedup.h"

#include "lstm_parser/lstm_canonical.h"
#include "rnn_tagger.h"
#include <queue>
#include <unordered_map>

#include "cnn/nodes.h"
#include "cnn/cnn.h"
#include "cnn/training.h"
#include "cnn/timing.h"
#include "cnn/rnn.h"
#include "cnn/gru.h"
#include "cnn/lstm.h"
#include "cnn/dict.h"
#include "cnn/expr.h"

#include <iostream>
#include <sstream>
#include <random>

#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>

#define IDX2C(i, j, ld) (((j) * (ld)) + (i)) // 0 based indexing

using namespace NLP::Tree;
using namespace cnn;
using namespace cnn::expr;
using namespace arma;


namespace NLP {
namespace CCG {

inline double add_logs(double x, double y){
  if(y <= x)
    return x + log1p(exp(y - x));
  else
    return y + log1p(exp(x - y));
}

class InsideOutside;
//class Model;

class LSTMParser::_Impl {
protected:
  void _load_features(const std::string &filename);
  void _load_weights(const std::string &filename);
  void _load_rules(const std::string &filename);

public:
  const LSTMConfig &cfg;

  std::string comment;

  Sentence &sent;
  //Sentence &sent_rnn_fmt;
  ulong nsentences;

  Categories &cats;

  Lexicon lexicon;
  DependencyAttributes dep_attrs;
  DistanceAttributes dist_attrs;
  RuleAttributes rule_attrs;

  CatFeature cat_feats;
  RuleFeature rule_feats;
  RuleHeadFeature rule_head_feats;
  RuleDepFeature rule_dep_feats;
  RuleDepDistFeature rule_dep_dist_feats;
  DepFeature dep_feats;
  DepDistFeature dep_dist_feats;
  GenruleFeature genrule_feats;

  // used only in the normal form parser when checking if a rule
  // instance is in CCGbank
  RuleInstances rule_instances;

  ulong nfeatures;
  double *weights;

  Chart chart;
  RNNRules rules;
  SuperCats results;
  std::vector<ulong> ids;

  InsideOutside inside_outside;

  const Cat *const NP;
  const Cat *const NbN;
  const Cat *const NPbNP;
  const Cat *const SbS;
  const Cat *const SfS;
  const Cat *const SbNPbSbNP;
  const Cat *const SbNPfSbNP;
  const Cat *const SfSbSfS;
  const Cat *const SbNPbSbNPbSbNPbSbNP;
  const Cat *const NPfNPbNP;

  const unsigned reduce_id_left_punct = 1;
  const unsigned reduce_id_left_punct_tc = 2;
  const unsigned reduce_id_right_punct = 3;
  const unsigned reduce_id_right_punct_tc = 4;
  const unsigned reduce_id_funny_conj = 5;
  const unsigned reduce_id_std_conj = 6;

  //Pool *m_shiftReduceFeaturePool;

  std::vector<std::vector<ShiftReduceAction*>* > m_goldTreeVec;
  std::vector<std::vector<std::vector<std::string>* >* > m_supercatsVec;
  std::vector<vector<pair<unsigned, unsigned> > > m_gold_tree_vec_rnn;

  unordered_map<string, unsigned> m_full_wordstr_emb_map;
  unordered_map<unsigned, vector<float>> m_ind_pretrained_emb_map;
  unordered_map<std::string, unsigned> m_pos_str_ind_map;
  unordered_map<unsigned, vector<float>> m_ind_pos_emb_map;
  unordered_map<unsigned, vector<float>> m_ind_super_emb_map;

  bool m_allowFragTree;
  //bool m_allowFragAndComplete;

  bool m_hasGoldAction;
  bool m_hasOutsideRule;
  ShiftReduceLattice *m_lattice;
  ulong m_beamSize;
  unsigned m_train;

  unsigned m_nclasses;
  unsigned m_vocsize;
  unsigned m_total_pos;
  unsigned m_total_super;
  unsigned m_head_limit;
  unsigned m_unary_base_count;
  unsigned m_reduce_base_count;

  cnn::LSTMBuilder *m_queue_l2rbuilder;
  cnn::LSTMBuilder *m_queue_r2lbuilder;
  cnn::LSTMBuilder *m_w_stackbuilder;
  cnn::LSTMBuilder *m_a_stackbuilder;
  cnn::LSTMBuilder *m_s_stackbuilder;
  cnn::LSTMBuilder *m_p_stackbuilder;
  cnn::LSTMBuilder *m_c_stackbuilder;

  LookupParameters* p_w;
  LookupParameters* p_pos;
  LookupParameters* p_super;
  LookupParameters* p_action;

  Parameters* p_acth;
  Parameters* p_acth_bias;
  Parameters* p_acth2act;
  Parameters* p_act;
  Parameters* p_act_bias;
  Parameters* p_att;

  Parameters* p_att_trans_w;
  Parameters* p_att_trans_s;
  Parameters* p_att_trans_a;
  Parameters* p_att_trans_p;
  Parameters* p_att_trans_q;
  Parameters* p_att_w;
  Parameters* p_att_s;
  Parameters* p_att_a;
  Parameters* p_att_p;
  Parameters* p_att_c;
  Parameters* p_w_att_trans_prev_context;
  Parameters* p_w_att_trans_prev_context2;
  Parameters* p_s_att_trans_prev_context;
  Parameters* p_s_att_trans_prev_context2;
  Parameters* p_a_att_trans_prev_context;
  Parameters* p_a_att_trans_prev_context2;
  Parameters* p_p_att_trans_prev_context;
  Parameters* p_p_att_trans_prev_context2;
  std::unordered_map<int, Expression> m_w_pointer_droppedout_state_map;
  std::unordered_map<int, Expression> m_s_pointer_droppedout_state_map;
  std::unordered_map<int, Expression> m_a_pointer_droppedout_state_map;
  std::unordered_map<int, Expression> m_p_pointer_droppedout_state_map;

  //LSTMCanonical m_cats_map;
  std::unordered_map<string, unsigned> m_cats_map_str;
  //unsigned m_cat_ind;

  unsigned m_UNK_ALPHNUM_LOWER_IND;
  unsigned m_UNK_ALPHNUM_UPPER_IND;
  unsigned m_UNK_NON_ALPHNUM_IND;
  unsigned m_EMPTY_WORD_IND;
  unsigned m_PAD_WORD_IND;
  unsigned m_PAD_POS_IND;
  unsigned m_UNK_POS_IND;
  unsigned m_EMPTY_POS_IND;
  unsigned m_PAD_SUPER_IND;
  unsigned m_UNK_SUPER_IND;
  unsigned m_SHIFT_IND;
  unsigned m_UNARY_IND;
  unsigned m_REDUCE_IND;
  unsigned m_PAD_ACTION_IND;

  // tagger
  unsigned m_T_UNKNWON_SUPERCAT_IND;
  unsigned m_T_UNK_ALPHNUM_LOWER_IND;
  unsigned m_T_UNK_ALPHNUM_UPPER_IND;
  unsigned m_T_UNK_NON_ALPHNUM_IND;
  unsigned m_T_UNK_SUFFIX_IND;

  unsigned m_t_layers;
  unsigned m_t_pretrained_dim;
  unsigned m_t_suf_dim;
  unsigned m_t_hidden_dim;
  unsigned m_t_dwin;
  unsigned m_t_tag_size;
  unsigned m_t_vocab_size;
  double m_tpdrop;
  double m_dcorr;
  double m_dtags;

  unordered_map<string, unsigned> m_t_cats_map_str;
  unordered_map<unsigned, string> m_label2rawtag_map;
  unordered_map<string, size_t> m_suf_str_ind_map;

  LookupParameters* pt_w;
  LookupParameters* pt_suf;
  Parameters* pt_l2th;
  Parameters* pt_r2th;
  Parameters* pt_thbias;
  Parameters* pt_att_w;
  Parameters* pt_l2ah;
  Parameters* pt_r2ah;
  Parameters* pt_c2ah;
  Parameters* pt_c2t;
  Parameters* pt_th2t;
  Parameters* pt_tbias;

  // for evaluating mtagging
  vector<MultiRaws> m_all_res;
  vector<vector<size_t>> m_all_gold_labels;

  void calc_tagging_acc();
  Expression build_tagging_graph(LSTMBuilder &l2rbuilder,
                                 LSTMBuilder &r2lbuilder,
                                 vector<vector<vector<unsigned>>> &sent_contextwins,
                                 vector<unsigned> &tags,
                                 ComputationGraph& cg,
                                 const bool eval, const float &BETA);
  Expression build_local_attention_tagging_graph(LSTMBuilder &l2rbuilder,
                                                 LSTMBuilder &r2lbuilder,
                                                 vector<vector<vector<unsigned>>> &sent_contextwins,
                                                 vector<unsigned>& tags,
                                                 ComputationGraph& cg,
                                                 bool test, const float &beta, const bool debug=false);

  //  unsigned m_T_UNK_ALPHNUM_LOWER_IND;
  //  unsigned m_T_UNK_ALPHNUM_UPPER_IND;
  //  unsigned m_T_UNK_NON_ALPHNUM_IND;

  int m_total_reduce_count;
  // for sanity check only
  unordered_map<unsigned, unsigned> m_emb_ind_map;
  string m_config_dir;
//  double m_avg_unique_prev_hypo_count; // per sent
//  double m_total_unique_prev_hypo_count;


  std::unordered_map<string, unsigned> m_lex_cat_ind_map;
  std::unordered_map<unsigned, string> m_lex_ind_cat_map;

  // precomputation trick related
  // key: word_feat_ind val: {emb_ind, count}
  unordered_map<unsigned, unordered_map<unsigned, unsigned>> m_word_feat_emb_ind_map;
  unordered_map<unsigned, unordered_map<unsigned, unsigned>> m_suf_feat_emb_ind_map;
  unordered_map<unsigned, unordered_map<unsigned, unsigned>> m_super_feat_emb_ind_map;

//  unordered_map<unsigned, unordered_map<unsigned, mat>> m_word_emb_lh_cache_val;
//  unordered_map<unsigned, unordered_map<unsigned, mat>> m_suf_emb_lh_cache_val;
  //vector<unordered_map<unsigned, mat>> m_super_emb_lh_cache_val;

  struct KeyHash {
    unsigned operator() (const ShiftReduceAction& k) const {
      Hash h = k.GetSuperCat()->ehash();
      h += k.GetSuperCat()->m_left_ind;
      h += k.GetSuperCat()->m_right_ind;
      h += k.GetAction();
      return (unsigned)h.value();
    }
  };

  struct KeyEqual {
    bool operator() (const ShiftReduceAction& k1, const ShiftReduceAction& k2) const {
      return k1.GetAction() == k2.GetAction() &&
          k1.GetSuperCat()->m_left_ind == k2.GetSuperCat()->m_left_ind &&
          k1.GetSuperCat()->m_right_ind == k2.GetSuperCat()->m_right_ind &&
          eqSR(k1.GetCat(), k2.GetCat());
    }
  };

  class Hypo_comp {
    bool reverse;
  public:
    Hypo_comp(const bool& reve=false)
  {reverse=reve;}
    bool operator() (ShiftReduceHypothesis *&h1, ShiftReduceHypothesis *&h2) const
    {
      if (reverse) return (h1->GetTotalScore()/h1->GetTotalActionCount()
          > h2->GetTotalScore()/h2->GetTotalActionCount());
      else return (h1->GetTotalScore()/h1->GetTotalActionCount()
          < h2->GetTotalScore()/h2->GetTotalActionCount());
    }
  };

  std::unordered_map<ShiftReduceAction, unsigned, KeyHash, KeyEqual> m_gold_equiv_map;

  _Impl(cnn::Model &model, cnn::Model &t_model,
        const LSTMConfig &cfg,
        Sentence &sent,
        Categories &cats,
        const unsigned srbeam,
        const string& config_dir,
        const unsigned &train,
        const bool &integrate);
  ~_Impl(void) {
    std::cerr << "_parser.h destructor" << std::endl;
    delete [] weights;
    chart.reset();
    //chart.~Chart();
    //delete m_shiftReduceFeatures;
    //delete m_lattice;
    //m_shiftReducePool->clear();
  };

  void set_stackbuiler_pts(cnn::LSTMBuilder &queue_l2rbuilder,
                           cnn::LSTMBuilder &queue_r2lbuilder,
                           cnn::LSTMBuilder &w_stackbuilder,
                           cnn::LSTMBuilder &s_stackbuilder,
                           cnn::LSTMBuilder &a_stackbuilder,
                           cnn::LSTMBuilder &p_stackbuilder);
  void create_n_push_goldact_tuple(ShiftReduceAction *&gold_action,
                                   const unsigned i, ShiftReduceHypothesis *&hypo,
                                   const unsigned NWORDS, const unsigned j, const pair<unsigned, unsigned> &gold_action_rnn_fmt,
                                   HypothesisPQueue &hypoQueue, bool &found_gold_action,
                                   unsigned &gold_action_class,
                                   unsigned &total_shift_count);
  void load_lex_cat_dict(const string &raw_cats_from_markedup);

  void sr_parse_train(const double BETA, const unsigned sent_id);
  const ShiftReduceHypothesis* sr_parse(const double BETA, unsigned sent_id);
  const ShiftReduceHypothesis* sr_parse_beam_search(const unsigned k, const unsigned sent_id,
                                                    const bool oracle=false, const bool train=false);
  const ShiftReduceHypothesis* sr_parse_max_margin(const double BETA, const unsigned sent_id);

  void construct_hypo(ComputationGraph &cg,
                      const vector<unsigned> &word_inds,
                      const vector<unsigned> &pos_inds,
                      HypothesisPQueue &hypoQueue,
                      const ShiftReduceHypothesis *&bestHypo, const ShiftReduceHypothesis *&goldHypo,
                      ShiftReduceAction *goldAction, const bool goldFinished);

  void count_gold_actions(ShiftReduceHypothesis *&hypo,
                          ShiftReduceAction &action);
  void _action(ComputationGraph &cg, const ShiftReduceHypothesis* &hypo, unsigned action,
               const SuperCat *supercat, const double totalScore, const vector<unsigned> &word_inds,
               const vector<unsigned> &pos_inds, Expression &act_log_prob,
               const unsigned action_id = 0, const double action_score=0.0);
  void _add_lex(const Cat *cat, const SuperCat *sc,
                bool replace, RuleID ruleid,
                std::vector<SuperCat *> &tmp,
                const unsigned tc_id = 0);
  void unary_tc(const SuperCat *sc, bool qu_parsing, std::vector<SuperCat *> &tmp);
  void unary_tr(const SuperCat *supercat, std::vector<SuperCat *> &tmp);

  //static Pool *GetFeaturePool() { return m_shiftReduceFeaturePool; }
  void load_gold_trees(const std::string &filename);
  void load_gold_trees_rnn_fmt(const std::string &filename);

  // clear cats etc., hashtables between iterations
  void Clear();
  void clear_xf1();

  void raws2words(const Raws &raw, Words &words) const;
  void words2word_inds(const vector<std::string> &raw, const vector<std::string> &pos,
                       std::vector<unsigned> &word_inds_vec,
                       std::vector<unsigned> &suf_inds_vec) const;

  void pos2pos_inds(const vector<std::string> &raw,
                    std::vector<unsigned> &pos_inds_vec) const;
  void suf2suf_inds(const vector<std::string> &raw,
                    std::vector<unsigned> &pos_inds_vec) const;

  void suf_cap2suf_cap_inds(const vector<std::string> &raw,
                            std::vector<unsigned> &suf_inds_vec,
                            std::vector<unsigned> &cap_inds_vec) const;

  const ShiftReduceHypothesis* sr_parser_train(const double BETA, const unsigned sent_id);
  void update_context(ComputationGraph &cg,
                      const ShiftReduceHypothesis *&hypo,
                      unsigned action,
                      const SuperCat *&sc,
                      const vector<unsigned> &word_inds,
                      const vector<unsigned> &pos_inds);
  void gen_context(ComputationGraph &cg,
                   ShiftReduceHypothesis* &hypo,
                   vector<cnn::RNNPointer> &state_ps,
                   unsigned type,
                   Expression &i_att_trans,
                   Expression &i_att_trans_q,
                   Expression &i_att,
                   Expression &queue_state,
                   Expression &i_pre_att_trans,
                   Expression &i_pre_att_trans2,
                   const Expression &prev_context,
                   Expression &context,
                   std::unordered_map<int, Expression> &state_map);

  //void train(const unsigned total_epochs, const string &model_path);
//  pair<unsigned, double> score_context(const vector<vector<unsigned> > &context,
//                const vector<unsigned> &feasible_y_vals);
//  mat score_context_beam_search(const vector<vector<unsigned>> &context,
//                                const vector<unsigned> &feasible_y_vals,
//                                mat &output_vals_feasible,
//                                const mat &prev_hidden_states,
//                                mat &new_hidden_states);
//  void score_context_beam_search_xf1(const vector<vector<unsigned>> &context,
//                                     const vector<unsigned> &feasible_y_vals,
//                                     mat &output_vals_feasible,
//                                     const mat &prev_hidden_states,
//                                     mat &new_hidden_states,
//                                     colvec &converted_context_vec);
  void compose_word_vec(const int left_ind, const int right_ind,
                        SuperCat *&supercat);
  void load_suf_str_ind_map(const string &file);
  void load_emb_mat(const string &filename);
  void load_pos_emb_mat(const string &filename);
  void load_super_emb_mat(const string &filename);
  void re_init_mats(const string &wx, const string &wh, const string &wy,
                    const string &emb, const string &suf,
                    const string &super_emb, const string &comp_mat);

  void dump_supercat_map(const std::string &path);
  void load_supercat_map(const string &file, bool init=false, bool tagger=false);
  void load_pos_str_ind_map(const string &file);

  void clean_str(string &word, vector<string> &ret) const;
  string get_suf(const string &word) const;
  vector<unsigned> word2vec(const string &word) const;
  vector<unsigned> word2vec_t(string &word) const;
  pair<vector<vector<unsigned>>, vector<unsigned>> convert_sent(const bool debug=false);
  void sent2contextwin(const vector<vector<unsigned>> &sent, vector<vector<vector<unsigned>>> &contextwins);
  bool is_lower(const string &word) const;
  bool is_alphnum(const string &word) const;
  void replace_all_substrs(string &str, const string &substr,  const string &re) const;
  void save_weights(const unsigned epoch, const string &model_path);

  // unit tests
  void test_contextwin2minibatch();
  void test_labels2minibatch();

  // XF1 training related
  struct CCGDep {
    friend std::ostream &operator<<(std::ostream&, const CCGDep&);
    const ulong sentId;
    const ulong head;
    const ulong markedup;
    const ulong slot;
    const ulong arg;
    //const ulong rule_id;

    CCGDep(const ulong sentId, ulong head,
           ulong markedup, ulong slot, ulong arg)
    : sentId(sentId), head(head), markedup(markedup), slot(slot), arg(arg) {}

  };

  struct dep_hash_1
  {
    ulong operator() (const tuple<ulong, ulong, ulong> &d) const {
      ulong h = 0;
      h += hash<ulong>()(std::get<0>(d));
      h += hash<ulong>()(std::get<1>(d));
      h += hash<ulong>()(std::get<2>(d));
      return h;
    }
  };

  struct dep_eq_1 {
    bool operator() (const tuple<ulong, ulong, ulong> &x,
                     const tuple<ulong, ulong, ulong> &y) const {
      return std::get<0>(x) == std::get<0>(y) &&
          std::get<1>(x) == std::get<1>(y) &&
          std::get<2>(x) == std::get<2>(y);
    }
  };

  struct dep_hash_2 {
    ulong operator() (const tuple<string, ulong, ulong, ulong> &d) const {
      ulong h = 0;
      h += hash<string>()(std::get<0>(d));
      h += hash<ulong>()(std::get<1>(d));
      h += hash<ulong>()(std::get<2>(d));
      h += hash<ulong>()(std::get<3>(d));
      return h;
    }
  };

  struct dep_eq_2 {
    bool operator() (const tuple<string, ulong, ulong, ulong> &x,
                     const tuple<string, ulong, ulong, ulong> &y) const {
      return std::get<0>(x) == std::get<0>(y) &&
          std::get<1>(x) == std::get<1>(y) &&
          std::get<2>(x) == std::get<2>(y) &&
          std::get<3>(x) == std::get<3>(y);
    }
  };

  struct dep_hash_3 {
    ulong operator() (const tuple<string, ulong, ulong, string, ulong> &d) const {
      ulong h = 0;
      h += hash<string>()(std::get<0>(d));
      h += hash<ulong>()(std::get<1>(d));
      h += hash<ulong>()(std::get<2>(d));
      h += hash<string>()(std::get<3>(d));
      h += hash<ulong>()(std::get<4>(d));
      return h;
    }
  };

  struct dep_eq_3 {
    bool operator() (const tuple<string, ulong, ulong, string, ulong> &x,
                     const tuple<string, ulong, ulong, string, ulong> &y) const {
      return std::get<0>(x) == std::get<0>(y) &&
          std::get<1>(x) == std::get<1>(y) &&
          std::get<2>(x) == std::get<2>(y) &&
          std::get<3>(x) == std::get<3>(y) &&
          std::get<4>(x) == std::get<4>(y);
    }
  };

  struct ccg_dep_equal {
    bool operator() (const CCGDep &d1, const CCGDep &d2) const
    {
      return d1.sentId == d2.sentId &&
          d1.head == d2.head &&
          d1.markedup == d2.markedup &&
          d1.slot == d2.slot &&
          d1.arg == d2.arg;
    }
  };

  struct ccg_dep_hash {
    ulong operator() (const CCGDep& d) const {
      ulong h = 0;
      h += d.sentId;
      h += d.head;
      h += d.markedup;
      h += d.slot;
      h += d.arg;
      return std::hash<ulong>()(h);
    }
  };

  unordered_map<ulong, ushort> m_deps_ignore_map_rule;
  unordered_map<tuple<ulong, ulong, ulong>, ushort, dep_hash_1, dep_eq_1> m_deps_ignore_map_1;
  unordered_map<tuple<string, ulong, ulong, ulong>, ushort, dep_hash_2, dep_eq_2> m_deps_ignore_map_2;
  unordered_map<tuple<string, ulong, ulong, string, ulong>, ushort, dep_hash_3, dep_eq_3> m_deps_ignore_map_3;

  unordered_map<ulong, unsigned> m_ccg_gold_dep_count_map_train;
  unordered_map<ulong, unsigned> m_ccg_gold_dep_count_map_dev;

  unordered_map<string, unsigned> m_unk_markedup_map;
  unordered_map<CCGDep, unsigned int, ccg_dep_hash, ccg_dep_equal>  m_ccg_dep_map_train;
  unordered_map<CCGDep, unsigned int, ccg_dep_hash, ccg_dep_equal>  m_ccg_dep_map_dev;

  void init_xf1_training(bool train);
  void load_gold_deps(const string &filename, bool train);
  void init_ignore_rules();
  void load_dep_ignore_list(const std::string &filename);
  void load_dep_ignore_list2(const std::string &filename);
  void load_dep_ignore_list3(const std::string &filename);
  pair<float, float> count_gold_deps(const SuperCat *sc, const ulong id, bool train=true);
  bool ignore_dep(const CCG::Filled *&filled);
  float calc_f1(const ShiftReduceHypothesis *&output, const unsigned id, bool train);
  void test_internal_f1_calc(unsigned sent_id);
  void get_deps(const CCG::SuperCat *sc, const ulong id,
                float &gold, float &total, const int FMT, bool train);
  float get_total_sent_gold_deps(const unsigned id, const bool train);
  const ShiftReduceHypothesis* sr_parse_train_xf1(const double BETA, const unsigned sent_id);
  const ShiftReduceHypothesis* sr_parse_beam_search_xf1(cnn::LSTMBuilder &t_l2rbuilder,
                                                        cnn::LSTMBuilder &t_r2lbuilder,
                                                        ComputationGraph& cg,
                                                        const unsigned sent_id, const float BETA,
                                                        float &total_xf1, float &total_parses,
                                                        float &total_acts, float &total_correct,
                                                        const bool oracle);
  void lstm_tag(cnn::LSTMBuilder &t_l2rbuilder, cnn::LSTMBuilder &t_r2lbuilder, ComputationGraph& cg, const float beta);
  fmat soft_max(const fmat &y);
  void xf1_grad_check(NLP::Taggers::RnnTagger &rnn_super, const unsigned k, const unsigned sent_id,
                      double &total_parses, const unsigned num_threads);
  void xf1_grad_check_super(NLP::Taggers::RnnTagger &rnn_super,
                                         const unsigned k, const unsigned sent_id,
                                         double &total_parses);
  void mark_nodes(const ShiftReduceHypothesis *&hypo, const unsigned &ind);

  void precompute_all_positions(unordered_map<unsigned, unordered_map<unsigned, unsigned>> &map,
                                const unsigned &k,
                                const bool word);
  void precompute_position(unordered_map<unsigned, unsigned> &map,
                           const unsigned &k,
                           vector<unsigned> &top_emb_inds);
  void precompute(const unsigned &k);
  void sr_parse_train(ComputationGraph* hg, const double BETA, const unsigned sent_id);

  void display_value(const Expression &source, ComputationGraph &cg, string what_to_say) {
      const Tensor &a = cg.get_value(source.i);

      cnn::real I = a.d.cols();
      cnn::real J = a.d.rows();
      cerr << what_to_say << " rows: " << J << " cols: " << I << endl;
      if (what_to_say.size() > 0)
          cout << what_to_say << endl;
      for (unsigned j = 0; j < J; ++j) {
          for (unsigned i = 0; i < I; ++i) {
              cnn::real v = TensorTools::AccessElement(a, IDX2C(j,i, J));
              std::cout << v << ' ';
          }
          std::cout << endl;
      }
  }

};

}
}

