/* -*- Mode: C++; -*- */
// C&C NLP tools
// Copyright (c) Universities of Edinburgh, Oxford and Sydney
// Copyright (c) James R. Curran
//
// This software is covered by a non-commercial use licence.
// See LICENCE.txt for the full text of the licence.
//
// If LICENCE.txt is not included in this distribution
// please email candc@it.usyd.edu.au to obtain a copy.

#pragma once

#include "base.h"

#include "utils.h"

#include "hashtable/base.h"

#include "share.h"

#include "parser/fixed.h"
#include "parser/atom.h"
#include "parser/feature.h"
#include "parser/varid.h"
#include "parser/category.h"
#include "parser/gr_constraints.h"
#include "parser/gr.h"
#include "parser/relation.h"
#include "parser/variable.h"
#include "parser/dependency.h"
#include "parser/distance.h"
#include "parser/filled.h"
#include "parser/relations.h"
#include "parser/supercat.h"
#include "parser/unify.h"

#include "tree/attributes.h"
#include <iostream>
#include "parser/integration.h"

namespace NLP {

using namespace HashTable;
using namespace CCG;

namespace Tree {

template <class Value>
class AttribEntry {
public:
  const Hash hash;
  ulong id;
  double weight;
  Value value;
  AttribEntry<Value> *next;

  // double used to prevent overflow
  double m_perceptronTotalWeight;
  double m_perceptronWeight;

  double m_lastUpdateIter;
  double m_lastUpdateSent;

  // new constructor
  AttribEntry(const Value &value, double weightTotal, double weight, Hash hash, AttribEntry<Value> *next):
    hash(hash), id(0), weight(0.0), value(value), next(next),
    m_perceptronTotalWeight(weightTotal), m_perceptronWeight(weight), m_lastUpdateIter(0), m_lastUpdateSent(0){}

  // original with the last two elements in the init list added
  AttribEntry(const Value &value, ulong id, Hash hash, AttribEntry<Value> *next):
    hash(hash), id(id), weight(0.0), value(value), next(next), m_perceptronTotalWeight(0.0), m_perceptronWeight(0.0),
    m_lastUpdateIter(0), m_lastUpdateSent(0){}

  ~AttribEntry(void){ /* do nothing */ }

  void *operator new(size_t size, Pool *pool){
    return (void *)pool->alloc(size);
  }
  void operator delete(void *, Pool *){ /* do nothing */ }

  static AttribEntry<Value> *createSR(Pool *pool, const Value &value, double weightTotal, double weight,
      NLP::Hash hash, AttribEntry<Value> *next)
          {
    AttribEntry *entry = new (pool) AttribEntry<Value>(value, weightTotal, weight, hash, next);
    return entry;
          }

  static AttribEntry<Value> *create(Pool *pool, const Value &value, ulong index,
      NLP::Hash hash, AttribEntry<Value> *next){
    AttribEntry *entry = new (pool) AttribEntry<Value>(value, index, hash, next);
    return entry;
  }

  bool equal(const Value &v, const NLP::Hash h){
    return hash == h && value.equal(v);
  }

  AttribEntry<Value> *find(const Value &v, const NLP::Hash h){
    for(AttribEntry<Value> *l = this; l; l = l->next)
      if(l->equal(v, h))
        return l;

    return 0;
  }

  ulong find_id(const Value &v, const NLP::Hash h){
    for(AttribEntry<Value> *l = this; l; l = l->next)
      if(l->equal(v, h))
        return l->id;

    return 0;
  }

  double find_weight(const Value &v, const NLP::Hash h){
    for(AttribEntry<Value> *l = this; l; l = l->next)
      if(l->equal(v, h))
        return l->weight;

    return 0.0;
  }

  void set_weights(const double *weights){
    weight = weights[id - 1];
    if(next)
      next->set_weights(weights);
  }

  ulong nchained(void){
    return next ? next->nchained() + 1 : 1;
  }

  double find_weightSR(const Value &v, const NLP::Hash h){
    for(AttribEntry<Value> *l = this; l; l = l->next)
      if(l->equal(v, h))
      {
        if (Integration::s_train)
          return l->m_perceptronWeight;
        else
          return l->m_perceptronTotalWeight;
      }

    return 0.0;
  }

  // last sent, last iter update (part of lazy update)

  void PerceptronLastUpdate()
  {
    double currentSent = Integration::s_perceptronId[0];
    double totalSent = Integration::s_perceptronId[1];
    /*  size_t currentIter = ids[2];
	  size_t totalIter = ids[3];*/

    assert(currentSent == totalSent);
    if (m_lastUpdateSent < totalSent)
    {
      m_perceptronTotalWeight += m_perceptronWeight * (currentSent - m_lastUpdateSent);
    }
  }


  void UpdatePerceptronTotalAndWeight(bool correct, const Value &v, const NLP::Hash h)
  {
    //AttribEntry<Value> *attribEntry = this;
    double increment = 0.0;
    if (correct) increment = 1.0;
    else increment = -1.0;

    double currentSent = Integration::s_perceptronId[0];
    //size_t totalSent = ids[1];
    //size_t currentIter = ids[2];
    //size_t totalIter = ids[3];

    // this feature has been updated previously
    if (m_lastUpdateSent != 0 && m_lastUpdateSent != currentSent)
    {
      // update total inactive count of this feature since last update (part of lazy update)
      /* attribEntry->m_perceptronTotalWeight += m_perceptronWeight *
		      (currentIter*totalSent + currentSent - m_lastUpdateIter*totalSent - m_lastUpdateSent);*/
      m_perceptronTotalWeight += m_perceptronWeight * (currentSent - m_lastUpdateSent);
    }
    // update current iteration weight (part of lazy update)
    m_perceptronWeight += increment;
    m_perceptronTotalWeight += increment;

    // mark this update (part of lazy update)
    //m_lastUpdateIter = currentIter; // t
    m_lastUpdateSent = currentSent; // i
  }

  /*      void SaveWeights(std::ofstream &out)
      {
	  out << value << " " <<  m_perceptronTotalWeight << " " << m_perceptronWeight << std::endl;
	  if (next)
	      next->SaveWeights(out);
      }*/

  void SaveWeights(std::ofstream &out)
  {
    for(AttribEntry<Value> *l = this; l; l = l->next)
    {
      l->PerceptronLastUpdate();
      out << l->value << " " <<  l->m_perceptronTotalWeight << " " << l->m_perceptronWeight << std::endl;
    }
  }
};

// this should really inherit from Base but there is a bug in gcc 4.0
// which stops this from compiling if you do (it works in gcc 3.2.2)
template <class Value, ulong NBUCKETS, ulong SPOOL>
class AttributesImpl {
public:
  typedef AttribEntry<Value> Entry;
  typedef std::vector<Entry *> Entries;

  const std::string name;
  size_t size;
protected:
  Pool *const pool_;
  const bool shared_;

  static const ulong NBUCKETS_ = NBUCKETS;
  static const ulong PENTRIES_ = SPOOL;

  Entry *buckets_[NBUCKETS];
public:
  AttributesImpl(const std::string &name, Pool *pool = 0)
: name(name), size(0),
  pool_(pool ? pool : new Pool(SPOOL)),
  shared_(pool != 0){
    memset(buckets_, 0, sizeof(buckets_));
  }

  virtual ~AttributesImpl(void){
    if(!shared_)
      delete pool_;
  }

  Pool *pool(void) { return pool_; }


  Entry *insert(const Value &value, ulong id, const Hash hash, const ulong bucket){
    Entry *entry = Entry::create(pool_, value, id, hash, buckets_[bucket]);
    buckets_[bucket] = entry;
    ++size;
    return entry;
  }

  // the next entry of new is set to front element, buckets_[bucket]
                                                             // front element is then set to the new entry
  Entry *insertSR(const Value &value, double weightTotal, double weight, const Hash hash, const ulong bucket)
  {
    Entry *entry = Entry::createSR(pool_, value, weightTotal, weight, hash, buckets_[bucket]);
    buckets_[bucket] = entry;
    ++size;
    return entry;
  }

  void insert(const Value &value, ulong id){
    const Hash hash = value.hash();
    insert(value, id, hash, hash % NBUCKETS_);
  }

  void insertSR(const Value &value, double weightTotal, double weight)
  {
    const Hash hash = value.hash();
    insertSR(value, weightTotal, weight, hash, hash % NBUCKETS_);
  }

  Entry *add(const Value &value){
    const Hash hash = value.hash();
    ulong bucket = hash % NBUCKETS;
    Entry *entry = buckets_[bucket]->find(value, hash);
    if(entry)
      return entry;

    return insert(value, 0, hash, bucket);
  }

  ulong find_id(const Value &value) const {
    Hash hash = value.hash();
    return buckets_[hash % NBUCKETS_]->find_id(value, hash);
  }

  double find_weight(const Value &value) const {
    Hash hash = value.hash();
    return buckets_[hash % NBUCKETS_]->find_weight(value, hash);
  }

  double find_weightSR(const Value &value) const {
    Hash hash = value.hash();
    return buckets_[hash % NBUCKETS_]->find_weightSR(value, hash);
  }

  void set_weights(const double *weights){
    for(ulong i = 0; i < NBUCKETS_; ++i)
      if(buckets_[i])
        buckets_[i]->set_weights(weights);
  }

  void PerceptronUpdate(bool correct, const Value &value)
  {
    Hash hash = value.hash();
    ulong bucket = hash % NBUCKETS;
    Entry *entry = buckets_[bucket]->find(value, hash);
    if (entry)
    {
      entry->UpdatePerceptronTotalAndWeight(correct, value, hash);
    }
    else
    {
      entry = insertSR(value, 0.0, 0.0, hash, hash % NBUCKETS_);
      entry->UpdatePerceptronTotalAndWeight(correct, value, hash);
    }
  }

  void SaveWeights(std::ofstream &out)
  {
    if (size > 0)
    {
      std::cerr << "### hashtable: " << name << " " << "load: " << double(size)/double(NBUCKETS) << std::endl;
      std::cerr << "### size: " << size << " buckets: " << NBUCKETS << std::endl;
      for (ulong i = 0; i < NBUCKETS_; ++i)
      {
        if(buckets_[i])
        {
          buckets_[i]->SaveWeights(out);
        }
      }
    }
  }

};

}
}

