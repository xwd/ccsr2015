#ifndef SRC_LIB_RNN_TAGGER_RNN_POS_H_
#define SRC_LIB_RNN_TAGGER_RNN_POS_H_

#include <armadillo>
#include <unordered_map>
#include "scored_ind.h"
#include "io/reader.h"
#include "io/reader_factory.h"
#include "tagger/super.h"

using namespace std;
using namespace arma;
using namespace NLP::Tagger;

namespace NLP {
namespace Taggers {

// this is an indicator for supercats not in the 425 classes in wsj02-21
// const static int UNKNWON_SUPERCAT_IND = -1;

class RnnPos {
public:

  unordered_map<size_t, size_t> m_voc;
  unordered_map<size_t, size_t> m_classes;

//  vector<vector<vector<size_t> > > m_train_words;
//  vector<vector<size_t> > m_train_labels;
//  vector<vector<vector<size_t> > > m_dev_words;
//  vector<vector<size_t> > m_dev_labels;
//  vector<vector<vector<size_t> > > m_test_words;
//  vector<vector<size_t> > m_test_labels;
  unordered_map<size_t, string> m_label2rawtag_map;

  unordered_map<string, size_t> m_full_wordstr_emb_map;
  unordered_map<string, size_t> m_suf_str_ind_map;
  unordered_map<string, size_t> m_lex_cat_str_ind_map;

  size_t m_de;
  size_t m_ds;
  size_t m_dc;
  size_t m_cs;
  size_t m_nh;
  size_t m_bs;
  size_t m_vocsize;
  size_t m_suffix_count;
  size_t m_nclasses;
  size_t m_ntokens0221;
  size_t m_ntokens00;
  size_t m_ntokens23;

  size_t m_WORD_PAD_IND;
  //size_t m_UNKNWON_SUPERCAT_IND;
  size_t m_UNK_ALPHNUM_LOWER_IND;
  size_t m_UNK_ALPHNUM_UPPER_IND;
  size_t m_UNK_NON_ALPHNUM_IND;
  size_t m_UNK_SUFFIX_IND;

  string m_model_dir;

  bool m_lowercase_words;

  mat m_wx;
  mat m_wh;
  mat m_wy;

  mat m_emb;
  mat m_suf;
  mat m_cap;

  mat m_y;
  mat m_h_tm1;

  size_t m_nepoch;
  size_t m_out_fmt;
  bool m_dropout;
  double m_dropout_success_prob;
  bool m_verbose;
  double m_lr;
  mat m_dropout_vec;
  bool m_use_candc_tagdicts;
  size_t m_candc_tagdict_cutoff;
  bool m_candc_tagdict_pos_backoff;
  //Super m_super;

//  RnnTagger(int nh, int vocsize, int nclasses, int cs, int ws, int suffix_count,
//            const string &wx, const string &wh, const string &wy,
//            const string &emb, const string &suf, const string &cap);

  RnnPos(size_t de, size_t ds, size_t dc,
            size_t cs, size_t nh, size_t bs,
            const string &all_untouched_embeddings,
            const string &suf_str_ind_file,
            const string &model_dir,
            bool lower_case_words,
            size_t nepochs,
            bool dropout,
            double dropout_success_prob,
            bool verbose,
            double lr);

  RnnPos(const string &model_dir,
            const string &all_untouched_embeddings,
            const string &suf_str_ind_file,
            size_t output_fmt,
            double dropout_success_prob,
            bool dropout);

  ~RnnPos(void);

  void train(NLP::IO::ReaderFactory &reader, IO::ReaderFactory &reader_dev);

  mat sigmoid(const mat &m);
  mat soft_max(const mat &y);
  double x_ent_multi(const mat&y, const mat&t);

  void load_train_data(const string &words, const string &labels);
  void load_dev_test_data();
  vector<vector<vector<int> > > load_words(const string &filename);
  vector<vector<int> > load_labels(const string &filename, size_t &ntokens);

  void sent2contextwin(const vector<vector<size_t> > &sent,
                       vector<vector<vector<size_t> > > &contextwins);

  void classify(const vector<vector<vector<size_t> > > &contextwins,
                const vector<size_t> &gold_labels,
                vector<size_t> &sent_res, double &err);
//  void classify_emb_multi(const vector<vector<vector<size_t> > > &contextwins,
//                          const vector<size_t> &gold_labels,
//                          vector<vector<pair<string, double> > > &sent_res,
//                          vector<double> &sent_res_1best_score, double &err);
//  void classify_emb_multi_0221(const vector<vector<vector<int> > > &contextwins,
//                               const vector<size_t> &gold_labels,
//                               vector<vector<ScoredInd> > &sent_res_ind,
//                               vector<double> &sent_res_1best_score, double &err);

  double eval_super(bool dev);
  pair<mat, mat> calc_lh_proj_dropout(size_t step, const vector<vector<size_t> > &word_vecs,
                                      mat *x_vals, mat *x_mask_vals);

  void load_ind2rawtag_map();

  vector<vector<pair<unsigned int, string> > >
  gen_kbest(const vector<vector<ScoredInd> > &sent_res_ind_sorted,
            const unsigned int k);
  double get_total_score(const vector<vector<ScoredInd> > &sent_res_ind_sorted,
      const vector<unsigned int> &ind_vec);

  void re_init_mats(const string &wx, const string &wh, const string &wy,
                    const string &emb, const string &suf, const string &cap);

  void load_suf_str_ind_map(const string &file);
  bool is_alphnum(const string &word) const;
  bool is_lower(const string &word) const;
  string get_suf(const string &word) const;
  void clean_str(string &word, vector<string> &ret) const;
  void replace_all_substrs(string &str, const string &substr, const string &re) const;
  vector<size_t> word2vec(string &word) const;
  double train_bptt_multi_emb(const vector<vector<vector<size_t> > > &contextwins,
                              const vector<size_t> &label_batch, mat &y);
  double train_bptt_multi_dropout(const vector<vector<vector<size_t> > > &contextwins,
                                  const vector<size_t> &label_batch,
                                   mat &y);
  void labels2minibatch(const vector<size_t> &labels, vector<vector<size_t> > &minibatch_all) const;
  void contextwin2minibatch(const vector<vector<vector<size_t> > > &sent_cw,
                            vector<vector<vector<vector<size_t> > > > &minibatch_all) const;
  void load_emb_mat(mat &emb, const string &filename);

  void tag_file(IO::ReaderFactory &reader, bool has_gold_labels);
  void tag_sent(Sentence &sent, vector<size_t> &sent_res,
                vector<size_t> &sent_gold_labels,
                double &err, mat &y, bool has_gold_labels);
  void classify(const vector<vector<vector<size_t> > > &contextwins,
                vector<size_t> &gold_labels,
                mat &y, vector<size_t> &sent_res, double &err, bool has_gold_labels);

  void load_emb_word_ind_map(const string &filename);

};

}
}

#endif /* SRC_LIB_RNN_TAGGER_RNN_POS_H_ */
