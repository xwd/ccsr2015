/*
 * rnn_parser.h
 *
 *  Created on: Feb 4, 2015
 *      Author: xwd
 */

#ifndef SRC_INCLUDE_RNN_PARSER_H_
#define SRC_INCLUDE_RNN_PARSER_H_

void load_cv0001_supercats(const std::string &filename,
                   std::vector<std::vector<std::vector<std::string>* >* > &supercatVec);

#endif /* SRC_INCLUDE_RNN_PARSER_H_ */
