#pragma once

#include <unordered_map>
#include "parser/ShiftReduceHypothesis.h"
#include <armadillo>
#include "rnn_parser/rnn_canonical.h"

using namespace std;

const static size_t empty_supercat_feat_ind = 0;
const static size_t unk_supercat_ind = 1;

namespace NLP { namespace CCG {

class ShiftReduceHypothesis;

class ContextLoader {

public:

  const SuperCat *s0c, *s1c, *s2c, *s3c;

  int q0w, q1w, q2w, q3w;
  int q0suf, q1suf, q2suf, q3suf;
  int q0cap, q1cap, q2cap, q3cap;

  // super feats related
  int s_s0w, s_s1w, s_s2w, s_s3w;
  int s_s0suf, s_s1suf, s_s2suf, s_s3suf;
  int s_s0cap, s_s1cap, s_s2cap, s_s3cap;

  std::vector<size_t> s0w;
  std::vector<size_t> s1w;
  std::vector<size_t> s2w;
  std::vector<size_t> s3w;

  std::vector<size_t> s0suf;
  std::vector<size_t> s1suf;
  std::vector<size_t> s2suf;
  std::vector<size_t> s3suf;

  std::vector<size_t> s0cap;
  std::vector<size_t> s1cap;
  std::vector<size_t> s2cap;
  std::vector<size_t> s3cap;

  std::vector<size_t> s0lw;
  std::vector<size_t> s0lsuf;
  std::vector<size_t> s0lcap;

  std::vector<size_t> s0rw;
  std::vector<size_t> s0rsuf;
  std::vector<size_t> s0rcap;

  std::vector<size_t> s0uw;
  std::vector<size_t> s0usuf;
  std::vector<size_t> s0ucap;

  std::vector<size_t> s1lw;
  std::vector<size_t> s1lsuf;
  std::vector<size_t> s1lcap;

  std::vector<size_t> s1rw;
  std::vector<size_t> s1rsuf;
  std::vector<size_t> s1rcap;

  std::vector<size_t> s1uw;
  std::vector<size_t> s1usuf;
  std::vector<size_t> s1ucap;

  size_t m_emb_last_col;
  size_t m_suf_last_col;

  size_t m_head_limit;

  // this keeps track of the total number of
  // cats seen so far during *training*
  size_t &m_cat_ind;
  bool m_train;
  //size_t m_action_feat;

  ContextLoader(const size_t emb_last_col,
                const size_t suf_last_col,
                size_t &cat_ind, const bool train)
  :s0c(0), s1c(0), s2c(0), s3c(0),
   q0w(emb_last_col), q1w(emb_last_col), q2w(emb_last_col), q3w(emb_last_col),
   q0suf(suf_last_col), q1suf(suf_last_col), q2suf(suf_last_col), q3suf(suf_last_col),
   q0cap(0), q1cap(0), q2cap(0), q3cap(0),
   s_s0w(emb_last_col), s_s1w(emb_last_col), s_s2w(emb_last_col), s_s3w(emb_last_col),
   s_s0suf(suf_last_col), s_s1suf(suf_last_col), s_s2suf(suf_last_col), s_s3suf(suf_last_col),
   s_s0cap(0), s_s1cap(0), s_s2cap(0), s_s3cap(0),
   m_emb_last_col(emb_last_col),
   m_suf_last_col(suf_last_col),
   m_head_limit(1),
   m_cat_ind(cat_ind),
   m_train(train) {}
   //m_action_feat(action_feat){}

  ~ContextLoader() {
    s0c = 0;
    s1c = 0;
    s2c = 0;
    s3c = 0;

    s0w.clear();
    s1w.clear();
    s2w.clear();
    s3w.clear();

    s0suf.clear();
    s1suf.clear();
    s2suf.clear();
    s3suf.clear();

    s0cap.clear();
    s1cap.clear();
    s2cap.clear();
    s3cap.clear();

    s0lw.clear();
    s0lsuf.clear();
    s0lcap.clear();

    s0rw.clear();
    s0rsuf.clear();
    s0rcap.clear();

    s0uw.clear();
    s0usuf.clear();
    s0ucap.clear();

    s1lw.clear();
    s1lsuf.clear();
    s1lcap.clear();

    s1rw.clear();
    s1rsuf.clear();
    s1rcap.clear();

    s1uw.clear();
    s1usuf.clear();
    s1ucap.clear();

  }


  const size_t canonize2ind(const Cat *cat,
                            RNNCanonical &canonical) {

    size_t ind = canonical.get_ind(cat);

    if (!m_train) {
      // at test time just return ind,
      // ind == 1 means the given cat was
      // not seen during training
      return ind;

    } else {

      // at training time, insert an unseen
      // cat into canonical
      // in RNN Canonical hashmap,
      // it should be made to reutrn unk_supercat_ind
      // for an unk supercat, it's currently set to 1
      if (ind == 1) {
        ++m_cat_ind;
        canonical.add(cat, m_cat_ind);
        return m_cat_ind;

      } else {
        assert(ind >= 2);
        return ind;
      }

    }
  }


  void load_context(const ShiftReduceHypothesis *hypo,
                    vector<size_t> &word_inds_ret,
                    vector<size_t> &suf_inds_ret,
                    vector<size_t> &cap_inds_ret,
                    vector<size_t> &supercat_inds_ret,
                    //vector<size_t> &action_inds_ret,
                    const vector<size_t> &word_inds,
                    const vector<size_t> &suf_inds,
                    const vector<size_t> &cap_inds,
                    RNNCanonical &cats_map) {

    // word_inds, suf_inds and cap_inds are from the input

    assert(word_inds.size() == suf_inds.size());
    assert(suf_inds.size() == cap_inds.size());

    const size_t q0_ind = hypo->GetNextInputIndex();
    const size_t q1_ind = hypo->GetNextInputIndex() + 1;
    const size_t q2_ind = hypo->GetNextInputIndex() + 2;
    const size_t q3_ind = hypo->GetNextInputIndex() + 3;

    s0c = hypo->GetStackSize() >= 1 ? hypo->GetStackTopSuperCat() : 0;
    s1c = hypo->GetStackSize() >= 2 ? hypo->GetPrvStack()->GetStackTopSuperCat() : 0;
    s2c = hypo->GetStackSize() >= 3 ? hypo->GetPrvStack()->GetPrvStack()->GetStackTopSuperCat() : 0;
    s3c = hypo->GetStackSize() >= 4 ? hypo->GetPrvStack()->GetPrvStack()->GetPrvStack()->GetStackTopSuperCat() : 0;

    //    q0w = hypo->GetNextInputIndex() < words.size() ? word_inds[hypo->GetNextInputIndex()] : -1;
    //    q1w = hypo->GetNextInputIndex() + 1 < words.size() ? word_inds[hypo->GetNextInputIndex() + 1] : -1;
    //    q2w = hypo->GetNextInputIndex() + 2 < words.size() ? word_inds[hypo->GetNextInputIndex() + 2] : -1;
    //    q3w = hypo->GetNextInputIndex() + 3 < words.size() ? word_inds[hypo->GetNextInputIndex() + 3] : -1;

    // action related feats
//
//    if (s0c != 0) {
//      action_inds_ret.push_back(hypo->GetStackTopAction());
//    } else {
//      // shift: 0, unary: 1, reduce: 2, no action: 3
//      action_inds_ret.push_back(3);
//    }

    // supercat related feats
    // assuming an empty feature gets a 0 value
    // i.e., the 0th column of the cat
    // embedding matrix

    if (m_train) {

      size_t ind = 0;

      // s0c
      if (s0c != 0) {

        assert(s0c->cat);
        ind = canonize2ind(s0c->cat, cats_map);
        assert(ind != unk_supercat_ind);
        supercat_inds_ret.push_back(ind); // s0C

        if (s0c->left != 0 && s0c->right != 0) {

          assert(s0c->left->cat);
          assert(s0c->right->cat);

          ind = canonize2ind(s0c->left->cat, cats_map);
          assert(ind != unk_supercat_ind);
          supercat_inds_ret.push_back(ind); // s0Lc

          ind = canonize2ind(s0c->right->cat, cats_map);
          assert(ind != unk_supercat_ind);
          supercat_inds_ret.push_back(ind); // s0Rc

          supercat_inds_ret.push_back(empty_supercat_feat_ind); // s0Uc

        } else if (s0c->left != 0 && s0c->right == 0) {

          assert(hypo->GetStackTopAction() == UNARY);

          supercat_inds_ret.push_back(empty_supercat_feat_ind); // s0Lc
          supercat_inds_ret.push_back(empty_supercat_feat_ind); // s0Rc

          ind = canonize2ind(s0c->left->cat, cats_map);
          assert(ind != unk_supercat_ind);
          supercat_inds_ret.push_back(ind); // s0Uc

        } else {

          assert(s0c->left == 0);
          assert(s0c->right == 0);

          supercat_inds_ret.push_back(empty_supercat_feat_ind); // s0Lc
          supercat_inds_ret.push_back(empty_supercat_feat_ind); // s0Rc
          supercat_inds_ret.push_back(empty_supercat_feat_ind); // s0Uc

        }

      } else {

        supercat_inds_ret.push_back(empty_supercat_feat_ind); // s0c
        supercat_inds_ret.push_back(empty_supercat_feat_ind); // s0Lc
        supercat_inds_ret.push_back(empty_supercat_feat_ind); // s0Rc
        supercat_inds_ret.push_back(empty_supercat_feat_ind); // s0Uc

      }

      // s1c
      if (s1c != 0) {

        assert(s1c->cat);
        ind = canonize2ind(s1c->cat, cats_map);
        assert(ind != unk_supercat_ind);
        supercat_inds_ret.push_back(ind); // s1c

        if (s1c->left != 0 && s1c->right != 0) {

          assert(s1c->left->cat);
          assert(s1c->right->cat);

          ind = canonize2ind(s1c->left->cat, cats_map);
          assert(ind != unk_supercat_ind);
          supercat_inds_ret.push_back(ind); // s1Lc

          ind = canonize2ind(s1c->right->cat, cats_map);
          assert(ind != unk_supercat_ind);
          supercat_inds_ret.push_back(ind); // s1Rc

          supercat_inds_ret.push_back(empty_supercat_feat_ind); // s1Uc

        } else if (s1c->left != 0 && s1c->right == 0) {

          assert(hypo->GetPrvStack()->GetStackTopAction() == UNARY);

          supercat_inds_ret.push_back(empty_supercat_feat_ind); // s1Lc
          supercat_inds_ret.push_back(empty_supercat_feat_ind); // s1Rc

          ind = canonize2ind(s1c->left->cat, cats_map);
          assert(ind != unk_supercat_ind);
          supercat_inds_ret.push_back(ind); // s1Uc

        } else {

          assert(s1c->left == 0);
          assert(s1c->right == 0);

          supercat_inds_ret.push_back(empty_supercat_feat_ind); // s1Lc
          supercat_inds_ret.push_back(empty_supercat_feat_ind); // s1Rc
          supercat_inds_ret.push_back(empty_supercat_feat_ind); // s1Uc

        }

      } else {

        supercat_inds_ret.push_back(empty_supercat_feat_ind); // s1c
        supercat_inds_ret.push_back(empty_supercat_feat_ind); // s1Lc
        supercat_inds_ret.push_back(empty_supercat_feat_ind); // s1Rc
        supercat_inds_ret.push_back(empty_supercat_feat_ind); // s1Uc

      }

      // s2c
      if (s2c != 0) {

        assert(s0c->cat);
        assert(s1c->cat);
        assert(s2c->cat);

        ind = canonize2ind(s2c->cat, cats_map);
        assert(ind != unk_supercat_ind);
        supercat_inds_ret.push_back(ind); // s2C

      } else {
        supercat_inds_ret.push_back(empty_supercat_feat_ind); // s2C
      }

      // s3c
      if (s3c != 0) {

        assert(s0c->cat);
        assert(s1c->cat);
        assert(s2c->cat);
        assert(s3c->cat);

        ind = canonize2ind(s3c->cat, cats_map);
        assert(ind != unk_supercat_ind);
        supercat_inds_ret.push_back(ind); // s3C

      } else {
        supercat_inds_ret.push_back(empty_supercat_feat_ind); // s3C
      }

    } else {  // this is for decoding at test time only

      size_t ind  = 0;

      if (s0c != 0) {

        assert(s0c->cat);

        ind = canonize2ind(s0c->cat, cats_map);
        supercat_inds_ret.push_back(ind); // s0C

        if (s0c->left != 0 && s0c->right != 0) {
          assert(s0c->left->cat);
          assert(s0c->right->cat);

          ind = canonize2ind(s0c->left->cat, cats_map);
          supercat_inds_ret.push_back(ind); // s0LC

          ind = canonize2ind(s0c->right->cat, cats_map);
          supercat_inds_ret.push_back(ind); // s0RC

          supercat_inds_ret.push_back(empty_supercat_feat_ind); // s0Uc

        } else if (s0c->left != 0 && s0c->right == 0) {

          assert(hypo->GetStackTopAction() == UNARY);
          supercat_inds_ret.push_back(empty_supercat_feat_ind); // s0Lc
          supercat_inds_ret.push_back(empty_supercat_feat_ind); // s0Rc

          ind = canonize2ind(s0c->left->cat, cats_map);
          supercat_inds_ret.push_back(ind); // s0UC

        } else {

          assert(s0c->left == 0);
          assert(s0c->right == 0);

          supercat_inds_ret.push_back(empty_supercat_feat_ind); // s0Lc
          supercat_inds_ret.push_back(empty_supercat_feat_ind); // s0Rc
          supercat_inds_ret.push_back(empty_supercat_feat_ind); // s0Uc

        }

      } else {

        supercat_inds_ret.push_back(empty_supercat_feat_ind); // s0c
        supercat_inds_ret.push_back(empty_supercat_feat_ind); // s0Lc
        supercat_inds_ret.push_back(empty_supercat_feat_ind); // s0Rc
        supercat_inds_ret.push_back(empty_supercat_feat_ind); // s0Uc

      }


      if (s1c != 0) {

        assert(s1c->cat);

        ind = canonize2ind(s1c->cat, cats_map);
        supercat_inds_ret.push_back(ind); // s1C

        if (s1c->left != 0 && s1c->right != 0) {
          assert(s1c->left->cat);
          assert(s1c->right->cat);

          ind = canonize2ind(s1c->left->cat, cats_map);
          supercat_inds_ret.push_back(ind); // s1LC

          ind = canonize2ind(s1c->right->cat, cats_map);
          supercat_inds_ret.push_back(ind); // s1RC

          supercat_inds_ret.push_back(empty_supercat_feat_ind); // s1Uc

        } else if (s1c->left != 0 && s1c->right == 0) {

          assert(hypo->GetPrvStack()->GetStackTopAction() == UNARY);
          supercat_inds_ret.push_back(empty_supercat_feat_ind); // s1Lc
          supercat_inds_ret.push_back(empty_supercat_feat_ind); // s1Rc

          ind = canonize2ind(s1c->left->cat, cats_map);
          supercat_inds_ret.push_back(ind); // s1UC

        } else {

          assert(s1c->left == 0);
          assert(s1c->right == 0);

          supercat_inds_ret.push_back(empty_supercat_feat_ind); // s1Lc
          supercat_inds_ret.push_back(empty_supercat_feat_ind); // s1Rc
          supercat_inds_ret.push_back(empty_supercat_feat_ind); // s1Uc

        }

      } else {

        supercat_inds_ret.push_back(empty_supercat_feat_ind); // s1c
        supercat_inds_ret.push_back(empty_supercat_feat_ind); // s1Lc
        supercat_inds_ret.push_back(empty_supercat_feat_ind); // s1Rc
        supercat_inds_ret.push_back(empty_supercat_feat_ind); // s1Uc

      }


      if (s2c != 0) {

        assert(s0c->cat);
        assert(s1c->cat);
        assert(s2c->cat);

        ind = canonize2ind(s2c->cat, cats_map);
        supercat_inds_ret.push_back(ind); // s2C

      } else {
        supercat_inds_ret.push_back(empty_supercat_feat_ind); // s2C
      }

      if (s3c != 0) {

        assert(s0c->cat);
        assert(s1c->cat);
        assert(s2c->cat);
        assert(s3c->cat);

        ind = canonize2ind(s3c->cat, cats_map);
        supercat_inds_ret.push_back(ind); // s3C

      } else {
        supercat_inds_ret.push_back(empty_supercat_feat_ind); // s3C
      }

    }


    if (q0_ind < word_inds.size()) {
      q0w = word_inds[q0_ind];
      q0suf = suf_inds[q0_ind];
      q0cap = cap_inds[q0_ind];
    }

    if (q1_ind < word_inds.size()) {
      q1w = word_inds[q1_ind];
      q1suf = suf_inds[q1_ind];
      q1cap = cap_inds[q1_ind];
    }

    if (q2_ind < word_inds.size()) {
      q2w = word_inds[q2_ind];
      q2suf = suf_inds[q2_ind];
      q2cap = cap_inds[q2_ind];
    }

    if (q3_ind < word_inds.size()) {
      q3w = word_inds[q3_ind];
      q3suf = suf_inds[q3_ind];
      q3cap = cap_inds[q3_ind];
    }

    word_inds_ret.push_back(q0w);
    word_inds_ret.push_back(q1w);
    word_inds_ret.push_back(q2w);
    word_inds_ret.push_back(q3w);

    suf_inds_ret.push_back(q0suf);
    suf_inds_ret.push_back(q1suf);
    suf_inds_ret.push_back(q2suf);
    suf_inds_ret.push_back(q3suf);

    cap_inds_ret.push_back(q0cap);
    cap_inds_ret.push_back(q1cap);
    cap_inds_ret.push_back(q2cap);
    cap_inds_ret.push_back(q3cap);

    // supertagger feats

    if (hypo->get_shift_count() == 0) {

      // do nothing
      // everything initialized in constructor

    } else if (hypo->get_shift_count() == 1) {

      s_s0w = word_inds[0];
      s_s0suf = suf_inds[0];
      s_s0cap = cap_inds[0];

    } else if (hypo->get_shift_count() == 2) {

      s_s0w = word_inds[1];
      s_s0suf = suf_inds[1];
      s_s0cap = cap_inds[1];

      s_s1w = word_inds[0];
      s_s1suf = suf_inds[0];
      s_s1cap = cap_inds[0];

    } else if (hypo->get_shift_count() == 3) {

      s_s0w = word_inds[2];
      s_s0suf = suf_inds[2];
      s_s0cap = cap_inds[2];

      s_s1w = word_inds[1];
      s_s1suf = suf_inds[1];
      s_s1cap = cap_inds[1];

      s_s2w = word_inds[0];
      s_s2suf = suf_inds[0];
      s_s2cap = cap_inds[0];

    } else if (hypo->get_shift_count() >= 4) {

      s_s0w = word_inds[3];
      s_s0suf = suf_inds[3];
      s_s0cap = cap_inds[3];

      s_s1w = word_inds[2];
      s_s1suf = suf_inds[2];
      s_s1cap = cap_inds[2];

      s_s2w = word_inds[1];
      s_s2suf = suf_inds[1];
      s_s2cap = cap_inds[1];

      s_s3w = word_inds[0];
      s_s3suf = suf_inds[0];
      s_s3cap = cap_inds[0];
    }

    word_inds_ret.push_back(s_s0w);
    word_inds_ret.push_back(s_s1w);
    word_inds_ret.push_back(s_s2w);
    word_inds_ret.push_back(s_s3w);

    suf_inds_ret.push_back(s_s0suf);
    suf_inds_ret.push_back(s_s1suf);
    suf_inds_ret.push_back(s_s2suf);
    suf_inds_ret.push_back(s_s3suf);

    cap_inds_ret.push_back(s_s0cap);
    cap_inds_ret.push_back(s_s1cap);
    cap_inds_ret.push_back(s_s2cap);
    cap_inds_ret.push_back(s_s3cap);

    if (s0c != 0) {
      assert(s0c->cat);
      get_values(true, &s0c->vars[s0c->cat->var], s0w, s0suf, s0cap,
                 word_inds, suf_inds, cap_inds);

      if (s0w.size() > 0) {
        assert(s0w.size() == s0suf.size());
        assert(s0suf.size() == s0cap.size());

        for (size_t i = 0; i < s0w.size(); ++i) {
          word_inds_ret.push_back(s0w[i]);
          suf_inds_ret.push_back(s0suf[i]);
          cap_inds_ret.push_back(s0cap[i]);
          if (i + 1 == m_head_limit)
            break;
        }

        if (s0w.size() < m_head_limit) {
          size_t diff = m_head_limit - s0w.size();
          for (size_t j = 0; j < diff; ++j) {
            word_inds_ret.push_back(m_emb_last_col);
            suf_inds_ret.push_back(m_suf_last_col);
            cap_inds_ret.push_back(0);
          }
        }

      } else {
        for (size_t k = 0; k < m_head_limit; ++k) {
          word_inds_ret.push_back(m_emb_last_col);
          suf_inds_ret.push_back(m_suf_last_col);
          cap_inds_ret.push_back(0);
        }
      }

    } else {
      for (size_t k = 0; k < m_head_limit; ++k) {
        word_inds_ret.push_back(m_emb_last_col);
        suf_inds_ret.push_back(m_suf_last_col);
        cap_inds_ret.push_back(0);
      }
    }

    if (s1c != 0) {
      assert(s1c->cat);
      get_values(true, &s1c->vars[s1c->cat->var], s1w, s1suf, s1cap,
                 word_inds, suf_inds, cap_inds);

      if (s1w.size() > 0) {
        assert(s1w.size() == s1suf.size());
        assert(s1suf.size() == s1cap.size());
        for (size_t i = 0; i < s1w.size(); ++i) {
          word_inds_ret.push_back(s1w[i]);
          suf_inds_ret.push_back(s1suf[i]);
          cap_inds_ret.push_back(s1cap[i]);
          if (i + 1 == m_head_limit)
            break;
        }

        if (s1w.size() < m_head_limit) {
          size_t diff = m_head_limit - s1w.size();
          for (size_t j = 0; j < diff; ++j) {
            word_inds_ret.push_back(m_emb_last_col);
            suf_inds_ret.push_back(m_suf_last_col);
            cap_inds_ret.push_back(0);
          }
        }
      } else {
        for (size_t k = 0; k < m_head_limit; ++k) {
          word_inds_ret.push_back(m_emb_last_col);
          suf_inds_ret.push_back(m_suf_last_col);
          cap_inds_ret.push_back(0);
        }
      }
    } else {
      for (size_t k = 0; k < m_head_limit; ++k) {
        word_inds_ret.push_back(m_emb_last_col);
        suf_inds_ret.push_back(m_suf_last_col);
        cap_inds_ret.push_back(0);
      }
    }

    if (s2c != 0) {
      assert(s2c->cat);
      get_values(true, &s2c->vars[s2c->cat->var], s2w, s2suf, s2cap,
                 word_inds, suf_inds, cap_inds);

      if (s2w.size() > 0) {
        assert(s2w.size() == s2suf.size());
        assert(s2suf.size() == s2cap.size());
        for (size_t i = 0; i < s2w.size(); ++i) {
          word_inds_ret.push_back(s2w[i]);
          suf_inds_ret.push_back(s2suf[i]);
          cap_inds_ret.push_back(s2cap[i]);
          if (i + 1 == m_head_limit)
            break;
        }

        if (s2w.size() < m_head_limit) {
          size_t diff = m_head_limit - s2w.size();
          for (size_t j = 0; j < diff; ++j) {
            word_inds_ret.push_back(m_emb_last_col);
            suf_inds_ret.push_back(m_suf_last_col);
            cap_inds_ret.push_back(0);
          }
        }
      } else {
        for (size_t k = 0; k < m_head_limit; ++k) {
          word_inds_ret.push_back(m_emb_last_col);
          suf_inds_ret.push_back(m_suf_last_col);
          cap_inds_ret.push_back(0);
        }
      }

    } else {
      for (size_t k = 0; k < m_head_limit; ++k) {
        word_inds_ret.push_back(m_emb_last_col);
        suf_inds_ret.push_back(m_suf_last_col);
        cap_inds_ret.push_back(0);
      }
    }

    if (s3c != 0) {
      assert(s3c->cat);
      get_values(true, &s3c->vars[s3c->cat->var], s3w, s3suf, s3cap,
                 word_inds, suf_inds, cap_inds);

      if (s3w.size() > 0) {
        assert(s3w.size() == s3suf.size());
        assert(s3suf.size() == s3cap.size());
        for (size_t i = 0; i < s3w.size(); ++i) {
          word_inds_ret.push_back(s3w[i]);
          suf_inds_ret.push_back(s3suf[i]);
          cap_inds_ret.push_back(s3cap[i]);
          if (i + 1 == m_head_limit)
            break;
        }

        if (s3w.size() < m_head_limit) {
          size_t diff = m_head_limit - s3w.size();
          for (size_t j = 0; j < diff; ++j) {
            word_inds_ret.push_back(m_emb_last_col);
            suf_inds_ret.push_back(m_suf_last_col);
            cap_inds_ret.push_back(0);
          }
        }
      } else {
        for (size_t k = 0; k < m_head_limit; ++k) {
          word_inds_ret.push_back(m_emb_last_col);
          suf_inds_ret.push_back(m_suf_last_col);
          cap_inds_ret.push_back(0);
        }
      }

    } else {
      for (size_t k = 0; k < m_head_limit; ++k) {
        word_inds_ret.push_back(m_emb_last_col);
        suf_inds_ret.push_back(m_suf_last_col);
        cap_inds_ret.push_back(0);
      }
    }

    //cerr << "size##########: " << word_inds_ret.size() << endl;


//    if (s3c != 0) {
//      assert(s3c->cat);
//      get_values(true, &s3c->vars[s3c->cat->var], s3w, s3suf, s3cap,
//                 word_inds, suf_inds, cap_inds);
//
//      if (s3w.size() > 0) {
//        assert(s3w.size() == s3suf.size());
//        assert(s3suf.size() == s3cap.size());
//        for (size_t i = 0; i < s3w.size(); ++i) {
//          word_inds_ret.push_back(s3w[i]);
//          suf_inds_ret.push_back(s3suf[i]);
//          cap_inds_ret.push_back(s3cap[i]);
//          if (i + 1 == m_head_limit)
//            break;
//        }
//
//        if (s3w.size() < m_head_limit) {
//          size_t diff = m_head_limit - s3w.size();
//          for (size_t j = 0; j < diff; ++j) {
//            word_inds_ret.push_back(m_emb_last_col);
//            suf_inds_ret.push_back(m_suf_last_col);
//            cap_inds_ret.push_back(0);
//          }
//        }
//      } else {
//        for (size_t k = 0; k < m_head_limit; ++k) {
//          word_inds_ret.push_back(m_emb_last_col);
//          suf_inds_ret.push_back(m_suf_last_col);
//          cap_inds_ret.push_back(0);
//        }
//      }
//
//    } else {
//      for (size_t k = 0; k < m_head_limit; ++k) {
//        word_inds_ret.push_back(m_emb_last_col);
//        suf_inds_ret.push_back(m_suf_last_col);
//        cap_inds_ret.push_back(0);
//      }
//    }

    // s0lw, s0rw, s0uw (s1)

    if (s0c != 0) {

      assert(s0c->cat);

      if (s0c->left != 0 && s0c->right != 0) {
        assert(s0c->left->cat);
        get_values(true, &s0c->left->vars[s0c->left->cat->var], s0lw, s0lsuf, s0lcap,
                       word_inds, suf_inds, cap_inds);

        fill_inds(s0lw, s0lsuf, s0lcap, word_inds_ret, suf_inds_ret, cap_inds_ret);

        assert(s0c->right->cat);
        get_values(true, &s0c->right->vars[s0c->right->cat->var], s0rw, s0rsuf, s0rcap,
                              word_inds, suf_inds, cap_inds);

        fill_inds(s0rw, s0rsuf, s0rcap, word_inds_ret, suf_inds_ret, cap_inds_ret);

        // s0u
        for (size_t k = 0; k < m_head_limit; ++k) {
          word_inds_ret.push_back(m_emb_last_col);
          suf_inds_ret.push_back(m_suf_last_col);
          cap_inds_ret.push_back(0);
        }

      } else if (s0c->left != 0 && s0c->right == 0) {

        // j < 2, since two feats: s0lw, s0rw
        for (size_t j = 0; j < 2; ++j) {
          for (size_t k = 0; k < m_head_limit; ++k) {
            word_inds_ret.push_back(m_emb_last_col);
            suf_inds_ret.push_back(m_suf_last_col);
            cap_inds_ret.push_back(0);
          }
        }

        assert(s0c->left->cat);

        get_values(true, &s0c->left->vars[s0c->left->cat->var], s0uw, s0usuf, s0ucap,
                              word_inds, suf_inds, cap_inds);

        fill_inds(s0uw, s0usuf, s0ucap, word_inds_ret, suf_inds_ret, cap_inds_ret);

      } else {

        assert(s0c->left == 0);
        assert(s0c->right == 0);

        // j < 3, since three feats: s0lw, s0rw, s0uw
        for (size_t j = 0; j < 3; ++j) {
          for (size_t k = 0; k < m_head_limit; ++k) {
            word_inds_ret.push_back(m_emb_last_col);
            suf_inds_ret.push_back(m_suf_last_col);
            cap_inds_ret.push_back(0);
          }
        }

      }

    } else {
      // j < 3, since three feats: s0lw, s0rw, s0uw
      for (size_t j = 0; j < 3; ++j) {
        for (size_t k = 0; k < m_head_limit; ++k) {
          word_inds_ret.push_back(m_emb_last_col);
          suf_inds_ret.push_back(m_suf_last_col);
          cap_inds_ret.push_back(0);
        }
      }
    }


    if (s1c != 0) {

      assert(s1c->cat);

      if (s1c->left != 0 && s1c->right != 0) {

        assert(s1c->left->cat);
        get_values(true, &s1c->left->vars[s1c->left->cat->var], s1lw, s1lsuf, s1lcap,
                       word_inds, suf_inds, cap_inds);

        fill_inds(s1lw, s1lsuf, s1lcap, word_inds_ret, suf_inds_ret, cap_inds_ret);

        assert(s1c->right->cat);
        get_values(true, &s1c->right->vars[s1c->right->cat->var], s1rw, s1rsuf, s1rcap,
                              word_inds, suf_inds, cap_inds);

        fill_inds(s1rw, s1rsuf, s1rcap, word_inds_ret, suf_inds_ret, cap_inds_ret);

        // s1u
        for (size_t k = 0; k < m_head_limit; ++k) {
          word_inds_ret.push_back(m_emb_last_col);
          suf_inds_ret.push_back(m_suf_last_col);
          cap_inds_ret.push_back(0);
        }

      } else if (s1c->left != 0 && s1c->right == 0) {

        // j < 2, since two feats: s1lw, s1rw
        for (size_t j = 0; j < 2; ++j) {
          for (size_t k = 0; k < m_head_limit; ++k) {
            word_inds_ret.push_back(m_emb_last_col);
            suf_inds_ret.push_back(m_suf_last_col);
            cap_inds_ret.push_back(0);
          }
        }

        assert(s1c->left->cat);

        get_values(true, &s1c->left->vars[s1c->left->cat->var], s1uw, s1usuf, s1ucap,
                              word_inds, suf_inds, cap_inds);

        fill_inds(s1uw, s1usuf, s1ucap, word_inds_ret, suf_inds_ret, cap_inds_ret);

      } else {

        assert(s1c->left == 0);
        assert(s1c->right == 0);

        // j < 3, since three feats: s0lw, s0rw, s0uw
        for (size_t j = 0; j < 3; ++j) {
          for (size_t k = 0; k < m_head_limit; ++k) {
            word_inds_ret.push_back(m_emb_last_col);
            suf_inds_ret.push_back(m_suf_last_col);
            cap_inds_ret.push_back(0);
          }
        }

      }

    } else {
      // j < 3, since three feats: s0lw, s0rw, s0uw
      for (size_t j = 0; j < 3; ++j) {
        for (size_t k = 0; k < m_head_limit; ++k) {
          word_inds_ret.push_back(m_emb_last_col);
          suf_inds_ret.push_back(m_suf_last_col);
          cap_inds_ret.push_back(0);
        }
      }
    }
}


  void fill_inds(const vector<size_t> &head_word_inds,
                 const vector<size_t> &head_word_suf_inds,
                 const vector<size_t> &head_word_cap_inds,
                 vector<size_t> &word_inds_ret,
                 vector<size_t> &suf_inds_ret,
                 vector<size_t> &cap_inds_ret) {

    if (head_word_inds.size() > 0) {

      assert(head_word_inds.size() == head_word_suf_inds.size());
      assert(head_word_suf_inds.size() == head_word_cap_inds.size());

      for (size_t i = 0; i < head_word_inds.size(); ++i) {
        word_inds_ret.push_back(head_word_inds[i]);
        suf_inds_ret.push_back(head_word_suf_inds[i]);
        cap_inds_ret.push_back(head_word_cap_inds[i]);
        if (i + 1 == m_head_limit)
          break;
      }

      if (head_word_inds.size() < m_head_limit) {
        size_t diff = m_head_limit - head_word_inds.size();
        for (size_t j = 0; j < diff; ++j) {
          word_inds_ret.push_back(m_emb_last_col);
          suf_inds_ret.push_back(m_suf_last_col);
          cap_inds_ret.push_back(0);
        }
      }

    } else {
      for (size_t k = 0; k < m_head_limit; ++k) {
        word_inds_ret.push_back(m_emb_last_col);
        suf_inds_ret.push_back(m_suf_last_col);
        cap_inds_ret.push_back(0);
      }
    }

  }


  void get_values(bool get_suf, const Variable *var,
                 std::vector<size_t> &heads,
                 std::vector<size_t> &sufs,
                 std::vector<size_t> &caps,
                 const vector<size_t> &word_inds,
                 const vector<size_t> &suf_inds,
                 const vector<size_t> &cap_inds) const {

    const Position *const end = var->fillers + Variable::NFILLERS;
    for(const Position *p = var->fillers; p != end && *p != Variable::SENTINEL; ++p) {
      if(!*p)
        continue;

      assert(*p - 1 >= 0);
      assert((size_t)(*p - 1) < word_inds.size());
      size_t value = word_inds[*p - 1];
      //if(!value)
      //continue;
      // the above is not needed here
      // since unknown words are dealt with
      // in word_inds etc.
      heads.push_back(value);

      if (get_suf) {
        //assert(tagValues[*p - 1]);
        assert((size_t)(*p - 1) < suf_inds.size());
        sufs.push_back(suf_inds[*p - 1]);
        caps.push_back(cap_inds[*p - 1]);
      }
    }
  }

};


} }


