/* -*- Mode: C++; -*- */
// C&C NLP tools
// Copyright (c) Universities of Edinburgh, Oxford and Sydney
// Copyright (c) James R. Curran
//
// This software is covered by a non-commercial use licence.
// See LICENCE.txt for the full text of the licence.
//
// If LICENCE.txt is not included in this distribution
// please email candc@it.usyd.edu.au to obtain a copy.


#ifndef SRC_INCLUDE_RNN_PARSER_RNN_CANONICAL_H_
#define SRC_INCLUDE_RNN_PARSER_RNN_CANONICAL_H_

namespace NLP {
  namespace CCG {

    class RNNCanonical {
    private:
      class _Impl;
      _Impl *_impl;
    public:
      RNNCanonical(void);
      RNNCanonical(const RNNCanonical &other);

      ~RNNCanonical(void);

      RNNCanonical &operator=(RNNCanonical &other);

      size_t size(void) const;

      const size_t get_ind(const Cat *cat) const;
      //const Cat *operator[](const Cat *cat) const { return get(cat); }
      const size_t operator[](const Cat *cat) const { return get_ind(cat); }

      const Cat *add(const Cat *cat, const size_t ind);
      const size_t dump(std::ofstream &out) const;

    };

  }
}


#endif /* SRC_INCLUDE_RNN_PARSER_RNN_CANONICAL_H_ */
