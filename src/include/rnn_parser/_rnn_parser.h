/* -*- Mode: C++; -*- */
// C&C NLP tools
// Copyright (c) Universities of Edinburgh, Oxford and Sydney
// Copyright (c) James R. Curran
//
// This software is covered by a non-commercial use licence.
// See LICENCE.txt for the full text of the licence.
//
// If LICENCE.txt is not included in this distribution
// please email candc@it.usyd.edu.au to obtain a copy.

//#pragma once
// this should be commented out when debugging
#define ARMA_NO_DEBUG

#include "base.h"

#include "config/config.h"

#include "pool.h"

#include "rnn_parser/rnn_parser.h"

#include "parser/variable.h"
#include "parser/dependency.h"
#include "parser/distance.h"
#include "parser/filled.h"
#include "parser/supercat.h"
#include "parser/unify.h"
#include "rnn_parser/rnn_rule.h"
#include "parser/cell.h"
#include "parser/equiv.h"
#include "parser/treebank.h"
#include "parser/chart.h"
#include "parser/rule_instances.h"
#include "tree/attributes.h"
#include "parser/depscore.h"
#include "parser/feature_type.h"
#include "parser/feature_dist_type.h"
#include "parser/feature_cat.h"
#include "parser/feature_rule.h"
#include "parser/feature_rule_head.h"
#include "parser/feature_rule_dep.h"
#include "parser/feature_rule_dep_dist.h"
#include "parser/feature_dep.h"
#include "parser/feature_dep_dist.h"
#include "parser/feature_genrule.h"
#include "parser/inside_outside.h"

#include "parser/ShiftReduceLattice.h"
#include "parser/ShiftReduceHypothesisQueue.h"
#include "parser/ShiftReduceFeature.h"
#include "parser/ShiftReduceContext.h"
#include "parser/markedup.h"

#include "rnn_parser/rnn_canonical.h"
#include "rnn_tagger.h"

#include <queue>
#include <unordered_map>
#include <armadillo>

using namespace arma;

namespace NLP {
namespace CCG {

inline double add_logs(double x, double y){
  if(y <= x)
    return x + log1p(exp(y - x));
  else
    return y + log1p(exp(x - y));
}

using namespace NLP::Tree;

class InsideOutside;

class RNNParser::_Impl {
protected:
  void _load_features(const std::string &filename);
  void _load_weights(const std::string &filename);
  void _load_rules(const std::string &filename);


public:
  const RNNConfig &cfg;

  std::string comment;

  Sentence &sent;
  //Sentence &sent_rnn_fmt;
  ulong nsentences;

  Categories &cats;

  Lexicon lexicon;
  DependencyAttributes dep_attrs;
  DistanceAttributes dist_attrs;
  RuleAttributes rule_attrs;

  CatFeature cat_feats;
  RuleFeature rule_feats;
  RuleHeadFeature rule_head_feats;
  RuleDepFeature rule_dep_feats;
  RuleDepDistFeature rule_dep_dist_feats;
  DepFeature dep_feats;
  DepDistFeature dep_dist_feats;
  GenruleFeature genrule_feats;

  // used only in the normal form parser when checking if a rule
  // instance is in CCGbank
  RuleInstances rule_instances;

  ulong nfeatures;
  double *weights;

  Chart chart;
  RNNRules rules;
  SuperCats results;
  std::vector<ulong> ids;

  InsideOutside inside_outside;

  const Cat *const NP;
  const Cat *const NbN;
  const Cat *const NPbNP;
  const Cat *const SbS;
  const Cat *const SfS;
  const Cat *const SbNPbSbNP;
  const Cat *const SbNPfSbNP;
  const Cat *const SfSbSfS;
  const Cat *const SbNPbSbNPbSbNPbSbNP;
  const Cat *const NPfNPbNP;

  const size_t reduce_id_left_punct = 1;
  const size_t reduce_id_left_punct_tc = 2;
  const size_t reduce_id_right_punct = 3;
  const size_t reduce_id_right_punct_tc = 4;
  const size_t reduce_id_funny_conj = 5;
  const size_t reduce_id_std_conj = 6;

  //Pool *m_shiftReduceFeaturePool;

  std::vector<std::vector<ShiftReduceAction*>* > m_goldTreeVec;
  std::vector<std::vector<std::vector<std::string>* >* > m_supercatsVec;
  std::vector<vector<pair<size_t, size_t> > > m_gold_tree_vec_rnn;
  unordered_map<string, size_t> m_full_wordstr_emb_map;

  //ShiftReduceFeature *m_shiftReduceFeatures;
  //ShiftReduceContext m_shiftReduceContext;

  // shift-reduce perceptron training related
  bool m_allowFragTree;
  //bool m_allowFragAndComplete;

  bool m_hasGoldAction;
  bool m_hasOutsideRule;
  ShiftReduceLattice *m_lattice;
  ulong m_beamSize;

  bool m_train;
  std::unordered_map<string, size_t> m_lex_cat_ind_map;
  std::unordered_map<size_t, string> m_lex_ind_cat_map;

  vector<vector<vector<vector<size_t> > > > m_all_sent_context_vec_train;
  vector<vector<size_t> > m_all_sent_target_vec_train;
  vector<vector<vector<size_t> > > m_all_sent_feasible_y_vals_train;

  // precomputation trick related
  // key: word_feat_ind val: {emb_ind, count}
  unordered_map<size_t, unordered_map<size_t, size_t>> m_word_feat_emb_ind_map;
  unordered_map<size_t, unordered_map<size_t, size_t>> m_suf_feat_emb_ind_map;
  unordered_map<size_t, unordered_map<size_t, size_t>> m_super_feat_emb_ind_map;

  unordered_map<size_t, unordered_map<size_t, mat>> m_word_emb_lh_cache_val;
  unordered_map<size_t, unordered_map<size_t, mat>> m_suf_emb_lh_cache_val;
  //vector<unordered_map<size_t, mat>> m_super_emb_lh_cache_val;

  // rnn related parameters

  size_t m_bs;
  size_t m_ds;
  size_t m_dc;
  size_t m_de;
  size_t m_nh;
  size_t m_dsuper;
  size_t m_daction;
  double m_lr;

  size_t m_nclasses;
  size_t m_vocsize;
  size_t m_suf_count;
  size_t m_feature_count;
  size_t m_supercat_feature_count;

  size_t m_const_feature_count;
  size_t m_variable_feature_count;
  size_t m_head_limit;

  mat m_wx;
  mat m_wh;
  mat m_wy;
  mat m_emb;
  mat m_suf;
  mat m_cap;

  mat m_dep_dist_mat;

  mat m_supercat_emb; // supercat embedding matrix
  mat m_action_emb;

  mat m_h_tm1;

  mat m_emb_all; // all Turain50 embeddings
  mat m_comp_mat;
  mat m_phrase_mat; // only used at training time

  //  size_t m_total_emb_dim_plus_unks;
  //  size_t m_total_suf_dim_plus_unk;
  //  size_t m_total_cap_dim;

  size_t m_unary_base_count;
  size_t m_reduce_base_count;

  RNNCanonical m_cats_map;
  size_t m_cat_ind;

  bool m_use_supercat_feats;
  string m_activation;
  size_t m_UNK_SUFFIX_IND; // 1st col in m_suf, 0th col is for empty feat
  size_t m_UNK_ALPHNUM_LOWER_IND = m_vocsize;
  size_t m_UNK_ALPHNUM_UPPER_IND = m_vocsize + 1;
  size_t m_UNK_NON_ALPHNUM_IND = m_vocsize + 2;
  unordered_map<std::string, size_t> m_pos_str_ind_map;
  unordered_map<std::string, size_t> m_suf_str_ind_map;

  bool m_dropout;
  double m_dropout_success_prob;
  mat m_dropout_vec;

  int m_total_reduce_count;
  const size_t m_phrase_emb_ind_base = 1000000;
  const size_t m_phrase_emb_feat_count = 4;

  // for sanity check only
  unordered_map<size_t, size_t> m_emb_ind_map;
  string m_config_dir;
  double m_avg_unique_prev_hypo_count; // per sent
  double m_total_unique_prev_hypo_count;

  mat m_wx_super_sub;

  //

  struct KeyHash {
    std::size_t operator() (const ShiftReduceAction& k) const {
      Hash h = k.GetSuperCat()->ehash();
      h += k.GetSuperCat()->m_left_ind;
      h += k.GetSuperCat()->m_right_ind;
      h += k.GetAction();
      return (size_t)h.value();
    }
  };

  struct KeyEqual {
    bool operator() (const ShiftReduceAction& k1, const ShiftReduceAction& k2) const {
      return k1.GetAction() == k2.GetAction() &&
          k1.GetSuperCat()->m_left_ind == k2.GetSuperCat()->m_left_ind &&
          k1.GetSuperCat()->m_right_ind == k2.GetSuperCat()->m_right_ind &&
          eqSR(k1.GetCat(), k2.GetCat());
    }
  };

  class Hypo_comp {
    bool reverse;
  public:
    Hypo_comp(const bool& reve=false)
  {reverse=reve;}
    bool operator() (ShiftReduceHypothesis *&h1, ShiftReduceHypothesis *&h2) const
    {
      if (reverse) return (h1->GetTotalScore()/h1->GetTotalActionCount()
          > h2->GetTotalScore()/h2->GetTotalActionCount());
      else return (h1->GetTotalScore()/h1->GetTotalActionCount()
          < h2->GetTotalScore()/h2->GetTotalActionCount());
    }
  };

  std::unordered_map<ShiftReduceAction, size_t, KeyHash, KeyEqual> m_gold_equiv_map;

  _Impl(const RNNConfig &cfg,
        Sentence &sent,
        Categories &cats,
        const size_t srbeam, const string& config_dir, bool train);
  ~_Impl(void)
  {
    std::cerr << "_parser.h destructor" << std::endl;
    delete [] weights;
    chart.reset();
    //chart.~Chart();
    //delete m_shiftReduceFeatures;
    //delete m_lattice;
    //m_shiftReducePool->clear();
  };
  void load_lex_cat_dict(const string &raw_cats_from_markedup);

  void sr_parse_train(const double BETA, const size_t sent_id);
  const ShiftReduceHypothesis* sr_parse(const double BETA, size_t sent_id);
  const ShiftReduceHypothesis* sr_parse_beam_search(const size_t k, const size_t sent_id,
                                                    const bool oracle=false, const bool train=false);
  const ShiftReduceHypothesis* sr_parse_max_margin(const double BETA, const size_t sent_id);

  void construct_hypo(HypothesisPQueue &hypoQueue, ShiftReduceLattice *lattice,
                      const ShiftReduceHypothesis *&bestHypo, const ShiftReduceHypothesis *&goldHypo,
                      ShiftReduceAction *goldAction, const bool goldFinished);
  void construct_hypo_max_margin(HypothesisPQueue &hypoQueue, ShiftReduceLattice *lattice,
                                 const ShiftReduceHypothesis *&bestHypo, const ShiftReduceHypothesis *&goldHypo,
                                 ShiftReduceAction *goldAction, const bool goldFinished, const size_t current_gold_action_count);
  void count_gold_actions(ShiftReduceHypothesis *&hypo,
                          ShiftReduceAction &action);
  void _action(const ShiftReduceHypothesis* hypo, size_t action, ShiftReduceLattice *lattice,
               const SuperCat *supercat, const double totalScore, const size_t action_id = 0, const double action_score=0.0);
  void _add_lex(const Cat *cat, const SuperCat *sc,
                bool replace, RuleID ruleid,
                std::vector<SuperCat *> &tmp,
                const size_t tc_id = 0);
  void unary_tc(const SuperCat *sc, bool qu_parsing, std::vector<SuperCat *> &tmp);
  void unary_tr(const SuperCat *supercat, std::vector<SuperCat *> &tmp);

  //static Pool *GetFeaturePool() { return m_shiftReduceFeaturePool; }
  void load_gold_trees(const std::string &filename);
  void load_gold_trees_rnn_fmt(const std::string &filename);

  // clear cats etc., hashtables between iterations
  void Clear();
  void clear_xf1();

  void raws2words(const Raws &raw, Words &words) const;
  void words2word_inds(const vector<std::string> &raw, const vector<std::string> &pos,
                       std::vector<size_t> &word_inds_vec,
                       std::vector<size_t> &suf_inds_vec) const;

  void pos2pos_inds(const vector<std::string> &raw,
                    std::vector<size_t> &pos_inds_vec) const;
  void suf2suf_inds(const vector<std::string> &raw,
                    std::vector<size_t> &pos_inds_vec) const;

  void suf_cap2suf_cap_inds(const vector<std::string> &raw,
                            std::vector<size_t> &suf_inds_vec,
                            std::vector<size_t> &cap_inds_vec) const;

  const ShiftReduceHypothesis* sr_parser_train(const double BETA, const size_t sent_id);


  void contextwin2minibatch(const vector<vector<vector<size_t> > > &sent_all_context,
                            vector<vector<vector<vector<vector<size_t> > > > > &minibatch_all_data);

  void labels2minibatch(const vector<size_t> &sent_all_labels,
                        vector<vector<vector<size_t> > > &minibatch_all_data);
  void feasible_y_vals2minibatch(const vector<vector<size_t> >&sent_all_feasible_y_vals,
                                 vector<vector<vector<vector<size_t> > > > &minibatch_all_data);
  void train(const size_t total_epochs, const string &model_path);

  double train_bptt_multi_emb(const vector<vector<vector<size_t> > > &context_batch,
                              const vector<size_t> &label_batch,
                              const vector<vector<size_t> > &feasible_y_vals_batch);

  double train_bptt_multi_emb_dropout(const vector<vector<vector<size_t> > > &context_batch,
                                      const vector<size_t> &label_batch,
                                      const vector<vector<size_t> > &feasible_y_vals_batch);

  pair<mat, mat> calc_lh_proj_emb(int step,
                                  const vector<vector<size_t> > &context,
                                  mat *x_vals, mat &h_tm1, mat *x_mask_vals);

  pair<mat, mat> calc_lh_proj_emb_grad_check(int step,
                                             const vector<vector<size_t> > &context,
                                             mat *x_vals, mat &h_tm1, mat &wx, mat &wh,
                                             mat &emb, mat &suf, mat &cap, mat &super_emb);

  void calc_lh_proj_emb_test(const vector<vector<size_t> > &context,
                             const mat & h_tm1,
                             mat &new_hidden_states);

  void calc_lh_proj_emb_test(const vector<vector<size_t> > &context,
                             const mat & h_tm1,
                             mat &new_hidden_states,
                             colvec &converted_context_vec);

  mat calc_lh_proj_emb_max_margin(const vector<vector<size_t> > &context,
                                  mat &h_tm1,
                                  colvec &current_hypo_converted_context_vec,
                                  colvec &new_x_mask_val);


  void train_bptt_multi_emb_grad_check(const vector<vector<vector<size_t> > > &context_batch,
                                       const vector<size_t> &label_batch,
                                       const vector<vector<size_t> > &feasible_y_vals_batch,
                                       size_t check);

  void update_rnn(const ShiftReduceHypothesis *hypo, const bool negative,
                  mat &grad_wh, mat &grad_wx, mat &grad_wy);

  void bptt_dropout_max_margin(const vector<vector<vector<size_t> > > &context_batch,
                               const vector<mat> &lh_vals,
                               const vector<mat> &lh_deriv_vals,
                               const vector<mat> &x_vals,
                               const vector<mat> &x_mask_vals,
                               const vector<mat> &delta_y_vals,
                               mat &grad_wh, mat &grad_wx, mat &grad_wy,
                               bool negative);

  pair<size_t, double>
  score_context(const vector<vector<size_t> > &context,
                const vector<size_t> &feasible_y_vals);
  mat score_context_beam_search(const vector<vector<size_t>> &context,
                                const vector<size_t> &feasible_y_vals,
                                mat &output_vals_feasible,
                                const mat &prev_hidden_states,
                                mat &new_hidden_states);
  void score_context_beam_search_xf1(const vector<vector<size_t>> &context,
                                     const vector<size_t> &feasible_y_vals,
                                     mat &output_vals_feasible,
                                     const mat &prev_hidden_states,
                                     mat &new_hidden_states,
                                     colvec &converted_context_vec);
  void compose_word_vec(const int left_ind, const int right_ind,
                        SuperCat *&supercat);
  void score_context_max_margin_no_softmax(const vector<vector<size_t> > &context,
                                           mat &h_tm1,
                                           mat &new_hidden_state,
                                           colvec &converted_context,
                                           colvec &mask_val,
                                           mat &ly_val);
  mat score_context_max_margin_softmax(const vector<vector<size_t> > &context,
                                       const vector<size_t> &feasible_y_vals,
                                       mat &output_vals_feasible,
                                       mat &h_tm1,
                                       mat &new_hidden_state,
                                       colvec &converted_context,
                                       colvec &mask_val);

  mat sigmoid(const mat &m);
  mat soft_max(const mat &y);
  double x_ent_multi(const mat&y, const mat&t);
  void load_emb_mat(mat &emb, const string &filename);
  void load_pos_emb_mat(mat &emb, const string &filename);
  void load_super_emb_mat(mat &emb, const string &filename);
  void re_init_mats(const string &wx, const string &wh, const string &wy,
                    const string &emb, const string &suf,
                    const string &super_emb, const string &comp_mat);

  void dump_supercat_map(const std::string &path);
  void load_supercat_map(const string &file, bool init=false);
  void load_pos_str_ind_map(const string &file);
  void load_suf_str_ind_map(const string &file);
  void load_emb_word_ind_map(const string &filename);

  void clean_str(string &word, vector<string> &ret) const;
  string get_suf(const string &word) const;
  vector<size_t> word2vec(const string &word) const;
  bool is_lower(const string &word) const;
  bool is_alphnum(const string &word) const;
  void replace_all_substrs(string &str, const string &substr,  const string &re) const;
  void save_weights(const size_t epoch, const string &model_path);

  // unit tests
  void test_contextwin2minibatch();
  void test_labels2minibatch();

  // XF1 training related
  struct CCGDep {
    friend std::ostream &operator<<(std::ostream&, const CCGDep&);
    const ulong sentId;
    const ulong head;
    const ulong markedup;
    const ulong slot;
    const ulong arg;
    //const ulong rule_id;

    CCGDep(const ulong sentId, ulong head,
           ulong markedup, ulong slot, ulong arg)
    : sentId(sentId), head(head), markedup(markedup), slot(slot), arg(arg) {}

  };

  struct dep_hash_1
  {
    ulong operator() (const tuple<ulong, ulong, ulong> &d) const {
      ulong h = 0;
      h += hash<ulong>()(std::get<0>(d));
      h += hash<ulong>()(std::get<1>(d));
      h += hash<ulong>()(std::get<2>(d));
      return h;
    }
  };

  struct dep_eq_1 {
    bool operator() (const tuple<ulong, ulong, ulong> &x,
                     const tuple<ulong, ulong, ulong> &y) const {
      return std::get<0>(x) == std::get<0>(y) &&
          std::get<1>(x) == std::get<1>(y) &&
          std::get<2>(x) == std::get<2>(y);
    }
  };

  struct dep_hash_2 {
    ulong operator() (const tuple<string, ulong, ulong, ulong> &d) const {
      ulong h = 0;
      h += hash<string>()(std::get<0>(d));
      h += hash<ulong>()(std::get<1>(d));
      h += hash<ulong>()(std::get<2>(d));
      h += hash<ulong>()(std::get<3>(d));
      return h;
    }
  };

  struct dep_eq_2 {
    bool operator() (const tuple<string, ulong, ulong, ulong> &x,
                     const tuple<string, ulong, ulong, ulong> &y) const {
      return std::get<0>(x) == std::get<0>(y) &&
          std::get<1>(x) == std::get<1>(y) &&
          std::get<2>(x) == std::get<2>(y) &&
          std::get<3>(x) == std::get<3>(y);
    }
  };

  struct dep_hash_3 {
    ulong operator() (const tuple<string, ulong, ulong, string, ulong> &d) const {
      ulong h = 0;
      h += hash<string>()(std::get<0>(d));
      h += hash<ulong>()(std::get<1>(d));
      h += hash<ulong>()(std::get<2>(d));
      h += hash<string>()(std::get<3>(d));
      h += hash<ulong>()(std::get<4>(d));
      return h;
    }
  };

  struct dep_eq_3 {
    bool operator() (const tuple<string, ulong, ulong, string, ulong> &x,
                     const tuple<string, ulong, ulong, string, ulong> &y) const {
      return std::get<0>(x) == std::get<0>(y) &&
          std::get<1>(x) == std::get<1>(y) &&
          std::get<2>(x) == std::get<2>(y) &&
          std::get<3>(x) == std::get<3>(y) &&
          std::get<4>(x) == std::get<4>(y);
    }
  };

  struct ccg_dep_equal {
    bool operator() (const CCGDep &d1, const CCGDep &d2) const
    {
      return d1.sentId == d2.sentId &&
          d1.head == d2.head &&
          d1.markedup == d2.markedup &&
          d1.slot == d2.slot &&
          d1.arg == d2.arg;
    }
  };

  struct ccg_dep_hash {
    ulong operator() (const CCGDep& d) const {
      ulong h = 0;
      h += d.sentId;
      h += d.head;
      h += d.markedup;
      h += d.slot;
      h += d.arg;
      return std::hash<ulong>()(h);
    }
  };

  unordered_map<ulong, ushort> m_deps_ignore_map_rule;
  unordered_map<tuple<ulong, ulong, ulong>, ushort, dep_hash_1, dep_eq_1> m_deps_ignore_map_1;
  unordered_map<tuple<string, ulong, ulong, ulong>, ushort, dep_hash_2, dep_eq_2> m_deps_ignore_map_2;
  unordered_map<tuple<string, ulong, ulong, string, ulong>, ushort, dep_hash_3, dep_eq_3> m_deps_ignore_map_3;

  unordered_map<ulong, size_t> m_ccg_gold_dep_count_map_train;
  unordered_map<ulong, size_t> m_ccg_gold_dep_count_map_dev;

  unordered_map<string, size_t> m_unk_markedup_map;
  unordered_map<CCGDep, unsigned int, ccg_dep_hash, ccg_dep_equal>  m_ccg_dep_map_train;
  unordered_map<CCGDep, unsigned int, ccg_dep_hash, ccg_dep_equal>  m_ccg_dep_map_dev;

  void init_xf1_training(bool train);
  void load_gold_deps(const string &filename, bool train);
  void init_ignore_rules();
  void load_dep_ignore_list(const std::string &filename);
  void load_dep_ignore_list2(const std::string &filename);
  void load_dep_ignore_list3(const std::string &filename);
  pair<double, double> count_gold_deps(const SuperCat *sc, const ulong id, bool train=true);
  bool ignore_dep(const CCG::Filled *&filled);
  double calc_f1(const ShiftReduceHypothesis *&output, const size_t id, bool train);
  void test_internal_f1_calc(size_t sent_id);
  void get_deps(const CCG::SuperCat *sc, const ulong id,
                double &gold, double &total, const int FMT, bool train);
  double get_total_sent_gold_deps(const size_t id, const bool train);
  const ShiftReduceHypothesis* sr_parse_train_xf1(const double BETA, const size_t sent_id);
  void train_xf1(double &product, const ShiftReduceHypothesis *&hypo,
                 mat &grad_wx, mat &grad_wh, mat &grad_wy);
  void train_xf1_mt(const double &product, const ShiftReduceHypothesis *&hypo,
                    mat &grad_wx, mat &grad_wh, mat &grad_wy, vector<mat> &delta_x_all_steps);
  void train_xf1_bptt(const size_t steps, double &product, const vector<vector<vector<size_t>>> &context_batch,
                      const vector<mat> &x_vals, const vector<mat> &h_vals,
                      const vector<mat> &h_deriv_vals, const vector<mat> &y_val_batch,
                      const vector<size_t> &y_ind_batch, const vector<mat> &y_all_ind_batch,
                      mat &grad_wx, mat &grad_wh, mat &grad_wy);
  void train_xf1_bptt(const size_t total_steps, double &product, const vector<vector<size_t>> *context_batch,
                      const mat *x_vals, const mat *h_vals,
                      const mat *h_deriv_vals, const mat *y_val_batch,
                      const size_t *y_ind_batch, const mat *y_all_ind_batch,
                      mat &grad_wx, mat &grad_wh, mat &grad_wy);
  void train_xf1_grad_check(double &product, const ShiftReduceHypothesis *&hypo,
                            mat &grad_wx, mat &grad_wh, mat &grad_wy, double &bp_grad_emb00);
  void train_xf1_bptt_grad_check(const size_t steps, double &product, const vector<vector<size_t>> *context_batch,
                                 const mat *x_vals, const mat *h_vals,
                                 const mat *h_deriv_vals, const mat *y_val_batch,
                                 const size_t *y_ind_batch, const mat *y_all_ind_batch,
                                 mat &grad_wx, mat &grad_wh, mat &grad_wy, double &bp_grad_emb);
  void train_xf1_bptt_mt(const size_t &steps, const double &product, const vector<vector<size_t>> *context_batch,
                         const mat *x_vals, const mat *h_vals,
                         const mat *h_deriv_vals, const mat *y_val_batch,
                         const size_t *y_ind_batch, const mat *y_all_ind_batch,
                         mat &grad_wx, mat &grad_wh, mat &grad_wy,
                         vector<mat> &delta_x_all_steps, const size_t &batch_id);
  void train_xf1_bptt_mt(const size_t &steps, const double &product,
                         const vector<mat> &x_vals, const vector<mat> &h_vals,
                         const vector<mat> &h_deriv_vals, const vector<mat> &y_val_batch,
                         const vector<size_t> &y_ind_batch, const vector<mat> &y_all_ind_batch,
                         mat &grad_wx, mat &grad_wh, mat &grad_wy,
                         vector<mat> &delta_x_all_steps, const size_t &batch_id);
  void xf1_mt(const size_t num_threads, const size_t ind, const size_t max_action_count, const mat &pxf1f1,
              vector<const ShiftReduceHypothesis*> &kbest_output_vec);
  void calc_lh_proj_emb_xf1(const vector<vector<size_t> > &context, const mat &h_tm1,
                            mat &new_hidden_states,
                            colvec &converted_context_vec);
  void calc_lh_proj_emb_xf1_cached(const vector<vector<size_t> > &context,
                                   const mat &h_tm1,
                                   mat &new_hidden_states,
                                   colvec &converted_context_vec,
                                   const bool train_xf1);
  const ShiftReduceHypothesis* sr_parse_beam_search_train_xf1(NLP::Taggers::RnnTagger &rnn_super,
                                                              const size_t k, const size_t sent_id,
                                                              const bool oracle, const bool train,
                                                              double &total_xf1,
                                                              double &total_parses,
                                                              const size_t &num_threads,
                                                              const bool &bptt_graph,
                                                              const bool &run_training);

  double sr_parse_beam_search_train_xf1_grad_check(const size_t k, const size_t sent_id);
  double sr_parse_beam_search_train_xf1_grad_check_super(NLP::Taggers::RnnTagger &rnn_super,
                                                         const size_t k, const size_t sent_id,
                                                         const bool oracle, const bool train);
  void xf1_grad_check(NLP::Taggers::RnnTagger &rnn_super, const size_t k, const size_t sent_id,
                      double &total_parses, const size_t num_threads);
  void xf1_grad_check_super(NLP::Taggers::RnnTagger &rnn_super,
                                         const size_t k, const size_t sent_id,
                                         double &total_parses);
  void mark_nodes(const ShiftReduceHypothesis *&hypo, const size_t &ind);

  void precompute_all_positions(unordered_map<size_t, unordered_map<size_t, size_t>> &map,
                                const size_t &k,
                                const bool word);
  void precompute_position(unordered_map<size_t, size_t> &map,
                           const size_t &k,
                           vector<size_t> &top_emb_inds);
  void precompute(const size_t &k);
  void convert_n_save(const unordered_map<size_t, unordered_map<size_t, mat>> &map,
                      const bool word);
  void load_lh_cache(const string &path,
                     unordered_map<size_t, unordered_map<size_t, mat>> &map);

};
}
}

