/* -*- Mode: C++; -*- */
// C&C NLP tools
// Copyright (c) Universities of Edinburgh, Oxford and Sydney
// Copyright (c) James R. Curran
//
// This software is covered by a non-commercial use licence.
// See LICENCE.txt for the full text of the licence.
//
// If LICENCE.txt is not included in this distribution
// please email candc@it.usyd.edu.au to obtain a copy.

#pragma once

#include "parser/fixed.h"
#include "parser/atom.h"
#include "parser/feature.h"
#include "parser/varid.h"
#include "parser/category.h"

#include "parser/markedup.h"
#include "parser/gr_constraints.h"
#include "parser/gr.h"
#include "parser/relation.h"
#include "parser/relations.h"
#include "parser/canonical.h"
#include "parser/categories.h"

#include "parser/decoder.h"

// added
#include "config/options.h"
#include "config/config.h"
#include "rnn_tagger.h"


namespace NLP {

using namespace Config;

namespace CCG {

class InsideOutside;
class ShiftReduceHypothesis;

class RNNParser {
public:
  class RNNConfig: public Directory {
  public:
    OpPath cats;
    OpPath markedup;
    OpPath weights;
    OpPath rules;

    std::string lexicon(void) const { return derived_path(path, "lexicon"); }
    std::string features(void) const { return derived_path(path, "features"); }

    Op<ulong> maxwords;
    Op<ulong> maxsupercats;

    Op<bool> alt_markedup;
    Op<bool> seen_rules;
    Op<bool> extra_rules;
    Op<bool> question_rules;
    Op<bool> eisner_nf;
    Op<bool> partial_gold;
    Op<double> beam;

    Op<bool> allowFrag;
    Op<ulong> bs;
    Op<ulong> ds;
    Op<ulong> dc;
    Op<ulong> de;
    Op<ulong> dsuper;
    Op<ulong> daction;

    Op<ulong> nh;
    Op<double> lr;
    Op<ulong> head_limit;
    Op<bool> use_pos;
    Op<bool> use_supercat_feats;
    Op<bool> use_phrase_emb_feats;
    Op<std::string> activation;
    Op<std::string> word_emb_file;
    Op<std::string> pos_emb_file;
    Op<std::string> super_emb_file;
    Op<bool> use_dropout;
    Op<bool> xf1_mini_batch;
    Op<double> dropout_success_prob;
    Op<bool> use_structrue_loss;
    Op<double> loss_pen;
    Op<bool> precompute;
    Op<bool> use_lh_cache;
    Op<bool> xf1_super;


    RNNConfig(const OpPath *base = 0, const std::string &name = "rnn_parser",
              const std::string &desc = "rnn parser config");
  };
  public:

  static const ulong LOAD_WEIGHTS = 2;
  static const ulong LOAD_FEATURES = 1;
  static const ulong LOAD_NONE = 0;

  //static Pool *s_shiftReduceFeaturePool;

  RNNParser();
  RNNParser(const RNNConfig &cfg,
            Sentence &sent,
            Categories &cats,
            const size_t srbeam, const std::string &config_dir,
            const bool train);
  ~RNNParser(void);

  void load_pos_str_ind_map(const std::string &filename);
  void load_lex_cat_dict(const std::string &filename);
  void load_gold_trees(const std::string &filename);
  void load_gold_trees_rnn_fmt(const std::string &filename);
  void Clear();
  void clear_xf1();

  const ShiftReduceHypothesis* sr_parse(const double BETA, const size_t sent_id);
  const ShiftReduceHypothesis* sr_parse_beam_search(const size_t k, const size_t sent_id,
                                                    const bool oracle=false, const bool train=false);

  void sr_parse_train(const double BETA, const size_t sent_id);
  void sr_parse_train_max_margin(const double BETA, const size_t sent_id);
  void train(const size_t total_epochs, const std::string &model_path);

  void re_init_mats(const std::string &wx, const std::string &wh, const std::string &wy,
                    const std::string &emb, const std::string &suf, const std::string &cap,
                    const std::string &super_emb, const std::string &comp_mat);

  void load_supercat_map(const std::string &file);
  void dump_supercat_map(std::string &path);
  void load_emb_mat(const std::string &file);
  void load_emb_mat_full(const std::string &file);
  void save_weights(const size_t epoch, const std::string &model_path);

  void init_xf1_training(bool train);
  void get_deps(const CCG::SuperCat *sc, const ulong id,
                double &gold, double &total, const int FMT, bool train);
  double get_total_sent_gold_deps(const size_t id, const bool train);
  void xf1_grad_check(NLP::Taggers::RnnTagger &rnn_super, const size_t k, const size_t sent, double &total_parses, const size_t num_threads);
  void xf1_grad_check_super(NLP::Taggers::RnnTagger &rnn_super, const size_t k, const size_t sent, double &total_parses);
  const ShiftReduceHypothesis* sr_parse_beam_search_train_xf1(NLP::Taggers::RnnTagger &rnn_super,

                                                              const size_t k, const size_t sent, const bool oracle,
                                                              const bool train,
                                                              double &total_xf1, double &total_parses,
                                                              const size_t &num_threads,
                                                              const bool &bptt_graph,
                                                              const bool &run_training);
  void precompute(const size_t &k);

  private:
  class _Impl;
  _Impl *_impl;
};

}
}
