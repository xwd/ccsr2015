#ifndef SRC_INCLUDE_RNN_PARSER_CONTEXT_LOAD_POS_H_
#define SRC_INCLUDE_RNN_PARSER_CONTEXT_LOAD_POS_H_

#pragma once

#include <unordered_map>
#include "parser/ShiftReduceHypothesis.h"
#include <armadillo>
#include "rnn_parser/rnn_canonical.h"

using namespace std;

const static size_t empty_supercat_feat_ind = 0; // for empty feat
const static size_t unk_supercat_ind = 1; // for unk feat
const static size_t empty_phrase_emb_ind = 0; // for empty feat
const static size_t null_child_word_vec_ind = 2000000;

namespace NLP { namespace CCG {

class ShiftReduceHypothesis;

class ContextLoader {

public:

  const SuperCat *s0c, *s1c, *s2c, *s3c;

  int q0w, q1w, q2w, q3w;
  int q0p, q1p, q2p, q3p;

  // super feats related
  int s_s0w, s_s1w, s_s2w, s_s3w;
  int s_s0p, s_s1p, s_s2p, s_s3p;

  size_t m_emb_empty_col; // for padding
  size_t m_suf_empty_col; // for padding

  size_t m_head_limit;

  // this keeps track of the total number of
  // cats seen so far during *training*
  size_t &m_cat_ind;
  bool m_train;
  //size_t m_action_feat;

  ContextLoader(size_t &cat_ind, size_t head_limit, const bool train)
  :s0c(0), s1c(0), s2c(0), s3c(0),

   q0w(0), q1w(0), q2w(0), q3w(0),
   q0p(0), q1p(0), q2p(0), q3p(0),

   s_s0w(0), s_s1w(0), s_s2w(0), s_s3w(0),
   s_s0p(0), s_s1p(0), s_s2p(0), s_s3p(0),

   m_emb_empty_col(0),
   m_suf_empty_col(0),
   m_head_limit(head_limit),
   m_cat_ind(cat_ind),
   m_train(train) {}

  ~ContextLoader() {

    s0c = 0;
    s1c = 0;
    s2c = 0;
    s3c = 0;

  }

  const size_t canonize2ind(const Cat *cat,
                            RNNCanonical &canonical) {

    size_t ind = canonical.get_ind(cat);

    if (!m_train) {
      // at test time just return ind,
      // ind == 1 means the given cat was
      // not seen during training
      return ind;

    } else {

      // at training time, insert an unseen
      // cat into canonical
      // in RNN Canonical hashmap,
      // it should be made to reutrn unk_supercat_ind
      // for an unk supercat, it's currently set to 1
      if (ind == 1) {
        ++m_cat_ind;
        canonical.add(cat, m_cat_ind);
        return m_cat_ind;

      } else {
        assert(ind >= 2);
        return ind;
      }

    }
  }


  void load_sc_context(const ShiftReduceHypothesis *hypo,
                       const SuperCat *sc,
                       vector<size_t> &supercat_inds_ret,
                       RNNCanonical &cats_map,
                       const size_t cat_ind) {

    size_t ind = 0;

    // s0c
    if (sc != 0) {

      assert(sc->cat);
      ind = canonize2ind(sc->cat, cats_map);
      if (m_train) assert(ind != unk_supercat_ind);
      supercat_inds_ret.emplace_back(ind); // s0C

      if (sc->left != 0 && sc->right != 0) {

        assert(sc->left->cat);
        assert(sc->right->cat);

        ind = canonize2ind(sc->left->cat, cats_map);
        if (m_train) assert(ind != unk_supercat_ind);
        supercat_inds_ret.emplace_back(ind); // s0Lc

        ind = canonize2ind(sc->right->cat, cats_map);
        if (m_train) assert(ind != unk_supercat_ind);
        supercat_inds_ret.emplace_back(ind); // s0Rc

        supercat_inds_ret.emplace_back(empty_supercat_feat_ind); // s0Uc

      } else if (sc->left != 0 && sc->right == 0) {

        if (cat_ind == 0)
          assert(hypo->GetStackTopAction() == UNARY);
        if (cat_ind == 1)
          assert(hypo->GetPrvStack()->GetStackTopAction() == UNARY);

        supercat_inds_ret.emplace_back(empty_supercat_feat_ind); // s0Lc
        supercat_inds_ret.emplace_back(empty_supercat_feat_ind); // s0Rc

        ind = canonize2ind(sc->left->cat, cats_map);
        if (m_train) assert(ind != unk_supercat_ind);
        supercat_inds_ret.emplace_back(ind); // s0Uc

      } else {

        assert(sc->left == 0);
        assert(sc->right == 0);

        supercat_inds_ret.emplace_back(empty_supercat_feat_ind); // s0Lc
        supercat_inds_ret.emplace_back(empty_supercat_feat_ind); // s0Rc
        supercat_inds_ret.emplace_back(empty_supercat_feat_ind); // s0Uc

      }

    } else {

      supercat_inds_ret.emplace_back(empty_supercat_feat_ind); // s0c
      supercat_inds_ret.emplace_back(empty_supercat_feat_ind); // s0Lc
      supercat_inds_ret.emplace_back(empty_supercat_feat_ind); // s0Rc
      supercat_inds_ret.emplace_back(empty_supercat_feat_ind); // s0Uc

    }

  }


  void load_head_context(const SuperCat *sc,
                         const vector<size_t> &word_inds,
                         const vector<size_t> &pos_inds,
                         vector<size_t> &word_inds_ret,
                         vector<size_t> &pos_inds_ret) {

    vector<size_t> sw_vec;
    vector<size_t> sp_vec;

    if (sc != 0) {
      assert(sc->cat);
      get_values(true, &sc->vars[sc->cat->var], sw_vec, sp_vec,
                 word_inds, pos_inds);

      if (sw_vec.size() > 0) {

        assert(sw_vec.size() == sp_vec.size());

        for (size_t i = 0; i < sw_vec.size(); ++i) {
          word_inds_ret.emplace_back(sw_vec[i]);
          pos_inds_ret.emplace_back(sp_vec[i]);
          if (i + 1 == m_head_limit)
            break;
        }

        if (sw_vec.size() < m_head_limit) {
          size_t diff = m_head_limit - sw_vec.size();
          for (size_t j = 0; j < diff; ++j) {
            word_inds_ret.emplace_back(m_emb_empty_col);
            pos_inds_ret.emplace_back(m_suf_empty_col);
          }
        }

      } else {
        for (size_t k = 0; k < m_head_limit; ++k) {
          word_inds_ret.emplace_back(m_emb_empty_col);
          pos_inds_ret.emplace_back(m_suf_empty_col);
        }
      }

    } else {
      for (size_t k = 0; k < m_head_limit; ++k) {
        word_inds_ret.emplace_back(m_emb_empty_col);
        pos_inds_ret.emplace_back(m_suf_empty_col);
      }
    }

  }


  void load_lr_context(const SuperCat *sc,
                       const vector<size_t> &word_inds,
                       const vector<size_t> &pos_inds,
                       vector<size_t> &word_inds_ret,
                       vector<size_t> &pos_inds_ret) {

    vector<size_t> slw_vec;
    vector<size_t> slp_vec;

    vector<size_t> srw_vec;
    vector<size_t> srp_vec;

    vector<size_t> suw_vec;
    vector<size_t> sup_vec;

    if (sc != 0) {

      assert(sc->cat);

      if (sc->left != 0 && sc->right != 0) {

        assert(sc->left->cat);
        assert(sc->right->cat);

        get_values(true, &sc->left->vars[sc->left->cat->var], slw_vec, slp_vec,
                   word_inds, pos_inds);

        fill_inds(slw_vec, slp_vec, word_inds_ret, pos_inds_ret);

        get_values(true, &sc->right->vars[sc->right->cat->var], srw_vec, srp_vec,
                   word_inds, pos_inds);

        fill_inds(srw_vec, srp_vec, word_inds_ret, pos_inds_ret);

        // s0u
        for (size_t k = 0; k < m_head_limit; ++k) {
          word_inds_ret.emplace_back(m_emb_empty_col);
          pos_inds_ret.emplace_back(m_suf_empty_col);
        }

      } else if (sc->left != 0 && sc->right == 0) {

        // j < 2, since two feats: s0lw, s0rw
        for (size_t j = 0; j < 2; ++j) {
          for (size_t k = 0; k < m_head_limit; ++k) {
            word_inds_ret.emplace_back(m_emb_empty_col);
            pos_inds_ret.emplace_back(m_suf_empty_col);
          }
        }

        assert(sc->left->cat);

        get_values(true, &sc->left->vars[sc->left->cat->var], suw_vec, sup_vec,
                   word_inds, pos_inds);

        fill_inds(suw_vec, sup_vec, word_inds_ret, pos_inds_ret);

      } else {

        assert(sc->left == 0);
        assert(sc->right == 0);

        // j < 3, since three feats: s0lw, s0rw, s0uw
        for (size_t j = 0; j < 3; ++j) {
          for (size_t k = 0; k < m_head_limit; ++k) {
            word_inds_ret.emplace_back(m_emb_empty_col);
            pos_inds_ret.emplace_back(m_suf_empty_col);
          }
        }

      }

    } else {
      // j < 3, since three feats: s0lw, s0rw, s0uw
      for (size_t j = 0; j < 3; ++j) {
        for (size_t k = 0; k < m_head_limit; ++k) {
          word_inds_ret.emplace_back(m_emb_empty_col);
          pos_inds_ret.emplace_back(m_suf_empty_col);
        }
      }
    }

  }


  ushort calc_dist(unsigned char l, unsigned char r) {

    ushort start = l - 1;
    ushort end = r - 1;

    ushort words = end - start - 1;
    if (words > 2)
      words = 2;
    return words;

//    ushort start = l - 1;
//    ushort end = r - 1;
//
//    if(type == DIST_ADJ_HEAD || type == DIST_ADJ_POS){
//      ushort words = end - start - 1;
//      if(words > 2)
//        words = 2;
//      return words;
//    }

  }


  void load_dep_context(const SuperCat *sc,
                        const vector<size_t> &word_inds,
                        const vector<size_t> &pos_inds,
                        vector<size_t> &word_inds_ret,
                        vector<size_t> &pos_inds_ret,
                        vector<size_t> &supercat_inds_ret,
                        vector<size_t> &dep_dist_ind_ret,
                        RNNCanonical &cats_map,
                        Categories &cats) {

    assert(sc->cat);
    size_t total = 0;

    for(const Filled *filled = sc->filled; filled; filled = filled->next) {

      total += 1;

      size_t head = word_inds[filled->head - 1];
      size_t arg = word_inds[filled->filler - 1];

      size_t head_pos = pos_inds[filled->head - 1];
      size_t arg_pos = pos_inds[filled->filler - 1];

      word_inds_ret.emplace_back(head);
      word_inds_ret.emplace_back(arg);

      pos_inds_ret.emplace_back(head_pos);
      pos_inds_ret.emplace_back(arg_pos);

      size_t cat_ind = canonize2ind(cats.parse(cats.relations[filled->rel].cat), cats_map);
      if (m_train) assert(cat_ind != unk_supercat_ind);
      supercat_inds_ret.emplace_back(cat_ind);

      unsigned char lpos, rpos;
      if(filled->head < filled->filler){
        lpos = filled->head;
        rpos = filled->filler;
      }else{
        rpos = filled->head;
        lpos = filled->filler;
      }

      ushort dist = calc_dist(lpos, rpos);
      assert(dist <= 2);
      dep_dist_ind_ret.emplace_back(size_t(dist));

      break; // currently, only consider the first filled dep

     }

    if (total == 0) {
      word_inds_ret.emplace_back(0); // head
      word_inds_ret.emplace_back(0); // arg
      pos_inds_ret.emplace_back(0);
      pos_inds_ret.emplace_back(0);
      supercat_inds_ret.emplace_back(empty_supercat_feat_ind);
      // possibl dist vals are: 0, 1, 2 and 3 for padding
      dep_dist_ind_ret.emplace_back(3);
    } else {
      assert(total == 1);
    }

    //cerr << "total dep: " << total << endl;

  }

  void load_context(const ShiftReduceHypothesis *hypo,
                    vector<size_t> &word_inds_ret,
                    vector<size_t> &pos_inds_ret,
                    vector<size_t> &supercat_inds_ret,
                    vector<size_t> &phrase_emb_ind_ret,
                    vector<size_t> &child_phrase_emb_ind_ret,
                    const vector<size_t> &word_inds,
                    const vector<size_t> &pos_inds,
                    RNNCanonical &cats_map,
                    Categories &cats,
                    const bool use_phrase,
                    const bool training) {

    // word_inds, pos_inds are from the input

    assert(word_inds.size() == pos_inds.size());

    const size_t q0_ind = hypo->GetNextInputIndex();
    const size_t q1_ind = hypo->GetNextInputIndex() + 1;
    const size_t q2_ind = hypo->GetNextInputIndex() + 2;
    const size_t q3_ind = hypo->GetNextInputIndex() + 3;

    s0c = hypo->GetStackSize() >= 1 ? hypo->GetStackTopSuperCat() : 0;
    s1c = hypo->GetStackSize() >= 2 ? hypo->GetPrvStack()->GetStackTopSuperCat() : 0;
    s2c = hypo->GetStackSize() >= 3 ? hypo->GetPrvStack()->GetPrvStack()->GetStackTopSuperCat() : 0;
    s3c = hypo->GetStackSize() >= 4 ? hypo->GetPrvStack()->GetPrvStack()->GetPrvStack()->GetStackTopSuperCat() : 0;

    //    q0w = hypo->GetNextInputIndex() < words.size() ? word_inds[hypo->GetNextInputIndex()] : -1;
    //    q1w = hypo->GetNextInputIndex() + 1 < words.size() ? word_inds[hypo->GetNextInputIndex() + 1] : -1;
    //    q2w = hypo->GetNextInputIndex() + 2 < words.size() ? word_inds[hypo->GetNextInputIndex() + 2] : -1;
    //    q3w = hypo->GetNextInputIndex() + 3 < words.size() ? word_inds[hypo->GetNextInputIndex() + 3] : -1;

    // action related feats
//
//    if (s0c != 0) {
//      action_inds_ret.emplace_back(hypo->GetStackTopAction());
//    } else {
//      // shift: 0, unary: 1, reduce: 2, no action: 3
//      action_inds_ret.emplace_back(3);
//    }

    // supercat related feats
    // assuming an empty feature gets a 0 value
    // i.e., the 0th column of the cat
    // embedding matrix

    load_sc_context(hypo, s0c, supercat_inds_ret, cats_map, 0);
    load_sc_context(hypo, s1c, supercat_inds_ret, cats_map, 1);

    // s2c
    if (s2c != 0) {

      assert(s0c->cat);
      assert(s1c->cat);
      assert(s2c->cat);

      size_t ind = canonize2ind(s2c->cat, cats_map);
      if (m_train) assert(ind != unk_supercat_ind);
      supercat_inds_ret.emplace_back(ind); // s2C

    } else {
      supercat_inds_ret.emplace_back(empty_supercat_feat_ind); // s2C
    }

    // s3c
    if (s3c != 0) {

      assert(s0c->cat);
      assert(s1c->cat);
      assert(s2c->cat);
      assert(s3c->cat);

      size_t ind = canonize2ind(s3c->cat, cats_map);
      if (m_train) assert(ind != unk_supercat_ind);
      supercat_inds_ret.emplace_back(ind); // s3C

    } else {
      supercat_inds_ret.emplace_back(empty_supercat_feat_ind); // s3C
    }


    if (q0_ind < word_inds.size()) {
      q0w = word_inds[q0_ind];
      q0p = pos_inds[q0_ind];
    }

    if (q1_ind < word_inds.size()) {
      q1w = word_inds[q1_ind];
      q1p = pos_inds[q1_ind];
    }

    if (q2_ind < word_inds.size()) {
      q2w = word_inds[q2_ind];
      q2p = pos_inds[q2_ind];
    }

    if (q3_ind < word_inds.size()) {
      q3w = word_inds[q3_ind];
      q3p = pos_inds[q3_ind];
    }

    word_inds_ret.emplace_back(q0w);
    word_inds_ret.emplace_back(q1w);
    word_inds_ret.emplace_back(q2w);
    word_inds_ret.emplace_back(q3w);

    pos_inds_ret.emplace_back(q0p);
    pos_inds_ret.emplace_back(q1p);
    pos_inds_ret.emplace_back(q2p);
    pos_inds_ret.emplace_back(q3p);

    // supertagger feats
    assert(hypo->get_shift_count() == hypo->GetNextInputIndex());

    if (hypo->get_shift_count() == 0) {

      // do nothing
      // everything initialized in constructor

    } else if (hypo->get_shift_count() == 1) {

      s_s0w = word_inds[0];
      s_s0p = pos_inds[0];

    } else if (hypo->get_shift_count() == 2) {

      s_s0w = word_inds[1];
      s_s0p = pos_inds[1];

      s_s1w = word_inds[0];
      s_s1p = pos_inds[0];

    } else if (hypo->get_shift_count() == 3) {

      s_s0w = word_inds[2];
      s_s0p = pos_inds[2];

      s_s1w = word_inds[1];
      s_s1p = pos_inds[1];

      s_s2w = word_inds[0];
      s_s2p = pos_inds[0];

    } else if (hypo->get_shift_count() >= 4) {

      size_t s0_pos = hypo->GetNextInputIndex() - 1;

      s_s0w = word_inds[s0_pos];
      s_s0p = pos_inds[s0_pos];

      s_s1w = word_inds[s0_pos -1];
      s_s1p = pos_inds[s0_pos - 1];

      s_s2w = word_inds[s0_pos - 2];
      s_s2p = pos_inds[s0_pos - 2];

      s_s3w = word_inds[s0_pos - 3];
      s_s3p = pos_inds[s0_pos - 3];
    }

    word_inds_ret.emplace_back(s_s0w);
    word_inds_ret.emplace_back(s_s1w);
    word_inds_ret.emplace_back(s_s2w);
    word_inds_ret.emplace_back(s_s3w);

    pos_inds_ret.emplace_back(s_s0p);
    pos_inds_ret.emplace_back(s_s1p);
    pos_inds_ret.emplace_back(s_s2p);
    pos_inds_ret.emplace_back(s_s3p);


    // head feats (s0, s1, s2, s3)

    load_head_context(s0c, word_inds, pos_inds, word_inds_ret, pos_inds_ret);
    load_head_context(s1c, word_inds, pos_inds, word_inds_ret, pos_inds_ret);
    load_head_context(s2c, word_inds, pos_inds, word_inds_ret, pos_inds_ret);
    load_head_context(s3c, word_inds, pos_inds, word_inds_ret, pos_inds_ret);

    // s0lw, s0rw, s0uw (s1)

    load_lr_context(s0c, word_inds, pos_inds, word_inds_ret, pos_inds_ret);
    load_lr_context(s1c, word_inds, pos_inds, word_inds_ret, pos_inds_ret);

    // phrase emb feats
    if (use_phrase) {
      if (s0c) {
        assert(s0c->m_word_vec_ind != -1);
        phrase_emb_ind_ret.emplace_back(s0c->m_word_vec_ind);
        if ((!s0c->left && !s0c->right) || (s0c->left && !s0c->right)) {
          // lex or unary, no left/right phrase embedding
          child_phrase_emb_ind_ret.emplace_back(null_child_word_vec_ind);
          child_phrase_emb_ind_ret.emplace_back(null_child_word_vec_ind);
        } else {
          // binary
          assert(s0c->left && s0c->right);
          child_phrase_emb_ind_ret.emplace_back(s0c->left->m_word_vec_ind);
          child_phrase_emb_ind_ret.emplace_back(s0c->right->m_word_vec_ind);
        }

      } else {
        phrase_emb_ind_ret.emplace_back(empty_phrase_emb_ind);
        child_phrase_emb_ind_ret.emplace_back(null_child_word_vec_ind);
        child_phrase_emb_ind_ret.emplace_back(null_child_word_vec_ind);
      }

      if (s1c) {
        assert(s1c->m_word_vec_ind != -1);
        phrase_emb_ind_ret.emplace_back(s1c->m_word_vec_ind);
        if ((!s1c->left && !s1c->right) || (s1c->left && !s1c->right)) {
          // lex or unary, no left/right phrase embedding
          child_phrase_emb_ind_ret.emplace_back(null_child_word_vec_ind);
          child_phrase_emb_ind_ret.emplace_back(null_child_word_vec_ind);
        } else {
          // binary
          assert(s1c->left && s1c->right);
          child_phrase_emb_ind_ret.emplace_back(s1c->left->m_word_vec_ind);
          child_phrase_emb_ind_ret.emplace_back(s1c->right->m_word_vec_ind);
        }

      } else {
        phrase_emb_ind_ret.emplace_back(empty_phrase_emb_ind);
        child_phrase_emb_ind_ret.emplace_back(null_child_word_vec_ind);
        child_phrase_emb_ind_ret.emplace_back(null_child_word_vec_ind);
      }


      if (s2c) {
        assert(s2c->m_word_vec_ind != -1);
        phrase_emb_ind_ret.emplace_back(s2c->m_word_vec_ind);
        if ((!s2c->left && !s2c->right) || (s2c->left && !s2c->right)) {
          // lex or unary, no left/right phrase embedding
          child_phrase_emb_ind_ret.emplace_back(null_child_word_vec_ind);
          child_phrase_emb_ind_ret.emplace_back(null_child_word_vec_ind);
        } else {
          // binary
          assert(s2c->left && s2c->right);
          child_phrase_emb_ind_ret.emplace_back(s2c->left->m_word_vec_ind);
          child_phrase_emb_ind_ret.emplace_back(s2c->right->m_word_vec_ind);
        }

      } else {
        phrase_emb_ind_ret.emplace_back(empty_phrase_emb_ind);
        child_phrase_emb_ind_ret.emplace_back(null_child_word_vec_ind);
        child_phrase_emb_ind_ret.emplace_back(null_child_word_vec_ind);
      }


      if (s3c) {
        assert(s3c->m_word_vec_ind != -1);
        phrase_emb_ind_ret.emplace_back(s3c->m_word_vec_ind);
        if ((!s3c->left && !s3c->right) || (s3c->left && !s3c->right)) {
          // lex or unary, no left/right phrase embedding
          child_phrase_emb_ind_ret.emplace_back(null_child_word_vec_ind);
          child_phrase_emb_ind_ret.emplace_back(null_child_word_vec_ind);
        } else {
          // binary
          assert(s3c->left && s3c->right);
          child_phrase_emb_ind_ret.emplace_back(s3c->left->m_word_vec_ind);
          child_phrase_emb_ind_ret.emplace_back(s3c->right->m_word_vec_ind);
        }

      } else {
        phrase_emb_ind_ret.emplace_back(empty_phrase_emb_ind);
        child_phrase_emb_ind_ret.emplace_back(null_child_word_vec_ind);
        child_phrase_emb_ind_ret.emplace_back(null_child_word_vec_ind);
      }


//      if (s1c) {
//        phrase_emb_ind_ret.emplace_back(s1c->m_word_vec_ind);
//      } else {
//        phrase_emb_ind_ret.emplace_back(empty_phrase_emb_ind);
//      }
//
//      if (s2c) {
//        phrase_emb_ind_ret.emplace_back(s2c->m_word_vec_ind);
//      } else {
//        phrase_emb_ind_ret.emplace_back(empty_phrase_emb_ind);
//      }
//
//      if (s3c) {
//        phrase_emb_ind_ret.emplace_back(s3c->m_word_vec_ind);
//      } else {
//        phrase_emb_ind_ret.emplace_back(empty_phrase_emb_ind);
//      }
    }

    // dependency feats
    // currently, assuming looking at only one filled dep
//    if (dep) {
//      if (s0c) {
//        load_dep_context(s0c, word_inds, pos_inds,
//                         word_inds_ret, pos_inds_ret, supercat_inds_ret,
//                         dep_dist_ind_ret, cats_map, cats);
//      } else {
//        word_inds_ret.emplace_back(0); // head
//        word_inds_ret.emplace_back(0); // arg
//        pos_inds_ret.emplace_back(0);
//        pos_inds_ret.emplace_back(0);
//        supercat_inds_ret.emplace_back(empty_supercat_feat_ind);
//        // possibl dist vals are: 0, 1, 2 and 3 for padding
//        dep_dist_ind_ret.emplace_back(3);
//      }
//
//
//      if (s1c) {
//        load_dep_context(s1c, word_inds, pos_inds,
//                         word_inds_ret, pos_inds_ret, supercat_inds_ret,
//                         dep_dist_ind_ret, cats_map, cats);
//      } else {
//        word_inds_ret.emplace_back(0); // head
//        word_inds_ret.emplace_back(0); // arg
//        pos_inds_ret.emplace_back(0);
//        pos_inds_ret.emplace_back(0);
//        supercat_inds_ret.emplace_back(empty_supercat_feat_ind);
//        dep_dist_ind_ret.emplace_back(3);
//      }
//
//    }

}


  void fill_inds(const vector<size_t> &head_word_inds,
                 const vector<size_t> &head_word_pos_inds,
                 vector<size_t> &word_inds_ret,
                 vector<size_t> &pos_inds_ret) {

    if (head_word_inds.size() > 0) {

      assert(head_word_inds.size() == head_word_pos_inds.size());

      for (size_t i = 0; i < head_word_inds.size(); ++i) {
        word_inds_ret.emplace_back(head_word_inds[i]);
        pos_inds_ret.emplace_back(head_word_pos_inds[i]);
        if (i + 1 == m_head_limit)
          break;
      }

      if (head_word_inds.size() < m_head_limit) {
        size_t diff = m_head_limit - head_word_inds.size();
        for (size_t j = 0; j < diff; ++j) {
          word_inds_ret.emplace_back(m_emb_empty_col);
          pos_inds_ret.emplace_back(m_suf_empty_col);
        }
      }

    } else {
      for (size_t k = 0; k < m_head_limit; ++k) {
        word_inds_ret.emplace_back(m_emb_empty_col);
        pos_inds_ret.emplace_back(m_suf_empty_col);
      }
    }

  }

  void get_values(bool get_pos, const Variable *var,
                  std::vector<size_t> &heads,
                  std::vector<size_t> &pos,
                  const vector<size_t> &word_inds,
                  const vector<size_t> &pos_inds) const {
    //double total = 0.0;
    const Position *const end = var->fillers + Variable::NFILLERS;
    for(const Position *p = var->fillers; p != end && *p != Variable::SENTINEL; ++p) {
      if(!*p)
        continue;

      //++total;

      assert(*p - 1 >= 0);
      assert((size_t)(*p - 1) < word_inds.size());
      size_t value = word_inds[*p - 1];
      //if(!value)
      //continue;
      // the above is not needed here
      // since unknown words are dealt with
      // in word_inds etc.
      heads.emplace_back(value);

      if (get_pos) {
        //assert(tagValues[*p - 1]);
        assert((size_t)(*p - 1) < pos_inds.size());
        pos.emplace_back(pos_inds[*p - 1]);
      }
    }
    //cerr << "total: " << total << endl;
  }
};

} }






#endif /* SRC_INCLUDE_RNN_PARSER_CONTEXT_LOAD_POS_H_ */
