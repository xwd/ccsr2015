#ifndef SRC_LIB_RNN_TAGGER_LSTM_TAGGER_H_
#define SRC_LIB_RNN_TAGGER_LSTM_TAGGER_H_

// this should be commented out when debugging

#include <unordered_map>
#include "scored_ind.h"
#include "io/reader.h"
#include "io/reader_factory.h"
#include "tagger/super.h"

#include "cnn/nodes.h"
#include "cnn/cnn.h"
#include "cnn/training.h"
#include "cnn/timing.h"
#include "cnn/rnn.h"
#include "cnn/gru.h"
#include "cnn/lstm.h"
#include "cnn/dict.h"
#include "cnn/expr.h"
#include "cnn/model.h"

using namespace std;
using namespace NLP::Tagger;
using namespace cnn;



namespace NLP {
namespace Taggers {

#define IDX2C(i, j, ld) (((j) * (ld)) + (i)) // 0 based indexing

// this is an indicator for supercats not in the 425 classes in wsj02-21
// const static int UNKNWON_SUPERCAT_IND = -1;


template<class T>
inline void hash_combine(std::size_t& seed, const T& v) {
  std::hash<T> hasher;
  seed ^= hasher(v) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
}

struct KeyHash {
  std::size_t operator()(const std::pair<int, int>& v) const {
    size_t seed = 0;
    hash_combine(seed, v.first);
    hash_combine(seed, v.second);
    return seed;
  }
};

struct KeyEqual {
  bool operator()(const std::pair<int, int>& lhs, const std::pair<int, int>& rhs) const {
    return lhs.first == rhs.first && lhs.second == rhs.second;
  }
};

class LSTMTagger {
public:

  bool att;
  float pdrop;
  unsigned layers;
  unsigned pretrained_dim;
  unsigned suf_dim;
  unsigned hidden_dim;
  unsigned dwin;
  unsigned tag_size;
  unsigned vocab_size;
  int unk_tag;
  unsigned tag_hidden_dim;
  unsigned local_att_win;
  size_t att_type;
  bool att_cache;
  bool att_h;

  unordered_map<size_t, size_t> m_voc;
  unordered_map<size_t, size_t> m_classes;
  unordered_map<size_t, string> m_label2rawtag_map;

  unordered_map<string, size_t> m_full_wordstr_emb_map;
  unordered_map<size_t, vector<float>> m_ind_pretrained_emb_map;
  unordered_map<string, size_t> m_suf_str_ind_map;
  unordered_map<string, size_t> m_lex_cat_str_ind_map;

  vector<vector<vector<pair<string, double>>>> m_all_res_dev;
  vector<vector<vector<pair<string, double>>>> m_all_res_test;
  vector<vector<vector<ScoredInd>>> m_all_res_train;

  // for mtag eval
  vector<vector<vector<float>>> m_all_sent_dist;
  void eval_msuper(IO::ReaderFactory &reader, const float beta=0.0);

//  size_t m_de;
//  size_t m_ds;
//  size_t m_dc;
//  size_t m_cs;
//  size_t m_nh;
//  size_t m_bs;
//  string m_model_dir;
//  bool m_lowercase_words;
//  size_t m_nepoch;
//  bool m_dropout;
//  double m_dropout_success_prob;
//  bool m_verbose;
//  double m_lr;
//  size_t m_nclasses;
//  size_t m_vocsize;
//  size_t m_suffix_count;
//  string m_act;
//  double m_clip;
//  bool m_bi;
  //  size_t m_ntokens00;
  //  size_t m_ntokens23;

//  size_t m_UNK_SUFFIX_IND;
//  size_t m_UNKNWON_SUPERCAT_IND;
//  size_t m_WORD_PAD_IND;
//  size_t m_UNK_ALPHNUM_LOWER_IND;
//  size_t m_UNK_ALPHNUM_UPPER_IND;
//  size_t m_UNK_NON_ALPHNUM_IND;

  // for xf1 training
//  vector<mat> m_x_vals;
//  vector<mat> m_lh_vals;
//  vector<mat> m_lh_vals_back;
//  vector<mat> m_lh_deriv_vals;
//  vector<mat> m_lh_deriv_vals_back;
  vector<vector<vector<size_t>>> m_contextwins;

  LSTMTagger(const string& model_dir, const string& emb_file,
             const size_t& de, const size_t& dh, const size_t &dwin, const size_t& num_layer,
             const double& pdrop, bool att, const size_t& att_type, const bool att_cache, const bool att_h);
 ~LSTMTagger(void);

 void run(NLP::IO::ReaderFactory &reader,
          IO::ReaderFactory &reader_dev,
          const string& model_dir,
          const string &model_fname);

 void load_train_data(const string &words, const string &labels);
 void load_dev_test_data();
 vector<vector<vector<int> > > load_words(const string &filename);
 vector<vector<int> > load_labels(const string &filename, size_t &ntokens);
 void load_ind2rawtag_map(const string& model_dir);
 void load_suf_str_ind_map(const string &file);

 void sent2contextwin(const vector<vector<size_t>> &sent,
                      vector<vector<vector<size_t>>> &contextwins);
 bool is_alphnum(const string &word) const;
 bool is_lower(const string &word) const;
 string get_suf(const string &word) const;
 void clean_str(string &word, vector<string> &ret) const;
 void replace_all_substrs(string &str, const string &substr, const string &re) const;
 vector<size_t> word2vec(string &word) const;
 //void load_emb_mat(mat &emb, const string &filename);

  LookupParameters* p_w;
  LookupParameters* p_suf;
  Parameters* p_l2th;
  Parameters* p_r2th;
  Parameters* p_thbias;
  Parameters* p_att_w;
  Parameters* p_l2ah;
  Parameters* p_r2ah;
  Parameters* p_c2ah;
  Parameters* p_c2t; // att without additional hidden
  Parameters* p_th2t;
  Parameters* p_tbias;
  Trainer* sgd;

  size_t m_UNKNWON_SUPERCAT_IND;
  size_t m_UNK_ALPHNUM_LOWER_IND;
  size_t m_UNK_ALPHNUM_UPPER_IND;
  size_t m_UNK_NON_ALPHNUM_IND;
  size_t m_UNK_SUFFIX_IND;

  void calc_tagging_acc();
  Expression build_tagging_graph(LSTMBuilder &l2rbuilder,
                                 LSTMBuilder &r2lbuilder,
                                 vector<vector<vector<size_t>>> &sent_contextwins,
                                 vector<size_t>& tags,
                                 ComputationGraph& cg,
                                 cnn::Model &model,
                                 double &correct, double &ntagged, bool eval=false);

  Expression build_global_attention_tagging_graph(LSTMBuilder &l2rbuilder,
                                                  LSTMBuilder &r2lbuilder,
                                                  vector<vector<vector<size_t>>> &sent_contextwins,
                                                  vector<size_t>& tags,
                                                  ComputationGraph& cg,
                                                  cnn::Model &model,
                                                  double &correct, double &ntagged, bool eval=false);

  Expression build_local_attention_tagging_graph(LSTMBuilder &l2rbuilder,
                                                 LSTMBuilder &r2lbuilder,
                                                 vector<vector<vector<size_t>>> &sent_contextwins,
                                                 vector<size_t>& tags,
                                                 ComputationGraph& cg,
                                                 cnn::Model &model,
                                                 double &correct, double &ntagged, bool eval=false);
  void decode(IO::ReaderFactory &reader, const string &model_dir, const string &fname, const float beta=0.0);
  void load_pretrained_word_emb(const std::string& file);
  void read_data(IO::ReaderFactory &reader,
                 vector<pair<vector<vector<size_t>>, vector<size_t>>> &data,
                 size_t &total_sents,
                 size_t &ntokens,
                 double &in_pretrained);

  void display_value(const Expression &source, ComputationGraph &cg, string what_to_say) {
      const Tensor &a = cg.get_value(source.i);

      cnn::real I = a.d.cols();
      cnn::real J = a.d.rows();
      cerr << what_to_say << " rows: " << J << " cols: " << I << endl;
      if (what_to_say.size() > 0)
          cout << what_to_say << endl;
      for (unsigned j = 0; j < J; ++j) {
          for (unsigned i = 0; i < I; ++i) {
              cnn::real v = TensorTools::AccessElement(a, IDX2C(j,i, J));
              std::cout << v << ' ';
          }
          std::cout << endl;
      }
  }


};

} }

#endif /* SRC_LIB_RNN_TAGGER_LSTM_TAGGER_H_ */
