/* -*- Mode: C++; -*- */
// C&C NLP tools
// Copyright (c) Universities of Edinburgh, Oxford and Sydney
// Copyright (c) James R. Curran
//
// This software is covered by a non-commercial use licence.
// See LICENCE.txt for the full text of the licence.
//
// If LICENCE.txt is not included in this distribution
// please email candc@it.usyd.edu.au to obtain a copy.

#pragma once

#include "parser/fixed.h"
#include "parser/atom.h"
#include "parser/feature.h"
#include "parser/varid.h"
#include "parser/category.h"

#include "parser/markedup.h"
#include "parser/gr_constraints.h"
#include "parser/gr.h"
#include "parser/relation.h"
#include "parser/relations.h"
#include "parser/canonical.h"
#include "parser/categories.h"

#include "parser/decoder.h"

// added
#include "config/options.h"
#include "config/config.h"


namespace NLP {

using namespace Config;

namespace CCG {

class InsideOutside;
class ShiftReduceHypothesis;

class Parser {
public:
  class Config: public Directory {
  public:
    OpPath cats;
    OpPath markedup;
    OpPath weights;
    OpPath rules;

    std::string lexicon(void) const { return derived_path(path, "lexicon"); }
    std::string features(void) const { return derived_path(path, "features"); }

    Op<ulong> maxwords;
    Op<ulong> maxsupercats;

    Op<bool> alt_markedup;
    Op<bool> seen_rules;
    Op<bool> extra_rules;
    Op<bool> question_rules;
    Op<bool> eisner_nf;
    Op<bool> partial_gold;
    Op<double> beam;

    Op<bool> allowFrag;

    Config(const OpPath *base = 0, const std::string &name = "parser",
        const std::string &desc = "parser config");
  };
  public:

  static const ulong LOAD_WEIGHTS = 2;
  static const ulong LOAD_FEATURES = 1;
  static const ulong LOAD_NONE = 0;

  //static Pool *s_shiftReduceFeaturePool;

  Parser();
  Parser(const Config &cfg, Sentence &sent,
      Categories &cats, const std::vector<double> &perceptronId, const bool train, const size_t srbeam, ulong load = LOAD_WEIGHTS);
  ~Parser(void);

  bool parse(double BETA);
  void LoadSupercats(const std::string &filename);
  void LoadGoldTree(const std::string &filename);
  void LoadModel(const std::string &filename);
  void Clear();
  void clear_xf1();

  bool deps_count(std::ostream &out);
  bool count_rules(void);
  void print_rules(std::ostream &out) const;
  bool print_forest(InsideOutside &inside_outside, std::ostream &out, ulong id,
      const std::vector<ulong> &correct, const std::vector<long> &rules);
  ulong get_feature(const std::string &filename, const std::string &line,
      std::vector<long> &rules) const;

  bool is_partial_gold(void) const;

  bool calc_scores(void);
  const SuperCat *best(Decoder &decoder);
  double calc_stats(ulong &nequiv, ulong &total);

  const ShiftReduceHypothesis* ShiftReduceParse(const double BETA, bool qu_parsing);
  void SaveWeights(std::ofstream &out);

  //static Pool *GetFeaturePool() { return _impl->GetFeaturePool(); };

  private:
  class _Impl;
  _Impl *_impl;
};

}
}

