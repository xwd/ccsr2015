#ifndef SHIFTREDUCEFEATURE_H_
#define SHIFTREDUCEFEATURE_H_

#include "parser/ShiftReduceAttributes.h"
#include "parser/ShiftReduceHypothesis.h"
#include "parser/GraphAttributes.h"
#include "tree/_baseimpl.h"
//#include "parser/parser.h"

#include <string>

namespace NLP
{
namespace CCG
{

class ShiftReduceContext;

using namespace NLP::HashTable;
using namespace NLP::Tree;
using namespace std;

// 1
const static size_t s0wp = 1;
const static size_t s1wp = 2;

const static size_t s0c_ = 3;
const static size_t s1c_ = 4;

const static size_t s0pc = 5;
const static size_t s1pc = 6;
const static size_t s2pc = 7;
const static size_t s3pc = 8;

const static size_t s0wc = 9;
const static size_t s1wc = 10;
const static size_t s2wc = 11;
const static size_t s3wc = 12;

// 2
const static size_t q0wp = 13;
const static size_t q1wp = 14;
const static size_t q2wp = 15;
const static size_t q3wp = 16;

// 3
const static size_t s0lpc = 17;
const static size_t s0lwc = 18;
const static size_t s0rpc = 19;
const static size_t s0rwc = 20;
const static size_t s0upc = 21;
const static size_t s0uwc = 22;
const static size_t s1lpc = 23;
const static size_t s1lwc = 24;
const static size_t s1rpc = 25;
const static size_t s1rwc = 26;
const static size_t s1upc = 27;
const static size_t s1uwc = 28;

// 4
const static size_t s0wcs1wc = 29;

const static size_t s0cs1w = 30;

const static size_t s0ws1c = 31;

const static size_t s0cs1c = 32;

const static size_t s0wcq0wp = 33;
const static size_t s1wcq0wp = 37;

const static size_t s0cq0wp = 34;
const static size_t s1cq0wp = 38;

const static size_t s0wcq0p = 35;
const static size_t s1wcq0p = 39;

const static size_t s0cq0p = 36;
const static size_t s1cq0p = 40;

// 5
const static size_t s0ps1pq0p = 45;
const static size_t s0pq0pq1p = 46;
const static size_t s0ps1ps2p = 47;

const static size_t s0wcs1cq0p = 41;
const static size_t s0cs1wcq0p = 42;
const static size_t s0cs1cq0wp = 43;
const static size_t s0cs1cq0p = 44;
const static size_t s0wcq0pq1p = 48;
const static size_t s0cq0wpq1p = 49;
const static size_t s0cq0pq1wp = 50;
const static size_t s0cq0pq1p = 51;
const static size_t s0wcs1cs2c = 52;
const static size_t s0cs1wcs2c = 53;
const static size_t s0cs1cs2wc = 54;
const static size_t s0cs1cs2c = 55;

// 6
const static size_t s0cs0lcs0rc = 56;
const static size_t s1cs1lcs1rc = 57;

const static size_t s0cs0rcq0p = 58;

const static size_t s0cs0rcq0w = 59;
const static size_t s0cs0lcs1w = 60;

const static size_t s0cs0lcs1c = 61;
const static size_t s0cs1cs1rc = 62;

const static size_t s0ws1cs1rc = 63;


class ShiftReduceFeature
{


private:
  Categories &cats;
  const Lexicon &lexicon;

  // 1
  SwpAttributes swpAttrs;
  ScAttributes scAttrs;
  SpcAttributes spcAttrs;
  SwcAttributes swcAttrs;

  // 2
  QwpAttributes qwpAttrs;

  // 3
  SlurpcAttributes slurpcAttrs;
  SlurwcAttributes slurwcAttrs;

  // 4
  S0wcS1wcAttributes s0wcs1wcAttrs;
  S0cS1wAttributes s0cs1wAttrs;
  S0wS1cAttributes s0ws1cAttrs;
  S0cS1cAttributes s0cs1cAttrs;

  SwcQwpAttributes swcqwpAttrs;
  ScQwpAttributes scqwpAttrs;
  SwcQpAttributes swcqpAttrs;
  ScQpAttributes scqpAttrs;

  // 5
  S0wcS1cQ0pAttributes s0wcs1cq0pAttrs;
  S0cS1wcQ0pAttributes s0cs1wcq0pAttrs;
  S0cS1cQ0wpAttributes s0cs1cq0wpAttrs;
  S0cS1cQ0pAttributes s0cs1cq0pAttrs;

  S0pSQpQSpAttributes s0psqpqspAttrs;

  S0wcQ0pQ1pAttributes s0wcq0pq1pAttrs;
  S0cQ0wpQ1pAttributes s0cq0wpq1pAttrs;
  S0cQ0pQ1wpAttributes s0cq0pq1wpAttrs;
  S0cQ0pQ1pAttributes s0cq0pq1pAttrs;
  S0wcS1cS2cAttributes s0wcs1cs2cAttrs;
  S0cS1wcS2cAttributes s0cs1wcs2cAttrs;
  S0cS1cS2wcAttributes s0cs1cs2wcAttrs;
  S0cS1cS2cAttributes s0cs1cs2cAttrs;


  // 6
  S01cS01LcS01rcAttributes s01cs01lcs01rcAttrs;
  S0cS0RcQ0pAttributes s0cs0rcq0pAttrs;
  S0cS0LRcQ0S1wAttributes s0cS0lrcq0s1wAttrs;
  S0cS0LS1cS1S1RcAttributes s0cs0ls1cs1s1rcAttrs;
  S0wS1cS1RcAttributes s0ws1cs1rcAttrs;


public:
  ShiftReduceFeature(Categories &cats, const Lexicon &lexicon):
    cats(cats), lexicon(lexicon), m_featureTypeCount() {}
  virtual ~ShiftReduceFeature(void)
  {
    // 1
    swpAttrs.~SwpAttributes();
    scAttrs.~ScAttributes();
    spcAttrs.~SpcAttributes();
    swcAttrs.~SwcAttributes();

    // 2
    qwpAttrs.~QwpAttributes();

    // 3
    slurpcAttrs.~SlurpcAttributes();
    slurwcAttrs.~SlurwcAttributes();

    // 4
    s0wcs1wcAttrs.~S0wcS1wcAttributes();
    s0cs1wAttrs.~S0cS1wAttributes();
    s0ws1cAttrs.~S0wS1cAttributes();
    s0cs1cAttrs.~S0cS1cAttributes();

    swcqwpAttrs.~SwcQwpAttributes();
    scqwpAttrs.~ScQwpAttributes();
    swcqpAttrs.~SwcQpAttributes();
    scqpAttrs.~ScQpAttributes();

    // 5
    s0wcs1cq0pAttrs.~S0wcS1cQ0pAttributes();
    s0cs1wcq0pAttrs.~S0cS1wcQ0pAttributes();
    s0cs1cq0wpAttrs.~S0cS1cQ0wpAttributes();
    s0cs1cq0pAttrs.~S0cS1cQ0pAttributes();

    s0psqpqspAttrs.~S0pSQpQSpAttributes();

    s0wcq0pq1pAttrs.~S0wcQ0pQ1pAttributes();
    s0cq0wpq1pAttrs.~S0cQ0wpQ1pAttributes();
    s0cq0pq1wpAttrs.~S0cQ0pQ1wpAttributes();
    s0cq0pq1pAttrs.~S0cQ0pQ1pAttributes();
    s0wcs1cs2cAttrs.~S0wcS1cS2cAttributes();
    s0cs1wcs2cAttrs.~S0cS1wcS2cAttributes();
    s0cs1cs2wcAttrs.~S0cS1cS2wcAttributes();
    s0cs1cs2cAttrs.~S0cS1cS2cAttributes();

    // 6
    s01cs01lcs01rcAttrs.~S01cS01LcS01rcAttributes();
    s0cs0rcq0pAttrs.~S0cS0RcQ0pAttributes();
    s0cS0lrcq0s1wAttrs.~S0cS0LRcQ0S1wAttributes();
    s0cs0ls1cs1s1rcAttrs.~S0cS0LS1cS1S1RcAttributes();
    s0ws1cs1rcAttrs.~S0wS1cS1RcAttributes();
  }

  double GetOrUpdateWeight(bool update, bool correct, const ShiftReduceContext &context, const ShiftReduceAction &action,
      const Words &words, const Words &tags);

  void SetWeights(std::istream &in, size_t type);
  bool SaveWeights(std::ofstream &out);
  bool CheckWeights(size_t total);

  const Cat *GetCat(std::string tmp);

  size_t m_featureTypeCount [33];



};


struct SwpValue
{
  friend std::ostream &operator<<(std::ostream&, const SwpValue&);

  const size_t type;
  const Word wordValue;
  const Word tagValue;
  const ShiftReduceAction action;

  SwpValue(const size_t type, const Word wordValue, const Word tagValue, const ShiftReduceAction &action)
  : type(type), wordValue(wordValue), tagValue(tagValue), action(action) {}


  Hash hash(void) const
  {
    Hash h(static_cast<ulong>(type));
    h += static_cast<ulong>(wordValue);
    h += static_cast<ulong>(tagValue);
    h += action.GetCat()->rhash;
    h += static_cast<ulong>(action.GetAction());
    return h;
  }

  bool equal(const SwpValue &other) const
  {
    if(other.type == type && other.wordValue == wordValue && other.tagValue == tagValue && other.action == action)
      return true;
    else
      return false;
  }
};

inline std::ostream &operator<<(std::ostream& out, const SwpValue& value)
{
  out << value.type << " " << value.wordValue.str() << " " << value.tagValue.str() << " " << value.action;
  return out;
}


struct ScValue
{
  friend std::ostream &operator<<(std::ostream&, const ScValue&);

  const size_t type;
  const Cat *cat;
  const ShiftReduceAction action;

  ScValue(const size_t type, const Cat *cat, const ShiftReduceAction &action)
  : type(type), cat(cat), action(action) {}


  Hash hash(void) const
  {
    Hash h(static_cast<ulong>(type));
    h += cat->rhash;
    h += action.GetCat()->rhash;
    h += static_cast<ulong>(action.GetAction());
    return h;
  }

  bool equal(const ScValue &other) const
  {
    if(other.type == type && other.action == action)
    {
      if (eqSR(other.cat, cat))
        return true;
      else return false;
    }
    else
    {
      return false;
    }
  }

};

inline std::ostream &operator<<(std::ostream& out, const ScValue& value)
{
  out << value.type << " " << value.cat << " " << value.action;
  return out;
}

struct SpcValue
{
  friend std::ostream &operator<<(std::ostream&, const SpcValue&);

  const size_t type;
  const Word tagValue;
  const Cat *cat;
  const ShiftReduceAction action;

  SpcValue(const size_t type, const Word tagValue, const Cat *cat, const ShiftReduceAction &action)
  : type(type), tagValue(tagValue), cat( cat), action(action) {}


  Hash hash(void) const
  {
    Hash h(static_cast<ulong>(type));
    h += static_cast<ulong>(tagValue);
    h += cat->rhash;
    h += action.GetCat()->rhash;
    h += static_cast<ulong>(action.GetAction());
    return h;
  }

  bool equal(const SpcValue &other) const
  {
    if(other.type == type && other.tagValue == tagValue && other.action == action)
    {
      if (eqSR(other.cat, cat))
        return true;
      else return false;
    }
    else
    {
      return false;
    }
  }

};

inline std::ostream &operator<<(std::ostream& out, const SpcValue& value) {
  out << value.type << " " << value.tagValue.str() << " " << value.cat << " " << value.action;
  return out;
}


struct SwcValue
{
  friend std::ostream &operator<<(std::ostream&, const SwcValue&);

  const size_t type;
  const Word wordValue;
  const Cat *cat;
  const ShiftReduceAction action;

  SwcValue(const size_t type, const Word wordValue, const Cat *cat, const ShiftReduceAction &action)
  : type(type), wordValue(wordValue), cat(cat), action(action) {}


  Hash hash(void) const
  {
    Hash h(static_cast<ulong>(type));
    h += static_cast<ulong>(wordValue);
    h += cat->rhash;
    h += action.GetCat()->rhash;
    h += static_cast<ulong>(action.GetAction());
    return h;
  }

  bool equal(const SwcValue &other) const
  {
    if(other.type == type && other.wordValue == wordValue && other.action == action)
    {
      if (eqSR(other.cat, cat))
        return true;
      else return false;
    }
    else
    {
      return false;
    }
  }

};

inline std::ostream &operator<<(std::ostream& out, const SwcValue& value) {
  out << value.type << " " << value.wordValue.str() << " " << value.cat << " " << value.action;
  return out;
}

// 2

struct QwpValue
{
  friend std::ostream &operator<<(std::ostream&, const QwpValue&);

  const size_t type;
  const Word wordValue;
  const Word tagValue;
  const ShiftReduceAction action;

  QwpValue(const size_t type, const Word wordValue, const Word tagValue, const ShiftReduceAction &action)
  : type(type), wordValue(wordValue), tagValue(tagValue), action(action) {}


  Hash hash(void) const
  {
    Hash h(static_cast<ulong>(wordValue));
    h += static_cast<ulong>(type);
    h += static_cast<ulong>(tagValue);
    h += action.GetCat()->rhash;
    h += static_cast<ulong>(action.GetAction());
    return h;
  }

  bool equal(const QwpValue &other) const
  {
    if(other.type == type && other.wordValue == wordValue &&
        other.tagValue == tagValue && other.action == action)
      return true;
    else
      return false;
  }
};

inline std::ostream &operator<<(std::ostream& out, const QwpValue& value) {
  out << value.type << " " << value.wordValue.str() << " " << value.tagValue.str() << " " << value.action;
  return out;
}


// 3

struct SlurpcValue
{
  friend std::ostream &operator<<(std::ostream&, const SlurpcValue&);

  const size_t type;
  const Word tagValue;
  const Cat *cat;
  const ShiftReduceAction action;

  SlurpcValue(const size_t type, const Word tagValue, const Cat *cat, const ShiftReduceAction &action)
  : type(type), tagValue(tagValue), cat(cat), action(action) {}

  Hash hash(void) const
  {
    Hash h(static_cast<ulong>(type));
    h += static_cast<ulong>(tagValue);
    h += cat->rhash;
    h += action.GetCat()->rhash;
    h += static_cast<ulong>(action.GetAction());
    return h;
  }

  bool equal(const SlurpcValue &other) const
  {
    if(other.type == type && other.tagValue == tagValue && other.action == action)
    {
      if (eqSR(other.cat, cat))
        return true;
      else
        return false;
    }
    else
    {
      return false;
    }
  }
};

inline std::ostream &operator<<(std::ostream& out, const SlurpcValue& value) {
  out << value.type << " " << value.tagValue.str() << " " << value.cat << " " << value.action;
  return out;
}


struct SlurwcValue
{
  friend std::ostream &operator<<(std::ostream&, const SlurwcValue&);

  const size_t type;
  const Word wordValue;
  const Cat *cat;
  const ShiftReduceAction action;

  SlurwcValue(const size_t type, const Word wordValue, const Cat *cat, const ShiftReduceAction &action)
  : type(type), wordValue(wordValue), cat(cat), action(action) {}

  Hash hash(void) const
  {
    Hash h(static_cast<ulong>(type));
    h += static_cast<ulong>(wordValue);
    h += cat->rhash;
    h += action.GetCat()->rhash;
    h += static_cast<ulong>(action.GetAction());
    return h;
  }

  bool equal(const SlurwcValue &other) const
  {
    if(other.type == type && other.wordValue == wordValue && other.action == action)
    {
      if (eqSR(other.cat, cat))
        return true;
      else
        return false;
    }
    else
    {
      return false;
    }
  }
};

inline std::ostream &operator<<(std::ostream& out, const SlurwcValue& value) {
  out << value.type << " " << value.wordValue.str() << " " << value.cat << " " << value.action;
  return out;
}


// 4

struct S0wcS1wcValue
{
  friend std::ostream &operator<<(std::ostream&, const S0wcS1wcValue&);

  const size_t type;
  const Word wordValueS0;
  const Cat *catS0;
  const Word wordValueS1;
  const Cat *catS1;
  const ShiftReduceAction action;

  S0wcS1wcValue(const size_t type, const Word wordValueS0, const Cat *catS0, const Word wordValueS1, const Cat *catS1, const ShiftReduceAction &action)
  : type(type), wordValueS0(wordValueS0), catS0( catS0), wordValueS1(wordValueS1), catS1( catS1), action(action) {}

  Hash hash(void) const
  {
    Hash h(static_cast<ulong>(type));
    h += static_cast<ulong>(wordValueS0);
    h += catS0->rhash;
    h += static_cast<ulong>(wordValueS1);
    h += catS1->rhash;
    h += action.GetCat()->rhash;
    h += static_cast<ulong>(action.GetAction());
    return h;
  }

  bool equal(const S0wcS1wcValue &other) const
  {
    if (other.type == type && other.wordValueS0 == wordValueS0 && other.wordValueS1 == wordValueS1 &&
        other.action == action)
    {
      if (eqSR(other.catS0, catS0) && eqSR(other.catS1, catS1))
        return true;
      else
        return false;
    }
    else
    {
      return false;
    }
  }
};

inline std::ostream &operator<<(std::ostream& out, const S0wcS1wcValue& value) {
  out << value.type << " " << value.wordValueS0.str() << " " << value.catS0 << " " << value.wordValueS1.str() << " " << value.catS1
      << " " << value.action;
  return out;
}



struct S0cS1wValue
{
  friend std::ostream &operator<<(std::ostream&, const S0cS1wValue&);

  const size_t type;
  const Cat *catS0;
  const Word wordValueS1;
  const ShiftReduceAction action;

  S0cS1wValue(const size_t type, const Cat *catS0, const Word wordValueS1, const ShiftReduceAction &action)
  : type(type), catS0(catS0), wordValueS1(wordValueS1), action(action) {}

  Hash hash(void) const
  {
    Hash h(static_cast<ulong>(type));
    h += catS0->rhash;
    h += static_cast<ulong>(wordValueS1);
    h += action.GetCat()->rhash;
    h += static_cast<ulong>(action.GetAction());
    return h;
  }

  bool equal(const S0cS1wValue &other) const
  {
    if(other.type == type && other.wordValueS1 == wordValueS1 &&
        other.action == action)
    {
      if (eqSR(other.catS0, catS0))
        return true;
      else
        return false;
    }
    else
    {
      return false;
    }
  }
};

inline std::ostream &operator<<(std::ostream& out, const S0cS1wValue& value) {
  out << value.type << " " << value.catS0 << " " << value.wordValueS1.str() << " " << value.action;
  return out;
}


struct S0wS1cValue
{
  friend std::ostream &operator<<(std::ostream&, const S0wS1cValue&);

  const size_t type;
  const Word wordValueS0;
  const Cat *catS1;
  const ShiftReduceAction action;

  S0wS1cValue(const size_t type, const Word wordValueS0, const Cat *catS1, const ShiftReduceAction &action)
  : type(type), wordValueS0(wordValueS0), catS1(catS1),  action(action) {}

  Hash hash(void) const
  {
    Hash h(static_cast<ulong>(type));
    h += static_cast<ulong>(wordValueS0);
    h += catS1->rhash;
    h += action.GetCat()->rhash;
    h += static_cast<ulong>(action.GetAction());
    return h;
  }

  bool equal(const S0wS1cValue &other) const
  {
    if(other.type == type && other.wordValueS0 == wordValueS0 && other.action == action)
    {
      if (eqSR(other.catS1, catS1))
        return true;
      else
        return false;
    }
    else
    {
      return false;
    }
  }
};

inline std::ostream &operator<<(std::ostream& out, const S0wS1cValue& value) {
  out << value.type << " " << value.wordValueS0.str() << " " << value.catS1 << " " << value.action;
  return out;
}


struct S0cS1cValue
{
  friend std::ostream &operator<<(std::ostream&, const S0cS1cValue&);

  const size_t type;
  const Cat *catS0;
  const Cat *catS1;
  const ShiftReduceAction action;

  S0cS1cValue(const size_t type, const Cat *catS0, const Cat *catS1, const ShiftReduceAction &action)
  : type(type), catS0(catS0), catS1(catS1),  action(action) {}

  Hash hash(void) const
  {
    Hash h(static_cast<ulong>(type));
    h += catS0->rhash;
    h += catS1->rhash;
    h += action.GetCat()->rhash;
    h += static_cast<ulong>(action.GetAction());
    return h;
  }

  bool equal(const S0cS1cValue &other) const
  {
    if(other.type == type && other.action == action)
    {
      if (eqSR(other.catS0, catS0) && eqSR(other.catS1, catS1))
        return true;
      else
        return false;
    }
    else
    {
      return false;
    }
  }
};

inline std::ostream &operator<<(std::ostream& out, const S0cS1cValue& value) {
  out << value.type << " " << value.catS0 << " " << value.catS1 << " " << value.action;
  return out;
}


struct SwcQwpValue
{
  friend std::ostream &operator<<(std::ostream&, const SwcQwpValue&);

  const size_t type;
  const Word wordValueS;
  const Cat *catS;
  const Word wordValueQ;
  const Word tagValueQ;
  const ShiftReduceAction action;

  SwcQwpValue(const size_t type, const Word wordValueS, const Cat *catS, const Word wordValueQ, const Word tagValueQ, const ShiftReduceAction &action)
  : type(type), wordValueS(wordValueS), catS(catS), wordValueQ(wordValueQ), tagValueQ(tagValueQ), action(action) {}

  Hash hash(void) const
  {
    Hash h(static_cast<ulong>(type));
    h += static_cast<ulong>(wordValueS);
    h += catS->rhash;
    h += static_cast<ulong>(wordValueQ);
    h += static_cast<ulong>(tagValueQ);
    h += action.GetCat()->rhash;
    h += static_cast<ulong>(action.GetAction());
    return h;
  }

  bool equal(const SwcQwpValue &other) const
  {
    if(other.type == type && other.wordValueS == wordValueS && other.wordValueQ == wordValueQ &&
        other.tagValueQ == tagValueQ && other.action == action)
    {
      if (eqSR(other.catS, catS))
        return true;
      else
        return false;
    }
    else
    {
      return false;
    }
  }
};

inline std::ostream &operator<<(std::ostream& out, const SwcQwpValue& value) {
  out << value.type << " " << value.wordValueS.str() << " " << value.catS << " " << value.wordValueQ.str() << " " << value.tagValueQ.str() << " " << value.action;
  return out;
}


struct ScQwpValue
{
  friend std::ostream &operator<<(std::ostream&, const ScQwpValue&);

  const size_t type;
  const Cat *catS;
  const Word wordValueQ;
  const Word tagValueQ;
  const ShiftReduceAction action;

  ScQwpValue(const size_t type, const Cat *catS, const Word wordValueQ, const Word tagValueQ, const ShiftReduceAction &action)
  : type(type), catS(catS), wordValueQ(wordValueQ), tagValueQ(tagValueQ), action(action) {}

  Hash hash(void) const
  {
    Hash h(static_cast<ulong>(type));
    h += catS->rhash;
    h += static_cast<ulong>(wordValueQ);
    h += static_cast<ulong>(tagValueQ);
    h += action.GetCat()->rhash;
    h += static_cast<ulong>(action.GetAction());
    return h;
  }

  bool equal(const ScQwpValue &other) const
  {
    if (other.type == type && other.wordValueQ == wordValueQ &&
        other.tagValueQ == tagValueQ && other.action == action)
    {
      if (eqSR(other.catS, catS))
        return true;
      else
        return false;
    }
    else
    {
      return false;
    }
  }
};

inline std::ostream &operator<<(std::ostream& out, const ScQwpValue& value) {
  out << value.type << " " << value.catS << " " << value.wordValueQ.str() << " " << value.tagValueQ.str() << " " << value.action;
  return out;
}


struct SwcQpValue
{
  friend std::ostream &operator<<(std::ostream&, const SwcQpValue&);

  const size_t type;
  const Word wordValueS;
  const Cat *catS;
  const Word tagValueQ;
  const ShiftReduceAction action;

  SwcQpValue(const size_t type, const Word wordValueS, const Cat *catS, const Word tagValueQ, const ShiftReduceAction &action)
  : type(type), wordValueS(wordValueS), catS(catS), tagValueQ(tagValueQ), action(action) {}

  Hash hash(void) const
  {
    Hash h(static_cast<ulong>(type));
    h += static_cast<ulong>(wordValueS);
    h += catS->rhash;
    h += static_cast<ulong>(tagValueQ);
    h += action.GetCat()->rhash;
    h += static_cast<ulong>(action.GetAction());
    return h;
  }

  bool equal(const SwcQpValue &other) const
  {
    if(other.type == type && other.wordValueS == wordValueS && other.tagValueQ == tagValueQ &&
        other.action == action)
    {
      if (eqSR(other.catS, catS))
        return true;
      else
        return false;
    }
    else
    {
      return false;
    }
  }
};

inline std::ostream &operator<<(std::ostream& out, const SwcQpValue& value) {
  out << value.type << " " << value.wordValueS.str() << " " << value.catS << " " << value.tagValueQ.str() << " " << value.action;
  return out;
}


struct ScQpValue
{
  friend std::ostream &operator<<(std::ostream&, const ScQpValue&);

  const size_t type;
  const Cat *catS;
  const Word tagValueQ;
  const ShiftReduceAction action;

  ScQpValue(const size_t type, const Cat *catS, const Word tagValueQ, const ShiftReduceAction &action)
  : type(type), catS(catS), tagValueQ(tagValueQ), action(action) {}

  Hash hash(void) const
  {
    Hash h(static_cast<ulong>(type));
    h += catS->rhash;
    h += static_cast<ulong>(tagValueQ);
    h += action.GetCat()->rhash;
    h += static_cast<ulong>(action.GetAction());
    return h;
  }

  bool equal(const ScQpValue &other) const
  {
    if(other.type == type && other.tagValueQ == tagValueQ && other.action == action)
    {
      if (eqSR(other.catS, catS))
        return true;
      else
        return false;
    }
    else
    {
      return false;
    }

  }
};

inline std::ostream &operator<<(std::ostream& out, const ScQpValue& value) {
  out << value.type << " " << value.catS << " " << value.tagValueQ.str() << " " << value.action;
  return out;
}



// 5

struct S0wcS1cQ0pValue
{
  friend std::ostream &operator<<(std::ostream&, const S0wcS1cQ0pValue&);

  const size_t type;
  const Word wordValueS0;
  const Cat *catS0;
  const Cat *catS1;
  const Word tagValueQ0;
  const ShiftReduceAction action;

  S0wcS1cQ0pValue(const size_t type, const Word wordValueS0, const Cat *catS0, const Cat *catS1, const Word tagValueQ0, const ShiftReduceAction &action)
  : type(type), wordValueS0(wordValueS0), catS0(catS0), catS1(catS1), tagValueQ0(tagValueQ0), action(action) {}

  Hash hash(void) const
  {
    Hash h(static_cast<ulong>(type));
    h += static_cast<ulong>(wordValueS0);
    h += catS0->rhash;
    h += catS1->rhash;
    h += static_cast<ulong>(tagValueQ0);
    h += action.GetCat()->rhash;
    h += static_cast<ulong>(action.GetAction());
    return h;
  }

  bool equal(const S0wcS1cQ0pValue &other) const
  {
    if(other.type == type && other.wordValueS0 == wordValueS0 && other.tagValueQ0 == tagValueQ0 &&
        other.action == action)
    {
      if (eqSR(other.catS0, catS0) && eqSR(other.catS1, catS1))
        return true;
      else
        return false;
    }
    else
    {
      return false;
    }
  }
};

inline std::ostream &operator<<(std::ostream& out, const S0wcS1cQ0pValue& value) {
  out << value.type << " " << value.wordValueS0.str() << " " << value.catS0 << " " << value.catS1
      << " " << value.tagValueQ0.str() << " " << value.action;
  return out;
}


struct S0cS1wcQ0pValue
{
  friend std::ostream &operator<<(std::ostream&, const S0cS1wcQ0pValue&);

  const size_t type;
  const Cat *catS0;
  const Word wordValueS1;
  const Cat *catS1;
  const Word tagValueQ0;
  const ShiftReduceAction action;

  S0cS1wcQ0pValue(const size_t type, const Cat *catS0, const Word wordValueS1, const Cat *catS1, const Word tagValueQ0, const ShiftReduceAction &action)
  : type(type), catS0(catS0), wordValueS1(wordValueS1),
    catS1(catS1), tagValueQ0(tagValueQ0), action(action) {}

  Hash hash(void) const
  {
    Hash h(static_cast<ulong>(type));
    h += catS0->rhash;
    h += static_cast<ulong>(wordValueS1);
    h += catS1->rhash;
    h += static_cast<ulong>(tagValueQ0);
    h += action.GetCat()->rhash;
    h += static_cast<ulong>(action.GetAction());
    return h;
  }

  bool equal(const S0cS1wcQ0pValue &other) const
  {
    if(other.type == type && other.wordValueS1 == wordValueS1 && other.tagValueQ0 == tagValueQ0 &&
        other.action == action)
    {
      if (eqSR(other.catS0, catS0) && eqSR(other.catS1, catS1))
        return true;
      else
        return false;
    }
    else
    {
      return false;
    }
  }
};

inline std::ostream &operator<<(std::ostream& out, const S0cS1wcQ0pValue& value) {
  out << value.type << " " << value.catS0 << " " << value.wordValueS1.str() << " " << value.catS1
      << " " << value.tagValueQ0.str() << " " << value.action;
  return out;
}


struct S0cS1cQ0wpValue
{
  friend std::ostream &operator<<(std::ostream&, const S0cS1cQ0wpValue&);

  const size_t type;
  const Cat *catS0;
  const Cat *catS1;
  const Word wordValueQ0;
  const Word tagValueQ0;
  const ShiftReduceAction action;

  S0cS1cQ0wpValue(const size_t type, const Cat *catS0, const Cat *catS1, const Word wordValueQ0, const Word tagValueQ0, const ShiftReduceAction &action)
  : type(type), catS0(catS0), catS1(catS1), wordValueQ0(wordValueQ0), tagValueQ0(tagValueQ0), action(action) {}

  Hash hash(void) const
  {
    Hash h(static_cast<ulong>(type));
    h += catS0->rhash;
    h += catS1->rhash;
    h += static_cast<ulong>(wordValueQ0);
    h += static_cast<ulong>(tagValueQ0);
    h += action.GetCat()->rhash;
    h += static_cast<ulong>(action.GetAction());
    return h;
  }

  bool equal(const S0cS1cQ0wpValue &other) const
  {
    if(other.type == type && other.wordValueQ0 == wordValueQ0 && other.tagValueQ0 == tagValueQ0 &&
        other.action == action)
    {
      if (eqSR(other.catS0, catS0) && eqSR(other.catS1, catS1))
        return true;
      else
        return false;
    }
    else
    {
      return false;
    }
  }
};

inline std::ostream &operator<<(std::ostream& out, const S0cS1cQ0wpValue& value) {
  out << value.type << " " << value.catS0 << " " << value.catS1
      << " " << value.wordValueQ0.str() << " " << value.tagValueQ0.str() << " " << value.action;
  return out;
}


struct S0cS1cQ0pValue
{
  friend std::ostream &operator<<(std::ostream&, const S0cS1cQ0pValue&);

  const size_t type;
  const Cat *catS0;
  const Cat *catS1;
  const Word tagValueQ0;
  const ShiftReduceAction action;

  S0cS1cQ0pValue(const size_t type, const Cat *catS0, const Cat *catS1, const Word tagValueQ0, const ShiftReduceAction &action)
  : type(type), catS0(catS0), catS1(catS1), tagValueQ0(tagValueQ0), action(action) {}

  Hash hash(void) const
  {
    Hash h(static_cast<ulong>(type));
    h += catS0->rhash;
    h += catS1->rhash;
    h += static_cast<ulong>(tagValueQ0);
    h += action.GetCat()->rhash;
    h += static_cast<ulong>(action.GetAction());
    return h;
  }

  bool equal(const S0cS1cQ0pValue &other) const
  {
    if(other.type == type && other.tagValueQ0 == tagValueQ0 &&
        other.action == action)
    {
      if (eqSR(other.catS0, catS0) && eqSR(other.catS1, catS1))
        return true;
      else
        return false;
    }
    else
    {
      return false;
    }
  }
};

inline std::ostream &operator<<(std::ostream& out, const S0cS1cQ0pValue& value) {
  out << value.type << " " << value.catS0 << " " << value.catS1
      << " " << value.tagValueQ0.str() << " " << value.action;
  return out;
}


struct S0pSQpQSpValue
{
  friend std::ostream &operator<<(std::ostream&, const S0pSQpQSpValue&);

  const size_t type;
  const Word tagValueS0;
  const Word tagValueSQ;
  const Word tagValueQS;
  const ShiftReduceAction action;

  S0pSQpQSpValue(const size_t type, const Word tagValueS0, const Word tagValueSQ, const Word tagValueQS, const ShiftReduceAction &action)
  : type(type), tagValueS0(tagValueS0), tagValueSQ(tagValueSQ), tagValueQS(tagValueQS), action(action) {}

  Hash hash(void) const
  {
    Hash h(static_cast<ulong>(type));
    h += static_cast<ulong>(tagValueS0);
    h += static_cast<ulong>(tagValueSQ);
    h += static_cast<ulong>(tagValueQS);
    h += action.GetCat()->rhash;
    h += static_cast<ulong>(action.GetAction());
    return h;
  }

  bool equal(const S0pSQpQSpValue &other) const
  {
    if(other.type == type && other.tagValueS0 == tagValueS0 &&
        other.tagValueSQ == tagValueSQ && other.tagValueQS == tagValueQS &&
        other.action == action)
      return true;
    else
      return false;

  }
};

inline std::ostream &operator<<(std::ostream& out, const S0pSQpQSpValue& value) {
  out << value.type << " " << value.tagValueS0.str() << " " << value.tagValueSQ.str() << " " << value.tagValueQS.str() << " " << value.action;
  return out;
}


struct S0wcQ0pQ1pValue
{
  friend std::ostream &operator<<(std::ostream&, const S0wcQ0pQ1pValue&);

  const size_t type;
  const Word wordValueS0;
  const Cat *catS0;
  const Word tagValueQ0;
  const Word tagValueQ1;
  const ShiftReduceAction action;

  S0wcQ0pQ1pValue(const size_t type, const Word wordValueS0, const Cat *catS0, const Word tagValueQ0, const Word tagValueQ1, const ShiftReduceAction &action)
  : type(type), wordValueS0(wordValueS0), catS0(catS0),
    tagValueQ0(tagValueQ0), tagValueQ1(tagValueQ1), action(action) {}

  Hash hash(void) const
  {
    Hash h(static_cast<ulong>(type));
    h += static_cast<ulong>(wordValueS0);
    h += catS0->rhash;
    h += static_cast<ulong>(tagValueQ0);
    h += static_cast<ulong>(tagValueQ1);
    h += action.GetCat()->rhash;
    h += static_cast<ulong>(action.GetAction());
    return h;
  }

  bool equal(const S0wcQ0pQ1pValue &other) const
  {
    if(other.type == type && other.wordValueS0 == wordValueS0 && other.tagValueQ0 == tagValueQ0 && other.tagValueQ1 == tagValueQ1 &&
        other.action == action)
    {
      if (eqSR(other.catS0, catS0))
        return true;
      else
        return false;
    }
    else
    {
      return false;
    }
  }
};

inline std::ostream &operator<<(std::ostream& out, const S0wcQ0pQ1pValue& value) {
  out << value.type << " " << value.wordValueS0.str() << " " << value.catS0
      << " " << value.tagValueQ0.str() << " " << value.tagValueQ1.str() << " " << value.action;
  return out;
}


struct S0cQ0wpQ1pValue
{
  friend std::ostream &operator<<(std::ostream&, const S0cQ0wpQ1pValue&);

  const size_t type;
  const Cat *catS0;
  const Word wordValueQ0;
  const Word tagValueQ0;
  const Word tagValueQ1;
  const ShiftReduceAction action;

  S0cQ0wpQ1pValue(const size_t type, const Cat *catS0, const Word wordValueQ0, const Word tagValueQ0, const Word tagValueQ1, const ShiftReduceAction &action)
  : type(type), catS0(catS0),
    wordValueQ0(wordValueQ0), tagValueQ0(tagValueQ0), tagValueQ1(tagValueQ1), action(action) {}

  Hash hash(void) const
  {
    Hash h(static_cast<ulong>(type));
    h += catS0->rhash;
    h += static_cast<ulong>(wordValueQ0);
    h += static_cast<ulong>(tagValueQ0);
    h += static_cast<ulong>(tagValueQ1);
    h += action.GetCat()->rhash;
    h += static_cast<ulong>(action.GetAction());
    return h;
  }

  bool equal(const S0cQ0wpQ1pValue &other) const
  {
    if(other.type == type && other.wordValueQ0 == wordValueQ0 && other.tagValueQ0 == tagValueQ0 && other.tagValueQ1 == tagValueQ1 &&
        other.action == action)
    {
      if (eqSR(other.catS0, catS0))
        return true;
      else
        return false;
    }
    else
    {
      return false;
    }
  }
};

inline std::ostream &operator<<(std::ostream& out, const S0cQ0wpQ1pValue& value) {
  out << value.type << " " << value.catS0 << " " << value.wordValueQ0.str()
		        << " " << value.tagValueQ0.str() << " " << value.tagValueQ1.str() << " " << value.action;
  return out;
}


struct S0cQ0pQ1wpValue
{
  friend std::ostream &operator<<(std::ostream&, const S0cQ0pQ1wpValue&);

  const size_t type;
  const Cat *catS0;
  const Word tagValueQ0;
  const Word wordValueQ1;
  const Word tagValueQ1;
  const ShiftReduceAction action;

  S0cQ0pQ1wpValue(const size_t type, const Cat *catS0, const Word tagValueQ0, const Word wordValueQ1, const Word tagValueQ1, const ShiftReduceAction &action)
  : type(type), catS0(catS0), tagValueQ0(tagValueQ0), wordValueQ1(wordValueQ1), tagValueQ1(tagValueQ1), action(action) {}

  Hash hash(void) const
  {
    Hash h(static_cast<ulong>(type));
    h += catS0->rhash;
    h += static_cast<ulong>(tagValueQ0);
    h += static_cast<ulong>(wordValueQ1);
    h += static_cast<ulong>(tagValueQ1);
    h += action.GetCat()->rhash;
    h += static_cast<ulong>(action.GetAction());
    return h;
  }

  bool equal(const S0cQ0pQ1wpValue &other) const
  {
    if(other.type == type && other.tagValueQ0 == tagValueQ0 && other.wordValueQ1 == wordValueQ1 && other.tagValueQ1 == tagValueQ1 &&
        other.action == action)
    {
      if (eqSR(other.catS0, catS0))
        return true;
      else
        return false;
    }
    else
    {
      return false;
    }
  }
};

inline std::ostream &operator<<(std::ostream& out, const S0cQ0pQ1wpValue& value) {
  out << value.type << " " << value.catS0 << " " << value.tagValueQ0.str() << " " << value.wordValueQ1.str()
		        << " " << value.tagValueQ1.str() << " " << value.action;
  return out;
}


struct S0cQ0pQ1pValue
{
  friend std::ostream &operator<<(std::ostream&, const S0cQ0pQ1pValue&);

  const size_t type;
  const Cat *catS0;
  const Word tagValueQ0;
  const Word tagValueQ1;
  const ShiftReduceAction action;

  S0cQ0pQ1pValue(const size_t type, const Cat *catS0, const Word tagValueQ0, const Word tagValueQ1, const ShiftReduceAction &action)
  : type(type), catS0(catS0),
    tagValueQ0(tagValueQ0), tagValueQ1(tagValueQ1), action(action) {}

  Hash hash(void) const
  {
    Hash h(static_cast<ulong>(type));
    h += catS0->rhash;
    h += static_cast<ulong>(tagValueQ0);
    h += static_cast<ulong>(tagValueQ1);
    h += action.GetCat()->rhash;
    h += static_cast<ulong>(action.GetAction());
    return h;
  }

  bool equal(const S0cQ0pQ1pValue &other) const
  {
    if(other.type == type && other.tagValueQ0 == tagValueQ0 && other.tagValueQ1 == tagValueQ1 &&
        other.action == action)
    {
      if (eqSR(other.catS0, catS0))
        return true;
      else
        return false;
    }
    else
    {
      return false;
    }
  }
};

inline std::ostream &operator<<(std::ostream& out, const S0cQ0pQ1pValue& value) {
  out << value.type << " " << value.catS0 << " " << value.tagValueQ0.str() << " " << value.tagValueQ1.str()
		        << " " << value.action;
  return out;
}

struct S0wcS1cS2cValue
{
  friend std::ostream &operator<<(std::ostream&, const S0wcS1cS2cValue&);

  const size_t type;
  const Word wordValueS0;
  const Cat *catS0;
  const Cat *catS1;
  const Cat *catS2;
  const ShiftReduceAction action;

  S0wcS1cS2cValue(const size_t type, const Word wordValueS0, const Cat *catS0, const Cat *catS1, const Cat *catS2, const ShiftReduceAction &action)
  : type(type), wordValueS0(wordValueS0), catS0(catS0), catS1(catS1), catS2(catS2), action(action) {}

  Hash hash(void) const
  {
    Hash h(static_cast<ulong>(type));
    h += static_cast<ulong>(wordValueS0);
    h += catS0->rhash;
    h += catS1->rhash;
    h += catS2->rhash;
    h += action.GetCat()->rhash;
    h += static_cast<ulong>(action.GetAction());
    return h;
  }

  bool equal(const S0wcS1cS2cValue &other) const
  {
    if(other.type == type && other.wordValueS0 == wordValueS0 &&
        other.action == action)
    {
      if (eqSR(other.catS0, catS0) && eqSR(other.catS1, catS1) && eqSR(other.catS2, catS2))
        return true;
      else
        return false;
    }
    else
    {
      return false;
    }
  }
};

inline std::ostream &operator<<(std::ostream& out, const S0wcS1cS2cValue& value) {
  out << value.type << " " << value.wordValueS0.str() << " " << value.catS0 << " " << value.catS1 <<
      " " << value.catS2 << " " << value.action;
  return out;
}


struct S0cS1wcS2cValue
{
  friend std::ostream &operator<<(std::ostream&, const S0cS1wcS2cValue&);

  const size_t type;
  const Cat *catS0;
  const Word wordValueS1;
  const Cat *catS1;
  const Cat *catS2;
  const ShiftReduceAction action;

  S0cS1wcS2cValue(const size_t type, const Cat *catS0, const Word wordValueS1, const Cat *catS1, const Cat *catS2, const ShiftReduceAction &action)
  : type(type), catS0(catS0), wordValueS1(wordValueS1), catS1(catS1), catS2(catS2), action(action) {}

  Hash hash(void) const
  {
    Hash h(static_cast<ulong>(type));
    h += catS0->rhash;
    h += static_cast<ulong>(wordValueS1);
    h += catS1->rhash;
    h += catS2->rhash;
    h += action.GetCat()->rhash;
    h += static_cast<ulong>(action.GetAction());
    return h;
  }

  bool equal(const S0cS1wcS2cValue &other) const
  {
    if(other.type == type && other.wordValueS1 == wordValueS1 &&
        other.action == action)
    {
      if (eqSR(other.catS0, catS0) && eqSR(other.catS1, catS1) && eqSR(other.catS2, catS2))
        return true;
      else
        return false;
    }
    else
    {
      return false;
    }
  }
};

inline std::ostream &operator<<(std::ostream& out, const S0cS1wcS2cValue& value) {
  out << value.type  << " " << value.catS0 << " " << value.wordValueS1.str() << " " << value.catS1
      << " " << value.catS2 << " " << value.action;
  return out;
}


struct S0cS1cS2wcValue
{
  friend std::ostream &operator<<(std::ostream&, const S0cS1cS2wcValue&);

  const size_t type;
  const Cat *catS0;
  const Cat *catS1;
  const Word wordValueS2;
  const Cat *catS2;
  const ShiftReduceAction action;

  S0cS1cS2wcValue(const size_t type, const Cat *catS0, const Cat *catS1, const Word wordValueS2, const Cat *catS2, const ShiftReduceAction &action)
  : type(type), catS0(catS0), catS1(catS1), wordValueS2(wordValueS2), catS2(catS2), action(action) {}

  Hash hash(void) const
  {
    Hash h(static_cast<ulong>(type));
    h += catS0->rhash;
    h += catS1->rhash;
    h += static_cast<ulong>(wordValueS2);
    h += catS2->rhash;
    h += action.GetCat()->rhash;
    h += static_cast<ulong>(action.GetAction());
    return h;
  }

  bool equal(const S0cS1cS2wcValue &other) const
  {
    if(other.type == type && other.wordValueS2 == wordValueS2 &&
        other.action == action)
    {
      if (eqSR(other.catS0, catS0) && eqSR(other.catS1, catS1) && eqSR(other.catS2, catS2))
        return true;
      else
        return false;
    }
    else
    {
      return false;
    }
  }
};

inline std::ostream &operator<<(std::ostream& out, const S0cS1cS2wcValue& value) {
  out << value.type  << " " << value.catS0 << " " << value.catS1 <<
      " " << value.wordValueS2.str() << " " << value.catS2 << " " << value.action;
  return out;
}

struct S0cS1cS2cValue
{
  friend std::ostream &operator<<(std::ostream&, const S0cS1cS2cValue&);

  const size_t type;
  const Cat *catS0;
  const Cat *catS1;
  const Cat *catS2;
  const ShiftReduceAction action;

  S0cS1cS2cValue(const size_t type, const Cat *catS0, const Cat *catS1, const Cat *catS2, const ShiftReduceAction &action)
  : type(type), catS0(catS0), catS1(catS1), catS2(catS2), action(action) {}

  Hash hash(void) const
  {
    Hash h(static_cast<ulong>(type));
    h += catS0->rhash;
    h += catS1->rhash;
    h += catS2->rhash;
    h += action.GetCat()->rhash;
    h += static_cast<ulong>(action.GetAction());
    return h;
  }

  bool equal(const S0cS1cS2cValue &other) const
  {
    if(other.type == type && other.action == action)
    {
      if (eqSR(other.catS0, catS0) && eqSR(other.catS1, catS1) && eqSR(other.catS2, catS2))
        return true;
      else
        return false;
    }
    else
    {
      return false;
    }
  }
};

inline std::ostream &operator<<(std::ostream& out, const S0cS1cS2cValue& value) {
  out << value.type  << " " << value.catS0 << " " << value.catS1 <<
      " " << value.catS2 << " " << value.action;
  return out;
}


// 6

struct S01cS01LcS01rcValue
{
  friend std::ostream &operator<<(std::ostream&, const S01cS01LcS01rcValue&);

  const size_t type;
  const Cat *cat0;
  const Cat *cat1;
  const Cat *cat2;
  const ShiftReduceAction action;

  S01cS01LcS01rcValue(const size_t type, const Cat *cat0, const Cat *cat1, const Cat *cat2, const ShiftReduceAction &action)
  : type(type), cat0(cat0), cat1(cat1), cat2(cat2), action(action) {}

  Hash hash(void) const
  {
    Hash h(static_cast<ulong>(type));
    h += cat0->rhash;
    h += cat1->rhash;
    h += cat2->rhash;
    h += action.GetCat()->rhash;
    h += static_cast<ulong>(action.GetAction());
    return h;
  }

  bool equal(const S01cS01LcS01rcValue &other) const
  {
    if (other.type == type && other.action == action)
      if (eqSR(other.cat0, cat0) && eqSR(other.cat1, cat1) && eqSR(other.cat2, cat2))
        return true;
      else
        return false;
    else
      return false;
  }
};

inline std::ostream &operator<<(std::ostream& out, const S01cS01LcS01rcValue& value) {
  out << value.type  << " " << value.cat0 << " " << value.cat1 <<
      " " << value.cat2 << " " << value.action;
  return out;
}


struct S0cS0RcQ0pValue
{
  friend std::ostream &operator<<(std::ostream&, const S0cS0RcQ0pValue&);

  const size_t type;
  const Cat *s0c;
  const Cat *s0rc;
  const Word q0p;
  const ShiftReduceAction action;

  S0cS0RcQ0pValue(const size_t type, const Cat *s0c, const Cat *s0rc, const Word q0p, const ShiftReduceAction &action)
  : type(type), s0c(s0c), s0rc(s0rc), q0p(q0p), action(action) {}

  Hash hash(void) const
  {
    Hash h(static_cast<ulong>(type));
    h += s0c->rhash;
    h += s0rc->rhash;
    h += static_cast<ulong>(q0p);
    h += action.GetCat()->rhash;
    h += static_cast<ulong>(action.GetAction());
    return h;
  }

  bool equal(const S0cS0RcQ0pValue &other) const
  {
    if(other.type == type && other.q0p == q0p && other.action == action)
      if (eqSR(other.s0c, s0c) && eqSR(other.s0rc, s0rc))
        return true;
      else
        return false;
    else
      return false;
  }
};

inline std::ostream &operator<<(std::ostream& out, const S0cS0RcQ0pValue& value) {
  out << value.type  << " " << value.s0c << " " << value.s0rc <<
      " " << value.q0p.str() << " " << value.action;
  return out;
}


struct S0cS0LRcQ0S1wValue
{
  friend std::ostream &operator<<(std::ostream&, const S0cS0LRcQ0S1wValue&);

  const size_t type;
  const Cat *s0c;
  const Cat *s0lrc;
  const Word word;
  const ShiftReduceAction action;

  S0cS0LRcQ0S1wValue(const size_t type, const Cat *s0c, const Cat *s0lrc, Word word, const ShiftReduceAction &action)
  : type(type), s0c(s0c), s0lrc(s0lrc), word(word), action(action) {}

  Hash hash(void) const
  {
    Hash h(static_cast<ulong>(type));
    h += s0c->rhash;
    h += s0lrc->rhash;
    h += static_cast<ulong>(word);
    h += action.GetCat()->rhash;
    h += static_cast<ulong>(action.GetAction());
    return h;
  }

  bool equal(const S0cS0LRcQ0S1wValue &other) const
  {
    if(other.type == type && other.word == word && other.action == action)
      if (eqSR(other.s0c, s0c) && eqSR(other.s0lrc, s0lrc))
        return true;
      else
        return false;
    else
      return false;
  }
};

inline std::ostream &operator<<(std::ostream& out, const S0cS0LRcQ0S1wValue& value) {
  out << value.type  << " " << value.s0c << " " << value.s0lrc <<
      " " << value.word.str() << " " << value.action;
  return out;
}

struct S0cS0LS1cS1S1RcValue
{
  friend std::ostream &operator<<(std::ostream&, const S0cS0LS1cS1S1RcValue&);

  const size_t type;
  const Cat *s0c;
  const Cat *s0lrc;
  const Cat *s1s1rc;
  const ShiftReduceAction action;

  S0cS0LS1cS1S1RcValue(const size_t type, const Cat *s0c, const Cat *s0lrc, const Cat *s1s1rc, const ShiftReduceAction &action)
  : type(type), s0c(s0c), s0lrc(s0lrc), s1s1rc(s1s1rc), action(action) {}

  Hash hash(void) const
  {
    Hash h(static_cast<ulong>(type));
    h += s0c->rhash;
    h += s0lrc->rhash;
    h += s1s1rc->rhash;
    h += action.GetCat()->rhash;
    h += static_cast<ulong>(action.GetAction());
    return h;
  }

  bool equal(const S0cS0LS1cS1S1RcValue &other) const
  {
    if(other.type == type && other.action == action)
      if (eqSR(other.s0c, s0c) && eqSR(other.s0lrc, s0lrc) && eqSR(other.s1s1rc, s1s1rc))
        return true;
      else
        return false;
    else
      return false;
  }
};

inline std::ostream &operator<<(std::ostream& out, const S0cS0LS1cS1S1RcValue& value) {
  out << value.type  << " " << value.s0c << " " << value.s0lrc <<
      " " << value.s1s1rc << " " << value.action;
  return out;
}

struct S0wS1cS1RcValue
{
  friend std::ostream &operator<<(std::ostream&, const S0wS1cS1RcValue&);

  const size_t type;
  const Word s0w;
  const Cat *s1c;
  const Cat *s1rc;
  const ShiftReduceAction action;

  S0wS1cS1RcValue(const size_t type, const Word s0w, const Cat *s1c, const Cat *s1rc, const ShiftReduceAction &action)
  : type(type), s0w(s0w), s1c(s1c), s1rc(s1rc), action(action) {}

  Hash hash(void) const
  {
    Hash h(static_cast<ulong>(type));
    h += static_cast<ulong>(s0w);
    h += s1c->rhash;
    h += s1rc->rhash;
    h += action.GetCat()->rhash;
    h += static_cast<ulong>(action.GetAction());
    return h;
  }

  bool equal(const S0wS1cS1RcValue &other) const
  {
    if(other.type == type && other.s0w == s0w && other.action == action)
      if (eqSR(other.s1c, s1c) && eqSR(other.s1rc, s1rc))
        return true;
      else
        return false;
    else
      return false;
  }
};

inline std::ostream &operator<<(std::ostream& out, const S0wS1cS1RcValue& value) {
  out << value.type  << " " << value.s0w.str() << " " << value.s1c <<
      " " << value.s1rc << " " << value.action;
  return out;
}


}
}


#endif /* SHIFTREDUCEFEATURE_H_ */
