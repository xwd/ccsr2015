#ifndef GRAPHATTRIBUTES_H_
#define GRAPHATTRIBUTES_H_

#include "base.h"
#include <cstddef>

namespace NLP { namespace CCG {

class PxValue;
class PxyValue;
class PxyCxyValue;
class XtYtZtValue;
class WtXtYtZtValue;
class DepValue;
class DepValueSimple;

class SRDepValue;

class PxAttributes
{
public:
    PxAttributes(void);
    PxAttributes(PxAttributes &other);
    ~PxAttributes(void);

    size_t size(void) const;
    void SaveWeights(std::ofstream &out);
    void SetFeature(const PxValue &value, double weightT, double weight);

    double GetOrUpdateWeight(bool update, bool correct,
	    const ulong type, const Word word, const ulong lr, const ulong dist);

private:
    class Impl;
    Impl *_impl;
};

class PxyAttributes
{
public:
    PxyAttributes(void);
    PxyAttributes(PxyAttributes &other);
    ~PxyAttributes(void);

    size_t size(void) const;
    void SaveWeights(std::ofstream &out);
    void SetFeature(const PxyValue &value, double weightT, double weight);

    double GetOrUpdateWeight(bool update, bool correct,
	    const ulong type, const Word word, const Word tag, const ulong lr, const ulong dist);

private:
    class Impl;
    Impl *_impl;
};

class PxyCxyAttributes
{
public:
    PxyCxyAttributes(void);
    PxyCxyAttributes(PxyCxyAttributes &other);
    ~PxyCxyAttributes(void);

    size_t size(void) const;
    void SaveWeights(std::ofstream &out);
    void SetFeature(const PxyCxyValue &value, double weightT, double weight);

    double GetOrUpdateWeight(bool update, bool correct,
	    const ulong type, const Word w1, const Word t1, const Word w2, const Word t2, const ulong lr, const ulong dist);

private:
    class Impl;
    Impl *_impl;
};

class XtYtZtAttributes
{
public:
    XtYtZtAttributes(void);
    XtYtZtAttributes(XtYtZtAttributes &other);
    ~XtYtZtAttributes(void);

    size_t size(void) const;
    void SaveWeights(std::ofstream &out);
    void SetFeature(const XtYtZtValue &value, double weightT, double weight);

    double GetOrUpdateWeight(bool update, bool correct,
	    const ulong type, const Word t1, const Word t2, const Word t3, const ulong lr, const ulong dist);

private:
    class Impl;
    Impl *_impl;
};

class WtXtYtZtAttributes
{
public:
    WtXtYtZtAttributes(void);
    WtXtYtZtAttributes(WtXtYtZtAttributes &other);
    ~WtXtYtZtAttributes(void);

    size_t size(void) const;
    void SaveWeights(std::ofstream &out);
    void SetFeature(const WtXtYtZtValue &value, double weightT, double weight);

    double GetOrUpdateWeight(bool update, bool correct,
	    const ulong type, const Word t1, const Word t2, const Word t3, const Word t4, const ulong lr, const ulong dist);

private:
    class Impl;
    Impl *_impl;
};

class DepAttributes {
public:
  DepAttributes(void);
  DepAttributes(DepAttributes &other);
  ~DepAttributes(void);

  size_t size(void) const;
  void SaveWeights(std::ofstream &out);
  void SetFeature(const DepValue &value, double weightT, double weight);

  double GetOrUpdateWeight(bool update, bool correct,
	  const ulong type, Word h, RelID rel, Word var, RuleID r, CatID lrange, ushort dist) const;
private:
  class Impl;
  Impl *_impl;
};

class DepAttributesSimple {
public:
    DepAttributesSimple(void);
    DepAttributesSimple(DepAttributesSimple &other);
    ~DepAttributesSimple(void);

  size_t size(void) const;
  void SetFeature(const DepValueSimple &value);
  double Get(const ulong sentId, Position head, RelID rel, Position var, CatID lrange, RuleID rule) const;
private:
  class Impl;
  Impl *_impl;
};

// c&c dep and dep_dis feats
class SRDepAttributes {

  typedef ulong Type;

public:
  SRDepAttributes(void);
  SRDepAttributes(SRDepAttributes &other);
  ~SRDepAttributes(void);

  size_t size(void) const;
  void SaveWeights(std::ofstream &out);
  void SetFeature(const SRDepValue &value, double weightT, double weight);
  double GetOrUpdateWeight(bool update, bool correct,
      Type t, Word h, RelID rel, Word var, RuleID r, CatID lrange, ushort dist) const;
private:
  class Impl;
  Impl *_impl;
};


} }


#endif /* GRAPHATTRIBUTES_H_ */
