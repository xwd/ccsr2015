#ifndef SHIFTREDUCELATTICE_H_
#define SHIFTREDUCELATTICE_H_

#include <cstdlib>
#include "std.h"
#include "pool.h"

#include "parser/ShiftReduceHypothesis.h"

namespace NLP {
namespace CCG {

class ShiftReduceLattice {

public:
  ShiftReduceLattice(const size_t nwords, const size_t beamSize, const size_t maxunaryactions)
: m_latticeArray(new const ShiftReduceHypothesis*[(nwords * (2 + maxunaryactions) + 2) * beamSize]),
  m_endIndex(0),
  m_prevEndIndex(0){}

  ~ShiftReduceLattice() {
    for (size_t i = 0; i <= this->GetEndIndex(); ++i)
      delete m_latticeArray[i];
    delete[] m_latticeArray;
  }

  void Add(const size_t index, const ShiftReduceHypothesis *hypo) {
    m_latticeArray[index] = hypo;
  }

  void IncrementEndIndex() {
    ++m_endIndex;
  }

  size_t GetEndIndex() const {
    return m_endIndex;
  }

  size_t GetPrevEndIndex() const {
    return m_prevEndIndex;
  }

  void SetPrevEndIndex(size_t ind) {
    m_prevEndIndex = ind;
  }

  const ShiftReduceHypothesis *GetLatticeArray(size_t ind) const {
    return m_latticeArray[ind];
  }


private:
  const ShiftReduceHypothesis **m_latticeArray;
  size_t m_endIndex;
  size_t m_prevEndIndex;
};

}}


#endif /* SHIFTREDUCELATTICE_H_ */
