#ifndef SHIFTREDUCEHYPOTHESISQUEUE_H_
#define SHIFTREDUCEHYPOTHESISQUEUE_H_

#include "parser/ShiftReduceHypothesis.h"

#include <queue>
#include <vector>
#include <armadillo>


namespace NLP
{
namespace CCG
{

class HypothesisTuple
{
  const ShiftReduceHypothesis *m_currentHypo;
  const ShiftReduceAction *m_action;
  double m_hypoTotalScore;

  //debug
  const size_t m_indexInLattice;

public:
  // max-margin related
  arma::mat m_hidden_states;
  const size_t m_action_id;
  double m_action_score;
  Expression m_act_log_prob;

  HypothesisTuple(const ShiftReduceHypothesis *currentHypo, const ShiftReduceAction *action,
                  double hypoTotalScore, const size_t indexInLattice)
: m_currentHypo(currentHypo),
  m_action(action),
  m_hypoTotalScore(hypoTotalScore),
  m_indexInLattice(indexInLattice),
  m_action_id(0),
  m_action_score(0.0) {}


  HypothesisTuple(const ShiftReduceHypothesis *currentHypo, const ShiftReduceAction *action,
                  double hypoTotalScore, const size_t indexInLattice, const arma::mat &hidden_states)
: m_currentHypo(currentHypo),
  m_action(action),
  m_hypoTotalScore(hypoTotalScore),
  m_indexInLattice(indexInLattice),
  m_hidden_states(hidden_states),
  m_action_id(0),
  m_action_score(0.0) {}

  // for max-margin objective
  HypothesisTuple(const ShiftReduceHypothesis *currentHypo, const ShiftReduceAction *action,
                  double hypoTotalScore, const size_t indexInLattice, const size_t action_id, const double action_score)
: m_currentHypo(currentHypo),
  m_action(action),
  m_hypoTotalScore(hypoTotalScore),
  m_indexInLattice(indexInLattice),
  m_action_id(action_id),
  m_action_score(action_score) {}

  // for xf1 training
  HypothesisTuple(const ShiftReduceHypothesis *currentHypo, const ShiftReduceAction *action,
                  double hypoTotalScore, const size_t indexInLattice, const size_t action_id, Expression act_log_prob)
: m_currentHypo(currentHypo),
  m_action(action),
  m_hypoTotalScore(hypoTotalScore),
  m_indexInLattice(indexInLattice),
  m_action_id(action_id),
  m_action_score(0.0),
  m_act_log_prob(act_log_prob) {}

  ~HypothesisTuple()
  {
    delete m_action;
    //delete m_currentHypo;
  }

  const ShiftReduceHypothesis *GetCurrentHypo () const
  {
    return m_currentHypo;
  }

  const ShiftReduceAction *GetAction() const
  {
    return m_action;
  }

  double GetHypoTotalScore() const
  {
    return m_hypoTotalScore;
  }

  const size_t GetIndexInLattice() const
  {
    return m_indexInLattice;
  }

};

class HypothesisTupleOrderer
{
public:
  bool operator()(const HypothesisTuple *A, const HypothesisTuple *B) const
  {
    return A->GetHypoTotalScore() < B->GetHypoTotalScore();
  }
};


typedef std::priority_queue<HypothesisTuple*, std::vector<HypothesisTuple*>,
    HypothesisTupleOrderer> HypothesisPQueue;

}
}



#endif /* SHIFTREDUCEHYPOTHESISQUEUE_H_ */
