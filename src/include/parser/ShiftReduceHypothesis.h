#ifndef SHIFTREDUCEHYPOTHESIS_H_
#define SHIFTREDUCEHYPOTHESIS_H_

//#include "parser/ShiftReduceLattice.h"
#include "parser/supercat.h"
//#include "parser/parser.h"
#include <armadillo>
#include <unordered_map>
#include "cnn/rnn.h"
#include "cnn/lstm.h"
//#include "cnn/nodes.h"
#include "cnn/cnn.h"
//#include "cnn/training.h"
//#include "cnn/timing.h"
//#include "cnn/dict.h"
//#include "cnn/expr.h"

using namespace std;

#define MAX_UNARY_ACTIONS 3
#define SHIFT 0
#define UNARY 1
#define COMBINE 2
#define FINISH 3

namespace NLP {
namespace CCG {

class ShiftReduceHypothesis {
public:
  ShiftReduceHypothesis(const SuperCat *stackTopCat,
      const ShiftReduceHypothesis *prevHypo,
      const ShiftReduceHypothesis *prevStack,
      size_t nextInputIndex,
      size_t unaryActionCount,
      size_t stackSize,
      double totalScore,
      bool finished,
      size_t stackTopAction,
      size_t totalActionCount,
      const size_t total_shift_count,
      const size_t action_id,
      const double action_score,
      const size_t indexInLattice)
: m_stackTopSuperCat(stackTopCat),
  m_prevHypo(prevHypo),
  m_prevStack(prevStack),
  m_nextInputIndex(nextInputIndex),
  m_unaryActionCount(unaryActionCount),
  m_stackSize(stackSize),
  m_totalScore(totalScore),
  m_finished(finished),
  m_stackTopAction(stackTopAction),
  m_totalActionCount(totalActionCount),
  m_isStartHypo(false),
  m_total_shift_count(total_shift_count),
  m_total_gold_action_count(0.0),
  m_action_id(action_id),
  m_action_score(action_score),
  m_stack_top_action_is_gold(false),
  m_indexInLattice(indexInLattice),
  m_in_kbest(false),
  m_hidden_states_set(false),
  m_w_stackbuilder(0),
  m_a_stackbuilder(0),
  m_s_stackbuilder(0),
  m_p_stackbuilder(0),
  m_c_stackbuilder(0),
  m_lstm_w_state(-1),
  m_lstm_a_state(-1),
  m_lstm_s_state(-1),
  m_lstm_p_state(-1) {}

  ~ShiftReduceHypothesis() {
    //delete m_prevHypo;
    //delete m_prevStack;
    if (m_c_stack_pointers.size() > 0) m_c_stack_pointers.clear();
    if (m_w_stack_pointers.size() > 0) m_w_stack_pointers.clear();
    if (m_s_stack_pointers.size() > 0) m_s_stack_pointers.clear();
    if (m_a_stack_pointers.size() > 0) m_a_stack_pointers.clear();
    if (m_p_stack_pointers.size() > 0) m_p_stack_pointers.clear();
    m_w_stackbuilder = 0;
    m_s_stackbuilder = 0;
    m_a_stackbuilder = 0;
    m_p_stackbuilder = 0;
    m_c_stackbuilder = 0;
    if (m_w_stack_contexts.size() > 0) m_w_stack_contexts.clear();
    if (m_s_stack_contexts.size() > 0) m_s_stack_contexts.clear();
    if (m_a_stack_contexts.size() > 0) m_a_stack_contexts.clear();
    if (m_p_stack_contexts.size() > 0) m_p_stack_contexts.clear();
  }

  arma::mat m_hidden_states; // the hidden layer that this hypo used to derive the next action
  std::vector<vector<size_t>> m_context_vec;
  arma::colvec m_converted_context_vec;
  arma::colvec m_x_mask_vec; // dropout mask for this hypo's context
  arma::mat m_output_vals_feasible; // the softmax_ed feasible output vals (this action included)
  mutable std::vector<const ShiftReduceHypothesis*> m_child_hypos;
  mutable arma::mat m_delta_h_vec;
  mutable arma::mat m_total_child_delta_h;
  mutable arma::mat m_total_delta_y;
  mutable std::unordered_map<size_t, std::vector<size_t>> m_action_kbest_hypo_inds_map;
  Expression m_total_log_prob;
  // key: a feasible action_id for this hypo, val: all kbest output inds this feasbile action leads to

  /*
    void *operator new(size_t size, Pool *pool) {
	return (void*)pool->alloc(size);
    }
   */

  //void operator delete(void *, Pool *pool) { /* do nothing */ }

  const ShiftReduceHypothesis* Shift(size_t index, const SuperCat *superCat,
                                     size_t unaryActionCount, const double totalScore,
                                     const size_t action_id, const double action_score,
                                     Expression &act_log_prob) const;
  const ShiftReduceHypothesis* Unary(size_t index, const SuperCat *superCat,
                                     size_t unaryActionCount, const double totalScore,
                                     const size_t action_id, const double action_score,
                                     Expression &act_log_prob) const;
  const ShiftReduceHypothesis* Combine(size_t index, const SuperCat *superCat,
                                       size_t unaryActionCount, const double totalScore,
                                       const size_t action_id, const double action_score,
                                       Expression &act_log_prob) const;
  //void Finish(size_t index, ShiftReduceLattice *lattice);

  const ShiftReduceHypothesis *GetPrevHypo() const { return m_prevHypo; }
  const ShiftReduceHypothesis *GetPrvStack() const { return m_prevStack; }
  size_t GetNextInputIndex() const { return m_nextInputIndex; }
  const SuperCat *GetStackTopSuperCat() const { return m_stackTopSuperCat; }
  void ReSetUnaryActionCount() { m_unaryActionCount = 0; }
  size_t GetUnaryActionCount() const { return m_unaryActionCount; }
  size_t GetStackSize() const { return m_stackSize; }
  double GetTotalScore() const { return m_totalScore; }
  void SetTotalScore(double score) { m_totalScore = score; }
  bool IsFinished(size_t NWORDS, bool allowFragTree) const {
    if (allowFragTree)
      return (m_nextInputIndex == NWORDS);
    else
      return m_nextInputIndex == NWORDS && m_stackSize == 1 ;
  }

  size_t GetStackTopAction() const { return m_stackTopAction; }
  size_t GetTotalActionCount() const { return m_totalActionCount; }
  void SetStartHypoFlag() { m_isStartHypo = true; }
  bool IsStartHypo() const { return m_isStartHypo; }
  const size_t GetIndexInLattice() const { return m_indexInLattice; }
  const size_t get_shift_count() const { return m_total_shift_count; }

  void update_w_state(cnn::RNNPointer p) { m_lstm_w_state = p; }
  void update_s_state(cnn::RNNPointer p) { m_lstm_s_state = p; }
  void update_a_state(cnn::RNNPointer p) { m_lstm_a_state = p; }
  void update_p_state(cnn::RNNPointer p) { m_lstm_p_state = p; }

public:
  const SuperCat *m_stackTopSuperCat;
  const ShiftReduceHypothesis *m_prevHypo;
  const ShiftReduceHypothesis *m_prevStack;
  size_t m_nextInputIndex;
  size_t m_unaryActionCount;
  size_t m_stackSize;
  double m_totalScore;
  bool m_finished;
  size_t m_stackTopAction;
  size_t m_totalActionCount;
  bool m_isStartHypo;
  size_t m_total_shift_count;
  double m_total_gold_action_count;
  size_t m_action_id;
  double m_action_score; // the score of the action that created this hypo
  bool m_stack_top_action_is_gold;
  arma::mat m_action_score_vec; // feasible actions only
  arma::mat m_feasible_action_ids;
  const size_t m_indexInLattice;
  mutable bool m_in_kbest;
  bool m_hidden_states_set;
  cnn::LSTMBuilder *m_w_stackbuilder;
  cnn::LSTMBuilder *m_a_stackbuilder;
  cnn::LSTMBuilder *m_s_stackbuilder;
  cnn::LSTMBuilder *m_p_stackbuilder;
  cnn::LSTMBuilder *m_c_stackbuilder;
  cnn::RNNPointer m_lstm_w_state;
  cnn::RNNPointer m_lstm_a_state;
  cnn::RNNPointer m_lstm_s_state;
  cnn::RNNPointer m_lstm_p_state;
  vector<cnn::RNNPointer> m_c_stack_pointers;
  vector<cnn::RNNPointer> m_w_stack_pointers;
  vector<cnn::RNNPointer> m_s_stack_pointers;
  vector<cnn::RNNPointer> m_a_stack_pointers;
  vector<cnn::RNNPointer> m_p_stack_pointers;
  vector<Expression> m_w_stack_contexts;
  vector<Expression> m_s_stack_contexts;
  vector<Expression> m_a_stack_contexts;
  vector<Expression> m_p_stack_contexts;
};

class ShiftReduceAction {
public:
  ShiftReduceAction(const size_t action, const SuperCat *supercat)
: m_action(action),
  m_supercat(supercat),
  m_cat(supercat->cat){}

  ShiftReduceAction(const size_t action, const Cat *cat)
  : m_action(action),
    m_supercat(0),
    m_cat(cat){}


  /*
    ShiftReduceAction(const ShiftReduceAction &other)
    : m_action(other.m_action), m_supercat(0), m_cat(Cat::Clone(Parser::s_shiftReduceFeaturePool, other.m_cat))
    {}*/

  ~ShiftReduceAction() {
    //delete m_supercat; //delete m_cat;
   }

  size_t GetAction() const { return m_action; }
  const SuperCat *GetSuperCat() const {  return m_supercat; }
  const Cat *GetCat() const { return m_cat;}

  friend bool operator==(const ShiftReduceAction &actionA, const ShiftReduceAction &actionB);
  friend std::ostream &operator<<(std::ostream&, const ShiftReduceAction&);


private:
  size_t m_action;
  const SuperCat *m_supercat;
  const Cat *m_cat;
};

inline bool operator==(const ShiftReduceAction &actionA, const ShiftReduceAction &actionB) {
  return actionA.m_action == actionB.m_action && eqSR(actionA.m_cat, actionB.m_cat);
  //return actionA.m_action == actionB.m_action && actionA.m_cat->out_novar_noX_noNB_SR_str() == actionB.m_cat->out_novar_noX_noNB_SR_str();
}

inline std::ostream &operator<<(std::ostream& out, const ShiftReduceAction& action) {
  out <<  action.GetAction() << " " << action.GetCat()->out_novar_noX_noNB_SR_str();
  //out <<  action.GetAction() << " ";
  //out << *action.GetCat();
  return out;
}

}
}


#endif /* SHIFTREDUCEHYPOTHESIS_H_ */
