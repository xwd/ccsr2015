#ifndef SHIFTREDUCEATTRIBUTES_H_
#define SHIFTREDUCEATTRIBUTES_H_


#include "base.h"
#include <cstddef>


namespace NLP
{
  namespace CCG
  {

  class SwpValue;
  class ScValue;
  class SpcValue;
  class SwcValue;

  class QwpValue;

  class SlurpcValue;
  class SlurwcValue;

  class S0wcS1wcValue;
  class S0cS1wValue;
  class S0wS1cValue;
  class S0cS1cValue;
  class SwcQwpValue;
  class ScQwpValue;
  class SwcQpValue;
  class ScQpValue;

  class S0wcS1cQ0pValue;
  class S0cS1wcQ0pValue;
  class S0cS1cQ0wpValue;
  class S0cS1cQ0pValue;
  class S0pSQpQSpValue;
  class S0wcQ0pQ1pValue;
  class S0cQ0wpQ1pValue;
  class S0cQ0pQ1wpValue;
  class S0cQ0pQ1pValue;
  class S0wcS1cS2cValue;
  class S0cS1wcS2cValue;
  class S0cS1cS2wcValue;
  class S0cS1cS2cValue;

  class S01cS01LcS01rcValue;
  class S0cS0RcQ0pValue;
  class S0cS0LRcQ0S1wValue;
  class S0cS0LS1cS1S1RcValue;
  class S0wS1cS1RcValue;


  class ShiftReduceAction;

  // 1

  class SwpAttributes
  {
  public:
      SwpAttributes(void);
      SwpAttributes(SwpAttributes &other);
      ~SwpAttributes(void);

      size_t size(void) const;
      void SaveWeights(std::ofstream &out);
      void SetFeature(const SwpValue &value, double weightT, double weight);

      double GetOrUpdateWeight(bool update, bool correct,
	      const size_t type, const Word wordValue, const Word tagValue,
	      const ShiftReduceAction &action);

  private:
      class Impl;
      Impl *_impl;
  };


  class ScAttributes
  {
  public:
      ScAttributes(void);
      ScAttributes(ScAttributes &other);
      ~ScAttributes(void);

      size_t size(void) const;
      void SaveWeights(std::ofstream &out);
      void SetFeature(const ScValue &value, double weightT, double weight);

      double GetOrUpdateWeight(bool update, bool correct,
	      const size_t type, const Cat *cat,
	      const ShiftReduceAction &action);

  private:
      class Impl;
      Impl *_impl;
  };


  class SpcAttributes
  {
  public:
      SpcAttributes(void);
      SpcAttributes(SpcAttributes &other);
      ~SpcAttributes(void);

      size_t size(void) const;
      void SaveWeights(std::ofstream &out);
      void SetFeature(const SpcValue &value, double weightT, double weight);

      double GetOrUpdateWeight(bool update, bool correct,
	      const size_t type, const Word tagValue, const Cat *cat,
	      const ShiftReduceAction &action);

  private:
      class Impl;
      Impl *_impl;
  };


  class SwcAttributes
  {
  public:
      SwcAttributes(void);
      SwcAttributes(SwcAttributes &other);
      ~SwcAttributes(void);

      size_t size(void) const;
      void SaveWeights(std::ofstream &out);
      void SetFeature(const SwcValue &value, double weightT, double weight);

      double GetOrUpdateWeight(bool update, bool correct,
	      const size_t type, const Word wordValue, const Cat *cat,
	      const ShiftReduceAction &action);

  private:
      class Impl;
      Impl *_impl;
  };


  // 2

  class QwpAttributes
  {
  public:
      QwpAttributes(void);
      QwpAttributes(QwpAttributes &other);
      ~QwpAttributes(void);

      size_t size(void) const;
      void SaveWeights(std::ofstream &out);
      void SetFeature(const QwpValue &value, double weightT, double weight);

      double GetOrUpdateWeight(bool update, bool correct,
	      const size_t type, const Word wordValue, const Word tagValue,
	      const ShiftReduceAction &action);
  private:
      class Impl;
      Impl *_impl;
  };


  // 3

  class SlurpcAttributes
  {
  public:
      SlurpcAttributes(void);
      SlurpcAttributes(SlurpcAttributes &other);
      ~SlurpcAttributes(void);

      size_t size(void) const;
      void SaveWeights(std::ofstream &out);
      void SetFeature(const SlurpcValue &value, double weightT, double weight);

      double GetOrUpdateWeight(bool update, bool correct,
	      const size_t type, const Word tagValue, const Cat *cat,
	      const ShiftReduceAction &action);
  private:
      class Impl;
      Impl *_impl;
  };


  class SlurwcAttributes
  {
  public:
      SlurwcAttributes(void);
      SlurwcAttributes(SlurwcAttributes &other);
      ~SlurwcAttributes(void);

      size_t size(void) const;
      void SaveWeights(std::ofstream &out);
      void SetFeature(const SlurwcValue &value, double weightT, double weight);

      double GetOrUpdateWeight(bool update, bool correct,
	      const size_t type, const Word wordValue, const Cat *cat,
	      const ShiftReduceAction &action);
  private:
      class Impl;
      Impl *_impl;
  };


  // 4


  class S0wcS1wcAttributes
  {
  public:
      S0wcS1wcAttributes(void);
      S0wcS1wcAttributes(S0wcS1wcAttributes &other);
      ~S0wcS1wcAttributes(void);

      size_t size(void) const;
      void SaveWeights(std::ofstream &out);
      void SetFeature(const S0wcS1wcValue &value, double weightT, double weight);

      double GetOrUpdateWeight(bool update, bool correct,
	      const size_t type, const Word s0w, const Cat *s0c, const Word s1w, const Cat *s1c,
	      const ShiftReduceAction &action);
  private:
      class Impl;
      Impl *_impl;
  };


  class S0cS1wAttributes
  {
  public:
      S0cS1wAttributes(void);
      S0cS1wAttributes(S0cS1wAttributes &other);
      ~S0cS1wAttributes(void);

      size_t size(void) const;
      void SaveWeights(std::ofstream &out);
      void SetFeature(const S0cS1wValue &value, double weightT, double weight);

      double GetOrUpdateWeight(bool update, bool correct,
	      const size_t type, const Cat *catS0, const Word wordValueS1,
	      const ShiftReduceAction &action);
  private:
      class Impl;
      Impl *_impl;
  };


  class S0wS1cAttributes
  {
  public:
      S0wS1cAttributes(void);
      S0wS1cAttributes(S0wS1cAttributes &other);
      ~S0wS1cAttributes(void);

      size_t size(void) const;
      void SaveWeights(std::ofstream &out);
      void SetFeature(const S0wS1cValue &value, double weightT, double weight);

      double GetOrUpdateWeight(bool update, bool correct,
	      const size_t type, const Word wordValueS0, const Cat *catS1,
	      const ShiftReduceAction &action);
  private:
      class Impl;
      Impl *_impl;
  };


  class S0cS1cAttributes
  {
  public:
      S0cS1cAttributes(void);
      S0cS1cAttributes(S0cS1cAttributes &other);
      ~S0cS1cAttributes(void);

      size_t size(void) const;
      void SaveWeights(std::ofstream &out);
      void SetFeature(const S0cS1cValue &value, double weightT, double weight);

      double GetOrUpdateWeight(bool update, bool correct,
	      const size_t type, const Cat *catS0, const Cat *catS1,
	      const ShiftReduceAction &action);
  private:
      class Impl;
      Impl *_impl;
  };


  class SwcQwpAttributes
  {
  public:
      SwcQwpAttributes(void);
      SwcQwpAttributes(SwcQwpAttributes &other);
      ~SwcQwpAttributes(void);

      size_t size(void) const;
      void SaveWeights(std::ofstream &out);
      void SetFeature(const SwcQwpValue &value, double weightT, double weight);

      double GetOrUpdateWeight(bool update, bool correct,
	      const size_t type, const Word wordValueS, const Cat *catS, const Word wordValueQ, const Word tagValueQ,
	      const ShiftReduceAction &action);
  private:
      class Impl;
      Impl *_impl;
  };



  class ScQwpAttributes
  {
  public:
      ScQwpAttributes(void);
      ScQwpAttributes(ScQwpAttributes &other);
      ~ScQwpAttributes(void);

      size_t size(void) const;
      void SaveWeights(std::ofstream &out);
      void SetFeature(const ScQwpValue &value, double weightT, double weight);

      double GetOrUpdateWeight(bool update, bool correct,
	      const size_t type, const Cat *catS, const Word wordValueQ, const Word tagValueQ,
	      const ShiftReduceAction &action);
  private:
      class Impl;
      Impl *_impl;
  };


  class SwcQpAttributes
  {
  public:
      SwcQpAttributes(void);
      SwcQpAttributes(SwcQpAttributes &other);
      ~SwcQpAttributes(void);

      size_t size(void) const;
      void SaveWeights(std::ofstream &out);
      void SetFeature(const SwcQpValue &value, double weightT, double weight);

      double GetOrUpdateWeight(bool update, bool correct,
	      const size_t type, const Word wordValueS, const Cat *catS, const Word tagValueQ,
	      const ShiftReduceAction &action);
  private:
      class Impl;
      Impl *_impl;
  };


  class ScQpAttributes
  {
  public:
      ScQpAttributes(void);
      ScQpAttributes(ScQpAttributes &other);
      ~ScQpAttributes(void);

      size_t size(void) const;
      void SaveWeights(std::ofstream &out);
      void SetFeature(const ScQpValue &value, double weightT, double weight);

      double GetOrUpdateWeight(bool update, bool correct,
	      const size_t type, const Cat *catS, const Word tagValueQ,
	      const ShiftReduceAction &action);
  private:
      class Impl;
      Impl *_impl;
  };


  // 5

  class S0wcS1cQ0pAttributes
  {
  public:
      S0wcS1cQ0pAttributes(void);
      S0wcS1cQ0pAttributes(S0wcS1cQ0pAttributes &other);
      ~S0wcS1cQ0pAttributes(void);

      size_t size(void) const;
      void SaveWeights(std::ofstream &out);
      void SetFeature(const S0wcS1cQ0pValue &value, double weightT, double weight);

      double GetOrUpdateWeight(bool update, bool correct,
	      const size_t type, const Word wordValueS0, const Cat *catS0, const Cat *catS1, const Word tagValueQ0,
	      const ShiftReduceAction &action);
  private:
      class Impl;
      Impl *_impl;
  };


  class S0cS1wcQ0pAttributes
  {
  public:
      S0cS1wcQ0pAttributes(void);
      S0cS1wcQ0pAttributes(S0cS1wcQ0pAttributes &other);
      ~S0cS1wcQ0pAttributes(void);

      size_t size(void) const;
      void SaveWeights(std::ofstream &out);
      void SetFeature(const S0cS1wcQ0pValue &value, double weightT, double weight);

      double GetOrUpdateWeight(bool update, bool correct,
	      const size_t type, const Cat *catS0, const Word wordValueS1, const Cat *catS1, const Word tagValueQ0,
	      const ShiftReduceAction &action);
  private:
      class Impl;
      Impl *_impl;
  };


  class S0cS1cQ0wpAttributes
  {
  public:
      S0cS1cQ0wpAttributes(void);
      S0cS1cQ0wpAttributes(S0cS1cQ0wpAttributes &other);
      ~S0cS1cQ0wpAttributes(void);

      size_t size(void) const;
      void SaveWeights(std::ofstream &out);
      void SetFeature(const S0cS1cQ0wpValue &value, double weightT, double weight);

      double GetOrUpdateWeight(bool update, bool correct,
	      const size_t type, const Cat *catS0, const Cat *catS1, const Word wordValueQ0, const Word tagValueQ0,
	      const ShiftReduceAction &action);
  private:
      class Impl;
      Impl *_impl;
  };


  class S0cS1cQ0pAttributes
  {
  public:
      S0cS1cQ0pAttributes(void);
      S0cS1cQ0pAttributes(S0cS1cQ0pAttributes &other);
      ~S0cS1cQ0pAttributes(void);

      size_t size(void) const;
      void SaveWeights(std::ofstream &out);
      void SetFeature(const S0cS1cQ0pValue &value, double weightT, double weight);

      double GetOrUpdateWeight(bool update, bool correct,
	      const size_t type, const Cat *catS0, const Cat *catS1, const Word tagValueQ0,
	      const ShiftReduceAction &action);
  private:
      class Impl;
      Impl *_impl;
  };


  class S0pSQpQSpAttributes
  {
  public:
      S0pSQpQSpAttributes(void);
      S0pSQpQSpAttributes(S0pSQpQSpAttributes &other);
      ~S0pSQpQSpAttributes(void);

      size_t size(void) const;
      void SaveWeights(std::ofstream &out);
      void SetFeature(const S0pSQpQSpValue &value, double weightT, double weight);

      double GetOrUpdateWeight(bool update, bool correct,
	      const size_t type, const Word tagValueS0, const Word tagValueSQ, const Word tagValueQS,
	      const ShiftReduceAction &action);
  private:
      class Impl;
      Impl *_impl;
  };


  class S0wcQ0pQ1pAttributes
  {
  public:
      S0wcQ0pQ1pAttributes(void);
      S0wcQ0pQ1pAttributes(S0wcQ0pQ1pAttributes &other);
      ~S0wcQ0pQ1pAttributes(void);

      size_t size(void) const;
      void SaveWeights(std::ofstream &out);
      void SetFeature(const S0wcQ0pQ1pValue &value, double weightT, double weight);

      double GetOrUpdateWeight(bool update, bool correct,
	      const size_t type, const Word wordValueS0, const Cat *catS0, const Word tagValueQ0, const Word tagValueQ1,
	      const ShiftReduceAction &action);
  private:
      class Impl;
      Impl *_impl;
  };


  class S0cQ0wpQ1pAttributes
  {
  public:
      S0cQ0wpQ1pAttributes(void);
      S0cQ0wpQ1pAttributes(S0cQ0wpQ1pAttributes &other);
      ~S0cQ0wpQ1pAttributes(void);

      size_t size(void) const;
      void SaveWeights(std::ofstream &out);
      void SetFeature(const S0cQ0wpQ1pValue &value, double weightT, double weight);

      double GetOrUpdateWeight(bool update, bool correct,
	      const size_t type, const Cat *catS0, const Word wordValueQ0, const Word tagValueQ0, const Word tagValueQ1,
	      const ShiftReduceAction &action);
  private:
      class Impl;
      Impl *_impl;
  };


  class S0cQ0pQ1wpAttributes
  {
  public:
      S0cQ0pQ1wpAttributes(void);
      S0cQ0pQ1wpAttributes(S0cQ0pQ1wpAttributes &other);
      ~S0cQ0pQ1wpAttributes(void);

      size_t size(void) const;
      void SaveWeights(std::ofstream &out);
      void SetFeature(const S0cQ0pQ1wpValue &value, double weightT, double weight);

      double GetOrUpdateWeight(bool update, bool correct,
	      const size_t type, const Cat *catS0, const Word tagValueQ0, const Word wordValueQ1, const Word tagValueQ1,
	      const ShiftReduceAction &action);
  private:
      class Impl;
      Impl *_impl;
  };


  class S0cQ0pQ1pAttributes
  {
  public:
      S0cQ0pQ1pAttributes(void);
      S0cQ0pQ1pAttributes(S0cQ0pQ1pAttributes &other);
      ~S0cQ0pQ1pAttributes(void);

      size_t size(void) const;
      void SaveWeights(std::ofstream &out);
      void SetFeature(const S0cQ0pQ1pValue &value, double weightT, double weight);

      double GetOrUpdateWeight(bool update, bool correct,
	      const size_t type, const Cat *catS0, const Word tagValueQ0, const Word tagValueQ1,
	      const ShiftReduceAction &action);
  private:
      class Impl;
      Impl *_impl;
  };



  class S0wcS1cS2cAttributes
  {
  public:
      S0wcS1cS2cAttributes(void);
      S0wcS1cS2cAttributes(S0wcS1cS2cAttributes &other);
      ~S0wcS1cS2cAttributes(void);

      size_t size(void) const;
      void SaveWeights(std::ofstream &out);
      void SetFeature(const S0wcS1cS2cValue &value, double weightT, double weight);

      double GetOrUpdateWeight(bool update, bool correct,
	      const size_t type, const Word wordValueS0, const Cat *catS0, const Cat *catS1, const Cat *catS2,
	      const ShiftReduceAction &action);
  private:
      class Impl;
      Impl *_impl;
  };


  class S0cS1wcS2cAttributes
  {
  public:
      S0cS1wcS2cAttributes(void);
      S0cS1wcS2cAttributes(S0cS1wcS2cAttributes &other);
      ~S0cS1wcS2cAttributes(void);

      size_t size(void) const;
      void SaveWeights(std::ofstream &out);
      void SetFeature(const S0cS1wcS2cValue &value, double weightT, double weight);

      double GetOrUpdateWeight(bool update, bool correct,
	      const size_t type, const Cat *catS0, const Word wordValueS1, const Cat *catS1, const Cat *catS2,
	      const ShiftReduceAction &action);
  private:
      class Impl;
      Impl *_impl;
  };


  class S0cS1cS2wcAttributes
  {
  public:
      S0cS1cS2wcAttributes(void);
      S0cS1cS2wcAttributes(S0cS1cS2wcAttributes &other);
      ~S0cS1cS2wcAttributes(void);

      size_t size(void) const;
      void SaveWeights(std::ofstream &out);
      void SetFeature(const S0cS1cS2wcValue &value, double weightT, double weight);

      double GetOrUpdateWeight(bool update, bool correct,
	      const size_t type, const Cat *catS0, const Cat *catS1, const Word wordValueS2, const Cat *catS2,
	      const ShiftReduceAction &action);
  private:
      class Impl;
      Impl *_impl;
  };


  class S0cS1cS2cAttributes
  {
  public:
      S0cS1cS2cAttributes(void);
      S0cS1cS2cAttributes(S0cS1cS2cAttributes &other);
      ~S0cS1cS2cAttributes(void);

      size_t size(void) const;
      void SaveWeights(std::ofstream &out);
      void SetFeature(const S0cS1cS2cValue &value, double weightT, double weight);

      double GetOrUpdateWeight(bool update, bool correct,
	      const size_t type, const Cat *catS0, const Cat *catS1, const Cat *catS2,
	      const ShiftReduceAction &action);
  private:
      class Impl;
      Impl *_impl;
  };


  // 6

  class S01cS01LcS01rcAttributes
  {
  public:
      S01cS01LcS01rcAttributes(void);
      S01cS01LcS01rcAttributes(S01cS01LcS01rcAttributes &other);
      ~S01cS01LcS01rcAttributes(void);

      size_t size(void) const;
      void SaveWeights(std::ofstream &out);
      void SetFeature(const S01cS01LcS01rcValue &value, double weightT, double weight);

      double GetOrUpdateWeight(bool update, bool correct,
	      const size_t type, const Cat *cat0, const Cat *cat1, const Cat *cat2,
	      const ShiftReduceAction &action);
  private:
      class Impl;
      Impl *_impl;
  };


  class S0cS0RcQ0pAttributes
  {
  public:
      S0cS0RcQ0pAttributes(void);
      S0cS0RcQ0pAttributes(S0cS0RcQ0pAttributes &other);
      ~S0cS0RcQ0pAttributes(void);

      size_t size(void) const;
      void SaveWeights(std::ofstream &out);
      void SetFeature(const S0cS0RcQ0pValue &value, double weightT, double weight);

      double GetOrUpdateWeight(bool update, bool correct,
	      const size_t type, const Cat *s0c, const Cat *s0rc, const Word q0p,
	      const ShiftReduceAction &action);
  private:
      class Impl;
      Impl *_impl;
  };

  class S0cS0LRcQ0S1wAttributes
  {
  public:
      S0cS0LRcQ0S1wAttributes(void);
      S0cS0LRcQ0S1wAttributes(S0cS0LRcQ0S1wAttributes &other);
      ~S0cS0LRcQ0S1wAttributes(void);

      size_t size(void) const;
      void SaveWeights(std::ofstream &out);
      void SetFeature(const S0cS0LRcQ0S1wValue &value, double weightT, double weight);

      double GetOrUpdateWeight(bool update, bool correct,
	      const size_t type, const Cat *s0c, const Cat *s0lrc, const Word q0s1w,
	      const ShiftReduceAction &action);
  private:
      class Impl;
      Impl *_impl;
  };


  class S0cS0LS1cS1S1RcAttributes
  {
  public:
      S0cS0LS1cS1S1RcAttributes(void);
      S0cS0LS1cS1S1RcAttributes(S0cS0LS1cS1S1RcAttributes &other);
      ~S0cS0LS1cS1S1RcAttributes(void);

      size_t size(void) const;
      void SaveWeights(std::ofstream &out);
      void SetFeature(const S0cS0LS1cS1S1RcValue &value, double weightT, double weight);

      double GetOrUpdateWeight(bool update, bool correct,
	      const size_t type, const Cat *cat0, const Cat *cat1, const Cat *cat2,
	      const ShiftReduceAction &action);
  private:
      class Impl;
      Impl *_impl;
  };




  class S0wS1cS1RcAttributes
  {
  public:
      S0wS1cS1RcAttributes(void);
      S0wS1cS1RcAttributes(S0wS1cS1RcAttributes &other);
      ~S0wS1cS1RcAttributes(void);

      size_t size(void) const;
      void SaveWeights(std::ofstream &out);
      void SetFeature(const S0wS1cS1RcValue &value, double weightT, double weight);

      double GetOrUpdateWeight(bool update, bool correct,
	      const size_t type, const Word S0w, const Cat *s1c, const Cat *s1rc,
	      const ShiftReduceAction &action);
  private:
      class Impl;
      Impl *_impl;
  };

  }
}




#endif /* SHIFTREDUCEATTRIBUTES_H_ */
