/* -*- Mode: C++; -*- */
// C&C NLP tools
// Copyright (c) Universities of Edinburgh, Oxford and Sydney
// Copyright (c) James R. Curran
//
// This software is covered by a non-commercial use licence.
// See LICENCE.txt for the full text of the licence.
//
// If LICENCE.txt is not included in this distribution
// please email candc@it.usyd.edu.au to obtain a copy.

#pragma once

#include "parser/_printer.h"

// added for SR
#include "model/model.h"
#include "config/config.h"
#include "tagger/tagger.h"
#include "tagger/super.h"
#include "parser/parser.h"
#include "parser/decoder_factory.h"
#include "rnn_parser/rnn_parser.h"

namespace NLP {

namespace CCG {

using namespace NLP::Config;
using namespace NLP::Tagger;

class Integration {
private:
  std::vector<std::vector<std::vector<std::string>* >* > m_supercatsVec;

public:
  static std::vector<double> s_perceptronId;
  static bool s_train;

  typedef std::vector<double> Betas;
  typedef std::vector<ulong> DictCutoffs;

  typedef int Format;

  const static Format FMT_WORDS = 1 << 0;
  const static Format FMT_LEMMA = 1 << 1;
  const static Format FMT_POS = 1 << 2;
  const static Format FMT_CHUNK = 1 << 3;
  const static Format FMT_NER = 1 << 4;
  const static Format FMT_SUPER = 1 << 5;
  const static Format FMT_CAT = 1 << 6;

  const static Format FMT_WS = 1 << 7;

  class Config: public Cfg {
  public:
    Op<ulong> start;
    Op<Betas> betas;
    Op<DictCutoffs> dict_cutoffs;

    Config(const std::string &name = "int",
        const std::string &desc = "supertagger and parser integration config");
    virtual ~Config(void){ /* do nothing */ }

    virtual void check(void);
  };
  public:
  long START;
  Betas BETAS;
  double MIN_BETA;
  DictCutoffs DICT_CUTOFFS;

  Super super;
  Categories cats;
  size_t m_srbeam;
  Parser parser;

  ulong nsentences;
  ulong nwords;

  ulong nexceptions;

  ulong nfail_nospan;
  ulong nfail_explode;
  ulong nfail_nospan_explode;
  ulong nfail_explode_nospan;

  std::vector<ulong> nsuccesses;
  /*      const Config &m_int_cfg;
      Super::Config &m_super_cfg;
      Parser::Config &m_parser_cfg;
      Sentence &m_sent;*/

  Integration(Integration::Config &int_cfg,
      Super::Config &super_cfg,
      RNNParser::RNNConfig &parser_cfg,
      Sentence &sent,
      const bool train, const size_t srbeam,
      ulong load = Parser::LOAD_WEIGHTS);

  Integration(Integration::Config &int_cfg,
      Super::Config &super_cfg,
      Parser::Config &parser_cfg,
      Sentence &sent,
      const bool train, const size_t srbeam,
      ulong load = Parser::LOAD_WEIGHTS);

  Integration(Integration::Config &int_cfg,
      Parser::Config &parser_cfg,
      Sentence &sent,
      const bool train, const size_t srbeam);

  Integration(Integration::Config &int_cfg,
      Super::Config &super_cfg,
      Parser::Config &paser_cfg);

  ~Integration(void){ /* do nothing */ }

  bool parse(Sentence &sent, Decoder &decoder, Printer &printer, bool USE_SUPER = true);
  void LoadSupercats(const std::string &filename);
  void LoadGoldTree(const std::string &filename);
//  bool ShiftReduceParse(bool train, Sentence &sent, Decoder &decoder,
//      Printer &printer, bool allowFrag, IO::Output &out, IO::Output &trace,
//			const Format FORMAT, bool USE_SUPER = true, bool TRACE = false);
//  bool ShiftReduceParseRnn(bool train, Sentence &sent, Decoder &decoder,
//        Printer &printer, bool allowFrag, IO::Output &out, IO::Output &trace,
//				const Format FORMAT, const std::vector<std::vector<std::pair<std::string, double> > > &tags_rnn,
//				bool USE_SUPER = true, bool TRACE = false);
  void LoadModel(const std::string &filename);
  void SaveWeights(std::string &path);
  void ClearParser();

  // only used for training
  //void DestoryParser();

  void
  add_gold(MultiRaw &mtags, const Raw &gold){
    for(MultiRaw::iterator j = mtags.begin(); j != mtags.end(); ++j)
      if(j->raw == gold){
        if(j != mtags.begin())
          j->score = 1.0;
        return;
      }

    ScoredRaw sr(gold, 1.0);
    mtags.push_back(sr);
  }

  void
  force_gold_super(Sentence &sent){
    for(ulong i = 0; i < sent.words.size(); ++i)
      add_gold(sent.msuper[i], sent.super[i]);
  }

  void lexical(Sentence &sent, IO::Output &out, const Format FORMAT){
    const ulong NWORDS = sent.words.size();

    if((FORMAT & FMT_LEMMA) && sent.lemmas.size() != NWORDS)
      throw NLP::Exception("not enough lemmas for the sentence");

    // don't need to check POS tags since they must already exist

    if((FORMAT & FMT_CHUNK) && sent.chunks.size() != NWORDS)
      throw NLP::Exception("not enough chunks for the sentence");

    if((FORMAT & FMT_NER) && sent.entities.size() != NWORDS)
      throw NLP::Exception("not enough named entities for the sentence");

    const bool HAS_CORRECT_STAG = sent.cats.size();

    if(FORMAT & FMT_WORDS){
      out.stream << "<c>";
      for(ulong i = 0; i < NWORDS; ++i){
        out.stream << ' ' << sent.words[i];
        if(FORMAT & FMT_LEMMA)
          out.stream << '|' << sent.lemmas[i];
        if(FORMAT & FMT_POS)
          out.stream << '|' << sent.pos[i];
        if(FORMAT & FMT_CHUNK)
          out.stream << '|' << sent.chunks[i];
        if(FORMAT & FMT_NER)
          out.stream << '|' << sent.entities[i];
        if(FORMAT & FMT_SUPER){
          out.stream << '|';
          if(HAS_CORRECT_STAG)
            sent.cats[i]->out_novar_noX(out.stream, false);
          else
            out.stream << sent.msuper[i][0].raw;
        }
        if(FORMAT & FMT_CAT){
          out.stream << '|';
          if(HAS_CORRECT_STAG)
            sent.cats[i]->out(out.stream);
          else
            out.stream << "none";
        }
      }
      out.stream << '\n';
    }
  }

};

}
}
