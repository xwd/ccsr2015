#ifndef GRAPHFEATURE_H_
#define GRAPHFEATURE_H_

#include <string>

namespace NLP {
namespace CCG {

// Pw, Pt, Cw, Ct
struct PxValue
{
    friend std::ostream &operator<<(std::ostream&, const PxValue&);

    const ulong type;
    const Word word;
    const ulong lr;
    const ulong dist;

    PxValue(const ulong type, const Word word, const ulong lr, const ulong dist)
    : type(type), word(word), lr(lr), dist(dist) {}

    Hash hash(void) const
    {
    	Hash h(type);
    	h += static_cast<ulong>(word);
    	h += lr;
    	h += dist;
    	return h;
    }

    bool equal(const PxValue &other) const
    {
	if(other.type == type && other.word == word
			&& other.lr == lr && other.dist == dist)
	    return true;
	else
	    return false;
    }
};

inline std::ostream &operator<<(std::ostream &out, const PxValue &value)
{
    out << value.type << " "
    	<< value.word.str() << " "
    	<< value.lr << " "
    	<< value.dist;
    return out;
}

// Pwt, Cwt
struct PxyValue
{
    friend std::ostream &operator<<(std::ostream&, const PxyValue&);

    const ulong type;
    const Word word;
    const Word tag;
    const ulong lr;
    const ulong dist;

    PxyValue(const ulong type, const Word word, const Word tag, const ulong lr, const ulong dist)
    : type(type), word(word), tag(tag), lr(lr), dist(dist) {}

    Hash hash(void) const
    {
    	Hash h(type);
    	h += static_cast<ulong>(word);
    	h += static_cast<ulong>(tag);
    	h += lr;
    	h += dist;
    	return h;
    }

    bool equal(const PxyValue &other) const
    {
	if(other.type == type && other.word == word && other.tag == tag
			&& other.lr == lr && other.dist == dist)
	    return true;
	else
	    return false;
    }
};

inline std::ostream &operator<<(std::ostream &out, const PxyValue &value)
{
    out << value.type << " "
    	<< value.word.str() << " "
    	<< value.tag.str() << " "
    	<< value.lr << " "
    	<< value.dist;
    return out;
}

// 3

struct PxyCxyValue
{
    friend std::ostream &operator<<(std::ostream&, const PxyCxyValue&);

    const ulong type;
    const Word w1;
    const Word t1;
    const Word w2;
    const Word t2;
    const ulong lr;
    const ulong dist;

    PxyCxyValue(const ulong type, const Word w1, const Word t1, const Word w2, const Word t2, const ulong lr, const ulong dist)
    : type(type), w1(w1), t1(t1), w2(w2), t2(t2), lr(lr), dist(dist) {}

    Hash hash(void) const
    {
    	Hash h(type);
    	h += static_cast<ulong>(w1);
    	h += static_cast<ulong>(t1);
    	h += static_cast<ulong>(w2);
    	h += static_cast<ulong>(t2);
    	h += lr;
    	h += dist;
    	return h;
    }

    bool equal(const PxyCxyValue &other) const
    {
	if(other.type == type && other.w1 == w1 && other.t1 == t1
			&& other.w2 == w2 && other.t2 == t2
			&& other.lr == lr && other.dist == dist)
	    return true;
	else
	    return false;
    }
};

inline std::ostream &operator<<(std::ostream &out, const PxyCxyValue &value)
{
    out << value.type << " "
    	<< value.w1.str() << " "
    	<< value.t1.str() << " "
    	<< value.w2.str() << " "
    	<< value.t2.str() << " "
    	<< value.lr << " "
    	<< value.dist;
    return out;
}

// 4 and rest of 5

struct XtYtZtValue
{
    friend std::ostream &operator<<(std::ostream&, const XtYtZtValue&);

    const ulong type;
    const Word t1;
    const Word t2;
    const Word t3;
    const ulong lr;
    const ulong dist;

    XtYtZtValue(const ulong type, const Word t1, const Word t2, const Word t3, const ulong lr, const ulong dist)
    : type(type), t1(t1), t2(t2), t3(t3), lr(lr), dist(dist) {}

    Hash hash(void) const
    {
    	Hash h(type);
    	h += static_cast<ulong>(t1);
    	h += static_cast<ulong>(t2);
    	h += static_cast<ulong>(t3);
    	h += lr;
    	h += dist;
    	return h;
    }

    bool equal(const XtYtZtValue &other) const
    {
	if(other.type == type && other.t1 == t1
			&& other.t2 == t2 && other.t3 == t3
			&& other.lr == lr && other.dist == dist)
	    return true;
	else
	    return false;
    }
};

inline std::ostream &operator<<(std::ostream &out, const XtYtZtValue &value)
{
    out << value.type << " "
    	<< value.t1.str() << " "
    	<< value.t2.str() << " "
    	<< value.t3.str() << " "
    	<< value.lr << " "
    	<< value.dist;
    return out;
}

// first 4 of 5

struct WtXtYtZtValue
{
    friend std::ostream &operator<<(std::ostream&, const WtXtYtZtValue&);

    const ulong type;
    const Word t1;
    const Word t2;
    const Word t3;
    const Word t4;
    const ulong lr;
    const ulong dist;

    WtXtYtZtValue(const ulong type, const Word t1, const Word t2, const Word t3, const Word t4, const ulong lr, const ulong dist)
    : type(type), t1(t1), t2(t2), t3(t3), t4(t4), lr(lr), dist(dist) {}

    Hash hash(void) const
    {
    	Hash h(type);
    	h += static_cast<ulong>(t1);
    	h += static_cast<ulong>(t2);
    	h += static_cast<ulong>(t3);
    	h += static_cast<ulong>(t4);
    	h += lr;
    	h += dist;
    	return h;
    }

    bool equal(const WtXtYtZtValue &other) const
    {
	if(other.type == type && other.t1 == t1
			&& other.t2 == t2 && other.t3 == t3 && other.t4 == t4
			&& other.lr == lr && other.dist == dist)
	    return true;
	else
	    return false;
    }
};

inline std::ostream &operator<<(std::ostream &out, const WtXtYtZtValue &value)
{
    out << value.type << " "
    	<< value.t1.str() << " "
    	<< value.t2.str() << " "
    	<< value.t3.str() << " "
    	<< value.t4.str() << " "
    	<< value.lr << " "
    	<< value.dist;
    return out;
}

struct DepValue
{
  friend std::ostream &operator<<(std::ostream&, const DepValue&);

  const ulong type;
  const Word head;
  const RelID rel;
  const Word var;

  const RuleID rule;
  const CatID lrange;
  const ushort dist;

  DepValue(const ulong type, Word head, RelID rel, Word var,
	  RuleID rule, CatID lrange, ushort dist)
    : type(type), head(head), rel(rel), var(var), rule(rule), lrange(lrange),
      dist(dist){}

  Hash hash(void) const {
    Hash h(type);
    h += static_cast<ulong>(head);
    h += rel;
    h += static_cast<ulong>(var);
    h += rule + lrange;
    h += dist;
    return h;
  }

  bool equal(const DepValue &other) const {
    return other.type == type && other.head == head && other.rel == rel &&
	    other.var == var && other.rule == rule && other.lrange == lrange && other.dist == dist;
  }
};

inline std::ostream &operator<<(std::ostream &out, const DepValue &value)
{
    if (value.type < 304)
    {
	assert(value.type > 300);
	out << value.type << " "
	<< value.head.str() << " "
	<< value.rel << " "
	<< value.var.str() << " "
	<< value.rule << " "
	<< value.lrange;
	//<< value.dist;
    }
    else
    {
	assert(value.type >= 304);
	out << value.type << " "
	<< value.head.str() << " "
	<< value.rel << " "
	//<< value.var.str() << " "
	<< value.rule << " "
	<< value.lrange << " "
	<< value.dist;
    }
    return out;
}


// this is for training with gold-standard deps only
struct DepValueSimple
{

  friend std::ostream &operator<<(std::ostream&, const DepValueSimple&);

  const ulong sentId;
  const ulong head;
  const RelID rel;
  const ulong filler;
  const CatID lrange;
  const ulong rule;

  DepValueSimple(const ulong sentId, ulong head, RelID rel, ulong filler,
	  CatID lrange, ulong rule)
    : sentId(sentId), head(head), rel(rel), filler(filler), lrange(lrange), rule(rule) {}

  Hash hash(void) const {
    Hash h(sentId);
    h += head;
    h += rel;
    h += filler;
    h += lrange;
    h += rule;
    return h;
  }

  bool equal(const DepValueSimple &other) const {
    return other.sentId == sentId && other.head == head && other.rel == rel &&
	    other.filler == filler && other.lrange == lrange && other.rule == rule;
  }
};

inline std::ostream &operator<<(std::ostream &out, const DepValueSimple &value)
{
    //out << value.sentId << " "
    out << (int)value.head << " "
    	<< value.rel << " "
    	<< (int)value.filler << " "
    	<< value.lrange << " "
    	<< (int)value.rule;
    return out;
}

struct SRDepValue
{
  friend std::ostream &operator<<(std::ostream&, const SRDepValue&);

  const Word head;
  const Word var;

  const size_t type;
  const ulong rule;
  const RelID rel;
  const CatID lrange;
  const ushort dist;

  SRDepValue(size_t type, Word head, RelID rel, Word var, RuleID rule,
       CatID lrange, ushort dist)
    : head(head), var(var), type(type), rule(static_cast<ulong>(rule)), rel(rel), lrange(lrange),
      dist(dist){}

  SRDepValue(size_t type, Word head, RelID rel, Word var, ulong rule,
       CatID lrange, ushort dist)
    : head(head), var(var), type(type), rule(rule), rel(rel), lrange(lrange),
      dist(dist){}

  Hash hash(void) const {
    Hash h(static_cast<ulong>(head));
    h += static_cast<ulong>(var);
    h += rel;
    h += rule + lrange + type;
    h += dist;
    return h;
  }

  bool equal(const SRDepValue &other) const {
    return other.type == type && other.head == head && other.var == var && other.rel == rel &&
      other.rule == rule && other.lrange == lrange && other.dist == dist;
  }
};

inline std::ostream &operator<<(std::ostream &out, const SRDepValue &value)
{
  out << value.type << " "
      << value.head.str() << " "
      << value.rel << " "
      << value.var.str() << " "
      << value.rule << " "
      << value.lrange << " "
      << value.dist;
  return out;
}

} }

#endif /* GRAPHFEATURE_H_ */
