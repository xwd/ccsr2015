#ifndef SR_CONTEXT_RNN_H_
#define SR_CONTEXT_RNN_H_

#include "parser/ShiftReduceHypothesis.h"
#include <armadillo>

using namespace arma;

namespace NLP { namespace CCG {


class ShiftReduceHypothesis;

class sr_rnn_context {

public:
  const Word *w0, *w1, *w2;

  const Word *s0w, *s1w, *s2w, *s3w, *s4w, *s5w, *s6w, *s7w;
  const Word *q0w, *q1w, *q2w, *q3w, *q4w, *q5w, *q6w, *q7w;
  const Word *s0lw, *s0rw, *s0uw;
  const Word *s1lw, *s1rw, *s1uw;
  const Word *s2lw, *s2rw, *s2uw;

  const Word *s0p, *s1p, *s2p, *s3p, *s4p, *s5p, *s6p, *s7p;
  const Word *q0p, *q1p, *q2p, *q3p, *q4p, *q5p, *q6p, *q7p;
  const Word *s0lp, *s0rp, *s0up;
  const Word *s1lp, *s1rp, *s1up;
  const Word *s2lp, *s2rp, *s2up;

  const SuperCat *s0c, *s1c, *s2c, *s3c, *s4c, *s5c, *s6c, *s7c;
  const SuperCat *q0c, *q1c, *q2c, *q3c, *q4c, *q5c, *q6c, *q7c;
  const SuperCat *s0lc, *s0rc, *s0uc;
  const SuperCat *s1lc, *s1rc, *s1uc;
  const SuperCat *s2lc, *s2rc, *s2uc;

  std::vector<Word> s0w_vec;
  std::vector<Word> s1w_vec;
  std::vector<Word> s2w_vec;
  std::vector<Word> s3w_vec;

  std::vector<Word> s0p_vec;
  std::vector<Word> s1p_vec;
  std::vector<Word> s2p_vec;
  std::vector<Word> s3p_vec;

  std::vector<Word> s0lw_vec;
  std::vector<Word> s0uw_vec;
  std::vector<Word> s1lw_vec;
  std::vector<Word> s1uw_vec;
  std::vector<Word> s0rw_vec;
  std::vector<Word> s1rw_vec;

  std::vector<Word> s0lp_vec;
  std::vector<Word> s1lp_vec;
  std::vector<Word> s0up_vec;
  std::vector<Word> s1up_vec;
  std::vector<Word> s0rp_vec;
  std::vector<Word> s1rp_vec;

  sr_rnn_context()
  :s0w(0), s1w(0), s2w(0), s3w(0), s4w(0), s5w(0), s6w(0), s7w(0),
   q0w(0), q1w(0), q2w(0), q3w(0), q4w(0), q5w(0), q6w(0), q7w(0),
   s0lw(0), s0rw(0), s0uw(0),
   s1lw(0), s1rw(0), s1uw(0),
   s2lw(0), s2rw(0), s2uw(0),
   s0p(0), s1p(0), s2p(0), s3p(0), s4p(0), s5p(0), s6p(0), s7p(0),
   q0p(0), q1p(0), q2p(0), q3p(0), q4p(0), q5p(0), q6p(0), q7p(0),
   s0lp(0), s0rp(0), s0up(0),
   s1lp(0), s1rp(0), s1up(0),
   s2lp(0), s2rp(0), s2up(0),
   s0c(0), s1c(0), s2c(0), s3c(0), s4c(0), s5c(0), s6c(0), s7c(0),
   q0c(0), q1c(0), q2c(0), q3c(0), q4c(0), q5c(0), q6c(0), q7c(0),
   s0lc(0), s0rc(0), s0uc(0),
   s1lc(0), s1rc(0), s1uc(0),
   s2lc(0), s2rc(0), s2uc(0) {}

  ~sr_rnn_context() {
    s0w = 0; s1w = 0; s2w = 0; s3w = 0; s4w = 0; s5w = 0; s6w = 0; s7w = 0;
    q0w = 0; q1w = 0; q2w = 0; q3w = 0; q4w = 0; q5w = 0; q6w = 0; q7w = 0;
    s0lw = 0; s0rw = 0; s0uw = 0;
    s1lw = 0; s1rw = 0; s1uw = 0;
    s2lw = 0; s2rw = 0; s2uw = 0;
    s0p = 0; s1p = 0; s2p = 0; s3p = 0; s4p = 0; s5p = 0; s6p = 0; s7p = 0;
    q0p = 0; q1p(0); q2p = 0; q3p = 0; q4p = 0; q5p = 0; q6p = 0; q7p = 0;
    s0lp = 0; s0rp = 0; s0up = 0;
    s1lp = 0; s1rp = 0; s1up = 0;
    s2lp = 0; s2rp = 0; s2up = 0;
    s0c = 0; s1c = 0; s2c = 0; s3c = 0; s4c = 0; s5c = 0; s6c = 0; s7c = 0;
    q0c = 0; q1c = 0; q2c = 0; q3c = 0; q4c = 0; q5c = 0; q6c = 0; q7c = 0;
    s0lc = 0; s0rc = 0; s0uc = 0;
    s1lc = 0; s1rc = 0; s1uc = 0;
    s2lc = 0; s2rc = 0; s2uc(0);

    s0w_vec.clear();
    s1w_vec.clear();
    s2w_vec.clear();
    s3w_vec.clear();

    s0p_vec.clear();
    s1p_vec.clear();
    s2p_vec.clear();
    s3p_vec.clear();

    s0lw_vec.clear();
    s0uw_vec.clear();
    s1lw_vec.clear();
    s1uw_vec.clear();
    s0rw_vec.clear();
    s1rw_vec.clear();

    s0lp_vec.clear();
    s1lp_vec.clear();
    s0up_vec.clear();
    s1up_vec.clear();
    s0rp_vec.clear();
    s1rp_vec.clear();
  }


  void load_context(const ShiftReduceHypothesis *hypo,
                    const Words &words, const Words &tags,
                    const std::unordered_map<std::string, int> &word_dict,
                    const std::unordered_map<std::string, int> &suf_dict,
                    std::vector<int> &ind_arr_ws,
                    std::vector<int> &ind_arr_lws, std::vector<int> &ind_arr_rws,
                    std::vector<int> &ind_arr_uws,
                    std::vector<int> &ind_arr_wq,
                    std::vector<int> &ind_arr_sufs,
                    std::vector<int> &ind_arr_lsufs, std::vector<int> &ind_arr_rsufs,
                    std::vector<int> &ind_arr_usufs,
                    std::vector<int> &ind_arr_sufq) {
    //if (hypo->GetStackSize() == 0) return; // empty stack, shift only
    assert(words.size() == tags.size());

    s0c = hypo->GetStackSize() >= 1 ? hypo->GetStackTopSuperCat() : 0;
    s1c = hypo->GetStackSize() >= 2 ? hypo->GetPrvStack()->GetStackTopSuperCat() : 0;
    s2c = hypo->GetStackSize() >= 3 ? hypo->GetPrvStack()->GetPrvStack()->GetStackTopSuperCat() : 0;
    s3c = hypo->GetStackSize() >= 4 ? hypo->GetPrvStack()->GetPrvStack()->GetPrvStack()->GetStackTopSuperCat() : 0;

    q0w = hypo->GetNextInputIndex() < words.size() ? &words[hypo->GetNextInputIndex()] : 0;
    q1w = hypo->GetNextInputIndex() + 1 < words.size() ? &words[hypo->GetNextInputIndex() + 1] : 0;
    q2w = hypo->GetNextInputIndex() + 2 < words.size() ? &words[hypo->GetNextInputIndex() + 2] : 0;
    q3w = hypo->GetNextInputIndex() + 3 < words.size() ? &words[hypo->GetNextInputIndex() + 3] : 0;

    q0p = hypo->GetNextInputIndex() < words.size() ? &tags[hypo->GetNextInputIndex()] : 0;
    q1p = hypo->GetNextInputIndex() + 1 < words.size() ? &tags[hypo->GetNextInputIndex() + 1] : 0;
    q2p = hypo->GetNextInputIndex() + 2 < words.size() ? &tags[hypo->GetNextInputIndex() + 2] : 0;
    q3p = hypo->GetNextInputIndex() + 3 < words.size() ? &tags[hypo->GetNextInputIndex() + 3] : 0;

    if (s0c != 0) {
      assert(s0c->cat);
      GetValues(true, s0w_vec, s0p_vec, &s0c->vars[s0c->cat->var], words, tags);

      if (s0c->left != 0 && s0c->right != 0) {
        GetValues(true, s0lw_vec, s0lp_vec, &s0c->left->vars[s0c->left->cat->var], words, tags);
        s0lc = s0c->left->cat;

        GetValues(true, s0rw_vec, s0rp_vec, &s0c->right->vars[s0c->right->cat->var], words, tags);
        s0rc = s0c->right->cat;
      }
      else {
        s0lc = 0;
        s0rc = 0;
      }

      // s0u
      if (hypo->GetStackTopAction() == UNARY) {
        assert(s0c->left != 0);
        assert(s0c->right == 0);
        s0uc = s0c->left->cat;
        GetValues(true, s0uw_vec, s0up_vec, &s0c->left->vars[s0c->left->cat->var], words, tags);
      }
      else {
        s0uc = 0;
      }

    }

    if (s1c != 0) {
      assert(s1c->cat);
      GetValues(true, s1w_vec, s1p_vec, &s1c->vars[s1c->cat->var], words, tags);

      if (s1c->left != 0 && s1c->right != 0) {
        GetValues(true, s1lw_vec, s1lp_vec, &s1c->left->vars[s1c->left->cat->var], words, tags);
        s1lc = s1c->left->cat;

        GetValues(true, s1rw_vec, s1rp_vec, &s1c->right->vars[s1c->right->cat->var], words, tags);
        s1rc = s1c->right->cat;
      }
      else {
        s1lc = 0;
        s1rc = 0;
      }

      // s1u

      if (hypo->GetPrvStack()->GetStackTopAction() == UNARY) {
        assert(s1c->left != 0);
        s1uc = s1c->left->cat;
        GetValues(true, s1uw_vec, s1up_vec, &s1c->left->vars[s1c->left->cat->var], words, tags);
      }
      else {
        s1uc = 0;
      }

    }

    if (s2c != 0) {
      assert(s2c->cat);
      GetValues(true, s2w_vec, s2p_vec, &s2c->vars[s2c->cat->var], words, tags);
    }

    if (s3c != 0) {
      assert(s3c->cat);
      GetValues(true, s3w_vec, s3p_vec, &s3c->vars[s3c->cat->var], words, tags);
    }

    // generate rnn input inds

    if (s0c != 0) {
      if (s0w_vec.size() > 0) {
        ind_arr_ws[0] = get_ind(s0w_vec[0], word_dict, suf_dict, false);
        assert(s0p_vec.size() > 0);
        ind_arr_sufs[0] = get_ind(s0p_vec[0], word_dict, suf_dict, true);
      } else {
        ind_arr_ws[0] = -1;
        ind_arr_sufs[0] = -1;
      }
    } else {
      ind_arr_ws[0] = -1;
      ind_arr_sufs[0] = -1;
    }

    if (s1c != 0) {
      if (s1w_vec.size() > 0) {
        ind_arr_ws[1] = get_ind(s1w_vec[0], word_dict, suf_dict, false);
        assert(s1p_vec.size() > 0);
        ind_arr_sufs[1] = get_ind(s1p_vec[0], word_dict, suf_dict, true);
      } else {
        ind_arr_ws[1] = -1;
        ind_arr_sufs[1] = -1;
      }
    } else {
      ind_arr_ws[1] = -1;
      ind_arr_sufs[1] = -1;
    }

    if (s2c != 0) {
      if (s2w_vec.size() > 0) {
        ind_arr_ws[2] = get_ind(s2w_vec[0], word_dict, suf_dict, false);
        assert(s2p_vec.size() > 0);
        ind_arr_sufs[1] = get_ind(s2p_vec[0], word_dict, suf_dict, true);
      } else {
        ind_arr_ws[2] = -1;
        ind_arr_sufs[2] = -1;
      }
    } else {
      ind_arr_ws[2] = -1;
      ind_arr_sufs[2] = -1;
    }

    if (s3c != 0) {
      if (s3w_vec.size() > 0) {
        ind_arr_ws[3] = get_ind(s3w_vec[0], word_dict, suf_dict, false);
        assert(s3p_vec.size() > 0);
        ind_arr_sufs[3] = get_ind(s3p_vec[0], word_dict, suf_dict, true);
      } else {
        ind_arr_ws[3] = -1;
        ind_arr_sufs[3] = -1;
      }
    } else {
      ind_arr_ws[3] = -1;
      ind_arr_sufs[3] = -1;
    }

    if (s0lc != 0 && s0rc != 0) {
      if (s0lw_vec.size() > 0) {
        ind_arr_lws[0] = get_ind(s0lw_vec[0], word_dict, suf_dict, false);
        assert(s0lp_vec.size() > 0);
        ind_arr_lsufs[0] = get_ind(s0lp_vec[0], word_dict, suf_dict, true);
      } else {
        ind_arr_lws[0] = -1;
        ind_arr_lsufs[0] = -1;
      }

      if (s0rw_vec.size() > 0) {
        ind_arr_rws[0] = get_ind(s0rw_vec[0], word_dict, suf_dict, false);
        assert(s0rp_vec.size() > 0);
        ind_arr_rsufs[0] = get_ind(s0rp_vec[0], word_dict, suf_dict, true);
      } else {
        ind_arr_rws[0] = -1;
        ind_arr_rsufs[0] = -1;
      }
    } else {
      ind_arr_lws[0] = -1;
      ind_arr_lsufs[0] = -1;
    }

    if (s1lc != 0 && s1rc != 0) {
      if (s1lw_vec.size() > 0) {
        ind_arr_lws[1] = get_ind(s1lw_vec[0], word_dict, suf_dict, false);
        assert(s1lp_vec.size() > 0);
        ind_arr_lsufs[1] = get_ind(s1lp_vec[0], word_dict, suf_dict, true);
      } else {
        ind_arr_lws[1] = -1;
        ind_arr_lsufs[1] = -1;
      }

      if (s1rw_vec.size() > 0) {
        ind_arr_rws[1] = get_ind(s1rw_vec[0], word_dict, suf_dict, false);
        assert(s0rp_vec.size() > 0);
        ind_arr_rsufs[1] = get_ind(s1rp_vec[0], word_dict, suf_dict, true);
      } else {
        ind_arr_rws[1] = -1;
        ind_arr_rsufs[1] = -1;
      }
    } else {
      ind_arr_lws[1] = -1;
      ind_arr_lsufs[1] = -1;
    }


    if (s0uc != 0) {
      if (s0uw_vec.size() > 0) {
        ind_arr_uws[0] = get_ind(s0uw_vec[0], word_dict, suf_dict, false);
        assert(s0up_vec.size() > 0);
        ind_arr_usufs[0] = get_ind(s0up_vec[0], word_dict, suf_dict, true);
      } else {
        ind_arr_uws[0] = -1;
        ind_arr_usufs[0] = -1;
      }
    } else {
      ind_arr_uws[0] = -1;
      ind_arr_usufs[0] = -1;
    }

    if (s1uc != 0) {
      if (s1uw_vec.size() > 0) {
        ind_arr_uws[1] = get_ind(s1uw_vec[0], word_dict, suf_dict, false);
        assert(s1up_vec.size() > 0);
        ind_arr_usufs[1] = get_ind(s1up_vec[0], word_dict, suf_dict, true);
      } else {
        ind_arr_uws[1] = -1;
        ind_arr_usufs[1] = -1;
      }
    } else {
      ind_arr_uws[1] = -1;
      ind_arr_usufs[1] = -1;
    }

    if (q0w != 0) {
      ind_arr_wq[0] = get_ind(q0w, word_dict, suf_dict, false);
      ind_arr_sufq[0] = get_ind(q0p, word_dict, suf_dict, ture);
    }
    if (q1w != 0) {
      ind_arr_wq[1] = get_ind(q1w, word_dict, suf_dict, false);
      ind_arr_sufq[1] = get_ind(q1p, word_dict, suf_dict, ture);
    }
    if (q2w != 0) {
      ind_arr_wq[2] = get_ind(q2w, word_dict, suf_dict, false);
      ind_arr_sufq[2] = get_ind(q2p, word_dict, suf_dict, ture);
    }
    if (q3w != 0) {
      ind_arr_wq[3] = get_ind(q3w, word_dict, suf_dict, false);
      ind_arr_sufq[3] = get_ind(q3p, word_dict, suf_dict, ture);
    }

  }


  int get_ind(Word *&word, const std::unordered_map<std::string, int> &word_dict,
      const std::unordered_map<std::string, int> &suf_dict, bool suf) {
    std::string w = word->str();
    std::unordered_map<str::string, int>::const_iterator itr;
    int ind = -1;
    if (suf) {
      itr = suf_dict.find(w);
      if (itr != suf_dict.end())
        ind = itr->second;
    } else {
      itr = word_dict.find(w);
      assert(itr != word_dict.end());
      ind = itr->second;
    }
    return ind;
  }


  void GetValues(bool getPos, std::vector<Word> &heads, std::vector<Word> &tags,
      const Variable *var, const Words &wordValues, const Words &tagValues) const {
    const Position *const end = var->fillers + Variable::NFILLERS;
    for(const Position *p = var->fillers; p != end && *p != Variable::SENTINEL; ++p){
      if(!*p)
        continue;

      Word value = wordValues[*p - 1];
      if(!value)
        continue;

      heads.push_back(value);
      if (getPos) {
        assert(tagValues[*p - 1]);
        tags.push_back(tagValues[*p - 1]);
      }
    }
  }
};


} }


#endif /* SHIFTREDUCECONTEXT_H_ */




