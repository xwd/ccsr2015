#ifndef SRC_LIB_RNN_TAGGER_RNN_TAGGER_H_
#define SRC_LIB_RNN_TAGGER_RNN_TAGGER_H_

// this should be commented out when debugging
#define ARMA_NO_DEBUG

#include <armadillo>
#include <unordered_map>
#include "scored_ind.h"
#include "io/reader.h"
#include "io/reader_factory.h"
#include "tagger/super.h"

using namespace std;
using namespace arma;
using namespace NLP::Tagger;

namespace NLP {
namespace Taggers {

// this is an indicator for supercats not in the 425 classes in wsj02-21
// const static int UNKNWON_SUPERCAT_IND = -1;

class RnnTagger {
public:

  unordered_map<size_t, size_t> m_voc;
  unordered_map<size_t, size_t> m_classes;

  //  vector<vector<vector<size_t> > > m_train_words;
  //  vector<vector<size_t> > m_train_labels;
  //  vector<vector<vector<size_t> > > m_dev_words;
  //  vector<vector<size_t> > m_dev_labels;
  //  vector<vector<vector<size_t> > > m_test_words;
  //  vector<vector<size_t> > m_test_labels;
  unordered_map<size_t, string> m_label2rawtag_map;
  unordered_map<string, size_t> m_full_wordstr_emb_map;
  unordered_map<string, size_t> m_suf_str_ind_map;
  unordered_map<string, size_t> m_lex_cat_str_ind_map;
  unordered_map<size_t, vector<float>> m_ind_pretrained_emb_map;


  vector<vector<vector<pair<string, double> > > > m_all_res_dev;
  vector<vector<vector<pair<string, double> > > > m_all_res_test;
  vector<vector<vector<ScoredInd > > > m_all_res_train;

  size_t m_de;
  size_t m_ds;
  size_t m_dc;
  size_t m_cs;
  size_t m_nh;
  size_t m_bs;
  string m_model_dir;
  bool m_lowercase_words;
  size_t m_nepoch;
  bool m_dropout;
  double m_dropout_success_prob;
  bool m_verbose;
  double m_lr;
  size_t m_nclasses;
  size_t m_vocsize;
  size_t m_suffix_count;
  string m_act;
  double m_clip;
  bool m_bi;
  //  size_t m_ntokens00;
  //  size_t m_ntokens23;

  size_t m_UNK_SUFFIX_IND;
  size_t m_UNKNWON_SUPERCAT_IND;
  size_t m_WORD_PAD_IND;
  size_t m_UNK_ALPHNUM_LOWER_IND;
  size_t m_UNK_ALPHNUM_UPPER_IND;
  size_t m_UNK_NON_ALPHNUM_IND;

  mat m_wx;
  mat m_wh;
  mat m_wy;

  mat m_emb;
  mat m_suf;
  mat m_cap;

  mat m_y;
  mat m_h_tm1;

  // bi-directional related
  mat m_h_tm1_back;
  mat m_wx_back;
  mat m_wh_back;

  size_t m_out_fmt;
  mat m_dropout_vec;
  bool m_use_candc_tagdicts;
  size_t m_candc_tagdict_cutoff;
  bool m_candc_tagdict_pos_backoff;

  // for xf1 training
  vector<mat> m_x_vals;
  vector<mat> m_lh_vals;
  vector<mat> m_lh_vals_back;
  vector<mat> m_lh_deriv_vals;
  vector<mat> m_lh_deriv_vals_back;
  vector<vector<vector<size_t>>> m_contextwins;

  RnnTagger(size_t de, size_t ds, size_t dc,
            size_t cs, size_t nh, size_t bs,
            const string &all_untouched_embeddings,
            const string &suf_str_ind_file,
            const string &model_dir,
            bool lower_case_words,
            size_t nepochs,
            bool dropout,
            double dropout_success_prob,
            bool verbose,
            double lr,
            double clip = 1.0,
            string act = "sig",
            bool bi=true);

  RnnTagger(const string &model_dir,
            const string &all_untouched_embeddings,
            const string &suf_str_ind_file,
            size_t output_fmt,
            double dropout_success_prob,
            bool dropout,
            string act = "sig",
            bool bi = true,
            bool init_weights=true,
            double lr = 0.0025);

  ~RnnTagger(void);

  void train(NLP::IO::ReaderFactory &reader, IO::ReaderFactory &reader_dev);
  void train_mt(IO::ReaderFactory &reader, IO::ReaderFactory &reader_dev, const size_t &thread_id, const string &temp_dir);

  mat sigmoid(const mat &m);
  mat soft_max(const mat &y);
  mat soft_max_log(const mat &y);
  double x_ent_multi(const mat&y, const mat&t);

  void load_train_data(const string &words, const string &labels);
  void load_dev_test_data();
  vector<vector<vector<int> > > load_words(const string &filename);
  vector<vector<int> > load_labels(const string &filename, size_t &ntokens);

  void sent2contextwin(const vector<vector<size_t> > &sent,
                       vector<vector<vector<size_t> > > &contextwins);

  void classify(const vector<vector<vector<size_t> > > &contextwins,
                const vector<size_t> &gold_labels,
                vector<size_t> &sent_res, double &err);
  //  void classify_emb_multi(const vector<vector<vector<size_t> > > &contextwins,
  //                          const vector<size_t> &gold_labels,
  //                          vector<vector<pair<string, double> > > &sent_res,
  //                          vector<double> &sent_res_1best_score, double &err);
  //  void classify_emb_multi_0221(const vector<vector<vector<int> > > &contextwins,
  //                               const vector<size_t> &gold_labels,
  //                               vector<vector<ScoredInd> > &sent_res_ind,
  //                               vector<double> &sent_res_1best_score, double &err);

  double eval_super(bool dev);
  pair<mat, mat> calc_lh_proj_emb(size_t step, const vector<vector<size_t> > &word_vecs, mat *x_vals);
  pair<mat, mat> calc_lh_proj_dropout(size_t step, const vector<vector<size_t> > &word_vecs,
                                      mat *x_vals, mat *x_mask_vals);

  void mtag_train_data(vector<vector<vector<vector<int> > > > &all_sent_contextwins);
  void mtag_file(IO::ReaderFactory &reader, double BETA);
  void load_ind2rawtag_map(const std::string &model_dir);

  vector<vector<pair<unsigned int, string> > >
  gen_kbest(const vector<vector<ScoredInd> > &sent_res_ind_sorted,
            const unsigned int k);
  double get_total_score(const vector<vector<ScoredInd> > &sent_res_ind_sorted,
                         const vector<unsigned int> &ind_vec);

  void re_init_mats(const string &wx, const string &wh, const string &wy,
                    const string &emb, const string &suf, const string &cap);

  void load_suf_str_ind_map(const string &file);
  bool is_alphnum(const string &word) const;
  bool is_lower(const string &word) const;
  string get_suf(const string &word) const;
  void clean_str(string &word, vector<string> &ret) const;
  void replace_all_substrs(string &str, const string &substr, const string &re) const;
  vector<size_t> word2vec(string &word) const;
  double train_bptt_multi_emb(const vector<vector<vector<size_t> > > &contextwins,
                              const vector<size_t> &label_batch, mat &y);
  double train_bptt_multi_dropout(const vector<vector<vector<size_t> > > &contextwins,
                                  const vector<size_t> &label_batch,
                                  mat &y);
  double train_bptt_multi_dropout_bi(const vector<vector<vector<size_t> > > &contextwins,
                                     const vector<size_t> &label_batch,
                                     mat &y);
  double train_bptt_multi_dropout_bi_global_norm_clip(const vector<vector<vector<size_t> > > &contextwins,
                                                      const vector<size_t> &label_batch,
                                                      mat &y);
  void labels2minibatch(const vector<size_t> &labels, vector<vector<size_t> > &minibatch_all) const;
  void contextwin2minibatch(const vector<vector<vector<size_t> > > &sent_cw,
                            vector<vector<vector<vector<size_t> > > > &minibatch_all) const;
  void load_emb_mat(mat &emb, const string &filename);
  void Test_kbest();

  void tag_file(IO::ReaderFactory &reader, bool has_gold_labels);
  void tag_sent(Sentence &sent, vector<size_t> &sent_res,
                vector<size_t> &sent_gold_labels,
                double &err, mat &y, bool has_gold_labels);
  void tag_sent_bi(Sentence &sent, vector<size_t> &sent_res,
                   vector<size_t> &sent_gold_labels,
                   double &err, mat &y, bool has_gold_labels);
  void classify(const vector<vector<vector<size_t> > > &contextwins,
                vector<size_t> &gold_labels,
                mat &y, vector<size_t> &sent_res, double &err, bool has_gold_labels);
  void classify_bi(const vector<vector<vector<size_t> > > &contextwins,
                   vector<size_t> &gold_labels,
                   mat &y, vector<size_t> &sent_res, double &err, bool has_gold_labels);

  void eval_mtag(IO::ReaderFactory &reader);
  void mtag_sent(Sentence &sent, double BETA);
  void classify_multi(const vector<vector<vector<size_t> > > &contextwins,
                      Sentence &sent, double BETA);
  void classify_multi_bi(const vector<vector<vector<size_t>>> &contextwins,
                         Sentence &sent, double BETA);

  void load_emb_word_ind_map(const string &filename);

  void train_xf1_bi(Sentence &sent, const mat &pxf1f1,
                    const vector<vector<size_t>> &all_tag_seq_inds,
                    const vector<unordered_map<size_t, vector<size_t>>> &word_position_tag_hypos_map_vec);
  void run_bptt_xf1_bi(const vector<mat> &ly_vals);


  // grad check (bidirectional)
  void grad_check_bi(IO::ReaderFactory &reader, IO::ReaderFactory &reader_dev);
  void tag_sent_bi_grad_check(Sentence &sent,
                              vector<size_t> &sent_res,
                              vector<size_t> &sent_gold_labels,
                              double &err, mat &y);
  void classify_bi_grad_check(const vector<vector<vector<size_t> > > &contextwins,
                              vector<size_t> &gold_labels, mat &y, double &err);
  void train_grad_check(NLP::IO::ReaderFactory &reader, IO::ReaderFactory &reader_dev);
  double train_bptt_multi_dropout_bi_grad_check(const vector<vector<vector<size_t> > > &contextwins,
                                                const vector<size_t> &label_batch,
                                                mat &y);
  void save_xf1_model(const size_t &i, const string &model_dir);
};

}
}

#endif /* SRC_LIB_RNN_TAGGER_RNN_TAGGER_H_ */
